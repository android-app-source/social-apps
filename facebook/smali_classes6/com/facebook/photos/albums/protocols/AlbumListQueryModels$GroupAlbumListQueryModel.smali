.class public final Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66904ffe
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 973791
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 973790
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 973788
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 973789
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 973782
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 973783
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 973784
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 973785
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 973786
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 973787
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 973774
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 973775
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 973776
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 973777
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 973778
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;

    .line 973779
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;->e:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 973780
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 973781
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 973792
    new-instance v0, LX/5gC;

    invoke-direct {v0, p1}, LX/5gC;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973772
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;->e:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;->e:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 973773
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;->e:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 973770
    invoke-virtual {p2}, LX/18L;->a()V

    .line 973771
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 973769
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 973766
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;-><init>()V

    .line 973767
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 973768
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 973765
    const v0, -0x883d252

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 973764
    const v0, 0x41e065f

    return v0
.end method
