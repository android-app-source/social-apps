.class public final Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 976378
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 976379
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 976380
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 976381
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 976382
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v4, 0x1

    const-wide/16 v6, 0x0

    .line 976383
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 976384
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 976385
    if-eqz v2, :cond_0

    .line 976386
    const-string v3, "album_cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976387
    invoke-static {v1, v2, p1, p2}, LX/5go;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 976388
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 976389
    if-eqz v2, :cond_1

    .line 976390
    const-string v2, "album_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976391
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 976392
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 976393
    if-eqz v2, :cond_2

    .line 976394
    const-string v3, "allow_contributors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976395
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 976396
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 976397
    if-eqz v2, :cond_3

    .line 976398
    const-string v3, "can_edit_caption"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976399
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 976400
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 976401
    if-eqz v2, :cond_4

    .line 976402
    const-string v3, "can_upload"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976403
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 976404
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 976405
    if-eqz v2, :cond_5

    .line 976406
    const-string v3, "can_viewer_delete"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976407
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 976408
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 976409
    cmp-long v4, v2, v6

    if-eqz v4, :cond_6

    .line 976410
    const-string v4, "created_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976411
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 976412
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 976413
    if-eqz v2, :cond_7

    .line 976414
    const-string v3, "explicit_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976415
    invoke-static {v1, v2, p1}, LX/5gp;->a(LX/15i;ILX/0nX;)V

    .line 976416
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 976417
    if-eqz v2, :cond_8

    .line 976418
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976419
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 976420
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 976421
    if-eqz v2, :cond_9

    .line 976422
    const-string v3, "media_owner_object"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976423
    invoke-static {v1, v2, p1}, LX/5gq;->a(LX/15i;ILX/0nX;)V

    .line 976424
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 976425
    if-eqz v2, :cond_a

    .line 976426
    const-string v3, "message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976427
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 976428
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 976429
    cmp-long v4, v2, v6

    if-eqz v4, :cond_b

    .line 976430
    const-string v4, "modified_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976431
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 976432
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 976433
    if-eqz v2, :cond_c

    .line 976434
    const-string v3, "owner"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976435
    invoke-static {v1, v2, p1}, LX/5gr;->a(LX/15i;ILX/0nX;)V

    .line 976436
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 976437
    if-eqz v2, :cond_d

    .line 976438
    const-string v3, "photo_items"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976439
    invoke-static {v1, v2, p1}, LX/5gs;->a(LX/15i;ILX/0nX;)V

    .line 976440
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 976441
    if-eqz v2, :cond_e

    .line 976442
    const-string v3, "privacy_scope"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976443
    invoke-static {v1, v2, p1, p2}, LX/5gv;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 976444
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 976445
    if-eqz v2, :cond_f

    .line 976446
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976447
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 976448
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 976449
    if-eqz v2, :cond_10

    .line 976450
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976451
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 976452
    :cond_10
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 976453
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 976454
    check-cast p1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$Serializer;->a(Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
