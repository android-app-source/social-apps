.class public final Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/5gd;
.implements LX/5gc;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1ecc4273
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkContributorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:J

.field private m:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:J

.field private s:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 975567
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 975568
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 975569
    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 975570
    return-void
.end method

.method private A()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975571
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->q:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->q:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 975572
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->q:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private B()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975573
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->s:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->s:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    .line 975574
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->s:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    return-object v0
.end method

.method private C()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975575
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->u:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->u:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    .line 975576
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->u:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975577
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->v:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->v:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 975578
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->v:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private w()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975579
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->e:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->e:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    .line 975580
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->e:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    return-object v0
.end method

.method private x()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975549
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->m:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->m:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    .line 975550
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->m:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    return-object v0
.end method

.method private y()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975688
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->o:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->o:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    .line 975689
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->o:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    return-object v0
.end method

.method private z()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975581
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->p:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->p:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    .line 975582
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->p:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 18

    .prologue
    .line 975583
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 975584
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->w()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 975585
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->c()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 975586
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->u()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 975587
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->x()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 975588
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->l()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 975589
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->y()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 975590
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->z()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 975591
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->A()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 975592
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->B()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 975593
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->v()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, 0x4222642f

    invoke-static {v6, v5, v7}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 975594
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->C()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 975595
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 975596
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->t()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 975597
    const/16 v5, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 975598
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v2}, LX/186;->b(II)V

    .line 975599
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 975600
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->g:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 975601
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->h:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 975602
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 975603
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->j:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 975604
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 975605
    const/4 v3, 0x7

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->l:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 975606
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 975607
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 975608
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 975609
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 975610
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 975611
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->r:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 975612
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 975613
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 975614
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 975615
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 975616
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 975617
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 975618
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 975619
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 975620
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->w()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 975621
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->w()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    .line 975622
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->w()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 975623
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    .line 975624
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->e:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    .line 975625
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->u()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 975626
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->u()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 975627
    if-eqz v2, :cond_1

    .line 975628
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    .line 975629
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->k:Ljava/util/List;

    move-object v1, v0

    .line 975630
    :cond_1
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->x()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 975631
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->x()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    .line 975632
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->x()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 975633
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    .line 975634
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->m:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    .line 975635
    :cond_2
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->y()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 975636
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->y()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    .line 975637
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->y()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 975638
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    .line 975639
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->o:Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    .line 975640
    :cond_3
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->z()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 975641
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->z()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    .line 975642
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->z()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 975643
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    .line 975644
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->p:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    .line 975645
    :cond_4
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->A()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 975646
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->A()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 975647
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->A()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 975648
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    .line 975649
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->q:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 975650
    :cond_5
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->B()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 975651
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->B()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    .line 975652
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->B()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 975653
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    .line 975654
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->s:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    .line 975655
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->v()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_7

    .line 975656
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->v()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4222642f

    invoke-static {v2, v0, v3}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 975657
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->v()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 975658
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    .line 975659
    iput v3, v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->t:I

    move-object v1, v0

    .line 975660
    :cond_7
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->C()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 975661
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->C()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    .line 975662
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->C()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 975663
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    .line 975664
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->u:Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    .line 975665
    :cond_8
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 975666
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 975667
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 975668
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    .line 975669
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->v:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 975670
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 975671
    if-nez v1, :cond_a

    :goto_0
    return-object p0

    .line 975672
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_a
    move-object p0, v1

    .line 975673
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975674
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 975675
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 975676
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->g:Z

    .line 975677
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->h:Z

    .line 975678
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->i:Z

    .line 975679
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->j:Z

    .line 975680
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->l:J

    .line 975681
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->r:J

    .line 975682
    const/16 v0, 0xf

    const v1, 0x4222642f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->t:I

    .line 975683
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 975684
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;-><init>()V

    .line 975685
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 975686
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975687
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->w()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final bQ_()Z
    .locals 2

    .prologue
    .line 975563
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 975564
    iget-boolean v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->i:Z

    return v0
.end method

.method public final bR_()Z
    .locals 2

    .prologue
    .line 975565
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 975566
    iget-boolean v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->j:Z

    return v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975534
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 975535
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 975536
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 975537
    iget-boolean v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 975538
    const v0, 0x2e3820bb

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 975539
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 975540
    iget-boolean v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 975541
    const v0, 0x3c68e4f

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 975542
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 975543
    iget-wide v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->l:J

    return-wide v0
.end method

.method public final synthetic k()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975544
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->x()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975545
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->n:Ljava/lang/String;

    .line 975546
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic m()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975547
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->y()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic n()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975548
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->z()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic o()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975551
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->A()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final p()J
    .locals 2

    .prologue
    .line 975552
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 975553
    iget-wide v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->r:J

    return-wide v0
.end method

.method public final synthetic q()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975554
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->B()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic r()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975555
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->C()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975556
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975557
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->w:Ljava/lang/String;

    .line 975558
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final u()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getContributors"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkContributorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 975559
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkContributorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->k:Ljava/util/List;

    .line 975560
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final v()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhotoItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 975561
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 975562
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->t:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
