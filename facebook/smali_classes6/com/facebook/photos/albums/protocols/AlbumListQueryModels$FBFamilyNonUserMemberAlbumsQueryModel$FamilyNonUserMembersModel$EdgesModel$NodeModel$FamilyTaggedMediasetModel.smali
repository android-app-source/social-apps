.class public final Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x12065d17
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 973564
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 973563
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 973561
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 973562
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 973553
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 973554
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 973555
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x653a24b0

    invoke-static {v2, v1, v3}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 973556
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 973557
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 973558
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 973559
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 973560
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 973538
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 973539
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 973540
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    .line 973541
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 973542
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    .line 973543
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    .line 973544
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 973545
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x653a24b0

    invoke-static {v2, v0, v3}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 973546
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 973547
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    .line 973548
    iput v3, v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->f:I

    move-object v1, v0

    .line 973549
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 973550
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 973551
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 973552
    goto :goto_0
.end method

.method public final a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMedia"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973536
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    .line 973537
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 973526
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 973527
    const/4 v0, 0x1

    const v1, 0x653a24b0

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->f:I

    .line 973528
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 973533
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;-><init>()V

    .line 973534
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 973535
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 973532
    const v0, -0x2ecaae14

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 973531
    const v0, -0x3df20c95

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 973529
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 973530
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
