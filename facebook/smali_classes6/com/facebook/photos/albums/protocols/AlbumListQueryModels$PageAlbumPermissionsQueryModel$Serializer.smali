.class public final Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 973942
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 973943
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 973915
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 973917
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 973918
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x4

    const/4 v3, 0x0

    .line 973919
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 973920
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 973921
    if-eqz v2, :cond_0

    .line 973922
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 973923
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 973924
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 973925
    if-eqz v2, :cond_1

    .line 973926
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 973927
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 973928
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 973929
    if-eqz v2, :cond_2

    .line 973930
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 973931
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 973932
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 973933
    if-eqz v2, :cond_3

    .line 973934
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 973935
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 973936
    :cond_3
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 973937
    if-eqz v2, :cond_4

    .line 973938
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 973939
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 973940
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 973941
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 973916
    check-cast p1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel$Serializer;->a(Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumPermissionsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
