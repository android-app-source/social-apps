.class public final Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4ce5a932
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 974039
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 974042
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 974040
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 974041
    return-void
.end method

.method private a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 974037
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    iput-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    .line 974038
    iget-object v0, p0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 974043
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 974044
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 974045
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 974046
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 974047
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 974048
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 974029
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 974030
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 974031
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    .line 974032
    invoke-direct {p0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 974033
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel;

    .line 974034
    iput-object v0, v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel;->e:Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$SimpleAlbumFieldsModel;

    .line 974035
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 974036
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 974024
    new-instance v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$PageAlbumsFragmentModel$EdgesModel;-><init>()V

    .line 974025
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 974026
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 974028
    const v0, 0x3ad2ea81

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 974027
    const v0, 0x110f03d0

    return v0
.end method
