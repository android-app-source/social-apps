.class public final Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 973723
    const-class v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;

    new-instance v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 973724
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 973725
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 973726
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 973727
    const/4 v2, 0x0

    .line 973728
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 973729
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 973730
    :goto_0
    move v1, v2

    .line 973731
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 973732
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 973733
    new-instance v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;

    invoke-direct {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;-><init>()V

    .line 973734
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 973735
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 973736
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 973737
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 973738
    :cond_0
    return-object v1

    .line 973739
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 973740
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, p0, :cond_3

    .line 973741
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 973742
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 973743
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v3, :cond_2

    .line 973744
    const-string p0, "group_albums"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 973745
    invoke-static {p1, v0}, LX/4Kp;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 973746
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 973747
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 973748
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1
.end method
