.class public Lcom/facebook/photos/creativeediting/model/DoodleParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/5i8;
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/DoodleParamsDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/DoodleParamsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/model/DoodleParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field private final overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "relative_image_overlay_params"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 982412
    const-class v0, Lcom/facebook/photos/creativeediting/model/DoodleParamsDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 982413
    const-class v0, Lcom/facebook/photos/creativeediting/model/DoodleParamsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 982414
    new-instance v0, LX/5iD;

    invoke-direct {v0}, LX/5iD;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 982415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 982416
    iput-object v2, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->id:Ljava/lang/String;

    .line 982417
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->newBuilder()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setLeftPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setTopPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setWidthPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setHeightPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setRotationDegree(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->a()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    .line 982418
    return-void
.end method

.method public constructor <init>(LX/5iF;)V
    .locals 2

    .prologue
    .line 982419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 982420
    iget-object v0, p1, LX/5iF;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->id:Ljava/lang/String;

    .line 982421
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->newBuilder()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v1

    iget-object v0, p1, LX/5iF;->a:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5iF;->b:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setLeftPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5iF;->c:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setTopPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5iF;->d:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setWidthPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5iF;->e:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setHeightPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5iF;->f:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setRotationDegree(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->a()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    .line 982422
    return-void

    .line 982423
    :cond_0
    iget-object v0, p1, LX/5iF;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    .line 982424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 982425
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->id:Ljava/lang/String;

    .line 982426
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 982427
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    .line 982428
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    .line 982429
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v3

    .line 982430
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v4

    .line 982431
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v5

    .line 982432
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->newBuilder()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setLeftPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setTopPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setWidthPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setHeightPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setRotationDegree(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->a()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    .line 982433
    return-void
.end method

.method private static a(FF)Z
    .locals 4
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 982460
    sub-float v0, p0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;Landroid/graphics/PointF;F)LX/362;
    .locals 2

    .prologue
    .line 982434
    new-instance v0, LX/5iF;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5iF;-><init>(Landroid/net/Uri;)V

    iget v1, p1, Landroid/graphics/RectF;->left:F

    .line 982435
    iput v1, v0, LX/5iF;->b:F

    .line 982436
    move-object v0, v0

    .line 982437
    iget v1, p1, Landroid/graphics/RectF;->top:F

    .line 982438
    iput v1, v0, LX/5iF;->c:F

    .line 982439
    move-object v0, v0

    .line 982440
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    .line 982441
    iput v1, v0, LX/5iF;->d:F

    .line 982442
    move-object v0, v0

    .line 982443
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 982444
    iput v1, v0, LX/5iF;->e:F

    .line 982445
    move-object v0, v0

    .line 982446
    iput p3, v0, LX/5iF;->f:F

    .line 982447
    move-object v0, v0

    .line 982448
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->id:Ljava/lang/String;

    .line 982449
    iput-object v1, v0, LX/5iF;->g:Ljava/lang/String;

    .line 982450
    move-object v0, v0

    .line 982451
    invoke-virtual {v0}, LX/5iF;->a()Lcom/facebook/photos/creativeediting/model/DoodleParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 982452
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    .line 982453
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    .line 982454
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 982455
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 982456
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v2, v0

    add-int/2addr v3, v1

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4
.end method

.method public final a()Landroid/graphics/RectF;
    .locals 6

    .prologue
    .line 982457
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v3

    iget-object v4, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v4

    iget-object v5, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v5}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v5

    add-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public final b()Landroid/graphics/PointF;
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 982458
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v2

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v3

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 982459
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getRotationDegree()F

    move-result v0

    return v0
.end method

.method public final d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 982408
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 982409
    const/4 v0, 0x0

    .line 982410
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 982411
    const/4 v0, 0x0

    return v0
.end method

.method public final e()F
    .locals 1

    .prologue
    .line 982375
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 982376
    if-ne p1, p0, :cond_1

    .line 982377
    :cond_0
    :goto_0
    return v0

    .line 982378
    :cond_1
    instance-of v2, p1, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 982379
    goto :goto_0

    .line 982380
    :cond_2
    check-cast p1, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    .line 982381
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->l()F

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->l()F

    move-result v3

    invoke-static {v2, v3}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->m()F

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->m()F

    move-result v3

    invoke-static {v2, v3}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->e()F

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->e()F

    move-result v3

    invoke-static {v2, v3}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->f()F

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->f()F

    move-result v3

    invoke-static {v2, v3}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->c()F

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->c()F

    move-result v3

    invoke-static {v2, v3}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/DoodleParams;->id:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->d()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->d()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 982382
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 982383
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 982384
    const/4 v0, 0x0

    return v0
.end method

.method public final hashCode()I
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 982385
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 982386
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 982387
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 982388
    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 982389
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 982390
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 982391
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 982392
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getRotationDegree()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 982393
    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 982394
    const/4 v0, 0x0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 982395
    const/4 v0, 0x0

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 982396
    const/4 v0, 0x0

    return v0
.end method

.method public final l()F
    .locals 1

    .prologue
    .line 982397
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v0

    return v0
.end method

.method public final m()F
    .locals 1

    .prologue
    .line 982398
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v0

    return v0
.end method

.method public final n()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;
    .locals 1

    .prologue
    .line 982399
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 982400
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 982401
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 982402
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 982403
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 982404
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 982405
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 982406
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getRotationDegree()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 982407
    return-void
.end method
