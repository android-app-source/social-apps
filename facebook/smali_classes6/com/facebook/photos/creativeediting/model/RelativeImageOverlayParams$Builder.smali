.class public final Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:LX/5jD;

.field private static final b:Ljava/lang/String;


# instance fields
.field public c:F

.field public d:F

.field public e:Ljava/lang/String;

.field public f:F

.field public g:F

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:F


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 985912
    const-class v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 985908
    new-instance v0, LX/5jD;

    invoke-direct {v0}, LX/5jD;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->a:LX/5jD;

    .line 985909
    new-instance v0, LX/5jC;

    invoke-direct {v0}, LX/5jC;-><init>()V

    .line 985910
    const-string v0, "RelativeImageOverlay"

    move-object v0, v0

    .line 985911
    sput-object v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 985905
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 985906
    sget-object v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->e:Ljava/lang/String;

    .line 985907
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;
    .locals 2

    .prologue
    .line 985904
    new-instance v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;-><init>(Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;)V

    return-object v0
.end method

.method public setHeightPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "height_percentage"
    .end annotation

    .prologue
    .line 985902
    iput p1, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->c:F

    .line 985903
    return-object p0
.end method

.method public setLeftPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "left_percentage"
    .end annotation

    .prologue
    .line 985913
    iput p1, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->d:F

    .line 985914
    return-object p0
.end method

.method public setRenderKey(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "render_key"
    .end annotation

    .prologue
    .line 985899
    const-string v0, "RelativeImageOverlay"

    move-object v0, v0

    .line 985900
    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->e:Ljava/lang/String;

    .line 985901
    return-object p0
.end method

.method public setRotationDegree(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rotation_degree"
    .end annotation

    .prologue
    .line 985897
    iput p1, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->f:F

    .line 985898
    return-object p0
.end method

.method public setTopPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "top_percentage"
    .end annotation

    .prologue
    .line 985895
    iput p1, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->g:F

    .line 985896
    return-object p0
.end method

.method public setUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uri"
    .end annotation

    .prologue
    .line 985891
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->h:Ljava/lang/String;

    .line 985892
    return-object p0
.end method

.method public setWidthPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "width_percentage"
    .end annotation

    .prologue
    .line 985893
    iput p1, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->i:F

    .line 985894
    return-object p0
.end method
