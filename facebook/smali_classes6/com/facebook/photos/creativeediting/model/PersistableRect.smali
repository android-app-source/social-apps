.class public Lcom/facebook/photos/creativeediting/model/PersistableRect;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/PersistableRect$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/PersistableRectSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/model/PersistableRect;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:F

.field private final b:F

.field private final c:F

.field private final d:F


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 985838
    const-class v0, Lcom/facebook/photos/creativeediting/model/PersistableRect$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 985837
    const-class v0, Lcom/facebook/photos/creativeediting/model/PersistableRectSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 985836
    new-instance v0, LX/5j9;

    invoke-direct {v0}, LX/5j9;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 985830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 985831
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->a:F

    .line 985832
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->b:F

    .line 985833
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->c:F

    .line 985834
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->d:F

    .line 985835
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;)V
    .locals 1

    .prologue
    .line 985799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 985800
    iget v0, p1, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->a:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->a:F

    .line 985801
    iget v0, p1, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->b:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->b:F

    .line 985802
    iget v0, p1, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->c:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->c:F

    .line 985803
    iget v0, p1, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->d:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->d:F

    .line 985804
    return-void
.end method

.method public static newBuilder()Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;
    .locals 2

    .prologue
    .line 985829
    new-instance v0, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 985828
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 985815
    if-ne p0, p1, :cond_1

    .line 985816
    :cond_0
    :goto_0
    return v0

    .line 985817
    :cond_1
    instance-of v2, p1, Lcom/facebook/photos/creativeediting/model/PersistableRect;

    if-nez v2, :cond_2

    move v0, v1

    .line 985818
    goto :goto_0

    .line 985819
    :cond_2
    check-cast p1, Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 985820
    iget v2, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->a:F

    iget v3, p1, Lcom/facebook/photos/creativeediting/model/PersistableRect;->a:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    move v0, v1

    .line 985821
    goto :goto_0

    .line 985822
    :cond_3
    iget v2, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->b:F

    iget v3, p1, Lcom/facebook/photos/creativeediting/model/PersistableRect;->b:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_4

    move v0, v1

    .line 985823
    goto :goto_0

    .line 985824
    :cond_4
    iget v2, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->c:F

    iget v3, p1, Lcom/facebook/photos/creativeediting/model/PersistableRect;->c:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_5

    move v0, v1

    .line 985825
    goto :goto_0

    .line 985826
    :cond_5
    iget v2, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->d:F

    iget v3, p1, Lcom/facebook/photos/creativeediting/model/PersistableRect;->d:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    move v0, v1

    .line 985827
    goto :goto_0
.end method

.method public getBottom()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bottom"
    .end annotation

    .prologue
    .line 985814
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->a:F

    return v0
.end method

.method public getLeft()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "left"
    .end annotation

    .prologue
    .line 985813
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->b:F

    return v0
.end method

.method public getRight()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "right"
    .end annotation

    .prologue
    .line 985812
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->c:F

    return v0
.end method

.method public getTop()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "top"
    .end annotation

    .prologue
    .line 985811
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->d:F

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 985810
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->a:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->c:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->d:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 985805
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 985806
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 985807
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->c:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 985808
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 985809
    return-void
.end method
