.class public final Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3d6d0829
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 984639
    const-class v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 984645
    const-class v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 984643
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 984644
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 984640
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 984641
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 984642
    return-void
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;
    .locals 4

    .prologue
    .line 984587
    if-nez p0, :cond_0

    .line 984588
    const/4 p0, 0x0

    .line 984589
    :goto_0
    return-object p0

    .line 984590
    :cond_0
    instance-of v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    if-eqz v0, :cond_1

    .line 984591
    check-cast p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    goto :goto_0

    .line 984592
    :cond_1
    new-instance v2, LX/5ii;

    invoke-direct {v2}, LX/5ii;-><init>()V

    .line 984593
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->b()J

    move-result-wide v0

    iput-wide v0, v2, LX/5ii;->a:J

    .line 984594
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 984595
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 984596
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 984597
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 984598
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/5ii;->b:LX/0Px;

    .line 984599
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/5ii;->c:Ljava/lang/String;

    .line 984600
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/5ii;->d:Ljava/lang/String;

    .line 984601
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->bd_()J

    move-result-wide v0

    iput-wide v0, v2, LX/5ii;->e:J

    .line 984602
    invoke-virtual {v2}, LX/5ii;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    .line 984627
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 984628
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 984629
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 984630
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 984631
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 984632
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->e:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 984633
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 984634
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 984635
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 984636
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->i:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 984637
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 984638
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 984619
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 984620
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 984621
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 984622
    if-eqz v1, :cond_0

    .line 984623
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 984624
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->f:Ljava/util/List;

    .line 984625
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 984626
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 984618
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 984646
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 984647
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->e:J

    .line 984648
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->i:J

    .line 984649
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 984616
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 984617
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->e:J

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 984613
    new-instance v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;-><init>()V

    .line 984614
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 984615
    return-object v0
.end method

.method public final bd_()J
    .locals 2

    .prologue
    .line 984611
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 984612
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->i:J

    return-wide v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 984609
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->f:Ljava/util/List;

    .line 984610
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 984607
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->g:Ljava/lang/String;

    .line 984608
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 984606
    const v0, -0x22b940d2

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 984604
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->h:Ljava/lang/String;

    .line 984605
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 984603
    const v0, 0x469f7c12

    return v0
.end method
