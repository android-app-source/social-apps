.class public Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParamsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 985984
    const-class v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    new-instance v1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParamsSerializer;

    invoke-direct {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParamsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 985985
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 985986
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 985987
    if-nez p0, :cond_0

    .line 985988
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 985989
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 985990
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParamsSerializer;->b(Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;LX/0nX;LX/0my;)V

    .line 985991
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 985992
    return-void
.end method

.method private static b(Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 985993
    const-string v0, "height_percentage"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 985994
    const-string v0, "left_percentage"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 985995
    const-string v0, "render_key"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->renderKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 985996
    const-string v0, "rotation_degree"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getRotationDegree()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 985997
    const-string v0, "top_percentage"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 985998
    const-string v0, "uri"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 985999
    const-string v0, "width_percentage"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 986000
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 986001
    check-cast p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParamsSerializer;->a(Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;LX/0nX;LX/0my;)V

    return-void
.end method
