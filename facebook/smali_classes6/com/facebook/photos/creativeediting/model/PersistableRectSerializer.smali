.class public Lcom/facebook/photos/creativeediting/model/PersistableRectSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/creativeediting/model/PersistableRect;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 985839
    const-class v0, Lcom/facebook/photos/creativeediting/model/PersistableRect;

    new-instance v1, Lcom/facebook/photos/creativeediting/model/PersistableRectSerializer;

    invoke-direct {v1}, Lcom/facebook/photos/creativeediting/model/PersistableRectSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 985840
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 985841
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/creativeediting/model/PersistableRect;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 985842
    if-nez p0, :cond_0

    .line 985843
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 985844
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 985845
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/creativeediting/model/PersistableRectSerializer;->b(Lcom/facebook/photos/creativeediting/model/PersistableRect;LX/0nX;LX/0my;)V

    .line 985846
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 985847
    return-void
.end method

.method private static b(Lcom/facebook/photos/creativeediting/model/PersistableRect;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 985848
    const-string v0, "bottom"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getBottom()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 985849
    const-string v0, "left"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getLeft()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 985850
    const-string v0, "right"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getRight()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 985851
    const-string v0, "top"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getTop()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 985852
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 985853
    check-cast p1, Lcom/facebook/photos/creativeediting/model/PersistableRect;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/creativeediting/model/PersistableRectSerializer;->a(Lcom/facebook/photos/creativeediting/model/PersistableRect;LX/0nX;LX/0my;)V

    return-void
.end method
