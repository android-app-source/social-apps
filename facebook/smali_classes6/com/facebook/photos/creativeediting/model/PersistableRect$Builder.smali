.class public final Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/PersistableRect_BuilderDeserializer;
.end annotation


# instance fields
.field public a:F

.field public b:F

.field public c:F

.field public d:F


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 985781
    const-class v0, Lcom/facebook/photos/creativeediting/model/PersistableRect_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 985782
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 985783
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/creativeediting/model/PersistableRect;
    .locals 2

    .prologue
    .line 985784
    new-instance v0, Lcom/facebook/photos/creativeediting/model/PersistableRect;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;-><init>(Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;)V

    return-object v0
.end method

.method public setBottom(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bottom"
    .end annotation

    .prologue
    .line 985785
    iput p1, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->a:F

    .line 985786
    return-object p0
.end method

.method public setLeft(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "left"
    .end annotation

    .prologue
    .line 985787
    iput p1, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->b:F

    .line 985788
    return-object p0
.end method

.method public setRight(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "right"
    .end annotation

    .prologue
    .line 985789
    iput p1, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->c:F

    .line 985790
    return-object p0
.end method

.method public setTop(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "top"
    .end annotation

    .prologue
    .line 985791
    iput p1, p0, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->d:F

    .line 985792
    return-object p0
.end method
