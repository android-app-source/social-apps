.class public Lcom/facebook/photos/creativeediting/model/VideoTrimParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/VideoTrimParamsDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/VideoTrimParamsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/model/VideoTrimParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final isTrimSpecified:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isTrimSpecified"
    .end annotation
.end field

.field public final videoTrimEndTimeMs:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "videoTrimEndTimeMs"
    .end annotation
.end field

.field public final videoTrimStartTimeMs:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "videoTirmStartTimeMs"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 986609
    const-class v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParamsDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 986629
    const-class v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParamsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 986628
    new-instance v0, LX/5jO;

    invoke-direct {v0}, LX/5jO;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 986623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986624
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->isTrimSpecified:Z

    .line 986625
    iput v1, p0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimStartTimeMs:I

    .line 986626
    iput v1, p0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimEndTimeMs:I

    .line 986627
    return-void
.end method

.method public constructor <init>(LX/5jP;)V
    .locals 1

    .prologue
    .line 986618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986619
    iget-boolean v0, p1, LX/5jP;->a:Z

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->isTrimSpecified:Z

    .line 986620
    iget v0, p1, LX/5jP;->b:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimStartTimeMs:I

    .line 986621
    iget v0, p1, LX/5jP;->c:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimEndTimeMs:I

    .line 986622
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/VideoTrimParams;
    .locals 2

    .prologue
    .line 986617
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    const-class v1, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    invoke-virtual {v0, p0, v1}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 986616
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder()LX/5jP;
    .locals 2

    .prologue
    .line 986615
    new-instance v0, LX/5jP;

    invoke-direct {v0}, LX/5jP;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 986614
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 986610
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 986611
    :goto_0
    return-void

    .line 986612
    :catch_0
    move-exception v0

    .line 986613
    const-class v1, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    const-string v2, "Unable to serialize class to write to parcel"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
