.class public final Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1d327065
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 984686
    const-class v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 984687
    const-class v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 984688
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 984689
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 984726
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 984727
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 984728
    return-void
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;
    .locals 10

    .prologue
    .line 984690
    if-nez p0, :cond_0

    .line 984691
    const/4 p0, 0x0

    .line 984692
    :goto_0
    return-object p0

    .line 984693
    :cond_0
    instance-of v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    if-eqz v0, :cond_1

    .line 984694
    check-cast p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    goto :goto_0

    .line 984695
    :cond_1
    new-instance v0, LX/5ij;

    invoke-direct {v0}, LX/5ij;-><init>()V

    .line 984696
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5ij;->a:Ljava/lang/String;

    .line 984697
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5ij;->b:Ljava/lang/String;

    .line 984698
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5ij;->c:Ljava/lang/String;

    .line 984699
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 984700
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 984701
    iget-object v3, v0, LX/5ij;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 984702
    iget-object v5, v0, LX/5ij;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 984703
    iget-object v7, v0, LX/5ij;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 984704
    const/4 v8, 0x3

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 984705
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 984706
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 984707
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 984708
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 984709
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 984710
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 984711
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 984712
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 984713
    new-instance v3, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    invoke-direct {v3, v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;-><init>(LX/15i;)V

    .line 984714
    move-object p0, v3

    .line 984715
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 984716
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 984717
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 984718
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 984719
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 984720
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 984721
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 984722
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 984723
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 984724
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 984725
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 984683
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 984684
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 984685
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 984682
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 984671
    new-instance v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;-><init>()V

    .line 984672
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 984673
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 984680
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->e:Ljava/lang/String;

    .line 984681
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 984678
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->f:Ljava/lang/String;

    .line 984679
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 984676
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->g:Ljava/lang/String;

    .line 984677
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 984675
    const v0, 0x6f460238

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 984674
    const v0, 0x16bfdee3

    return v0
.end method
