.class public Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/CreativeEditingDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:F

.field public final b:Lcom/facebook/photos/creativeediting/model/PersistableRect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/DoodleParams;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;

.field public final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Z

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Z

.field public final l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/TextParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 982153
    const-class v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 982154
    const-class v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 982155
    new-instance v0, LX/5i9;

    invoke-direct {v0}, LX/5i9;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 982156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 982157
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a:F

    .line 982158
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 982159
    iput-object v5, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->b:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 982160
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 982161
    iput-object v5, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->c:Ljava/lang/String;

    .line 982162
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/photos/creativeediting/model/DoodleParams;

    move v1, v2

    .line 982163
    :goto_2
    array-length v0, v4

    if-ge v1, v0, :cond_2

    .line 982164
    sget-object v0, Lcom/facebook/photos/creativeediting/model/DoodleParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    .line 982165
    aput-object v0, v4, v1

    .line 982166
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 982167
    :cond_0
    sget-object v0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->b:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    goto :goto_0

    .line 982168
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->c:Ljava/lang/String;

    goto :goto_1

    .line 982169
    :cond_2
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->d:LX/0Px;

    .line 982170
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 982171
    iput-object v5, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->e:Ljava/lang/String;

    .line 982172
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->f:Ljava/lang/String;

    .line 982173
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/photos/creativeediting/model/StickerParams;

    move v1, v2

    .line 982174
    :goto_4
    array-length v0, v4

    if-ge v1, v0, :cond_4

    .line 982175
    sget-object v0, Lcom/facebook/photos/creativeediting/model/StickerParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 982176
    aput-object v0, v4, v1

    .line 982177
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 982178
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->e:Ljava/lang/String;

    goto :goto_3

    .line 982179
    :cond_4
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->g:LX/0Px;

    .line 982180
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move v1, v2

    .line 982181
    :goto_5
    array-length v0, v4

    if-ge v1, v0, :cond_5

    .line 982182
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 982183
    aput-object v0, v4, v1

    .line 982184
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 982185
    :cond_5
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->h:LX/0Px;

    .line 982186
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_6

    move v0, v3

    :goto_6
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->i:Z

    .line 982187
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    .line 982188
    iput-object v5, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->j:Ljava/lang/String;

    .line 982189
    :goto_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_8

    :goto_8
    iput-boolean v3, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->k:Z

    .line 982190
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [Lcom/facebook/photos/creativeediting/model/StickerParams;

    move v1, v2

    .line 982191
    :goto_9
    array-length v0, v3

    if-ge v1, v0, :cond_9

    .line 982192
    sget-object v0, Lcom/facebook/photos/creativeediting/model/StickerParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 982193
    aput-object v0, v3, v1

    .line 982194
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    :cond_6
    move v0, v2

    .line 982195
    goto :goto_6

    .line 982196
    :cond_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->j:Ljava/lang/String;

    goto :goto_7

    :cond_8
    move v3, v2

    .line 982197
    goto :goto_8

    .line 982198
    :cond_9
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->l:LX/0Px;

    .line 982199
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v1, v0, [Lcom/facebook/photos/creativeediting/model/TextParams;

    .line 982200
    :goto_a
    array-length v0, v1

    if-ge v2, v0, :cond_a

    .line 982201
    sget-object v0, Lcom/facebook/photos/creativeediting/model/TextParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/TextParams;

    .line 982202
    aput-object v0, v1, v2

    .line 982203
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    .line 982204
    :cond_a
    invoke-static {v1}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->m:LX/0Px;

    .line 982205
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;)V
    .locals 1

    .prologue
    .line 982091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 982092
    iget v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->b:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a:F

    .line 982093
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->c:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->b:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 982094
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->c:Ljava/lang/String;

    .line 982095
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->e:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->d:LX/0Px;

    .line 982096
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->e:Ljava/lang/String;

    .line 982097
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->f:Ljava/lang/String;

    .line 982098
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->h:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->g:LX/0Px;

    .line 982099
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->i:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->h:LX/0Px;

    .line 982100
    iget-boolean v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->i:Z

    .line 982101
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->j:Ljava/lang/String;

    .line 982102
    iget-boolean v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->l:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->k:Z

    .line 982103
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->m:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->l:LX/0Px;

    .line 982104
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->n:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->m:LX/0Px;

    .line 982105
    return-void
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 2

    .prologue
    .line 982206
    new-instance v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;-><init>(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 2

    .prologue
    .line 982207
    new-instance v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 982208
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 982209
    if-ne p0, p1, :cond_1

    .line 982210
    :cond_0
    :goto_0
    return v0

    .line 982211
    :cond_1
    instance-of v2, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    if-nez v2, :cond_2

    move v0, v1

    .line 982212
    goto :goto_0

    .line 982213
    :cond_2
    check-cast p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 982214
    iget v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a:F

    iget v3, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    move v0, v1

    .line 982215
    goto :goto_0

    .line 982216
    :cond_3
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->b:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->b:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 982217
    goto :goto_0

    .line 982218
    :cond_4
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 982219
    goto :goto_0

    .line 982220
    :cond_5
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->d:LX/0Px;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->d:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 982221
    goto :goto_0

    .line 982222
    :cond_6
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 982223
    goto :goto_0

    .line 982224
    :cond_7
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 982225
    goto :goto_0

    .line 982226
    :cond_8
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->g:LX/0Px;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->g:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 982227
    goto :goto_0

    .line 982228
    :cond_9
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->h:LX/0Px;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->h:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 982229
    goto :goto_0

    .line 982230
    :cond_a
    iget-boolean v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->i:Z

    iget-boolean v3, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->i:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 982231
    goto :goto_0

    .line 982232
    :cond_b
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->j:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 982233
    goto :goto_0

    .line 982234
    :cond_c
    iget-boolean v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->k:Z

    iget-boolean v3, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->k:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 982235
    goto/16 :goto_0

    .line 982236
    :cond_d
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->l:LX/0Px;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->l:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 982237
    goto/16 :goto_0

    .line 982238
    :cond_e
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->m:LX/0Px;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->m:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 982239
    goto/16 :goto_0
.end method

.method public getAspectRatio()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "aspect_ratio"
    .end annotation

    .prologue
    .line 982240
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a:F

    return v0
.end method

.method public getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "crop_box"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 982241
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->b:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    return-object v0
.end method

.method public getDisplayUri()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "display_uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 982152
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getDoodleParams()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_params"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/DoodleParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 982242
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->d:LX/0Px;

    return-object v0
.end method

.method public getEditedUri()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "edited_uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 982084
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getFilterName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "filter_name"
    .end annotation

    .prologue
    .line 982085
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getFrameOverlayItems()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "frame_overlay_items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 982086
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->g:LX/0Px;

    return-object v0
.end method

.method public getFramePacks()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "frame_packs"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 982087
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->h:LX/0Px;

    return-object v0
.end method

.method public getOriginalUri()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "original_uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 982088
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getStickerParams()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sticker_params"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 982089
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->l:LX/0Px;

    return-object v0
.end method

.method public getTextParams()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_params"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/TextParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 982090
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->m:LX/0Px;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 982106
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->b:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->d:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->g:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->h:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->l:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->m:LX/0Px;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isRotated()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_rotated"
    .end annotation

    .prologue
    .line 982107
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->i:Z

    return v0
.end method

.method public shouldPostProcess()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_post_process"
    .end annotation

    .prologue
    .line 982108
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->k:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 982109
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 982110
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->b:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    if-nez v0, :cond_0

    .line 982111
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 982112
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 982113
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 982114
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 982115
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->d:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    .line 982116
    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 982117
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 982118
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 982119
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->b:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 982120
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 982121
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 982122
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 982123
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 982124
    :goto_3
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 982125
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 982126
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_4
    if-ge v3, v4, :cond_4

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->g:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 982127
    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/model/StickerParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 982128
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 982129
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 982130
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3

    .line 982131
    :cond_4
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 982132
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_5
    if-ge v3, v4, :cond_5

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->h:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 982133
    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 982134
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 982135
    :cond_5
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->i:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 982136
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->j:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 982137
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 982138
    :goto_7
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->k:Z

    if-eqz v0, :cond_8

    :goto_8
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 982139
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 982140
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_9
    if-ge v1, v3, :cond_9

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->l:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 982141
    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/model/StickerParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 982142
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    :cond_6
    move v0, v2

    .line 982143
    goto :goto_6

    .line 982144
    :cond_7
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 982145
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_7

    :cond_8
    move v1, v2

    .line 982146
    goto :goto_8

    .line 982147
    :cond_9
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 982148
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    :goto_a
    if-ge v2, v1, :cond_a

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->m:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/TextParams;

    .line 982149
    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/model/TextParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 982150
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    .line 982151
    :cond_a
    return-void
.end method
