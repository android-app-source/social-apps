.class public final Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2b81edc6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 987550
    const-class v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 987549
    const-class v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 987525
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 987526
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 987541
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 987542
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 987543
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 987544
    iget v1, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 987545
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 987546
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 987547
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 987548
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 987538
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 987539
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 987540
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987536
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;->f:Ljava/lang/String;

    .line 987537
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 987532
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 987533
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;->e:I

    .line 987534
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;->g:I

    .line 987535
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 987529
    new-instance v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;-><init>()V

    .line 987530
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 987531
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 987528
    const v0, -0x587dee0d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 987527
    const v0, 0x437b93b

    return v0
.end method
