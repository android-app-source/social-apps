.class public final Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x50a312db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 988767
    const-class v0, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 988768
    const-class v0, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 988762
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 988763
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 988754
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 988755
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 988756
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 988757
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 988758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 988759
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 988764
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 988765
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 988766
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 988760
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel;->e:Ljava/lang/String;

    .line 988761
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 988751
    new-instance v0, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel;-><init>()V

    .line 988752
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 988753
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 988750
    const v0, 0xcece79

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 988749
    const v0, -0x726d476c

    return v0
.end method
