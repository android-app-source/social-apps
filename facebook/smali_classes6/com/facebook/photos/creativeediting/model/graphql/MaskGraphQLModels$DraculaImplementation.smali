.class public final Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 986699
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 986700
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 986738
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 986739
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 986718
    if-nez p1, :cond_0

    .line 986719
    :goto_0
    return v0

    .line 986720
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 986721
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 986722
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 986723
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 986724
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 986725
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 986726
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 986727
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 986728
    invoke-virtual {p3, v3, v2}, LX/186;->b(II)V

    .line 986729
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 986730
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 986731
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 986732
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 986733
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 986734
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 986735
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 986736
    invoke-virtual {p3, v3, v2}, LX/186;->b(II)V

    .line 986737
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2fba5941 -> :sswitch_1
        -0xb2d1599 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 986709
    if-nez p0, :cond_0

    move v0, v1

    .line 986710
    :goto_0
    return v0

    .line 986711
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 986712
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 986713
    :goto_1
    if-ge v1, v2, :cond_2

    .line 986714
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 986715
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 986716
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 986717
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 986702
    const/4 v7, 0x0

    .line 986703
    const/4 v1, 0x0

    .line 986704
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 986705
    invoke-static {v2, v3, v0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 986706
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 986707
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 986708
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 986701
    new-instance v0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 986667
    sparse-switch p0, :sswitch_data_0

    .line 986668
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 986669
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x2fba5941 -> :sswitch_0
        -0xb2d1599 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 986698
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 986696
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;->b(I)V

    .line 986697
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 986740
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 986741
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 986742
    :cond_0
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 986743
    iput p2, p0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;->b:I

    .line 986744
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 986695
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 986694
    new-instance v0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 986691
    iget v0, p0, LX/1vt;->c:I

    .line 986692
    move v0, v0

    .line 986693
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 986688
    iget v0, p0, LX/1vt;->c:I

    .line 986689
    move v0, v0

    .line 986690
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 986685
    iget v0, p0, LX/1vt;->b:I

    .line 986686
    move v0, v0

    .line 986687
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 986682
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 986683
    move-object v0, v0

    .line 986684
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 986673
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 986674
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 986675
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 986676
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 986677
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 986678
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 986679
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 986680
    invoke-static {v3, v9, v2}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 986681
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 986670
    iget v0, p0, LX/1vt;->c:I

    .line 986671
    move v0, v0

    .line 986672
    return v0
.end method
