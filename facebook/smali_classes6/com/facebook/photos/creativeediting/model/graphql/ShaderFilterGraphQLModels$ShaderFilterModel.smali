.class public final Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6c420a4e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 988639
    const-class v0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 988638
    const-class v0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 988636
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 988637
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 988626
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 988627
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 988628
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 988629
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 988630
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 988631
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 988632
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 988633
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 988634
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 988635
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 988623
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 988624
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 988625
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 988622
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 988611
    new-instance v0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;-><init>()V

    .line 988612
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 988613
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 988621
    const v0, 0x182f1fe0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 988620
    const v0, 0x7ddb43dd

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 988618
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->e:Ljava/lang/String;

    .line 988619
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 988616
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->f:Ljava/lang/String;

    .line 988617
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 988614
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->g:Ljava/lang/String;

    .line 988615
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->g:Ljava/lang/String;

    return-object v0
.end method
