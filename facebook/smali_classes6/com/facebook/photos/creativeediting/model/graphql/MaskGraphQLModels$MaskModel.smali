.class public final Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x556972d4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 986809
    const-class v0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 986808
    const-class v0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 986806
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 986807
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 986794
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 986795
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->j()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 986796
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 986797
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 986798
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x2fba5941

    invoke-static {v4, v3, v5}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 986799
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 986800
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 986801
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 986802
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 986803
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 986804
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 986805
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 986779
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 986780
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->j()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 986781
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->j()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 986782
    if-eqz v1, :cond_2

    .line 986783
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    .line 986784
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->e:LX/3Sb;

    move-object v1, v0

    .line 986785
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 986786
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2fba5941

    invoke-static {v2, v0, v3}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 986787
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 986788
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    .line 986789
    iput v3, v0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->h:I

    move-object v1, v0

    .line 986790
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 986791
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    .line 986792
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object p0, v1

    .line 986793
    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 986778
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 986810
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 986811
    const/4 v0, 0x3

    const v1, -0x2fba5941

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->h:I

    .line 986812
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 986775
    new-instance v0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;-><init>()V

    .line 986776
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 986777
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 986774
    const v0, 0x4a18312b    # 2493514.8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 986773
    const v0, 0x2fd4c3c3

    return v0
.end method

.method public final j()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFaceRecognitionModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 986771
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, -0xb2d1599

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->e:LX/3Sb;

    .line 986772
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 986769
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->f:Ljava/lang/String;

    .line 986770
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 986767
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->g:Ljava/lang/String;

    .line 986768
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPackagedFile"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 986765
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 986766
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
