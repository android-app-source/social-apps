.class public final Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x23548f55
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$Serializer;
.end annotation


# instance fields
.field private A:D

.field private B:D

.field private C:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:D

.field private E:D

.field private F:D

.field private G:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:D

.field private g:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:D

.field private m:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:D

.field private p:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:I

.field private r:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:I

.field private t:D

.field private u:I

.field private v:D

.field private w:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:D

.field private y:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 987321
    const-class v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 987302
    const-class v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 987303
    const/16 v0, 0x1d

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 987304
    return-void
.end method

.method private F()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987305
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->e:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->e:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;

    .line 987306
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->e:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;

    return-object v0
.end method

.method private G()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987307
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->g:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->g:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    .line 987308
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->g:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    return-object v0
.end method

.method private H()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987309
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->i:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->i:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987310
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->i:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    return-object v0
.end method

.method private I()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987311
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->j:Ljava/lang/String;

    .line 987312
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private J()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987313
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->k:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->k:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987314
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->k:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    return-object v0
.end method

.method private K()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987315
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->m:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->m:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987316
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->m:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    return-object v0
.end method

.method private L()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987317
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->n:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->n:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987318
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->n:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    return-object v0
.end method

.method private M()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987319
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->p:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->p:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987320
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->p:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    return-object v0
.end method

.method private N()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987322
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->r:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->r:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    .line 987323
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->r:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    return-object v0
.end method

.method private O()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987449
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->w:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->w:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987450
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->w:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    return-object v0
.end method

.method private P()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987324
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->y:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->y:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    .line 987325
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->y:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    return-object v0
.end method

.method private Q()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987447
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->C:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->C:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987448
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->C:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    return-object v0
.end method

.method private R()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987445
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->G:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->G:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;

    .line 987446
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->G:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final synthetic A()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987444
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->Q()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final B()D
    .locals 2

    .prologue
    .line 987442
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987443
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->D:D

    return-wide v0
.end method

.method public final C()D
    .locals 2

    .prologue
    .line 987440
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987441
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->E:D

    return-wide v0
.end method

.method public final D()D
    .locals 2

    .prologue
    const/4 v0, 0x3

    .line 987438
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987439
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->F:D

    return-wide v0
.end method

.method public final synthetic E()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987437
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->R()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 21

    .prologue
    .line 987390
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 987391
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->F()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 987392
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->G()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 987393
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->e()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 987394
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->H()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 987395
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->I()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 987396
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->J()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 987397
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->K()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 987398
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->L()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 987399
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->M()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 987400
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->N()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 987401
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->O()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 987402
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->P()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 987403
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->Q()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 987404
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->R()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 987405
    const/16 v3, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 987406
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 987407
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->f:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 987408
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 987409
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 987410
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 987411
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 987412
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 987413
    const/4 v3, 0x7

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->l:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 987414
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 987415
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 987416
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->o:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 987417
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 987418
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->q:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 987419
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 987420
    const/16 v2, 0xe

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->s:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 987421
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->t:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 987422
    const/16 v2, 0x10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->u:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 987423
    const/16 v3, 0x11

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->v:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 987424
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 987425
    const/16 v3, 0x13

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->x:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 987426
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 987427
    const/16 v2, 0x15

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->z:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 987428
    const/16 v3, 0x16

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->A:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 987429
    const/16 v3, 0x17

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->B:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 987430
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 987431
    const/16 v3, 0x19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->D:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 987432
    const/16 v3, 0x1a

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->E:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 987433
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->F:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 987434
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 987435
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 987436
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 987327
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 987328
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->F()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 987329
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->F()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;

    .line 987330
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->F()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 987331
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 987332
    iput-object v0, v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->e:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;

    .line 987333
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->G()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 987334
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->G()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    .line 987335
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->G()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 987336
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 987337
    iput-object v0, v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->g:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    .line 987338
    :cond_1
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->H()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 987339
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->H()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987340
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->H()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 987341
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 987342
    iput-object v0, v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->i:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987343
    :cond_2
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->J()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 987344
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->J()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987345
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->J()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 987346
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 987347
    iput-object v0, v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->k:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987348
    :cond_3
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->K()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 987349
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->K()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987350
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->K()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 987351
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 987352
    iput-object v0, v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->m:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987353
    :cond_4
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->L()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 987354
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->L()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987355
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->L()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 987356
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 987357
    iput-object v0, v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->n:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987358
    :cond_5
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->M()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 987359
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->M()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987360
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->M()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 987361
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 987362
    iput-object v0, v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->p:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987363
    :cond_6
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->N()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 987364
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->N()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    .line 987365
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->N()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 987366
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 987367
    iput-object v0, v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->r:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    .line 987368
    :cond_7
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->O()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 987369
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->O()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987370
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->O()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 987371
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 987372
    iput-object v0, v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->w:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987373
    :cond_8
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->P()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 987374
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->P()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    .line 987375
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->P()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 987376
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 987377
    iput-object v0, v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->y:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    .line 987378
    :cond_9
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->Q()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 987379
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->Q()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987380
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->Q()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 987381
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 987382
    iput-object v0, v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->C:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    .line 987383
    :cond_a
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->R()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 987384
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->R()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;

    .line 987385
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->R()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 987386
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 987387
    iput-object v0, v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->G:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;

    .line 987388
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 987389
    if-nez v1, :cond_c

    :goto_0
    return-object p0

    :cond_c
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987326
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->I()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 987284
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 987285
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->f:D

    .line 987286
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->l:D

    .line 987287
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->o:D

    .line 987288
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->q:I

    .line 987289
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->s:I

    .line 987290
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->t:D

    .line 987291
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->u:I

    .line 987292
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->v:D

    .line 987293
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->x:D

    .line 987294
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->z:I

    .line 987295
    const/16 v0, 0x16

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->A:D

    .line 987296
    const/16 v0, 0x17

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->B:D

    .line 987297
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->D:D

    .line 987298
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->E:D

    .line 987299
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->F:D

    .line 987300
    return-void
.end method

.method public final synthetic aR_()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987301
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->J()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aS_()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987263
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->H()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 987260
    new-instance v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;-><init>()V

    .line 987261
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 987262
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987259
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->F()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()D
    .locals 2

    .prologue
    .line 987257
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987258
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->f:D

    return-wide v0
.end method

.method public final synthetic d()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987256
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->G()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 987255
    const v0, 0x42fba878

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987246
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->h:Ljava/lang/String;

    .line 987247
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 987248
    const v0, 0x2afc3f97

    return v0
.end method

.method public final j()D
    .locals 2

    .prologue
    .line 987244
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987245
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->l:D

    return-wide v0
.end method

.method public final synthetic k()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987251
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->K()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic l()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987252
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->L()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final m()D
    .locals 2

    .prologue
    .line 987249
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987250
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->o:D

    return-wide v0
.end method

.method public final synthetic n()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987264
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->M()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final o()I
    .locals 2

    .prologue
    .line 987265
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987266
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->q:I

    return v0
.end method

.method public final synthetic p()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987283
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->N()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final q()I
    .locals 2

    .prologue
    .line 987267
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987268
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->s:I

    return v0
.end method

.method public final r()D
    .locals 2

    .prologue
    .line 987269
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987270
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->t:D

    return-wide v0
.end method

.method public final s()I
    .locals 2

    .prologue
    .line 987271
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987272
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->u:I

    return v0
.end method

.method public final t()D
    .locals 2

    .prologue
    .line 987273
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987274
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->v:D

    return-wide v0
.end method

.method public final synthetic u()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987275
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->O()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final v()D
    .locals 2

    .prologue
    .line 987276
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987277
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->x:D

    return-wide v0
.end method

.method public final synthetic w()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 987278
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->P()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final x()I
    .locals 2

    .prologue
    .line 987279
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987280
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->z:I

    return v0
.end method

.method public final y()D
    .locals 2

    .prologue
    .line 987281
    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987282
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->A:D

    return-wide v0
.end method

.method public final z()D
    .locals 2

    .prologue
    .line 987253
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 987254
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->B:D

    return-wide v0
.end method
