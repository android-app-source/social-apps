.class public Lcom/facebook/photos/creativeediting/model/StickerParamsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/creativeediting/model/StickerParams;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 986254
    const-class v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    new-instance v1, Lcom/facebook/photos/creativeediting/model/StickerParamsSerializer;

    invoke-direct {v1}, Lcom/facebook/photos/creativeediting/model/StickerParamsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 986255
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 986270
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/creativeediting/model/StickerParams;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 986264
    if-nez p0, :cond_0

    .line 986265
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 986266
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 986267
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/creativeediting/model/StickerParamsSerializer;->b(Lcom/facebook/photos/creativeediting/model/StickerParams;LX/0nX;LX/0my;)V

    .line 986268
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 986269
    return-void
.end method

.method private static b(Lcom/facebook/photos/creativeediting/model/StickerParams;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 986257
    const-string v0, "id"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 986258
    const-string v0, "uniqueId"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 986259
    const-string v0, "isFlipped"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 986260
    const-string v0, "isSelectable"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->k()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 986261
    const-string v0, "isFrameItem"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->j()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 986262
    const-string v0, "relative_image_overlay_params"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->o()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 986263
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 986256
    check-cast p1, Lcom/facebook/photos/creativeediting/model/StickerParams;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/creativeediting/model/StickerParamsSerializer;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;LX/0nX;LX/0my;)V

    return-void
.end method
