.class public Lcom/facebook/photos/creativeediting/model/DoodleParamsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/creativeediting/model/DoodleParams;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 982494
    const-class v0, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    new-instance v1, Lcom/facebook/photos/creativeediting/model/DoodleParamsSerializer;

    invoke-direct {v1}, Lcom/facebook/photos/creativeediting/model/DoodleParamsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 982495
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 982493
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/creativeediting/model/DoodleParams;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 982483
    if-nez p0, :cond_0

    .line 982484
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 982485
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 982486
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/creativeediting/model/DoodleParamsSerializer;->b(Lcom/facebook/photos/creativeediting/model/DoodleParams;LX/0nX;LX/0my;)V

    .line 982487
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 982488
    return-void
.end method

.method private static b(Lcom/facebook/photos/creativeediting/model/DoodleParams;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 982490
    const-string v0, "id"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 982491
    const-string v0, "relative_image_overlay_params"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->n()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 982492
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 982489
    check-cast p1, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/creativeediting/model/DoodleParamsSerializer;->a(Lcom/facebook/photos/creativeediting/model/DoodleParams;LX/0nX;LX/0my;)V

    return-void
.end method
