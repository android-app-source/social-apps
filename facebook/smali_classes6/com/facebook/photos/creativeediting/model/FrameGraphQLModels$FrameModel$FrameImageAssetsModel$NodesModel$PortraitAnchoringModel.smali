.class public final Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x75a7831a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:D

.field private g:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 983431
    const-class v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 983437
    const-class v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 983435
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 983436
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 983432
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 983433
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 983434
    return-void
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;
    .locals 4

    .prologue
    .line 983420
    if-nez p0, :cond_0

    .line 983421
    const/4 p0, 0x0

    .line 983422
    :goto_0
    return-object p0

    .line 983423
    :cond_0
    instance-of v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;

    if-eqz v0, :cond_1

    .line 983424
    check-cast p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;

    goto :goto_0

    .line 983425
    :cond_1
    new-instance v0, LX/5iX;

    invoke-direct {v0}, LX/5iX;-><init>()V

    .line 983426
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v1

    iput-object v1, v0, LX/5iX;->a:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 983427
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->b()D

    move-result-wide v2

    iput-wide v2, v0, LX/5iX;->b:D

    .line 983428
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->c()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v1

    iput-object v1, v0, LX/5iX;->c:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 983429
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->d()D

    move-result-wide v2

    iput-wide v2, v0, LX/5iX;->d:D

    .line 983430
    invoke-virtual {v0}, LX/5iX;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 983410
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 983411
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 983412
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->c()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 983413
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 983414
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 983415
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->f:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 983416
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 983417
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->h:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 983418
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 983419
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 983407
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 983408
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 983409
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 983438
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 983439
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->e:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 983403
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 983404
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->f:D

    .line 983405
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->h:D

    .line 983406
    return-void
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 983401
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 983402
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->f:D

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 983398
    new-instance v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;-><init>()V

    .line 983399
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 983400
    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 983392
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->g:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->g:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 983393
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->g:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    return-object v0
.end method

.method public final d()D
    .locals 2

    .prologue
    .line 983396
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 983397
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;->h:D

    return-wide v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 983395
    const v0, 0x61e777c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 983394
    const v0, -0x53f9e556

    return v0
.end method
