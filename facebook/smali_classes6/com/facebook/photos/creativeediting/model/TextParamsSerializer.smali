.class public Lcom/facebook/photos/creativeediting/model/TextParamsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/creativeediting/model/TextParams;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 986574
    const-class v0, Lcom/facebook/photos/creativeediting/model/TextParams;

    new-instance v1, Lcom/facebook/photos/creativeediting/model/TextParamsSerializer;

    invoke-direct {v1}, Lcom/facebook/photos/creativeediting/model/TextParamsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 986575
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 986590
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/creativeediting/model/TextParams;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 986584
    if-nez p0, :cond_0

    .line 986585
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 986586
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 986587
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/creativeediting/model/TextParamsSerializer;->b(Lcom/facebook/photos/creativeediting/model/TextParams;LX/0nX;LX/0my;)V

    .line 986588
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 986589
    return-void
.end method

.method private static b(Lcom/facebook/photos/creativeediting/model/TextParams;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 986577
    const-string v0, "id"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/TextParams;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 986578
    const-string v0, "text_string"

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textString:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 986579
    const-string v0, "text_color"

    iget v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textColor:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 986580
    const-string v0, "isSelectable"

    iget-boolean v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->isSelectable:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 986581
    const-string v0, "isFrameItem"

    iget-boolean v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->isFrameItem:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 986582
    const-string v0, "relative_image_overlay_params"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/TextParams;->p()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 986583
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 986576
    check-cast p1, Lcom/facebook/photos/creativeediting/model/TextParams;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/creativeediting/model/TextParamsSerializer;->a(Lcom/facebook/photos/creativeediting/model/TextParams;LX/0nX;LX/0my;)V

    return-void
.end method
