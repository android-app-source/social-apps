.class public final Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x727cb68d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel$Serializer;
.end annotation


# instance fields
.field private e:D

.field private f:D

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:D

.field private i:Z

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 982980
    const-class v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 982979
    const-class v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 982977
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 982978
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 982974
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 982975
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 982976
    return-void
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;
    .locals 12

    .prologue
    .line 982942
    if-nez p0, :cond_0

    .line 982943
    const/4 p0, 0x0

    .line 982944
    :goto_0
    return-object p0

    .line 982945
    :cond_0
    instance-of v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;

    if-eqz v0, :cond_1

    .line 982946
    check-cast p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;

    goto :goto_0

    .line 982947
    :cond_1
    new-instance v0, LX/5iQ;

    invoke-direct {v0}, LX/5iQ;-><init>()V

    .line 982948
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->b()D

    move-result-wide v2

    iput-wide v2, v0, LX/5iQ;->a:D

    .line 982949
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->c()D

    move-result-wide v2

    iput-wide v2, v0, LX/5iQ;->b:D

    .line 982950
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5iQ;->c:Ljava/lang/String;

    .line 982951
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->e()D

    move-result-wide v2

    iput-wide v2, v0, LX/5iQ;->d:D

    .line 982952
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->aY_()Z

    move-result v1

    iput-boolean v1, v0, LX/5iQ;->e:Z

    .line 982953
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->aZ_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5iQ;->f:Ljava/lang/String;

    .line 982954
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->j()D

    move-result-wide v2

    iput-wide v2, v0, LX/5iQ;->g:D

    .line 982955
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 982956
    iget-object v5, v0, LX/5iQ;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 982957
    iget-object v5, v0, LX/5iQ;->f:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 982958
    const/4 v5, 0x7

    invoke-virtual {v4, v5}, LX/186;->c(I)V

    .line 982959
    const/4 v5, 0x0

    iget-wide v6, v0, LX/5iQ;->a:D

    const-wide/16 v8, 0x0

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 982960
    const/4 v5, 0x1

    iget-wide v6, v0, LX/5iQ;->b:D

    const-wide/16 v8, 0x0

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 982961
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v10}, LX/186;->b(II)V

    .line 982962
    const/4 v5, 0x3

    iget-wide v6, v0, LX/5iQ;->d:D

    const-wide/16 v8, 0x0

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 982963
    const/4 v5, 0x4

    iget-boolean v6, v0, LX/5iQ;->e:Z

    invoke-virtual {v4, v5, v6}, LX/186;->a(IZ)V

    .line 982964
    const/4 v5, 0x5

    invoke-virtual {v4, v5, v11}, LX/186;->b(II)V

    .line 982965
    const/4 v5, 0x6

    iget-wide v6, v0, LX/5iQ;->g:D

    const-wide/16 v8, 0x0

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 982966
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 982967
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 982968
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 982969
    const/4 v4, 0x0

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 982970
    new-instance v4, LX/15i;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 982971
    new-instance v5, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;

    invoke-direct {v5, v4}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;-><init>(LX/15i;)V

    .line 982972
    move-object p0, v5

    .line 982973
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 982929
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 982930
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 982931
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->aZ_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 982932
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 982933
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->e:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 982934
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->f:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 982935
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 982936
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->h:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 982937
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 982938
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 982939
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->k:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 982940
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 982941
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 982926
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 982927
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 982928
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 982901
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->aZ_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 982919
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 982920
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->e:D

    .line 982921
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->f:D

    .line 982922
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->h:D

    .line 982923
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->i:Z

    .line 982924
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->k:D

    .line 982925
    return-void
.end method

.method public final aY_()Z
    .locals 2

    .prologue
    .line 982917
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 982918
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->i:Z

    return v0
.end method

.method public final aZ_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 982899
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->j:Ljava/lang/String;

    .line 982900
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final b()D
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 982902
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 982903
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->e:D

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 982904
    new-instance v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;-><init>()V

    .line 982905
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 982906
    return-object v0
.end method

.method public final c()D
    .locals 2

    .prologue
    .line 982907
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 982908
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->f:D

    return-wide v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 982909
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->g:Ljava/lang/String;

    .line 982910
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 982911
    const v0, -0x45f969d5

    return v0
.end method

.method public final e()D
    .locals 2

    .prologue
    .line 982912
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 982913
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->h:D

    return-wide v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 982914
    const v0, -0x7976cbd9

    return v0
.end method

.method public final j()D
    .locals 2

    .prologue
    .line 982915
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 982916
    iget-wide v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->k:D

    return-wide v0
.end method
