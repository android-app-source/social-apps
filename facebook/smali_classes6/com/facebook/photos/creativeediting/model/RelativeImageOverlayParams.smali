.class public Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParamsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:F

.field private final b:F

.field private final c:Ljava/lang/String;

.field private final d:F

.field private final e:F

.field private final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:F


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 985981
    const-class v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 985982
    const-class v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParamsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 985921
    new-instance v0, LX/5jB;

    invoke-direct {v0}, LX/5jB;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 985970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 985971
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->a:F

    .line 985972
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->b:F

    .line 985973
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->c:Ljava/lang/String;

    .line 985974
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->d:F

    .line 985975
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->e:F

    .line 985976
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 985977
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->f:Ljava/lang/String;

    .line 985978
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->g:F

    .line 985979
    return-void

    .line 985980
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;)V
    .locals 1

    .prologue
    .line 985961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 985962
    iget v0, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->c:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->a:F

    .line 985963
    iget v0, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->d:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->b:F

    .line 985964
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->c:Ljava/lang/String;

    .line 985965
    iget v0, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->f:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->d:F

    .line 985966
    iget v0, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->g:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->e:F

    .line 985967
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->f:Ljava/lang/String;

    .line 985968
    iget v0, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->i:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->g:F

    .line 985969
    return-void
.end method

.method public static newBuilder()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;
    .locals 2

    .prologue
    .line 985960
    new-instance v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 985959
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 985940
    if-ne p0, p1, :cond_1

    .line 985941
    :cond_0
    :goto_0
    return v0

    .line 985942
    :cond_1
    instance-of v2, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 985943
    goto :goto_0

    .line 985944
    :cond_2
    check-cast p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    .line 985945
    iget v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->a:F

    iget v3, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->a:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    move v0, v1

    .line 985946
    goto :goto_0

    .line 985947
    :cond_3
    iget v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->b:F

    iget v3, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->b:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_4

    move v0, v1

    .line 985948
    goto :goto_0

    .line 985949
    :cond_4
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 985950
    goto :goto_0

    .line 985951
    :cond_5
    iget v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->d:F

    iget v3, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->d:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_6

    move v0, v1

    .line 985952
    goto :goto_0

    .line 985953
    :cond_6
    iget v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->e:F

    iget v3, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->e:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_7

    move v0, v1

    .line 985954
    goto :goto_0

    .line 985955
    :cond_7
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 985956
    goto :goto_0

    .line 985957
    :cond_8
    iget v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->g:F

    iget v3, p1, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->g:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    move v0, v1

    .line 985958
    goto :goto_0
.end method

.method public getHeightPercentage()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "height_percentage"
    .end annotation

    .prologue
    .line 985983
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->a:F

    return v0
.end method

.method public getLeftPercentage()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "left_percentage"
    .end annotation

    .prologue
    .line 985939
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->b:F

    return v0
.end method

.method public getRotationDegree()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rotation_degree"
    .end annotation

    .prologue
    .line 985938
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->d:F

    return v0
.end method

.method public getTopPercentage()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "top_percentage"
    .end annotation

    .prologue
    .line 985937
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->e:F

    return v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 985936
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getWidthPercentage()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "width_percentage"
    .end annotation

    .prologue
    .line 985935
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->g:F

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 985934
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->a:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->d:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->g:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public renderKey()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "render_key"
    .end annotation

    .prologue
    .line 985933
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 985922
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 985923
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 985924
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 985925
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 985926
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->e:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 985927
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 985928
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 985929
    :goto_0
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->g:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 985930
    return-void

    .line 985931
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 985932
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
