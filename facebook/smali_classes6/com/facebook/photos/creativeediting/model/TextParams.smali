.class public Lcom/facebook/photos/creativeediting/model/TextParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/5i8;
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/TextParamsDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/TextParamsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/model/TextParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final isFrameItem:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isFrameItem"
    .end annotation
.end field

.field public final isSelectable:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isSelectable"
    .end annotation
.end field

.field private final overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "relative_image_overlay_params"
    .end annotation
.end field

.field public final textColor:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_color"
    .end annotation
.end field

.field public final textString:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_string"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 986475
    const-class v0, Lcom/facebook/photos/creativeediting/model/TextParamsDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 986476
    const-class v0, Lcom/facebook/photos/creativeediting/model/TextParamsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 986477
    new-instance v0, LX/5jM;

    invoke-direct {v0}, LX/5jM;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/model/TextParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 986478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986479
    iput-object v2, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->id:Ljava/lang/String;

    .line 986480
    iput-object v2, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textString:Ljava/lang/String;

    .line 986481
    iput v3, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textColor:I

    .line 986482
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->isSelectable:Z

    .line 986483
    iput-boolean v3, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->isFrameItem:Z

    .line 986484
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->newBuilder()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setLeftPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setTopPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setWidthPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setHeightPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setRotationDegree(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->a()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    .line 986485
    return-void
.end method

.method public constructor <init>(LX/5jN;)V
    .locals 2

    .prologue
    .line 986486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986487
    iget-object v0, p1, LX/5jN;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->id:Ljava/lang/String;

    .line 986488
    iget-object v0, p1, LX/5jN;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textString:Ljava/lang/String;

    .line 986489
    iget v0, p1, LX/5jN;->h:I

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textColor:I

    .line 986490
    iget-boolean v0, p1, LX/5jN;->j:Z

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->isSelectable:Z

    .line 986491
    iget-boolean v0, p1, LX/5jN;->k:Z

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->isFrameItem:Z

    .line 986492
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->newBuilder()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v1

    iget-object v0, p1, LX/5jN;->b:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5jN;->c:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setLeftPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5jN;->d:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setTopPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5jN;->e:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setWidthPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5jN;->f:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setHeightPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5jN;->g:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setRotationDegree(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->a()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    .line 986493
    return-void

    .line 986494
    :cond_0
    iget-object v0, p1, LX/5jN;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 986495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986496
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->id:Ljava/lang/String;

    .line 986497
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 986498
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textString:Ljava/lang/String;

    .line 986499
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v4

    .line 986500
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v5

    .line 986501
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v6

    .line 986502
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v7

    .line 986503
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v8

    .line 986504
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textColor:I

    .line 986505
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->isSelectable:Z

    .line 986506
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->isFrameItem:Z

    .line 986507
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->newBuilder()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setLeftPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setTopPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setWidthPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setHeightPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setRotationDegree(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->a()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    .line 986508
    return-void

    :cond_0
    move v0, v2

    .line 986509
    goto :goto_0

    :cond_1
    move v1, v2

    .line 986510
    goto :goto_1
.end method

.method private static a(FF)Z
    .locals 4
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 986511
    sub-float v0, p0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;Landroid/graphics/PointF;F)LX/362;
    .locals 3

    .prologue
    .line 986524
    new-instance v0, LX/5jN;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textString:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/TextParams;->d()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/5jN;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget v1, p1, Landroid/graphics/RectF;->left:F

    .line 986525
    iput v1, v0, LX/5jN;->c:F

    .line 986526
    move-object v0, v0

    .line 986527
    iget v1, p1, Landroid/graphics/RectF;->top:F

    .line 986528
    iput v1, v0, LX/5jN;->d:F

    .line 986529
    move-object v0, v0

    .line 986530
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    .line 986531
    iput v1, v0, LX/5jN;->e:F

    .line 986532
    move-object v0, v0

    .line 986533
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 986534
    iput v1, v0, LX/5jN;->f:F

    .line 986535
    move-object v0, v0

    .line 986536
    iput p3, v0, LX/5jN;->g:F

    .line 986537
    move-object v0, v0

    .line 986538
    iget v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textColor:I

    .line 986539
    iput v1, v0, LX/5jN;->h:I

    .line 986540
    move-object v0, v0

    .line 986541
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->id:Ljava/lang/String;

    .line 986542
    iput-object v1, v0, LX/5jN;->i:Ljava/lang/String;

    .line 986543
    move-object v0, v0

    .line 986544
    iget-boolean v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->isFrameItem:Z

    .line 986545
    iput-boolean v1, v0, LX/5jN;->k:Z

    .line 986546
    move-object v0, v0

    .line 986547
    invoke-virtual {v0}, LX/5jN;->a()Lcom/facebook/photos/creativeediting/model/TextParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 986512
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    .line 986513
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    .line 986514
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 986515
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 986516
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v2, v0

    add-int/2addr v3, v1

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4
.end method

.method public final a()Landroid/graphics/RectF;
    .locals 6

    .prologue
    .line 986517
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v3

    iget-object v4, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v4

    iget-object v5, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v5}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v5

    add-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public final b()Landroid/graphics/PointF;
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 986518
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v2

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v3

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 986519
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getRotationDegree()F

    move-result v0

    return v0
.end method

.method public final d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 986520
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 986521
    const/4 v0, 0x0

    .line 986522
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 986474
    const/4 v0, 0x0

    return v0
.end method

.method public final e()F
    .locals 1

    .prologue
    .line 986523
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 986434
    if-ne p1, p0, :cond_1

    .line 986435
    :cond_0
    :goto_0
    return v0

    .line 986436
    :cond_1
    instance-of v2, p1, Lcom/facebook/photos/creativeediting/model/TextParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 986437
    goto :goto_0

    .line 986438
    :cond_2
    check-cast p1, Lcom/facebook/photos/creativeediting/model/TextParams;

    .line 986439
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/TextParams;->n()F

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->n()F

    move-result v3

    invoke-static {v2, v3}, Lcom/facebook/photos/creativeediting/model/TextParams;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/TextParams;->o()F

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->o()F

    move-result v3

    invoke-static {v2, v3}, Lcom/facebook/photos/creativeediting/model/TextParams;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/TextParams;->e()F

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->e()F

    move-result v3

    invoke-static {v2, v3}, Lcom/facebook/photos/creativeediting/model/TextParams;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/TextParams;->f()F

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->f()F

    move-result v3

    invoke-static {v2, v3}, Lcom/facebook/photos/creativeediting/model/TextParams;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/TextParams;->c()F

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->c()F

    move-result v3

    invoke-static {v2, v3}, Lcom/facebook/photos/creativeediting/model/TextParams;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/TextParams;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textColor:I

    iget v3, p1, Lcom/facebook/photos/creativeediting/model/TextParams;->textColor:I

    if-ne v2, v3, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/TextParams;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/TextParams;->d()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->d()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 986440
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 986441
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 986442
    const/4 v0, 0x0

    return v0
.end method

.method public final hashCode()I
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 986443
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 986444
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 986445
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 986446
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 986447
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getRotationDegree()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 986448
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textString:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 986449
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 986450
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 986451
    :cond_0
    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 986452
    const/4 v0, 0x0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 986453
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->isFrameItem:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 986454
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->isSelectable:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 986455
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textString:Ljava/lang/String;

    return-object v0
.end method

.method public final m()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 986456
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textColor:I

    return v0
.end method

.method public final n()F
    .locals 1

    .prologue
    .line 986457
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v0

    return v0
.end method

.method public final o()F
    .locals 1

    .prologue
    .line 986458
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v0

    return v0
.end method

.method public final p()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;
    .locals 1

    .prologue
    .line 986459
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 986460
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 986461
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 986462
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 986463
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 986464
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 986465
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 986466
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 986467
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getRotationDegree()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 986468
    iget v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->textColor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 986469
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->isSelectable:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 986470
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;->isFrameItem:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 986471
    return-void

    :cond_0
    move v0, v2

    .line 986472
    goto :goto_0

    :cond_1
    move v1, v2

    .line 986473
    goto :goto_1
.end method
