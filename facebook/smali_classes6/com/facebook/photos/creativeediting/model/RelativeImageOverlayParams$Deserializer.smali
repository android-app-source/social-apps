.class public final Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 985920
    new-instance v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Deserializer;->a:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 985918
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 985919
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;
    .locals 1

    .prologue
    .line 985916
    sget-object v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Deserializer;->a:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    .line 985917
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->a()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 985915
    invoke-static {p1, p2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v0

    return-object v0
.end method
