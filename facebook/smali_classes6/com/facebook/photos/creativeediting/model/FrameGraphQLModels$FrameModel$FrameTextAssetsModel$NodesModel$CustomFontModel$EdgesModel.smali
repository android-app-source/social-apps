.class public final Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6d95f784
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 983732
    const-class v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 983733
    const-class v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 983734
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 983735
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 983736
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 983737
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 983738
    return-void
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;
    .locals 8

    .prologue
    .line 983739
    if-nez p0, :cond_0

    .line 983740
    const/4 p0, 0x0

    .line 983741
    :goto_0
    return-object p0

    .line 983742
    :cond_0
    instance-of v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;

    if-eqz v0, :cond_1

    .line 983743
    check-cast p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;

    goto :goto_0

    .line 983744
    :cond_1
    new-instance v0, LX/5ic;

    invoke-direct {v0}, LX/5ic;-><init>()V

    .line 983745
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    move-result-object v1

    iput-object v1, v0, LX/5ic;->a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    .line 983746
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 983747
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 983748
    iget-object v3, v0, LX/5ic;->a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 983749
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 983750
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 983751
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 983752
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 983753
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 983754
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 983755
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 983756
    new-instance v3, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;

    invoke-direct {v3, v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;-><init>(LX/15i;)V

    .line 983757
    move-object p0, v3

    .line 983758
    goto :goto_0
.end method

.method private j()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 983759
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;->e:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;->e:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    .line 983760
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;->e:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 983761
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 983762
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;->j()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 983763
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 983764
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 983765
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 983766
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 983767
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 983768
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;->j()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 983769
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;->j()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    .line 983770
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;->j()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 983771
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;

    .line 983772
    iput-object v0, v1, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;->e:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    .line 983773
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 983774
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 983775
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;->j()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 983776
    new-instance v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;-><init>()V

    .line 983777
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 983778
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 983779
    const v0, -0x6eb61149

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 983780
    const v0, -0x1bbf68c0

    return v0
.end method
