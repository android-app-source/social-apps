.class public Lcom/facebook/photos/creativeediting/model/StickerParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/5i8;
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/StickerParamsDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/model/StickerParamsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field private final isFlipped:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isFlipped"
    .end annotation
.end field

.field private final isFrameItem:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isFrameItem"
    .end annotation
.end field

.field private final isSelectable:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isSelectable"
    .end annotation
.end field

.field private final overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "relative_image_overlay_params"
    .end annotation
.end field

.field public final uniqueId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uniqueId"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 986224
    const-class v0, Lcom/facebook/photos/creativeediting/model/StickerParamsDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 986166
    const-class v0, Lcom/facebook/photos/creativeediting/model/StickerParamsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 986167
    new-instance v0, LX/5jF;

    invoke-direct {v0}, LX/5jF;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/model/StickerParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 986168
    new-instance v0, LX/5jG;

    invoke-direct {v0}, LX/5jG;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/photos/creativeediting/model/StickerParams;-><init>(LX/5jG;)V

    .line 986169
    return-void
.end method

.method public constructor <init>(LX/5jG;)V
    .locals 2

    .prologue
    .line 986170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986171
    iget-object v0, p1, LX/5jG;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->id:Ljava/lang/String;

    .line 986172
    iget-object v0, p1, LX/5jG;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->uniqueId:Ljava/lang/String;

    .line 986173
    iget-boolean v0, p1, LX/5jG;->i:Z

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isFlipped:Z

    .line 986174
    iget-boolean v0, p1, LX/5jG;->j:Z

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isSelectable:Z

    .line 986175
    iget-boolean v0, p1, LX/5jG;->k:Z

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isFrameItem:Z

    .line 986176
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->newBuilder()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v1

    iget-object v0, p1, LX/5jG;->a:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/5jG;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5jG;->e:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setLeftPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5jG;->f:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setTopPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5jG;->g:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setWidthPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5jG;->h:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setHeightPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    iget v1, p1, LX/5jG;->d:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setRotationDegree(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->a()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    .line 986177
    return-void

    .line 986178
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 986179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986180
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->id:Ljava/lang/String;

    .line 986181
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->uniqueId:Ljava/lang/String;

    .line 986182
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v3

    .line 986183
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 986184
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v5

    .line 986185
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v6

    .line 986186
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v7

    .line 986187
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v8

    .line 986188
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isFlipped:Z

    .line 986189
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isSelectable:Z

    .line 986190
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isFrameItem:Z

    .line 986191
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->newBuilder()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setLeftPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setTopPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setWidthPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setHeightPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setRotationDegree(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->a()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    .line 986192
    return-void

    :cond_0
    move v0, v2

    .line 986193
    goto :goto_0

    :cond_1
    move v0, v2

    .line 986194
    goto :goto_1

    :cond_2
    move v1, v2

    .line 986195
    goto :goto_2
.end method

.method private static a(FF)Z
    .locals 4
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 986196
    sub-float v0, p0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 986227
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;Landroid/graphics/PointF;F)LX/362;
    .locals 3

    .prologue
    .line 986197
    new-instance v0, LX/5jG;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->g()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/5jG;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    iget v1, p1, Landroid/graphics/RectF;->left:F

    .line 986198
    iput v1, v0, LX/5jG;->e:F

    .line 986199
    move-object v0, v0

    .line 986200
    iget v1, p1, Landroid/graphics/RectF;->top:F

    .line 986201
    iput v1, v0, LX/5jG;->f:F

    .line 986202
    move-object v0, v0

    .line 986203
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    .line 986204
    iput v1, v0, LX/5jG;->g:F

    .line 986205
    move-object v0, v0

    .line 986206
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 986207
    iput v1, v0, LX/5jG;->h:F

    .line 986208
    move-object v0, v0

    .line 986209
    iput p3, v0, LX/5jG;->d:F

    .line 986210
    move-object v0, v0

    .line 986211
    iget-boolean v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isFlipped:Z

    .line 986212
    iput-boolean v1, v0, LX/5jG;->i:Z

    .line 986213
    move-object v0, v0

    .line 986214
    iget-boolean v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isFrameItem:Z

    .line 986215
    iput-boolean v1, v0, LX/5jG;->k:Z

    .line 986216
    move-object v0, v0

    .line 986217
    invoke-virtual {v0}, LX/5jG;->a()Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 986218
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    .line 986219
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    .line 986220
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 986221
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 986222
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v2, v0

    add-int/2addr v3, v1

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4
.end method

.method public final a()Landroid/graphics/RectF;
    .locals 6

    .prologue
    .line 986223
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v3

    iget-object v4, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v4

    iget-object v5, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v5}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v5

    add-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/StickerParams;)Z
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 986161
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->m()F

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->m()F

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->n()F

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->n()F

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->e()F

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->e()F

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->f()F

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->f()F

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->c()F

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->c()F

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->p()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isFlipped:Z

    iget-boolean v1, p1, Lcom/facebook/photos/creativeediting/model/StickerParams;->isFlipped:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/graphics/PointF;
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 986225
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v2

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v3

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 986226
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getRotationDegree()F

    move-result v0

    return v0
.end method

.method public final d()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 986162
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 986163
    const/4 v0, 0x0

    .line 986164
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 986165
    const/4 v0, 0x0

    return v0
.end method

.method public final e()F
    .locals 1

    .prologue
    .line 986117
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 986118
    if-ne p1, p0, :cond_1

    .line 986119
    :cond_0
    :goto_0
    return v0

    .line 986120
    :cond_1
    instance-of v2, p1, Lcom/facebook/photos/creativeediting/model/StickerParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 986121
    goto :goto_0

    .line 986122
    :cond_2
    check-cast p1, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 986123
    invoke-virtual {p0, p1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->uniqueId:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/StickerParams;->uniqueId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 986124
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 986125
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 986126
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isFlipped:Z

    return v0
.end method

.method public final hashCode()I
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 986127
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 986128
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 986129
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 986130
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 986131
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getRotationDegree()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 986132
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 986133
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 986134
    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 986135
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->uniqueId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 986136
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isFlipped:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 986137
    return v0

    .line 986138
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 986139
    const/4 v0, 0x1

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 986140
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isFrameItem:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 986141
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isSelectable:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 986142
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->uniqueId:Ljava/lang/String;

    return-object v0
.end method

.method public final m()F
    .locals 1

    .prologue
    .line 986143
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v0

    return v0
.end method

.method public final n()F
    .locals 1

    .prologue
    .line 986144
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v0

    return v0
.end method

.method public final o()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;
    .locals 1

    .prologue
    .line 986145
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 986146
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 986147
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->uniqueId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 986148
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getRotationDegree()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 986149
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 986150
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 986151
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 986152
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 986153
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->overlayParams:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 986154
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isFlipped:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 986155
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isSelectable:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 986156
    iget-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;->isFrameItem:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 986157
    return-void

    :cond_0
    move v0, v2

    .line 986158
    goto :goto_0

    :cond_1
    move v0, v2

    .line 986159
    goto :goto_1

    :cond_2
    move v1, v2

    .line 986160
    goto :goto_2
.end method
