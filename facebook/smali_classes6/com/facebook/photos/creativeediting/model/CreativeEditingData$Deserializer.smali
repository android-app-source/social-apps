.class public final Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 982083
    new-instance v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Deserializer;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 982078
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 982079
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .locals 1

    .prologue
    .line 982081
    sget-object v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Deserializer;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    .line 982082
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 982080
    invoke-static {p1, p2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    return-object v0
.end method
