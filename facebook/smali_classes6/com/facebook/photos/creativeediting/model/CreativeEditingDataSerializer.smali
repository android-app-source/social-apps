.class public Lcom/facebook/photos/creativeediting/model/CreativeEditingDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 982243
    const-class v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    new-instance v1, Lcom/facebook/photos/creativeediting/model/CreativeEditingDataSerializer;

    invoke-direct {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 982244
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 982245
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 982246
    if-nez p0, :cond_0

    .line 982247
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 982248
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 982249
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingDataSerializer;->b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0nX;LX/0my;)V

    .line 982250
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 982251
    return-void
.end method

.method private static b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 982252
    const-string v0, "aspect_ratio"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getAspectRatio()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 982253
    const-string v0, "crop_box"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 982254
    const-string v0, "display_uri"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 982255
    const-string v0, "doodle_params"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDoodleParams()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 982256
    const-string v0, "edited_uri"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 982257
    const-string v0, "filter_name"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 982258
    const-string v0, "frame_overlay_items"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFrameOverlayItems()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 982259
    const-string v0, "frame_packs"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFramePacks()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 982260
    const-string v0, "is_rotated"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->isRotated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 982261
    const-string v0, "original_uri"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getOriginalUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 982262
    const-string v0, "should_post_process"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->shouldPostProcess()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 982263
    const-string v0, "sticker_params"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 982264
    const-string v0, "text_params"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 982265
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 982266
    check-cast p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingDataSerializer;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0nX;LX/0my;)V

    return-void
.end method
