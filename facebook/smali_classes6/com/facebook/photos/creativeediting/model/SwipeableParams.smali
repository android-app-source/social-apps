.class public Lcom/facebook/photos/creativeediting/model/SwipeableParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:LX/5jI;

.field public d:Ljava/lang/String;

.field private e:Lcom/facebook/videocodec/effects/model/ColorFilter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 986282
    new-instance v0, LX/5jH;

    invoke-direct {v0}, LX/5jH;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 986283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986284
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    .line 986285
    sget-object v0, Lcom/facebook/photos/creativeediting/model/StickerParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a:Ljava/util/List;

    .line 986286
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    .line 986287
    invoke-static {}, LX/5jI;->values()[LX/5jI;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    .line 986288
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->d:Ljava/lang/String;

    .line 986289
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;LX/5jI;Ljava/lang/String;Lcom/facebook/videocodec/effects/model/ColorFilter;Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;)V
    .locals 1
    .param p5    # Lcom/facebook/videocodec/effects/model/ColorFilter;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;",
            "Ljava/lang/String;",
            "LX/5jI;",
            "Ljava/lang/String;",
            "Lcom/facebook/videocodec/effects/model/ColorFilter;",
            "Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;",
            ")V"
        }
    .end annotation

    .prologue
    .line 986290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986291
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    .line 986292
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 986293
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 986294
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a:Ljava/util/List;

    .line 986295
    iput-object p2, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    .line 986296
    iput-object p3, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    .line 986297
    iput-object p4, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->d:Ljava/lang/String;

    .line 986298
    iput-object p5, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->e:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 986299
    iput-object p6, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->f:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    .line 986300
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 986301
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 986302
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 986303
    if-ne p1, p0, :cond_0

    move v0, v1

    .line 986304
    :goto_0
    return v0

    .line 986305
    :cond_0
    instance-of v0, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    if-nez v0, :cond_1

    move v0, v2

    .line 986306
    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 986307
    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 986308
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 986309
    iget-object v4, v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v4, v4

    .line 986310
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 986311
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object v3, v3

    .line 986312
    iget-object v4, v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object v0, v4

    .line 986313
    invoke-virtual {v3, v0}, LX/5jI;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a()LX/0Px;

    move-result-object v0

    check-cast p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a()LX/0Px;

    move-result-object v3

    const/4 p1, 0x1

    const/4 p0, 0x0

    .line 986314
    if-nez v0, :cond_4

    if-nez v3, :cond_4

    move p0, p1

    .line 986315
    :cond_2
    :goto_1
    move v0, p0

    .line 986316
    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    .line 986317
    :cond_4
    if-nez v0, :cond_5

    if-nez v3, :cond_2

    .line 986318
    :cond_5
    if-eqz v0, :cond_6

    if-eqz v3, :cond_2

    .line 986319
    :cond_6
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ne v4, v5, :cond_2

    move v6, p0

    .line 986320
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v6, v4, :cond_7

    .line 986321
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 986322
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 986323
    invoke-virtual {v4, v5}, Lcom/facebook/photos/creativeediting/model/StickerParams;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 986324
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_2

    :cond_7
    move p0, p1

    .line 986325
    goto :goto_1
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 986326
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 986327
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    invoke-virtual {v1}, LX/5jI;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 986328
    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 986329
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/facebook/photos/creativeediting/model/StickerParams;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 986330
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 986331
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    invoke-virtual {v0}, LX/5jI;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 986332
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 986333
    return-void
.end method
