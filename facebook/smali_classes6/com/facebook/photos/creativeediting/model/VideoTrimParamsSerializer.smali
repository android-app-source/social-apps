.class public Lcom/facebook/photos/creativeediting/model/VideoTrimParamsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/creativeediting/model/VideoTrimParams;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 986653
    const-class v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    new-instance v1, Lcom/facebook/photos/creativeediting/model/VideoTrimParamsSerializer;

    invoke-direct {v1}, Lcom/facebook/photos/creativeediting/model/VideoTrimParamsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 986654
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 986666
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/creativeediting/model/VideoTrimParams;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 986660
    if-nez p0, :cond_0

    .line 986661
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 986662
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 986663
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/creativeediting/model/VideoTrimParamsSerializer;->b(Lcom/facebook/photos/creativeediting/model/VideoTrimParams;LX/0nX;LX/0my;)V

    .line 986664
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 986665
    return-void
.end method

.method private static b(Lcom/facebook/photos/creativeediting/model/VideoTrimParams;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 986656
    const-string v0, "isTrimSpecified"

    iget-boolean v1, p0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->isTrimSpecified:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 986657
    const-string v0, "videoTirmStartTimeMs"

    iget v1, p0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimStartTimeMs:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 986658
    const-string v0, "videoTrimEndTimeMs"

    iget v1, p0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimEndTimeMs:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 986659
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 986655
    check-cast p1, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/creativeediting/model/VideoTrimParamsSerializer;->a(Lcom/facebook/photos/creativeediting/model/VideoTrimParams;LX/0nX;LX/0my;)V

    return-void
.end method
