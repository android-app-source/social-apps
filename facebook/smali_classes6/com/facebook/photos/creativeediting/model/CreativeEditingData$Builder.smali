.class public final Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/model/CreativeEditingData_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:F

.field public c:Lcom/facebook/photos/creativeediting/model/PersistableRect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/DoodleParams;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;",
            ">;"
        }
    .end annotation
.end field

.field public j:Z

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/TextParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 982006
    const-class v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 982003
    new-instance v0, LX/5iA;

    invoke-direct {v0}, LX/5iA;-><init>()V

    .line 982004
    sget-object v0, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v0}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 982005
    sput-object v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 982063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 982064
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 982065
    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->e:LX/0Px;

    .line 982066
    sget-object v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->g:Ljava/lang/String;

    .line 982067
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 982068
    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->h:LX/0Px;

    .line 982069
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 982070
    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->i:LX/0Px;

    .line 982071
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 982072
    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->m:LX/0Px;

    .line 982073
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 982074
    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->n:LX/0Px;

    .line 982075
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V
    .locals 1

    .prologue
    .line 982032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 982033
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 982034
    instance-of v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    if-eqz v0, :cond_0

    .line 982035
    check-cast p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 982036
    iget v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a:F

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->b:F

    .line 982037
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->b:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->c:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 982038
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->d:Ljava/lang/String;

    .line 982039
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->e:LX/0Px;

    .line 982040
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->f:Ljava/lang/String;

    .line 982041
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->g:Ljava/lang/String;

    .line 982042
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->g:LX/0Px;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->h:LX/0Px;

    .line 982043
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->h:LX/0Px;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->i:LX/0Px;

    .line 982044
    iget-boolean v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->i:Z

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->j:Z

    .line 982045
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->k:Ljava/lang/String;

    .line 982046
    iget-boolean v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->k:Z

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->l:Z

    .line 982047
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->l:LX/0Px;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->m:LX/0Px;

    .line 982048
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->m:LX/0Px;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->n:LX/0Px;

    .line 982049
    :goto_0
    return-void

    .line 982050
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getAspectRatio()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->b:F

    .line 982051
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->c:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 982052
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->d:Ljava/lang/String;

    .line 982053
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDoodleParams()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->e:LX/0Px;

    .line 982054
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->f:Ljava/lang/String;

    .line 982055
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->g:Ljava/lang/String;

    .line 982056
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFrameOverlayItems()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->h:LX/0Px;

    .line 982057
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFramePacks()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->i:LX/0Px;

    .line 982058
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->isRotated()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->j:Z

    .line 982059
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getOriginalUri()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->k:Ljava/lang/String;

    .line 982060
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->shouldPostProcess()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->l:Z

    .line 982061
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->m:LX/0Px;

    .line 982062
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->n:LX/0Px;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .locals 2

    .prologue
    .line 982031
    new-instance v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;-><init>(Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;)V

    return-object v0
.end method

.method public setAspectRatio(F)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "aspect_ratio"
    .end annotation

    .prologue
    .line 982029
    iput p1, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->b:F

    .line 982030
    return-object p0
.end method

.method public setCropBox(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/PersistableRect;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "crop_box"
    .end annotation

    .prologue
    .line 982027
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->c:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 982028
    return-object p0
.end method

.method public setDisplayUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "display_uri"
    .end annotation

    .prologue
    .line 982025
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->d:Ljava/lang/String;

    .line 982026
    return-object p0
.end method

.method public setDoodleParams(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_params"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/DoodleParams;",
            ">;)",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;"
        }
    .end annotation

    .prologue
    .line 982023
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->e:LX/0Px;

    .line 982024
    return-object p0
.end method

.method public setEditedUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "edited_uri"
    .end annotation

    .prologue
    .line 982076
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->f:Ljava/lang/String;

    .line 982077
    return-object p0
.end method

.method public setFilterName(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "filter_name"
    .end annotation

    .prologue
    .line 982021
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->g:Ljava/lang/String;

    .line 982022
    return-object p0
.end method

.method public setFrameOverlayItems(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "frame_overlay_items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;)",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;"
        }
    .end annotation

    .prologue
    .line 982019
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->h:LX/0Px;

    .line 982020
    return-object p0
.end method

.method public setFramePacks(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "frame_packs"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;",
            ">;)",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;"
        }
    .end annotation

    .prologue
    .line 982017
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->i:LX/0Px;

    .line 982018
    return-object p0
.end method

.method public setIsRotated(Z)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_rotated"
    .end annotation

    .prologue
    .line 982015
    iput-boolean p1, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->j:Z

    .line 982016
    return-object p0
.end method

.method public setOriginalUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "original_uri"
    .end annotation

    .prologue
    .line 982013
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->k:Ljava/lang/String;

    .line 982014
    return-object p0
.end method

.method public setShouldPostProcess(Z)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_post_process"
    .end annotation

    .prologue
    .line 982011
    iput-boolean p1, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->l:Z

    .line 982012
    return-object p0
.end method

.method public setStickerParams(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sticker_params"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;)",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;"
        }
    .end annotation

    .prologue
    .line 982009
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->m:LX/0Px;

    .line 982010
    return-object p0
.end method

.method public setTextParams(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_params"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/TextParams;",
            ">;)",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;"
        }
    .end annotation

    .prologue
    .line 982007
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->n:LX/0Px;

    .line 982008
    return-object p0
.end method
