.class public final Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 992323
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;

    new-instance v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 992324
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 992325
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 992326
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 992327
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 992328
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 992329
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 992330
    if-eqz v2, :cond_0

    .line 992331
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 992332
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 992333
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 992334
    if-eqz v2, :cond_1

    .line 992335
    const-string p0, "container_story"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 992336
    invoke-static {v1, v2, p1, p2}, LX/5kz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 992337
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 992338
    if-eqz v2, :cond_2

    .line 992339
    const-string p0, "owner_relation_to_user"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 992340
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 992341
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 992342
    if-eqz v2, :cond_3

    .line 992343
    const-string p0, "photo_upload_source"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 992344
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 992345
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 992346
    if-eqz v2, :cond_5

    .line 992347
    const-string p0, "privacy_scope"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 992348
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 992349
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 992350
    if-eqz p0, :cond_4

    .line 992351
    const-string v0, "label"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 992352
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 992353
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 992354
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 992355
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 992356
    check-cast p1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$Serializer;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;LX/0nX;LX/0my;)V

    return-void
.end method
