.class public final Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x618592f0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 995682
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 995693
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 995691
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 995692
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 995685
    iput-object p1, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->g:Ljava/lang/String;

    .line 995686
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 995687
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 995688
    if-eqz v0, :cond_0

    .line 995689
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 995690
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 995683
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->h:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->h:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    .line 995684
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->h:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 995670
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 995671
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 995672
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 995673
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 995674
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 995675
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 995676
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 995677
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 995678
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 995679
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 995680
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 995681
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 995662
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 995663
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 995664
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    .line 995665
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 995666
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    .line 995667
    iput-object v0, v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->h:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    .line 995668
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 995669
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 995661
    new-instance v0, LX/5kb;

    invoke-direct {v0, p1}, LX/5kb;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 995694
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 995655
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 995656
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 995657
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 995658
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 995659
    :goto_0
    return-void

    .line 995660
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 995652
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 995653
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->a(Ljava/lang/String;)V

    .line 995654
    :cond_0
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 995649
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 995650
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 995651
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 995646
    new-instance v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    invoke-direct {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;-><init>()V

    .line 995647
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 995648
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 995639
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->f:Ljava/lang/String;

    .line 995640
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 995644
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->g:Ljava/lang/String;

    .line 995645
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 995643
    const v0, -0x66470484

    return v0
.end method

.method public final synthetic e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 995642
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 995641
    const v0, 0x499e8e7

    return v0
.end method
