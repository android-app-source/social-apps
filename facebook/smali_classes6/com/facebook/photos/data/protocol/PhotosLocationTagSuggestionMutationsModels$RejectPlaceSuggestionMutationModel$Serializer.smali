.class public final Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$RejectPlaceSuggestionMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$RejectPlaceSuggestionMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 989499
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$RejectPlaceSuggestionMutationModel;

    new-instance v1, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$RejectPlaceSuggestionMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$RejectPlaceSuggestionMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 989500
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 989501
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$RejectPlaceSuggestionMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 989502
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 989503
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 989504
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 989505
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 989506
    if-eqz v2, :cond_0

    .line 989507
    const-string p0, "photo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 989508
    invoke-static {v1, v2, p1, p2}, LX/5k7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 989509
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 989510
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 989511
    check-cast p1, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$RejectPlaceSuggestionMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$RejectPlaceSuggestionMutationModel$Serializer;->a(Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$RejectPlaceSuggestionMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
