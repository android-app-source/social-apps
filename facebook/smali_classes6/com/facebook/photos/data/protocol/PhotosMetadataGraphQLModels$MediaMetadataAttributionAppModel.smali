.class public final Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x614e998
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 992560
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 992559
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 992557
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 992558
    return-void
.end method

.method private j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992555
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->g:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->g:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;

    .line 992556
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->g:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;

    return-object v0
.end method

.method private k()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992553
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->h:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->h:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;

    .line 992554
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->h:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 992541
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 992542
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 992543
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 992544
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 992545
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->k()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 992546
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 992547
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 992548
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 992549
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 992550
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 992551
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 992552
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 992528
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 992529
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 992530
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;

    .line 992531
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 992532
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    .line 992533
    iput-object v0, v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->g:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;

    .line 992534
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->k()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 992535
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->k()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;

    .line 992536
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->k()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 992537
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    .line 992538
    iput-object v0, v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->h:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;

    .line 992539
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 992540
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992561
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 992525
    new-instance v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    invoke-direct {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;-><init>()V

    .line 992526
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 992527
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992517
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->e:Ljava/lang/String;

    .line 992518
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992523
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->f:Ljava/lang/String;

    .line 992524
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992522
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$NativeStoreObjectModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 992521
    const v0, 0x59049ee9

    return v0
.end method

.method public final synthetic e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992520
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;->k()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel$SquareLogoModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 992519
    const v0, -0x3ff252d0

    return v0
.end method
