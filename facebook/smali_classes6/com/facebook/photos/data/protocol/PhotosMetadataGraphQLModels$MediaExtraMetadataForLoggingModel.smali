.class public final Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xfc38924
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 992390
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 992398
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 992396
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 992397
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992393
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 992394
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 992395
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992391
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;

    .line 992392
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992357
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->g:Ljava/lang/String;

    .line 992358
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992388
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->h:Ljava/lang/String;

    .line 992389
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPrivacyScope"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992386
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 992387
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 992399
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 992400
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 992401
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 992402
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 992403
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 992404
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->m()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, -0x1a5c850e

    invoke-static {v5, v4, v6}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 992405
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 992406
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 992407
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 992408
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 992409
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 992410
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 992411
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 992412
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 992371
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 992372
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 992373
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;

    .line 992374
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 992375
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;

    .line 992376
    iput-object v0, v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;

    .line 992377
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 992378
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x1a5c850e

    invoke-static {v2, v0, v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 992379
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 992380
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;

    .line 992381
    iput v3, v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->i:I

    move-object v1, v0

    .line 992382
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 992383
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 992384
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 992385
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 992370
    new-instance v0, LX/5kK;

    invoke-direct {v0, p1}, LX/5kK;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 992367
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 992368
    const/4 v0, 0x4

    const v1, -0x1a5c850e

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;->i:I

    .line 992369
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 992365
    invoke-virtual {p2}, LX/18L;->a()V

    .line 992366
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 992364
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 992361
    new-instance v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;

    invoke-direct {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;-><init>()V

    .line 992362
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 992363
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 992360
    const v0, 0x3e87034e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 992359
    const v0, 0x46c7fc4

    return v0
.end method
