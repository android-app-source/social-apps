.class public final Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x65493562
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 992136
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 992169
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 992167
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 992168
    return-void
.end method

.method private j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992165
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    .line 992166
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 992155
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 992156
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 992157
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 992158
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->c()Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 992159
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 992160
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 992161
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 992162
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 992163
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 992164
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 992147
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 992148
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 992149
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    .line 992150
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 992151
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    .line 992152
    iput-object v0, v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    .line 992153
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 992154
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992146
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 992143
    new-instance v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    invoke-direct {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;-><init>()V

    .line 992144
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 992145
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992141
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->f:Ljava/lang/String;

    .line 992142
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992139
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->g:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->g:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    .line 992140
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->g:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 992138
    const v0, -0x73461ad2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 992137
    const v0, 0x33319599

    return v0
.end method
