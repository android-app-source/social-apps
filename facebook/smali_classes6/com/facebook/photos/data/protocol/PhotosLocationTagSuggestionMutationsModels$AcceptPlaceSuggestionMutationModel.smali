.class public final Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4aef7a91
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$PlaceSuggestionMutationPhotoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 989346
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 989370
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 989368
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 989369
    return-void
.end method

.method private a()Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$PlaceSuggestionMutationPhotoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 989366
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel;->e:Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$PlaceSuggestionMutationPhotoFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$PlaceSuggestionMutationPhotoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$PlaceSuggestionMutationPhotoFieldsModel;

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel;->e:Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$PlaceSuggestionMutationPhotoFieldsModel;

    .line 989367
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel;->e:Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$PlaceSuggestionMutationPhotoFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 989360
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 989361
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel;->a()Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$PlaceSuggestionMutationPhotoFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 989362
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 989363
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 989364
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 989365
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 989352
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 989353
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel;->a()Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$PlaceSuggestionMutationPhotoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 989354
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel;->a()Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$PlaceSuggestionMutationPhotoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$PlaceSuggestionMutationPhotoFieldsModel;

    .line 989355
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel;->a()Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$PlaceSuggestionMutationPhotoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 989356
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel;

    .line 989357
    iput-object v0, v1, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel;->e:Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$PlaceSuggestionMutationPhotoFieldsModel;

    .line 989358
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 989359
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 989349
    new-instance v0, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel;

    invoke-direct {v0}, Lcom/facebook/photos/data/protocol/PhotosLocationTagSuggestionMutationsModels$AcceptPlaceSuggestionMutationModel;-><init>()V

    .line 989350
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 989351
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 989348
    const v0, -0x21dc58b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 989347
    const v0, 0x411019d5

    return v0
.end method
