.class public final Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 989119
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 989120
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 989117
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 989118
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 989104
    if-nez p1, :cond_0

    .line 989105
    :goto_0
    return v0

    .line 989106
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 989107
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 989108
    :pswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 989109
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 989110
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 989111
    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(III)I

    move-result v3

    .line 989112
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 989113
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 989114
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 989115
    invoke-virtual {p3, v6, v3, v0}, LX/186;->a(III)V

    .line 989116
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x514fab5b
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 989103
    new-instance v0, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 989100
    packed-switch p0, :pswitch_data_0

    .line 989101
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 989102
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x514fab5b
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 989099
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 989097
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;->b(I)V

    .line 989098
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 989092
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 989093
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 989094
    :cond_0
    iput-object p1, p0, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;->a:LX/15i;

    .line 989095
    iput p2, p0, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;->b:I

    .line 989096
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 989091
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 989066
    new-instance v0, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 989088
    iget v0, p0, LX/1vt;->c:I

    .line 989089
    move v0, v0

    .line 989090
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 989085
    iget v0, p0, LX/1vt;->c:I

    .line 989086
    move v0, v0

    .line 989087
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 989082
    iget v0, p0, LX/1vt;->b:I

    .line 989083
    move v0, v0

    .line 989084
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 989079
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 989080
    move-object v0, v0

    .line 989081
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 989070
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 989071
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 989072
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 989073
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 989074
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 989075
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 989076
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 989077
    invoke-static {v3, v9, v2}, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 989078
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 989067
    iget v0, p0, LX/1vt;->c:I

    .line 989068
    move v0, v0

    .line 989069
    return v0
.end method
