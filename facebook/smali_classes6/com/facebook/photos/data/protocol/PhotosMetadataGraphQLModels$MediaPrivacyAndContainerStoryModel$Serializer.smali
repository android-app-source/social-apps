.class public final Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 994661
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel;

    new-instance v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 994662
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 994663
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 994664
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 994665
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 994666
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 994667
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994668
    if-eqz v2, :cond_0

    .line 994669
    const-string p0, "container_story"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994670
    invoke-static {v1, v2, p1, p2}, LX/5lP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 994671
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994672
    if-eqz v2, :cond_1

    .line 994673
    const-string p0, "legacy_photo_container"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994674
    invoke-static {v1, v2, p1, p2}, LX/5lR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 994675
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994676
    if-eqz v2, :cond_2

    .line 994677
    const-string p0, "privacy_scope"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994678
    invoke-static {v1, v2, p1, p2}, LX/5lM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 994679
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 994680
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 994681
    check-cast p1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$Serializer;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel;LX/0nX;LX/0my;)V

    return-void
.end method
