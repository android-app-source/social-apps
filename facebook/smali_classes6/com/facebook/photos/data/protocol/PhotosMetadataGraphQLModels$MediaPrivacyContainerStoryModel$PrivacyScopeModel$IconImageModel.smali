.class public final Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x32384fc8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 994792
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 994791
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 994789
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 994790
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 994783
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 994784
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 994785
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 994786
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 994787
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 994788
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 994780
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 994781
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 994782
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 994773
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel;->e:Ljava/lang/String;

    .line 994774
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 994777
    new-instance v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel;

    invoke-direct {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel;-><init>()V

    .line 994778
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 994779
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 994776
    const v0, 0x1bdc2160

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 994775
    const v0, 0x437b93b

    return v0
.end method
