.class public final Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1ba8ca18
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 995430
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 995429
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 995427
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 995428
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 995425
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 995426
    iget v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 995415
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 995416
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 995417
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 995418
    iget v1, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->e:I

    invoke-virtual {p1, v3, v1, v3}, LX/186;->a(III)V

    .line 995419
    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->f:I

    invoke-virtual {p1, v1, v2, v3}, LX/186;->a(III)V

    .line 995420
    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->g:I

    invoke-virtual {p1, v1, v2, v3}, LX/186;->a(III)V

    .line 995421
    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->h:I

    invoke-virtual {p1, v1, v2, v3}, LX/186;->a(III)V

    .line 995422
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 995423
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 995424
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 995412
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 995413
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 995414
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 995406
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 995407
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->e:I

    .line 995408
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->f:I

    .line 995409
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->g:I

    .line 995410
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->h:I

    .line 995411
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 995404
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 995405
    iget v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 995393
    new-instance v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;

    invoke-direct {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;-><init>()V

    .line 995394
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 995395
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 995402
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 995403
    iget v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->g:I

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 995400
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 995401
    iget v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->h:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 995399
    const v0, -0x1cb65e8b

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 995397
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->i:Ljava/lang/String;

    .line 995398
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel$TilesModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 995396
    const v0, 0x44e6bbe0

    return v0
.end method
