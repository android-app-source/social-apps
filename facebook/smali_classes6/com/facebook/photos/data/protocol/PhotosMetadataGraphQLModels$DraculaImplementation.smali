.class public final Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 991619
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 991620
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 991652
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 991653
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 991633
    if-nez p1, :cond_0

    .line 991634
    :goto_0
    return v0

    .line 991635
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 991636
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 991637
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 991638
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 991639
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 991640
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 991641
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 991642
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 991643
    const v2, -0x11478f57

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 991644
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 991645
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 991646
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 991647
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 991648
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 991649
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 991650
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 991651
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x1a5c850e -> :sswitch_0
        -0x11478f57 -> :sswitch_2
        -0x7c69cb6 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 991632
    new-instance v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 991627
    sparse-switch p2, :sswitch_data_0

    .line 991628
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 991629
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 991630
    const v1, -0x11478f57

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 991631
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1a5c850e -> :sswitch_1
        -0x11478f57 -> :sswitch_1
        -0x7c69cb6 -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 991621
    if-eqz p1, :cond_0

    .line 991622
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 991623
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;

    .line 991624
    if-eq v0, v1, :cond_0

    .line 991625
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 991626
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 991618
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 991616
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 991617
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 991654
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 991655
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 991656
    :cond_0
    iput-object p1, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 991657
    iput p2, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;->b:I

    .line 991658
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 991615
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 991614
    new-instance v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 991590
    iget v0, p0, LX/1vt;->c:I

    .line 991591
    move v0, v0

    .line 991592
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 991611
    iget v0, p0, LX/1vt;->c:I

    .line 991612
    move v0, v0

    .line 991613
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 991608
    iget v0, p0, LX/1vt;->b:I

    .line 991609
    move v0, v0

    .line 991610
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 991605
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 991606
    move-object v0, v0

    .line 991607
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 991596
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 991597
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 991598
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 991599
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 991600
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 991601
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 991602
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 991603
    invoke-static {v3, v9, v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 991604
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 991593
    iget v0, p0, LX/1vt;->c:I

    .line 991594
    move v0, v0

    .line 991595
    return v0
.end method
