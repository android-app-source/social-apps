.class public final Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xd4f8072
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 991982
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 991981
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 991979
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 991980
    return-void
.end method

.method private j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 991977
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;

    .line 991978
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 991957
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 991958
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 991959
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 991960
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 991961
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 991962
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 991969
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 991970
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 991971
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;

    .line 991972
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 991973
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;

    .line 991974
    iput-object v0, v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;

    .line 991975
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 991976
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 991968
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 991965
    new-instance v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;

    invoke-direct {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$InlineActivityInfoModel$TaggableActivityIconModel;-><init>()V

    .line 991966
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 991967
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 991964
    const v0, -0x2198e521

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 991963
    const v0, 0x2615e4cf

    return v0
.end method
