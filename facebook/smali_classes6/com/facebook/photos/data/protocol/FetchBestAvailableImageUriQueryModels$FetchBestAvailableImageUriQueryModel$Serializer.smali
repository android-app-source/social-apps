.class public final Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 989176
    const-class v0, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;

    new-instance v1, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 989177
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 989175
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 989178
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 989179
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 989180
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 989181
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 989182
    if-eqz v2, :cond_0

    .line 989183
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 989184
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 989185
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 989186
    if-eqz v2, :cond_4

    .line 989187
    const-string p0, "image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 989188
    const/4 p2, 0x0

    .line 989189
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 989190
    invoke-virtual {v1, v2, p2, p2}, LX/15i;->a(III)I

    move-result p0

    .line 989191
    if-eqz p0, :cond_1

    .line 989192
    const-string v0, "height"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 989193
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 989194
    :cond_1
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 989195
    if-eqz p0, :cond_2

    .line 989196
    const-string v0, "uri"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 989197
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 989198
    :cond_2
    const/4 p0, 0x2

    invoke-virtual {v1, v2, p0, p2}, LX/15i;->a(III)I

    move-result p0

    .line 989199
    if-eqz p0, :cond_3

    .line 989200
    const-string v0, "width"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 989201
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 989202
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 989203
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 989204
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 989174
    check-cast p1, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel$Serializer;->a(Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
