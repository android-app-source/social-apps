.class public final Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x500f4797
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 992307
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 992270
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 992271
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 992272
    return-void
.end method

.method private a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 992273
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;

    .line 992274
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;

    return-object v0
.end method

.method private a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;)V
    .locals 3
    .param p1    # Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 992275
    iput-object p1, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;

    .line 992276
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 992277
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 992278
    if-eqz v0, :cond_0

    .line 992279
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 992280
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 992281
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 992282
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 992283
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 992284
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 992285
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 992286
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 992287
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 992288
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 992289
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;

    .line 992290
    invoke-direct {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 992291
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;

    .line 992292
    iput-object v0, v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;

    .line 992293
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 992294
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 992295
    new-instance v0, LX/5kJ;

    invoke-direct {v0, p1}, LX/5kJ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 992296
    invoke-virtual {p2}, LX/18L;->a()V

    .line 992297
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 992298
    const-string v0, "privacy_scope"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 992299
    check-cast p2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;

    invoke-direct {p0, p2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel$PrivacyScopeModel;)V

    .line 992300
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 992301
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 992302
    new-instance v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;

    invoke-direct {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel$ContainerStoryModel;-><init>()V

    .line 992303
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 992304
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 992305
    const v0, 0x2b41936

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 992306
    const v0, 0x4c808d5

    return v0
.end method
