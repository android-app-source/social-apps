.class public final Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 996209
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 996208
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 996206
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 996207
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 996200
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 996201
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 996202
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 996203
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 996204
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 996205
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 996197
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 996198
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 996199
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 996196
    new-instance v0, LX/5kn;

    invoke-direct {v0, p1}, LX/5kn;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 996210
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 996194
    invoke-virtual {p2}, LX/18L;->a()V

    .line 996195
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 996193
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 996190
    new-instance v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;

    invoke-direct {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;-><init>()V

    .line 996191
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 996192
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 996188
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;->e:Ljava/lang/String;

    .line 996189
    iget-object v0, p0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 996187
    const v0, 0x33e6ae81

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 996186
    const v0, 0x25d6af

    return v0
.end method
