.class public final Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 993978
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel;

    new-instance v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 993979
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 994146
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 993981
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 993982
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    .line 993983
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 993984
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 993985
    if-eqz v2, :cond_0

    .line 993986
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 993987
    invoke-static {v1, v0, v5, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 993988
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 993989
    if-eqz v2, :cond_1

    .line 993990
    const-string v3, "album"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 993991
    invoke-static {v1, v2, p1}, LX/5lF;->a(LX/15i;ILX/0nX;)V

    .line 993992
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 993993
    if-eqz v2, :cond_2

    .line 993994
    const-string v3, "attribution_app"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 993995
    invoke-static {v1, v2, p1, p2}, LX/5l3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 993996
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 993997
    if-eqz v2, :cond_3

    .line 993998
    const-string v3, "attribution_app_metadata"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 993999
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 994000
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994001
    if-eqz v2, :cond_4

    .line 994002
    const-string v3, "can_viewer_add_tags"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994003
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994004
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994005
    if-eqz v2, :cond_5

    .line 994006
    const-string v3, "can_viewer_delete"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994007
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994008
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994009
    if-eqz v2, :cond_6

    .line 994010
    const-string v3, "can_viewer_download"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994011
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994012
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994013
    if-eqz v2, :cond_7

    .line 994014
    const-string v3, "can_viewer_edit"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994015
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994016
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994017
    if-eqz v2, :cond_8

    .line 994018
    const-string v3, "can_viewer_export"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994019
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994020
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994021
    if-eqz v2, :cond_9

    .line 994022
    const-string v3, "can_viewer_make_cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994023
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994024
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994025
    if-eqz v2, :cond_a

    .line 994026
    const-string v3, "can_viewer_make_profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994027
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994028
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994029
    if-eqz v2, :cond_b

    .line 994030
    const-string v3, "can_viewer_report"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994031
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994032
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994033
    if-eqz v2, :cond_c

    .line 994034
    const-string v3, "can_viewer_share"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994035
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994036
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994037
    if-eqz v2, :cond_d

    .line 994038
    const-string v3, "can_viewer_share_externally"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994039
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994040
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994041
    if-eqz v2, :cond_e

    .line 994042
    const-string v3, "can_viewer_suggest_location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994043
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994044
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994045
    if-eqz v2, :cond_f

    .line 994046
    const-string v3, "can_viewer_untag"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994047
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994048
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 994049
    cmp-long v4, v2, v6

    if-eqz v4, :cond_10

    .line 994050
    const-string v4, "created_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994051
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 994052
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994053
    if-eqz v2, :cond_11

    .line 994054
    const-string v3, "creation_story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994055
    invoke-static {v1, v2, p1, p2}, LX/5l9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 994056
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994057
    if-eqz v2, :cond_12

    .line 994058
    const-string v3, "explicit_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994059
    invoke-static {v1, v2, p1}, LX/5lG;->a(LX/15i;ILX/0nX;)V

    .line 994060
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994061
    if-eqz v2, :cond_13

    .line 994062
    const-string v3, "face_boxes"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994063
    invoke-static {v1, v2, p1, p2}, LX/5lY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 994064
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994065
    if-eqz v2, :cond_14

    .line 994066
    const-string v3, "focus"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994067
    invoke-static {v1, v2, p1}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 994068
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994069
    if-eqz v2, :cond_15

    .line 994070
    const-string v3, "has_stickers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994071
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994072
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 994073
    if-eqz v2, :cond_16

    .line 994074
    const-string v3, "hd_playable_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994075
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 994076
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 994077
    if-eqz v2, :cond_17

    .line 994078
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994079
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 994080
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994081
    if-eqz v2, :cond_18

    .line 994082
    const-string v3, "image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994083
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 994084
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994085
    if-eqz v2, :cond_19

    .line 994086
    const-string v3, "imageHigh"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994087
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 994088
    :cond_19
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994089
    if-eqz v2, :cond_1a

    .line 994090
    const-string v3, "imageLow"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994091
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 994092
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994093
    if-eqz v2, :cond_1b

    .line 994094
    const-string v3, "imageMedium"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994095
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 994096
    :cond_1b
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994097
    if-eqz v2, :cond_1c

    .line 994098
    const-string v3, "imageThumbnail"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994099
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 994100
    :cond_1c
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994101
    if-eqz v2, :cond_1d

    .line 994102
    const-string v3, "inline_activities"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994103
    invoke-static {v1, v2, p1, p2}, LX/5lA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 994104
    :cond_1d
    const/16 v2, 0x1e

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 994105
    if-eqz v2, :cond_1e

    .line 994106
    const-string v3, "is_playable"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994107
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 994108
    :cond_1e
    const/16 v2, 0x1f

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994109
    if-eqz v2, :cond_1f

    .line 994110
    const-string v3, "largeThumbnail"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994111
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 994112
    :cond_1f
    const/16 v2, 0x20

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994113
    if-eqz v2, :cond_20

    .line 994114
    const-string v3, "message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994115
    invoke-static {v1, v2, p1, p2}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 994116
    :cond_20
    const/16 v2, 0x21

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994117
    if-eqz v2, :cond_21

    .line 994118
    const-string v3, "owner"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994119
    invoke-static {v1, v2, p1}, LX/5lB;->a(LX/15i;ILX/0nX;)V

    .line 994120
    :cond_21
    const/16 v2, 0x22

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994121
    if-eqz v2, :cond_22

    .line 994122
    const-string v3, "pending_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994123
    invoke-static {v1, v2, p1}, LX/5lH;->a(LX/15i;ILX/0nX;)V

    .line 994124
    :cond_22
    const/16 v2, 0x23

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 994125
    if-eqz v2, :cond_23

    .line 994126
    const-string v3, "playable_duration_in_ms"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994127
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 994128
    :cond_23
    const/16 v2, 0x24

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 994129
    if-eqz v2, :cond_24

    .line 994130
    const-string v3, "playable_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994131
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 994132
    :cond_24
    const/16 v2, 0x25

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994133
    if-eqz v2, :cond_25

    .line 994134
    const-string v3, "profile_picture_overlay"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994135
    invoke-static {v1, v2, p1, p2}, LX/5Qb;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 994136
    :cond_25
    const/16 v2, 0x26

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994137
    if-eqz v2, :cond_26

    .line 994138
    const-string v3, "tags"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994139
    invoke-static {v1, v2, p1, p2}, LX/5lj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 994140
    :cond_26
    const/16 v2, 0x27

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 994141
    if-eqz v2, :cond_27

    .line 994142
    const-string v3, "with_tags"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 994143
    invoke-static {v1, v2, p1, p2}, LX/5lJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 994144
    :cond_27
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 994145
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 993980
    check-cast p1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$Serializer;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel;LX/0nX;LX/0my;)V

    return-void
.end method
