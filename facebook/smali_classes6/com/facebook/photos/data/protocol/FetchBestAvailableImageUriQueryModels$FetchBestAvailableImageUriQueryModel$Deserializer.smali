.class public final Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 989121
    const-class v0, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;

    new-instance v1, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 989122
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 989123
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 989124
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 989125
    const/4 v2, 0x0

    .line 989126
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 989127
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 989128
    :goto_0
    move v1, v2

    .line 989129
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 989130
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 989131
    new-instance v1, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;

    invoke-direct {v1}, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;-><init>()V

    .line 989132
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 989133
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 989134
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 989135
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 989136
    :cond_0
    return-object v1

    .line 989137
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 989138
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 989139
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 989140
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 989141
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 989142
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 989143
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 989144
    :cond_4
    const-string v5, "image"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 989145
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 989146
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_e

    .line 989147
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 989148
    :goto_2
    move v1, v4

    .line 989149
    goto :goto_1

    .line 989150
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 989151
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 989152
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 989153
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 989154
    :cond_7
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_b

    .line 989155
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 989156
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 989157
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_7

    if-eqz v10, :cond_7

    .line 989158
    const-string p0, "height"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 989159
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v6

    move v9, v6

    move v6, v5

    goto :goto_3

    .line 989160
    :cond_8
    const-string p0, "uri"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 989161
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_3

    .line 989162
    :cond_9
    const-string p0, "width"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 989163
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v7, v1

    move v1, v5

    goto :goto_3

    .line 989164
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 989165
    :cond_b
    const/4 v10, 0x3

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 989166
    if-eqz v6, :cond_c

    .line 989167
    invoke-virtual {v0, v4, v9, v4}, LX/186;->a(III)V

    .line 989168
    :cond_c
    invoke-virtual {v0, v5, v8}, LX/186;->b(II)V

    .line 989169
    if-eqz v1, :cond_d

    .line 989170
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v7, v4}, LX/186;->a(III)V

    .line 989171
    :cond_d
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_e
    move v1, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    goto :goto_3
.end method
