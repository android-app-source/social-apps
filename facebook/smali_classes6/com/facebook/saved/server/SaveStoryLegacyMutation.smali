.class public Lcom/facebook/saved/server/SaveStoryLegacyMutation;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/4V2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/4V2",
        "<",
        "Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0aG;

.field private final b:LX/0SG;

.field private final c:Lcom/facebook/graphql/model/GraphQLNode;

.field private final d:Ljava/lang/String;

.field private final e:Z

.field private final f:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field

.field private final g:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0aG;LX/0SG;Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3    # Lcom/facebook/graphql/model/GraphQLNode;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1020055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1020056
    iput-object p1, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->a:LX/0aG;

    .line 1020057
    iput-object p2, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->b:LX/0SG;

    .line 1020058
    iput-object p3, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->c:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1020059
    iput-object p4, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->d:Ljava/lang/String;

    .line 1020060
    iput-boolean p5, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->e:Z

    .line 1020061
    iput-object p6, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->f:Ljava/lang/String;

    .line 1020062
    iput-object p7, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->g:Ljava/lang/String;

    .line 1020063
    return-void
.end method

.method private d()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;
    .locals 2

    .prologue
    .line 1020047
    new-instance v0, LX/40S;

    invoke-direct {v0}, LX/40S;-><init>()V

    iget-object v1, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->c:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    .line 1020048
    iput-object v1, v0, LX/40S;->b:Ljava/lang/String;

    .line 1020049
    move-object v1, v0

    .line 1020050
    iget-boolean v0, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->e:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1020051
    :goto_0
    iput-object v0, v1, LX/40S;->d:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1020052
    move-object v0, v1

    .line 1020053
    invoke-virtual {v0}, LX/40S;->a()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1020064
    const-string v0, "LegacyLinkSetItemSave"

    return-object v0
.end method

.method public final synthetic b()LX/0jT;
    .locals 1

    .prologue
    .line 1020054
    invoke-direct {p0}, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->d()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1020036
    new-instance v1, LX/5un;

    iget-boolean v0, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->e:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/5uo;->SAVE:LX/5uo;

    :goto_0
    iget-object v2, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->g:Ljava/lang/String;

    .line 1020037
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 1020038
    invoke-direct {v1, v0, v2, v3, v4}, LX/5un;-><init>(LX/5uo;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    iget-object v0, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1020039
    iput-object v0, v1, LX/5un;->c:LX/0am;

    .line 1020040
    move-object v0, v1

    .line 1020041
    invoke-virtual {v0}, LX/5un;->a()Lcom/facebook/saved/server/UpdateSavedStateParams;

    move-result-object v0

    .line 1020042
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1020043
    const-string v1, "update_saved_state_params"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1020044
    iget-object v0, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->a:LX/0aG;

    const-string v1, "update_saved_state"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v4, Lcom/facebook/saved/server/SaveStoryLegacyMutation;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x21f4d473

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 1020045
    new-instance v0, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0}, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->d()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    move-result-object v1

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v3, p0, Lcom/facebook/saved/server/SaveStoryLegacyMutation;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;J)V

    return-object v0

    .line 1020046
    :cond_0
    sget-object v0, LX/5uo;->UNSAVE:LX/5uo;

    goto :goto_0
.end method
