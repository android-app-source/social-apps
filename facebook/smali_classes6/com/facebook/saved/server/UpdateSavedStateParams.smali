.class public Lcom/facebook/saved/server/UpdateSavedStateParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/saved/server/UpdateSavedStateParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/5uo;

.field public final g:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation
.end field

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1020198
    new-instance v0, LX/5um;

    invoke-direct {v0}, LX/5um;-><init>()V

    sput-object v0, Lcom/facebook/saved/server/UpdateSavedStateParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5un;)V
    .locals 1

    .prologue
    .line 1020176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1020177
    iget-object v0, p1, LX/5un;->a:LX/0am;

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->b:LX/0am;

    .line 1020178
    iget-object v0, p1, LX/5un;->d:LX/0am;

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->c:LX/0am;

    .line 1020179
    iget-object v0, p1, LX/5un;->c:LX/0am;

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->d:LX/0am;

    .line 1020180
    iget-object v0, p1, LX/5un;->f:LX/5uo;

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->f:LX/5uo;

    .line 1020181
    iget-object v0, p1, LX/5un;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->g:Ljava/lang/String;

    .line 1020182
    iget-object v0, p1, LX/5un;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->h:Ljava/lang/String;

    .line 1020183
    iget-object v0, p1, LX/5un;->i:LX/0Px;

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->i:LX/0Px;

    .line 1020184
    iget-object v0, p1, LX/5un;->e:LX/0am;

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->e:LX/0am;

    .line 1020185
    iget-object v0, p1, LX/5un;->b:LX/0am;

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->a:LX/0am;

    .line 1020186
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1020187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1020188
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->b:LX/0am;

    .line 1020189
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->c:LX/0am;

    .line 1020190
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->d:LX/0am;

    .line 1020191
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->e:LX/0am;

    .line 1020192
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5uo;

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->f:LX/5uo;

    .line 1020193
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->g:Ljava/lang/String;

    .line 1020194
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->h:Ljava/lang/String;

    .line 1020195
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->i:LX/0Px;

    .line 1020196
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->a:LX/0am;

    .line 1020197
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1020175
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1020165
    iget-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1020166
    iget-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1020167
    iget-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1020168
    iget-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1020169
    iget-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->f:LX/5uo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1020170
    iget-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1020171
    iget-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1020172
    iget-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->i:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1020173
    iget-object v0, p0, Lcom/facebook/saved/server/UpdateSavedStateParams;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1020174
    return-void
.end method
