.class public final Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Ljava/util/Map;

.field public final synthetic c:LX/2nC;


# direct methods
.method public constructor <init>(LX/2nC;ZLjava/util/Map;)V
    .locals 0

    .prologue
    .line 1063440
    iput-object p1, p0, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;->c:LX/2nC;

    iput-boolean p2, p0, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;->a:Z

    iput-object p3, p0, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;->b:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1063414
    iget-object v0, p0, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;->c:LX/2nC;

    .line 1063415
    iget-object v1, v0, LX/2nC;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2nC;->a:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    move v0, v1

    .line 1063416
    if-eqz v0, :cond_0

    .line 1063417
    iget-object v0, p0, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;->c:LX/2nC;

    .line 1063418
    invoke-static {v0}, LX/2nC;->e(LX/2nC;)Ljava/util/Map;

    move-result-object v1

    .line 1063419
    if-nez v1, :cond_2

    .line 1063420
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;->a:Z

    if-eqz v0, :cond_1

    .line 1063421
    iget-object v0, p0, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;->c:LX/2nC;

    iget-object v0, v0, LX/2nC;->c:LX/0Zb;

    iget-object v1, p0, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;->c:LX/2nC;

    iget-object v1, v1, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1063422
    iget-object v0, p0, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;->c:LX/2nC;

    const/4 v1, 0x0

    .line 1063423
    iput-object v1, v0, LX/2nC;->i:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063424
    :goto_1
    return-void

    .line 1063425
    :cond_1
    iget-object v0, p0, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;->c:LX/2nC;

    iget-object v1, p0, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;->b:Ljava/util/Map;

    invoke-static {v0, v1}, LX/2nC;->a$redex0(LX/2nC;Ljava/util/Map;)V

    .line 1063426
    iget-object v0, p0, Lcom/facebook/browser/liteclient/logging/BrowserOpenUrlLogger$1;->c:LX/2nC;

    .line 1063427
    iget-object v1, v0, LX/2nC;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2nC;->a:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1063428
    goto :goto_1

    .line 1063429
    :cond_2
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "fb4a_iab_open_url"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1063430
    invoke-virtual {v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063431
    new-instance v1, Ljava/io/File;

    iget-object v3, v0, LX/2nC;->k:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "browser_ipc_failed"

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1063432
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    .line 1063433
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1063434
    iget-object v1, v0, LX/2nC;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v4, "android_browser_ipc_marker_error"

    const-string v5, "file %s can\'t be deleted!"

    const-string v6, "browser_ipc_failed"

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6}, Ljava/lang/RuntimeException;-><init>()V

    invoke-virtual {v1, v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1063435
    :cond_3
    move v1, v3

    .line 1063436
    if-eqz v1, :cond_4

    .line 1063437
    const-string v1, "crash"

    const-string v3, "main"

    invoke-virtual {v2, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063438
    :goto_2
    iget-object v1, v0, LX/2nC;->c:LX/0Zb;

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 1063439
    :cond_4
    const-string v1, "crash"

    const-string v3, "browser"

    invoke-virtual {v2, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_2
.end method
