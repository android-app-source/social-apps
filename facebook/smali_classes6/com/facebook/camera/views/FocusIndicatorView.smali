.class public Lcom/facebook/camera/views/FocusIndicatorView;
.super Landroid/view/View;
.source ""

# interfaces
.implements LX/6IM;


# instance fields
.field public a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1073873
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1073874
    iput-object p1, p0, Lcom/facebook/camera/views/FocusIndicatorView;->a:Landroid/content/Context;

    .line 1073875
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 1073876
    iget-object v0, p0, Lcom/facebook/camera/views/FocusIndicatorView;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/camera/views/FocusIndicatorView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1073877
    return-void
.end method

.method private setDrawable(I)V
    .locals 1

    .prologue
    .line 1073878
    invoke-virtual {p0}, Lcom/facebook/camera/views/FocusIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/camera/views/FocusIndicatorView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1073879
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1073880
    const v0, 0x7f0201c5

    invoke-direct {p0, v0}, Lcom/facebook/camera/views/FocusIndicatorView;->setDrawable(I)V

    .line 1073881
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/camera/views/FocusIndicatorView;->setVisibility(I)V

    .line 1073882
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1073883
    const v0, 0x7f04001a

    invoke-direct {p0, v0}, Lcom/facebook/camera/views/FocusIndicatorView;->a(I)V

    .line 1073884
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/facebook/camera/views/FocusIndicatorView;->setVisibility(I)V

    .line 1073885
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1073886
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/facebook/camera/views/FocusIndicatorView;->setVisibility(I)V

    .line 1073887
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1073888
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/camera/views/FocusIndicatorView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1073889
    return-void
.end method
