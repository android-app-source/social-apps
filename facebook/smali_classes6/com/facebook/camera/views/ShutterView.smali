.class public Lcom/facebook/camera/views/ShutterView;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:LX/6I9;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1073937
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1073938
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/camera/views/ShutterView;->setAlpha(F)V

    .line 1073939
    return-void
.end method


# virtual methods
.method public final a(LX/6I9;)V
    .locals 4

    .prologue
    .line 1073940
    iput-object p1, p0, Lcom/facebook/camera/views/ShutterView;->a:LX/6I9;

    .line 1073941
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/facebook/camera/views/ShutterView;->setAlpha(F)V

    .line 1073942
    invoke-virtual {p0}, Lcom/facebook/camera/views/ShutterView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 1073943
    const-wide/16 v2, 0x50

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 1073944
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 1073945
    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 1073946
    iget-object v1, p0, Lcom/facebook/camera/views/ShutterView;->a:LX/6I9;

    if-eqz v1, :cond_0

    .line 1073947
    new-instance v1, LX/6IN;

    invoke-direct {v1, p0}, LX/6IN;-><init>(Lcom/facebook/camera/views/ShutterView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1073948
    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1073949
    return-void
.end method
