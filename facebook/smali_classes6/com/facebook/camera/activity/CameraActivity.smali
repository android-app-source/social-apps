.class public final Lcom/facebook/camera/activity/CameraActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/6HE;
.implements LX/0l6;
.implements LX/15O;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final p:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static t:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "LX/6HD;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile u:Landroid/net/Uri;

.field private static final v:Ljava/lang/Object;


# instance fields
.field private q:LX/6HF;

.field private r:LX/6IA;

.field private s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1071479
    const-class v0, Lcom/facebook/camera/activity/CameraActivity;

    sput-object v0, Lcom/facebook/camera/activity/CameraActivity;->p:Ljava/lang/Class;

    .line 1071480
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/camera/activity/CameraActivity;->v:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1071481
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1071482
    return-void
.end method

.method private a(LX/6HF;LX/6IA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1071483
    iput-object p1, p0, Lcom/facebook/camera/activity/CameraActivity;->q:LX/6HF;

    .line 1071484
    iput-object p2, p0, Lcom/facebook/camera/activity/CameraActivity;->r:LX/6IA;

    .line 1071485
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/camera/activity/CameraActivity;

    new-instance p1, LX/FAn;

    invoke-static {v1}, LX/73w;->b(LX/0QB;)LX/73w;

    move-result-object v0

    check-cast v0, LX/73w;

    invoke-static {v1}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v2

    check-cast v2, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v1}, LX/74I;->a(LX/0QB;)LX/74I;

    move-result-object v3

    check-cast v3, LX/74I;

    invoke-direct {p1, v0, v2, v3}, LX/FAn;-><init>(LX/73w;Lcom/facebook/performancelogger/PerformanceLogger;LX/74I;)V

    move-object v0, p1

    check-cast v0, LX/6HF;

    invoke-static {v1}, LX/6IA;->b(LX/0QB;)LX/6IA;

    move-result-object v1

    check-cast v1, LX/6IA;

    invoke-direct {p0, v0, v1}, Lcom/facebook/camera/activity/CameraActivity;->a(LX/6HF;LX/6IA;)V

    return-void
.end method

.method private static b(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1071486
    sget-object v1, Lcom/facebook/camera/activity/CameraActivity;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 1071487
    :try_start_0
    sput-object p0, Lcom/facebook/camera/activity/CameraActivity;->u:Landroid/net/Uri;

    .line 1071488
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b([BI)V
    .locals 3

    .prologue
    .line 1071489
    sget-object v1, Lcom/facebook/camera/activity/CameraActivity;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 1071490
    :try_start_0
    new-instance v0, Ljava/lang/ref/SoftReference;

    new-instance v2, LX/6HD;

    invoke-direct {v2, p0, p1}, LX/6HD;-><init>([BI)V

    invoke-direct {v0, v2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/facebook/camera/activity/CameraActivity;->t:Ljava/lang/ref/SoftReference;

    .line 1071491
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static l()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1071492
    sget-object v1, Lcom/facebook/camera/activity/CameraActivity;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 1071493
    :try_start_0
    sget-object v0, Lcom/facebook/camera/activity/CameraActivity;->u:Landroid/net/Uri;

    monitor-exit v1

    return-object v0

    .line 1071494
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static p()V
    .locals 2

    .prologue
    .line 1071495
    sget-object v1, Lcom/facebook/camera/activity/CameraActivity;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 1071496
    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/facebook/camera/activity/CameraActivity;->t:Ljava/lang/ref/SoftReference;

    .line 1071497
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/camera/activity/CameraActivity;->u:Landroid/net/Uri;

    .line 1071498
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 1071499
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/camera/activity/CameraFallbackActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x53a

    invoke-virtual {p0, v0, v1}, Lcom/facebook/camera/activity/CameraActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1071500
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1071501
    const-string v0, "camera"

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1071502
    invoke-static {p1}, Lcom/facebook/camera/activity/CameraActivity;->b(Landroid/net/Uri;)V

    .line 1071503
    iget-boolean v0, p0, Lcom/facebook/camera/activity/CameraActivity;->s:Z

    if-eqz v0, :cond_0

    .line 1071504
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/facebook/camera/activity/CameraActivity;->setResult(I)V

    .line 1071505
    invoke-virtual {p0}, Lcom/facebook/camera/activity/CameraActivity;->finish()V

    .line 1071506
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;Ljava/util/List;LX/6HR;Landroid/graphics/Point;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;",
            "LX/6HR;",
            "Landroid/graphics/Point;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1071507
    invoke-static {p1, p4}, LX/6IF;->a(Ljava/util/List;Landroid/graphics/Point;)Landroid/hardware/Camera$Size;

    move-result-object v0

    iput-object v0, p3, LX/6HR;->a:Landroid/hardware/Camera$Size;

    .line 1071508
    invoke-static {p2, p4}, LX/6IF;->a(Ljava/util/List;Landroid/graphics/Point;)Landroid/hardware/Camera$Size;

    move-result-object v0

    iput-object v0, p3, LX/6HR;->b:Landroid/hardware/Camera$Size;

    .line 1071509
    return-void
.end method

.method public final a([BI)V
    .locals 0

    .prologue
    .line 1071304
    invoke-static {p1, p2}, Lcom/facebook/camera/activity/CameraActivity;->b([BI)V

    .line 1071305
    return-void
.end method

.method public final b()LX/6c5;
    .locals 1

    .prologue
    .line 1071478
    sget-object v0, LX/6c5;->HIDE:LX/6c5;

    return-object v0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 1071510
    invoke-virtual {p0, p1}, Lcom/facebook/camera/activity/CameraActivity;->setRequestedOrientation(I)V

    .line 1071511
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1071268
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1071269
    invoke-static {p0, p0}, Lcom/facebook/camera/activity/CameraActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1071270
    invoke-virtual {p0}, Lcom/facebook/camera/activity/CameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1071271
    const-string v1, "return_after_snap"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/camera/activity/CameraActivity;->s:Z

    .line 1071272
    const-string v1, "photo_flow_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1071273
    if-eqz v1, :cond_0

    .line 1071274
    iget-object v2, p0, Lcom/facebook/camera/activity/CameraActivity;->q:LX/6HF;

    invoke-interface {v2, v1}, LX/6HF;->b(Ljava/lang/String;)V

    .line 1071275
    :cond_0
    iget-object v1, p0, Lcom/facebook/camera/activity/CameraActivity;->r:LX/6IA;

    sget-object v2, Lcom/facebook/camera/activity/CameraActivity;->p:Ljava/lang/Class;

    iget-object v3, p0, Lcom/facebook/camera/activity/CameraActivity;->q:LX/6HF;

    .line 1071276
    iput-object v2, v1, LX/6IA;->b:Ljava/lang/Class;

    .line 1071277
    iput-object v0, v1, LX/6IA;->c:Landroid/content/Intent;

    .line 1071278
    iput-object v3, v1, LX/6IA;->ab:LX/6HF;

    .line 1071279
    iput-object p0, v1, LX/6IA;->d:LX/6HE;

    .line 1071280
    iget-object v0, p0, Lcom/facebook/camera/activity/CameraActivity;->r:LX/6IA;

    .line 1071281
    iget-object v1, v0, LX/6IA;->c:Landroid/content/Intent;

    const-string v2, "source_activity"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1071282
    if-nez v1, :cond_1

    .line 1071283
    const-string v1, "<unspecified>"

    .line 1071284
    :cond_1
    iget-object v2, v0, LX/6IA;->ab:LX/6HF;

    invoke-interface {v2, p1, v1}, LX/6HF;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 1071285
    new-instance v2, LX/6HG;

    invoke-direct {v2, v1}, LX/6HG;-><init>(Ljava/lang/String;)V

    iput-object v2, v0, LX/6IA;->ac:LX/6HG;

    .line 1071286
    new-instance v1, LX/6I8;

    iget-object v2, v0, LX/6IA;->af:LX/03V;

    invoke-direct {v1, v0, v2}, LX/6I8;-><init>(LX/6IA;LX/03V;)V

    iput-object v1, v0, LX/6IA;->D:LX/6I8;

    .line 1071287
    invoke-static {v0}, LX/6IA;->p(LX/6IA;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x41200000    # 10.0f

    invoke-static {v1, v2}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v1

    .line 1071288
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, v0, LX/6IA;->p:Landroid/graphics/Rect;

    .line 1071289
    new-instance v2, LX/6Hu;

    invoke-static {v0}, LX/6IA;->p(LX/6IA;)Landroid/app/Activity;

    move-result-object v3

    iget-object v1, v0, LX/6IA;->am:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v4, v0, LX/6IA;->b:Ljava/lang/Class;

    invoke-direct {v2, v3, v1, v4}, LX/6Hu;-><init>(Landroid/app/Activity;ZLjava/lang/Class;)V

    .line 1071290
    iget-object v1, v2, LX/6Hu;->b:LX/6IG;

    iput-object v1, v0, LX/6IA;->ao:LX/6IG;

    .line 1071291
    iget-object v1, v2, LX/6Hu;->a:LX/6IG;

    iput-object v1, v0, LX/6IA;->an:LX/6IG;

    .line 1071292
    iget v1, v2, LX/6Hu;->c:I

    iput v1, v0, LX/6IA;->ap:I

    .line 1071293
    iget-object v1, v0, LX/6IA;->d:LX/6HE;

    iget v3, v2, LX/6Hu;->e:I

    invoke-interface {v1, v3}, LX/6HE;->b(I)V

    .line 1071294
    iget-object v1, v0, LX/6IA;->ab:LX/6HF;

    iget-object v3, v0, LX/6IA;->ao:LX/6IG;

    invoke-interface {v1, v3}, LX/6HF;->a(LX/6IG;)V

    .line 1071295
    iget-object v1, v0, LX/6IA;->ab:LX/6HF;

    iget-object v3, v0, LX/6IA;->an:LX/6IG;

    invoke-interface {v1, v3}, LX/6HF;->b(LX/6IG;)V

    .line 1071296
    iget v1, v2, LX/6Hu;->d:I

    move v0, v1

    .line 1071297
    invoke-virtual {p0, v0}, Lcom/facebook/camera/activity/CameraActivity;->setContentView(I)V

    .line 1071298
    iget-object v0, p0, Lcom/facebook/camera/activity/CameraActivity;->r:LX/6IA;

    const v1, 0x7f0d0867

    invoke-virtual {p0, v1}, Lcom/facebook/camera/activity/CameraActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6IA;->a(Landroid/view/View;)V

    .line 1071299
    iget-object v0, p0, Lcom/facebook/camera/activity/CameraActivity;->r:LX/6IA;

    const/4 v1, 0x1

    .line 1071300
    iget-object v2, v0, LX/6IA;->ab:LX/6HF;

    invoke-interface {v2, v1}, LX/6HF;->b(Z)V

    .line 1071301
    iput-boolean v1, v0, LX/6IA;->as:Z

    .line 1071302
    invoke-static {v0}, LX/6IA;->r(LX/6IA;)V

    .line 1071303
    return-void
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 1071306
    invoke-virtual {p0, p1}, Lcom/facebook/camera/activity/CameraActivity;->setResult(I)V

    .line 1071307
    return-void
.end method

.method public final m()Landroid/app/Activity;
    .locals 0

    .prologue
    .line 1071308
    return-object p0
.end method

.method public final n()Landroid/content/Context;
    .locals 0

    .prologue
    .line 1071309
    return-object p0
.end method

.method public final o()V
    .locals 0

    .prologue
    .line 1071310
    invoke-direct {p0}, Lcom/facebook/camera/activity/CameraActivity;->q()V

    .line 1071311
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 9

    .prologue
    .line 1071312
    invoke-static {}, Lcom/facebook/camera/activity/CameraActivity;->p()V

    .line 1071313
    iget-object v0, p0, Lcom/facebook/camera/activity/CameraActivity;->r:LX/6IA;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1071314
    invoke-static {v0}, LX/6IA;->p(LX/6IA;)Landroid/app/Activity;

    move-result-object v6

    .line 1071315
    iput-boolean v2, v0, LX/6IA;->av:Z

    .line 1071316
    const/16 v4, 0x53a

    if-ne p1, v4, :cond_1

    .line 1071317
    iput-boolean v3, v0, LX/6IA;->av:Z

    .line 1071318
    packed-switch p2, :pswitch_data_0

    .line 1071319
    :cond_0
    :goto_0
    return-void

    .line 1071320
    :pswitch_0
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1071321
    iget-object v2, v0, LX/6IA;->ai:LX/2Ib;

    invoke-virtual {v2}, LX/2Ib;->c()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, v0, LX/6IA;->aw:Landroid/net/Uri;

    .line 1071322
    const-string v2, "output"

    iget-object v3, v0, LX/6IA;->aw:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1071323
    iget-object v2, v0, LX/6IA;->ag:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x53b

    invoke-static {v0}, LX/6IA;->p(LX/6IA;)Landroid/app/Activity;

    move-result-object v4

    invoke-interface {v2, v1, v3, v4}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1071324
    :goto_1
    goto :goto_0

    .line 1071325
    :pswitch_1
    invoke-virtual {v0}, LX/6IA;->o()V

    goto :goto_0

    .line 1071326
    :pswitch_2
    invoke-virtual {v0, v3}, LX/6IA;->b(I)V

    goto :goto_0

    .line 1071327
    :pswitch_3
    invoke-virtual {v0, v1}, LX/6IA;->b(I)V

    goto :goto_0

    .line 1071328
    :pswitch_4
    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 1071329
    :cond_1
    if-eqz p2, :cond_0

    .line 1071330
    const/16 v4, 0x534

    if-ne p1, v4, :cond_3

    .line 1071331
    iput-boolean v3, v0, LX/6IA;->av:Z

    .line 1071332
    if-nez p3, :cond_2

    .line 1071333
    new-instance p3, Landroid/content/Intent;

    invoke-direct {p3}, Landroid/content/Intent;-><init>()V

    .line 1071334
    :cond_2
    const-string v2, "mediaContentType"

    invoke-virtual {p3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1071335
    invoke-virtual {v6, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1071336
    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 1071337
    :cond_3
    const/16 v4, 0x536

    if-ne p1, v4, :cond_4

    .line 1071338
    const/4 v1, 0x4

    if-eq p2, v1, :cond_0

    .line 1071339
    iput-boolean v3, v0, LX/6IA;->av:Z

    .line 1071340
    invoke-virtual {v6, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1071341
    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 1071342
    :cond_4
    const/16 v4, 0x537

    if-ne p1, v4, :cond_c

    .line 1071343
    iput-boolean v3, v0, LX/6IA;->av:Z

    .line 1071344
    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 1071345
    if-nez v5, :cond_5

    .line 1071346
    iput-boolean v2, v0, LX/6IA;->av:Z

    goto :goto_0

    .line 1071347
    :cond_5
    const/4 v4, 0x0

    .line 1071348
    const-string v7, "image/"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1071349
    iget-object v1, v0, LX/6IA;->ab:LX/6HF;

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-interface {v1, v5}, LX/6HF;->a(Landroid/net/Uri;)V

    .line 1071350
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v1, v5}, LX/2Ib;->a(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v1

    .line 1071351
    if-eqz v1, :cond_6

    .line 1071352
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "file://"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move v4, v3

    move-object v5, v1

    move v1, v2

    .line 1071353
    :goto_2
    if-eqz v1, :cond_a

    .line 1071354
    iput-boolean v2, v0, LX/6IA;->av:Z

    .line 1071355
    const v1, 0x7f0811a1

    invoke-static {v6, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_6
    move v1, v3

    move-object v5, v4

    move v4, v3

    .line 1071356
    goto :goto_2

    :cond_7
    const-string v7, "video/"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1071357
    iget-object v5, v0, LX/6IA;->ab:LX/6HF;

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-interface {v5, v7}, LX/6HF;->b(Landroid/net/Uri;)V

    .line 1071358
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-static {v5, v7}, LX/2Ib;->b(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v5

    .line 1071359
    if-eqz v5, :cond_8

    .line 1071360
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "file://"

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    move-object v5, v4

    move v4, v1

    move v1, v2

    goto :goto_2

    :cond_8
    move-object v5, v4

    move v4, v1

    move v1, v3

    .line 1071361
    goto :goto_2

    .line 1071362
    :cond_9
    iget-object v1, v0, LX/6IA;->af:LX/03V;

    iget-object v7, v0, LX/6IA;->b:Ljava/lang/Class;

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    const-string p0, "unknown content type:"

    invoke-direct {v8, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v7, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    move-object v5, v4

    move v4, v2

    goto :goto_2

    .line 1071363
    :cond_a
    if-nez v5, :cond_b

    .line 1071364
    iput-boolean v2, v0, LX/6IA;->av:Z

    .line 1071365
    const v1, 0x7f0811a2

    invoke-static {v6, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1071366
    :cond_b
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1071367
    invoke-static {v0, v5, v4}, LX/6IA;->a(LX/6IA;Landroid/net/Uri;I)V

    goto/16 :goto_0

    .line 1071368
    :cond_c
    const/16 v1, 0x538

    if-ne p1, v1, :cond_d

    .line 1071369
    iput-boolean v3, v0, LX/6IA;->av:Z

    .line 1071370
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1071371
    iget-object v2, v0, LX/6IA;->ab:LX/6HF;

    invoke-interface {v2, v3}, LX/6HF;->a(Z)V

    .line 1071372
    if-eqz v1, :cond_0

    .line 1071373
    invoke-static {v0}, LX/6IA;->p(LX/6IA;)Landroid/app/Activity;

    move-result-object v3

    .line 1071374
    iget-boolean v2, v0, LX/6IA;->E:Z

    if-eqz v2, :cond_10

    .line 1071375
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1071376
    iget-object v4, v0, LX/6IA;->ab:LX/6HF;

    invoke-interface {v4, v2}, LX/6HF;->a(Landroid/content/Intent;)V

    .line 1071377
    const-string v4, "mediaContentType"

    const/4 v5, 0x2

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1071378
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1071379
    const/4 v4, -0x1

    invoke-virtual {v3, v4, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1071380
    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    .line 1071381
    :goto_3
    goto/16 :goto_0

    .line 1071382
    :cond_d
    const/16 v1, 0x53b

    if-ne p1, v1, :cond_f

    .line 1071383
    iput-boolean v3, v0, LX/6IA;->av:Z

    .line 1071384
    iget-boolean v1, v0, LX/6IA;->E:Z

    if-eqz v1, :cond_e

    .line 1071385
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1071386
    iget-object v2, v0, LX/6IA;->aw:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1071387
    invoke-virtual {v6, v3, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1071388
    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1071389
    :cond_e
    iget-object v1, v0, LX/6IA;->aw:Landroid/net/Uri;

    invoke-static {v0, v1, v3}, LX/6IA;->a(LX/6IA;Landroid/net/Uri;I)V

    goto/16 :goto_0

    .line 1071390
    :cond_f
    const/16 v1, 0x53c

    if-ne p1, v1, :cond_0

    .line 1071391
    iput-boolean v3, v0, LX/6IA;->av:Z

    .line 1071392
    invoke-virtual {v6, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1071393
    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1071394
    :catch_0
    iget-object v1, v0, LX/6IA;->aj:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f08119f

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    const/16 v3, 0x11

    .line 1071395
    iput v3, v2, LX/27k;->b:I

    .line 1071396
    move-object v2, v2

    .line 1071397
    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    goto/16 :goto_1

    .line 1071398
    :cond_10
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 1071399
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.PICK"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v1, "image/*"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 1071400
    invoke-virtual {v4, v5}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    .line 1071401
    if-eqz v1, :cond_11

    .line 1071402
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 1071403
    if-nez v4, :cond_11

    .line 1071404
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v4, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "android.intent.action.MAIN"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "android.intent.category.LAUNCHER"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 1071405
    :cond_11
    const/high16 v5, 0x10000000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1071406
    move-object v4, v4

    .line 1071407
    new-instance v5, LX/6Hq;

    sget-object v1, LX/6Hp;->EXTERNAL:LX/6Hp;

    invoke-direct {v5, v4, v1}, LX/6Hq;-><init>(Landroid/content/Intent;LX/6Hp;)V

    move-object v4, v5

    .line 1071408
    if-eqz v4, :cond_12

    iget-object v2, v4, LX/6Hq;->a:Landroid/content/Intent;

    .line 1071409
    :goto_4
    if-eqz v2, :cond_14

    .line 1071410
    iget-object v4, v4, LX/6Hq;->b:LX/6Hp;

    sget-object v5, LX/6Hp;->INTERNAL:LX/6Hp;

    if-ne v4, v5, :cond_13

    .line 1071411
    iget-object v4, v0, LX/6IA;->ab:LX/6HF;

    const-string v5, "launching_composer_for_video"

    invoke-interface {v4, v5}, LX/6HF;->a(Ljava/lang/String;)V

    .line 1071412
    iget-object v4, v0, LX/6IA;->ab:LX/6HF;

    invoke-interface {v4, v2}, LX/6HF;->a(Landroid/content/Intent;)V

    .line 1071413
    iget-object v4, v0, LX/6IA;->ag:Lcom/facebook/content/SecureContextHelper;

    const/16 v5, 0x534

    invoke-interface {v4, v2, v5, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto/16 :goto_3

    .line 1071414
    :cond_12
    const/4 v2, 0x0

    goto :goto_4

    .line 1071415
    :cond_13
    iget-object v4, v0, LX/6IA;->ag:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v2, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_3

    .line 1071416
    :cond_14
    iget-object v2, v0, LX/6IA;->b:Ljava/lang/Class;

    const-string v3, "no ComposerForVideo intent could be created"

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 1071417
    iget-object v0, p0, Lcom/facebook/camera/activity/CameraActivity;->r:LX/6IA;

    const/4 v1, 0x0

    .line 1071418
    iget-object v2, v0, LX/6IA;->ar:LX/6HU;

    .line 1071419
    iget-boolean v3, v2, LX/6HU;->k:Z

    move v2, v3

    .line 1071420
    if-eqz v2, :cond_0

    .line 1071421
    iget-object v2, v0, LX/6IA;->ar:LX/6HU;

    invoke-virtual {v2}, LX/6HU;->h()V

    .line 1071422
    invoke-static {v0, v1}, LX/6IA;->g(LX/6IA;Z)V

    .line 1071423
    :cond_0
    invoke-static {v0}, LX/6IA;->y(LX/6IA;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1071424
    iget-object v1, v0, LX/6IA;->ab:LX/6HF;

    const-string v2, "back_pressed"

    invoke-interface {v1, v2}, LX/6HF;->a(Ljava/lang/String;)V

    .line 1071425
    const/4 v1, 0x1

    .line 1071426
    :cond_1
    move v0, v1

    .line 1071427
    if-eqz v0, :cond_2

    .line 1071428
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1071429
    :cond_2
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1071430
    packed-switch p1, :pswitch_data_0

    .line 1071431
    invoke-super {p0, p1, p2}, Lcom/facebook/base/activity/FbFragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 1071432
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x54
        :pswitch_0
    .end packed-switch
.end method

.method public final onPause()V
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0xa86ddcf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1071433
    iget-object v1, p0, Lcom/facebook/camera/activity/CameraActivity;->r:LX/6IA;

    .line 1071434
    iget-object v4, v1, LX/6IA;->ac:LX/6HG;

    .line 1071435
    const/4 v5, 0x0

    iput-boolean v5, v4, LX/6HG;->f:Z

    .line 1071436
    iget-wide v5, v4, LX/6HG;->e:J

    invoke-static {}, LX/6HG;->m()J

    move-result-wide v7

    iget-wide v9, v4, LX/6HG;->g:J

    sub-long/2addr v7, v9

    add-long/2addr v5, v7

    iput-wide v5, v4, LX/6HG;->e:J

    .line 1071437
    const/4 v4, 0x0

    iput-boolean v4, v1, LX/6IA;->at:Z

    .line 1071438
    invoke-static {v1}, LX/6IA;->r(LX/6IA;)V

    .line 1071439
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 1071440
    const/16 v1, 0x23

    const v2, 0x718ac4e4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x85fcf20

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1071441
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 1071442
    iget-object v1, p0, Lcom/facebook/camera/activity/CameraActivity;->r:LX/6IA;

    .line 1071443
    iget-object v4, v1, LX/6IA;->ac:LX/6HG;

    .line 1071444
    const/4 v5, 0x1

    iput-boolean v5, v4, LX/6HG;->f:Z

    .line 1071445
    invoke-static {}, LX/6HG;->m()J

    move-result-wide v5

    iput-wide v5, v4, LX/6HG;->g:J

    .line 1071446
    iget-object v4, v1, LX/6IA;->D:LX/6I8;

    invoke-virtual {v4}, LX/6I7;->b()V

    .line 1071447
    const/4 v4, 0x1

    iput-boolean v4, v1, LX/6IA;->at:Z

    .line 1071448
    invoke-static {v1}, LX/6IA;->r(LX/6IA;)V

    .line 1071449
    const/16 v1, 0x23

    const v2, 0x321a36f9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1071450
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1071451
    iget-object v0, p0, Lcom/facebook/camera/activity/CameraActivity;->q:LX/6HF;

    invoke-interface {v0, p1}, LX/6HF;->a(Landroid/os/Bundle;)V

    .line 1071452
    return-void
.end method

.method public final onSearchRequested()Z
    .locals 1

    .prologue
    .line 1071453
    const/4 v0, 0x0

    return v0
.end method

.method public final onStop()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x11517d7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1071454
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 1071455
    iget-object v1, p0, Lcom/facebook/camera/activity/CameraActivity;->r:LX/6IA;

    if-eqz v1, :cond_3

    .line 1071456
    iget-object v1, p0, Lcom/facebook/camera/activity/CameraActivity;->r:LX/6IA;

    .line 1071457
    iget-object v2, v1, LX/6IA;->aq:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    .line 1071458
    iget-object v4, v1, LX/6IA;->ar:LX/6HU;

    .line 1071459
    sget-object v5, LX/6Ho;->c:LX/0Tn;

    iget p0, v4, LX/6HU;->x:I

    invoke-interface {v2, v5, p0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 1071460
    iget-object v5, v4, LX/6HU;->f:LX/6HI;

    .line 1071461
    sget-object p0, LX/6Ho;->b:LX/0Tn;

    iget-object v4, v5, LX/6HI;->g:Ljava/lang/String;

    invoke-interface {v2, p0, v4}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1071462
    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1071463
    iget-boolean v2, v1, LX/6IA;->av:Z

    if-eqz v2, :cond_0

    iget-object v2, v1, LX/6IA;->ac:LX/6HG;

    invoke-virtual {v2}, LX/6HG;->l()F

    move-result v2

    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v2, v2, v4

    if-ltz v2, :cond_1

    .line 1071464
    :cond_0
    iget-object v2, v1, LX/6IA;->ab:LX/6HF;

    iget-object v4, v1, LX/6IA;->ac:LX/6HG;

    invoke-interface {v2, v4}, LX/6HF;->a(LX/6HG;)V

    .line 1071465
    :cond_1
    iget-object v2, v1, LX/6IA;->C:Landroid/animation/ObjectAnimator;

    if-eqz v2, :cond_2

    .line 1071466
    iget-object v2, v1, LX/6IA;->C:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1071467
    const/4 v2, 0x0

    iput-object v2, v1, LX/6IA;->C:Landroid/animation/ObjectAnimator;

    .line 1071468
    :cond_2
    iget-object v2, v1, LX/6IA;->n:LX/6IL;

    invoke-virtual {v2}, LX/6IL;->b()V

    .line 1071469
    iget-object v2, v1, LX/6IA;->o:LX/6IL;

    invoke-virtual {v2}, LX/6IL;->b()V

    .line 1071470
    iget-object v2, v1, LX/6IA;->ac:LX/6HG;

    invoke-virtual {v2}, LX/6HG;->a()V

    .line 1071471
    :cond_3
    const/16 v1, 0x23

    const v2, 0x359e36a1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onUserInteraction()V
    .locals 5

    .prologue
    .line 1071472
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onUserInteraction()V

    .line 1071473
    iget-object v0, p0, Lcom/facebook/camera/activity/CameraActivity;->r:LX/6IA;

    if-eqz v0, :cond_0

    .line 1071474
    iget-object v0, p0, Lcom/facebook/camera/activity/CameraActivity;->r:LX/6IA;

    .line 1071475
    iget-object v1, v0, LX/6IA;->Y:LX/1ql;

    if-eqz v1, :cond_0

    .line 1071476
    iget-object v1, v0, LX/6IA;->Y:LX/1ql;

    const-wide/32 v3, 0x2bf20

    invoke-virtual {v1, v3, v4}, LX/1ql;->a(J)V

    .line 1071477
    :cond_0
    return-void
.end method
