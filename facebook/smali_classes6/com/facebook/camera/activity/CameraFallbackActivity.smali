.class public Lcom/facebook/camera/activity/CameraFallbackActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1071512
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1071513
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1071514
    const v0, 0x7f030223

    invoke-virtual {p0, v0}, Lcom/facebook/camera/activity/CameraFallbackActivity;->setContentView(I)V

    .line 1071515
    const v0, 0x7f0d085c

    invoke-virtual {p0, v0}, Lcom/facebook/camera/activity/CameraFallbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1071516
    const v0, 0x7f0d085d

    invoke-virtual {p0, v0}, Lcom/facebook/camera/activity/CameraFallbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1071517
    const v0, 0x7f0d085e

    invoke-virtual {p0, v0}, Lcom/facebook/camera/activity/CameraFallbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1071518
    const v0, 0x7f0d085f

    invoke-virtual {p0, v0}, Lcom/facebook/camera/activity/CameraFallbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1071519
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x57d32a5a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1071520
    const/4 v2, -0x1

    .line 1071521
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    .line 1071522
    const v5, 0x7f0d085c

    if-ne v4, v5, :cond_1

    .line 1071523
    const/4 v0, 0x4

    .line 1071524
    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/camera/activity/CameraFallbackActivity;->setResult(I)V

    .line 1071525
    invoke-virtual {p0}, Lcom/facebook/camera/activity/CameraFallbackActivity;->finish()V

    .line 1071526
    const v0, -0x44b8e476

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    .line 1071527
    :cond_1
    const v5, 0x7f0d085e

    if-eq v4, v5, :cond_0

    .line 1071528
    const v0, 0x7f0d085f

    if-ne v4, v0, :cond_2

    move v0, v1

    .line 1071529
    goto :goto_0

    .line 1071530
    :cond_2
    const v0, 0x7f0d085d

    if-ne v4, v0, :cond_3

    .line 1071531
    const/4 v0, 0x3

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method
