.class public Lcom/facebook/android/maps/MapView;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements LX/68F;
.implements LX/68G;
.implements LX/68H;


# static fields
.field public static final a:D


# instance fields
.field private A:Z

.field private final B:Landroid/graphics/RectF;

.field private C:F

.field private D:F

.field private E:LX/68j;

.field private F:Z

.field private final G:[F

.field private final H:[F

.field private I:Lcom/facebook/android/maps/internal/AutoAnimationsHelper;

.field public J:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/68J;",
            ">;"
        }
    .end annotation
.end field

.field private K:LX/67m;

.field private L:J

.field private M:F

.field private N:Z

.field private O:Z

.field private P:Z

.field public Q:J

.field public R:J

.field private S:Z

.field private final T:Landroid/content/ComponentCallbacks;

.field private final U:Landroid/content/BroadcastReceiver;

.field public b:Z

.field public c:I

.field public d:I

.field public e:F

.field public f:F

.field public g:I

.field public h:F

.field public i:F

.field public j:F

.field public final k:Landroid/graphics/Matrix;

.field public final l:Landroid/graphics/Matrix;

.field public m:D

.field public n:D

.field public o:D

.field public p:D

.field public q:Z

.field public r:J

.field public s:I

.field private t:Landroid/content/Context;

.field private u:LX/681;

.field public v:LX/680;

.field private w:LX/68Q;

.field private final x:Landroid/graphics/Paint;

.field private y:Z

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1054634
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/facebook/android/maps/MapView;->a:D

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    .line 1054472
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1054473
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->x:Landroid/graphics/Paint;

    .line 1054474
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->B:Landroid/graphics/RectF;

    .line 1054475
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    .line 1054476
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    .line 1054477
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->G:[F

    .line 1054478
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->H:[F

    .line 1054479
    iput-wide v2, p0, Lcom/facebook/android/maps/MapView;->m:D

    .line 1054480
    iput-wide v2, p0, Lcom/facebook/android/maps/MapView;->n:D

    .line 1054481
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->L:J

    .line 1054482
    new-instance v0, LX/68A;

    invoke-direct {v0, p0}, LX/68A;-><init>(Lcom/facebook/android/maps/MapView;)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->T:Landroid/content/ComponentCallbacks;

    .line 1054483
    new-instance v0, LX/68B;

    invoke-direct {v0, p0}, LX/68B;-><init>(Lcom/facebook/android/maps/MapView;)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->U:Landroid/content/BroadcastReceiver;

    .line 1054484
    new-instance v0, LX/681;

    invoke-direct {v0}, LX/681;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/facebook/android/maps/MapView;->a(Landroid/content/Context;LX/681;)V

    .line 1054485
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/681;)V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    .line 1054486
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1054487
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->x:Landroid/graphics/Paint;

    .line 1054488
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->B:Landroid/graphics/RectF;

    .line 1054489
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    .line 1054490
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    .line 1054491
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->G:[F

    .line 1054492
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->H:[F

    .line 1054493
    iput-wide v2, p0, Lcom/facebook/android/maps/MapView;->m:D

    .line 1054494
    iput-wide v2, p0, Lcom/facebook/android/maps/MapView;->n:D

    .line 1054495
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->L:J

    .line 1054496
    new-instance v0, LX/68A;

    invoke-direct {v0, p0}, LX/68A;-><init>(Lcom/facebook/android/maps/MapView;)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->T:Landroid/content/ComponentCallbacks;

    .line 1054497
    new-instance v0, LX/68B;

    invoke-direct {v0, p0}, LX/68B;-><init>(Lcom/facebook/android/maps/MapView;)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->U:Landroid/content/BroadcastReceiver;

    .line 1054498
    invoke-direct {p0, p1, p2}, Lcom/facebook/android/maps/MapView;->a(Landroid/content/Context;LX/681;)V

    .line 1054499
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    .line 1054500
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1054501
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->x:Landroid/graphics/Paint;

    .line 1054502
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->B:Landroid/graphics/RectF;

    .line 1054503
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    .line 1054504
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    .line 1054505
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->G:[F

    .line 1054506
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->H:[F

    .line 1054507
    iput-wide v2, p0, Lcom/facebook/android/maps/MapView;->m:D

    .line 1054508
    iput-wide v2, p0, Lcom/facebook/android/maps/MapView;->n:D

    .line 1054509
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->L:J

    .line 1054510
    new-instance v0, LX/68A;

    invoke-direct {v0, p0}, LX/68A;-><init>(Lcom/facebook/android/maps/MapView;)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->T:Landroid/content/ComponentCallbacks;

    .line 1054511
    new-instance v0, LX/68B;

    invoke-direct {v0, p0}, LX/68B;-><init>(Lcom/facebook/android/maps/MapView;)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->U:Landroid/content/BroadcastReceiver;

    .line 1054512
    invoke-static {p1, p2}, LX/681;->a(Landroid/content/Context;Landroid/util/AttributeSet;)LX/681;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/android/maps/MapView;->a(Landroid/content/Context;LX/681;)V

    .line 1054513
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    .line 1054514
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1054515
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->x:Landroid/graphics/Paint;

    .line 1054516
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->B:Landroid/graphics/RectF;

    .line 1054517
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    .line 1054518
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    .line 1054519
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->G:[F

    .line 1054520
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->H:[F

    .line 1054521
    iput-wide v2, p0, Lcom/facebook/android/maps/MapView;->m:D

    .line 1054522
    iput-wide v2, p0, Lcom/facebook/android/maps/MapView;->n:D

    .line 1054523
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->L:J

    .line 1054524
    new-instance v0, LX/68A;

    invoke-direct {v0, p0}, LX/68A;-><init>(Lcom/facebook/android/maps/MapView;)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->T:Landroid/content/ComponentCallbacks;

    .line 1054525
    new-instance v0, LX/68B;

    invoke-direct {v0, p0}, LX/68B;-><init>(Lcom/facebook/android/maps/MapView;)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->U:Landroid/content/BroadcastReceiver;

    .line 1054526
    invoke-static {p1, p2}, LX/681;->a(Landroid/content/Context;Landroid/util/AttributeSet;)LX/681;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/android/maps/MapView;->a(Landroid/content/Context;LX/681;)V

    .line 1054527
    return-void
.end method

.method public static a(D)D
    .locals 2

    .prologue
    .line 1054528
    const-wide/16 v0, 0x0

    cmpg-double v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-double v0, v0

    add-double/2addr v0, p0

    return-wide v0

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p0, v0

    if-lez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;FF)LX/67m;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/67m;",
            ">;FF)",
            "LX/67m;"
        }
    .end annotation

    .prologue
    .line 1054529
    const/4 v3, 0x0

    .line 1054530
    const/4 v2, 0x0

    .line 1054531
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_0
    if-ltz v4, :cond_1

    .line 1054532
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67m;

    .line 1054533
    iget-boolean v1, v0, LX/67m;->i:Z

    move v1, v1

    .line 1054534
    if-eqz v1, :cond_2

    .line 1054535
    invoke-virtual {v0, p1, p2}, LX/67m;->a(FF)I

    move-result v1

    .line 1054536
    const/4 v5, 0x2

    if-ne v1, v5, :cond_0

    .line 1054537
    :goto_1
    return-object v0

    .line 1054538
    :cond_0
    if-le v1, v2, :cond_2

    move v6, v1

    move-object v1, v0

    move v0, v6

    .line 1054539
    :goto_2
    add-int/lit8 v2, v4, -0x1

    move v4, v2

    move-object v3, v1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v3

    .line 1054540
    goto :goto_1

    :cond_2
    move v0, v2

    move-object v1, v3

    goto :goto_2
.end method

.method private a()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1054541
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v0}, LX/680;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->j:LX/3BW;

    .line 1054542
    iget-boolean v1, v0, LX/3BW;->e:Z

    move v0, v1

    .line 1054543
    if-nez v0, :cond_0

    .line 1054544
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->j:LX/3BW;

    invoke-virtual {v0, v4}, LX/3BW;->a(Z)V

    .line 1054545
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/android/maps/MapView;->O:Z

    if-nez v0, :cond_2

    .line 1054546
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 1054547
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->t:Landroid/content/Context;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->T:Landroid/content/ComponentCallbacks;

    invoke-virtual {v0, v1}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 1054548
    :cond_1
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->t:Landroid/content/Context;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->U:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1054549
    iput-boolean v4, p0, Lcom/facebook/android/maps/MapView;->O:Z

    .line 1054550
    :cond_2
    return-void
.end method

.method private a(IF)V
    .locals 2

    .prologue
    .line 1054551
    iput p1, p0, Lcom/facebook/android/maps/MapView;->g:I

    .line 1054552
    iput p2, p0, Lcom/facebook/android/maps/MapView;->h:F

    .line 1054553
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/android/maps/MapView;->g:I

    shl-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/android/maps/MapView;->s:I

    .line 1054554
    iget v0, p0, Lcom/facebook/android/maps/MapView;->s:I

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    .line 1054555
    iget p1, v1, LX/680;->B:I

    move v1, p1

    .line 1054556
    mul-int/2addr v0, v1

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->r:J

    .line 1054557
    return-void
.end method

.method private a(Landroid/content/Context;LX/681;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1054558
    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/android/maps/MapView;->Q:J

    .line 1054559
    invoke-virtual {p0, v0}, Lcom/facebook/android/maps/MapView;->setWillNotDraw(Z)V

    .line 1054560
    iput-object p1, p0, Lcom/facebook/android/maps/MapView;->t:Landroid/content/Context;

    .line 1054561
    iput-object p2, p0, Lcom/facebook/android/maps/MapView;->u:LX/681;

    .line 1054562
    new-instance v1, LX/68j;

    invoke-direct {v1, p1, p0}, LX/68j;-><init>(Landroid/content/Context;LX/68G;)V

    iput-object v1, p0, Lcom/facebook/android/maps/MapView;->E:LX/68j;

    .line 1054563
    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->E:LX/68j;

    iget-object v2, p0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    .line 1054564
    iput-object v2, v1, LX/68j;->b:Landroid/graphics/Matrix;

    .line 1054565
    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->E:LX/68j;

    const v2, 0x3f5eb852    # 0.87f

    .line 1054566
    iput v2, v1, LX/68j;->E:F

    .line 1054567
    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->E:LX/68j;

    const v2, 0x3f59999a    # 0.85f

    .line 1054568
    iput v2, v1, LX/68j;->F:F

    .line 1054569
    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->t:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/android/maps/MapView;->z:Z

    .line 1054570
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->A:Z

    .line 1054571
    new-instance v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;

    invoke-direct {v0, p0, p0}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;-><init>(Landroid/view/View;LX/68F;)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->I:Lcom/facebook/android/maps/internal/AutoAnimationsHelper;

    .line 1054572
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->I:Lcom/facebook/android/maps/internal/AutoAnimationsHelper;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    .line 1054573
    iput-object v1, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->m:Landroid/graphics/Matrix;

    .line 1054574
    sget-object v0, LX/3BU;->h:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1054575
    invoke-static {}, LX/3BU;->a()V

    .line 1054576
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Z)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const-wide/16 v10, 0x0

    .line 1054577
    invoke-static {}, LX/31U;->a()J

    move-result-wide v6

    .line 1054578
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1054579
    const v0, -0xf121b

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1054580
    iput-boolean v2, p0, Lcom/facebook/android/maps/MapView;->b:Z

    .line 1054581
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_3

    .line 1054582
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->i:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67m;

    .line 1054583
    iget-boolean v1, v0, LX/67m;->i:Z

    move v1, v1

    .line 1054584
    if-eqz v1, :cond_1

    if-eqz p2, :cond_0

    if-eqz p2, :cond_1

    .line 1054585
    iget-boolean v1, v0, LX/67m;->l:Z

    move v1, v1

    .line 1054586
    if-eqz v1, :cond_1

    .line 1054587
    :cond_0
    invoke-virtual {v0, p1}, LX/67m;->a(Landroid/graphics/Canvas;)V

    .line 1054588
    instance-of v1, v0, LX/68l;

    if-eqz v1, :cond_1

    .line 1054589
    check-cast v0, LX/68l;

    .line 1054590
    iget-boolean v8, p0, Lcom/facebook/android/maps/MapView;->b:Z

    iget v1, v0, LX/68P;->s:I

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    and-int/2addr v1, v8

    iput-boolean v1, p0, Lcom/facebook/android/maps/MapView;->b:Z

    .line 1054591
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    move v1, v3

    .line 1054592
    goto :goto_1

    .line 1054593
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/android/maps/MapView;->b:Z

    if-eqz v0, :cond_5

    .line 1054594
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->t:LX/67w;

    if-eqz v0, :cond_4

    .line 1054595
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v0}, LX/680;->p()V

    .line 1054596
    :cond_4
    iget-boolean v0, p0, Lcom/facebook/android/maps/MapView;->S:Z

    if-eqz v0, :cond_5

    .line 1054597
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->l:LX/68m;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->u:LX/681;

    .line 1054598
    iget-object v2, v1, LX/681;->m:Ljava/lang/String;

    move-object v1, v2

    .line 1054599
    iget-object v2, v0, LX/68m;->A:LX/68n;

    .line 1054600
    sget-object v4, LX/31U;->z:LX/31U;

    invoke-virtual {v4}, LX/31U;->c()Z

    move-result v4

    if-nez v4, :cond_8

    .line 1054601
    :goto_2
    iput-boolean v3, p0, Lcom/facebook/android/maps/MapView;->S:Z

    .line 1054602
    :cond_5
    invoke-static {}, LX/31U;->a()J

    move-result-wide v0

    .line 1054603
    iget-object v2, p0, Lcom/facebook/android/maps/MapView;->u:LX/681;

    .line 1054604
    iget-object v3, v2, LX/681;->m:Ljava/lang/String;

    move-object v2, v3

    .line 1054605
    sget-object v3, LX/31U;->a:LX/31U;

    sub-long v4, v0, v6

    invoke-virtual {v3, v4, v5}, LX/31U;->a(J)V

    .line 1054606
    iget-wide v4, p0, Lcom/facebook/android/maps/MapView;->Q:J

    cmp-long v3, v4, v10

    if-lez v3, :cond_6

    .line 1054607
    sget-object v3, LX/31U;->B:LX/31U;

    new-instance v4, LX/68C;

    invoke-direct {v4, p0, v0, v1, v2}, LX/68C;-><init>(Lcom/facebook/android/maps/MapView;JLjava/lang/String;)V

    invoke-virtual {v3, v4}, LX/31U;->a(Ljava/util/Map;)V

    .line 1054608
    iput-wide v10, p0, Lcom/facebook/android/maps/MapView;->Q:J

    .line 1054609
    :cond_6
    iget-wide v4, p0, Lcom/facebook/android/maps/MapView;->R:J

    cmp-long v3, v4, v10

    if-lez v3, :cond_7

    .line 1054610
    sget-object v3, LX/31U;->A:LX/31U;

    new-instance v4, LX/68D;

    invoke-direct {v4, p0, v0, v1, v2}, LX/68D;-><init>(Lcom/facebook/android/maps/MapView;JLjava/lang/String;)V

    invoke-virtual {v3, v4}, LX/31U;->a(Ljava/util/Map;)V

    .line 1054611
    iput-wide v10, p0, Lcom/facebook/android/maps/MapView;->R:J

    .line 1054612
    :cond_7
    return-void

    .line 1054613
    :cond_8
    sget-object v4, LX/31U;->D:LX/31U;

    new-instance v0, LX/68T;

    invoke-direct {v0, v2, v1}, LX/68T;-><init>(LX/68W;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, LX/31U;->a(Ljava/util/Map;)V

    goto :goto_2
.end method

.method private b(FFFF)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1054614
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->G:[F

    iget v1, p0, Lcom/facebook/android/maps/MapView;->e:F

    sub-float/2addr v1, p1

    aput v1, v0, v2

    .line 1054615
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->G:[F

    iget v1, p0, Lcom/facebook/android/maps/MapView;->f:F

    sub-float/2addr v1, p2

    aput v1, v0, v4

    .line 1054616
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->G:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapVectors([F)V

    .line 1054617
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->G:[F

    aget v0, v0, v2

    iget-wide v2, p0, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v1, v2

    div-float/2addr v0, v1

    add-float/2addr v0, p3

    float-to-double v0, v0

    invoke-static {v0, v1}, Lcom/facebook/android/maps/MapView;->a(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->m:D

    .line 1054618
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->G:[F

    aget v0, v0, v4

    iget-wide v2, p0, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v1, v2

    div-float/2addr v0, v1

    add-float/2addr v0, p4

    float-to-double v0, v0

    iget-wide v2, p0, Lcom/facebook/android/maps/MapView;->r:J

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/android/maps/MapView;->a(DJ)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->n:D

    .line 1054619
    return-void
.end method

.method private c()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1054620
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->j:LX/3BW;

    .line 1054621
    iget-boolean v1, v0, LX/3BW;->e:Z

    move v0, v1

    .line 1054622
    if-eqz v0, :cond_0

    .line 1054623
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->j:LX/3BW;

    invoke-virtual {v0, v2}, LX/3BW;->a(Z)V

    .line 1054624
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/android/maps/MapView;->O:Z

    if-eqz v0, :cond_2

    .line 1054625
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 1054626
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->t:Landroid/content/Context;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->T:Landroid/content/ComponentCallbacks;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 1054627
    :cond_1
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->t:Landroid/content/Context;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->U:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1054628
    iput-boolean v2, p0, Lcom/facebook/android/maps/MapView;->O:Z

    .line 1054629
    :cond_2
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v0}, LX/680;->a()V

    .line 1054630
    sget-object v1, LX/31U;->k:[LX/31U;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object p0, v1, v0

    .line 1054631
    invoke-static {p0}, LX/31U;->e(LX/31U;)V

    .line 1054632
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1054633
    :cond_3
    return-void
.end method

.method private c(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1054647
    if-eqz p1, :cond_0

    const-string v0, "zoom"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1054648
    :cond_0
    :goto_0
    return-void

    .line 1054649
    :cond_1
    const-string v0, "zoom"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "scale"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/android/maps/MapView;->a(IF)V

    .line 1054650
    const-string v0, "xVisibleCenter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v2, v2, LX/680;->c:I

    iget-object v3, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v3, v3, LX/680;->e:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    iget-wide v4, p0, Lcom/facebook/android/maps/MapView;->r:J

    shl-long/2addr v4, v6

    div-long/2addr v2, v4

    long-to-double v2, v2

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->m:D

    .line 1054651
    const-string v0, "yVisibleCenter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v2, v2, LX/680;->d:I

    iget-object v3, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v3, v3, LX/680;->f:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    iget-wide v4, p0, Lcom/facebook/android/maps/MapView;->r:J

    shl-long/2addr v4, v6

    div-long/2addr v2, v4

    long-to-double v2, v2

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->n:D

    .line 1054652
    const-string v0, "rotation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/facebook/android/maps/MapView;->j:F

    .line 1054653
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/facebook/android/maps/MapView;->h:F

    iget v2, p0, Lcom/facebook/android/maps/MapView;->h:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 1054654
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/facebook/android/maps/MapView;->j:F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1054655
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1054656
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->P:Z

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1054635
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->l:LX/68m;

    invoke-virtual {v0, v1}, LX/68P;->b(Z)V

    .line 1054636
    iput-boolean v1, p0, Lcom/facebook/android/maps/MapView;->q:Z

    .line 1054637
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v0}, LX/680;->a()V

    .line 1054638
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->I:Lcom/facebook/android/maps/internal/AutoAnimationsHelper;

    const/4 p0, 0x0

    .line 1054639
    iget-object v1, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1054640
    iput-boolean p0, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->d:Z

    .line 1054641
    iput-boolean p0, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->e:Z

    .line 1054642
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->f:Z

    .line 1054643
    invoke-static {v0}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->i(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;)V

    .line 1054644
    invoke-static {v0}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->h(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;)V

    .line 1054645
    invoke-static {v0}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->j(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;)V

    .line 1054646
    return-void
.end method

.method private e(FFF)Z
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v3, 0x40000000    # 2.0f

    .line 1054748
    iget v0, p0, Lcom/facebook/android/maps/MapView;->h:F

    mul-float v1, v0, p1

    .line 1054749
    iget v0, p0, Lcom/facebook/android/maps/MapView;->g:I

    .line 1054750
    :goto_0
    cmpl-float v2, v1, v3

    if-lez v2, :cond_0

    .line 1054751
    div-float/2addr v1, v3

    .line 1054752
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1054753
    :cond_0
    :goto_1
    cmpg-float v2, v1, v4

    if-gez v2, :cond_1

    .line 1054754
    mul-float/2addr v1, v3

    .line 1054755
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1054756
    :cond_1
    int-to-float v0, v0

    add-float/2addr v0, v1

    sub-float/2addr v0, v4

    invoke-virtual {p0, v0, p2, p3}, Lcom/facebook/android/maps/MapView;->c(FFF)Z

    move-result v0

    return v0
.end method

.method private h(FF)V
    .locals 4

    .prologue
    .line 1054745
    iget-wide v0, p0, Lcom/facebook/android/maps/MapView;->m:D

    iget-wide v2, p0, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v2, v2

    div-float v2, p1, v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Lcom/facebook/android/maps/MapView;->a(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->m:D

    .line 1054746
    iget-wide v0, p0, Lcom/facebook/android/maps/MapView;->n:D

    iget-wide v2, p0, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v2, v2

    div-float v2, p2, v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    iget-wide v2, p0, Lcom/facebook/android/maps/MapView;->r:J

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/android/maps/MapView;->a(DJ)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->n:D

    .line 1054747
    return-void
.end method

.method private j()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1054730
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->B:Landroid/graphics/RectF;

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 1054731
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->B:Landroid/graphics/RectF;

    iget v1, p0, Lcom/facebook/android/maps/MapView;->c:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1054732
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->B:Landroid/graphics/RectF;

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 1054733
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->B:Landroid/graphics/RectF;

    iget v1, p0, Lcom/facebook/android/maps/MapView;->d:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1054734
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->B:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1054735
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->H:[F

    iget v1, p0, Lcom/facebook/android/maps/MapView;->e:F

    neg-float v1, v1

    aput v1, v0, v3

    .line 1054736
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->H:[F

    iget v1, p0, Lcom/facebook/android/maps/MapView;->f:F

    neg-float v1, v1

    aput v1, v0, v4

    .line 1054737
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->H:[F

    iget v1, p0, Lcom/facebook/android/maps/MapView;->e:F

    aput v1, v0, v5

    .line 1054738
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->H:[F

    iget v1, p0, Lcom/facebook/android/maps/MapView;->f:F

    neg-float v1, v1

    aput v1, v0, v6

    .line 1054739
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->H:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapVectors([F)V

    .line 1054740
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->H:[F

    aget v0, v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->H:[F

    aget v1, v1, v5

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1054741
    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->H:[F

    aget v1, v1, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget-object v2, p0, Lcom/facebook/android/maps/MapView;->H:[F

    aget v2, v2, v6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 1054742
    iget-wide v2, p0, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v2, v2

    div-float/2addr v0, v2

    float-to-double v2, v0

    iput-wide v2, p0, Lcom/facebook/android/maps/MapView;->o:D

    .line 1054743
    iget-wide v2, p0, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v0, v2

    div-float v0, v1, v0

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->p:D

    .line 1054744
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1054723
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->l:LX/68m;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/68P;->b(Z)V

    .line 1054724
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->I:Lcom/facebook/android/maps/internal/AutoAnimationsHelper;

    .line 1054725
    iget-object v1, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1054726
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->f:Z

    .line 1054727
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->e:Z

    .line 1054728
    invoke-static {v0, v0}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->a(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;Ljava/lang/Runnable;)V

    .line 1054729
    return-void
.end method


# virtual methods
.method public final a(DJ)D
    .locals 5

    .prologue
    .line 1054719
    iget-wide v0, p0, Lcom/facebook/android/maps/MapView;->r:J

    long-to-double v0, v0

    long-to-double v2, p3

    div-double/2addr v0, v2

    .line 1054720
    iget-wide v2, p0, Lcom/facebook/android/maps/MapView;->p:D

    mul-double/2addr v0, v2

    .line 1054721
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v0

    .line 1054722
    cmpg-double v4, p1, v0

    if-gez v4, :cond_1

    move-wide p1, v0

    :cond_0
    :goto_0
    return-wide p1

    :cond_1
    cmpl-double v0, p1, v2

    if-lez v0, :cond_0

    move-wide p1, v2

    goto :goto_0
.end method

.method public final a(DD)V
    .locals 3

    .prologue
    .line 1054716
    invoke-static {p1, p2}, Lcom/facebook/android/maps/MapView;->a(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->m:D

    .line 1054717
    iget-wide v0, p0, Lcom/facebook/android/maps/MapView;->r:J

    invoke-virtual {p0, p3, p4, v0, v1}, Lcom/facebook/android/maps/MapView;->a(DJ)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->n:D

    .line 1054718
    return-void
.end method

.method public final a(FF)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1054709
    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->F:Z

    .line 1054710
    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->N:Z

    .line 1054711
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/android/maps/MapView;->M:F

    .line 1054712
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->i:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/facebook/android/maps/MapView;->a(Ljava/util/List;FF)LX/67m;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    .line 1054713
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    if-eqz v0, :cond_0

    .line 1054714
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    invoke-virtual {v0}, LX/67m;->m()V

    .line 1054715
    :cond_0
    return-void
.end method

.method public final a(FFF)V
    .locals 3

    .prologue
    .line 1054696
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->w:LX/68Q;

    .line 1054697
    iget-boolean v1, v0, LX/68Q;->e:Z

    move v0, v1

    .line 1054698
    if-eqz v0, :cond_1

    .line 1054699
    invoke-direct {p0}, Lcom/facebook/android/maps/MapView;->e()V

    .line 1054700
    iput p2, p0, Lcom/facebook/android/maps/MapView;->C:F

    .line 1054701
    iput p3, p0, Lcom/facebook/android/maps/MapView;->D:F

    .line 1054702
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/android/maps/MapView;->e(FFF)Z

    move-result v0

    .line 1054703
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/android/maps/MapView;->A:Z

    if-eqz v0, :cond_0

    .line 1054704
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->I:Lcom/facebook/android/maps/internal/AutoAnimationsHelper;

    .line 1054705
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, p1, v1

    iput v1, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->n:F

    .line 1054706
    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->o:J

    .line 1054707
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/android/maps/MapView;->invalidate()V

    .line 1054708
    :cond_1
    return-void
.end method

.method public final a(FFFF)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/high16 v1, 0x3f800000    # 1.0f

    .line 1054683
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    invoke-virtual {v0, p1, p2}, LX/67m;->d(FF)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1054684
    :cond_0
    :goto_0
    return-void

    .line 1054685
    :cond_1
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->w:LX/68Q;

    .line 1054686
    iget-boolean v2, v0, LX/68Q;->c:Z

    move v0, v2

    .line 1054687
    if-eqz v0, :cond_0

    .line 1054688
    invoke-virtual {p0, v6}, Lcom/facebook/android/maps/MapView;->requestDisallowInterceptTouchEvent(Z)V

    .line 1054689
    invoke-direct {p0}, Lcom/facebook/android/maps/MapView;->e()V

    .line 1054690
    invoke-direct {p0, p3, p4}, Lcom/facebook/android/maps/MapView;->h(FF)V

    .line 1054691
    invoke-virtual {p0}, Lcom/facebook/android/maps/MapView;->invalidate()V

    .line 1054692
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->g:LX/67n;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_3

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/android/maps/MapView;->L:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0xc8

    cmp-long v2, v2, v4

    if-ltz v2, :cond_4

    .line 1054693
    iget-object v2, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v2}, LX/680;->q()V

    .line 1054694
    iput-wide v0, p0, Lcom/facebook/android/maps/MapView;->L:J

    .line 1054695
    :cond_4
    iput-boolean v6, p0, Lcom/facebook/android/maps/MapView;->F:Z

    goto :goto_0
.end method

.method public final a(LX/68J;)V
    .locals 1

    .prologue
    .line 1054677
    iget-boolean v0, p0, Lcom/facebook/android/maps/MapView;->y:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->J:Ljava/util/Queue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->J:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1054678
    :cond_0
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-interface {p1, v0}, LX/68J;->a(LX/680;)V

    .line 1054679
    :goto_0
    return-void

    .line 1054680
    :cond_1
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->J:Ljava/util/Queue;

    if-nez v0, :cond_2

    .line 1054681
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->J:Ljava/util/Queue;

    .line 1054682
    :cond_2
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->J:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1054657
    new-instance v0, LX/680;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->u:LX/681;

    invoke-direct {v0, p0, v1}, LX/680;-><init>(Lcom/facebook/android/maps/MapView;LX/681;)V

    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    .line 1054658
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->u:LX/681;

    .line 1054659
    iget-object v1, v0, LX/681;->a:LX/692;

    move-object v0, v1

    .line 1054660
    if-nez v0, :cond_0

    .line 1054661
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v0, v0, LX/680;->b:F

    float-to-int v0, v0

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v1, v1, LX/680;->b:F

    rem-float/2addr v1, v3

    add-float/2addr v1, v3

    invoke-direct {p0, v0, v1}, Lcom/facebook/android/maps/MapView;->a(IF)V

    .line 1054662
    :goto_0
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    .line 1054663
    iget-object v1, v0, LX/680;->E:LX/68Q;

    move-object v0, v1

    .line 1054664
    iput-object v0, p0, Lcom/facebook/android/maps/MapView;->w:LX/68Q;

    .line 1054665
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/facebook/android/maps/MapView;->h:F

    iget v2, p0, Lcom/facebook/android/maps/MapView;->h:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 1054666
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/facebook/android/maps/MapView;->j:F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1054667
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1054668
    invoke-direct {p0, p1}, Lcom/facebook/android/maps/MapView;->c(Landroid/os/Bundle;)V

    .line 1054669
    return-void

    .line 1054670
    :cond_0
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->u:LX/681;

    .line 1054671
    iget-object v1, v0, LX/681;->a:LX/692;

    move-object v0, v1

    .line 1054672
    iget v1, v0, LX/692;->b:F

    float-to-int v1, v1

    iget v2, v0, LX/692;->b:F

    rem-float/2addr v2, v3

    add-float/2addr v2, v3

    invoke-direct {p0, v1, v2}, Lcom/facebook/android/maps/MapView;->a(IF)V

    .line 1054673
    iget-object v1, v0, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    if-eqz v1, :cond_1

    .line 1054674
    iget-object v1, v0, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v2, v1, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v2, v3}, LX/31h;->d(D)F

    move-result v1

    float-to-double v2, v1

    iput-wide v2, p0, Lcom/facebook/android/maps/MapView;->m:D

    .line 1054675
    iget-object v1, v0, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v2, v1, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v2, v3}, LX/31h;->b(D)F

    move-result v1

    float-to-double v2, v1

    iput-wide v2, p0, Lcom/facebook/android/maps/MapView;->n:D

    .line 1054676
    :cond_1
    iget v0, v0, LX/692;->d:F

    iput v0, p0, Lcom/facebook/android/maps/MapView;->j:F

    goto :goto_0
.end method

.method public final a(F)Z
    .locals 3

    .prologue
    .line 1054463
    iget v0, p0, Lcom/facebook/android/maps/MapView;->j:F

    add-float/2addr v0, p1

    iget v1, p0, Lcom/facebook/android/maps/MapView;->C:F

    iget v2, p0, Lcom/facebook/android/maps/MapView;->D:F

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/android/maps/MapView;->d(FFF)V

    .line 1054464
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1054465
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v1, v1, LX/680;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1054466
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67m;

    .line 1054467
    instance-of v3, v0, LX/68P;

    if-eqz v3, :cond_0

    .line 1054468
    check-cast v0, LX/68P;

    invoke-virtual {v0}, LX/68P;->q()V

    .line 1054469
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1054470
    :cond_1
    invoke-static {}, LX/68r;->b()V

    .line 1054471
    return-void
.end method

.method public final b(FF)V
    .locals 1

    .prologue
    .line 1054367
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->q:Z

    .line 1054368
    iget-boolean v0, p0, Lcom/facebook/android/maps/MapView;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->I:Lcom/facebook/android/maps/internal/AutoAnimationsHelper;

    .line 1054369
    iget-boolean p1, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->e:Z

    move v0, p1

    .line 1054370
    if-nez v0, :cond_0

    .line 1054371
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v0}, LX/680;->q()V

    .line 1054372
    :cond_0
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    if-eqz v0, :cond_1

    .line 1054373
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    invoke-virtual {v0}, LX/67m;->n()V

    .line 1054374
    :cond_1
    return-void
.end method

.method public final b(FFF)V
    .locals 4

    .prologue
    .line 1054350
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->w:LX/68Q;

    .line 1054351
    iget-boolean v1, v0, LX/68Q;->b:Z

    move v0, v1

    .line 1054352
    if-eqz v0, :cond_1

    .line 1054353
    iget-boolean v0, p0, Lcom/facebook/android/maps/MapView;->N:Z

    if-eqz v0, :cond_2

    .line 1054354
    invoke-direct {p0}, Lcom/facebook/android/maps/MapView;->e()V

    .line 1054355
    iput p2, p0, Lcom/facebook/android/maps/MapView;->C:F

    .line 1054356
    iput p3, p0, Lcom/facebook/android/maps/MapView;->D:F

    .line 1054357
    iget v0, p0, Lcom/facebook/android/maps/MapView;->j:F

    add-float/2addr v0, p1

    invoke-virtual {p0, v0, p2, p3}, Lcom/facebook/android/maps/MapView;->d(FFF)V

    .line 1054358
    iget-boolean v0, p0, Lcom/facebook/android/maps/MapView;->A:Z

    if-eqz v0, :cond_0

    .line 1054359
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->I:Lcom/facebook/android/maps/internal/AutoAnimationsHelper;

    .line 1054360
    iput p1, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->p:F

    .line 1054361
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->q:J

    .line 1054362
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/android/maps/MapView;->invalidate()V

    .line 1054363
    :cond_1
    :goto_0
    return-void

    .line 1054364
    :cond_2
    iget v0, p0, Lcom/facebook/android/maps/MapView;->M:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/facebook/android/maps/MapView;->M:F

    .line 1054365
    iget v0, p0, Lcom/facebook/android/maps/MapView;->M:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x41000000    # 8.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1054366
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->N:Z

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 1054342
    iget-boolean v0, p0, Lcom/facebook/android/maps/MapView;->P:Z

    if-eqz v0, :cond_0

    .line 1054343
    :goto_0
    return-void

    .line 1054344
    :cond_0
    const-string v0, "xVisibleCenter"

    iget-wide v2, p0, Lcom/facebook/android/maps/MapView;->m:D

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v1, v1, LX/680;->c:I

    iget-object v4, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v4, v4, LX/680;->e:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    iget-wide v6, p0, Lcom/facebook/android/maps/MapView;->r:J

    shl-long/2addr v6, v8

    div-long/2addr v4, v6

    long-to-double v4, v4

    add-double/2addr v2, v4

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 1054345
    const-string v0, "yVisibleCenter"

    iget-wide v2, p0, Lcom/facebook/android/maps/MapView;->n:D

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v1, v1, LX/680;->d:I

    iget-object v4, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v4, v4, LX/680;->f:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    iget-wide v6, p0, Lcom/facebook/android/maps/MapView;->r:J

    shl-long/2addr v6, v8

    div-long/2addr v4, v6

    long-to-double v4, v4

    add-double/2addr v2, v4

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 1054346
    const-string v0, "zoom"

    iget v1, p0, Lcom/facebook/android/maps/MapView;->g:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1054347
    const-string v0, "scale"

    iget v1, p0, Lcom/facebook/android/maps/MapView;->h:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 1054348
    const-string v0, "rotation"

    iget v1, p0, Lcom/facebook/android/maps/MapView;->j:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 1054349
    iput-boolean v8, p0, Lcom/facebook/android/maps/MapView;->P:Z

    goto :goto_0
.end method

.method public final b(F)Z
    .locals 2

    .prologue
    .line 1054341
    iget v0, p0, Lcom/facebook/android/maps/MapView;->C:F

    iget v1, p0, Lcom/facebook/android/maps/MapView;->D:F

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/android/maps/MapView;->e(FFF)Z

    move-result v0

    return v0
.end method

.method public final c(FF)V
    .locals 2

    .prologue
    .line 1054332
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->q:Z

    .line 1054333
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    invoke-virtual {v0, p1, p2}, LX/67m;->b(FF)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1054334
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    invoke-virtual {v0, v1}, LX/680;->c(LX/67m;)V

    .line 1054335
    :cond_0
    :goto_0
    return-void

    .line 1054336
    :cond_1
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/680;->c(LX/67m;)V

    .line 1054337
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->p:LX/6ad;

    if-eqz v0, :cond_0

    .line 1054338
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->p:LX/6ad;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v1, v1, LX/680;->k:LX/31h;

    invoke-virtual {v1, p1, p2}, LX/31h;->a(FF)Lcom/facebook/android/maps/model/LatLng;

    .line 1054339
    iget-object p0, v0, LX/6ad;->a:LX/6Zu;

    invoke-interface {p0}, LX/6Zu;->a()V

    .line 1054340
    goto :goto_0
.end method

.method public final c(FFF)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    .line 1054316
    iget-object v2, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v2, v2, LX/680;->k:LX/31h;

    iget-object v3, p0, Lcom/facebook/android/maps/MapView;->G:[F

    invoke-virtual {v2, p2, p3, v3}, LX/31h;->a(FF[F)V

    .line 1054317
    iget-object v2, p0, Lcom/facebook/android/maps/MapView;->G:[F

    aget v2, v2, v1

    .line 1054318
    iget-object v3, p0, Lcom/facebook/android/maps/MapView;->G:[F

    aget v3, v3, v0

    .line 1054319
    iget-object v4, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v4, v4, LX/680;->b:F

    invoke-static {p1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iget-object v5, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v5, v5, LX/680;->a:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 1054320
    float-to-int v5, v4

    .line 1054321
    rem-float/2addr v4, v8

    add-float/2addr v4, v8

    .line 1054322
    iget v6, p0, Lcom/facebook/android/maps/MapView;->g:I

    .line 1054323
    iget v7, p0, Lcom/facebook/android/maps/MapView;->h:F

    div-float v7, v4, v7

    .line 1054324
    invoke-direct {p0, v5, v4}, Lcom/facebook/android/maps/MapView;->a(IF)V

    .line 1054325
    iget-object v4, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v4, v7, v7, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1054326
    iget-object v4, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    iget-object v5, p0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1054327
    invoke-direct {p0}, Lcom/facebook/android/maps/MapView;->j()V

    .line 1054328
    invoke-direct {p0, p2, p3, v2, v3}, Lcom/facebook/android/maps/MapView;->b(FFFF)V

    .line 1054329
    iget v2, p0, Lcom/facebook/android/maps/MapView;->g:I

    if-eq v2, v6, :cond_0

    .line 1054330
    iget-object v2, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v2}, LX/680;->q()V

    .line 1054331
    :cond_0
    cmpl-float v2, v7, v8

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 1054310
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->q:Z

    .line 1054311
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->w:LX/68Q;

    .line 1054312
    iget-boolean v1, v0, LX/68Q;->e:Z

    move v0, v1

    .line 1054313
    if-eqz v0, :cond_0

    .line 1054314
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-static {}, LX/67e;->b()LX/67d;

    move-result-object v1

    const/16 v2, 0xc8

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/680;->a(LX/67d;ILX/6aX;)V

    .line 1054315
    :cond_0
    return-void
.end method

.method public final d(FF)V
    .locals 1

    .prologue
    .line 1054305
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    invoke-virtual {v0, p1, p2}, LX/67m;->e(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1054306
    :goto_0
    return-void

    .line 1054307
    :cond_0
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->q:LX/67x;

    if-eqz v0, :cond_1

    .line 1054308
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->k:LX/31h;

    invoke-virtual {v0, p1, p2}, LX/31h;->a(FF)Lcom/facebook/android/maps/model/LatLng;

    .line 1054309
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->q:Z

    goto :goto_0
.end method

.method public final d(FFF)V
    .locals 4

    .prologue
    .line 1054295
    iget-boolean v0, p0, Lcom/facebook/android/maps/MapView;->z:Z

    if-eqz v0, :cond_0

    .line 1054296
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->k:LX/31h;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->G:[F

    invoke-virtual {v0, p2, p3, v1}, LX/31h;->a(FF[F)V

    .line 1054297
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->G:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 1054298
    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->G:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 1054299
    iget-object v2, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/facebook/android/maps/MapView;->j:F

    sub-float v3, p1, v3

    invoke-virtual {v2, v3, p2, p3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1054300
    iget-object v2, p0, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1054301
    const/high16 v2, 0x43b40000    # 360.0f

    rem-float v2, p1, v2

    iput v2, p0, Lcom/facebook/android/maps/MapView;->j:F

    .line 1054302
    invoke-direct {p0}, Lcom/facebook/android/maps/MapView;->j()V

    .line 1054303
    invoke-direct {p0, p2, p3, v0, v1}, Lcom/facebook/android/maps/MapView;->b(FFFF)V

    .line 1054304
    :cond_0
    return-void
.end method

.method public final e(FF)V
    .locals 4

    .prologue
    .line 1054286
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->q:Z

    .line 1054287
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->K:LX/67m;

    invoke-virtual {v0, p1, p2}, LX/67m;->c(FF)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1054288
    :cond_0
    :goto_0
    return-void

    .line 1054289
    :cond_1
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->r:LX/67v;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->r:LX/67v;

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v1, v1, LX/680;->k:LX/31h;

    invoke-virtual {v1, p1, p2}, LX/31h;->a(FF)Lcom/facebook/android/maps/model/LatLng;

    invoke-interface {v0}, LX/67v;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1054290
    :cond_2
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->w:LX/68Q;

    .line 1054291
    iget-boolean v1, v0, LX/68Q;->e:Z

    move v0, v1

    .line 1054292
    if-eqz v0, :cond_0

    .line 1054293
    new-instance v0, Landroid/graphics/Point;

    float-to-int v1, p1

    float-to-int v2, p2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 1054294
    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v2, v0}, LX/67e;->a(FLandroid/graphics/Point;)LX/67d;

    move-result-object v0

    const/16 v2, 0xc8

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/680;->a(LX/67d;ILX/6aX;)V

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1054278
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->w:LX/68Q;

    .line 1054279
    iget-boolean v1, v0, LX/68Q;->e:Z

    move v0, v1

    .line 1054280
    if-eqz v0, :cond_0

    .line 1054281
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->q:Z

    .line 1054282
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->I:Lcom/facebook/android/maps/internal/AutoAnimationsHelper;

    .line 1054283
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->j:Z

    .line 1054284
    invoke-direct {p0}, Lcom/facebook/android/maps/MapView;->k()V

    .line 1054285
    :cond_0
    return-void
.end method

.method public final f(FF)V
    .locals 5

    .prologue
    .line 1054271
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->w:LX/68Q;

    .line 1054272
    iget-boolean v1, v0, LX/68Q;->c:Z

    move v0, v1

    .line 1054273
    if-eqz v0, :cond_0

    .line 1054274
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->q:Z

    .line 1054275
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->I:Lcom/facebook/android/maps/internal/AutoAnimationsHelper;

    iget v1, p0, Lcom/facebook/android/maps/MapView;->c:I

    iget v2, p0, Lcom/facebook/android/maps/MapView;->d:I

    float-to-int v3, p1

    float-to-int v4, p2

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->a(IIII)V

    .line 1054276
    invoke-direct {p0}, Lcom/facebook/android/maps/MapView;->k()V

    .line 1054277
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1054263
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->w:LX/68Q;

    .line 1054264
    iget-boolean v1, v0, LX/68Q;->b:Z

    move v0, v1

    .line 1054265
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/android/maps/MapView;->N:Z

    if-eqz v0, :cond_0

    .line 1054266
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->q:Z

    .line 1054267
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->I:Lcom/facebook/android/maps/internal/AutoAnimationsHelper;

    .line 1054268
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->k:Z

    .line 1054269
    invoke-direct {p0}, Lcom/facebook/android/maps/MapView;->k()V

    .line 1054270
    :cond_0
    return-void
.end method

.method public final g(FF)Z
    .locals 1

    .prologue
    .line 1054375
    invoke-direct {p0, p1, p2}, Lcom/facebook/android/maps/MapView;->h(FF)V

    .line 1054376
    const/4 v0, 0x1

    return v0
.end method

.method public final getMap()LX/680;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1054462
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    return-object v0
.end method

.method public getZoom()F
    .locals 2

    .prologue
    .line 1054377
    iget v0, p0, Lcom/facebook/android/maps/MapView;->g:I

    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/android/maps/MapView;->h:F

    add-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v0, v1

    return v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1054378
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v0}, LX/680;->q()V

    .line 1054379
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 1054380
    invoke-virtual {p0}, Lcom/facebook/android/maps/MapView;->invalidate()V

    .line 1054381
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x3abfbcf6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1054382
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 1054383
    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    if-nez v1, :cond_0

    .line 1054384
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "MapView.onCreate() must be called!"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x2d

    const v3, -0x1e9370b2

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1

    .line 1054385
    :cond_0
    invoke-direct {p0}, Lcom/facebook/android/maps/MapView;->a()V

    .line 1054386
    sget-object v1, LX/31U;->z:LX/31U;

    invoke-virtual {v1}, LX/31U;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1054387
    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->u:LX/681;

    .line 1054388
    iget-object v2, v1, LX/681;->m:Ljava/lang/String;

    move-object v1, v2

    .line 1054389
    sget-object v2, LX/31U;->z:LX/31U;

    new-instance v3, LX/68E;

    invoke-direct {v3, p0, v1}, LX/68E;-><init>(Lcom/facebook/android/maps/MapView;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/31U;->a(Ljava/util/Map;)V

    .line 1054390
    :cond_1
    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/android/maps/MapView;->R:J

    .line 1054391
    iget-boolean v1, p0, Lcom/facebook/android/maps/MapView;->S:Z

    if-nez v1, :cond_2

    .line 1054392
    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v1, v1, LX/680;->l:LX/68m;

    .line 1054393
    iget-object v2, v1, LX/68m;->A:LX/68n;

    invoke-virtual {v2}, LX/68W;->b()V

    .line 1054394
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/android/maps/MapView;->S:Z

    .line 1054395
    :cond_2
    const v1, -0x2d63ed37

    invoke-static {v1, v0}, LX/02F;->g(II)V

    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1054396
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1054397
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v0}, LX/680;->t()V

    .line 1054398
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x53e0e663

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1054399
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 1054400
    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    .line 1054401
    iget-object v2, v1, LX/680;->w:LX/68p;

    if-eqz v2, :cond_0

    .line 1054402
    iget-object v2, v1, LX/680;->w:LX/68p;

    invoke-virtual {v2}, LX/68p;->p()V

    .line 1054403
    :cond_0
    invoke-virtual {v1}, LX/680;->t()V

    .line 1054404
    invoke-static {}, LX/68r;->b()V

    .line 1054405
    invoke-direct {p0}, Lcom/facebook/android/maps/MapView;->c()V

    .line 1054406
    const/16 v1, 0x2d

    const v2, -0x6cd95da3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 1054407
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/android/maps/MapView;->a(Landroid/graphics/Canvas;Z)V

    .line 1054408
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 8

    .prologue
    .line 1054409
    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    .line 1054410
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 1054411
    invoke-virtual {p0}, Lcom/facebook/android/maps/MapView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/facebook/android/maps/MapView;->c:I

    .line 1054412
    invoke-virtual {p0}, Lcom/facebook/android/maps/MapView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/android/maps/MapView;->d:I

    .line 1054413
    iget v0, p0, Lcom/facebook/android/maps/MapView;->c:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/android/maps/MapView;->e:F

    .line 1054414
    iget v0, p0, Lcom/facebook/android/maps/MapView;->d:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/android/maps/MapView;->f:F

    .line 1054415
    const/4 v0, 0x0

    .line 1054416
    iget v1, p0, Lcom/facebook/android/maps/MapView;->d:I

    iget v4, p0, Lcom/facebook/android/maps/MapView;->c:I

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-double v4, v1

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v4, v6

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    .line 1054417
    iget v6, v1, LX/680;->B:I

    move v1, v6

    .line 1054418
    int-to-double v6, v1

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v1, v4

    .line 1054419
    int-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    sget-wide v6, Lcom/facebook/android/maps/MapView;->a:D

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-float v1, v4

    iput v1, p0, Lcom/facebook/android/maps/MapView;->i:F

    .line 1054420
    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    .line 1054421
    iget v4, v1, LX/680;->b:F

    iget-object v5, v1, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v5, v5, Lcom/facebook/android/maps/MapView;->i:F

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iput v4, v1, LX/680;->b:F

    .line 1054422
    iget v1, p0, Lcom/facebook/android/maps/MapView;->g:I

    int-to-float v1, v1

    iget v4, p0, Lcom/facebook/android/maps/MapView;->h:F

    add-float/2addr v1, v4

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v1, v4

    iget-object v4, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v4, v4, LX/680;->b:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_0

    .line 1054423
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v0, v0, LX/680;->b:F

    float-to-int v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v4, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget v4, v4, LX/680;->b:F

    const/high16 v5, 0x3f800000    # 1.0f

    rem-float/2addr v4, v5

    add-float/2addr v1, v4

    invoke-direct {p0, v0, v1}, Lcom/facebook/android/maps/MapView;->a(IF)V

    .line 1054424
    const/4 v0, 0x1

    .line 1054425
    :cond_0
    invoke-direct {p0}, Lcom/facebook/android/maps/MapView;->j()V

    .line 1054426
    iget-boolean v1, p0, Lcom/facebook/android/maps/MapView;->y:Z

    if-nez v1, :cond_1

    .line 1054427
    iget v0, p0, Lcom/facebook/android/maps/MapView;->g:I

    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/android/maps/MapView;->h:F

    add-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v1}, LX/680;->k()F

    move-result v1

    iget-object v4, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v4}, LX/680;->l()F

    move-result v4

    invoke-virtual {p0, v0, v1, v4}, Lcom/facebook/android/maps/MapView;->c(FFF)Z

    .line 1054428
    iget-wide v0, p0, Lcom/facebook/android/maps/MapView;->m:D

    iget-wide v4, p0, Lcom/facebook/android/maps/MapView;->n:D

    invoke-virtual {p0, v0, v1, v4, v5}, Lcom/facebook/android/maps/MapView;->a(DD)V

    .line 1054429
    iget v0, p0, Lcom/facebook/android/maps/MapView;->j:F

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v1}, LX/680;->k()F

    move-result v1

    iget-object v4, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v4}, LX/680;->l()F

    move-result v4

    invoke-virtual {p0, v0, v1, v4}, Lcom/facebook/android/maps/MapView;->d(FFF)V

    .line 1054430
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/android/maps/MapView;->y:Z

    .line 1054431
    const/4 v0, 0x1

    .line 1054432
    :cond_1
    if-eqz v0, :cond_2

    .line 1054433
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    invoke-virtual {v0}, LX/680;->q()V

    .line 1054434
    :cond_2
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v1, v1, LX/680;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    .line 1054435
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->v:LX/680;

    iget-object v0, v0, LX/680;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67m;

    invoke-virtual {v0}, LX/67m;->b()V

    .line 1054436
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1054437
    :cond_3
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->J:Ljava/util/Queue;

    if-eqz v0, :cond_4

    .line 1054438
    new-instance v0, Lcom/facebook/android/maps/MapView$3;

    invoke-direct {v0, p0}, Lcom/facebook/android/maps/MapView$3;-><init>(Lcom/facebook/android/maps/MapView;)V

    invoke-static {v0}, LX/31l;->c(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V

    .line 1054439
    :cond_4
    sget-object v0, LX/31U;->b:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v0, v2, v3}, LX/31U;->a(J)V

    .line 1054440
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1054441
    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "zoom"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1054442
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1054443
    :goto_0
    return-void

    .line 1054444
    :cond_1
    const-string v1, "parentBundle"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1054445
    const-string v1, "parentBundle"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1054446
    :cond_2
    invoke-direct {p0, v0}, Lcom/facebook/android/maps/MapView;->c(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 1054447
    iget-boolean v0, p0, Lcom/facebook/android/maps/MapView;->P:Z

    if-eqz v0, :cond_0

    .line 1054448
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1054449
    :goto_0
    return-object v0

    .line 1054450
    :cond_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 1054451
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1054452
    invoke-virtual {p0, v0}, Lcom/facebook/android/maps/MapView;->b(Landroid/os/Bundle;)V

    .line 1054453
    const-string v2, "parentBundle"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x3b5c052c

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1054454
    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    .line 1054455
    :try_start_0
    iget-object v0, p0, Lcom/facebook/android/maps/MapView;->E:LX/68j;

    invoke-virtual {v0, p1}, LX/68j;->a(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1054456
    sget-object v4, LX/31U;->c:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v6

    sub-long v2, v6, v2

    invoke-virtual {v4, v2, v3}, LX/31U;->a(J)V

    const v2, 0x13c18dcc

    invoke-static {v5, v5, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v0

    :catchall_0
    move-exception v0

    sget-object v4, LX/31U;->c:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v6

    sub-long v2, v6, v2

    invoke-virtual {v4, v2, v3}, LX/31U;->a(J)V

    const v2, -0x75cc63fd

    invoke-static {v2, v1}, LX/02F;->a(II)V

    throw v0
.end method

.method public final onWindowVisibilityChanged(I)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x5b7a9f6c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1054457
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowVisibilityChanged(I)V

    .line 1054458
    if-nez p1, :cond_0

    .line 1054459
    invoke-direct {p0}, Lcom/facebook/android/maps/MapView;->a()V

    .line 1054460
    :goto_0
    const v1, 0x633410b9

    invoke-static {v1, v0}, LX/02F;->g(II)V

    return-void

    .line 1054461
    :cond_0
    invoke-direct {p0}, Lcom/facebook/android/maps/MapView;->c()V

    goto :goto_0
.end method
