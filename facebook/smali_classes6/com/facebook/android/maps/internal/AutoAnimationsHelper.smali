.class public Lcom/facebook/android/maps/internal/AutoAnimationsHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:Landroid/view/View;

.field private final b:LX/68F;

.field private final c:Landroid/widget/OverScroller;

.field public d:Z

.field public e:Z

.field public f:Z

.field private g:Z

.field private h:F

.field private i:F

.field public j:Z

.field public k:Z

.field private l:[F

.field public m:Landroid/graphics/Matrix;

.field public n:F

.field public o:J

.field public p:F

.field public q:J


# direct methods
.method public constructor <init>(Landroid/view/View;LX/68F;)V
    .locals 2

    .prologue
    .line 1055226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1055227
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->l:[F

    .line 1055228
    iput-object p1, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->a:Landroid/view/View;

    .line 1055229
    iput-object p2, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->b:LX/68F;

    .line 1055230
    new-instance v0, Landroid/widget/OverScroller;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->c:Landroid/widget/OverScroller;

    .line 1055231
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 1055232
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->c:Landroid/widget/OverScroller;

    const v1, 0x3d0f5c29    # 0.035f

    invoke-virtual {v0, v1}, Landroid/widget/OverScroller;->setFriction(F)V

    .line 1055233
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;Ljava/lang/Runnable;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1055234
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1055235
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->postOnAnimation(Ljava/lang/Runnable;)V

    .line 1055236
    :goto_0
    return-void

    .line 1055237
    :cond_0
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->a:Landroid/view/View;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public static h(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;)V
    .locals 1

    .prologue
    .line 1055238
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->n:F

    .line 1055239
    return-void
.end method

.method public static i(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;)V
    .locals 2

    .prologue
    .line 1055240
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->c:Landroid/widget/OverScroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/OverScroller;->forceFinished(Z)V

    .line 1055241
    return-void
.end method

.method public static j(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;)V
    .locals 1

    .prologue
    .line 1055242
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->p:F

    .line 1055243
    return-void
.end method


# virtual methods
.method public final a(IIII)V
    .locals 9

    .prologue
    const v6, 0x7fffffff

    const/high16 v5, -0x80000000

    .line 1055244
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->c:Landroid/widget/OverScroller;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v7, v5

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/OverScroller;->fling(IIIIIIII)V

    .line 1055245
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->g:Z

    .line 1055246
    return-void
.end method

.method public final run()V
    .locals 15

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1055247
    iget-boolean v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->f:Z

    if-eqz v0, :cond_0

    .line 1055248
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->b:LX/68F;

    invoke-interface {v0}, LX/68F;->h()V

    .line 1055249
    :goto_0
    return-void

    .line 1055250
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->d:Z

    if-nez v0, :cond_2

    .line 1055251
    iget-boolean v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->k:Z

    if-eqz v0, :cond_9

    .line 1055252
    invoke-static {p0}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->i(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;)V

    .line 1055253
    invoke-static {p0}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->h(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;)V

    .line 1055254
    :cond_1
    :goto_1
    iput-boolean v2, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->j:Z

    .line 1055255
    iput-boolean v2, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->k:Z

    .line 1055256
    iput-boolean v1, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->d:Z

    .line 1055257
    :cond_2
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->c:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1055258
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->l:[F

    iget-object v3, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->c:Landroid/widget/OverScroller;

    invoke-virtual {v3}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v3

    int-to-float v3, v3

    aput v3, v0, v2

    .line 1055259
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->l:[F

    iget-object v3, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->c:Landroid/widget/OverScroller;

    invoke-virtual {v3}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v3

    int-to-float v3, v3

    aput v3, v0, v1

    .line 1055260
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->m:Landroid/graphics/Matrix;

    if-eqz v0, :cond_3

    .line 1055261
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->m:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->l:[F

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1055262
    :cond_3
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->l:[F

    aget v0, v0, v2

    .line 1055263
    iget-object v3, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->l:[F

    aget v3, v3, v1

    .line 1055264
    iget-boolean v4, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->g:Z

    if-eqz v4, :cond_4

    .line 1055265
    iput-boolean v2, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->g:Z

    .line 1055266
    iput v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->h:F

    .line 1055267
    iput v3, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->i:F

    .line 1055268
    :cond_4
    iget-object v4, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->b:LX/68F;

    iget v5, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->h:F

    sub-float v5, v0, v5

    iget v6, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->i:F

    sub-float v6, v3, v6

    invoke-interface {v4, v5, v6}, LX/68F;->g(FF)Z

    move-result v4

    .line 1055269
    iput v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->h:F

    .line 1055270
    iput v3, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->i:F

    .line 1055271
    if-eqz v4, :cond_a

    move v0, v1

    .line 1055272
    :goto_2
    const/4 v8, 0x1

    .line 1055273
    iget v7, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->n:F

    float-to-double v9, v7

    const-wide v11, -0x407b851eb851eb85L    # -0.01

    cmpg-double v7, v9, v11

    if-ltz v7, :cond_5

    iget v7, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->n:F

    float-to-double v9, v7

    const-wide v11, 0x3f847ae147ae147bL    # 0.01

    cmpl-double v7, v9, v11

    if-lez v7, :cond_10

    .line 1055274
    :cond_5
    iget-wide v9, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->o:J

    const-wide/16 v11, 0x0

    cmp-long v7, v9, v11

    if-nez v7, :cond_f

    .line 1055275
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    iput-wide v9, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->o:J

    move v7, v8

    .line 1055276
    :goto_3
    iget v9, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->n:F

    float-to-double v9, v9

    const-wide v11, 0x3fed47ae20000000L    # 0.9150000214576721

    int-to-double v13, v7

    invoke-static {v11, v12, v13, v14}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v11

    mul-double/2addr v9, v11

    double-to-float v7, v9

    iput v7, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->n:F

    .line 1055277
    :goto_4
    move v3, v8

    .line 1055278
    if-eqz v3, :cond_6

    .line 1055279
    iget-object v3, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->b:LX/68F;

    const/high16 v4, 0x3f800000    # 1.0f

    iget v5, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->n:F

    add-float/2addr v4, v5

    invoke-interface {v3, v4}, LX/68F;->b(F)Z

    move-result v3

    .line 1055280
    if-eqz v3, :cond_c

    move v0, v1

    .line 1055281
    :cond_6
    :goto_5
    const/4 v8, 0x1

    .line 1055282
    iget v7, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->p:F

    float-to-double v9, v7

    const-wide v11, -0x407b851eb851eb85L    # -0.01

    cmpg-double v7, v9, v11

    if-ltz v7, :cond_7

    iget v7, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->p:F

    float-to-double v9, v7

    const-wide v11, 0x3f847ae147ae147bL    # 0.01

    cmpl-double v7, v9, v11

    if-lez v7, :cond_12

    .line 1055283
    :cond_7
    iget-wide v9, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->q:J

    const-wide/16 v11, 0x0

    cmp-long v7, v9, v11

    if-nez v7, :cond_11

    .line 1055284
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    iput-wide v9, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->q:J

    move v7, v8

    .line 1055285
    :goto_6
    iget v9, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->p:F

    float-to-double v9, v9

    const-wide v11, 0x3feb333340000000L    # 0.8500000238418579

    int-to-double v13, v7

    invoke-static {v11, v12, v13, v14}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v11

    mul-double/2addr v9, v11

    double-to-float v7, v9

    iput v7, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->p:F

    .line 1055286
    :goto_7
    move v3, v8

    .line 1055287
    if-eqz v3, :cond_8

    .line 1055288
    iget-object v3, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->b:LX/68F;

    iget v4, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->p:F

    invoke-interface {v3, v4}, LX/68F;->a(F)Z

    move-result v3

    .line 1055289
    if-eqz v3, :cond_d

    move v0, v1

    .line 1055290
    :cond_8
    :goto_8
    if-eqz v0, :cond_e

    .line 1055291
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 1055292
    invoke-static {p0, p0}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->a(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 1055293
    :cond_9
    iget-boolean v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->j:Z

    if-eqz v0, :cond_1

    .line 1055294
    invoke-static {p0}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->i(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;)V

    .line 1055295
    invoke-static {p0}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->j(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;)V

    goto/16 :goto_1

    .line 1055296
    :cond_a
    invoke-static {p0}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->i(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;)V

    :cond_b
    move v0, v2

    goto/16 :goto_2

    .line 1055297
    :cond_c
    invoke-static {p0}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->h(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;)V

    goto :goto_5

    .line 1055298
    :cond_d
    invoke-static {p0}, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->j(Lcom/facebook/android/maps/internal/AutoAnimationsHelper;)V

    goto :goto_8

    .line 1055299
    :cond_e
    iput-boolean v2, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->d:Z

    .line 1055300
    iput-boolean v2, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->e:Z

    .line 1055301
    iget-object v0, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->b:LX/68F;

    invoke-interface {v0}, LX/68F;->h()V

    goto/16 :goto_0

    .line 1055302
    :cond_f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    iget-wide v11, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->o:J

    sub-long/2addr v9, v11

    long-to-int v7, v9

    div-int/lit8 v7, v7, 0xa

    .line 1055303
    iget-wide v9, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->o:J

    mul-int/lit8 v11, v7, 0xa

    int-to-long v11, v11

    add-long/2addr v9, v11

    iput-wide v9, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->o:J

    goto/16 :goto_3

    .line 1055304
    :cond_10
    const/4 v8, 0x0

    goto/16 :goto_4

    .line 1055305
    :cond_11
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    iget-wide v11, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->q:J

    sub-long/2addr v9, v11

    long-to-int v7, v9

    div-int/lit8 v7, v7, 0xa

    .line 1055306
    iget-wide v9, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->q:J

    mul-int/lit8 v11, v7, 0xa

    int-to-long v11, v11

    add-long/2addr v9, v11

    iput-wide v9, p0, Lcom/facebook/android/maps/internal/AutoAnimationsHelper;->q:J

    goto :goto_6

    .line 1055307
    :cond_12
    const/4 v8, 0x0

    goto :goto_7
.end method
