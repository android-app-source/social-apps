.class public final Lcom/facebook/android/maps/internal/CacheableUrlTileProvider$1;
.super Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/68W;


# direct methods
.method public constructor <init>(LX/68W;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1055311
    iput-object p1, p0, Lcom/facebook/android/maps/internal/CacheableUrlTileProvider$1;->b:LX/68W;

    iput-object p2, p0, Lcom/facebook/android/maps/internal/CacheableUrlTileProvider$1;->a:Landroid/content/Context;

    invoke-direct {p0}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1055312
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Environment;->isExternalStorageRemovable()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_0
    move v0, v2

    .line 1055313
    :goto_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-lt v3, v4, :cond_1

    iget-object v3, p0, Lcom/facebook/android/maps/internal/CacheableUrlTileProvider$1;->a:Landroid/content/Context;

    const-string v4, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {v3, v4}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/facebook/android/maps/internal/CacheableUrlTileProvider$1;->a:Landroid/content/Context;

    const-string v4, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v3, v4}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    move v1, v2

    .line 1055314
    :cond_2
    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 1055315
    :goto_1
    invoke-virtual {v0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v2

    .line 1055316
    const-wide/16 v4, 0x1e

    cmp-long v1, v2, v4

    if-ltz v1, :cond_3

    .line 1055317
    const-wide/16 v4, 0x64

    cmp-long v1, v2, v4

    if-ltz v1, :cond_6

    const/high16 v1, 0x500000

    .line 1055318
    :goto_2
    new-instance v2, Ljava/io/File;

    const-string v3, ".facebook_cache"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1055319
    const/4 v0, 0x2

    const/4 v3, 0x1

    int-to-long v4, v1

    :try_start_0
    invoke-static {v2, v0, v3, v4, v5}, LX/68h;->a(Ljava/io/File;IIJ)LX/68h;

    move-result-object v0

    .line 1055320
    sput-object v0, LX/68W;->f:LX/68h;

    .line 1055321
    new-instance v0, Lcom/facebook/android/maps/internal/CacheableUrlTileProvider$BackgroundWriter;

    invoke-direct {v0}, Lcom/facebook/android/maps/internal/CacheableUrlTileProvider$BackgroundWriter;-><init>()V

    const v1, 0x53578885

    invoke-static {v0, v1}, LX/00l;->a(Ljava/lang/Runnable;I)Ljava/lang/Thread;

    move-result-object v0

    .line 1055322
    sput-object v0, LX/68W;->e:Ljava/lang/Thread;

    .line 1055323
    sget-object v0, LX/68W;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1055324
    :cond_3
    :goto_3
    return-void

    :cond_4
    move v0, v1

    .line 1055325
    goto :goto_0

    .line 1055326
    :cond_5
    iget-object v0, p0, Lcom/facebook/android/maps/internal/CacheableUrlTileProvider$1;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    goto :goto_1

    .line 1055327
    :cond_6
    const/high16 v1, 0x200000

    goto :goto_2

    .line 1055328
    :catch_0
    move-exception v0

    .line 1055329
    sget-object v1, LX/31U;->p:LX/31U;

    invoke-virtual {v1, v0}, LX/31U;->a(Ljava/lang/Throwable;)V

    goto :goto_3
.end method
