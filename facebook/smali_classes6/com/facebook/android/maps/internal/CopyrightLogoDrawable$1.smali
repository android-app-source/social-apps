.class public final Lcom/facebook/android/maps/internal/CopyrightLogoDrawable$1;
.super Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;
.source ""


# instance fields
.field public final synthetic a:LX/68Y;


# direct methods
.method public constructor <init>(LX/68Y;)V
    .locals 0

    .prologue
    .line 1055631
    iput-object p1, p0, Lcom/facebook/android/maps/internal/CopyrightLogoDrawable$1;->a:LX/68Y;

    invoke-direct {p0}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1055632
    iget-object v0, p0, Lcom/facebook/android/maps/internal/CopyrightLogoDrawable$1;->a:LX/68Y;

    iget-object v0, v0, LX/68Y;->u:Ljava/lang/String;

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1055633
    if-nez v0, :cond_1

    .line 1055634
    :try_start_0
    new-instance v2, Ljava/net/URL;

    iget-object v3, p0, Lcom/facebook/android/maps/internal/CopyrightLogoDrawable$1;->a:LX/68Y;

    iget-object v3, v3, LX/68Y;->t:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->openStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1055635
    :try_start_1
    iget-object v3, p0, Lcom/facebook/android/maps/internal/CopyrightLogoDrawable$1;->a:LX/68Y;

    iget-object v3, v3, LX/67m;->g:Landroid/content/Context;

    const-string v4, "copyright_logo"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 1055636
    const/16 v3, 0x800

    new-array v3, v3, [B

    .line 1055637
    :goto_0
    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 1055638
    const/4 v5, 0x0

    invoke-virtual {v1, v3, v5, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 1055639
    :catch_0
    :goto_1
    if-eqz v2, :cond_0

    .line 1055640
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 1055641
    :cond_0
    :goto_2
    if-eqz v1, :cond_1

    .line 1055642
    :try_start_3
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 1055643
    :cond_1
    :goto_3
    new-instance v1, Lcom/facebook/android/maps/internal/CopyrightLogoDrawable$1$1;

    invoke-direct {v1, p0, v0}, Lcom/facebook/android/maps/internal/CopyrightLogoDrawable$1$1;-><init>(Lcom/facebook/android/maps/internal/CopyrightLogoDrawable$1;Landroid/graphics/Bitmap;)V

    invoke-static {v1}, LX/31l;->c(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V

    .line 1055644
    return-void

    .line 1055645
    :cond_2
    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    .line 1055646
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 1055647
    iget-object v3, p0, Lcom/facebook/android/maps/internal/CopyrightLogoDrawable$1;->a:LX/68Y;

    iget-object v3, v3, LX/68Y;->u:Ljava/lang/String;

    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    .line 1055648
    if-eqz v2, :cond_3

    .line 1055649
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 1055650
    :cond_3
    :goto_4
    if-eqz v1, :cond_1

    .line 1055651
    :try_start_6
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_3

    .line 1055652
    :catch_1
    goto :goto_3

    .line 1055653
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_5
    if-eqz v2, :cond_4

    .line 1055654
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 1055655
    :cond_4
    :goto_6
    if-eqz v1, :cond_5

    .line 1055656
    :try_start_8
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 1055657
    :cond_5
    :goto_7
    throw v0

    :catch_2
    goto :goto_4

    :catch_3
    goto :goto_2

    :catch_4
    goto :goto_3

    :catch_5
    goto :goto_6

    :catch_6
    goto :goto_7

    .line 1055658
    :catchall_1
    move-exception v0

    goto :goto_5

    :catch_7
    move-object v2, v1

    goto :goto_1
.end method
