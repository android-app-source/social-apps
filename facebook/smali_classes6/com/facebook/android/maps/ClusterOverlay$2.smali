.class public final Lcom/facebook/android/maps/ClusterOverlay$2;
.super Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;
.source ""


# instance fields
.field public final synthetic a:LX/67r;


# direct methods
.method public constructor <init>(LX/67r;)V
    .locals 0

    .prologue
    .line 1053453
    iput-object p1, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    invoke-direct {p0}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1053412
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    .line 1053413
    iput-object v7, v0, LX/67r;->B:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 1053414
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    invoke-virtual {v0}, LX/67m;->f()V

    .line 1053415
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v0, v0, LX/67m;->e:LX/680;

    invoke-virtual {v0}, LX/680;->c()LX/692;

    move-result-object v0

    iget v4, v0, LX/692;->b:F

    .line 1053416
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v0, v0, LX/67r;->q:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1053417
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v0, v0, LX/67r;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1053418
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v0, v0, LX/67r;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67h;

    move v1, v2

    .line 1053419
    :goto_1
    iget v5, v0, LX/67h;->b:I

    if-ge v1, v5, :cond_0

    .line 1053420
    iget-object v5, v0, LX/67h;->a:[LX/682;

    aget-object v5, v5, v1

    .line 1053421
    iput-object v0, v5, LX/682;->e:LX/67h;

    .line 1053422
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1053423
    :cond_0
    iget-object v1, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v1, v1, LX/67r;->z:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1053424
    :cond_1
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v1, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v1, v1, LX/67r;->q:Ljava/util/Set;

    invoke-static {v0, v1}, LX/67r;->a$redex0(LX/67r;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 1053425
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67h;

    .line 1053426
    iget-object v1, v0, LX/67h;->q:LX/67m;

    move-object v1, v1

    .line 1053427
    instance-of v3, v1, LX/698;

    if-eqz v3, :cond_2

    .line 1053428
    iget-object v3, v0, LX/67h;->a:[LX/682;

    aget-object v3, v3, v2

    .line 1053429
    iget-object v8, v3, LX/682;->e:LX/67h;

    move-object v3, v8

    .line 1053430
    if-nez v3, :cond_3

    move-object v3, v0

    .line 1053431
    :cond_3
    iput-object v3, v0, LX/67h;->p:LX/67h;

    .line 1053432
    check-cast v1, LX/698;

    .line 1053433
    invoke-virtual {v1, v6}, LX/698;->a(F)V

    .line 1053434
    iget-object v3, v0, LX/67h;->p:LX/67h;

    move-object v3, v3

    .line 1053435
    invoke-virtual {v3}, LX/67h;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/698;->a(Lcom/facebook/android/maps/model/LatLng;)V

    .line 1053436
    iget-object v1, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v1, v1, LX/67r;->A:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1053437
    :cond_4
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v0, v0, LX/67r;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 1053438
    :goto_3
    if-ge v2, v1, :cond_5

    .line 1053439
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v0, v0, LX/67r;->z:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67h;

    .line 1053440
    iput-object v7, v0, LX/67h;->p:LX/67h;

    .line 1053441
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1053442
    :cond_5
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v0, v0, LX/67r;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1053443
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v0, v0, LX/67r;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1053444
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v6, v1}, LX/68u;->a(FF)LX/68u;

    move-result-object v1

    .line 1053445
    iput-object v1, v0, LX/67r;->y:LX/68u;

    .line 1053446
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v0, v0, LX/67r;->y:LX/68u;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, LX/68u;->a(J)LX/68u;

    .line 1053447
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v0, v0, LX/67r;->y:LX/68u;

    iget-object v1, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    invoke-virtual {v0, v1}, LX/68u;->a(LX/67p;)V

    .line 1053448
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v0, v0, LX/67r;->y:LX/68u;

    iget-object v1, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    invoke-virtual {v0, v1}, LX/68u;->a(LX/67q;)V

    .line 1053449
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    iget-object v0, v0, LX/67r;->y:LX/68u;

    invoke-virtual {v0}, LX/68u;->f()V

    .line 1053450
    :cond_6
    iget-object v0, p0, Lcom/facebook/android/maps/ClusterOverlay$2;->a:LX/67r;

    .line 1053451
    iput v4, v0, LX/67r;->E:F

    .line 1053452
    return-void
.end method
