.class public final Lcom/facebook/android/maps/TiledMapDrawable$1$1;
.super Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;
.source ""


# instance fields
.field public final synthetic a:LX/69C;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/android/maps/TiledMapDrawable$1;


# direct methods
.method public constructor <init>(Lcom/facebook/android/maps/TiledMapDrawable$1;LX/69C;Z)V
    .locals 0

    .prologue
    .line 1054964
    iput-object p1, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iput-object p2, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->a:LX/69C;

    iput-boolean p3, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->b:Z

    invoke-direct {p0}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 1054965
    iget-object v0, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget-object v0, v0, Lcom/facebook/android/maps/TiledMapDrawable$1;->f:LX/68P;

    iget-object v0, v0, LX/67m;->e:LX/680;

    .line 1054966
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 1054967
    iget v0, v0, Lcom/facebook/android/maps/MapView;->g:I

    .line 1054968
    iget-object v1, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->a:LX/69C;

    if-eqz v1, :cond_3

    .line 1054969
    iget-object v1, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget-object v1, v1, Lcom/facebook/android/maps/TiledMapDrawable$1;->f:LX/68P;

    iget-object v1, v1, LX/68P;->o:LX/68s;

    iget-object v2, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->a:LX/69C;

    invoke-virtual {v1, v2}, LX/68s;->a(LX/69C;)V

    .line 1054970
    iget-boolean v1, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->b:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget v1, v1, Lcom/facebook/android/maps/TiledMapDrawable$1;->c:I

    add-int/lit8 v0, v0, 0x1

    if-gt v1, v0, :cond_2

    .line 1054971
    iget-object v0, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget-object v0, v0, Lcom/facebook/android/maps/TiledMapDrawable$1;->f:LX/68P;

    iget-boolean v0, v0, LX/68P;->u:Z

    if-eqz v0, :cond_0

    .line 1054972
    iget-object v0, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->a:LX/69C;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, LX/69C;->d:J

    .line 1054973
    :cond_0
    iget-object v0, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget-object v0, v0, Lcom/facebook/android/maps/TiledMapDrawable$1;->f:LX/68P;

    invoke-virtual {v0}, LX/67m;->f()V

    .line 1054974
    sget-object v0, LX/68P;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1054975
    sget-object v0, LX/68P;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1054976
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1054977
    sget-object v0, LX/68P;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68P;

    invoke-virtual {v0}, LX/67m;->f()V

    .line 1054978
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1054979
    :cond_1
    sget-object v0, LX/68P;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1054980
    :cond_2
    :goto_1
    return-void

    .line 1054981
    :cond_3
    iget-object v1, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget v1, v1, Lcom/facebook/android/maps/TiledMapDrawable$1;->d:I

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget v1, v1, Lcom/facebook/android/maps/TiledMapDrawable$1;->c:I

    iget-object v2, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget-object v2, v2, Lcom/facebook/android/maps/TiledMapDrawable$1;->f:LX/68P;

    iget v2, v2, LX/68P;->v:I

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget v1, v1, Lcom/facebook/android/maps/TiledMapDrawable$1;->c:I

    if-ne v1, v0, :cond_2

    iget-object v0, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget-object v0, v0, Lcom/facebook/android/maps/TiledMapDrawable$1;->f:LX/68P;

    iget-object v1, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget v1, v1, Lcom/facebook/android/maps/TiledMapDrawable$1;->a:I

    iget-object v2, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget v2, v2, Lcom/facebook/android/maps/TiledMapDrawable$1;->b:I

    iget-object v3, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget v3, v3, Lcom/facebook/android/maps/TiledMapDrawable$1;->c:I

    const/4 v5, 0x1

    .line 1054982
    shl-int v6, v5, v3

    .line 1054983
    iget-object v7, v0, LX/67m;->f:LX/31h;

    iget-object v8, v0, LX/68P;->z:LX/31i;

    invoke-virtual {v7, v8}, LX/31h;->a(LX/31i;)V

    .line 1054984
    iget-object v7, v0, LX/68P;->z:LX/31i;

    iget-wide v7, v7, LX/31i;->c:D

    int-to-double v9, v6

    mul-double/2addr v7, v9

    int-to-double v9, v1

    cmpg-double v7, v7, v9

    if-gtz v7, :cond_5

    int-to-double v7, v1

    iget-object v9, v0, LX/68P;->z:LX/31i;

    iget-wide v9, v9, LX/31i;->d:D

    int-to-double v11, v6

    mul-double/2addr v9, v11

    cmpg-double v7, v7, v9

    if-gtz v7, :cond_5

    iget-object v7, v0, LX/68P;->z:LX/31i;

    iget-wide v7, v7, LX/31i;->a:D

    int-to-double v9, v6

    mul-double/2addr v7, v9

    int-to-double v9, v2

    cmpg-double v7, v7, v9

    if-gtz v7, :cond_5

    int-to-double v7, v2

    iget-object v9, v0, LX/68P;->z:LX/31i;

    iget-wide v9, v9, LX/31i;->b:D

    int-to-double v11, v6

    mul-double/2addr v9, v11

    cmpg-double v6, v7, v9

    if-gtz v6, :cond_5

    :goto_2
    move v0, v5

    .line 1054985
    if-eqz v0, :cond_2

    .line 1054986
    :cond_4
    iget-object v0, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget-object v0, v0, Lcom/facebook/android/maps/TiledMapDrawable$1;->f:LX/68P;

    iget-object v1, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget v1, v1, Lcom/facebook/android/maps/TiledMapDrawable$1;->a:I

    iget-object v2, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget v2, v2, Lcom/facebook/android/maps/TiledMapDrawable$1;->b:I

    iget-object v3, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget v3, v3, Lcom/facebook/android/maps/TiledMapDrawable$1;->c:I

    iget-object v4, p0, Lcom/facebook/android/maps/TiledMapDrawable$1$1;->c:Lcom/facebook/android/maps/TiledMapDrawable$1;

    iget v4, v4, Lcom/facebook/android/maps/TiledMapDrawable$1;->d:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v1, v2, v3, v4}, LX/68P;->a(IIII)V

    goto :goto_1

    :cond_5
    const/4 v5, 0x0

    goto :goto_2
.end method
