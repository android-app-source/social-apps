.class public final Lcom/facebook/android/maps/StaticMapView$1;
.super Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;
.source ""


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Landroid/net/Uri;

.field public final synthetic d:LX/3BP;


# direct methods
.method public constructor <init>(LX/3BP;Landroid/view/View;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1054900
    iput-object p1, p0, Lcom/facebook/android/maps/StaticMapView$1;->d:LX/3BP;

    iput-object p2, p0, Lcom/facebook/android/maps/StaticMapView$1;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/facebook/android/maps/StaticMapView$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/android/maps/StaticMapView$1;->c:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;-><init>()V

    return-void
.end method

.method private b()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1054901
    :try_start_0
    new-instance v1, Ljava/net/URL;

    iget-object v2, p0, Lcom/facebook/android/maps/StaticMapView$1;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1054902
    if-eqz v2, :cond_1

    .line 1054903
    :try_start_1
    const-string v1, "mapImage"

    invoke-static {v2, v1}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1054904
    if-eqz v2, :cond_0

    .line 1054905
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 1054906
    :cond_0
    :goto_0
    return-object v0

    .line 1054907
    :cond_1
    if-eqz v2, :cond_0

    .line 1054908
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 1054909
    :catch_0
    goto :goto_0

    .line 1054910
    :catch_1
    move-exception v1

    move-object v2, v0

    .line 1054911
    :goto_1
    :try_start_4
    sget-object v3, LX/31U;->t:LX/31U;

    invoke-virtual {v3, v1}, LX/31U;->a(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1054912
    if-eqz v2, :cond_0

    .line 1054913
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 1054914
    :catch_2
    goto :goto_0

    .line 1054915
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    if-eqz v2, :cond_2

    .line 1054916
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 1054917
    :cond_2
    :goto_3
    throw v0

    :catch_3
    goto :goto_0

    :catch_4
    goto :goto_3

    .line 1054918
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 1054919
    :catch_5
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 1054920
    invoke-direct {p0}, Lcom/facebook/android/maps/StaticMapView$1;->b()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1054921
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/android/maps/StaticMapView$1;->d:LX/3BP;

    iget-object v1, v1, LX/3BP;->f:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    if-eq v1, p0, :cond_1

    .line 1054922
    :cond_0
    :goto_0
    return-void

    .line 1054923
    :cond_1
    new-instance v1, Lcom/facebook/android/maps/StaticMapView$1$1;

    invoke-direct {v1, p0, v0, p0}, Lcom/facebook/android/maps/StaticMapView$1$1;-><init>(Lcom/facebook/android/maps/StaticMapView$1;Landroid/graphics/drawable/Drawable;Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V

    invoke-static {v1}, LX/31l;->c(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V

    goto :goto_0
.end method
