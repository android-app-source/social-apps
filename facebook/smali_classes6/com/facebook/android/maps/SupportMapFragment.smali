.class public Lcom/facebook/android/maps/SupportMapFragment;
.super Landroid/support/v4/app/Fragment;
.source ""


# instance fields
.field private a:LX/681;

.field private b:Lcom/facebook/android/maps/MapView;

.field private c:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/68J;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1054959
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 1054960
    return-void
.end method

.method public static a(LX/681;)Lcom/facebook/android/maps/SupportMapFragment;
    .locals 1

    .prologue
    .line 1054961
    new-instance v0, Lcom/facebook/android/maps/SupportMapFragment;

    invoke-direct {v0}, Lcom/facebook/android/maps/SupportMapFragment;-><init>()V

    .line 1054962
    iput-object p0, v0, Lcom/facebook/android/maps/SupportMapFragment;->a:LX/681;

    .line 1054963
    return-object v0
.end method


# virtual methods
.method public final a(LX/68J;)V
    .locals 1

    .prologue
    .line 1054943
    iget-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->b:Lcom/facebook/android/maps/MapView;

    if-eqz v0, :cond_0

    .line 1054944
    iget-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->b:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0, p1}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 1054945
    :goto_0
    return-void

    .line 1054946
    :cond_0
    iget-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->c:Ljava/util/Queue;

    if-nez v0, :cond_1

    .line 1054947
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->c:Ljava/util/Queue;

    .line 1054948
    :cond_1
    iget-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->c:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x6b429d33

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1054949
    iget-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->a:LX/681;

    if-eqz v0, :cond_0

    .line 1054950
    new-instance v0, Lcom/facebook/android/maps/MapView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/android/maps/SupportMapFragment;->a:LX/681;

    invoke-direct {v0, v2, v3}, Lcom/facebook/android/maps/MapView;-><init>(Landroid/content/Context;LX/681;)V

    iput-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->b:Lcom/facebook/android/maps/MapView;

    .line 1054951
    :goto_0
    iget-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->b:Lcom/facebook/android/maps/MapView;

    const v2, 0x1463120

    invoke-virtual {v0, v2}, Lcom/facebook/android/maps/MapView;->setId(I)V

    .line 1054952
    iget-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->c:Ljava/util/Queue;

    if-eqz v0, :cond_2

    .line 1054953
    :goto_1
    iget-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68J;

    if-eqz v0, :cond_1

    .line 1054954
    iget-object v2, p0, Lcom/facebook/android/maps/SupportMapFragment;->b:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v2, v0}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    goto :goto_1

    .line 1054955
    :cond_0
    new-instance v0, Lcom/facebook/android/maps/MapView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/android/maps/MapView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->b:Lcom/facebook/android/maps/MapView;

    goto :goto_0

    .line 1054956
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->c:Ljava/util/Queue;

    .line 1054957
    :cond_2
    iget-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->b:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0, p3}, Lcom/facebook/android/maps/MapView;->a(Landroid/os/Bundle;)V

    .line 1054958
    iget-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->b:Lcom/facebook/android/maps/MapView;

    const v2, 0x66acac83

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x51d5567f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1054940
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 1054941
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/android/maps/SupportMapFragment;->b:Lcom/facebook/android/maps/MapView;

    .line 1054942
    const/16 v1, 0x2b

    const v2, 0x260b36f1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInflate(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1054937
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onInflate(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    .line 1054938
    invoke-static {p1, p2}, LX/681;->a(Landroid/content/Context;Landroid/util/AttributeSet;)LX/681;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->a:LX/681;

    .line 1054939
    return-void
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 1054934
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onLowMemory()V

    .line 1054935
    iget-object v0, p0, Lcom/facebook/android/maps/SupportMapFragment;->b:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->b()V

    .line 1054936
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x48213c5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1054932
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 1054933
    const/16 v1, 0x2b

    const v2, -0x29c9d6a5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3084f808

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1054930
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 1054931
    const/16 v1, 0x2b

    const v2, 0x5f4419f6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
