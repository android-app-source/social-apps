.class public final Lcom/facebook/android/maps/StaticMapView$1$1;
.super Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;
.source ""


# instance fields
.field public final synthetic a:Landroid/graphics/drawable/Drawable;

.field public final synthetic b:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

.field public final synthetic c:Lcom/facebook/android/maps/StaticMapView$1;


# direct methods
.method public constructor <init>(Lcom/facebook/android/maps/StaticMapView$1;Landroid/graphics/drawable/Drawable;Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V
    .locals 0

    .prologue
    .line 1054890
    iput-object p1, p0, Lcom/facebook/android/maps/StaticMapView$1$1;->c:Lcom/facebook/android/maps/StaticMapView$1;

    iput-object p2, p0, Lcom/facebook/android/maps/StaticMapView$1$1;->a:Landroid/graphics/drawable/Drawable;

    iput-object p3, p0, Lcom/facebook/android/maps/StaticMapView$1$1;->b:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    invoke-direct {p0}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 1054891
    iget-object v0, p0, Lcom/facebook/android/maps/StaticMapView$1$1;->c:Lcom/facebook/android/maps/StaticMapView$1;

    iget-object v0, v0, Lcom/facebook/android/maps/StaticMapView$1;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/android/maps/StaticMapView$1$1;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1054892
    iget-object v0, p0, Lcom/facebook/android/maps/StaticMapView$1$1;->c:Lcom/facebook/android/maps/StaticMapView$1;

    iget-object v0, v0, Lcom/facebook/android/maps/StaticMapView$1;->d:LX/3BP;

    iget-wide v0, v0, LX/3BP;->r:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    sget-object v0, LX/31U;->C:LX/31U;

    invoke-virtual {v0}, LX/31U;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1054893
    sget-object v0, LX/31U;->C:LX/31U;

    new-instance v1, LX/68N;

    invoke-direct {v1, p0}, LX/68N;-><init>(Lcom/facebook/android/maps/StaticMapView$1$1;)V

    invoke-virtual {v0, v1}, LX/31U;->a(Ljava/util/Map;)V

    .line 1054894
    iget-object v0, p0, Lcom/facebook/android/maps/StaticMapView$1$1;->c:Lcom/facebook/android/maps/StaticMapView$1;

    iget-object v0, v0, Lcom/facebook/android/maps/StaticMapView$1;->d:LX/3BP;

    .line 1054895
    iput-wide v2, v0, LX/3BP;->r:J

    .line 1054896
    :cond_0
    iget-object v0, p0, Lcom/facebook/android/maps/StaticMapView$1$1;->c:Lcom/facebook/android/maps/StaticMapView$1;

    iget-object v0, v0, Lcom/facebook/android/maps/StaticMapView$1;->d:LX/3BP;

    iget-object v0, v0, LX/3BP;->f:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    iget-object v1, p0, Lcom/facebook/android/maps/StaticMapView$1$1;->b:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    if-ne v0, v1, :cond_1

    .line 1054897
    iget-object v0, p0, Lcom/facebook/android/maps/StaticMapView$1$1;->c:Lcom/facebook/android/maps/StaticMapView$1;

    iget-object v0, v0, Lcom/facebook/android/maps/StaticMapView$1;->d:LX/3BP;

    const/4 v1, 0x0

    .line 1054898
    iput-object v1, v0, LX/3BP;->f:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 1054899
    :cond_1
    return-void
.end method
