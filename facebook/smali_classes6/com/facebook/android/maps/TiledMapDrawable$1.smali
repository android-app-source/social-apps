.class public final Lcom/facebook/android/maps/TiledMapDrawable$1;
.super Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:I

.field public final synthetic d:I

.field public final synthetic e:LX/69C;

.field public final synthetic f:LX/68P;


# direct methods
.method public constructor <init>(LX/68P;IIIILX/69C;)V
    .locals 0

    .prologue
    .line 1054987
    iput-object p1, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->f:LX/68P;

    iput p2, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->a:I

    iput p3, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->b:I

    iput p4, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->c:I

    iput p5, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->d:I

    iput-object p6, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->e:LX/69C;

    invoke-direct {p0}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1054988
    iget-object v0, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->e:LX/69C;

    .line 1054989
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p0

    if-ne v1, p0, :cond_0

    .line 1054990
    invoke-static {v0}, LX/69C;->e(LX/69C;)V

    .line 1054991
    :goto_0
    return-void

    .line 1054992
    :cond_0
    iget-object v1, v0, LX/69C;->t:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    invoke-static {v1}, LX/31l;->c(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V

    goto :goto_0
.end method

.method public final run()V
    .locals 5

    .prologue
    .line 1054993
    iget-object v0, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->f:LX/68P;

    iget v1, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->a:I

    iget v2, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->b:I

    iget v3, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->c:I

    invoke-virtual {v0, v1, v2, v3}, LX/68P;->b(III)LX/69C;

    move-result-object v1

    .line 1054994
    sget-object v0, LX/68U;->a:LX/69C;

    if-eq v1, v0, :cond_1

    .line 1054995
    const/4 v0, 0x0

    .line 1054996
    :goto_0
    if-eqz v1, :cond_0

    .line 1054997
    iget v2, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->a:I

    iget v3, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->b:I

    iget v4, p0, Lcom/facebook/android/maps/TiledMapDrawable$1;->c:I

    invoke-virtual {v1, v2, v3, v4}, LX/69C;->a(III)LX/69C;

    .line 1054998
    :cond_0
    new-instance v2, Lcom/facebook/android/maps/TiledMapDrawable$1$1;

    invoke-direct {v2, p0, v1, v0}, Lcom/facebook/android/maps/TiledMapDrawable$1$1;-><init>(Lcom/facebook/android/maps/TiledMapDrawable$1;LX/69C;Z)V

    invoke-static {v2}, LX/31l;->c(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V

    .line 1054999
    return-void

    .line 1055000
    :cond_1
    iget v0, v1, LX/69C;->c:I

    iget v1, v1, LX/69C;->b:I

    invoke-static {v0, v1}, LX/69C;->a(II)LX/69C;

    move-result-object v1

    .line 1055001
    sget-object v0, LX/69C;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, LX/69C;->a(Landroid/graphics/Bitmap;)V

    .line 1055002
    const/4 v0, 0x1

    goto :goto_0
.end method
