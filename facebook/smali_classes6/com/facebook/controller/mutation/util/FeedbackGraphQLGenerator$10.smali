.class public final Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/37X;

.field public final synthetic b:Ljava/util/concurrent/Callable;

.field public final synthetic c:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic d:LX/2lk;

.field public final synthetic e:LX/3iV;


# direct methods
.method public constructor <init>(LX/3iV;LX/37X;Ljava/util/concurrent/Callable;Lcom/google/common/util/concurrent/SettableFuture;LX/2lk;)V
    .locals 0

    .prologue
    .line 1086085
    iput-object p1, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->e:LX/3iV;

    iput-object p2, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->a:LX/37X;

    iput-object p3, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->b:Ljava/util/concurrent/Callable;

    iput-object p4, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->c:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p5, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->d:LX/2lk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1086086
    :try_start_0
    iget-object v0, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->a:LX/37X;

    invoke-virtual {v0}, LX/37X;->g()I

    .line 1086087
    iget-object v0, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->b:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 1086088
    iget-boolean v1, v0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v1, v1

    .line 1086089
    if-nez v1, :cond_1

    .line 1086090
    new-instance v1, Lcom/facebook/fbservice/service/ServiceException;

    invoke-direct {v1, v0}, Lcom/facebook/fbservice/service/ServiceException;-><init>(Lcom/facebook/fbservice/service/OperationResult;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1086091
    :catch_0
    move-exception v0

    .line 1086092
    :try_start_1
    iget-object v1, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->a:LX/37X;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/37X;->a(Z)V

    .line 1086093
    iget-object v1, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->c:Lcom/google/common/util/concurrent/SettableFuture;

    instance-of v2, v0, Ljava/util/concurrent/ExecutionException;

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1086094
    iget-object v0, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->a:LX/37X;

    invoke-virtual {v0}, LX/1NB;->e()V

    .line 1086095
    :goto_0
    return-void

    .line 1086096
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->a:LX/37X;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/37X;->a(Z)V

    .line 1086097
    iget-object v1, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->c:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, 0x5d8b1374

    invoke-static {v1, v0, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1086098
    :try_start_3
    iget-object v0, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->a:LX/37X;

    invoke-virtual {v0}, LX/1NB;->c()V

    .line 1086099
    iget-object v0, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->e:LX/3iV;

    iget-object v0, v0, LX/3iV;->b:LX/0tX;

    iget-object v1, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->a:LX/37X;

    iget-object v2, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->d:LX/2lk;

    invoke-virtual {v0, v1, v2}, LX/0tX;->a(LX/37X;LX/2lk;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1086100
    :goto_1
    iget-object v0, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->a:LX/37X;

    invoke-virtual {v0}, LX/1NB;->e()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;->a:LX/37X;

    invoke-virtual {v1}, LX/1NB;->e()V

    throw v0

    :catch_1
    goto :goto_1
.end method
