.class public final Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 905622
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 905623
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 905620
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 905621
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 905604
    if-nez p1, :cond_0

    .line 905605
    :goto_0
    return v0

    .line 905606
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 905607
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 905608
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 905609
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 905610
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 905611
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 905612
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 905613
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 905614
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 905615
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 905616
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 905617
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 905618
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 905619
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4e363642
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 905603
    new-instance v0, Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 905600
    packed-switch p0, :pswitch_data_0

    .line 905601
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 905602
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x4e363642
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 905566
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 905598
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;->b(I)V

    .line 905599
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 905593
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 905594
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 905595
    :cond_0
    iput-object p1, p0, Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 905596
    iput p2, p0, Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;->b:I

    .line 905597
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 905592
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 905591
    new-instance v0, Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 905588
    iget v0, p0, LX/1vt;->c:I

    .line 905589
    move v0, v0

    .line 905590
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 905585
    iget v0, p0, LX/1vt;->c:I

    .line 905586
    move v0, v0

    .line 905587
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 905582
    iget v0, p0, LX/1vt;->b:I

    .line 905583
    move v0, v0

    .line 905584
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 905579
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 905580
    move-object v0, v0

    .line 905581
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 905570
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 905571
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 905572
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 905573
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 905574
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 905575
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 905576
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 905577
    invoke-static {v3, v9, v2}, Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 905578
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 905567
    iget v0, p0, LX/1vt;->c:I

    .line 905568
    move v0, v0

    .line 905569
    return v0
.end method
