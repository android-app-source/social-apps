.class public final Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 918436
    const-class v0, Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel;

    new-instance v1, Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 918437
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 918409
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 918411
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 918412
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 918413
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 918414
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 918415
    if-eqz v2, :cond_0

    .line 918416
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 918417
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 918418
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 918419
    if-eqz v2, :cond_1

    .line 918420
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 918421
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 918422
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 918423
    if-eqz v2, :cond_2

    .line 918424
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 918425
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 918426
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 918427
    if-eqz v2, :cond_3

    .line 918428
    const-string p0, "profile_picture"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 918429
    invoke-static {v1, v2, p1}, LX/5SY;->a(LX/15i;ILX/0nX;)V

    .line 918430
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 918431
    if-eqz v2, :cond_4

    .line 918432
    const-string p0, "url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 918433
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 918434
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 918435
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 918410
    check-cast p1, Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$Serializer;->a(Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
