.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x52ac5655
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:J

.field private h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1015271
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1015270
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1015268
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1015269
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1015265
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1015266
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1015267
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;
    .locals 4

    .prologue
    .line 1015248
    if-nez p0, :cond_0

    .line 1015249
    const/4 p0, 0x0

    .line 1015250
    :goto_0
    return-object p0

    .line 1015251
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    if-eqz v0, :cond_1

    .line 1015252
    check-cast p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    goto :goto_0

    .line 1015253
    :cond_1
    new-instance v2, LX/5t5;

    invoke-direct {v2}, LX/5t5;-><init>()V

    .line 1015254
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1015255
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1015256
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    invoke-static {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1015257
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1015258
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/5t5;->a:LX/0Px;

    .line 1015259
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/5t5;->b:Ljava/lang/String;

    .line 1015260
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->d()J

    move-result-wide v0

    iput-wide v0, v2, LX/5t5;->c:J

    .line 1015261
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->e()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    move-result-object v0

    iput-object v0, v2, LX/5t5;->d:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    .line 1015262
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->au_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/5t5;->e:Ljava/lang/String;

    .line 1015263
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->av_()LX/174;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    iput-object v0, v2, LX/5t5;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1015264
    invoke-virtual {v2}, LX/5t5;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object p0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1015242
    iput-object p1, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1015243
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1015244
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1015245
    if-eqz v0, :cond_0

    .line 1015246
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1015247
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1015240
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    .line 1015241
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1015238
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1015239
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 1015223
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1015224
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1015225
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1015226
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1015227
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->au_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1015228
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1015229
    const/4 v2, 0x6

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1015230
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1015231
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1015232
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->g:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1015233
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1015234
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1015235
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1015236
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1015237
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1015205
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1015206
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1015207
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1015208
    if-eqz v1, :cond_3

    .line 1015209
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    .line 1015210
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1015211
    :goto_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1015212
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    .line 1015213
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1015214
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    .line 1015215
    iput-object v0, v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->h:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    .line 1015216
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1015217
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1015218
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1015219
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    .line 1015220
    iput-object v0, v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1015221
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1015222
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1015204
    new-instance v0, LX/5t8;

    invoke-direct {v0, p1}, LX/5t8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1015203
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->au_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1015272
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1015273
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->g:J

    .line 1015274
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1015182
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1015183
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1015184
    const-string v0, "message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1015185
    check-cast p2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-direct {p0, p2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->a(Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;)V

    .line 1015186
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1015187
    return-void
.end method

.method public final au_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1015188
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->i:Ljava/lang/String;

    .line 1015189
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic av_()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1015190
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1015191
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->e:Ljava/util/List;

    .line 1015192
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1015193
    new-instance v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;-><init>()V

    .line 1015194
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1015195
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1015196
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->f:Ljava/lang/String;

    .line 1015197
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 1015198
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1015199
    iget-wide v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->g:J

    return-wide v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1015200
    const v0, 0xd362c7b

    return v0
.end method

.method public final synthetic e()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1015201
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1015202
    const v0, 0x4c808d5

    return v0
.end method
