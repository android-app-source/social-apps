.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x36a9f28c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1015337
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1015377
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1015375
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1015376
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1015372
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1015373
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1015374
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;
    .locals 8

    .prologue
    .line 1015348
    if-nez p0, :cond_0

    .line 1015349
    const/4 p0, 0x0

    .line 1015350
    :goto_0
    return-object p0

    .line 1015351
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;

    if-eqz v0, :cond_1

    .line 1015352
    check-cast p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;

    goto :goto_0

    .line 1015353
    :cond_1
    new-instance v0, LX/5tA;

    invoke-direct {v0}, LX/5tA;-><init>()V

    .line 1015354
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v1

    iput-object v1, v0, LX/5tA;->a:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    .line 1015355
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->b()I

    move-result v1

    iput v1, v0, LX/5tA;->b:I

    .line 1015356
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->c()I

    move-result v1

    iput v1, v0, LX/5tA;->c:I

    .line 1015357
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 1015358
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1015359
    iget-object v3, v0, LX/5tA;->a:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1015360
    const/4 v5, 0x3

    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 1015361
    invoke-virtual {v2, v7, v3}, LX/186;->b(II)V

    .line 1015362
    iget v3, v0, LX/5tA;->b:I

    invoke-virtual {v2, v6, v3, v7}, LX/186;->a(III)V

    .line 1015363
    const/4 v3, 0x2

    iget v5, v0, LX/5tA;->c:I

    invoke-virtual {v2, v3, v5, v7}, LX/186;->a(III)V

    .line 1015364
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1015365
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1015366
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1015367
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1015368
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1015369
    new-instance v3, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;-><init>(LX/15i;)V

    .line 1015370
    move-object p0, v3

    .line 1015371
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1015346
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    .line 1015347
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1015338
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1015339
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1015340
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1015341
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1015342
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1015343
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1015344
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1015345
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1015378
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1015379
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1015380
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    .line 1015381
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1015382
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;

    .line 1015383
    iput-object v0, v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    .line 1015384
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1015385
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1015323
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1015324
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1015325
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->f:I

    .line 1015326
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->g:I

    .line 1015327
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1015328
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1015329
    iget v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1015330
    new-instance v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;-><init>()V

    .line 1015331
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1015332
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1015333
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1015334
    iget v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel$ImageRangesModel;->g:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1015335
    const v0, 0x2d2cfbbe

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1015336
    const v0, 0x353fb0f

    return v0
.end method
