.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x36a9f28c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1013825
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1013824
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1013822
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1013823
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1013819
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1013820
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1013821
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;
    .locals 8

    .prologue
    .line 1013795
    if-nez p0, :cond_0

    .line 1013796
    const/4 p0, 0x0

    .line 1013797
    :goto_0
    return-object p0

    .line 1013798
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;

    if-eqz v0, :cond_1

    .line 1013799
    check-cast p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;

    goto :goto_0

    .line 1013800
    :cond_1
    new-instance v0, LX/5sr;

    invoke-direct {v0}, LX/5sr;-><init>()V

    .line 1013801
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v1

    iput-object v1, v0, LX/5sr;->a:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    .line 1013802
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->b()I

    move-result v1

    iput v1, v0, LX/5sr;->b:I

    .line 1013803
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->c()I

    move-result v1

    iput v1, v0, LX/5sr;->c:I

    .line 1013804
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 1013805
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1013806
    iget-object v3, v0, LX/5sr;->a:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1013807
    const/4 v5, 0x3

    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 1013808
    invoke-virtual {v2, v7, v3}, LX/186;->b(II)V

    .line 1013809
    iget v3, v0, LX/5sr;->b:I

    invoke-virtual {v2, v6, v3, v7}, LX/186;->a(III)V

    .line 1013810
    const/4 v3, 0x2

    iget v5, v0, LX/5sr;->c:I

    invoke-virtual {v2, v3, v5, v7}, LX/186;->a(III)V

    .line 1013811
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1013812
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1013813
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1013814
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1013815
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1013816
    new-instance v3, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;-><init>(LX/15i;)V

    .line 1013817
    move-object p0, v3

    .line 1013818
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1013793
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    .line 1013794
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1013785
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1013786
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1013787
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1013788
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1013789
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1013790
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1013791
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1013792
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1013777
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1013778
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1013779
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    .line 1013780
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1013781
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;

    .line 1013782
    iput-object v0, v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    .line 1013783
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1013784
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1013776
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1013763
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1013764
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->f:I

    .line 1013765
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->g:I

    .line 1013766
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1013774
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1013775
    iget v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1013771
    new-instance v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;-><init>()V

    .line 1013772
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1013773
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1013769
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1013770
    iget v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionLinkableTextWithEntitiesWithImagesModel$ImageRangesModel;->g:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1013768
    const v0, 0x9b30a45

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1013767
    const v0, 0x353fb0f

    return v0
.end method
