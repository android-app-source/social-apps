.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1014839
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel;

    new-instance v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1014840
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1014841
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1014842
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1014843
    invoke-static {p1, v0}, LX/5tU;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1014844
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1014845
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1014846
    new-instance v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel;-><init>()V

    .line 1014847
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1014848
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1014849
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1014850
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1014851
    :cond_0
    return-object v1
.end method
