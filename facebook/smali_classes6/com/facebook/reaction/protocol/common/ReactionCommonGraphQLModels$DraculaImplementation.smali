.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1012934
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1012935
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1012956
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1012957
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x2

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1012940
    if-nez p1, :cond_0

    .line 1012941
    :goto_0
    return v0

    .line 1012942
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1012943
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1012944
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v2

    .line 1012945
    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v3

    .line 1012946
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1012947
    invoke-virtual {p3, v0, v2}, LX/186;->a(IZ)V

    .line 1012948
    invoke-virtual {p3, v1, v3}, LX/186;->a(IZ)V

    .line 1012949
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1012950
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v6

    .line 1012951
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1012952
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1012953
    invoke-virtual {p3, v0, v6, v0}, LX/186;->a(III)V

    move-object v0, p3

    .line 1012954
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1012955
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5c53fb09 -> :sswitch_0
        0x77ef32a0 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1012939
    new-instance v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1012936
    sparse-switch p0, :sswitch_data_0

    .line 1012937
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1012938
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x5c53fb09 -> :sswitch_0
        0x77ef32a0 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1012926
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1012932
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;->b(I)V

    .line 1012933
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1012927
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1012928
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1012929
    :cond_0
    iput-object p1, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1012930
    iput p2, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;->b:I

    .line 1012931
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1012958
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1012901
    new-instance v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1012902
    iget v0, p0, LX/1vt;->c:I

    .line 1012903
    move v0, v0

    .line 1012904
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1012905
    iget v0, p0, LX/1vt;->c:I

    .line 1012906
    move v0, v0

    .line 1012907
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1012908
    iget v0, p0, LX/1vt;->b:I

    .line 1012909
    move v0, v0

    .line 1012910
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1012911
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1012912
    move-object v0, v0

    .line 1012913
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1012914
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1012915
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1012916
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1012917
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1012918
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1012919
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1012920
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1012921
    invoke-static {v3, v9, v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1012922
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1012923
    iget v0, p0, LX/1vt;->c:I

    .line 1012924
    move v0, v0

    .line 1012925
    return v0
.end method
