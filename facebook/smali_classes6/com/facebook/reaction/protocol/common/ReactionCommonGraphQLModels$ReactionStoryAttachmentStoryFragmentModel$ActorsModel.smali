.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x645d6d3b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1015067
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1015066
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1015064
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1015065
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1015004
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1015005
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1015006
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;
    .locals 10

    .prologue
    .line 1015038
    if-nez p0, :cond_0

    .line 1015039
    const/4 p0, 0x0

    .line 1015040
    :goto_0
    return-object p0

    .line 1015041
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    if-eqz v0, :cond_1

    .line 1015042
    check-cast p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    goto :goto_0

    .line 1015043
    :cond_1
    new-instance v0, LX/5t3;

    invoke-direct {v0}, LX/5t3;-><init>()V

    .line 1015044
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/5t3;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1015045
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5t3;->b:Ljava/lang/String;

    .line 1015046
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->c()LX/5sY;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;->a(LX/5sY;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5t3;->c:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1015047
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 1015048
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1015049
    iget-object v3, v0, LX/5t3;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1015050
    iget-object v5, v0, LX/5t3;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1015051
    iget-object v7, v0, LX/5t3;->c:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1015052
    const/4 v8, 0x3

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 1015053
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 1015054
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1015055
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1015056
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1015057
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1015058
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1015059
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1015060
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1015061
    new-instance v3, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;-><init>(LX/15i;)V

    .line 1015062
    move-object p0, v3

    .line 1015063
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1015036
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1015037
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1015026
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1015027
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1015028
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1015029
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1015030
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1015031
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1015032
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1015033
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1015034
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1015035
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1015018
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1015019
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1015020
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1015021
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1015022
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    .line 1015023
    iput-object v0, v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1015024
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1015025
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1015068
    new-instance v0, LX/5t4;

    invoke-direct {v0, p1}, LX/5t4;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1015015
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1015016
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1015017
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1015013
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1015014
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1015012
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1015009
    new-instance v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;-><init>()V

    .line 1015010
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1015011
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1015007
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->f:Ljava/lang/String;

    .line 1015008
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()LX/5sY;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1015003
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1015002
    const v0, -0x76cf2404

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1015001
    const v0, 0x3c2b9d5

    return v0
.end method
