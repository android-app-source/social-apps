.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3b926b4a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1014782
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1014781
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1014779
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1014780
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1014776
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1014777
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1014778
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;
    .locals 8

    .prologue
    .line 1014756
    if-nez p0, :cond_0

    .line 1014757
    const/4 p0, 0x0

    .line 1014758
    :goto_0
    return-object p0

    .line 1014759
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;

    if-eqz v0, :cond_1

    .line 1014760
    check-cast p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;

    goto :goto_0

    .line 1014761
    :cond_1
    new-instance v0, LX/5t0;

    invoke-direct {v0}, LX/5t0;-><init>()V

    .line 1014762
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v1

    iput-object v1, v0, LX/5t0;->a:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    .line 1014763
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1014764
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1014765
    iget-object v3, v0, LX/5t0;->a:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1014766
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1014767
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1014768
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1014769
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1014770
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1014771
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1014772
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1014773
    new-instance v3, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;-><init>(LX/15i;)V

    .line 1014774
    move-object p0, v3

    .line 1014775
    goto :goto_0
.end method

.method private j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1014754
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    .line 1014755
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1014734
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1014735
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1014736
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1014737
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1014738
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1014739
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1014746
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1014747
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1014748
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    .line 1014749
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1014750
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;

    .line 1014751
    iput-object v0, v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;->e:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    .line 1014752
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1014753
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1014745
    invoke-direct {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;->j()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1014742
    new-instance v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel$CoverPhotoModel$PhotoModel;-><init>()V

    .line 1014743
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1014744
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1014741
    const v0, -0x12ed7d76

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1014740
    const v0, 0x4984e12

    return v0
.end method
