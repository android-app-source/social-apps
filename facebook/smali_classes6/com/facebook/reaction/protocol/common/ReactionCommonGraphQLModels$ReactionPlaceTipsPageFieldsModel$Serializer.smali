.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1014509
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel;

    new-instance v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1014510
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1014511
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1014472
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1014473
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x7

    .line 1014474
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1014475
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1014476
    if-eqz v2, :cond_0

    .line 1014477
    const-string v3, "address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1014478
    invoke-static {v1, v2, p1}, LX/4aS;->a(LX/15i;ILX/0nX;)V

    .line 1014479
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1014480
    if-eqz v2, :cond_1

    .line 1014481
    const-string v3, "can_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1014482
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1014483
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1014484
    if-eqz v2, :cond_2

    .line 1014485
    const-string v3, "contextItemRows"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1014486
    invoke-static {v1, v2, p1, p2}, LX/5O0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1014487
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1014488
    if-eqz v2, :cond_3

    .line 1014489
    const-string v3, "does_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1014490
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1014491
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1014492
    if-eqz v2, :cond_4

    .line 1014493
    const-string v3, "location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1014494
    invoke-static {v1, v2, p1}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 1014495
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1014496
    if-eqz v2, :cond_5

    .line 1014497
    const-string v3, "menu_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1014498
    invoke-static {v1, v2, p1}, LX/5tP;->a(LX/15i;ILX/0nX;)V

    .line 1014499
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1014500
    if-eqz v2, :cond_6

    .line 1014501
    const-string v3, "overall_star_rating"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1014502
    invoke-static {v1, v2, p1}, LX/5tQ;->a(LX/15i;ILX/0nX;)V

    .line 1014503
    :cond_6
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1014504
    if-eqz v2, :cond_7

    .line 1014505
    const-string v2, "subscribe_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1014506
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1014507
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1014508
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1014471
    check-cast p1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel$Serializer;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
