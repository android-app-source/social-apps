.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x32f6ff2b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1013178
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1013177
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1013175
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1013176
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1013172
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1013173
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1013174
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;
    .locals 8

    .prologue
    .line 1013152
    if-nez p0, :cond_0

    .line 1013153
    const/4 p0, 0x0

    .line 1013154
    :goto_0
    return-object p0

    .line 1013155
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;

    if-eqz v0, :cond_1

    .line 1013156
    check-cast p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;

    goto :goto_0

    .line 1013157
    :cond_1
    new-instance v0, LX/5si;

    invoke-direct {v0}, LX/5si;-><init>()V

    .line 1013158
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5si;->a:Ljava/lang/String;

    .line 1013159
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1013160
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1013161
    iget-object v3, v0, LX/5si;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1013162
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1013163
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1013164
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1013165
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1013166
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1013167
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1013168
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1013169
    new-instance v3, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;-><init>(LX/15i;)V

    .line 1013170
    move-object p0, v3

    .line 1013171
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1013136
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1013137
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1013138
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1013139
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1013140
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1013141
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1013149
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1013150
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1013151
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1013147
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;->e:Ljava/lang/String;

    .line 1013148
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1013144
    new-instance v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;-><init>()V

    .line 1013145
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1013146
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1013143
    const v0, 0x7b93523f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1013142
    const v0, 0x752a03d5

    return v0
.end method
