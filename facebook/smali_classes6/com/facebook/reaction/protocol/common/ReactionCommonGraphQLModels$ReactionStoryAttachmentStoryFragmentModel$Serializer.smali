.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1015180
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    new-instance v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1015181
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1015175
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1015177
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1015178
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/5tX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1015179
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1015176
    check-cast p1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$Serializer;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
