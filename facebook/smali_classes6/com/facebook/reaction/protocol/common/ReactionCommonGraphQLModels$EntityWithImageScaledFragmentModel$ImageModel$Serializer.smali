.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel$ImageModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel$ImageModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1012987
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel$ImageModel;

    new-instance v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel$ImageModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel$ImageModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1012988
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1012989
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel$ImageModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1012990
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1012991
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1}, LX/5tC;->a(LX/15i;ILX/0nX;)V

    .line 1012992
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1012993
    check-cast p1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel$ImageModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel$ImageModel$Serializer;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$EntityWithImageScaledFragmentModel$ImageModel;LX/0nX;LX/0my;)V

    return-void
.end method
