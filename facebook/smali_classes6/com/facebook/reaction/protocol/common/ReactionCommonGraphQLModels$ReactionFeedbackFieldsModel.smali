.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x273eb04c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1013563
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1013562
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1013560
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1013561
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1013557
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1013558
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1013559
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;
    .locals 9

    .prologue
    .line 1013530
    if-nez p0, :cond_0

    .line 1013531
    const/4 p0, 0x0

    .line 1013532
    :goto_0
    return-object p0

    .line 1013533
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    if-eqz v0, :cond_1

    .line 1013534
    check-cast p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    goto :goto_0

    .line 1013535
    :cond_1
    new-instance v0, LX/5sn;

    invoke-direct {v0}, LX/5sn;-><init>()V

    .line 1013536
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->b()Z

    move-result v1

    iput-boolean v1, v0, LX/5sn;->a:Z

    .line 1013537
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/5sn;->b:Z

    .line 1013538
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5sn;->c:Ljava/lang/String;

    .line 1013539
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5sn;->d:Ljava/lang/String;

    .line 1013540
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1013541
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1013542
    iget-object v3, v0, LX/5sn;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1013543
    iget-object v5, v0, LX/5sn;->d:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1013544
    const/4 v7, 0x4

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1013545
    iget-boolean v7, v0, LX/5sn;->a:Z

    invoke-virtual {v2, v8, v7}, LX/186;->a(IZ)V

    .line 1013546
    iget-boolean v7, v0, LX/5sn;->b:Z

    invoke-virtual {v2, v6, v7}, LX/186;->a(IZ)V

    .line 1013547
    const/4 v7, 0x2

    invoke-virtual {v2, v7, v3}, LX/186;->b(II)V

    .line 1013548
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1013549
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1013550
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1013551
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1013552
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1013553
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1013554
    new-instance v3, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;-><init>(LX/15i;)V

    .line 1013555
    move-object p0, v3

    .line 1013556
    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1013524
    iput-boolean p1, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->e:Z

    .line 1013525
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1013526
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1013527
    if-eqz v0, :cond_0

    .line 1013528
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1013529
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1013518
    iput-boolean p1, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->f:Z

    .line 1013519
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1013520
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1013521
    if-eqz v0, :cond_0

    .line 1013522
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1013523
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1013508
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1013509
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1013510
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1013511
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1013512
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1013513
    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->f:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1013514
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1013515
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1013516
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1013517
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1013490
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1013491
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1013492
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1013507
    new-instance v0, LX/5so;

    invoke-direct {v0, p1}, LX/5so;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1013564
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1013503
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1013504
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->e:Z

    .line 1013505
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->f:Z

    .line 1013506
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1013493
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1013494
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1013495
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1013496
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1013497
    :goto_0
    return-void

    .line 1013498
    :cond_0
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1013499
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1013500
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1013501
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1013502
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1013485
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1013486
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->a(Z)V

    .line 1013487
    :cond_0
    :goto_0
    return-void

    .line 1013488
    :cond_1
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1013489
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->b(Z)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1013482
    new-instance v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;-><init>()V

    .line 1013483
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1013484
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1013480
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1013481
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1013478
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1013479
    iget-boolean v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->f:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1013476
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->g:Ljava/lang/String;

    .line 1013477
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1013475
    const v0, 0x3d354c0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1013473
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->h:Ljava/lang/String;

    .line 1013474
    iget-object v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1013472
    const v0, -0x78fb05b

    return v0
.end method
