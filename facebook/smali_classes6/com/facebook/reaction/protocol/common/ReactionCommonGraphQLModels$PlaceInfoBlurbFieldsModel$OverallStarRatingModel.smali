.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x107c00cc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel$Serializer;
.end annotation


# instance fields
.field private e:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1013202
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1013203
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1013204
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1013205
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1013206
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1013207
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1013208
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;
    .locals 12

    .prologue
    .line 1013209
    if-nez p0, :cond_0

    .line 1013210
    const/4 p0, 0x0

    .line 1013211
    :goto_0
    return-object p0

    .line 1013212
    :cond_0
    instance-of v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;

    if-eqz v0, :cond_1

    .line 1013213
    check-cast p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;

    goto :goto_0

    .line 1013214
    :cond_1
    new-instance v0, LX/5sk;

    invoke-direct {v0}, LX/5sk;-><init>()V

    .line 1013215
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;->a()D

    move-result-wide v2

    iput-wide v2, v0, LX/5sk;->a:D

    .line 1013216
    const/4 v11, 0x1

    const/4 v5, 0x0

    const/4 v10, 0x0

    .line 1013217
    new-instance v4, LX/186;

    const/16 v6, 0x80

    invoke-direct {v4, v6}, LX/186;-><init>(I)V

    .line 1013218
    invoke-virtual {v4, v11}, LX/186;->c(I)V

    .line 1013219
    iget-wide v6, v0, LX/5sk;->a:D

    const-wide/16 v8, 0x0

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 1013220
    invoke-virtual {v4}, LX/186;->d()I

    move-result v6

    .line 1013221
    invoke-virtual {v4, v6}, LX/186;->d(I)V

    .line 1013222
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 1013223
    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1013224
    new-instance v4, LX/15i;

    move-object v5, v6

    move-object v6, v10

    move-object v7, v10

    move v8, v11

    move-object v9, v10

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1013225
    new-instance v5, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;

    invoke-direct {v5, v4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;-><init>(LX/15i;)V

    .line 1013226
    move-object p0, v5

    .line 1013227
    goto :goto_0
.end method


# virtual methods
.method public final a()D
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1013228
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1013229
    iget-wide v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;->e:D

    return-wide v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1013230
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1013231
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1013232
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;->e:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1013233
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1013234
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1013235
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1013236
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1013237
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1013238
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1013239
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;->e:D

    .line 1013240
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1013241
    new-instance v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;

    invoke-direct {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;-><init>()V

    .line 1013242
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1013243
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1013244
    const v0, -0x3e177b34

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1013245
    const v0, -0x6e856243

    return v0
.end method
