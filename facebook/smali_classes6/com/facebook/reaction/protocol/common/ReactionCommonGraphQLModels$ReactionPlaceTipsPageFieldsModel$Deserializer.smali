.class public final Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1014408
    const-class v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel;

    new-instance v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1014409
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1014410
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1014411
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1014412
    const/4 v10, 0x0

    .line 1014413
    const/4 v9, 0x0

    .line 1014414
    const/4 v8, 0x0

    .line 1014415
    const/4 v7, 0x0

    .line 1014416
    const/4 v6, 0x0

    .line 1014417
    const/4 v5, 0x0

    .line 1014418
    const/4 v4, 0x0

    .line 1014419
    const/4 v3, 0x0

    .line 1014420
    const/4 v2, 0x0

    .line 1014421
    const/4 v1, 0x0

    .line 1014422
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, p0, :cond_2

    .line 1014423
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1014424
    const/4 v1, 0x0

    .line 1014425
    :goto_0
    move v1, v1

    .line 1014426
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1014427
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1014428
    new-instance v1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel;

    invoke-direct {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPlaceTipsPageFieldsModel;-><init>()V

    .line 1014429
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1014430
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1014431
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1014432
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1014433
    :cond_0
    return-object v1

    .line 1014434
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1014435
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_a

    .line 1014436
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1014437
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1014438
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 1014439
    const-string p0, "address"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1014440
    invoke-static {p1, v0}, LX/4aS;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1014441
    :cond_3
    const-string p0, "can_viewer_like"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1014442
    const/4 v2, 0x1

    .line 1014443
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 1014444
    :cond_4
    const-string p0, "contextItemRows"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1014445
    invoke-static {p1, v0}, LX/5O0;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1014446
    :cond_5
    const-string p0, "does_viewer_like"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1014447
    const/4 v1, 0x1

    .line 1014448
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 1014449
    :cond_6
    const-string p0, "location"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1014450
    invoke-static {p1, v0}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1014451
    :cond_7
    const-string p0, "menu_info"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1014452
    invoke-static {p1, v0}, LX/5tP;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1014453
    :cond_8
    const-string p0, "overall_star_rating"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1014454
    invoke-static {p1, v0}, LX/5tQ;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1014455
    :cond_9
    const-string p0, "subscribe_status"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1014456
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto/16 :goto_1

    .line 1014457
    :cond_a
    const/16 v11, 0x8

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1014458
    const/4 v11, 0x0

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1014459
    if-eqz v2, :cond_b

    .line 1014460
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v9}, LX/186;->a(IZ)V

    .line 1014461
    :cond_b
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1014462
    if-eqz v1, :cond_c

    .line 1014463
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 1014464
    :cond_c
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1014465
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1014466
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1014467
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1014468
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
