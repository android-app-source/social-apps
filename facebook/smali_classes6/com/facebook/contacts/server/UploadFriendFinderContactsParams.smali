.class public Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6OZ;

.field public final b:Ljava/lang/String;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/server/UploadBulkContactChange;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1085284
    new-instance v0, LX/6OY;

    invoke-direct {v0}, LX/6OY;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6OZ;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6OZ;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/contacts/server/UploadBulkContactChange;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1085274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085275
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1085276
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1085277
    iput-object p1, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->a:LX/6OZ;

    .line 1085278
    iput-object p2, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->b:Ljava/lang/String;

    .line 1085279
    invoke-static {p3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->c:LX/0Px;

    .line 1085280
    iput-boolean p4, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->d:Z

    .line 1085281
    return-void

    :cond_0
    move v0, v2

    .line 1085282
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1085283
    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1085290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085291
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6OZ;->valueOf(Ljava/lang/String;)LX/6OZ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->a:LX/6OZ;

    .line 1085292
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->b:Ljava/lang/String;

    .line 1085293
    const-class v0, Lcom/facebook/contacts/server/UploadBulkContactChange;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->c:LX/0Px;

    .line 1085294
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->d:Z

    .line 1085295
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1085296
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1085285
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->a:LX/6OZ;

    invoke-virtual {v0}, LX/6OZ;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085286
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085287
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1085288
    iget-boolean v0, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1085289
    return-void
.end method
