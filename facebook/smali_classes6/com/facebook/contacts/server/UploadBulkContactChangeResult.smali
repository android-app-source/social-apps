.class public Lcom/facebook/contacts/server/UploadBulkContactChangeResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/UploadBulkContactChangeResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6OQ;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field private final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/6OP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1085154
    new-instance v0, LX/6OO;

    invoke-direct {v0}, LX/6OO;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6OQ;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/6OP;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6OQ;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;",
            ">;",
            "LX/6OP;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1085140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085141
    iput-object p1, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->a:LX/6OQ;

    .line 1085142
    iput-object p2, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->b:Ljava/lang/String;

    .line 1085143
    iput-object p3, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->c:Ljava/lang/String;

    .line 1085144
    iput-object p4, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->d:LX/0Px;

    .line 1085145
    iput-object p5, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->e:LX/6OP;

    .line 1085146
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1085147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085148
    const-class v0, LX/6OQ;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6OQ;

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->a:LX/6OQ;

    .line 1085149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->b:Ljava/lang/String;

    .line 1085150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->c:Ljava/lang/String;

    .line 1085151
    const-class v0, Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->d:LX/0Px;

    .line 1085152
    const-class v0, LX/6OP;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6OP;

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->e:LX/6OP;

    .line 1085153
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1085139
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1085138
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UploadBulkContactChangeResult ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->a:LX/6OQ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") confidence: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->e:LX/6OP;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " local id: ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] -> remote id: ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1085132
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->a:LX/6OQ;

    invoke-virtual {v0}, LX/6OQ;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085133
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085134
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085135
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1085136
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->e:LX/6OP;

    invoke-virtual {v0}, LX/6OP;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085137
    return-void
.end method
