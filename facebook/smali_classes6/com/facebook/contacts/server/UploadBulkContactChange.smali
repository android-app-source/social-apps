.class public Lcom/facebook/contacts/server/UploadBulkContactChange;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/UploadBulkContactChange;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/contacts/model/PhonebookContact;

.field public final d:LX/6ON;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1085097
    new-instance v0, LX/6OM;

    invoke-direct {v0}, LX/6OM;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/UploadBulkContactChange;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1085091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085092
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChange;->a:Ljava/lang/String;

    .line 1085093
    const-class v0, Lcom/facebook/contacts/model/PhonebookContact;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookContact;

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChange;->c:Lcom/facebook/contacts/model/PhonebookContact;

    .line 1085094
    const-class v0, LX/6ON;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6ON;

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChange;->d:LX/6ON;

    .line 1085095
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChange;->b:Ljava/lang/String;

    .line 1085096
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/contacts/model/PhonebookContact;LX/6ON;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1085098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085099
    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1085100
    if-eqz p3, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1085101
    sget-object v0, LX/6ON;->DELETE:LX/6ON;

    if-eq p3, v0, :cond_0

    if-eqz p2, :cond_3

    :cond_0
    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1085102
    if-eqz p4, :cond_4

    :goto_3
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1085103
    iput-object p1, p0, Lcom/facebook/contacts/server/UploadBulkContactChange;->a:Ljava/lang/String;

    .line 1085104
    iput-object p2, p0, Lcom/facebook/contacts/server/UploadBulkContactChange;->c:Lcom/facebook/contacts/model/PhonebookContact;

    .line 1085105
    iput-object p3, p0, Lcom/facebook/contacts/server/UploadBulkContactChange;->d:LX/6ON;

    .line 1085106
    iput-object p4, p0, Lcom/facebook/contacts/server/UploadBulkContactChange;->b:Ljava/lang/String;

    .line 1085107
    return-void

    :cond_1
    move v0, v2

    .line 1085108
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1085109
    goto :goto_1

    :cond_3
    move v0, v2

    .line 1085110
    goto :goto_2

    :cond_4
    move v1, v2

    .line 1085111
    goto :goto_3
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1085090
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1085085
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChange;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085086
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChange;->c:Lcom/facebook/contacts/model/PhonebookContact;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1085087
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChange;->d:LX/6ON;

    invoke-virtual {v0}, LX/6ON;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085088
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactChange;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085089
    return-void
.end method
