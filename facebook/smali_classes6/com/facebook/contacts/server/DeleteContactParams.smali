.class public Lcom/facebook/contacts/server/DeleteContactParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/DeleteContactParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/contacts/graphql/Contact;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1084861
    new-instance v0, LX/6O7;

    invoke-direct {v0}, LX/6O7;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/DeleteContactParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1084858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1084859
    const-class v0, Lcom/facebook/contacts/graphql/Contact;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    iput-object v0, p0, Lcom/facebook/contacts/server/DeleteContactParams;->a:Lcom/facebook/contacts/graphql/Contact;

    .line 1084860
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1084857
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1084855
    iget-object v0, p0, Lcom/facebook/contacts/server/DeleteContactParams;->a:Lcom/facebook/contacts/graphql/Contact;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1084856
    return-void
.end method
