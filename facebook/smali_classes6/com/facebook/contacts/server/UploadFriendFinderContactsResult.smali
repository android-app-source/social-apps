.class public Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:LX/6Ob;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1085322
    new-instance v0, LX/6Oa;

    invoke-direct {v0}, LX/6Oa;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1085318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085319
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;->a:Ljava/lang/String;

    .line 1085320
    const-class v0, LX/6Ob;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Ob;

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;->b:LX/6Ob;

    .line 1085321
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/6Ob;)V
    .locals 0

    .prologue
    .line 1085310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085311
    iput-object p1, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;->a:Ljava/lang/String;

    .line 1085312
    iput-object p2, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;->b:LX/6Ob;

    .line 1085313
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1085317
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1085314
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085315
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;->b:LX/6Ob;

    invoke-virtual {v0}, LX/6Ob;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085316
    return-void
.end method
