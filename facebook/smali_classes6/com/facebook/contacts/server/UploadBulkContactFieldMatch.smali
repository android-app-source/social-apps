.class public Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/6OS;

.field private final b:LX/6OT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1085186
    new-instance v0, LX/6OR;

    invoke-direct {v0}, LX/6OR;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6OS;LX/6OT;)V
    .locals 0

    .prologue
    .line 1085182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085183
    iput-object p1, p0, Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;->a:LX/6OS;

    .line 1085184
    iput-object p2, p0, Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;->b:LX/6OT;

    .line 1085185
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1085173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085174
    const-class v0, LX/6OS;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6OS;

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;->a:LX/6OS;

    .line 1085175
    const-class v0, LX/6OT;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6OT;

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;->b:LX/6OT;

    .line 1085176
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1085181
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1085180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " match type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;->a:LX/6OS;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " value type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;->b:LX/6OT;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1085177
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;->a:LX/6OS;

    invoke-virtual {v0}, LX/6OS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085178
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;->b:LX/6OT;

    invoke-virtual {v0}, LX/6OT;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085179
    return-void
.end method
