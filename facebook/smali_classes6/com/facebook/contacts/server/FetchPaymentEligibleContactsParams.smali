.class public Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1085025
    new-instance v0, LX/6OI;

    invoke-direct {v0}, LX/6OI;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1085021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085022
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->a:Ljava/lang/String;

    .line 1085023
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->b:I

    .line 1085024
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1085026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085027
    iput-object p1, p0, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->a:Ljava/lang/String;

    .line 1085028
    iput p2, p0, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->b:I

    .line 1085029
    return-void
.end method

.method public static a(Ljava/lang/CharSequence;)Z
    .locals 1

    .prologue
    .line 1085020
    invoke-static {p0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1085019
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1085016
    iget-object v0, p0, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085017
    iget v0, p0, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1085018
    return-void
.end method
