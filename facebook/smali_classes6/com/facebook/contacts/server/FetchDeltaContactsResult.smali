.class public Lcom/facebook/contacts/server/FetchDeltaContactsResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/FetchDeltaContactsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1084973
    new-instance v0, LX/6OG;

    invoke-direct {v0}, LX/6OG;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0ta;LX/0Px;LX/0Px;Ljava/lang/String;ZJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ta;",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "ZJ)V"
        }
    .end annotation

    .prologue
    .line 1084974
    invoke-direct {p0, p1, p6, p7}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 1084975
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1084976
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1084977
    iput-object p2, p0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->a:LX/0Px;

    .line 1084978
    iput-object p3, p0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->b:LX/0Px;

    .line 1084979
    iput-object p4, p0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->c:Ljava/lang/String;

    .line 1084980
    iput-boolean p5, p0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->d:Z

    .line 1084981
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1084982
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 1084983
    const-class v1, Lcom/facebook/contacts/graphql/Contact;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->a:LX/0Px;

    .line 1084984
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->b:LX/0Px;

    .line 1084985
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->c:Ljava/lang/String;

    .line 1084986
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->d:Z

    .line 1084987
    return-void

    .line 1084988
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1084989
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1084990
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1084991
    iget-object v0, p0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1084992
    iget-object v0, p0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1084993
    iget-object v0, p0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1084994
    iget-boolean v0, p0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1084995
    return-void

    .line 1084996
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
