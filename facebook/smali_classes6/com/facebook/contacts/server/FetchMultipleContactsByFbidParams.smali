.class public Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0rS;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1085000
    new-instance v0, LX/6OH;

    invoke-direct {v0}, LX/6OH;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Rf;LX/0rS;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/0rS;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1085001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085002
    iput-object p1, p0, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;->a:LX/0Rf;

    .line 1085003
    iput-object p2, p0, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;->b:LX/0rS;

    .line 1085004
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1085005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085006
    const-class v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;->a:LX/0Rf;

    .line 1085007
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;->b:LX/0rS;

    .line 1085008
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1085009
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1085010
    iget-object v0, p0, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;->a:LX/0Rf;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1085011
    iget-object v0, p0, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;->b:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085012
    return-void
.end method
