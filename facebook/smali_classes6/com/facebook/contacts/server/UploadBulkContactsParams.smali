.class public Lcom/facebook/contacts/server/UploadBulkContactsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/UploadBulkContactsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/server/UploadBulkContactChange;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/contacts/ContactSurface;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1085190
    new-instance v0, LX/6OU;

    invoke-direct {v0}, LX/6OU;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/UploadBulkContactsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1085191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085192
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactsParams;->a:Ljava/lang/String;

    .line 1085193
    const-class v0, Lcom/facebook/contacts/server/UploadBulkContactChange;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactsParams;->b:LX/0Px;

    .line 1085194
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/contacts/ContactSurface;->valueOf(Ljava/lang/String;)Lcom/facebook/contacts/ContactSurface;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactsParams;->c:Lcom/facebook/contacts/ContactSurface;

    .line 1085195
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;Lcom/facebook/contacts/ContactSurface;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/server/UploadBulkContactChange;",
            ">;",
            "Lcom/facebook/contacts/ContactSurface;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1085196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085197
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1085198
    if-eqz p2, :cond_2

    move v0, v2

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1085199
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_2
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1085200
    iput-object p1, p0, Lcom/facebook/contacts/server/UploadBulkContactsParams;->a:Ljava/lang/String;

    .line 1085201
    iput-object p2, p0, Lcom/facebook/contacts/server/UploadBulkContactsParams;->b:LX/0Px;

    .line 1085202
    iput-object p3, p0, Lcom/facebook/contacts/server/UploadBulkContactsParams;->c:Lcom/facebook/contacts/ContactSurface;

    .line 1085203
    return-void

    :cond_1
    move v0, v1

    .line 1085204
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1085205
    goto :goto_1

    :cond_3
    move v2, v1

    .line 1085206
    goto :goto_2
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1085207
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1085208
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactsParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085209
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactsParams;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1085210
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactsParams;->c:Lcom/facebook/contacts/ContactSurface;

    invoke-virtual {v0}, Lcom/facebook/contacts/ContactSurface;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1085211
    return-void
.end method
