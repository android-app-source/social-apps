.class public Lcom/facebook/contacts/server/FetchDeltaContactsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/FetchDeltaContactsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1084965
    new-instance v0, LX/6OF;

    invoke-direct {v0}, LX/6OF;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/FetchDeltaContactsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 1084957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1084958
    iput p1, p0, Lcom/facebook/contacts/server/FetchDeltaContactsParams;->a:I

    .line 1084959
    iput-object p2, p0, Lcom/facebook/contacts/server/FetchDeltaContactsParams;->b:Ljava/lang/String;

    .line 1084960
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1084966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1084967
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/server/FetchDeltaContactsParams;->a:I

    .line 1084968
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/FetchDeltaContactsParams;->b:Ljava/lang/String;

    .line 1084969
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1084964
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1084961
    iget v0, p0, Lcom/facebook/contacts/server/FetchDeltaContactsParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1084962
    iget-object v0, p0, Lcom/facebook/contacts/server/FetchDeltaContactsParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1084963
    return-void
.end method
