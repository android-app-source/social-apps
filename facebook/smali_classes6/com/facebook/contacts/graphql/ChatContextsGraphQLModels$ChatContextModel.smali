.class public final Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2c89069d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusSecondaryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1079118
    const-class v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1079143
    const-class v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1079141
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1079142
    return-void
.end method

.method private j()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1079139
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->e:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->e:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;

    .line 1079140
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->e:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;

    return-object v0
.end method

.method private k()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusSecondaryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1079137
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->f:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusSecondaryModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusSecondaryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusSecondaryModel;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->f:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusSecondaryModel;

    .line 1079138
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->f:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusSecondaryModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1079127
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1079128
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->j()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1079129
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->k()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusSecondaryModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1079130
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->b()Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1079131
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1079132
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1079133
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1079134
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1079135
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1079136
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1079144
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1079145
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->j()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1079146
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->j()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;

    .line 1079147
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->j()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1079148
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    .line 1079149
    iput-object v0, v1, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->e:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;

    .line 1079150
    :cond_0
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->k()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusSecondaryModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1079151
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->k()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusSecondaryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusSecondaryModel;

    .line 1079152
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->k()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusSecondaryModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1079153
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    .line 1079154
    iput-object v0, v1, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->f:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusSecondaryModel;

    .line 1079155
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1079156
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1079126
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->j()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel$ContextStatusModel;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLUserChatContextType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1079124
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->g:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->g:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 1079125
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->g:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1079121
    new-instance v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    invoke-direct {v0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;-><init>()V

    .line 1079122
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1079123
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1079120
    const v0, -0x333aaa25

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1079119
    const v0, -0x1b2001b4

    return v0
.end method
