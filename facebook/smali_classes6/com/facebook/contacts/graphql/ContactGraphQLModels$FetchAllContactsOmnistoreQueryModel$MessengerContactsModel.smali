.class public final Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7a29f1fb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1080490
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1080489
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1080466
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1080467
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNodes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1080487
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel;->e:Ljava/util/List;

    .line 1080488
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1080481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1080482
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1080483
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1080484
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1080485
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1080486
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1080473
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1080474
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1080475
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1080476
    if-eqz v1, :cond_0

    .line 1080477
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel;

    .line 1080478
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel;->e:Ljava/util/List;

    .line 1080479
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1080480
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1080470
    new-instance v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel;

    invoke-direct {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchAllContactsOmnistoreQueryModel$MessengerContactsModel;-><init>()V

    .line 1080471
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1080472
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1080469
    const v0, 0x197bce50

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1080468
    const v0, 0x35468fe4

    return v0
.end method
