.class public final Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/6Lx;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x59b94a74
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1079016
    const-class v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1079015
    const-class v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1079013
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1079014
    return-void
.end method

.method private j()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1079011
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->e:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->e:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    .line 1079012
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->e:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1079003
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1079004
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->j()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1079005
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1079006
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1079007
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1079008
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1079009
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1079010
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1078995
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1078996
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->j()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1078997
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->j()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    .line 1078998
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->j()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1078999
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;

    .line 1079000
    iput-object v0, v1, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->e:Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    .line 1079001
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1079002
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1078994
    new-instance v0, LX/6Lz;

    invoke-direct {v0, p1}, LX/6Lz;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1079017
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1078992
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1078993
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1078991
    return-void
.end method

.method public final synthetic b()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1078990
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->j()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1078987
    new-instance v0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;

    invoke-direct {v0}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;-><init>()V

    .line 1078988
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1078989
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1078983
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->f:Ljava/lang/String;

    .line 1078984
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextForUserModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1078986
    const v0, 0x20bd4b4b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1078985
    const v0, 0x285feb

    return v0
.end method
