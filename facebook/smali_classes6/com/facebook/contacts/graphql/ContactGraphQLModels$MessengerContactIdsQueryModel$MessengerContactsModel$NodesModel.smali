.class public final Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2d55dc1c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$RepresentedProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1081469
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1081468
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1081466
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1081467
    return-void
.end method

.method private a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$RepresentedProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1081464
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel;->e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$RepresentedProfileModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$RepresentedProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$RepresentedProfileModel;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel;->e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$RepresentedProfileModel;

    .line 1081465
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel;->e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$RepresentedProfileModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1081458
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1081459
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$RepresentedProfileModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1081460
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1081461
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1081462
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1081463
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1081450
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1081451
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$RepresentedProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1081452
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$RepresentedProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$RepresentedProfileModel;

    .line 1081453
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$RepresentedProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1081454
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel;

    .line 1081455
    iput-object v0, v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel;->e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel$RepresentedProfileModel;

    .line 1081456
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1081457
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1081449
    new-instance v0, LX/6MU;

    invoke-direct {v0, p1}, LX/6MU;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1081441
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1081442
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1081448
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1081445
    new-instance v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$MessengerContactIdsQueryModel$MessengerContactsModel$NodesModel;-><init>()V

    .line 1081446
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1081447
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1081444
    const v0, 0x7825e349

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1081443
    const v0, -0x64104400

    return v0
.end method
