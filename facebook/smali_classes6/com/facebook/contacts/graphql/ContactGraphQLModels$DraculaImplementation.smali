.class public final Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1080351
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1080352
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1080416
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1080417
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1080370
    if-nez p1, :cond_0

    move v0, v1

    .line 1080371
    :goto_0
    return v0

    .line 1080372
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1080373
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1080374
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v2

    .line 1080375
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$ImportedPhoneEntriesModel$PrimaryFieldModel;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$ImportedPhoneEntriesModel$PrimaryFieldModel;

    .line 1080376
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1080377
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1080378
    invoke-virtual {p3, v1, v2}, LX/186;->a(IZ)V

    .line 1080379
    invoke-virtual {p3, v5, v0}, LX/186;->b(II)V

    .line 1080380
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1080381
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1080382
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1080383
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1080384
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1080385
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1080386
    :sswitch_2
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1080387
    invoke-virtual {p0, p1, v5, v1}, LX/15i;->a(III)I

    move-result v2

    .line 1080388
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1080389
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1080390
    invoke-virtual {p3, v5, v2, v1}, LX/186;->a(III)V

    .line 1080391
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1080392
    :sswitch_3
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1080393
    invoke-virtual {p0, p1, v5, v1}, LX/15i;->a(III)I

    move-result v2

    .line 1080394
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v3

    .line 1080395
    invoke-virtual {p3, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1080396
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1080397
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1080398
    invoke-virtual {p3, v5, v2, v1}, LX/186;->a(III)V

    .line 1080399
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 1080400
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1080401
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1080402
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1080403
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1080404
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1080405
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1080406
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1080407
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1080408
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1080409
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1080410
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1080411
    invoke-virtual {p0, p1, v5, v1}, LX/15i;->a(III)I

    move-result v2

    .line 1080412
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1080413
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1080414
    invoke-virtual {p3, v5, v2, v1}, LX/186;->a(III)V

    .line 1080415
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4309df98 -> :sswitch_5
        -0x20741a65 -> :sswitch_0
        0x15136e12 -> :sswitch_1
        0x4d8c57f1 -> :sswitch_3
        0x528ee1d9 -> :sswitch_2
        0x69f66839 -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1080361
    if-nez p0, :cond_0

    move v0, v1

    .line 1080362
    :goto_0
    return v0

    .line 1080363
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1080364
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1080365
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1080366
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1080367
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1080368
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1080369
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1080354
    const/4 v7, 0x0

    .line 1080355
    const/4 v1, 0x0

    .line 1080356
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1080357
    invoke-static {v2, v3, v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1080358
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1080359
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1080360
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1080353
    new-instance v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1080346
    if-eqz p0, :cond_0

    .line 1080347
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1080348
    if-eq v0, p0, :cond_0

    .line 1080349
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1080350
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1080341
    sparse-switch p2, :sswitch_data_0

    .line 1080342
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1080343
    :sswitch_0
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$ImportedPhoneEntriesModel$PrimaryFieldModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$ImportedPhoneEntriesModel$PrimaryFieldModel;

    .line 1080344
    invoke-static {v0, p3}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1080345
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x4309df98 -> :sswitch_1
        -0x20741a65 -> :sswitch_0
        0x15136e12 -> :sswitch_1
        0x4d8c57f1 -> :sswitch_1
        0x528ee1d9 -> :sswitch_1
        0x69f66839 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1080340
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1080418
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1080419
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1080335
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1080336
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1080337
    :cond_0
    iput-object p1, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1080338
    iput p2, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->b:I

    .line 1080339
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1080334
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1080333
    new-instance v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1080309
    iget v0, p0, LX/1vt;->c:I

    .line 1080310
    move v0, v0

    .line 1080311
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1080330
    iget v0, p0, LX/1vt;->c:I

    .line 1080331
    move v0, v0

    .line 1080332
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1080327
    iget v0, p0, LX/1vt;->b:I

    .line 1080328
    move v0, v0

    .line 1080329
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1080324
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1080325
    move-object v0, v0

    .line 1080326
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1080315
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1080316
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1080317
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1080318
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1080319
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1080320
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1080321
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1080322
    invoke-static {v3, v9, v2}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1080323
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1080312
    iget v0, p0, LX/1vt;->c:I

    .line 1080313
    move v0, v0

    .line 1080314
    return v0
.end method
