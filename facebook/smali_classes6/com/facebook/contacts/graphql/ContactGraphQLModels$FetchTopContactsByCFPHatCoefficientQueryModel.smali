.class public final Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3c167e57
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1081319
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1081318
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1081316
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1081317
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1081310
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1081311
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1081312
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1081313
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1081314
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1081315
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1081320
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1081321
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1081322
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;

    .line 1081323
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1081324
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;

    .line 1081325
    iput-object v0, v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;->e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;

    .line 1081326
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1081327
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessengerContacts"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1081308
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;->e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;->e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;

    .line 1081309
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;->e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1081305
    new-instance v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;

    invoke-direct {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;-><init>()V

    .line 1081306
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1081307
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1081304
    const v0, -0x7fd4fb0e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1081303
    const v0, -0x6747e1ce

    return v0
.end method
