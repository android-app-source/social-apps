.class public Lcom/facebook/contacts/graphql/ContactPhoneSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/contacts/graphql/ContactPhone;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1082119
    const-class v0, Lcom/facebook/contacts/graphql/ContactPhone;

    new-instance v1, Lcom/facebook/contacts/graphql/ContactPhoneSerializer;

    invoke-direct {v1}, Lcom/facebook/contacts/graphql/ContactPhoneSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1082120
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1082121
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/contacts/graphql/ContactPhone;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1082113
    if-nez p0, :cond_0

    .line 1082114
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1082115
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1082116
    invoke-static {p0, p1, p2}, Lcom/facebook/contacts/graphql/ContactPhoneSerializer;->b(Lcom/facebook/contacts/graphql/ContactPhone;LX/0nX;LX/0my;)V

    .line 1082117
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1082118
    return-void
.end method

.method private static b(Lcom/facebook/contacts/graphql/ContactPhone;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1082107
    const-string v0, "id"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/ContactPhone;->mId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1082108
    const-string v0, "label"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/ContactPhone;->mLabel:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1082109
    const-string v0, "displayNumber"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/ContactPhone;->mDisplayNumber:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1082110
    const-string v0, "universalNumber"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/ContactPhone;->mUniversalNumber:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1082111
    const-string v0, "isVerified"

    iget-boolean v1, p0, Lcom/facebook/contacts/graphql/ContactPhone;->mIsVerified:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1082112
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1082106
    check-cast p1, Lcom/facebook/contacts/graphql/ContactPhone;

    invoke-static {p1, p2, p3}, Lcom/facebook/contacts/graphql/ContactPhoneSerializer;->a(Lcom/facebook/contacts/graphql/ContactPhone;LX/0nX;LX/0my;)V

    return-void
.end method
