.class public Lcom/facebook/contacts/ContactSurfaceDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1078827
    const-class v0, Lcom/facebook/contacts/ContactSurface;

    new-instance v1, Lcom/facebook/contacts/ContactSurfaceDeserializer;

    invoke-direct {v1}, Lcom/facebook/contacts/ContactSurfaceDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1078828
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1078829
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1078830
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/contacts/ContactSurface;->fromString(Ljava/lang/String;)Lcom/facebook/contacts/ContactSurface;

    move-result-object v0

    return-object v0
.end method
