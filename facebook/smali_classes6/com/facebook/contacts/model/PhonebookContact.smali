.class public Lcom/facebook/contacts/model/PhonebookContact;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/model/PhonebookContact;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Z

.field public final l:Z

.field public final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/model/PhonebookPhoneNumber;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/model/PhonebookEmailAddress;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/model/PhonebookInstantMessaging;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/model/PhonebookNickname;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/model/PhonebookAddress;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/model/PhonebookWebsite;",
            ">;"
        }
    .end annotation
.end field

.field public final s:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/model/PhonebookRelation;",
            ">;"
        }
    .end annotation
.end field

.field public final t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/model/PhonebookOrganization;",
            ">;"
        }
    .end annotation
.end field

.field public final u:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/model/PhonebookEvent;",
            ">;"
        }
    .end annotation
.end field

.field public final v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/model/PhonebookContactMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public final w:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/model/PhonebookWhatsappProfile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1082848
    new-instance v0, LX/6NE;

    invoke-direct {v0}, LX/6NE;-><init>()V

    sput-object v0, Lcom/facebook/contacts/model/PhonebookContact;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6NG;)V
    .locals 1

    .prologue
    .line 1082823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1082824
    iget-object v0, p1, LX/6NG;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    .line 1082825
    iget-object v0, p1, LX/6NG;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->b:Ljava/lang/String;

    .line 1082826
    iget-object v0, p1, LX/6NG;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->c:Ljava/lang/String;

    .line 1082827
    iget-object v0, p1, LX/6NG;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->d:Ljava/lang/String;

    .line 1082828
    iget-object v0, p1, LX/6NG;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->e:Ljava/lang/String;

    .line 1082829
    iget-object v0, p1, LX/6NG;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->f:Ljava/lang/String;

    .line 1082830
    iget-object v0, p1, LX/6NG;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->g:Ljava/lang/String;

    .line 1082831
    iget-object v0, p1, LX/6NG;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->h:Ljava/lang/String;

    .line 1082832
    iget-object v0, p1, LX/6NG;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->i:Ljava/lang/String;

    .line 1082833
    iget-object v0, p1, LX/6NG;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->j:Ljava/lang/String;

    .line 1082834
    iget-boolean v0, p1, LX/6NG;->k:Z

    iput-boolean v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->k:Z

    .line 1082835
    iget-boolean v0, p1, LX/6NG;->l:Z

    iput-boolean v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->l:Z

    .line 1082836
    iget-object v0, p1, LX/6NG;->m:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->m:LX/0Px;

    .line 1082837
    iget-object v0, p1, LX/6NG;->n:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->n:LX/0Px;

    .line 1082838
    iget-object v0, p1, LX/6NG;->o:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->o:LX/0Px;

    .line 1082839
    iget-object v0, p1, LX/6NG;->p:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->p:LX/0Px;

    .line 1082840
    iget-object v0, p1, LX/6NG;->q:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->q:LX/0Px;

    .line 1082841
    iget-object v0, p1, LX/6NG;->r:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->r:LX/0Px;

    .line 1082842
    iget-object v0, p1, LX/6NG;->s:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->s:LX/0Px;

    .line 1082843
    iget-object v0, p1, LX/6NG;->t:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->t:LX/0Px;

    .line 1082844
    iget-object v0, p1, LX/6NG;->u:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->u:LX/0Px;

    .line 1082845
    iget-object v0, p1, LX/6NG;->v:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->v:LX/0Px;

    .line 1082846
    iget-object v0, p1, LX/6NG;->w:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->w:LX/0Px;

    .line 1082847
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1082877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1082878
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    .line 1082879
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->b:Ljava/lang/String;

    .line 1082880
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->c:Ljava/lang/String;

    .line 1082881
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->d:Ljava/lang/String;

    .line 1082882
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->e:Ljava/lang/String;

    .line 1082883
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->f:Ljava/lang/String;

    .line 1082884
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->g:Ljava/lang/String;

    .line 1082885
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->h:Ljava/lang/String;

    .line 1082886
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->i:Ljava/lang/String;

    .line 1082887
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->j:Ljava/lang/String;

    .line 1082888
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->k:Z

    .line 1082889
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->l:Z

    .line 1082890
    const-class v0, Lcom/facebook/contacts/model/PhonebookPhoneNumber;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1082891
    if-nez v1, :cond_0

    .line 1082892
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1082893
    :goto_0
    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->m:LX/0Px;

    .line 1082894
    const-class v0, Lcom/facebook/contacts/model/PhonebookEmailAddress;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1082895
    if-nez v0, :cond_1

    .line 1082896
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1082897
    :goto_1
    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->n:LX/0Px;

    .line 1082898
    const-class v0, Lcom/facebook/contacts/model/PhonebookInstantMessaging;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1082899
    if-nez v0, :cond_2

    .line 1082900
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1082901
    :goto_2
    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->o:LX/0Px;

    .line 1082902
    const-class v0, Lcom/facebook/contacts/model/PhonebookNickname;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1082903
    if-nez v0, :cond_3

    .line 1082904
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1082905
    :goto_3
    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->p:LX/0Px;

    .line 1082906
    const-class v0, Lcom/facebook/contacts/model/PhonebookAddress;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1082907
    if-nez v0, :cond_4

    .line 1082908
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1082909
    :goto_4
    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->q:LX/0Px;

    .line 1082910
    const-class v0, Lcom/facebook/contacts/model/PhonebookWebsite;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1082911
    if-nez v0, :cond_5

    .line 1082912
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1082913
    :goto_5
    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->r:LX/0Px;

    .line 1082914
    const-class v0, Lcom/facebook/contacts/model/PhonebookRelation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1082915
    if-nez v1, :cond_6

    .line 1082916
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1082917
    :goto_6
    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->s:LX/0Px;

    .line 1082918
    const-class v0, Lcom/facebook/contacts/model/PhonebookOrganization;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1082919
    if-nez v0, :cond_7

    .line 1082920
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1082921
    :goto_7
    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->t:LX/0Px;

    .line 1082922
    const-class v0, Lcom/facebook/contacts/model/PhonebookEvent;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1082923
    if-nez v0, :cond_8

    .line 1082924
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1082925
    :goto_8
    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->u:LX/0Px;

    .line 1082926
    const-class v0, Lcom/facebook/contacts/model/PhonebookContactMetadata;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1082927
    if-nez v0, :cond_9

    .line 1082928
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1082929
    :goto_9
    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->v:LX/0Px;

    .line 1082930
    const-class v0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1082931
    if-nez v0, :cond_a

    .line 1082932
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1082933
    :goto_a
    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->w:LX/0Px;

    .line 1082934
    return-void

    .line 1082935
    :cond_0
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 1082936
    :cond_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto/16 :goto_1

    .line 1082937
    :cond_2
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto/16 :goto_2

    .line 1082938
    :cond_3
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto/16 :goto_3

    .line 1082939
    :cond_4
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto/16 :goto_4

    .line 1082940
    :cond_5
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_5

    .line 1082941
    :cond_6
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_6

    .line 1082942
    :cond_7
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_7

    .line 1082943
    :cond_8
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_8

    .line 1082944
    :cond_9
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_9

    .line 1082945
    :cond_a
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_a
.end method

.method private static a(Ljava/util/List;)LX/0Px;
    .locals 2

    .prologue
    .line 1082874
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1082875
    new-instance v1, LX/6NF;

    invoke-direct {v1}, LX/6NF;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1082876
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1082946
    const/4 v0, 0x0

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1082873
    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->m:LX/0Px;

    invoke-static {v2}, Lcom/facebook/contacts/model/PhonebookContact;->a(Ljava/util/List;)LX/0Px;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->n:LX/0Px;

    invoke-static {v2}, Lcom/facebook/contacts/model/PhonebookContact;->a(Ljava/util/List;)LX/0Px;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->o:LX/0Px;

    invoke-static {v2}, Lcom/facebook/contacts/model/PhonebookContact;->a(Ljava/util/List;)LX/0Px;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->p:LX/0Px;

    invoke-static {v2}, Lcom/facebook/contacts/model/PhonebookContact;->a(Ljava/util/List;)LX/0Px;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->q:LX/0Px;

    invoke-static {v2}, Lcom/facebook/contacts/model/PhonebookContact;->a(Ljava/util/List;)LX/0Px;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->r:LX/0Px;

    invoke-static {v2}, Lcom/facebook/contacts/model/PhonebookContact;->a(Ljava/util/List;)LX/0Px;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->s:LX/0Px;

    invoke-static {v2}, Lcom/facebook/contacts/model/PhonebookContact;->a(Ljava/util/List;)LX/0Px;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->t:LX/0Px;

    invoke-static {v2}, Lcom/facebook/contacts/model/PhonebookContact;->a(Ljava/util/List;)LX/0Px;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->u:LX/0Px;

    invoke-static {v2}, Lcom/facebook/contacts/model/PhonebookContact;->a(Ljava/util/List;)LX/0Px;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->v:LX/0Px;

    invoke-static {v2}, Lcom/facebook/contacts/model/PhonebookContact;->a(Ljava/util/List;)LX/0Px;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContact;->w:LX/0Px;

    invoke-static {v2}, Lcom/facebook/contacts/model/PhonebookContact;->a(Ljava/util/List;)LX/0Px;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1082849
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1082850
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1082851
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1082852
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1082853
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1082854
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1082855
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1082856
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1082857
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1082858
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1082859
    iget-boolean v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->k:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1082860
    iget-boolean v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->l:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1082861
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->m:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1082862
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->n:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1082863
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->o:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1082864
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->p:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1082865
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->q:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1082866
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->r:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1082867
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->s:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1082868
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->t:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1082869
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->u:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1082870
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->v:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1082871
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookContact;->w:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1082872
    return-void
.end method
