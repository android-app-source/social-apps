.class public Lcom/facebook/contacts/model/PhonebookWhatsappProfile;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/model/PhonebookWhatsappProfile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1083167
    new-instance v0, LX/6NR;

    invoke-direct {v0}, LX/6NR;-><init>()V

    sput-object v0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1083168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1083169
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->a:Ljava/lang/String;

    .line 1083170
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->b:Ljava/lang/String;

    .line 1083171
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1083172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1083173
    iput-object p1, p0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->a:Ljava/lang/String;

    .line 1083174
    iput-object p2, p0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->b:Ljava/lang/String;

    .line 1083175
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1083176
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1083177
    if-ne p0, p1, :cond_1

    .line 1083178
    :cond_0
    :goto_0
    return v0

    .line 1083179
    :cond_1
    instance-of v2, p1, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;

    if-nez v2, :cond_2

    move v0, v1

    .line 1083180
    goto :goto_0

    .line 1083181
    :cond_2
    check-cast p1, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;

    .line 1083182
    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1083183
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1083184
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1083185
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1083186
    return-void
.end method
