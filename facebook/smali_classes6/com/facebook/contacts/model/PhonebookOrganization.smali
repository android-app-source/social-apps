.class public Lcom/facebook/contacts/model/PhonebookOrganization;
.super Lcom/facebook/contacts/model/PhonebookContactField;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/model/PhonebookOrganization;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1083068
    new-instance v0, LX/6NN;

    invoke-direct {v0}, LX/6NN;-><init>()V

    sput-object v0, Lcom/facebook/contacts/model/PhonebookOrganization;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1083069
    invoke-direct {p0, p1}, Lcom/facebook/contacts/model/PhonebookContactField;-><init>(Landroid/os/Parcel;)V

    .line 1083070
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->a:Ljava/lang/String;

    .line 1083071
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->b:Ljava/lang/String;

    .line 1083072
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->c:Ljava/lang/String;

    .line 1083073
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->d:Ljava/lang/String;

    .line 1083074
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->e:Ljava/lang/String;

    .line 1083075
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->f:Ljava/lang/String;

    .line 1083076
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->g:Ljava/lang/String;

    .line 1083077
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1083078
    invoke-direct {p0, p2, p3}, Lcom/facebook/contacts/model/PhonebookContactField;-><init>(ILjava/lang/String;)V

    .line 1083079
    iput-object p1, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->a:Ljava/lang/String;

    .line 1083080
    iput-object p4, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->b:Ljava/lang/String;

    .line 1083081
    iput-object p5, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->c:Ljava/lang/String;

    .line 1083082
    iput-object p6, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->d:Ljava/lang/String;

    .line 1083083
    iput-object p7, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->e:Ljava/lang/String;

    .line 1083084
    iput-object p8, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->f:Ljava/lang/String;

    .line 1083085
    iput-object p9, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->g:Ljava/lang/String;

    .line 1083086
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1083087
    invoke-super {p0, p1}, Lcom/facebook/contacts/model/PhonebookContactField;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/facebook/contacts/model/PhonebookOrganization;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->a:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/facebook/contacts/model/PhonebookOrganization;

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookOrganization;->a:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->b:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/facebook/contacts/model/PhonebookOrganization;

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookOrganization;->b:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->c:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/facebook/contacts/model/PhonebookOrganization;

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookOrganization;->c:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->d:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/facebook/contacts/model/PhonebookOrganization;

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookOrganization;->d:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->e:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/facebook/contacts/model/PhonebookOrganization;

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookOrganization;->e:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->f:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/facebook/contacts/model/PhonebookOrganization;

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookOrganization;->f:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->g:Ljava/lang/String;

    check-cast p1, Lcom/facebook/contacts/model/PhonebookOrganization;

    iget-object v1, p1, Lcom/facebook/contacts/model/PhonebookOrganization;->g:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1083088
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/contacts/model/PhonebookContactField;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookContactField;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1083089
    invoke-super {p0, p1, p2}, Lcom/facebook/contacts/model/PhonebookContactField;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1083090
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1083091
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1083092
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1083093
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1083094
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1083095
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1083096
    iget-object v0, p0, Lcom/facebook/contacts/model/PhonebookOrganization;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1083097
    return-void
.end method
