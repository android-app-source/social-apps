.class public final enum Lcom/facebook/contacts/ContactSurface;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/ContactSurfaceDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/contacts/ContactSurface;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/contacts/ContactSurface;

.field public static final enum GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

.field public static final enum MESSENGER:Lcom/facebook/contacts/ContactSurface;

.field public static final enum OTHERS:Lcom/facebook/contacts/ContactSurface;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1078813
    const-class v0, Lcom/facebook/contacts/ContactSurfaceDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1078814
    new-instance v0, Lcom/facebook/contacts/ContactSurface;

    const-string v1, "GROWTH_CONTACT_IMPORTER"

    invoke-direct {v0, v1, v2}, Lcom/facebook/contacts/ContactSurface;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    .line 1078815
    new-instance v0, Lcom/facebook/contacts/ContactSurface;

    const-string v1, "MESSENGER"

    invoke-direct {v0, v1, v3}, Lcom/facebook/contacts/ContactSurface;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/contacts/ContactSurface;->MESSENGER:Lcom/facebook/contacts/ContactSurface;

    .line 1078816
    new-instance v0, Lcom/facebook/contacts/ContactSurface;

    const-string v1, "OTHERS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/contacts/ContactSurface;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/contacts/ContactSurface;->OTHERS:Lcom/facebook/contacts/ContactSurface;

    .line 1078817
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/contacts/ContactSurface;

    sget-object v1, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/contacts/ContactSurface;->MESSENGER:Lcom/facebook/contacts/ContactSurface;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/contacts/ContactSurface;->OTHERS:Lcom/facebook/contacts/ContactSurface;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/contacts/ContactSurface;->$VALUES:[Lcom/facebook/contacts/ContactSurface;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1078818
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/contacts/ContactSurface;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonCreator;
    .end annotation

    .prologue
    .line 1078819
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    invoke-virtual {v0}, Lcom/facebook/contacts/ContactSurface;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1078820
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    .line 1078821
    :goto_0
    return-object v0

    .line 1078822
    :cond_0
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->MESSENGER:Lcom/facebook/contacts/ContactSurface;

    invoke-virtual {v0}, Lcom/facebook/contacts/ContactSurface;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1078823
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->MESSENGER:Lcom/facebook/contacts/ContactSurface;

    goto :goto_0

    .line 1078824
    :cond_1
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->OTHERS:Lcom/facebook/contacts/ContactSurface;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/contacts/ContactSurface;
    .locals 1

    .prologue
    .line 1078825
    const-class v0, Lcom/facebook/contacts/ContactSurface;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/ContactSurface;

    return-object v0
.end method

.method public static values()[Lcom/facebook/contacts/ContactSurface;
    .locals 1

    .prologue
    .line 1078826
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->$VALUES:[Lcom/facebook/contacts/ContactSurface;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/contacts/ContactSurface;

    return-object v0
.end method
