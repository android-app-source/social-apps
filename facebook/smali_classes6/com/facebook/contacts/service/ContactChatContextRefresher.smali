.class public Lcom/facebook/contacts/service/ContactChatContextRefresher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile e:Lcom/facebook/contacts/service/ContactChatContextRefresher;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3LK;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0aG;

.field public final d:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1085325
    const-class v0, Lcom/facebook/contacts/service/ContactChatContextRefresher;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/contacts/service/ContactChatContextRefresher;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0aG;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/3LK;",
            ">;",
            "LX/0aG;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1085326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085327
    iput-object p1, p0, Lcom/facebook/contacts/service/ContactChatContextRefresher;->b:LX/0Or;

    .line 1085328
    iput-object p2, p0, Lcom/facebook/contacts/service/ContactChatContextRefresher;->c:LX/0aG;

    .line 1085329
    iput-object p3, p0, Lcom/facebook/contacts/service/ContactChatContextRefresher;->d:LX/0Uh;

    .line 1085330
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/contacts/service/ContactChatContextRefresher;
    .locals 6

    .prologue
    .line 1085331
    sget-object v0, Lcom/facebook/contacts/service/ContactChatContextRefresher;->e:Lcom/facebook/contacts/service/ContactChatContextRefresher;

    if-nez v0, :cond_1

    .line 1085332
    const-class v1, Lcom/facebook/contacts/service/ContactChatContextRefresher;

    monitor-enter v1

    .line 1085333
    :try_start_0
    sget-object v0, Lcom/facebook/contacts/service/ContactChatContextRefresher;->e:Lcom/facebook/contacts/service/ContactChatContextRefresher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1085334
    if-eqz v2, :cond_0

    .line 1085335
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1085336
    new-instance v5, Lcom/facebook/contacts/service/ContactChatContextRefresher;

    const/16 v3, 0x3f9

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {v5, p0, v3, v4}, Lcom/facebook/contacts/service/ContactChatContextRefresher;-><init>(LX/0Or;LX/0aG;LX/0Uh;)V

    .line 1085337
    move-object v0, v5

    .line 1085338
    sput-object v0, Lcom/facebook/contacts/service/ContactChatContextRefresher;->e:Lcom/facebook/contacts/service/ContactChatContextRefresher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1085339
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1085340
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1085341
    :cond_1
    sget-object v0, Lcom/facebook/contacts/service/ContactChatContextRefresher;->e:Lcom/facebook/contacts/service/ContactChatContextRefresher;

    return-object v0

    .line 1085342
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1085343
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
