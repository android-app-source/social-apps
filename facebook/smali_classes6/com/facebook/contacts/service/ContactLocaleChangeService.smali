.class public Lcom/facebook/contacts/service/ContactLocaleChangeService;
.super LX/1ZN;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/contacts/service/ContactLocaleChangeService;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private c:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1085357
    const-class v0, Lcom/facebook/contacts/service/ContactLocaleChangeService;

    .line 1085358
    sput-object v0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->a:Ljava/lang/Class;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1085359
    sget-object v0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1085360
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1085361
    iput-object v0, p0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->d:LX/0Ot;

    .line 1085362
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1085363
    iput-object v0, p0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->e:LX/0Ot;

    .line 1085364
    return-void
.end method

.method private static a(Lcom/facebook/contacts/service/ContactLocaleChangeService;LX/0Or;LX/0Ot;LX/0Ot;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/contacts/service/ContactLocaleChangeService;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1085365
    iput-object p1, p0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->c:LX/0Or;

    iput-object p2, p0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->d:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->e:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->f:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/contacts/service/ContactLocaleChangeService;

    const/16 v1, 0x12cb

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x542

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x29f

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x438

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-static {p0, v1, v2, v3, v0}, Lcom/facebook/contacts/service/ContactLocaleChangeService;->a(Lcom/facebook/contacts/service/ContactLocaleChangeService;LX/0Or;LX/0Ot;LX/0Ot;LX/0Or;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x505941f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 1085366
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uq;

    invoke-virtual {v0}, LX/0Uq;->b()V

    .line 1085367
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1085368
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1085369
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "mark_full_contact_sync_required"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/contacts/service/ContactLocaleChangeService;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x6fdd50e8

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0, v7}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 1085370
    sget-object v1, LX/6Oe;->a:[I

    iget-object v0, p0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Jp;

    invoke-virtual {v0}, LX/2Jp;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1085371
    :cond_0
    :goto_0
    const v0, -0x6d41f16f

    invoke-static {v0, v6}, LX/02F;->d(II)V

    return-void

    .line 1085372
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "sync_contacts_partial"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/contacts/service/ContactLocaleChangeService;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x6ecd98a2

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0, v7}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    goto :goto_0

    .line 1085373
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactLocaleChangeService;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "reindex_omnistore_contacts"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/contacts/service/ContactLocaleChangeService;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x45bdf2a2

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0, v7}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x702e3807

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1085374
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1085375
    invoke-static {p0, p0}, Lcom/facebook/contacts/service/ContactLocaleChangeService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1085376
    const/16 v1, 0x25

    const v2, -0x753fe151

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
