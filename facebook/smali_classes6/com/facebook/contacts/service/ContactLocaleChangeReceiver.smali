.class public Lcom/facebook/contacts/service/ContactLocaleChangeReceiver;
.super LX/0Yd;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1085353
    const-class v0, Lcom/facebook/contacts/service/ContactLocaleChangeReceiver;

    sput-object v0, Lcom/facebook/contacts/service/ContactLocaleChangeReceiver;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1085354
    invoke-static {}, Lcom/facebook/contacts/service/ContactLocaleChangeReceiver;->c()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0Yd;-><init>(Ljava/util/Map;)V

    .line 1085355
    return-void
.end method

.method private static c()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0YZ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1085351
    new-instance v0, LX/6Od;

    invoke-direct {v0}, LX/6Od;-><init>()V

    .line 1085352
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method
