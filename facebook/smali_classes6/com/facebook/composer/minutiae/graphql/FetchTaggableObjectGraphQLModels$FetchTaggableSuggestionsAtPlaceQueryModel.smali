.class public final Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/5LG;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x658b0cf7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 900467
    const-class v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 900468
    const-class v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 900469
    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 900470
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 900471
    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 900472
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 900473
    return-void
.end method


# virtual methods
.method public final synthetic A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900474
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->C()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic B()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900475
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    move-result-object v0

    return-object v0
.end method

.method public final C()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900476
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->f:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->f:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    .line 900477
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->f:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    return-object v0
.end method

.method public final D()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900478
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->g:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->g:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    .line 900479
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->g:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    return-object v0
.end method

.method public final E()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900480
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900481
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method public final F()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900458
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900459
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method public final G()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900482
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->o:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->o:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900483
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->o:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method public final H()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900484
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->p:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->p:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900485
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->p:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method public final I()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900486
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->q:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->q:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900487
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->q:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method public final J()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900488
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->r:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->r:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900489
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->r:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method public final K()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900490
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->w:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->w:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    .line 900491
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->w:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 16

    .prologue
    .line 900492
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 900493
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 900494
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->C()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 900495
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->D()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 900496
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->j()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 900497
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->l()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 900498
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->n()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 900499
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->E()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 900500
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->F()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 900501
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->G()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 900502
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->H()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 900503
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->I()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 900504
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->J()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 900505
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 900506
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->K()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 900507
    const/16 v15, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->c(I)V

    .line 900508
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v1}, LX/186;->b(II)V

    .line 900509
    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 900510
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 900511
    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 900512
    const/4 v1, 0x4

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 900513
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 900514
    const/4 v1, 0x6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->k:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 900515
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 900516
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 900517
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 900518
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 900519
    const/16 v1, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 900520
    const/16 v1, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 900521
    const/16 v1, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 900522
    const/16 v1, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 900523
    const/16 v1, 0xf

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->t:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 900524
    const/16 v1, 0x10

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->u:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 900525
    const/16 v1, 0x11

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->v:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 900526
    const/16 v1, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v14}, LX/186;->b(II)V

    .line 900527
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 900528
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    return v1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 900529
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 900530
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 900531
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    .line 900532
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 900533
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    .line 900534
    iput-object v0, v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->e:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    .line 900535
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->C()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 900536
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->C()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    .line 900537
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->C()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 900538
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    .line 900539
    iput-object v0, v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->f:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    .line 900540
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->D()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 900541
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->D()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    .line 900542
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->D()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 900543
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    .line 900544
    iput-object v0, v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->g:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    .line 900545
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->E()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 900546
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->E()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900547
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->E()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 900548
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    .line 900549
    iput-object v0, v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900550
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->F()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 900551
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->F()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900552
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->F()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 900553
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    .line 900554
    iput-object v0, v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900555
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->G()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 900556
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->G()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900557
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->G()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 900558
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    .line 900559
    iput-object v0, v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->o:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900560
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->H()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 900561
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->H()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900562
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->H()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 900563
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    .line 900564
    iput-object v0, v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->p:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900565
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->I()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 900566
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->I()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900567
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->I()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 900568
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    .line 900569
    iput-object v0, v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->q:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900570
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->J()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 900571
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->J()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900572
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->J()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 900573
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    .line 900574
    iput-object v0, v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->r:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 900575
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->K()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 900576
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->K()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    .line 900577
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->K()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 900578
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    .line 900579
    iput-object v0, v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->w:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    .line 900580
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 900581
    if-nez v1, :cond_a

    :goto_0
    return-object p0

    :cond_a
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900582
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 900460
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 900461
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->i:Z

    .line 900462
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->k:I

    .line 900463
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->t:Z

    .line 900464
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->u:Z

    .line 900465
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->v:Z

    .line 900466
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 900583
    new-instance v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    invoke-direct {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;-><init>()V

    .line 900584
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 900585
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 900429
    const v0, -0x59815247

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 900430
    const v0, -0xe40ca

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900431
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->h:Ljava/lang/String;

    .line 900432
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 900433
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 900434
    iget-boolean v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->i:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900435
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->j:Ljava/lang/String;

    .line 900436
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final m()I
    .locals 2

    .prologue
    .line 900437
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 900438
    iget v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->k:I

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900439
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->l:Ljava/lang/String;

    .line 900440
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900441
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->s:Ljava/lang/String;

    .line 900442
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 900443
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 900444
    iget-boolean v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->t:Z

    return v0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 900445
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 900446
    iget-boolean v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->u:Z

    return v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 900447
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 900448
    iget-boolean v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->v:Z

    return v0
.end method

.method public final s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900449
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->e:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->e:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    .line 900450
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->e:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    return-object v0
.end method

.method public final synthetic t()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900451
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->J()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900452
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->I()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic v()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900453
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->H()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic w()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900454
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->G()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic x()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900455
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->F()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900456
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->E()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900457
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->D()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v0

    return-object v0
.end method
