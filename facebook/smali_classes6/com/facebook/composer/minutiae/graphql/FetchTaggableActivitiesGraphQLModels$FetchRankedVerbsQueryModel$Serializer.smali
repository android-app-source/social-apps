.class public final Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 899604
    const-class v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel;

    new-instance v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 899605
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 899606
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 899607
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 899608
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 899609
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 899610
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 899611
    if-eqz v2, :cond_2

    .line 899612
    const-string p0, "minutiae_suggestions"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899613
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 899614
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 899615
    if-eqz p0, :cond_1

    .line 899616
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899617
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 899618
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 899619
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v2

    invoke-static {v1, v2, p1, p2}, LX/5L8;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 899620
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 899621
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 899622
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 899623
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 899624
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 899625
    check-cast p1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$Serializer;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
