.class public final Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 901810
    const-class v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel;

    new-instance v1, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 901811
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 901809
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 901760
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 901761
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 901762
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 901763
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 901764
    if-eqz v2, :cond_0

    .line 901765
    const-string v3, "glyph"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901766
    invoke-static {v1, v2, p1}, LX/5Lj;->a(LX/15i;ILX/0nX;)V

    .line 901767
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 901768
    if-eqz v2, :cond_1

    .line 901769
    const-string v3, "iconImageLarge"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901770
    invoke-static {v1, v2, p1}, LX/5Lk;->a(LX/15i;ILX/0nX;)V

    .line 901771
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 901772
    if-eqz v2, :cond_2

    .line 901773
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901774
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 901775
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 901776
    if-eqz v2, :cond_3

    .line 901777
    const-string v3, "is_linking_verb"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901778
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 901779
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 901780
    if-eqz v2, :cond_4

    .line 901781
    const-string v3, "legacy_api_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901782
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 901783
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 901784
    if-eqz v2, :cond_5

    .line 901785
    const-string v3, "prefetch_priority"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901786
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 901787
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 901788
    if-eqz v2, :cond_6

    .line 901789
    const-string v3, "present_participle"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901790
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 901791
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 901792
    if-eqz v2, :cond_7

    .line 901793
    const-string v3, "prompt"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901794
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 901795
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 901796
    if-eqz v2, :cond_8

    .line 901797
    const-string v3, "supports_audio_suggestions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901798
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 901799
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 901800
    if-eqz v2, :cond_9

    .line 901801
    const-string v3, "supports_freeform"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901802
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 901803
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 901804
    if-eqz v2, :cond_a

    .line 901805
    const-string v3, "supports_offline_posting"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901806
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 901807
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 901808
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 901759
    check-cast p1, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$Serializer;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
