.class public final Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 901919
    const-class v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel;

    new-instance v1, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 901920
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 901921
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 901922
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 901923
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 901924
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 901925
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 901926
    if-eqz v2, :cond_2

    .line 901927
    const-string p0, "all_icons"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901928
    const/4 p0, 0x0

    .line 901929
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 901930
    invoke-virtual {v1, v2, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 901931
    if-eqz p0, :cond_0

    .line 901932
    const-string v0, "count"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901933
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 901934
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 901935
    if-eqz p0, :cond_1

    .line 901936
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901937
    invoke-static {v1, p0, p1, p2}, LX/5Lg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 901938
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 901939
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 901940
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 901941
    check-cast p1, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel$Serializer;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityIconsModel;LX/0nX;LX/0my;)V

    return-void
.end method
