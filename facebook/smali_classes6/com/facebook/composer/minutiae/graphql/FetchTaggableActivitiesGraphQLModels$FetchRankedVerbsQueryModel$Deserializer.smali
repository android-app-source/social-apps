.class public final Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 899509
    const-class v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel;

    new-instance v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 899510
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 899554
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 899511
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 899512
    const/4 v2, 0x0

    .line 899513
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 899514
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 899515
    :goto_0
    move v1, v2

    .line 899516
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 899517
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 899518
    new-instance v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel;

    invoke-direct {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel;-><init>()V

    .line 899519
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 899520
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 899521
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 899522
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 899523
    :cond_0
    return-object v1

    .line 899524
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 899525
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 899526
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 899527
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 899528
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, p0, :cond_2

    if-eqz v3, :cond_2

    .line 899529
    const-string v4, "minutiae_suggestions"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 899530
    const/4 v3, 0x0

    .line 899531
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 899532
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 899533
    :goto_2
    move v1, v3

    .line 899534
    goto :goto_1

    .line 899535
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 899536
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 899537
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 899538
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 899539
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_8

    .line 899540
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 899541
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 899542
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v4, :cond_6

    .line 899543
    const-string p0, "nodes"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 899544
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 899545
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, p0, :cond_7

    .line 899546
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, p0, :cond_7

    .line 899547
    invoke-static {p1, v0}, LX/5L8;->b(LX/15w;LX/186;)I

    move-result v4

    .line 899548
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 899549
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 899550
    goto :goto_3

    .line 899551
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 899552
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 899553
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3
.end method
