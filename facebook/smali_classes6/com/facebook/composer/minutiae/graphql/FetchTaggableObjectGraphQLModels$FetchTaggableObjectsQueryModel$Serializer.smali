.class public final Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 900140
    const-class v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;

    new-instance v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 900141
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 900142
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 900143
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 900144
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 900145
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 900146
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 900147
    if-eqz v2, :cond_0

    .line 900148
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 900149
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 900150
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 900151
    if-eqz v2, :cond_1

    .line 900152
    const-string p0, "iconImageLarge"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 900153
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 900154
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 900155
    if-eqz v2, :cond_2

    .line 900156
    const-string p0, "previewTemplateAtPlace"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 900157
    invoke-static {v1, v2, p1, p2}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 900158
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 900159
    if-eqz v2, :cond_3

    .line 900160
    const-string p0, "previewTemplateNoTags"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 900161
    invoke-static {v1, v2, p1, p2}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 900162
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 900163
    if-eqz v2, :cond_4

    .line 900164
    const-string p0, "previewTemplateWithPeople"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 900165
    invoke-static {v1, v2, p1, p2}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 900166
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 900167
    if-eqz v2, :cond_5

    .line 900168
    const-string p0, "previewTemplateWithPeopleAtPlace"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 900169
    invoke-static {v1, v2, p1, p2}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 900170
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 900171
    if-eqz v2, :cond_6

    .line 900172
    const-string p0, "previewTemplateWithPerson"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 900173
    invoke-static {v1, v2, p1, p2}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 900174
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 900175
    if-eqz v2, :cond_7

    .line 900176
    const-string p0, "previewTemplateWithPersonAtPlace"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 900177
    invoke-static {v1, v2, p1, p2}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 900178
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 900179
    if-eqz v2, :cond_8

    .line 900180
    const-string p0, "taggable_activity_suggestions"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 900181
    invoke-static {v1, v2, p1, p2}, LX/5LP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 900182
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 900183
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 900184
    check-cast p1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel$Serializer;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
