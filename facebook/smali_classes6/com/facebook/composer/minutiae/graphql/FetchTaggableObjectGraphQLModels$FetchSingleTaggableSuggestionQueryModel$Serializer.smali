.class public final Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchSingleTaggableSuggestionQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchSingleTaggableSuggestionQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 899849
    const-class v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchSingleTaggableSuggestionQueryModel;

    new-instance v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchSingleTaggableSuggestionQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchSingleTaggableSuggestionQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 899850
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 899933
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchSingleTaggableSuggestionQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 899852
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 899853
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 899854
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 899855
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 899856
    if-eqz v2, :cond_0

    .line 899857
    const-string v3, "all_icons"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899858
    invoke-static {v1, v2, p1}, LX/5Lm;->a(LX/15i;ILX/0nX;)V

    .line 899859
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 899860
    if-eqz v2, :cond_1

    .line 899861
    const-string v3, "glyph"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899862
    invoke-static {v1, v2, p1}, LX/5Lj;->a(LX/15i;ILX/0nX;)V

    .line 899863
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 899864
    if-eqz v2, :cond_2

    .line 899865
    const-string v3, "iconImageLarge"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899866
    invoke-static {v1, v2, p1}, LX/5Lk;->a(LX/15i;ILX/0nX;)V

    .line 899867
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 899868
    if-eqz v2, :cond_3

    .line 899869
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899870
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 899871
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 899872
    if-eqz v2, :cond_4

    .line 899873
    const-string v3, "is_linking_verb"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899874
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 899875
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 899876
    if-eqz v2, :cond_5

    .line 899877
    const-string v3, "legacy_api_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899878
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 899879
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 899880
    if-eqz v2, :cond_6

    .line 899881
    const-string v3, "prefetch_priority"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899882
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 899883
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 899884
    if-eqz v2, :cond_7

    .line 899885
    const-string v3, "present_participle"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899886
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 899887
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 899888
    if-eqz v2, :cond_8

    .line 899889
    const-string v3, "previewTemplateAtPlace"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899890
    invoke-static {v1, v2, p1, p2}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 899891
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 899892
    if-eqz v2, :cond_9

    .line 899893
    const-string v3, "previewTemplateNoTags"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899894
    invoke-static {v1, v2, p1, p2}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 899895
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 899896
    if-eqz v2, :cond_a

    .line 899897
    const-string v3, "previewTemplateWithPeople"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899898
    invoke-static {v1, v2, p1, p2}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 899899
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 899900
    if-eqz v2, :cond_b

    .line 899901
    const-string v3, "previewTemplateWithPeopleAtPlace"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899902
    invoke-static {v1, v2, p1, p2}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 899903
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 899904
    if-eqz v2, :cond_c

    .line 899905
    const-string v3, "previewTemplateWithPerson"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899906
    invoke-static {v1, v2, p1, p2}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 899907
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 899908
    if-eqz v2, :cond_d

    .line 899909
    const-string v3, "previewTemplateWithPersonAtPlace"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899910
    invoke-static {v1, v2, p1, p2}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 899911
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 899912
    if-eqz v2, :cond_e

    .line 899913
    const-string v3, "prompt"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899914
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 899915
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 899916
    if-eqz v2, :cond_f

    .line 899917
    const-string v3, "supports_audio_suggestions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899918
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 899919
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 899920
    if-eqz v2, :cond_10

    .line 899921
    const-string v3, "supports_freeform"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899922
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 899923
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 899924
    if-eqz v2, :cond_11

    .line 899925
    const-string v3, "supports_offline_posting"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899926
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 899927
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 899928
    if-eqz v2, :cond_12

    .line 899929
    const-string v3, "taggable_activity_suggestions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899930
    invoke-static {v1, v2, p1, p2}, LX/5LP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 899931
    :cond_12
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 899932
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 899851
    check-cast p1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchSingleTaggableSuggestionQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchSingleTaggableSuggestionQueryModel$Serializer;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchSingleTaggableSuggestionQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
