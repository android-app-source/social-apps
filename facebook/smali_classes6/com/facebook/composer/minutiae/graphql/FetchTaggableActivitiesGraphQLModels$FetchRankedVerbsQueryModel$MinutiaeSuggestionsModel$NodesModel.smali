.class public final Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7daa23c1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 899603
    const-class v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 899602
    const-class v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 899600
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 899601
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 899592
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 899593
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 899594
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 899595
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 899596
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 899597
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 899598
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 899599
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 899584
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 899585
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 899586
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 899587
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 899588
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;

    .line 899589
    iput-object v0, v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;->f:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 899590
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 899591
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 899582
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;->e:Ljava/lang/String;

    .line 899583
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 899579
    new-instance v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;-><init>()V

    .line 899580
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 899581
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 899575
    const v0, -0x6d0356db

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 899578
    const v0, -0x39812c80    # -16308.875f

    return v0
.end method

.method public final j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 899576
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;->f:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;->f:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 899577
    iget-object v0, p0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel$MinutiaeSuggestionsModel$NodesModel;->f:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    return-object v0
.end method
