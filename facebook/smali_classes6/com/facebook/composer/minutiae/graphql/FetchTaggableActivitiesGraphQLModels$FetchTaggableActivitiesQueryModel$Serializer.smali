.class public final Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchTaggableActivitiesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchTaggableActivitiesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 899688
    const-class v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchTaggableActivitiesQueryModel;

    new-instance v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchTaggableActivitiesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchTaggableActivitiesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 899689
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 899704
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchTaggableActivitiesQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 899691
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 899692
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 899693
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 899694
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 899695
    if-eqz v2, :cond_1

    .line 899696
    const-string p0, "taggable_activities"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 899697
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 899698
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_0

    .line 899699
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/5Ln;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 899700
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 899701
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 899702
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 899703
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 899690
    check-cast p1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchTaggableActivitiesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchTaggableActivitiesQueryModel$Serializer;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchTaggableActivitiesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
