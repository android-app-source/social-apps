.class public Lcom/facebook/composer/minutiae/model/MinutiaeObjectSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 903710
    const-class v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    new-instance v1, Lcom/facebook/composer/minutiae/model/MinutiaeObjectSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/minutiae/model/MinutiaeObjectSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 903711
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 903712
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 903713
    if-nez p0, :cond_0

    .line 903714
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 903715
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 903716
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/minutiae/model/MinutiaeObjectSerializer;->b(Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/0nX;LX/0my;)V

    .line 903717
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 903718
    return-void
.end method

.method private static b(Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 903719
    const-string v0, "verb"

    iget-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 903720
    const-string v0, "object"

    iget-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 903721
    const-string v0, "custom_icon"

    iget-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 903722
    const-string v0, "suggestion_mechanism"

    iget-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->suggestionMechanism:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 903723
    const-string v0, "hide_attachment"

    iget-boolean v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->hideAttachment:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 903724
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 903725
    check-cast p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/minutiae/model/MinutiaeObjectSerializer;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/0nX;LX/0my;)V

    return-void
.end method
