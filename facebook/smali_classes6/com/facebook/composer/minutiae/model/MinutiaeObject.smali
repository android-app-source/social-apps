.class public Lcom/facebook/composer/minutiae/model/MinutiaeObject;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/minutiae/model/MinutiaeObjectDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/minutiae/model/MinutiaeObjectSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "custom_icon"
    .end annotation
.end field

.field public final hideAttachment:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "hide_attachment"
    .end annotation
.end field

.field public final object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "object"
    .end annotation
.end field

.field public final suggestionMechanism:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "suggestion_mechanism"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "verb"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 903457
    const-class v0, Lcom/facebook/composer/minutiae/model/MinutiaeObjectDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 903684
    const-class v0, Lcom/facebook/composer/minutiae/model/MinutiaeObjectSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 903683
    new-instance v0, LX/5Lu;

    invoke-direct {v0}, LX/5Lu;-><init>()V

    sput-object v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 903675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 903676
    iput-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 903677
    iput-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 903678
    iput-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    .line 903679
    iput-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->suggestionMechanism:Ljava/lang/String;

    .line 903680
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->hideAttachment:Z

    .line 903681
    iput-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    .line 903682
    return-void
.end method

.method public constructor <init>(LX/2s1;)V
    .locals 1

    .prologue
    .line 903667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 903668
    iget-object v0, p1, LX/2s1;->a:LX/5LG;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->a(LX/5LG;)Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 903669
    iget-object v0, p1, LX/2s1;->b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 903670
    iget-object v0, p1, LX/2s1;->c:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    .line 903671
    iget-object v0, p1, LX/2s1;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->suggestionMechanism:Ljava/lang/String;

    .line 903672
    iget-boolean v0, p1, LX/2s1;->e:Z

    iput-boolean v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->hideAttachment:Z

    .line 903673
    iget-object v0, p1, LX/2s1;->g:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    .line 903674
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 903658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 903659
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 903660
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 903661
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    .line 903662
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->suggestionMechanism:Ljava/lang/String;

    .line 903663
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->hideAttachment:Z

    .line 903664
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    iput-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    .line 903665
    return-void

    .line 903666
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 903657
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 903654
    invoke-static {p0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b(Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 903655
    const/4 v0, 0x0

    .line 903656
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    invoke-static {v0, p1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/graphql/model/GraphQLInlineActivity;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLInlineActivity;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 903653
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/graphql/model/GraphQLInlineActivity;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLInlineActivity;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 10
    .param p1    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 903605
    invoke-static {p0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b(Lcom/facebook/graphql/model/GraphQLInlineActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 903606
    :goto_0
    return-object v0

    .line 903607
    :cond_0
    new-instance v1, LX/4Z7;

    invoke-direct {v1}, LX/4Z7;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 903608
    iput-object v2, v1, LX/4Z7;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 903609
    move-object v1, v1

    .line 903610
    new-instance v2, LX/25F;

    invoke-direct {v2}, LX/25F;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v3

    .line 903611
    iput-object v3, v2, LX/25F;->T:Ljava/lang/String;

    .line 903612
    move-object v2, v2

    .line 903613
    iput-object p1, v2, LX/25F;->V:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 903614
    move-object v2, v2

    .line 903615
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    if-nez v3, :cond_3

    .line 903616
    :cond_1
    :goto_1
    iput-object v0, v2, LX/25F;->C:Ljava/lang/String;

    .line 903617
    move-object v0, v2

    .line 903618
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 903619
    iput-object v2, v0, LX/25F;->aH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 903620
    move-object v0, v0

    .line 903621
    invoke-virtual {v0}, LX/25F;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    .line 903622
    iput-object v0, v1, LX/4Z7;->h:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 903623
    move-object v0, v1

    .line 903624
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v1

    .line 903625
    iput-object v1, v0, LX/4Z7;->e:Ljava/lang/String;

    .line 903626
    move-object v1, v0

    .line 903627
    if-eqz p1, :cond_4

    const/4 v0, 0x1

    .line 903628
    :goto_2
    iput-boolean v0, v1, LX/4Z7;->i:Z

    .line 903629
    move-object v0, v1

    .line 903630
    new-instance v1, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;-><init>(LX/4Z7;)V

    .line 903631
    move-object v0, v1

    .line 903632
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->l()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v1

    .line 903633
    new-instance v2, LX/2s1;

    invoke-direct {v2}, LX/2s1;-><init>()V

    invoke-static {v0}, LX/5Lt;->a(Lcom/facebook/graphql/model/GraphQLTaggableActivitySuggestionsEdge;)Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    move-result-object v0

    .line 903634
    iput-object v0, v2, LX/2s1;->b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 903635
    move-object v0, v2

    .line 903636
    const/4 v6, 0x0

    .line 903637
    if-nez v1, :cond_5

    .line 903638
    :cond_2
    :goto_3
    move-object v1, v6

    .line 903639
    iput-object v1, v0, LX/2s1;->a:LX/5LG;

    .line 903640
    move-object v0, v0

    .line 903641
    invoke-virtual {v0}, LX/2s1;->a()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    goto :goto_0

    .line 903642
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 903643
    :cond_5
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 903644
    invoke-static {v4, v1}, LX/5Lt;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivity;)I

    move-result v5

    .line 903645
    if-eqz v5, :cond_2

    .line 903646
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 903647
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 903648
    const/4 v4, 0x0

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 903649
    new-instance v4, LX/15i;

    const/4 v8, 0x1

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 903650
    instance-of v5, v1, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v5, :cond_6

    .line 903651
    const-string v5, "MinutiaeModelConversionHelper.getMinutiaeTaggableActivity"

    invoke-virtual {v4, v5, v1}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 903652
    :cond_6
    new-instance v6, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-direct {v6, v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;-><init>(LX/15i;)V

    goto :goto_3
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 903601
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b(Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 903602
    invoke-static {p0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 903603
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    .line 903604
    :cond_1
    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 2

    .prologue
    .line 903587
    new-instance v0, LX/5LL;

    invoke-direct {v0}, LX/5LL;-><init>()V

    .line 903588
    iput-object p1, v0, LX/5LL;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 903589
    move-object v0, v0

    .line 903590
    iput-object p3, v0, LX/5LL;->g:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    .line 903591
    move-object v0, v0

    .line 903592
    iput-object p0, v0, LX/5LL;->d:Ljava/lang/String;

    .line 903593
    move-object v0, v0

    .line 903594
    invoke-virtual {v0}, LX/5LL;->a()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    move-result-object v0

    .line 903595
    new-instance v1, LX/2s1;

    invoke-direct {v1}, LX/2s1;-><init>()V

    .line 903596
    iput-object v0, v1, LX/2s1;->b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 903597
    move-object v0, v1

    .line 903598
    iput-object p2, v0, LX/2s1;->a:LX/5LG;

    .line 903599
    move-object v0, v0

    .line 903600
    invoke-virtual {v0}, LX/2s1;->a()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/model/MinutiaeTag;
    .locals 1

    .prologue
    .line 903586
    if-nez p0, :cond_0

    sget-object v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->d()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 903585
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b(Lcom/facebook/graphql/model/GraphQLInlineActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLInlineActivity;)Z
    .locals 1

    .prologue
    .line 903584
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->l()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3
    .param p0    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 903583
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b(Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fP()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fP()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fP()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 903582
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLInterfaces$MinutiaeIcon;",
            ">;"
        }
    .end annotation

    .prologue
    .line 903579
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 903580
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->b()LX/0Px;

    move-result-object v0

    .line 903581
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z
    .locals 2

    .prologue
    .line 903573
    if-ne p0, p1, :cond_0

    .line 903574
    const/4 v0, 0x1

    .line 903575
    :goto_0
    return v0

    .line 903576
    :cond_0
    if-nez p1, :cond_1

    .line 903577
    const/4 v0, 0x0

    goto :goto_0

    .line 903578
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->d()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->d()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/model/MinutiaeTag;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 903572
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/facebook/ipc/composer/model/MinutiaeTag;
    .locals 4

    .prologue
    .line 903564
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 903565
    :goto_0
    iget-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 903566
    iget-boolean v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->hideAttachment:Z

    if-eqz v1, :cond_1

    .line 903567
    iget-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->suggestionMechanism:Ljava/lang/String;

    invoke-static {v1, v2, v0, v3}, Lcom/facebook/ipc/composer/model/MinutiaeTag;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v0

    .line 903568
    :goto_1
    return-object v0

    .line 903569
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 903570
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->suggestionMechanism:Ljava/lang/String;

    invoke-static {v1, v2, v0, v3}, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v0

    goto :goto_1

    .line 903571
    :cond_2
    iget-object v1, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->suggestionMechanism:Ljava/lang/String;

    invoke-static {v1, v2, v0, v3}, Lcom/facebook/ipc/composer/model/MinutiaeTag;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v0

    goto :goto_1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 903563
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/1y5;
    .locals 1

    .prologue
    .line 903560
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->k()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 903561
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    .line 903562
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 903559
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->J()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .locals 7

    .prologue
    .line 903467
    new-instance v1, LX/4Wz;

    invoke-direct {v1}, LX/4Wz;-><init>()V

    new-instance v0, LX/4X0;

    invoke-direct {v0}, LX/4X0;-><init>()V

    new-instance v2, LX/4XR;

    invoke-direct {v2}, LX/4XR;-><init>()V

    iget-object v3, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 903468
    iput-object v3, v2, LX/4XR;->iB:Ljava/lang/String;

    .line 903469
    move-object v2, v2

    .line 903470
    invoke-virtual {v2}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 903471
    iput-object v2, v0, LX/4X0;->c:Lcom/facebook/graphql/model/GraphQLNode;

    .line 903472
    move-object v0, v0

    .line 903473
    iget-object v2, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    .line 903474
    if-nez v2, :cond_1

    .line 903475
    const/4 v3, 0x0

    .line 903476
    :goto_0
    move-object v2, v3

    .line 903477
    iput-object v2, v0, LX/4X0;->d:Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 903478
    move-object v2, v0

    .line 903479
    new-instance v3, LX/4Z5;

    invoke-direct {v3}, LX/4Z5;-><init>()V

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 903480
    :goto_1
    if-nez v0, :cond_5

    .line 903481
    const/4 v4, 0x0

    .line 903482
    :goto_2
    move-object v0, v4

    .line 903483
    iput-object v0, v3, LX/4Z5;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 903484
    move-object v0, v3

    .line 903485
    invoke-virtual {v0}, LX/4Z5;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v0

    .line 903486
    iput-object v0, v2, LX/4X0;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 903487
    move-object v0, v2

    .line 903488
    invoke-virtual {v0}, LX/4X0;->a()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 903489
    iput-object v0, v1, LX/4Wz;->b:LX/0Px;

    .line 903490
    move-object v0, v1

    .line 903491
    invoke-virtual {v0}, LX/4Wz;->a()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    goto :goto_1

    .line 903492
    :cond_1
    new-instance v3, LX/4Z3;

    invoke-direct {v3}, LX/4Z3;-><init>()V

    .line 903493
    invoke-interface {v2}, LX/5LG;->B()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    move-result-object v4

    .line 903494
    if-nez v4, :cond_2

    .line 903495
    const/4 v5, 0x0

    .line 903496
    :goto_3
    move-object v4, v5

    .line 903497
    iput-object v4, v3, LX/4Z3;->b:Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    .line 903498
    invoke-interface {v2}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v4

    .line 903499
    if-nez v4, :cond_3

    .line 903500
    const/4 v5, 0x0

    .line 903501
    :goto_4
    move-object v4, v5

    .line 903502
    iput-object v4, v3, LX/4Z3;->c:Lcom/facebook/graphql/model/GraphQLImage;

    .line 903503
    invoke-interface {v2}, LX/5LG;->z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v4

    .line 903504
    if-nez v4, :cond_4

    .line 903505
    const/4 v5, 0x0

    .line 903506
    :goto_5
    move-object v4, v5

    .line 903507
    iput-object v4, v3, LX/4Z3;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 903508
    invoke-interface {v2}, LX/5LG;->j()Ljava/lang/String;

    move-result-object v4

    .line 903509
    iput-object v4, v3, LX/4Z3;->e:Ljava/lang/String;

    .line 903510
    invoke-interface {v2}, LX/5LG;->k()Z

    move-result v4

    .line 903511
    iput-boolean v4, v3, LX/4Z3;->f:Z

    .line 903512
    invoke-interface {v2}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v4

    .line 903513
    iput-object v4, v3, LX/4Z3;->g:Ljava/lang/String;

    .line 903514
    invoke-interface {v2}, LX/5LG;->m()I

    move-result v4

    .line 903515
    iput v4, v3, LX/4Z3;->h:I

    .line 903516
    invoke-interface {v2}, LX/5LG;->n()Ljava/lang/String;

    move-result-object v4

    .line 903517
    iput-object v4, v3, LX/4Z3;->i:Ljava/lang/String;

    .line 903518
    invoke-interface {v2}, LX/5LG;->y()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {v4}, LX/5Lt;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v4

    .line 903519
    iput-object v4, v3, LX/4Z3;->j:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 903520
    invoke-interface {v2}, LX/5LG;->x()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {v4}, LX/5Lt;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v4

    .line 903521
    iput-object v4, v3, LX/4Z3;->k:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 903522
    invoke-interface {v2}, LX/5LG;->w()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {v4}, LX/5Lt;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v4

    .line 903523
    iput-object v4, v3, LX/4Z3;->n:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 903524
    invoke-interface {v2}, LX/5LG;->v()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {v4}, LX/5Lt;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v4

    .line 903525
    iput-object v4, v3, LX/4Z3;->o:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 903526
    invoke-interface {v2}, LX/5LG;->u()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {v4}, LX/5Lt;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v4

    .line 903527
    iput-object v4, v3, LX/4Z3;->p:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 903528
    invoke-interface {v2}, LX/5LG;->t()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {v4}, LX/5Lt;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v4

    .line 903529
    iput-object v4, v3, LX/4Z3;->q:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 903530
    invoke-interface {v2}, LX/5LG;->o()Ljava/lang/String;

    move-result-object v4

    .line 903531
    iput-object v4, v3, LX/4Z3;->u:Ljava/lang/String;

    .line 903532
    invoke-interface {v2}, LX/5LG;->p()Z

    move-result v4

    .line 903533
    iput-boolean v4, v3, LX/4Z3;->v:Z

    .line 903534
    invoke-interface {v2}, LX/5LG;->q()Z

    move-result v4

    .line 903535
    iput-boolean v4, v3, LX/4Z3;->w:Z

    .line 903536
    invoke-interface {v2}, LX/5LG;->r()Z

    move-result v4

    .line 903537
    iput-boolean v4, v3, LX/4Z3;->x:Z

    .line 903538
    invoke-virtual {v3}, LX/4Z3;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v3

    goto/16 :goto_0

    .line 903539
    :cond_2
    new-instance v5, LX/4Z4;

    invoke-direct {v5}, LX/4Z4;-><init>()V

    .line 903540
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;->a()I

    move-result v6

    .line 903541
    iput v6, v5, LX/4Z4;->b:I

    .line 903542
    invoke-virtual {v5}, LX/4Z4;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    move-result-object v5

    goto/16 :goto_3

    .line 903543
    :cond_3
    new-instance v5, LX/2dc;

    invoke-direct {v5}, LX/2dc;-><init>()V

    .line 903544
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;->a()Ljava/lang/String;

    move-result-object v6

    .line 903545
    iput-object v6, v5, LX/2dc;->h:Ljava/lang/String;

    .line 903546
    invoke-virtual {v5}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    goto/16 :goto_4

    .line 903547
    :cond_4
    new-instance v5, LX/2dc;

    invoke-direct {v5}, LX/2dc;-><init>()V

    .line 903548
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;->a()Ljava/lang/String;

    move-result-object v6

    .line 903549
    iput-object v6, v5, LX/2dc;->h:Ljava/lang/String;

    .line 903550
    invoke-virtual {v5}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    goto/16 :goto_5

    .line 903551
    :cond_5
    new-instance v4, LX/2dc;

    invoke-direct {v4}, LX/2dc;-><init>()V

    .line 903552
    invoke-interface {v0}, LX/1Fb;->a()I

    move-result v5

    .line 903553
    iput v5, v4, LX/2dc;->c:I

    .line 903554
    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    .line 903555
    iput-object v5, v4, LX/2dc;->h:Ljava/lang/String;

    .line 903556
    invoke-interface {v0}, LX/1Fb;->c()I

    move-result v5

    .line 903557
    iput v5, v4, LX/2dc;->i:I

    .line 903558
    invoke-virtual {v4}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    goto/16 :goto_2
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 903466
    iget-boolean v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->hideAttachment:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel$OpenGraphComposerPreviewModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 903458
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 903459
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 903460
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->customIcon:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeIconModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 903461
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->suggestionMechanism:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 903462
    iget-boolean v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->hideAttachment:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 903463
    iget-object v0, p0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 903464
    return-void

    .line 903465
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
