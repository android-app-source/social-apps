.class public Lcom/facebook/composer/publish/common/PendingStoryPersistentDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/publish/common/PendingStoryPersistentData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 904642
    const-class v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    new-instance v1, Lcom/facebook/composer/publish/common/PendingStoryPersistentDataSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/publish/common/PendingStoryPersistentDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 904643
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 904644
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/publish/common/PendingStoryPersistentData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904645
    if-nez p0, :cond_0

    .line 904646
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 904647
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 904648
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/publish/common/PendingStoryPersistentDataSerializer;->b(Lcom/facebook/composer/publish/common/PendingStoryPersistentData;LX/0nX;LX/0my;)V

    .line 904649
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 904650
    return-void
.end method

.method private static b(Lcom/facebook/composer/publish/common/PendingStoryPersistentData;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 904651
    const-string v0, "graphql_story"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->story:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904652
    const-string v0, "post_params_wrapper"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904653
    const-string v0, "publish_attempt_info"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->publishAttemptInfo:Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904654
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904655
    check-cast p1, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/publish/common/PendingStoryPersistentDataSerializer;->a(Lcom/facebook/composer/publish/common/PendingStoryPersistentData;LX/0nX;LX/0my;)V

    return-void
.end method
