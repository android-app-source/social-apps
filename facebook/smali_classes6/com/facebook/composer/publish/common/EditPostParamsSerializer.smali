.class public Lcom/facebook/composer/publish/common/EditPostParamsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/publish/common/EditPostParams;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 904119
    const-class v0, Lcom/facebook/composer/publish/common/EditPostParams;

    new-instance v1, Lcom/facebook/composer/publish/common/EditPostParamsSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/publish/common/EditPostParamsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 904120
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 904118
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/publish/common/EditPostParams;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904112
    if-nez p0, :cond_0

    .line 904113
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 904114
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 904115
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/publish/common/EditPostParamsSerializer;->b(Lcom/facebook/composer/publish/common/EditPostParams;LX/0nX;LX/0my;)V

    .line 904116
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 904117
    return-void
.end method

.method private static b(Lcom/facebook/composer/publish/common/EditPostParams;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 904090
    const-string v0, "cache_ids"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 904091
    const-string v0, "composer_session_id"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getComposerSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904092
    const-string v0, "has_media_fb_ids"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->hasMediaFbIds()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 904093
    const-string v0, "is_photo_container"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->isPhotoContainer()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 904094
    const-string v0, "legacy_story_api_id"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getLegacyStoryApiId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904095
    const-string v0, "link_edit"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getLinkEdit()Lcom/facebook/composer/publish/common/LinkEdit;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904096
    const-string v0, "media_captions"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getMediaCaptions()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 904097
    const-string v0, "media_fb_ids"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getMediaFbIds()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 904098
    const-string v0, "message"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getMessage()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904099
    const-string v0, "minutiae_tag"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getMinutiaeTag()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904100
    const-string v0, "original_post_time"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getOriginalPostTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 904101
    const-string v0, "place_tag"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getPlaceTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904102
    const-string v0, "privacy"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getPrivacy()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904103
    const-string v0, "product_item_attachment"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904104
    const-string v0, "rich_text_style"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904105
    const-string v0, "should_publish_unpublished_content"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->shouldPublishUnpublishedContent()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 904106
    const-string v0, "source_type"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getSourceType()LX/21D;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904107
    const-string v0, "sticker_edit"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getStickerEdit()Lcom/facebook/composer/publish/common/StickerEdit;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904108
    const-string v0, "story_id"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getStoryId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904109
    const-string v0, "tagged_ids"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getTaggedIds()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 904110
    const-string v0, "target_id"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/EditPostParams;->getTargetId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 904111
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904089
    check-cast p1, Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/publish/common/EditPostParamsSerializer;->a(Lcom/facebook/composer/publish/common/EditPostParams;LX/0nX;LX/0my;)V

    return-void
.end method
