.class public Lcom/facebook/composer/publish/common/PendingStorySerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/publish/common/PendingStory;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 904662
    const-class v0, Lcom/facebook/composer/publish/common/PendingStory;

    new-instance v1, Lcom/facebook/composer/publish/common/PendingStorySerializer;

    invoke-direct {v1}, Lcom/facebook/composer/publish/common/PendingStorySerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 904663
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 904657
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/publish/common/PendingStory;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904664
    if-nez p0, :cond_0

    .line 904665
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 904666
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 904667
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/publish/common/PendingStorySerializer;->b(Lcom/facebook/composer/publish/common/PendingStory;LX/0nX;LX/0my;)V

    .line 904668
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 904669
    return-void
.end method

.method private static b(Lcom/facebook/composer/publish/common/PendingStory;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 904658
    const-string v0, "pending_story_persistent_data"

    .line 904659
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    move-object v1, v1

    .line 904660
    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904661
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904656
    check-cast p1, Lcom/facebook/composer/publish/common/PendingStory;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/publish/common/PendingStorySerializer;->a(Lcom/facebook/composer/publish/common/PendingStory;LX/0nX;LX/0my;)V

    return-void
.end method
