.class public Lcom/facebook/composer/publish/common/LinkEditSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/publish/common/LinkEdit;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 904338
    const-class v0, Lcom/facebook/composer/publish/common/LinkEdit;

    new-instance v1, Lcom/facebook/composer/publish/common/LinkEditSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/publish/common/LinkEditSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 904339
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 904340
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/publish/common/LinkEdit;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904341
    if-nez p0, :cond_0

    .line 904342
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 904343
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 904344
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/publish/common/LinkEditSerializer;->b(Lcom/facebook/composer/publish/common/LinkEdit;LX/0nX;LX/0my;)V

    .line 904345
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 904346
    return-void
.end method

.method private static b(Lcom/facebook/composer/publish/common/LinkEdit;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 904347
    const-string v0, "no_link"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/LinkEdit;->noLink:Ljava/lang/Boolean;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 904348
    const-string v0, "link_data"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/LinkEdit;->linkData:Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904349
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904350
    check-cast p1, Lcom/facebook/composer/publish/common/LinkEdit;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/publish/common/LinkEditSerializer;->a(Lcom/facebook/composer/publish/common/LinkEdit;LX/0nX;LX/0my;)V

    return-void
.end method
