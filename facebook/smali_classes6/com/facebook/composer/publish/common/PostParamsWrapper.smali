.class public Lcom/facebook/composer/publish/common/PostParamsWrapper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/PostParamsWrapperDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/publish/common/PostParamsWrapperSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/common/PostParamsWrapper;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "edit_post_params"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "publish_post_params"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904754
    const-class v0, Lcom/facebook/composer/publish/common/PostParamsWrapperDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904753
    const-class v0, Lcom/facebook/composer/publish/common/PostParamsWrapperSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 904752
    new-instance v0, LX/5M4;

    invoke-direct {v0}, LX/5M4;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 904748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904749
    iput-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 904750
    iput-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    .line 904751
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 904744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904745
    const-class v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 904746
    const-class v0, Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/EditPostParams;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    .line 904747
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/common/EditPostParams;)V
    .locals 1

    .prologue
    .line 904739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904740
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 904741
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 904742
    iput-object p1, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    .line 904743
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/common/PostParamsWrapper;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 904730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904731
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 904732
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    if-eqz v0, :cond_2

    .line 904733
    new-instance v0, LX/5M9;

    iget-object v1, p1, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    invoke-direct {v0, v1}, LX/5M9;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    invoke-virtual {v0}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 904734
    iput-object v2, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    .line 904735
    :goto_1
    return-void

    .line 904736
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 904737
    :cond_2
    iput-object v2, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 904738
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-static {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->a(Lcom/facebook/composer/publish/common/EditPostParams;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->a()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V
    .locals 1

    .prologue
    .line 904725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904726
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 904727
    iput-object p1, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 904728
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    .line 904729
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 904722
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    if-eqz v0, :cond_0

    .line 904723
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    .line 904724
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getComposerSessionId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 904755
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    if-eqz v0, :cond_0

    .line 904756
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-wide v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->originalPostTime:J

    .line 904757
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getOriginalPostTime()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 904719
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    if-eqz v0, :cond_0

    .line 904720
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-wide v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    .line 904721
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getTargetId()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 904716
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    if-eqz v0, :cond_0

    .line 904717
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-boolean v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPhotoContainer:Z

    .line 904718
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->isPhotoContainer()Z

    move-result v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 904715
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/2rt;
    .locals 1

    .prologue
    .line 904712
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    if-eqz v0, :cond_0

    .line 904713
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerType:LX/2rt;

    .line 904714
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/2rt;->STATUS:LX/2rt;

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 904709
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    if-eqz v0, :cond_0

    .line 904710
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->stickerId:Ljava/lang/String;

    .line 904711
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 904706
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    if-eqz v0, :cond_0

    .line 904707
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getLegacyStoryApiId()Ljava/lang/String;

    move-result-object v0

    .line 904708
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/facebook/composer/publish/common/EditPostParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 904705
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    return-object v0
.end method

.method public final i()Lcom/facebook/composer/publish/common/PublishPostParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 904704
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 904701
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->publishPostParams:Lcom/facebook/composer/publish/common/PublishPostParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 904702
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PostParamsWrapper;->editPostParams:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 904703
    return-void
.end method
