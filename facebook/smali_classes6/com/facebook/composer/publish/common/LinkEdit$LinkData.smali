.class public final Lcom/facebook/composer/publish/common/LinkEdit$LinkData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/LinkEdit_LinkDataDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/publish/common/LinkEdit_LinkDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/common/LinkEdit$LinkData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final link:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "link"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904260
    const-class v0, Lcom/facebook/composer/publish/common/LinkEdit_LinkDataDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904283
    const-class v0, Lcom/facebook/composer/publish/common/LinkEdit_LinkDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 904282
    new-instance v0, LX/5Lz;

    invoke-direct {v0}, LX/5Lz;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 904279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904280
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;->link:Ljava/lang/String;

    .line 904281
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 904276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904277
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;->link:Ljava/lang/String;

    .line 904278
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 904273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904274
    iput-object p1, p0, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;->link:Ljava/lang/String;

    .line 904275
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/composer/publish/common/LinkEdit$LinkData;
    .locals 1

    .prologue
    .line 904272
    new-instance v0, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    invoke-direct {v0, p0}, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 904271
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 904264
    if-ne p0, p1, :cond_0

    .line 904265
    const/4 v0, 0x1

    .line 904266
    :goto_0
    return v0

    .line 904267
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 904268
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 904269
    :cond_2
    check-cast p1, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    .line 904270
    iget-object v0, p0, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;->link:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;->link:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 904263
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;->link:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 904261
    iget-object v0, p0, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;->link:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 904262
    return-void
.end method
