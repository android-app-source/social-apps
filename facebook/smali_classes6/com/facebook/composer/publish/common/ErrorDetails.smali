.class public Lcom/facebook/composer/publish/common/ErrorDetails;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/ErrorDetailsDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/publish/common/ErrorDetailsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/common/ErrorDetails;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final errorCode:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "error_code"
    .end annotation
.end field

.field public final errorSubcode:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "error_subcode"
    .end annotation
.end field

.field public final isRetriable:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_retriable"
    .end annotation
.end field

.field public final isSentryWarningWithUserConfirmationRequired:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sentry_warning_with_user_confirmation_required"
    .end annotation
.end field

.field public final isVideoTranscodingError:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_transcoding_error"
    .end annotation
.end field

.field public final logMessage:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "log_message"
    .end annotation
.end field

.field public final userMessage:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "message"
    .end annotation
.end field

.field public final userTitle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "user_title"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904196
    const-class v0, Lcom/facebook/composer/publish/common/ErrorDetailsDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904195
    const-class v0, Lcom/facebook/composer/publish/common/ErrorDetailsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 904165
    new-instance v0, LX/5Lx;

    invoke-direct {v0}, LX/5Lx;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/ErrorDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 904193
    new-instance v0, LX/2rc;

    invoke-direct {v0}, LX/2rc;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/composer/publish/common/ErrorDetails;-><init>(LX/2rc;)V

    .line 904194
    return-void
.end method

.method public constructor <init>(LX/2rc;)V
    .locals 1

    .prologue
    .line 904183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904184
    iget-boolean v0, p1, LX/2rc;->a:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    .line 904185
    iget-object v0, p1, LX/2rc;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->userMessage:Ljava/lang/String;

    .line 904186
    iget-object v0, p1, LX/2rc;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->logMessage:Ljava/lang/String;

    .line 904187
    iget v0, p1, LX/2rc;->d:I

    iput v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->errorCode:I

    .line 904188
    iget v0, p1, LX/2rc;->e:I

    iput v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->errorSubcode:I

    .line 904189
    iget-boolean v0, p1, LX/2rc;->f:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isSentryWarningWithUserConfirmationRequired:Z

    .line 904190
    iget-object v0, p1, LX/2rc;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->userTitle:Ljava/lang/String;

    .line 904191
    iget-boolean v0, p1, LX/2rc;->h:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isVideoTranscodingError:Z

    .line 904192
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 904197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904198
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    .line 904199
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->userMessage:Ljava/lang/String;

    .line 904200
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->logMessage:Ljava/lang/String;

    .line 904201
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->errorCode:I

    .line 904202
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->errorSubcode:I

    .line 904203
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isSentryWarningWithUserConfirmationRequired:Z

    .line 904204
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->userTitle:Ljava/lang/String;

    .line 904205
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isVideoTranscodingError:Z

    .line 904206
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 904182
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 904176
    if-ne p0, p1, :cond_1

    .line 904177
    :cond_0
    :goto_0
    return v0

    .line 904178
    :cond_1
    instance-of v2, p1, Lcom/facebook/composer/publish/common/ErrorDetails;

    if-nez v2, :cond_2

    move v0, v1

    .line 904179
    goto :goto_0

    .line 904180
    :cond_2
    check-cast p1, Lcom/facebook/composer/publish/common/ErrorDetails;

    .line 904181
    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p1, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->userMessage:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/ErrorDetails;->userMessage:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->logMessage:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/ErrorDetails;->logMessage:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->errorCode:I

    iget v3, p1, Lcom/facebook/composer/publish/common/ErrorDetails;->errorCode:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->errorSubcode:I

    iget v3, p1, Lcom/facebook/composer/publish/common/ErrorDetails;->errorSubcode:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isSentryWarningWithUserConfirmationRequired:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p1, Lcom/facebook/composer/publish/common/ErrorDetails;->isSentryWarningWithUserConfirmationRequired:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->userTitle:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/ErrorDetails;->userTitle:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 904175
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->userMessage:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->logMessage:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->errorCode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->errorSubcode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isSentryWarningWithUserConfirmationRequired:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->userTitle:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 904166
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 904167
    iget-object v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->userMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 904168
    iget-object v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->logMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 904169
    iget v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->errorCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 904170
    iget v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->errorSubcode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 904171
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isSentryWarningWithUserConfirmationRequired:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 904172
    iget-object v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->userTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 904173
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isVideoTranscodingError:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 904174
    return-void
.end method
