.class public final Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/PublishAttemptInfo_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:Lcom/facebook/composer/publish/common/ErrorDetails;

.field private static final b:LX/5Ro;


# instance fields
.field public c:I

.field public d:Lcom/facebook/composer/publish/common/ErrorDetails;

.field public e:LX/5Ro;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904818
    const-class v0, Lcom/facebook/composer/publish/common/PublishAttemptInfo_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 904819
    new-instance v0, LX/5M6;

    invoke-direct {v0}, LX/5M6;-><init>()V

    .line 904820
    new-instance v0, LX/2rc;

    invoke-direct {v0}, LX/2rc;-><init>()V

    invoke-virtual {v0}, LX/2rc;->a()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    move-object v0, v0

    .line 904821
    sput-object v0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->a:Lcom/facebook/composer/publish/common/ErrorDetails;

    .line 904822
    new-instance v0, LX/5M7;

    invoke-direct {v0}, LX/5M7;-><init>()V

    .line 904823
    sget-object v0, LX/5Ro;->NONE:LX/5Ro;

    move-object v0, v0

    .line 904824
    sput-object v0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->b:LX/5Ro;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 904803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904804
    sget-object v0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->a:Lcom/facebook/composer/publish/common/ErrorDetails;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->d:Lcom/facebook/composer/publish/common/ErrorDetails;

    .line 904805
    sget-object v0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->b:LX/5Ro;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->e:LX/5Ro;

    .line 904806
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V
    .locals 1

    .prologue
    .line 904807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904808
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 904809
    instance-of v0, p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    if-eqz v0, :cond_0

    .line 904810
    check-cast p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    .line 904811
    iget v0, p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a:I

    iput v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->c:I

    .line 904812
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->b:Lcom/facebook/composer/publish/common/ErrorDetails;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->d:Lcom/facebook/composer/publish/common/ErrorDetails;

    .line 904813
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->c:LX/5Ro;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->e:LX/5Ro;

    .line 904814
    :goto_0
    return-void

    .line 904815
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->getAttemptCount()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->c:I

    .line 904816
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->getErrorDetails()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->d:Lcom/facebook/composer/publish/common/ErrorDetails;

    .line 904817
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->getRetrySource()LX/5Ro;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->e:LX/5Ro;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/publish/common/PublishAttemptInfo;
    .locals 2

    .prologue
    .line 904802
    new-instance v0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    invoke-direct {v0, p0}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;-><init>(Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;)V

    return-object v0
.end method

.method public setAttemptCount(I)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attempt_count"
    .end annotation

    .prologue
    .line 904800
    iput p1, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->c:I

    .line 904801
    return-object p0
.end method

.method public setErrorDetails(Lcom/facebook/composer/publish/common/ErrorDetails;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "error_details"
    .end annotation

    .prologue
    .line 904798
    iput-object p1, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->d:Lcom/facebook/composer/publish/common/ErrorDetails;

    .line 904799
    return-object p0
.end method

.method public setRetrySource(LX/5Ro;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "retry_source"
    .end annotation

    .prologue
    .line 904796
    iput-object p1, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->e:LX/5Ro;

    .line 904797
    return-object p0
.end method
