.class public Lcom/facebook/composer/publish/common/LinkEdit;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/LinkEditDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/publish/common/LinkEditSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/common/LinkEdit;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/composer/publish/common/LinkEdit;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field


# instance fields
.field public final linkData:Lcom/facebook/composer/publish/common/LinkEdit$LinkData;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "link_data"
    .end annotation
.end field

.field public final noLink:Ljava/lang/Boolean;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "no_link"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904315
    const-class v0, Lcom/facebook/composer/publish/common/LinkEditDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904284
    const-class v0, Lcom/facebook/composer/publish/common/LinkEditSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 904313
    new-instance v0, Lcom/facebook/composer/publish/common/LinkEdit;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/composer/publish/common/LinkEdit;-><init>(Ljava/lang/Boolean;)V

    sput-object v0, Lcom/facebook/composer/publish/common/LinkEdit;->a:Lcom/facebook/composer/publish/common/LinkEdit;

    .line 904314
    new-instance v0, LX/5Ly;

    invoke-direct {v0}, LX/5Ly;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/LinkEdit;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 904309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904310
    iput-object v0, p0, Lcom/facebook/composer/publish/common/LinkEdit;->noLink:Ljava/lang/Boolean;

    .line 904311
    iput-object v0, p0, Lcom/facebook/composer/publish/common/LinkEdit;->linkData:Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    .line 904312
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 904305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904306
    invoke-static {p1}, LX/46R;->e(Landroid/os/Parcel;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/LinkEdit;->noLink:Ljava/lang/Boolean;

    .line 904307
    const-class v0, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/LinkEdit;->linkData:Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    .line 904308
    return-void
.end method

.method private constructor <init>(Lcom/facebook/composer/publish/common/LinkEdit$LinkData;)V
    .locals 1

    .prologue
    .line 904301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904302
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/LinkEdit;->noLink:Ljava/lang/Boolean;

    .line 904303
    iput-object p1, p0, Lcom/facebook/composer/publish/common/LinkEdit;->linkData:Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    .line 904304
    return-void
.end method

.method private constructor <init>(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 904297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904298
    iput-object p1, p0, Lcom/facebook/composer/publish/common/LinkEdit;->noLink:Ljava/lang/Boolean;

    .line 904299
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/LinkEdit;->linkData:Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    .line 904300
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/composer/publish/common/LinkEdit;
    .locals 2

    .prologue
    .line 904296
    new-instance v0, Lcom/facebook/composer/publish/common/LinkEdit;

    invoke-static {p0}, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;->a(Ljava/lang/String;)Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/composer/publish/common/LinkEdit;-><init>(Lcom/facebook/composer/publish/common/LinkEdit$LinkData;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 904295
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 904289
    if-ne p0, p1, :cond_1

    .line 904290
    :cond_0
    :goto_0
    return v0

    .line 904291
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 904292
    goto :goto_0

    .line 904293
    :cond_3
    check-cast p1, Lcom/facebook/composer/publish/common/LinkEdit;

    .line 904294
    iget-object v2, p0, Lcom/facebook/composer/publish/common/LinkEdit;->noLink:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/LinkEdit;->noLink:Ljava/lang/Boolean;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/composer/publish/common/LinkEdit;->linkData:Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/LinkEdit;->linkData:Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 904288
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/composer/publish/common/LinkEdit;->noLink:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/composer/publish/common/LinkEdit;->linkData:Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 904285
    iget-object v0, p0, Lcom/facebook/composer/publish/common/LinkEdit;->noLink:Ljava/lang/Boolean;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Boolean;)V

    .line 904286
    iget-object v0, p0, Lcom/facebook/composer/publish/common/LinkEdit;->linkData:Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 904287
    return-void
.end method
