.class public Lcom/facebook/composer/publish/common/LinkEdit_LinkDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/publish/common/LinkEdit$LinkData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 904382
    const-class v0, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    new-instance v1, Lcom/facebook/composer/publish/common/LinkEdit_LinkDataSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/publish/common/LinkEdit_LinkDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 904383
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 904381
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/publish/common/LinkEdit$LinkData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904372
    if-nez p0, :cond_0

    .line 904373
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 904374
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 904375
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/publish/common/LinkEdit_LinkDataSerializer;->b(Lcom/facebook/composer/publish/common/LinkEdit$LinkData;LX/0nX;LX/0my;)V

    .line 904376
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 904377
    return-void
.end method

.method private static b(Lcom/facebook/composer/publish/common/LinkEdit$LinkData;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 904379
    const-string v0, "link"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;->link:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904380
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904378
    check-cast p1, Lcom/facebook/composer/publish/common/LinkEdit$LinkData;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/publish/common/LinkEdit_LinkDataSerializer;->a(Lcom/facebook/composer/publish/common/LinkEdit$LinkData;LX/0nX;LX/0my;)V

    return-void
.end method
