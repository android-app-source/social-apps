.class public Lcom/facebook/composer/publish/common/PublishAttemptInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/PublishAttemptInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/publish/common/PublishAttemptInfoSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/common/PublishAttemptInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:Lcom/facebook/composer/publish/common/ErrorDetails;

.field public final c:LX/5Ro;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904865
    const-class v0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904864
    const-class v0, Lcom/facebook/composer/publish/common/PublishAttemptInfoSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 904863
    new-instance v0, LX/5M5;

    invoke-direct {v0}, LX/5M5;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 904858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904859
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a:I

    .line 904860
    const-class v0, Lcom/facebook/composer/publish/common/ErrorDetails;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/ErrorDetails;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->b:Lcom/facebook/composer/publish/common/ErrorDetails;

    .line 904861
    invoke-static {}, LX/5Ro;->values()[LX/5Ro;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->c:LX/5Ro;

    .line 904862
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;)V
    .locals 1

    .prologue
    .line 904853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904854
    iget v0, p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a:I

    .line 904855
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->d:Lcom/facebook/composer/publish/common/ErrorDetails;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/ErrorDetails;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->b:Lcom/facebook/composer/publish/common/ErrorDetails;

    .line 904856
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->e:LX/5Ro;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Ro;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->c:LX/5Ro;

    .line 904857
    return-void
.end method

.method public static a(Lcom/facebook/composer/publish/common/PublishAttemptInfo;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;
    .locals 2

    .prologue
    .line 904852
    new-instance v0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;-><init>(Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;
    .locals 2

    .prologue
    .line 904851
    new-instance v0, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    invoke-direct {v0}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 904850
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 904839
    if-ne p0, p1, :cond_1

    .line 904840
    :cond_0
    :goto_0
    return v0

    .line 904841
    :cond_1
    instance-of v2, p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    if-nez v2, :cond_2

    move v0, v1

    .line 904842
    goto :goto_0

    .line 904843
    :cond_2
    check-cast p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    .line 904844
    iget v2, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a:I

    iget v3, p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 904845
    goto :goto_0

    .line 904846
    :cond_3
    iget-object v2, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->b:Lcom/facebook/composer/publish/common/ErrorDetails;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->b:Lcom/facebook/composer/publish/common/ErrorDetails;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 904847
    goto :goto_0

    .line 904848
    :cond_4
    iget-object v2, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->c:LX/5Ro;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->c:LX/5Ro;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 904849
    goto :goto_0
.end method

.method public getAttemptCount()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attempt_count"
    .end annotation

    .prologue
    .line 904831
    iget v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a:I

    return v0
.end method

.method public getErrorDetails()Lcom/facebook/composer/publish/common/ErrorDetails;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "error_details"
    .end annotation

    .prologue
    .line 904838
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->b:Lcom/facebook/composer/publish/common/ErrorDetails;

    return-object v0
.end method

.method public getRetrySource()LX/5Ro;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "retry_source"
    .end annotation

    .prologue
    .line 904837
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->c:LX/5Ro;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 904836
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->b:Lcom/facebook/composer/publish/common/ErrorDetails;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->c:LX/5Ro;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 904832
    iget v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 904833
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->b:Lcom/facebook/composer/publish/common/ErrorDetails;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 904834
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->c:LX/5Ro;

    invoke-virtual {v0}, LX/5Ro;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 904835
    return-void
.end method
