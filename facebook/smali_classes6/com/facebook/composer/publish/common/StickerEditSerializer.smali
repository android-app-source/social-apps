.class public Lcom/facebook/composer/publish/common/StickerEditSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/publish/common/StickerEdit;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 905219
    const-class v0, Lcom/facebook/composer/publish/common/StickerEdit;

    new-instance v1, Lcom/facebook/composer/publish/common/StickerEditSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/publish/common/StickerEditSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 905220
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 905221
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/publish/common/StickerEdit;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 905222
    if-nez p0, :cond_0

    .line 905223
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 905224
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 905225
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/publish/common/StickerEditSerializer;->b(Lcom/facebook/composer/publish/common/StickerEdit;LX/0nX;LX/0my;)V

    .line 905226
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 905227
    return-void
.end method

.method private static b(Lcom/facebook/composer/publish/common/StickerEdit;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 905228
    const-string v0, "no_sticker"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/StickerEdit;->noSticker:Ljava/lang/Boolean;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905229
    const-string v0, "referenced_sticker_id"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/StickerEdit;->referencedSticker:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905230
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 905231
    check-cast p1, Lcom/facebook/composer/publish/common/StickerEdit;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/publish/common/StickerEditSerializer;->a(Lcom/facebook/composer/publish/common/StickerEdit;LX/0nX;LX/0my;)V

    return-void
.end method
