.class public final Lcom/facebook/composer/publish/common/EditPostParams$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/composer/publish/common/EditPostParams;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/composer/publish/common/EditPostParams_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 903850
    new-instance v0, Lcom/facebook/composer/publish/common/EditPostParams_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/composer/publish/common/EditPostParams_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/EditPostParams$Deserializer;->a:Lcom/facebook/composer/publish/common/EditPostParams_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 903851
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 903852
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/composer/publish/common/EditPostParams;
    .locals 1

    .prologue
    .line 903848
    sget-object v0, Lcom/facebook/composer/publish/common/EditPostParams$Deserializer;->a:Lcom/facebook/composer/publish/common/EditPostParams_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    .line 903849
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->a()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 903847
    invoke-static {p1, p2}, Lcom/facebook/composer/publish/common/EditPostParams$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v0

    return-object v0
.end method
