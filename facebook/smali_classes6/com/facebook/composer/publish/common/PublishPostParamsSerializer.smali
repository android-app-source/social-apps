.class public Lcom/facebook/composer/publish/common/PublishPostParamsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/publish/common/PublishPostParams;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 905089
    const-class v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    new-instance v1, Lcom/facebook/composer/publish/common/PublishPostParamsSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/publish/common/PublishPostParamsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 905090
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 905091
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/publish/common/PublishPostParams;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 905092
    if-nez p0, :cond_0

    .line 905093
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 905094
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 905095
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/publish/common/PublishPostParamsSerializer;->b(Lcom/facebook/composer/publish/common/PublishPostParams;LX/0nX;LX/0my;)V

    .line 905096
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 905097
    return-void
.end method

.method private static b(Lcom/facebook/composer/publish/common/PublishPostParams;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 905098
    const-string v0, "composer_type"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerType:LX/2rt;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 905099
    const-string v0, "target_id"

    iget-wide v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 905100
    const-string v0, "raw_message"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905101
    const-string v0, "page_id"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeTag:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905102
    const-string v0, "tagged_ids"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->taggedIds:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 905103
    const-string v0, "album_id"

    iget-wide v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->albumId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 905104
    const-string v0, "sponsor_id"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sponsorId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905105
    const-string v0, "privacy"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905106
    const-string v0, "composer_session_logging_data"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionLoggingData:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 905107
    const-string v0, "link"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->link:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905108
    const-string v0, "user_id"

    iget-wide v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->userId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 905109
    const-string v0, "shareable"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 905110
    const-string v0, "tracking"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->tracking:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905111
    const-string v0, "nectarModule"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->nectarModule:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905112
    const-string v0, "schedule_publish_time"

    iget-wide v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->schedulePublishTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 905113
    const-string v0, "publish_mode"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->publishMode:LX/5Rn;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 905114
    const-string v0, "is_tags_user_selected"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isTagsUserSelected:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905115
    const-string v0, "proxied_app_id"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905116
    const-string v0, "proxied_app_name"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905117
    const-string v0, "android_key_hash"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->androidKeyHash:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905118
    const-string v0, "ref"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ref:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905119
    const-string v0, "name"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->name:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905120
    const-string v0, "caption"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->caption:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905121
    const-string v0, "description"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->description:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905122
    const-string v0, "quote"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->quote:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905123
    const-string v0, "picture"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->picture:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905124
    const-string v0, "is_photo_container"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPhotoContainer:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905125
    const-string v0, "composer_session_id"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905126
    const-string v0, "idempotence_token"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->idempotenceToken:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905127
    const-string v0, "is_explicit_location"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isExplicitLocation:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905128
    const-string v0, "is_checkin"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isCheckin:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905129
    const-string v0, "text_only_place"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->textOnlyPlace:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905130
    const-string v0, "place_attachment_removed"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeAttachmentRemoved:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905131
    const-string v0, "original_post_time"

    iget-wide v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->originalPostTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 905132
    const-string v0, "minutiae_tag"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 905133
    const-string v0, "connection_class"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->connectionClass:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905134
    const-string v0, "promote_budget"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mBudgetData:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 905135
    const-string v0, "product_item"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 905136
    const-string v0, "marketplace_id"

    iget-wide v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mMarketplaceId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 905137
    const-string v0, "is_throwback_post"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isThrowbackPost:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905138
    const-string v0, "reshare_original_post"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->reshareOriginalPost:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905139
    const-string v0, "throwback_card"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->throwbackCardJson:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905140
    const-string v0, "source_type"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sourceType:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905141
    const-string v0, "referenced_sticker_id"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->stickerId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905142
    const-string v0, "is_backout_draft"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isBackoutDraft:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905143
    const-string v0, "is_compost_draftable"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isCompostDraftable:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905144
    const-string v0, "title"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->title:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905145
    const-string v0, "media_fbids"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaFbIds:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 905146
    const-string v0, "sync_object_uuid"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->syncObjectUUIDs:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 905147
    const-string v0, "media_captions"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaCaptions:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 905148
    const-string v0, "souvenir"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->souvenir:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905149
    const-string v0, "prompt_analytics"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 905150
    const-string v0, "warnAcknowledged"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->warnAcknowledged:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905151
    const-string v0, "canHandleWarning"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->canHandleSentryWarning:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905152
    const-string v0, "posting_to_feed_only"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isFeedOnlyPost:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905153
    const-string v0, "is_meme_share"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isMemeShare:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905154
    const-string v0, "rich_text_style"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 905155
    const-string v0, "is_member_bio_post"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isGroupMemberBioPost:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905156
    const-string v0, "cta_link"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaLink:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905157
    const-string v0, "cta_type"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaType:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 905158
    const-string v0, "is_placelist_post"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPlacelistPost:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 905159
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 905160
    check-cast p1, Lcom/facebook/composer/publish/common/PublishPostParams;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/publish/common/PublishPostParamsSerializer;->a(Lcom/facebook/composer/publish/common/PublishPostParams;LX/0nX;LX/0my;)V

    return-void
.end method
