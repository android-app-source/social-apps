.class public Lcom/facebook/composer/publish/common/EditPostParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/EditPostParams$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/publish/common/EditPostParamsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/common/EditPostParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final d:Z

.field public final e:Ljava/lang/String;

.field public final f:Lcom/facebook/composer/publish/common/LinkEdit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Lcom/facebook/ipc/composer/model/MinutiaeTag;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:J

.field public final l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Z

.field public final q:LX/21D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:Lcom/facebook/composer/publish/common/StickerEdit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final s:Ljava/lang/String;

.field public final t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final u:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 903962
    const-class v0, Lcom/facebook/composer/publish/common/EditPostParams$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 903963
    const-class v0, Lcom/facebook/composer/publish/common/EditPostParamsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 903964
    new-instance v0, LX/5Lw;

    invoke-direct {v0}, LX/5Lw;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/EditPostParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 903965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 903966
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 903967
    iput-object v6, p0, Lcom/facebook/composer/publish/common/EditPostParams;->a:LX/0Px;

    .line 903968
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->b:Ljava/lang/String;

    .line 903969
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v2, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->c:Z

    .line 903970
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v2, :cond_3

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->d:Z

    .line 903971
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->e:Ljava/lang/String;

    .line 903972
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 903973
    iput-object v6, p0, Lcom/facebook/composer/publish/common/EditPostParams;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    .line 903974
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    move v0, v1

    .line 903975
    :goto_4
    array-length v4, v3

    if-ge v0, v4, :cond_5

    .line 903976
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 903977
    aput-object v4, v3, v0

    .line 903978
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 903979
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    move v0, v1

    .line 903980
    :goto_5
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 903981
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 903982
    aput-object v4, v3, v0

    .line 903983
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 903984
    :cond_1
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->a:LX/0Px;

    goto :goto_0

    :cond_2
    move v0, v1

    .line 903985
    goto :goto_1

    :cond_3
    move v0, v1

    .line 903986
    goto :goto_2

    .line 903987
    :cond_4
    const-class v0, Lcom/facebook/composer/publish/common/LinkEdit;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/LinkEdit;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    goto :goto_3

    .line 903988
    :cond_5
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->g:LX/0Px;

    .line 903989
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    move v0, v1

    .line 903990
    :goto_6
    array-length v4, v3

    if-ge v0, v4, :cond_6

    .line 903991
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 903992
    aput-object v4, v3, v0

    .line 903993
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 903994
    :cond_6
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->h:LX/0Px;

    .line 903995
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    .line 903996
    iput-object v6, p0, Lcom/facebook/composer/publish/common/EditPostParams;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 903997
    :goto_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_8

    .line 903998
    iput-object v6, p0, Lcom/facebook/composer/publish/common/EditPostParams;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 903999
    :goto_8
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/composer/publish/common/EditPostParams;->k:J

    .line 904000
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_9

    .line 904001
    iput-object v6, p0, Lcom/facebook/composer/publish/common/EditPostParams;->l:Ljava/lang/String;

    .line 904002
    :goto_9
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_a

    .line 904003
    iput-object v6, p0, Lcom/facebook/composer/publish/common/EditPostParams;->m:Ljava/lang/String;

    .line 904004
    :goto_a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_b

    .line 904005
    iput-object v6, p0, Lcom/facebook/composer/publish/common/EditPostParams;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 904006
    :goto_b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_c

    .line 904007
    iput-object v6, p0, Lcom/facebook/composer/publish/common/EditPostParams;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 904008
    :goto_c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v2, :cond_d

    :goto_d
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->p:Z

    .line 904009
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_e

    .line 904010
    iput-object v6, p0, Lcom/facebook/composer/publish/common/EditPostParams;->q:LX/21D;

    .line 904011
    :goto_e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_f

    .line 904012
    iput-object v6, p0, Lcom/facebook/composer/publish/common/EditPostParams;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    .line 904013
    :goto_f
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->s:Ljava/lang/String;

    .line 904014
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_10

    .line 904015
    iput-object v6, p0, Lcom/facebook/composer/publish/common/EditPostParams;->t:LX/0Px;

    .line 904016
    :goto_10
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->u:J

    .line 904017
    return-void

    .line 904018
    :cond_7
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    goto :goto_7

    .line 904019
    :cond_8
    sget-object v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    goto :goto_8

    .line 904020
    :cond_9
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->l:Ljava/lang/String;

    goto :goto_9

    .line 904021
    :cond_a
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->m:Ljava/lang/String;

    goto :goto_a

    .line 904022
    :cond_b
    sget-object v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    goto :goto_b

    .line 904023
    :cond_c
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    goto :goto_c

    :cond_d
    move v2, v1

    .line 904024
    goto :goto_d

    .line 904025
    :cond_e
    invoke-static {}, LX/21D;->values()[LX/21D;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->q:LX/21D;

    goto :goto_e

    .line 904026
    :cond_f
    sget-object v0, Lcom/facebook/composer/publish/common/StickerEdit;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/StickerEdit;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    goto :goto_f

    .line 904027
    :cond_10
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Long;

    .line 904028
    :goto_11
    array-length v2, v0

    if-ge v1, v2, :cond_11

    .line 904029
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 904030
    aput-object v2, v0, v1

    .line 904031
    add-int/lit8 v1, v1, 0x1

    goto :goto_11

    .line 904032
    :cond_11
    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->t:LX/0Px;

    goto :goto_10
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/common/EditPostParams$Builder;)V
    .locals 2

    .prologue
    .line 903864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 903865
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->a:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->a:LX/0Px;

    .line 903866
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->b:Ljava/lang/String;

    .line 903867
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->c:Z

    .line 903868
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->d:Z

    .line 903869
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->e:Ljava/lang/String;

    .line 903870
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    .line 903871
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->g:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->g:LX/0Px;

    .line 903872
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->h:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->h:LX/0Px;

    .line 903873
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 903874
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 903875
    iget-wide v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->k:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->k:J

    .line 903876
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->l:Ljava/lang/String;

    .line 903877
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->m:Ljava/lang/String;

    .line 903878
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 903879
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 903880
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->p:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->p:Z

    .line 903881
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->q:LX/21D;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->q:LX/21D;

    .line 903882
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    .line 903883
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->s:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->s:Ljava/lang/String;

    .line 903884
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->t:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->t:LX/0Px;

    .line 903885
    iget-wide v0, p1, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->u:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->u:J

    .line 903886
    return-void
.end method

.method public static a(Lcom/facebook/composer/publish/common/EditPostParams;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 2

    .prologue
    .line 904033
    new-instance v0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;-><init>(Lcom/facebook/composer/publish/common/EditPostParams;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 2

    .prologue
    .line 904034
    new-instance v0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    invoke-direct {v0}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 904035
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 904042
    if-ne p0, p1, :cond_1

    .line 904043
    :cond_0
    :goto_0
    return v0

    .line 904044
    :cond_1
    instance-of v2, p1, Lcom/facebook/composer/publish/common/EditPostParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 904045
    goto :goto_0

    .line 904046
    :cond_2
    check-cast p1, Lcom/facebook/composer/publish/common/EditPostParams;

    .line 904047
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->a:LX/0Px;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->a:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 904048
    goto :goto_0

    .line 904049
    :cond_3
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 904050
    goto :goto_0

    .line 904051
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->c:Z

    iget-boolean v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 904052
    goto :goto_0

    .line 904053
    :cond_5
    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->d:Z

    iget-boolean v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 904054
    goto :goto_0

    .line 904055
    :cond_6
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 904056
    goto :goto_0

    .line 904057
    :cond_7
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 904058
    goto :goto_0

    .line 904059
    :cond_8
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->g:LX/0Px;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->g:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 904060
    goto :goto_0

    .line 904061
    :cond_9
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->h:LX/0Px;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->h:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 904062
    goto :goto_0

    .line 904063
    :cond_a
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 904064
    goto :goto_0

    .line 904065
    :cond_b
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 904066
    goto :goto_0

    .line 904067
    :cond_c
    iget-wide v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->k:J

    iget-wide v4, p1, Lcom/facebook/composer/publish/common/EditPostParams;->k:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    move v0, v1

    .line 904068
    goto/16 :goto_0

    .line 904069
    :cond_d
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->l:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->l:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 904070
    goto/16 :goto_0

    .line 904071
    :cond_e
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->m:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->m:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 904072
    goto/16 :goto_0

    .line 904073
    :cond_f
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 904074
    goto/16 :goto_0

    .line 904075
    :cond_10
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 904076
    goto/16 :goto_0

    .line 904077
    :cond_11
    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->p:Z

    iget-boolean v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->p:Z

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 904078
    goto/16 :goto_0

    .line 904079
    :cond_12
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->q:LX/21D;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->q:LX/21D;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 904080
    goto/16 :goto_0

    .line 904081
    :cond_13
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    move v0, v1

    .line 904082
    goto/16 :goto_0

    .line 904083
    :cond_14
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->s:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->s:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    move v0, v1

    .line 904084
    goto/16 :goto_0

    .line 904085
    :cond_15
    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->t:LX/0Px;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/EditPostParams;->t:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move v0, v1

    .line 904086
    goto/16 :goto_0

    .line 904087
    :cond_16
    iget-wide v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->u:J

    iget-wide v4, p1, Lcom/facebook/composer/publish/common/EditPostParams;->u:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 904088
    goto/16 :goto_0
.end method

.method public getCacheIds()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cache_ids"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 904036
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->a:LX/0Px;

    return-object v0
.end method

.method public getComposerSessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_session_id"
    .end annotation

    .prologue
    .line 904037
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getLegacyStoryApiId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "legacy_story_api_id"
    .end annotation

    .prologue
    .line 904038
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkEdit()Lcom/facebook/composer/publish/common/LinkEdit;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "link_edit"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 904039
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    return-object v0
.end method

.method public getMediaCaptions()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_captions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 904040
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->g:LX/0Px;

    return-object v0
.end method

.method public getMediaFbIds()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_fb_ids"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 904041
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->h:LX/0Px;

    return-object v0
.end method

.method public getMessage()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "message"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 903960
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public getMinutiaeTag()Lcom/facebook/ipc/composer/model/MinutiaeTag;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "minutiae_tag"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 903961
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    return-object v0
.end method

.method public getOriginalPostTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "original_post_time"
    .end annotation

    .prologue
    .line 903853
    iget-wide v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->k:J

    return-wide v0
.end method

.method public getPlaceTag()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "place_tag"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 903854
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->l:Ljava/lang/String;

    return-object v0
.end method

.method public getPrivacy()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "privacy"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 903855
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->m:Ljava/lang/String;

    return-object v0
.end method

.method public getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "product_item_attachment"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 903856
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    return-object v0
.end method

.method public getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rich_text_style"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 903857
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    return-object v0
.end method

.method public getSourceType()LX/21D;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "source_type"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 903858
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->q:LX/21D;

    return-object v0
.end method

.method public getStickerEdit()Lcom/facebook/composer/publish/common/StickerEdit;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sticker_edit"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 903859
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    return-object v0
.end method

.method public getStoryId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "story_id"
    .end annotation

    .prologue
    .line 903860
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->s:Ljava/lang/String;

    return-object v0
.end method

.method public getTaggedIds()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tagged_ids"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 903861
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->t:LX/0Px;

    return-object v0
.end method

.method public getTargetId()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_id"
    .end annotation

    .prologue
    .line 903862
    iget-wide v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->u:J

    return-wide v0
.end method

.method public hasMediaFbIds()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_media_fb_ids"
    .end annotation

    .prologue
    .line 903863
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->c:Z

    return v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 903887
    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->a:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->g:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->h:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->k:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->l:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->m:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->p:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->q:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->s:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->t:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-wide v2, p0, Lcom/facebook/composer/publish/common/EditPostParams;->u:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isPhotoContainer()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_photo_container"
    .end annotation

    .prologue
    .line 903888
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->d:Z

    return v0
.end method

.method public shouldPublishUnpublishedContent()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_publish_unpublished_content"
    .end annotation

    .prologue
    .line 903889
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->p:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 903890
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->a:LX/0Px;

    if-nez v0, :cond_1

    .line 903891
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 903892
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 903893
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 903894
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->d:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 903895
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 903896
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    if-nez v0, :cond_4

    .line 903897
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 903898
    :goto_2
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 903899
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_3
    if-ge v3, v4, :cond_5

    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->g:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 903900
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 903901
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 903902
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 903903
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 903904
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_4
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->a:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 903905
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 903906
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_2
    move v0, v2

    .line 903907
    goto :goto_0

    :cond_3
    move v0, v2

    .line 903908
    goto :goto_1

    .line 903909
    :cond_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 903910
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_2

    .line 903911
    :cond_5
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 903912
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_5
    if-ge v3, v4, :cond_6

    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->h:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 903913
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 903914
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 903915
    :cond_6
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-nez v0, :cond_8

    .line 903916
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 903917
    :goto_6
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    if-nez v0, :cond_9

    .line 903918
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 903919
    :goto_7
    iget-wide v4, p0, Lcom/facebook/composer/publish/common/EditPostParams;->k:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 903920
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->l:Ljava/lang/String;

    if-nez v0, :cond_a

    .line 903921
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 903922
    :goto_8
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->m:Ljava/lang/String;

    if-nez v0, :cond_b

    .line 903923
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 903924
    :goto_9
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    if-nez v0, :cond_c

    .line 903925
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 903926
    :goto_a
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    if-nez v0, :cond_d

    .line 903927
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 903928
    :goto_b
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->p:Z

    if-eqz v0, :cond_e

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 903929
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->q:LX/21D;

    if-nez v0, :cond_f

    .line 903930
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 903931
    :goto_d
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    if-nez v0, :cond_10

    .line 903932
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 903933
    :goto_e
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 903934
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->t:LX/0Px;

    if-nez v0, :cond_11

    .line 903935
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 903936
    :cond_7
    iget-wide v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->u:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 903937
    return-void

    .line 903938
    :cond_8
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 903939
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto :goto_6

    .line 903940
    :cond_9
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 903941
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/MinutiaeTag;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_7

    .line 903942
    :cond_a
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 903943
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_8

    .line 903944
    :cond_b
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 903945
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_9

    .line 903946
    :cond_c
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 903947
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_a

    .line 903948
    :cond_d
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 903949
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_b

    :cond_e
    move v0, v2

    .line 903950
    goto :goto_c

    .line 903951
    :cond_f
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 903952
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->q:LX/21D;

    invoke-virtual {v0}, LX/21D;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_d

    .line 903953
    :cond_10
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 903954
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/publish/common/StickerEdit;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_e

    .line 903955
    :cond_11
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 903956
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 903957
    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    :goto_f
    if-ge v2, v1, :cond_7

    iget-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams;->t:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 903958
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 903959
    add-int/lit8 v2, v2, 0x1

    goto :goto_f
.end method
