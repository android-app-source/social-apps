.class public Lcom/facebook/composer/publish/common/MediaAttachmentSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/publish/common/MediaAttachment;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 904536
    const-class v0, Lcom/facebook/composer/publish/common/MediaAttachment;

    new-instance v1, Lcom/facebook/composer/publish/common/MediaAttachmentSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/publish/common/MediaAttachmentSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 904537
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 904535
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/publish/common/MediaAttachment;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904529
    if-nez p0, :cond_0

    .line 904530
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 904531
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 904532
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/publish/common/MediaAttachmentSerializer;->b(Lcom/facebook/composer/publish/common/MediaAttachment;LX/0nX;LX/0my;)V

    .line 904533
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 904534
    return-void
.end method

.method private static b(Lcom/facebook/composer/publish/common/MediaAttachment;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 904519
    const-string v0, "has_crop"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/MediaAttachment;->getHasCrop()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 904520
    const-string v0, "has_filter"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/MediaAttachment;->getHasFilter()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 904521
    const-string v0, "has_rotation"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/MediaAttachment;->getHasRotation()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 904522
    const-string v0, "media_fbid"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/MediaAttachment;->getMediaFbid()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904523
    const-string v0, "message"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/MediaAttachment;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904524
    const-string v0, "notify_when_processed"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/MediaAttachment;->getNotifyWhenProcessed()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 904525
    const-string v0, "stickers"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/MediaAttachment;->getStickers()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904526
    const-string v0, "sync_object_uuid"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/MediaAttachment;->getSyncObjectUuid()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904527
    const-string v0, "text_overlay"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/MediaAttachment;->getTextOverlay()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904528
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904518
    check-cast p1, Lcom/facebook/composer/publish/common/MediaAttachment;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/publish/common/MediaAttachmentSerializer;->a(Lcom/facebook/composer/publish/common/MediaAttachment;LX/0nX;LX/0my;)V

    return-void
.end method
