.class public Lcom/facebook/composer/publish/common/StickerEdit;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/StickerEditDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/publish/common/StickerEditSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/common/StickerEdit;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/composer/publish/common/StickerEdit;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field


# instance fields
.field public final noSticker:Ljava/lang/Boolean;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "no_sticker"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final referencedSticker:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "referenced_sticker_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 905196
    const-class v0, Lcom/facebook/composer/publish/common/StickerEditDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 905195
    const-class v0, Lcom/facebook/composer/publish/common/StickerEditSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 905193
    new-instance v0, Lcom/facebook/composer/publish/common/StickerEdit;

    invoke-direct {v0}, Lcom/facebook/composer/publish/common/StickerEdit;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/StickerEdit;->a:Lcom/facebook/composer/publish/common/StickerEdit;

    .line 905194
    new-instance v0, LX/5MB;

    invoke-direct {v0}, LX/5MB;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/StickerEdit;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 905189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905190
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/StickerEdit;->noSticker:Ljava/lang/Boolean;

    .line 905191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/StickerEdit;->referencedSticker:Ljava/lang/String;

    .line 905192
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 905166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905167
    invoke-static {p1}, LX/46R;->e(Landroid/os/Parcel;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/StickerEdit;->noSticker:Ljava/lang/Boolean;

    .line 905168
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/StickerEdit;->referencedSticker:Ljava/lang/String;

    .line 905169
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 905182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905183
    if-nez p1, :cond_0

    .line 905184
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/StickerEdit;->noSticker:Ljava/lang/Boolean;

    .line 905185
    iput-object v1, p0, Lcom/facebook/composer/publish/common/StickerEdit;->referencedSticker:Ljava/lang/String;

    .line 905186
    :goto_0
    return-void

    .line 905187
    :cond_0
    iput-object v1, p0, Lcom/facebook/composer/publish/common/StickerEdit;->noSticker:Ljava/lang/Boolean;

    .line 905188
    iput-object p1, p0, Lcom/facebook/composer/publish/common/StickerEdit;->referencedSticker:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/composer/publish/common/StickerEdit;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 905181
    new-instance v0, Lcom/facebook/composer/publish/common/StickerEdit;

    invoke-direct {v0, p0}, Lcom/facebook/composer/publish/common/StickerEdit;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 905180
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 905174
    if-ne p0, p1, :cond_1

    .line 905175
    :cond_0
    :goto_0
    return v0

    .line 905176
    :cond_1
    instance-of v2, p1, Lcom/facebook/composer/publish/common/StickerEdit;

    if-nez v2, :cond_2

    move v0, v1

    .line 905177
    goto :goto_0

    .line 905178
    :cond_2
    check-cast p1, Lcom/facebook/composer/publish/common/StickerEdit;

    .line 905179
    iget-object v2, p0, Lcom/facebook/composer/publish/common/StickerEdit;->noSticker:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/StickerEdit;->noSticker:Ljava/lang/Boolean;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/composer/publish/common/StickerEdit;->referencedSticker:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/StickerEdit;->referencedSticker:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 905173
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/composer/publish/common/StickerEdit;->noSticker:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/composer/publish/common/StickerEdit;->referencedSticker:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 905170
    iget-object v0, p0, Lcom/facebook/composer/publish/common/StickerEdit;->noSticker:Ljava/lang/Boolean;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Boolean;)V

    .line 905171
    iget-object v0, p0, Lcom/facebook/composer/publish/common/StickerEdit;->referencedSticker:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 905172
    return-void
.end method
