.class public Lcom/facebook/composer/publish/common/PendingStory;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/publish/common/PendingStorySerializer;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/4Zi;

.field public final c:I

.field public final dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "pending_story_persistent_data"
    .end annotation
.end field


# direct methods
.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904607
    const-class v0, Lcom/facebook/composer/publish/common/PendingStorySerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 904615
    const-class v0, Lcom/facebook/composer/publish/common/PendingStory;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/publish/common/PendingStory;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/5M1;)V
    .locals 1

    .prologue
    .line 904610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904611
    iget-object v0, p1, LX/5M1;->a:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    .line 904612
    iget-object v0, p1, LX/5M1;->b:LX/4Zi;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->b:LX/4Zi;

    .line 904613
    iget v0, p1, LX/5M1;->c:I

    iput v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->c:I

    .line 904614
    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 1

    .prologue
    .line 904609
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->b:LX/4Zi;

    invoke-virtual {v0, p1, p2}, LX/4Zi;->b(J)I

    move-result v0

    return v0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1

    .prologue
    .line 904608
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->story:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final b()Lcom/facebook/composer/publish/common/PostParamsWrapper;
    .locals 1

    .prologue
    .line 904593
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    return-object v0
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 904602
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->b:LX/4Zi;

    .line 904603
    invoke-virtual {v0}, LX/4Zi;->d()Z

    move-result p0

    if-nez p0, :cond_0

    .line 904604
    :goto_0
    return-void

    .line 904605
    :cond_0
    const/16 p0, 0x3e8

    invoke-virtual {v0, p1, p2, p0}, LX/4Zi;->c(JI)V

    .line 904606
    sget-object p0, LX/4Zg;->FINISHING_UP:LX/4Zg;

    iput-object p0, v0, LX/4Zi;->c:LX/4Zg;

    goto :goto_0
.end method

.method public final b(JZ)V
    .locals 3

    .prologue
    .line 904616
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->b:LX/4Zi;

    invoke-virtual {v0}, LX/4Zi;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 904617
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->b:LX/4Zi;

    invoke-virtual {v0}, LX/4Zi;->a()V

    .line 904618
    :cond_0
    if-eqz p3, :cond_1

    iget v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->c:I

    .line 904619
    :goto_0
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PendingStory;->b:LX/4Zi;

    invoke-virtual {v1, p1, p2, v0}, LX/4Zi;->a(JI)V

    .line 904620
    return-void

    .line 904621
    :cond_1
    const/16 v0, 0x320

    goto :goto_0
.end method

.method public final c()Lcom/facebook/composer/publish/common/PublishAttemptInfo;
    .locals 1

    .prologue
    .line 904601
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->publishAttemptInfo:Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 904600
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->publishAttemptInfo:Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/facebook/composer/publish/common/ErrorDetails;
    .locals 1

    .prologue
    .line 904599
    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/PendingStory;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->publishAttemptInfo:Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->getErrorDetails()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 904596
    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/PendingStory;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    .line 904597
    iget-object p0, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->publishAttemptInfo:Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->getAttemptCount()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    move v0, p0

    .line 904598
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 904595
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->b:LX/4Zi;

    invoke-virtual {v0}, LX/4Zi;->c()Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 904594
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStory;->b:LX/4Zi;

    invoke-virtual {v0}, LX/4Zi;->b()Z

    move-result v0

    return v0
.end method
