.class public Lcom/facebook/composer/publish/common/PendingStoryPersistentData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/publish/common/PendingStoryPersistentDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/common/PendingStoryPersistentData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "post_params_wrapper"
    .end annotation
.end field

.field public final publishAttemptInfo:Lcom/facebook/composer/publish/common/PublishAttemptInfo;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "publish_attempt_info"
    .end annotation
.end field

.field public final story:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "graphql_story"
    .end annotation
.end field


# direct methods
.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904641
    const-class v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 904640
    new-instance v0, LX/5M2;

    invoke-direct {v0}, LX/5M2;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 904625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904626
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->story:Lcom/facebook/graphql/model/GraphQLStory;

    .line 904627
    const-class v0, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    .line 904628
    const-class v0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->publishAttemptInfo:Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    .line 904629
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V
    .locals 0

    .prologue
    .line 904635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904636
    iput-object p1, p0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->story:Lcom/facebook/graphql/model/GraphQLStory;

    .line 904637
    iput-object p2, p0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    .line 904638
    iput-object p3, p0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->publishAttemptInfo:Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    .line 904639
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 904634
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 904630
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->story:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 904631
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 904632
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->publishAttemptInfo:Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 904633
    return-void
.end method
