.class public final Lcom/facebook/composer/publish/common/MediaAttachment$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/MediaAttachment_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904403
    const-class v0, Lcom/facebook/composer/publish/common/MediaAttachment_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 904387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904388
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/publish/common/MediaAttachment;
    .locals 2

    .prologue
    .line 904404
    new-instance v0, Lcom/facebook/composer/publish/common/MediaAttachment;

    invoke-direct {v0, p0}, Lcom/facebook/composer/publish/common/MediaAttachment;-><init>(Lcom/facebook/composer/publish/common/MediaAttachment$Builder;)V

    return-object v0
.end method

.method public setHasCrop(Z)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_crop"
    .end annotation

    .prologue
    .line 904407
    iput-boolean p1, p0, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->a:Z

    .line 904408
    return-object p0
.end method

.method public setHasFilter(Z)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_filter"
    .end annotation

    .prologue
    .line 904405
    iput-boolean p1, p0, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->b:Z

    .line 904406
    return-object p0
.end method

.method public setHasRotation(Z)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_rotation"
    .end annotation

    .prologue
    .line 904399
    iput-boolean p1, p0, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->c:Z

    .line 904400
    return-object p0
.end method

.method public setMediaFbid(Ljava/lang/String;)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_fbid"
    .end annotation

    .prologue
    .line 904401
    iput-object p1, p0, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->d:Ljava/lang/String;

    .line 904402
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "message"
    .end annotation

    .prologue
    .line 904397
    iput-object p1, p0, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->e:Ljava/lang/String;

    .line 904398
    return-object p0
.end method

.method public setNotifyWhenProcessed(Z)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "notify_when_processed"
    .end annotation

    .prologue
    .line 904395
    iput-boolean p1, p0, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->f:Z

    .line 904396
    return-object p0
.end method

.method public setStickers(Ljava/lang/String;)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "stickers"
    .end annotation

    .prologue
    .line 904393
    iput-object p1, p0, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->g:Ljava/lang/String;

    .line 904394
    return-object p0
.end method

.method public setSyncObjectUuid(Ljava/lang/String;)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sync_object_uuid"
    .end annotation

    .prologue
    .line 904391
    iput-object p1, p0, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->h:Ljava/lang/String;

    .line 904392
    return-object p0
.end method

.method public setTextOverlay(Ljava/lang/String;)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_overlay"
    .end annotation

    .prologue
    .line 904389
    iput-object p1, p0, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->i:Ljava/lang/String;

    .line 904390
    return-object p0
.end method
