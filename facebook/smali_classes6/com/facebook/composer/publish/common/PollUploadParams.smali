.class public Lcom/facebook/composer/publish/common/PollUploadParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/common/PollUploadParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z

.field public final e:Z

.field public final f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 904673
    new-instance v0, LX/5M3;

    invoke-direct {v0}, LX/5M3;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/PollUploadParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 904674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904675
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->a:Ljava/lang/String;

    .line 904676
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->b:Ljava/lang/String;

    .line 904677
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->c:Ljava/util/List;

    .line 904678
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->d:Z

    .line 904679
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->e:Z

    .line 904680
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->f:Z

    .line 904681
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZZZ)V"
        }
    .end annotation

    .prologue
    .line 904682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904683
    iput-object p1, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->a:Ljava/lang/String;

    .line 904684
    iput-object p2, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->b:Ljava/lang/String;

    .line 904685
    iput-object p3, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->c:Ljava/util/List;

    .line 904686
    iput-boolean p4, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->d:Z

    .line 904687
    iput-boolean p5, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->e:Z

    .line 904688
    iput-boolean p6, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->f:Z

    .line 904689
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 904690
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 904691
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 904692
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 904693
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 904694
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 904695
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 904696
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PollUploadParams;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 904697
    return-void
.end method
