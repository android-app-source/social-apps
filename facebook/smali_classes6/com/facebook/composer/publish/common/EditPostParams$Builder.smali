.class public final Lcom/facebook/composer/publish/common/EditPostParams$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/EditPostParams_BuilderDeserializer;
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Lcom/facebook/composer/publish/common/LinkEdit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/ipc/composer/model/MinutiaeTag;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:J

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Z

.field public q:LX/21D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/composer/publish/common/StickerEdit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;

.field public t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 903844
    const-class v0, Lcom/facebook/composer/publish/common/EditPostParams_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 903835
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 903836
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->b:Ljava/lang/String;

    .line 903837
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->e:Ljava/lang/String;

    .line 903838
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 903839
    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->g:LX/0Px;

    .line 903840
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 903841
    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->h:LX/0Px;

    .line 903842
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->s:Ljava/lang/String;

    .line 903843
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/common/EditPostParams;)V
    .locals 2

    .prologue
    .line 903788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 903789
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 903790
    instance-of v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;

    if-eqz v0, :cond_0

    .line 903791
    check-cast p1, Lcom/facebook/composer/publish/common/EditPostParams;

    .line 903792
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->a:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->a:LX/0Px;

    .line 903793
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->b:Ljava/lang/String;

    .line 903794
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->c:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->c:Z

    .line 903795
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->d:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->d:Z

    .line 903796
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->e:Ljava/lang/String;

    .line 903797
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    .line 903798
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->g:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->g:LX/0Px;

    .line 903799
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->h:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->h:LX/0Px;

    .line 903800
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 903801
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 903802
    iget-wide v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->k:J

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->k:J

    .line 903803
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->l:Ljava/lang/String;

    .line 903804
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->m:Ljava/lang/String;

    .line 903805
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 903806
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 903807
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->p:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->p:Z

    .line 903808
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->q:LX/21D;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->q:LX/21D;

    .line 903809
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    .line 903810
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->s:Ljava/lang/String;

    .line 903811
    iget-object v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->t:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->t:LX/0Px;

    .line 903812
    iget-wide v0, p1, Lcom/facebook/composer/publish/common/EditPostParams;->u:J

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->u:J

    .line 903813
    :goto_0
    return-void

    .line 903814
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->a:LX/0Px;

    .line 903815
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getComposerSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->b:Ljava/lang/String;

    .line 903816
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->hasMediaFbIds()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->c:Z

    .line 903817
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->isPhotoContainer()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->d:Z

    .line 903818
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getLegacyStoryApiId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->e:Ljava/lang/String;

    .line 903819
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getLinkEdit()Lcom/facebook/composer/publish/common/LinkEdit;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    .line 903820
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getMediaCaptions()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->g:LX/0Px;

    .line 903821
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getMediaFbIds()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->h:LX/0Px;

    .line 903822
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getMessage()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 903823
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getMinutiaeTag()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 903824
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getOriginalPostTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->k:J

    .line 903825
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getPlaceTag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->l:Ljava/lang/String;

    .line 903826
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getPrivacy()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->m:Ljava/lang/String;

    .line 903827
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 903828
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 903829
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->shouldPublishUnpublishedContent()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->p:Z

    .line 903830
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getSourceType()LX/21D;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->q:LX/21D;

    .line 903831
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getStickerEdit()Lcom/facebook/composer/publish/common/StickerEdit;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    .line 903832
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getStoryId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->s:Ljava/lang/String;

    .line 903833
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getTaggedIds()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->t:LX/0Px;

    .line 903834
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/EditPostParams;->getTargetId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->u:J

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/publish/common/EditPostParams;
    .locals 2

    .prologue
    .line 903787
    new-instance v0, Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-direct {v0, p0}, Lcom/facebook/composer/publish/common/EditPostParams;-><init>(Lcom/facebook/composer/publish/common/EditPostParams$Builder;)V

    return-object v0
.end method

.method public setCacheIds(LX/0Px;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cache_ids"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/composer/publish/common/EditPostParams$Builder;"
        }
    .end annotation

    .prologue
    .line 903785
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->a:LX/0Px;

    .line 903786
    return-object p0
.end method

.method public setComposerSessionId(Ljava/lang/String;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_session_id"
    .end annotation

    .prologue
    .line 903783
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->b:Ljava/lang/String;

    .line 903784
    return-object p0
.end method

.method public setHasMediaFbIds(Z)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_media_fb_ids"
    .end annotation

    .prologue
    .line 903781
    iput-boolean p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->c:Z

    .line 903782
    return-object p0
.end method

.method public setIsPhotoContainer(Z)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_photo_container"
    .end annotation

    .prologue
    .line 903779
    iput-boolean p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->d:Z

    .line 903780
    return-object p0
.end method

.method public setLegacyStoryApiId(Ljava/lang/String;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "legacy_story_api_id"
    .end annotation

    .prologue
    .line 903777
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->e:Ljava/lang/String;

    .line 903778
    return-object p0
.end method

.method public setLinkEdit(Lcom/facebook/composer/publish/common/LinkEdit;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .param p1    # Lcom/facebook/composer/publish/common/LinkEdit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "link_edit"
    .end annotation

    .prologue
    .line 903775
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->f:Lcom/facebook/composer/publish/common/LinkEdit;

    .line 903776
    return-object p0
.end method

.method public setMediaCaptions(LX/0Px;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_captions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/composer/publish/common/EditPostParams$Builder;"
        }
    .end annotation

    .prologue
    .line 903773
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->g:LX/0Px;

    .line 903774
    return-object p0
.end method

.method public setMediaFbIds(LX/0Px;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_fb_ids"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/composer/publish/common/EditPostParams$Builder;"
        }
    .end annotation

    .prologue
    .line 903771
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->h:LX/0Px;

    .line 903772
    return-object p0
.end method

.method public setMessage(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "message"
    .end annotation

    .prologue
    .line 903845
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 903846
    return-object p0
.end method

.method public setMinutiaeTag(Lcom/facebook/ipc/composer/model/MinutiaeTag;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/MinutiaeTag;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "minutiae_tag"
    .end annotation

    .prologue
    .line 903769
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 903770
    return-object p0
.end method

.method public setOriginalPostTime(J)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "original_post_time"
    .end annotation

    .prologue
    .line 903767
    iput-wide p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->k:J

    .line 903768
    return-object p0
.end method

.method public setPlaceTag(Ljava/lang/String;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "place_tag"
    .end annotation

    .prologue
    .line 903747
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->l:Ljava/lang/String;

    .line 903748
    return-object p0
.end method

.method public setPrivacy(Ljava/lang/String;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "privacy"
    .end annotation

    .prologue
    .line 903765
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->m:Ljava/lang/String;

    .line 903766
    return-object p0
.end method

.method public setProductItemAttachment(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ProductItemAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "product_item_attachment"
    .end annotation

    .prologue
    .line 903763
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->n:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 903764
    return-object p0
.end method

.method public setRichTextStyle(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rich_text_style"
    .end annotation

    .prologue
    .line 903761
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 903762
    return-object p0
.end method

.method public setShouldPublishUnpublishedContent(Z)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_publish_unpublished_content"
    .end annotation

    .prologue
    .line 903759
    iput-boolean p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->p:Z

    .line 903760
    return-object p0
.end method

.method public setSourceType(LX/21D;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .param p1    # LX/21D;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "source_type"
    .end annotation

    .prologue
    .line 903757
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->q:LX/21D;

    .line 903758
    return-object p0
.end method

.method public setStickerEdit(Lcom/facebook/composer/publish/common/StickerEdit;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .param p1    # Lcom/facebook/composer/publish/common/StickerEdit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sticker_edit"
    .end annotation

    .prologue
    .line 903755
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->r:Lcom/facebook/composer/publish/common/StickerEdit;

    .line 903756
    return-object p0
.end method

.method public setStoryId(Ljava/lang/String;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "story_id"
    .end annotation

    .prologue
    .line 903753
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->s:Ljava/lang/String;

    .line 903754
    return-object p0
.end method

.method public setTaggedIds(LX/0Px;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tagged_ids"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/facebook/composer/publish/common/EditPostParams$Builder;"
        }
    .end annotation

    .prologue
    .line 903751
    iput-object p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->t:LX/0Px;

    .line 903752
    return-object p0
.end method

.method public setTargetId(J)Lcom/facebook/composer/publish/common/EditPostParams$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_id"
    .end annotation

    .prologue
    .line 903749
    iput-wide p1, p0, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->u:J

    .line 903750
    return-object p0
.end method
