.class public Lcom/facebook/composer/publish/common/MediaAttachment;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/MediaAttachment$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/publish/common/MediaAttachmentSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/common/MediaAttachment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Z

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Z

.field private final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904415
    const-class v0, Lcom/facebook/composer/publish/common/MediaAttachment$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 904516
    const-class v0, Lcom/facebook/composer/publish/common/MediaAttachmentSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 904515
    new-instance v0, LX/5M0;

    invoke-direct {v0}, LX/5M0;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/MediaAttachment;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 904490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904491
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->a:Z

    .line 904492
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->b:Z

    .line 904493
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->c:Z

    .line 904494
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 904495
    iput-object v3, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->d:Ljava/lang/String;

    .line 904496
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 904497
    iput-object v3, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->e:Ljava/lang/String;

    .line 904498
    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->f:Z

    .line 904499
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    .line 904500
    iput-object v3, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->g:Ljava/lang/String;

    .line 904501
    :goto_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    .line 904502
    iput-object v3, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->h:Ljava/lang/String;

    .line 904503
    :goto_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_8

    .line 904504
    iput-object v3, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->i:Ljava/lang/String;

    .line 904505
    :goto_8
    return-void

    :cond_0
    move v0, v2

    .line 904506
    goto :goto_0

    :cond_1
    move v0, v2

    .line 904507
    goto :goto_1

    :cond_2
    move v0, v2

    .line 904508
    goto :goto_2

    .line 904509
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->d:Ljava/lang/String;

    goto :goto_3

    .line 904510
    :cond_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->e:Ljava/lang/String;

    goto :goto_4

    :cond_5
    move v1, v2

    .line 904511
    goto :goto_5

    .line 904512
    :cond_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->g:Ljava/lang/String;

    goto :goto_6

    .line 904513
    :cond_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->h:Ljava/lang/String;

    goto :goto_7

    .line 904514
    :cond_8
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->i:Ljava/lang/String;

    goto :goto_8
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/common/MediaAttachment$Builder;)V
    .locals 1

    .prologue
    .line 904479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904480
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->a:Z

    .line 904481
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->b:Z

    .line 904482
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->c:Z

    .line 904483
    iget-object v0, p1, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->d:Ljava/lang/String;

    .line 904484
    iget-object v0, p1, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->e:Ljava/lang/String;

    .line 904485
    iget-boolean v0, p1, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->f:Z

    .line 904486
    iget-object v0, p1, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->g:Ljava/lang/String;

    .line 904487
    iget-object v0, p1, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->h:Ljava/lang/String;

    .line 904488
    iget-object v0, p1, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->i:Ljava/lang/String;

    .line 904489
    return-void
.end method

.method public static newBuilder()Lcom/facebook/composer/publish/common/MediaAttachment$Builder;
    .locals 2

    .prologue
    .line 904478
    new-instance v0, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;

    invoke-direct {v0}, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 904477
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 904454
    if-ne p0, p1, :cond_1

    .line 904455
    :cond_0
    :goto_0
    return v0

    .line 904456
    :cond_1
    instance-of v2, p1, Lcom/facebook/composer/publish/common/MediaAttachment;

    if-nez v2, :cond_2

    move v0, v1

    .line 904457
    goto :goto_0

    .line 904458
    :cond_2
    check-cast p1, Lcom/facebook/composer/publish/common/MediaAttachment;

    .line 904459
    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->a:Z

    iget-boolean v3, p1, Lcom/facebook/composer/publish/common/MediaAttachment;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 904460
    goto :goto_0

    .line 904461
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->b:Z

    iget-boolean v3, p1, Lcom/facebook/composer/publish/common/MediaAttachment;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 904462
    goto :goto_0

    .line 904463
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->c:Z

    iget-boolean v3, p1, Lcom/facebook/composer/publish/common/MediaAttachment;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 904464
    goto :goto_0

    .line 904465
    :cond_5
    iget-object v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/MediaAttachment;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 904466
    goto :goto_0

    .line 904467
    :cond_6
    iget-object v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/MediaAttachment;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 904468
    goto :goto_0

    .line 904469
    :cond_7
    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->f:Z

    iget-boolean v3, p1, Lcom/facebook/composer/publish/common/MediaAttachment;->f:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 904470
    goto :goto_0

    .line 904471
    :cond_8
    iget-object v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/MediaAttachment;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 904472
    goto :goto_0

    .line 904473
    :cond_9
    iget-object v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/MediaAttachment;->h:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 904474
    goto :goto_0

    .line 904475
    :cond_a
    iget-object v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/publish/common/MediaAttachment;->i:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 904476
    goto :goto_0
.end method

.method public getHasCrop()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_crop"
    .end annotation

    .prologue
    .line 904453
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->a:Z

    return v0
.end method

.method public getHasFilter()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_filter"
    .end annotation

    .prologue
    .line 904517
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->b:Z

    return v0
.end method

.method public getHasRotation()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_rotation"
    .end annotation

    .prologue
    .line 904452
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->c:Z

    return v0
.end method

.method public getMediaFbid()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_fbid"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 904451
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "message"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 904450
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getNotifyWhenProcessed()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "notify_when_processed"
    .end annotation

    .prologue
    .line 904449
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->f:Z

    return v0
.end method

.method public getStickers()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "stickers"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 904448
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getSyncObjectUuid()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sync_object_uuid"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 904447
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getTextOverlay()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_overlay"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 904446
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 904445
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 904416
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 904417
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 904418
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 904419
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 904420
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 904421
    :goto_3
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 904422
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 904423
    :goto_4
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->f:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 904424
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->g:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 904425
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 904426
    :goto_6
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->h:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 904427
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 904428
    :goto_7
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->i:Ljava/lang/String;

    if-nez v0, :cond_8

    .line 904429
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 904430
    :goto_8
    return-void

    :cond_0
    move v0, v2

    .line 904431
    goto :goto_0

    :cond_1
    move v0, v2

    .line 904432
    goto :goto_1

    :cond_2
    move v0, v2

    .line 904433
    goto :goto_2

    .line 904434
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 904435
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3

    .line 904436
    :cond_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 904437
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_4

    :cond_5
    move v0, v2

    .line 904438
    goto :goto_5

    .line 904439
    :cond_6
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 904440
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_6

    .line 904441
    :cond_7
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 904442
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_7

    .line 904443
    :cond_8
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 904444
    iget-object v0, p0, Lcom/facebook/composer/publish/common/MediaAttachment;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_8
.end method
