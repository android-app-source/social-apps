.class public Lcom/facebook/composer/publish/common/PostParamsWrapperSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/publish/common/PostParamsWrapper;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 904780
    const-class v0, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    new-instance v1, Lcom/facebook/composer/publish/common/PostParamsWrapperSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapperSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 904781
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 904782
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/publish/common/PostParamsWrapper;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904783
    if-nez p0, :cond_0

    .line 904784
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 904785
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 904786
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/publish/common/PostParamsWrapperSerializer;->b(Lcom/facebook/composer/publish/common/PostParamsWrapper;LX/0nX;LX/0my;)V

    .line 904787
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 904788
    return-void
.end method

.method private static b(Lcom/facebook/composer/publish/common/PostParamsWrapper;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 904789
    const-string v0, "publish_post_params"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->i()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904790
    const-string v0, "edit_post_params"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->h()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904791
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904792
    check-cast p1, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/publish/common/PostParamsWrapperSerializer;->a(Lcom/facebook/composer/publish/common/PostParamsWrapper;LX/0nX;LX/0my;)V

    return-void
.end method
