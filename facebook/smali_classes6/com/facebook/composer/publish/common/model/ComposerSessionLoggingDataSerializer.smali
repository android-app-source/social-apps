.class public Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 905308
    const-class v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    new-instance v1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingDataSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 905309
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 905310
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 905302
    if-nez p0, :cond_0

    .line 905303
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 905304
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 905305
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingDataSerializer;->b(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;LX/0nX;LX/0my;)V

    .line 905306
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 905307
    return-void
.end method

.method private static b(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 905298
    const-string v0, "composition_duration"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->getCompositionDuration()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 905299
    const-string v0, "number_of_copy_pastes"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->getNumberOfPastes()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 905300
    const-string v0, "number_of_keystrokes"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->getNumberOfKeystrokes()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 905301
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 905297
    check-cast p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingDataSerializer;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;LX/0nX;LX/0my;)V

    return-void
.end method
