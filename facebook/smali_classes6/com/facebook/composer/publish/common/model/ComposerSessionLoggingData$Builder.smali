.class public final Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData_BuilderDeserializer;
.end annotation


# instance fields
.field public a:J

.field public b:I

.field public c:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 905254
    const-class v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 905237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905238
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)V
    .locals 2

    .prologue
    .line 905243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905244
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 905245
    instance-of v0, p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    if-eqz v0, :cond_0

    .line 905246
    check-cast p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 905247
    iget-wide v0, p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->a:J

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->a:J

    .line 905248
    iget v0, p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->b:I

    iput v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->b:I

    .line 905249
    iget v0, p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->c:I

    iput v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->c:I

    .line 905250
    :goto_0
    return-void

    .line 905251
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->getCompositionDuration()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->a:J

    .line 905252
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->getNumberOfPastes()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->b:I

    .line 905253
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->getNumberOfKeystrokes()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->c:I

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;
    .locals 2

    .prologue
    .line 905255
    new-instance v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    invoke-direct {v0, p0}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;-><init>(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;)V

    return-object v0
.end method

.method public setCompositionDuration(J)Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composition_duration"
    .end annotation

    .prologue
    .line 905241
    iput-wide p1, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->a:J

    .line 905242
    return-object p0
.end method

.method public setNumberOfCopyPastes(I)Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "number_of_copy_pastes"
    .end annotation

    .prologue
    .line 905239
    iput p1, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->b:I

    .line 905240
    return-object p0
.end method

.method public setNumberOfKeystrokes(I)Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "number_of_keystrokes"
    .end annotation

    .prologue
    .line 905235
    iput p1, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->c:I

    .line 905236
    return-object p0
.end method
