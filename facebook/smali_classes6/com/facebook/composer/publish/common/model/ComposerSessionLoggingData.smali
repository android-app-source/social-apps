.class public Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:I

.field public final c:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 905296
    const-class v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 905295
    const-class v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 905294
    new-instance v0, LX/5MC;

    invoke-direct {v0}, LX/5MC;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 905289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905290
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->a:J

    .line 905291
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->b:I

    .line 905292
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->c:I

    .line 905293
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;)V
    .locals 2

    .prologue
    .line 905284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905285
    iget-wide v0, p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->a:J

    .line 905286
    iget v0, p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->b:I

    .line 905287
    iget v0, p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->c:I

    .line 905288
    return-void
.end method

.method public static a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;
    .locals 2

    .prologue
    .line 905262
    new-instance v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;-><init>(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;
    .locals 2

    .prologue
    .line 905283
    new-instance v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;

    invoke-direct {v0}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 905282
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 905271
    if-ne p0, p1, :cond_1

    .line 905272
    :cond_0
    :goto_0
    return v0

    .line 905273
    :cond_1
    instance-of v2, p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    if-nez v2, :cond_2

    move v0, v1

    .line 905274
    goto :goto_0

    .line 905275
    :cond_2
    check-cast p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 905276
    iget-wide v2, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->a:J

    iget-wide v4, p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 905277
    goto :goto_0

    .line 905278
    :cond_3
    iget v2, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->b:I

    iget v3, p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 905279
    goto :goto_0

    .line 905280
    :cond_4
    iget v2, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->c:I

    iget v3, p1, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 905281
    goto :goto_0
.end method

.method public getCompositionDuration()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composition_duration"
    .end annotation

    .prologue
    .line 905270
    iget-wide v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->a:J

    return-wide v0
.end method

.method public getNumberOfKeystrokes()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "number_of_keystrokes"
    .end annotation

    .prologue
    .line 905269
    iget v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->c:I

    return v0
.end method

.method public getNumberOfPastes()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "number_of_copy_pastes"
    .end annotation

    .prologue
    .line 905268
    iget v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->b:I

    return v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 905267
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 905263
    iget-wide v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 905264
    iget v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 905265
    iget v0, p0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 905266
    return-void
.end method
