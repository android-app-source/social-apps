.class public Lcom/facebook/composer/publish/common/PublishAttemptInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/publish/common/PublishAttemptInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 904866
    const-class v0, Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    new-instance v1, Lcom/facebook/composer/publish/common/PublishAttemptInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/publish/common/PublishAttemptInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 904867
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 904868
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/publish/common/PublishAttemptInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904869
    if-nez p0, :cond_0

    .line 904870
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 904871
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 904872
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/publish/common/PublishAttemptInfoSerializer;->b(Lcom/facebook/composer/publish/common/PublishAttemptInfo;LX/0nX;LX/0my;)V

    .line 904873
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 904874
    return-void
.end method

.method private static b(Lcom/facebook/composer/publish/common/PublishAttemptInfo;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 904875
    const-string v0, "attempt_count"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->getAttemptCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 904876
    const-string v0, "error_details"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->getErrorDetails()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904877
    const-string v0, "retry_source"

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->getRetrySource()LX/5Ro;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 904878
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904879
    check-cast p1, Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/publish/common/PublishAttemptInfoSerializer;->a(Lcom/facebook/composer/publish/common/PublishAttemptInfo;LX/0nX;LX/0my;)V

    return-void
.end method
