.class public Lcom/facebook/composer/publish/common/ErrorDetailsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/publish/common/ErrorDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 904252
    const-class v0, Lcom/facebook/composer/publish/common/ErrorDetails;

    new-instance v1, Lcom/facebook/composer/publish/common/ErrorDetailsSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/publish/common/ErrorDetailsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 904253
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 904235
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/publish/common/ErrorDetails;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904246
    if-nez p0, :cond_0

    .line 904247
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 904248
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 904249
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/publish/common/ErrorDetailsSerializer;->b(Lcom/facebook/composer/publish/common/ErrorDetails;LX/0nX;LX/0my;)V

    .line 904250
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 904251
    return-void
.end method

.method private static b(Lcom/facebook/composer/publish/common/ErrorDetails;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 904237
    const-string v0, "is_retriable"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 904238
    const-string v0, "message"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->userMessage:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904239
    const-string v0, "log_message"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->logMessage:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904240
    const-string v0, "error_code"

    iget v1, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->errorCode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 904241
    const-string v0, "error_subcode"

    iget v1, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->errorSubcode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 904242
    const-string v0, "sentry_warning_with_user_confirmation_required"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isSentryWarningWithUserConfirmationRequired:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 904243
    const-string v0, "user_title"

    iget-object v1, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->userTitle:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 904244
    const-string v0, "video_transcoding_error"

    iget-boolean v1, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isVideoTranscodingError:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 904245
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 904236
    check-cast p1, Lcom/facebook/composer/publish/common/ErrorDetails;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/publish/common/ErrorDetailsSerializer;->a(Lcom/facebook/composer/publish/common/ErrorDetails;LX/0nX;LX/0my;)V

    return-void
.end method
