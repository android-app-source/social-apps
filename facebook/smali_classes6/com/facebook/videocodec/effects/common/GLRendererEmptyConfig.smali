.class public Lcom/facebook/videocodec/effects/common/GLRendererEmptyConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/videocodec/effects/common/GLRendererEmptyConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1039771
    new-instance v0, LX/61F;

    invoke-direct {v0}, LX/61F;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/common/GLRendererEmptyConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1039774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039775
    iput-object p1, p0, Lcom/facebook/videocodec/effects/common/GLRendererEmptyConfig;->a:Ljava/lang/String;

    .line 1039776
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1039777
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1039772
    iget-object v0, p0, Lcom/facebook/videocodec/effects/common/GLRendererEmptyConfig;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1039773
    return-void
.end method
