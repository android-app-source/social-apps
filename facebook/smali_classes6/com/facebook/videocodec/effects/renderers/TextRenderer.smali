.class public Lcom/facebook/videocodec/effects/renderers/TextRenderer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/61B;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/videocodec/effects/model/TextData;

.field private c:LX/7Sq;

.field private d:LX/5Pb;

.field private final e:[F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1075999
    const-class v0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/videocodec/effects/model/TextData;LX/7Sq;)V
    .locals 1
    .param p1    # Lcom/facebook/videocodec/effects/model/TextData;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1076000
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1076001
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->e:[F

    .line 1076002
    iput-object p1, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->a:Lcom/facebook/videocodec/effects/model/TextData;

    .line 1076003
    iput-object p2, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->c:LX/7Sq;

    .line 1076004
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    const/high16 v1, -0x40800000    # -1.0f

    const/4 v3, 0x0

    .line 1076005
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->e:[F

    invoke-static {v0, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1076006
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->e:[F

    invoke-static {v0, v3, v1, v1, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1076007
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->e:[F

    int-to-float v1, p1

    div-float v1, v5, v1

    int-to-float v2, p2

    div-float v2, v5, v2

    invoke-static {v0, v3, v1, v2, v4}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 1076008
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1076009
    const v0, 0x7f070089

    const v1, 0x7f070088

    invoke-interface {p1, v0, v1}, LX/5Pc;->a(II)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->d:LX/5Pb;

    .line 1076010
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->a:Lcom/facebook/videocodec/effects/model/TextData;

    .line 1076011
    iget-object v1, v0, Lcom/facebook/videocodec/effects/model/TextData;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1076012
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1076013
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->c:LX/7Sq;

    iget-object v1, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->a:Lcom/facebook/videocodec/effects/model/TextData;

    .line 1076014
    iget v2, v1, Lcom/facebook/videocodec/effects/model/TextData;->b:I

    move v1, v2

    .line 1076015
    const/4 v3, 0x0

    .line 1076016
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-static {v0, v2, v1, v3, v3}, LX/7Sq;->a(LX/7Sq;Landroid/graphics/Typeface;III)Z

    .line 1076017
    :goto_0
    return-void

    .line 1076018
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->c:LX/7Sq;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->a:Lcom/facebook/videocodec/effects/model/TextData;

    .line 1076019
    iget-object p1, v2, Lcom/facebook/videocodec/effects/model/TextData;->a:Ljava/lang/String;

    move-object v2, p1

    .line 1076020
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->a:Lcom/facebook/videocodec/effects/model/TextData;

    .line 1076021
    iget p0, v2, Lcom/facebook/videocodec/effects/model/TextData;->b:I

    move v2, p0

    .line 1076022
    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/io/File;)Landroid/graphics/Typeface;

    move-result-object p0

    .line 1076023
    invoke-static {v0, p0, v2, v3, v3}, LX/7Sq;->a(LX/7Sq;Landroid/graphics/Typeface;III)Z

    .line 1076024
    goto :goto_0
.end method

.method public a([F[F[FJ)V
    .locals 7

    .prologue
    .line 1076025
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->a:Lcom/facebook/videocodec/effects/model/TextData;

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/TextData;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1076026
    :goto_0
    return-void

    .line 1076027
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->d:LX/5Pb;

    invoke-virtual {v0}, LX/5Pb;->a()LX/5Pa;

    move-result-object v0

    .line 1076028
    const-string v1, "uSceneMatrix"

    iget-object v2, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->e:[F

    invoke-virtual {v0, v1, v2}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    .line 1076029
    iget-object v1, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->c:LX/7Sq;

    invoke-virtual {v1, v0}, LX/7Sq;->a(LX/5Pa;)V

    .line 1076030
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->a:Lcom/facebook/videocodec/effects/model/TextData;

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/TextData;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/model/TextData$Text;

    .line 1076031
    iget-object v4, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->c:LX/7Sq;

    iget-object v5, v0, Lcom/facebook/videocodec/effects/model/TextData$Text;->a:Ljava/lang/String;

    iget v6, v0, Lcom/facebook/videocodec/effects/model/TextData$Text;->b:F

    iget v0, v0, Lcom/facebook/videocodec/effects/model/TextData$Text;->c:F

    invoke-virtual {v4, v5, v6, v0}, LX/7Sq;->a(Ljava/lang/String;FF)V

    .line 1076032
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1076033
    :cond_1
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->c:LX/7Sq;

    .line 1076034
    iget-object v1, v0, LX/7Sq;->a:LX/7Sp;

    iget-object v2, v0, LX/7Sq;->b:LX/5Pa;

    invoke-virtual {v1, v2}, LX/7Sp;->a(LX/5Pa;)V

    .line 1076035
    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1076036
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 1076037
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/TextRenderer;->a:Lcom/facebook/videocodec/effects/model/TextData;

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/TextData;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
