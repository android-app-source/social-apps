.class public final Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;",
            ">;"
        }
    .end annotation
.end field

.field public b:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1039824
    new-instance v0, LX/61L;

    invoke-direct {v0}, LX/61L;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1039825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039826
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a:Ljava/util/List;

    .line 1039827
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->b:F

    .line 1039828
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1039829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039830
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a:Ljava/util/List;

    .line 1039831
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1039832
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->b:F

    .line 1039833
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 1039834
    iget v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1039835
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1039836
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1039837
    iget v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1039838
    return-void
.end method
