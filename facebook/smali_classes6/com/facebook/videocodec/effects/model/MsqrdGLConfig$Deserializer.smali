.class public final Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1039901
    new-instance v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Deserializer;->a:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1039902
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1039903
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;
    .locals 1

    .prologue
    .line 1039904
    sget-object v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Deserializer;->a:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;

    .line 1039905
    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->a()Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1039906
    invoke-static {p1, p2}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    move-result-object v0

    return-object v0
.end method
