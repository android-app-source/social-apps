.class public Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1040205
    const-class v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    new-instance v1, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1040206
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1040198
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1040199
    if-nez p0, :cond_0

    .line 1040200
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1040201
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1040202
    invoke-static {p0, p1, p2}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfigSerializer;->b(Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;LX/0nX;LX/0my;)V

    .line 1040203
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1040204
    return-void
.end method

.method private static b(Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1040194
    const-string v0, "asset_path"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->assetPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1040195
    const-string v0, "render_key"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->renderKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1040196
    const-string v0, "shader_filter_model"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->getShaderFilterModel()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1040197
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1040193
    check-cast p1, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfigSerializer;->a(Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;LX/0nX;LX/0my;)V

    return-void
.end method
