.class public Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/videocodec/effects/model/StyleTransferGLConfigSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1040321
    const-class v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1040320
    const-class v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfigSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1040319
    new-instance v0, LX/61Y;

    invoke-direct {v0}, LX/61Y;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1040309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040310
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1040311
    iput-object v1, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->a:Ljava/lang/String;

    .line 1040312
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1040313
    iput-object v1, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->b:Ljava/lang/String;

    .line 1040314
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->c:Ljava/lang/String;

    .line 1040315
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    .line 1040316
    return-void

    .line 1040317
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->a:Ljava/lang/String;

    goto :goto_0

    .line 1040318
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->b:Ljava/lang/String;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;)V
    .locals 1

    .prologue
    .line 1040303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040304
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->a:Ljava/lang/String;

    .line 1040305
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->b:Ljava/lang/String;

    .line 1040306
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->c:Ljava/lang/String;

    .line 1040307
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    .line 1040308
    return-void
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;)Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;
    .locals 2

    .prologue
    .line 1040302
    new-instance v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;-><init>(Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1040322
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1040289
    if-ne p0, p1, :cond_1

    .line 1040290
    :cond_0
    :goto_0
    return v0

    .line 1040291
    :cond_1
    instance-of v2, p1, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    if-nez v2, :cond_2

    move v0, v1

    .line 1040292
    goto :goto_0

    .line 1040293
    :cond_2
    check-cast p1, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    .line 1040294
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1040295
    goto :goto_0

    .line 1040296
    :cond_3
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1040297
    goto :goto_0

    .line 1040298
    :cond_4
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1040299
    goto :goto_0

    .line 1040300
    :cond_5
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1040301
    goto :goto_0
.end method

.method public getStyleTransferModel()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "style_transfer_model"
    .end annotation

    .prologue
    .line 1040288
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1040287
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public initPredictPath()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "init_predict_path"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1040286
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->a:Ljava/lang/String;

    return-object v0
.end method

.method public initResPath()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "init_res_path"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1040273
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->b:Ljava/lang/String;

    return-object v0
.end method

.method public renderKey()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "render_key"
    .end annotation

    .prologue
    .line 1040285
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1040274
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1040275
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1040276
    :goto_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1040277
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1040278
    :goto_1
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1040279
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1040280
    return-void

    .line 1040281
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1040282
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 1040283
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1040284
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1
.end method
