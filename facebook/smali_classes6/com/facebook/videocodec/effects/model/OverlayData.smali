.class public Lcom/facebook/videocodec/effects/model/OverlayData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/videocodec/effects/model/OverlayData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Landroid/graphics/Bitmap;

.field public c:[F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1040027
    new-instance v0, LX/61Q;

    invoke-direct {v0}, LX/61Q;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/OverlayData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;[F)V
    .locals 0

    .prologue
    .line 1040023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040024
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/OverlayData;->a:Landroid/net/Uri;

    .line 1040025
    iput-object p2, p0, Lcom/facebook/videocodec/effects/model/OverlayData;->c:[F

    .line 1040026
    return-void
.end method


# virtual methods
.method public final c()Z
    .locals 1

    .prologue
    .line 1040017
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/OverlayData;->b:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/OverlayData;->a:Landroid/net/Uri;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1040022
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1040018
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/OverlayData;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1040019
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/OverlayData;->b:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1040020
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/OverlayData;->c:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloatArray([F)V

    .line 1040021
    return-void
.end method
