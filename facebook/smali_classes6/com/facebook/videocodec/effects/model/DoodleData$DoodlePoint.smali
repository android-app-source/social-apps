.class public final Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:F

.field public final b:F

.field public final c:I

.field public final d:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1039860
    new-instance v0, LX/61M;

    invoke-direct {v0}, LX/61M;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(FFIF)V
    .locals 0

    .prologue
    .line 1039854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039855
    iput p1, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->a:F

    .line 1039856
    iput p2, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->b:F

    .line 1039857
    iput p3, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->c:I

    .line 1039858
    iput p4, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->d:F

    .line 1039859
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1039848
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039849
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->a:F

    .line 1039850
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->b:F

    .line 1039851
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->c:I

    .line 1039852
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->d:F

    .line 1039853
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1039842
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1039843
    iget v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1039844
    iget v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1039845
    iget v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039846
    iget v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodlePoint;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1039847
    return-void
.end method
