.class public final Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;


# instance fields
.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public final mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "style_transfer_model"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1040245
    const-class v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1040246
    new-instance v0, LX/61Z;

    invoke-direct {v0}, LX/61Z;-><init>()V

    .line 1040247
    const-string v0, "StyleTransfer"

    move-object v0, v0

    .line 1040248
    sput-object v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->a:Ljava/lang/String;

    .line 1040249
    new-instance v0, LX/61a;

    invoke-direct {v0}, LX/61a;-><init>()V

    .line 1040250
    const/4 v0, 0x0

    move-object v0, v0

    .line 1040251
    sput-object v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->b:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1040252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040253
    sget-object v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->e:Ljava/lang/String;

    .line 1040254
    sget-object v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->b:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    .line 1040255
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;)V
    .locals 1

    .prologue
    .line 1040256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040257
    sget-object v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->e:Ljava/lang/String;

    .line 1040258
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    .line 1040259
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;
    .locals 2

    .prologue
    .line 1040260
    new-instance v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    invoke-direct {v0, p0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;-><init>(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;)V

    return-object v0
.end method

.method public setInitPredictPath(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "init_predict_path"
    .end annotation

    .prologue
    .line 1040261
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->c:Ljava/lang/String;

    .line 1040262
    return-object p0
.end method

.method public setInitResPath(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "init_res_path"
    .end annotation

    .prologue
    .line 1040263
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->d:Ljava/lang/String;

    .line 1040264
    return-object p0
.end method

.method public setRenderKey(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "render_key"
    .end annotation

    .prologue
    .line 1040265
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->e:Ljava/lang/String;

    .line 1040266
    return-object p0
.end method
