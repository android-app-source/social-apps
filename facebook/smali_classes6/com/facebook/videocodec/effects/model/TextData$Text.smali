.class public final Lcom/facebook/videocodec/effects/model/TextData$Text;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:Ljava/lang/String;

.field public b:F

.field public c:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1040370
    new-instance v0, LX/61c;

    invoke-direct {v0}, LX/61c;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/TextData$Text;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1040371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040372
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/TextData$Text;->a:Ljava/lang/String;

    .line 1040373
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/effects/model/TextData$Text;->b:F

    .line 1040374
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/effects/model/TextData$Text;->c:F

    .line 1040375
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;FF)V
    .locals 2

    .prologue
    .line 1040376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040377
    sget-object v0, LX/1IA;->ASCII:LX/1IA;

    invoke-virtual {v0, p1}, LX/1IA;->matchesAllOf(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "Text string must only use ASCII"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1040378
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/TextData$Text;->a:Ljava/lang/String;

    .line 1040379
    iput p2, p0, Lcom/facebook/videocodec/effects/model/TextData$Text;->b:F

    .line 1040380
    iput p3, p0, Lcom/facebook/videocodec/effects/model/TextData$Text;->c:F

    .line 1040381
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1040382
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1040383
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/TextData$Text;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1040384
    iget v0, p0, Lcom/facebook/videocodec/effects/model/TextData$Text;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1040385
    iget v0, p0, Lcom/facebook/videocodec/effects/model/TextData$Text;->c:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1040386
    return-void
.end method
