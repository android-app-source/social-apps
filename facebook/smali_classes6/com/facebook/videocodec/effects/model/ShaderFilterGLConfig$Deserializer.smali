.class public final Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1040148
    new-instance v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Deserializer;->a:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1040143
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1040144
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;
    .locals 1

    .prologue
    .line 1040146
    sget-object v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Deserializer;->a:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;

    .line 1040147
    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->a()Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1040145
    invoke-static {p1, p2}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    move-result-object v0

    return-object v0
.end method
