.class public Lcom/facebook/videocodec/effects/model/ColorFilter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/videocodec/effects/model/ColorFilter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:F

.field public c:F

.field public d:F

.field public e:F

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1039817
    new-instance v0, LX/61J;

    invoke-direct {v0}, LX/61J;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/ColorFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1039815
    const-string v1, "default"

    const/4 v6, 0x0

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/facebook/videocodec/effects/model/ColorFilter;-><init>(Ljava/lang/String;FFFFZ)V

    .line 1039816
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;FFFFZ)V
    .locals 0

    .prologue
    .line 1039794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039795
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/ColorFilter;->a:Ljava/lang/String;

    .line 1039796
    iput p2, p0, Lcom/facebook/videocodec/effects/model/ColorFilter;->b:F

    .line 1039797
    iput p3, p0, Lcom/facebook/videocodec/effects/model/ColorFilter;->c:F

    .line 1039798
    iput p4, p0, Lcom/facebook/videocodec/effects/model/ColorFilter;->d:F

    .line 1039799
    iput p5, p0, Lcom/facebook/videocodec/effects/model/ColorFilter;->e:F

    .line 1039800
    iput-boolean p6, p0, Lcom/facebook/videocodec/effects/model/ColorFilter;->f:Z

    .line 1039801
    return-void
.end method

.method public static g()Lcom/facebook/videocodec/effects/model/ColorFilter;
    .locals 7

    .prologue
    .line 1039814
    new-instance v0, Lcom/facebook/videocodec/effects/model/ColorFilter;

    const-string v1, "Pop"

    const v2, 0x3f028f5c    # 0.51f

    const v3, 0x3d8f5c29    # 0.07f

    const v4, 0x3ea3d70a    # 0.32f

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/videocodec/effects/model/ColorFilter;-><init>(Ljava/lang/String;FFFFZ)V

    return-object v0
.end method

.method public static h()Lcom/facebook/videocodec/effects/model/ColorFilter;
    .locals 7

    .prologue
    .line 1039813
    new-instance v0, Lcom/facebook/videocodec/effects/model/ColorFilter;

    const-string v1, "Classical"

    const/high16 v2, -0x40800000    # -1.0f

    const v3, 0x3e99999a    # 0.3f

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/videocodec/effects/model/ColorFilter;-><init>(Ljava/lang/String;FFFFZ)V

    return-object v0
.end method

.method public static i()Lcom/facebook/videocodec/effects/model/ColorFilter;
    .locals 7

    .prologue
    const v2, 0x3de147ae    # 0.11f

    .line 1039812
    new-instance v0, Lcom/facebook/videocodec/effects/model/ColorFilter;

    const-string v1, "Country"

    const v4, -0x41333333    # -0.4f

    const v5, 0x3da3d70a    # 0.08f

    const/4 v6, 0x1

    move v3, v2

    invoke-direct/range {v0 .. v6}, Lcom/facebook/videocodec/effects/model/ColorFilter;-><init>(Ljava/lang/String;FFFFZ)V

    return-object v0
.end method

.method public static j()Lcom/facebook/videocodec/effects/model/ColorFilter;
    .locals 7

    .prologue
    .line 1039811
    new-instance v0, Lcom/facebook/videocodec/effects/model/ColorFilter;

    const-string v1, "Funk"

    const v2, 0x3e051eb8    # 0.13f

    const/4 v3, 0x0

    const v4, 0x3e8a3d71    # 0.27f

    const v5, -0x42333333    # -0.1f

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/videocodec/effects/model/ColorFilter;-><init>(Ljava/lang/String;FFFFZ)V

    return-object v0
.end method

.method public static k()Lcom/facebook/videocodec/effects/model/ColorFilter;
    .locals 7

    .prologue
    .line 1039810
    new-instance v0, Lcom/facebook/videocodec/effects/model/ColorFilter;

    const-string v1, "Acid"

    const v2, 0x3e051eb8    # 0.13f

    const/4 v3, 0x0

    const v4, -0x415c28f6    # -0.32f

    const v5, 0x3e947ae1    # 0.29f

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/videocodec/effects/model/ColorFilter;-><init>(Ljava/lang/String;FFFFZ)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1039809
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1039802
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/ColorFilter;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1039803
    iget v0, p0, Lcom/facebook/videocodec/effects/model/ColorFilter;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1039804
    iget v0, p0, Lcom/facebook/videocodec/effects/model/ColorFilter;->c:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1039805
    iget v0, p0, Lcom/facebook/videocodec/effects/model/ColorFilter;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1039806
    iget v0, p0, Lcom/facebook/videocodec/effects/model/ColorFilter;->e:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1039807
    iget-boolean v0, p0, Lcom/facebook/videocodec/effects/model/ColorFilter;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1039808
    return-void
.end method
