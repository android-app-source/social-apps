.class public Lcom/facebook/videocodec/effects/model/MsqrdGLConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1039976
    const-class v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    new-instance v1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1039977
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1039975
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1039978
    if-nez p0, :cond_0

    .line 1039979
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1039980
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1039981
    invoke-static {p0, p1, p2}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfigSerializer;->b(Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;LX/0nX;LX/0my;)V

    .line 1039982
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1039983
    return-void
.end method

.method private static b(Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1039969
    const-string v0, "attribution_text"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->getAttributionText()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1039970
    const-string v0, "attribution_thumbnail"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->getAttributionThumbnail()Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1039971
    const-string v0, "instruction_text"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->getInstructionText()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1039972
    const-string v0, "mask_model"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->getMaskModel()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1039973
    const-string v0, "render_key"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->renderKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1039974
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1039968
    check-cast p1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfigSerializer;->a(Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;LX/0nX;LX/0my;)V

    return-void
.end method
