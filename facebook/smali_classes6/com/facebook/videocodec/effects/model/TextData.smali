.class public Lcom/facebook/videocodec/effects/model/TextData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/videocodec/effects/model/TextData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/videocodec/effects/model/TextData$Text;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1040404
    new-instance v0, LX/61b;

    invoke-direct {v0}, LX/61b;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/TextData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1040387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040388
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/TextData;->a:Ljava/lang/String;

    .line 1040389
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/effects/model/TextData;->b:I

    .line 1040390
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/TextData;->c:Ljava/util/List;

    .line 1040391
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/TextData;->c:Ljava/util/List;

    sget-object v1, Lcom/facebook/videocodec/effects/model/TextData$Text;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1040392
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/videocodec/effects/model/TextData$Text;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1040399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040400
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/TextData;->a:Ljava/lang/String;

    .line 1040401
    iput p2, p0, Lcom/facebook/videocodec/effects/model/TextData;->b:I

    .line 1040402
    iput-object p3, p0, Lcom/facebook/videocodec/effects/model/TextData;->c:Ljava/util/List;

    .line 1040403
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/videocodec/effects/model/TextData$Text;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1040398
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/TextData;->c:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1040397
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1040393
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/TextData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1040394
    iget v0, p0, Lcom/facebook/videocodec/effects/model/TextData;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1040395
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/TextData;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1040396
    return-void
.end method
