.class public Lcom/facebook/videocodec/effects/model/DoodleData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/videocodec/effects/model/DoodleData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1039861
    new-instance v0, LX/61K;

    invoke-direct {v0}, LX/61K;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/DoodleData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1039862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039863
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    .line 1039864
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1039865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039866
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    .line 1039867
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1039868
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData;->b:Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    .line 1039869
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1039870
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1039871
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1039872
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/DoodleData;->b:Lcom/facebook/videocodec/effects/model/DoodleData$DoodleLine;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1039873
    return-void
.end method
