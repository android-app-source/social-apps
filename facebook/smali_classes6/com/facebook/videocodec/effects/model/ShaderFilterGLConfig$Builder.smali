.class public final Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;


# instance fields
.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public final mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shader_filter_model"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1040123
    const-class v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1040124
    new-instance v0, LX/61V;

    invoke-direct {v0}, LX/61V;-><init>()V

    .line 1040125
    const-string v0, "ShaderFilter"

    move-object v0, v0

    .line 1040126
    sput-object v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->a:Ljava/lang/String;

    .line 1040127
    new-instance v0, LX/61W;

    invoke-direct {v0}, LX/61W;-><init>()V

    .line 1040128
    const/4 v0, 0x0

    move-object v0, v0

    .line 1040129
    sput-object v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->b:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1040130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040131
    sget-object v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->d:Ljava/lang/String;

    .line 1040132
    sget-object v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->b:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    .line 1040133
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;)V
    .locals 1

    .prologue
    .line 1040134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040135
    sget-object v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->d:Ljava/lang/String;

    .line 1040136
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    .line 1040137
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;
    .locals 2

    .prologue
    .line 1040138
    new-instance v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    invoke-direct {v0, p0}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;-><init>(Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;)V

    return-object v0
.end method

.method public setAssetPath(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "asset_path"
    .end annotation

    .prologue
    .line 1040139
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->c:Ljava/lang/String;

    .line 1040140
    return-object p0
.end method

.method public setRenderKey(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "render_key"
    .end annotation

    .prologue
    .line 1040141
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->d:Ljava/lang/String;

    .line 1040142
    return-object p0
.end method
