.class public Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfigSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1040192
    const-class v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1040149
    const-class v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfigSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1040191
    new-instance v0, LX/61U;

    invoke-direct {v0}, LX/61U;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1040182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040183
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1040184
    iput-object v1, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->a:Ljava/lang/String;

    .line 1040185
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->b:Ljava/lang/String;

    .line 1040186
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1040187
    iput-object v1, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->c:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    .line 1040188
    :goto_1
    return-void

    .line 1040189
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->a:Ljava/lang/String;

    goto :goto_0

    .line 1040190
    :cond_1
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->c:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;)V
    .locals 1

    .prologue
    .line 1040177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040178
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->a:Ljava/lang/String;

    .line 1040179
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->b:Ljava/lang/String;

    .line 1040180
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->c:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    .line 1040181
    return-void
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;)Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;
    .locals 2

    .prologue
    .line 1040176
    new-instance v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;-><init>(Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;)V

    return-object v0
.end method


# virtual methods
.method public assetPath()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "asset_path"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1040175
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1040174
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1040163
    if-ne p0, p1, :cond_1

    .line 1040164
    :cond_0
    :goto_0
    return v0

    .line 1040165
    :cond_1
    instance-of v2, p1, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    if-nez v2, :cond_2

    move v0, v1

    .line 1040166
    goto :goto_0

    .line 1040167
    :cond_2
    check-cast p1, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    .line 1040168
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1040169
    goto :goto_0

    .line 1040170
    :cond_3
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1040171
    goto :goto_0

    .line 1040172
    :cond_4
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->c:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->c:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1040173
    goto :goto_0
.end method

.method public getShaderFilterModel()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shader_filter_model"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1040162
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->c:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1040161
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->c:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public renderKey()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "render_key"
    .end annotation

    .prologue
    .line 1040160
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1040150
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1040151
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1040152
    :goto_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1040153
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->c:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    if-nez v0, :cond_1

    .line 1040154
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1040155
    :goto_1
    return-void

    .line 1040156
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1040157
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 1040158
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1040159
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->c:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto :goto_1
.end method
