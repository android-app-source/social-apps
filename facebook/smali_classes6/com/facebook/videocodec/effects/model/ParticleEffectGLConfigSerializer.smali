.class public Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1040094
    const-class v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    new-instance v1, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1040095
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1040093
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1040083
    if-nez p0, :cond_0

    .line 1040084
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1040085
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1040086
    invoke-static {p0, p1, p2}, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfigSerializer;->b(Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;LX/0nX;LX/0my;)V

    .line 1040087
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1040088
    return-void
.end method

.method private static b(Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1040090
    const-string v0, "particle_effect_model"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->getParticleEffectModel()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1040091
    const-string v0, "render_key"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->renderKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1040092
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1040089
    check-cast p1, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfigSerializer;->a(Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;LX/0nX;LX/0my;)V

    return-void
.end method
