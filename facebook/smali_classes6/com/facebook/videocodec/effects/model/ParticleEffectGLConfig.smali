.class public Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfigSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

.field private final b:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1040081
    const-class v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1040055
    const-class v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfigSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1040080
    new-instance v0, LX/61R;

    invoke-direct {v0}, LX/61R;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1040076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040077
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    .line 1040078
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->b:Ljava/lang/String;

    .line 1040079
    return-void
.end method

.method public constructor <init>(Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;)V
    .locals 1

    .prologue
    .line 1040072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040073
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    .line 1040074
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->b:Ljava/lang/String;

    .line 1040075
    return-void
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;)Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;
    .locals 2

    .prologue
    .line 1040071
    new-instance v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;-><init>(Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1040082
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1040062
    if-ne p0, p1, :cond_1

    .line 1040063
    :cond_0
    :goto_0
    return v0

    .line 1040064
    :cond_1
    instance-of v2, p1, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    if-nez v2, :cond_2

    move v0, v1

    .line 1040065
    goto :goto_0

    .line 1040066
    :cond_2
    check-cast p1, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    .line 1040067
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1040068
    goto :goto_0

    .line 1040069
    :cond_3
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1040070
    goto :goto_0
.end method

.method public getParticleEffectModel()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "particle_effect_model"
    .end annotation

    .prologue
    .line 1040061
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1040060
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public renderKey()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "render_key"
    .end annotation

    .prologue
    .line 1040059
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1040056
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1040057
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1040058
    return-void
.end method
