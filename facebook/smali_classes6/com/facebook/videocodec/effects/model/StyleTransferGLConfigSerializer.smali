.class public Lcom/facebook/videocodec/effects/model/StyleTransferGLConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1040336
    const-class v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    new-instance v1, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1040337
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1040335
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1040323
    if-nez p0, :cond_0

    .line 1040324
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1040325
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1040326
    invoke-static {p0, p1, p2}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfigSerializer;->b(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;LX/0nX;LX/0my;)V

    .line 1040327
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1040328
    return-void
.end method

.method private static b(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1040330
    const-string v0, "init_predict_path"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->initPredictPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1040331
    const-string v0, "init_res_path"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->initResPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1040332
    const-string v0, "render_key"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->renderKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1040333
    const-string v0, "style_transfer_model"

    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->getStyleTransferModel()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1040334
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1040329
    check-cast p1, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfigSerializer;->a(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;LX/0nX;LX/0my;)V

    return-void
.end method
