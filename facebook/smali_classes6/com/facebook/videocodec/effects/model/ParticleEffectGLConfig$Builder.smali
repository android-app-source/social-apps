.class public final Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

.field private static final b:Ljava/lang/String;


# instance fields
.field public c:Ljava/lang/String;

.field public final mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "particle_effect_model"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1040031
    const-class v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1040032
    new-instance v0, LX/61S;

    invoke-direct {v0}, LX/61S;-><init>()V

    .line 1040033
    const/4 v0, 0x0

    move-object v0, v0

    .line 1040034
    sput-object v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    .line 1040035
    new-instance v0, LX/61T;

    invoke-direct {v0}, LX/61T;-><init>()V

    .line 1040036
    const-string v0, "ParticleEffect"

    move-object v0, v0

    .line 1040037
    sput-object v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1040038
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040039
    sget-object v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    .line 1040040
    sget-object v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;->c:Ljava/lang/String;

    .line 1040041
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;)V
    .locals 1

    .prologue
    .line 1040042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040043
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    .line 1040044
    sget-object v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;->c:Ljava/lang/String;

    .line 1040045
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;
    .locals 2

    .prologue
    .line 1040046
    new-instance v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    invoke-direct {v0, p0}, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;-><init>(Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;)V

    return-object v0
.end method

.method public setRenderKey(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "render_key"
    .end annotation

    .prologue
    .line 1040047
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;->c:Ljava/lang/String;

    .line 1040048
    return-object p0
.end method
