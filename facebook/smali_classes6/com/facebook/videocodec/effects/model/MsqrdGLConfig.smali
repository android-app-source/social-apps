.class public Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/videocodec/effects/model/MsqrdGLConfigSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

.field private final e:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1039967
    const-class v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1039966
    const-class v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfigSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1039965
    new-instance v0, LX/61N;

    invoke-direct {v0}, LX/61N;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1039952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039953
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1039954
    iput-object v1, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->a:Ljava/lang/String;

    .line 1039955
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1039956
    iput-object v1, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->b:Landroid/net/Uri;

    .line 1039957
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1039958
    iput-object v1, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->c:Ljava/lang/String;

    .line 1039959
    :goto_2
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    .line 1039960
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->e:Ljava/lang/String;

    .line 1039961
    return-void

    .line 1039962
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->a:Ljava/lang/String;

    goto :goto_0

    .line 1039963
    :cond_1
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->b:Landroid/net/Uri;

    goto :goto_1

    .line 1039964
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->c:Ljava/lang/String;

    goto :goto_2
.end method

.method public constructor <init>(Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;)V
    .locals 1

    .prologue
    .line 1039945
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039946
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->a:Ljava/lang/String;

    .line 1039947
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->d:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->b:Landroid/net/Uri;

    .line 1039948
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->c:Ljava/lang/String;

    .line 1039949
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->mMaskModel:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    .line 1039950
    iget-object v0, p1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->e:Ljava/lang/String;

    .line 1039951
    return-void
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;)Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;
    .locals 2

    .prologue
    .line 1039944
    new-instance v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;-><init>(Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1039907
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1039929
    if-ne p0, p1, :cond_1

    .line 1039930
    :cond_0
    :goto_0
    return v0

    .line 1039931
    :cond_1
    instance-of v2, p1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    if-nez v2, :cond_2

    move v0, v1

    .line 1039932
    goto :goto_0

    .line 1039933
    :cond_2
    check-cast p1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    .line 1039934
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1039935
    goto :goto_0

    .line 1039936
    :cond_3
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->b:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->b:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1039937
    goto :goto_0

    .line 1039938
    :cond_4
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1039939
    goto :goto_0

    .line 1039940
    :cond_5
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1039941
    goto :goto_0

    .line 1039942
    :cond_6
    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1039943
    goto :goto_0
.end method

.method public getAttributionText()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attribution_text"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1039928
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getAttributionThumbnail()Landroid/net/Uri;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attribution_thumbnail"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1039927
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public getInstructionText()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "instruction_text"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1039926
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getMaskModel()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mask_model"
    .end annotation

    .prologue
    .line 1039925
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1039924
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->b:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public renderKey()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "render_key"
    .end annotation

    .prologue
    .line 1039923
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1039908
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1039909
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039910
    :goto_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->b:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 1039911
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039912
    :goto_1
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1039913
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039914
    :goto_2
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->d:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1039915
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1039916
    return-void

    .line 1039917
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039918
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 1039919
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039920
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_1

    .line 1039921
    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039922
    iget-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2
.end method
