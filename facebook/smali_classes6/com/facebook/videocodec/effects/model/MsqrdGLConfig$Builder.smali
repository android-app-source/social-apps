.class public final Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/videocodec/effects/model/MsqrdGLConfig_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

.field private static final b:Ljava/lang/String;


# instance fields
.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;

.field public final mMaskModel:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mask_model"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1039896
    const-class v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1039879
    new-instance v0, LX/61O;

    invoke-direct {v0}, LX/61O;-><init>()V

    .line 1039880
    const/4 v0, 0x0

    move-object v0, v0

    .line 1039881
    sput-object v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->a:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    .line 1039882
    new-instance v0, LX/61P;

    invoke-direct {v0}, LX/61P;-><init>()V

    .line 1039883
    const-string v0, "Msqrd"

    move-object v0, v0

    .line 1039884
    sput-object v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1039897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039898
    sget-object v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->a:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->mMaskModel:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    .line 1039899
    sget-object v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->f:Ljava/lang/String;

    .line 1039900
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;)V
    .locals 1

    .prologue
    .line 1039891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039892
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->mMaskModel:Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    .line 1039893
    sget-object v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->f:Ljava/lang/String;

    .line 1039894
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;
    .locals 2

    .prologue
    .line 1039895
    new-instance v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    invoke-direct {v0, p0}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;-><init>(Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;)V

    return-object v0
.end method

.method public setAttributionText(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attribution_text"
    .end annotation

    .prologue
    .line 1039889
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->c:Ljava/lang/String;

    .line 1039890
    return-object p0
.end method

.method public setAttributionThumbnail(Landroid/net/Uri;)Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attribution_thumbnail"
    .end annotation

    .prologue
    .line 1039887
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->d:Landroid/net/Uri;

    .line 1039888
    return-object p0
.end method

.method public setInstructionText(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "instruction_text"
    .end annotation

    .prologue
    .line 1039885
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->e:Ljava/lang/String;

    .line 1039886
    return-object p0
.end method

.method public setRenderKey(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "render_key"
    .end annotation

    .prologue
    .line 1039877
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->f:Ljava/lang/String;

    .line 1039878
    return-object p0
.end method
