.class public Lcom/facebook/videocodec/effects/model/SolidColorData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/videocodec/effects/model/SolidColorData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1040235
    new-instance v0, LX/61X;

    invoke-direct {v0}, LX/61X;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/effects/model/SolidColorData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 1040236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040237
    iput p1, p0, Lcom/facebook/videocodec/effects/model/SolidColorData;->a:I

    .line 1040238
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1040239
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1040240
    iget v0, p0, Lcom/facebook/videocodec/effects/model/SolidColorData;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1040241
    return-void
.end method
