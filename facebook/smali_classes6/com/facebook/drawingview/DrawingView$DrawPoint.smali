.class public Lcom/facebook/drawingview/DrawingView$DrawPoint;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/drawingview/DrawingView$DrawPoint;",
            ">;"
        }
    .end annotation
.end field

.field public static c:Landroid/graphics/Path;


# instance fields
.field public d:F

.field public e:F

.field public f:F

.field public g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 906468
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    sput-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->c:Landroid/graphics/Path;

    .line 906469
    new-instance v0, LX/5Nb;

    invoke-direct {v0}, LX/5Nb;-><init>()V

    sput-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(FFFI)V
    .locals 0

    .prologue
    .line 906462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 906463
    iput p1, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->d:F

    .line 906464
    iput p2, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->e:F

    .line 906465
    iput p3, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    .line 906466
    iput p4, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->g:I

    .line 906467
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 906456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 906457
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->d:F

    .line 906458
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->e:F

    .line 906459
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    .line 906460
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->g:I

    .line 906461
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 906441
    invoke-direct {p0, p1}, Lcom/facebook/drawingview/DrawingView$DrawPoint;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Paint;Landroid/graphics/Canvas;Landroid/view/View;)V
    .locals 5

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    .line 906448
    sget-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->c:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 906449
    sget-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->c:Landroid/graphics/Path;

    iget v1, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->d:F

    iget v2, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->e:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 906450
    sget-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->c:Landroid/graphics/Path;

    iget v1, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->d:F

    add-float/2addr v1, v3

    iget v2, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->e:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 906451
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->g:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 906452
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 906453
    sget-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->c:Landroid/graphics/Path;

    invoke-virtual {p2, v0, p1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 906454
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->d:F

    iget v1, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->e:F

    iget v2, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->d:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->e:F

    iget v4, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    add-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {p3, v0, v1, v2, v3}, Landroid/view/View;->invalidate(IIII)V

    .line 906455
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 906447
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 906442
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 906443
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->e:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 906444
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 906445
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 906446
    return-void
.end method
