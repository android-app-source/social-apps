.class public final Lcom/facebook/drawingview/DrawingView$DrawQuad;
.super Lcom/facebook/drawingview/DrawingView$DrawLine;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/drawingview/DrawingView$DrawQuad;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public h:F

.field public i:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 906507
    new-instance v0, LX/5Nc;

    invoke-direct {v0}, LX/5Nc;-><init>()V

    sput-object v0, Lcom/facebook/drawingview/DrawingView$DrawQuad;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(FFFFFFFI)V
    .locals 8

    .prologue
    .line 906508
    const/4 v7, 0x0

    move-object v0, p0

    move v1, p3

    move v2, p4

    move v3, p5

    move v4, p6

    move v5, p7

    move/from16 v6, p8

    invoke-direct/range {v0 .. v7}, Lcom/facebook/drawingview/DrawingView$DrawLine;-><init>(FFFFFIB)V

    .line 906509
    iput p1, p0, Lcom/facebook/drawingview/DrawingView$DrawQuad;->h:F

    .line 906510
    iput p2, p0, Lcom/facebook/drawingview/DrawingView$DrawQuad;->i:F

    .line 906511
    return-void
.end method

.method public synthetic constructor <init>(FFFFFFFIB)V
    .locals 0

    .prologue
    .line 906512
    invoke-direct/range {p0 .. p8}, Lcom/facebook/drawingview/DrawingView$DrawQuad;-><init>(FFFFFFFI)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 906513
    invoke-direct {p0, p1}, Lcom/facebook/drawingview/DrawingView$DrawLine;-><init>(Landroid/os/Parcel;)V

    .line 906514
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/drawingview/DrawingView$DrawQuad;->h:F

    .line 906515
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/drawingview/DrawingView$DrawQuad;->i:F

    .line 906516
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 906517
    invoke-direct {p0, p1}, Lcom/facebook/drawingview/DrawingView$DrawQuad;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Paint;Landroid/graphics/Canvas;Landroid/view/View;)V
    .locals 5

    .prologue
    .line 906518
    sget-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->c:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 906519
    sget-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->c:Landroid/graphics/Path;

    iget v1, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->a:F

    iget v2, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->b:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 906520
    sget-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->c:Landroid/graphics/Path;

    iget v1, p0, Lcom/facebook/drawingview/DrawingView$DrawQuad;->h:F

    iget v2, p0, Lcom/facebook/drawingview/DrawingView$DrawQuad;->i:F

    iget v3, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->d:F

    iget v4, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->e:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 906521
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->g:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 906522
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 906523
    sget-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->c:Landroid/graphics/Path;

    invoke-virtual {p2, v0, p1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 906524
    invoke-virtual {p0, p3}, Lcom/facebook/drawingview/DrawingView$DrawLine;->a(Landroid/view/View;)V

    .line 906525
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 906526
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 906527
    invoke-super {p0, p1, p2}, Lcom/facebook/drawingview/DrawingView$DrawLine;->writeToParcel(Landroid/os/Parcel;I)V

    .line 906528
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawQuad;->h:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 906529
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawQuad;->i:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 906530
    return-void
.end method
