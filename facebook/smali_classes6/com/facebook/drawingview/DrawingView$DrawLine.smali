.class public Lcom/facebook/drawingview/DrawingView$DrawLine;
.super Lcom/facebook/drawingview/DrawingView$DrawPoint;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/drawingview/DrawingView$DrawLine;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:F

.field public b:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 906496
    new-instance v0, LX/5Na;

    invoke-direct {v0}, LX/5Na;-><init>()V

    sput-object v0, Lcom/facebook/drawingview/DrawingView$DrawLine;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(FFFFFI)V
    .locals 0

    .prologue
    .line 906470
    invoke-direct {p0, p1, p2, p5, p6}, Lcom/facebook/drawingview/DrawingView$DrawPoint;-><init>(FFFI)V

    .line 906471
    iput p3, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->a:F

    .line 906472
    iput p4, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->b:F

    .line 906473
    return-void
.end method

.method public synthetic constructor <init>(FFFFFIB)V
    .locals 0

    .prologue
    .line 906495
    invoke-direct/range {p0 .. p6}, Lcom/facebook/drawingview/DrawingView$DrawLine;-><init>(FFFFFI)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 906491
    invoke-direct {p0, p1}, Lcom/facebook/drawingview/DrawingView$DrawPoint;-><init>(Landroid/os/Parcel;)V

    .line 906492
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->a:F

    .line 906493
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->b:F

    .line 906494
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 906490
    invoke-direct {p0, p1}, Lcom/facebook/drawingview/DrawingView$DrawLine;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Paint;Landroid/graphics/Canvas;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 906482
    sget-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->c:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 906483
    sget-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->c:Landroid/graphics/Path;

    iget v1, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->a:F

    iget v2, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->b:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 906484
    sget-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->c:Landroid/graphics/Path;

    iget v1, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->d:F

    iget v2, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->e:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 906485
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->g:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 906486
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 906487
    sget-object v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->c:Landroid/graphics/Path;

    invoke-virtual {p2, v0, p1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 906488
    invoke-virtual {p0, p3}, Lcom/facebook/drawingview/DrawingView$DrawLine;->a(Landroid/view/View;)V

    .line 906489
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 906479
    if-eqz p1, :cond_0

    .line 906480
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->a:F

    iget v1, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->d:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget v1, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->b:F

    iget v2, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->e:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget v2, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->a:F

    iget v3, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->d:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget v3, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->b:F

    iget v4, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->e:F

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iget v4, p0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    add-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->invalidate(IIII)V

    .line 906481
    :cond_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 906478
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 906474
    invoke-super {p0, p1, p2}, Lcom/facebook/drawingview/DrawingView$DrawPoint;->writeToParcel(Landroid/os/Parcel;I)V

    .line 906475
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 906476
    iget v0, p0, Lcom/facebook/drawingview/DrawingView$DrawLine;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 906477
    return-void
.end method
