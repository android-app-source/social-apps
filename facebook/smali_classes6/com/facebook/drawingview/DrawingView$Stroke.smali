.class public final Lcom/facebook/drawingview/DrawingView$Stroke;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/drawingview/DrawingView$Stroke;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/drawingview/DrawingView$DrawPoint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 906555
    new-instance v0, LX/5Nh;

    invoke-direct {v0}, LX/5Nh;-><init>()V

    sput-object v0, Lcom/facebook/drawingview/DrawingView$Stroke;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 906545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 906546
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/drawingview/DrawingView$Stroke;->a:Ljava/util/LinkedList;

    .line 906547
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView$Stroke;->a:Ljava/util/LinkedList;

    const-class v1, Lcom/facebook/drawingview/DrawingView$DrawQuad;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 906548
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 906554
    invoke-direct {p0, p1}, Lcom/facebook/drawingview/DrawingView$Stroke;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/LinkedList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/drawingview/DrawingView$DrawPoint;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 906556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 906557
    iput-object p1, p0, Lcom/facebook/drawingview/DrawingView$Stroke;->a:Ljava/util/LinkedList;

    .line 906558
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/drawingview/DrawingView$DrawPoint;)V
    .locals 1

    .prologue
    .line 906552
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView$Stroke;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 906553
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 906551
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 906549
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView$Stroke;->a:Ljava/util/LinkedList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 906550
    return-void
.end method
