.class public Lcom/facebook/drawingview/DrawingView;
.super Landroid/view/View;
.source ""


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final A:Ljava/lang/Runnable;

.field private B:LX/5Nd;

.field public a:Lcom/facebook/drawingview/DrawingView$Stroke;

.field public b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/drawingview/DrawingView$Stroke;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/5Ne;

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Bitmap;

.field private g:Landroid/graphics/Canvas;

.field private h:Landroid/graphics/Paint;

.field private i:F

.field private j:F

.field public k:F

.field public l:I

.field private m:LX/5Nf;

.field public n:LX/5Ng;

.field private o:Landroid/view/accessibility/AccessibilityManager;

.field private p:Landroid/view/VelocityTracker;

.field private q:I

.field private r:I

.field private final s:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/MotionEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final t:F

.field private u:Z

.field private v:F

.field private w:F

.field private x:Z

.field private final y:Ljava/lang/Runnable;

.field private final z:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 906673
    const-class v0, Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/drawingview/DrawingView;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 906674
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/drawingview/DrawingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 906675
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 906676
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/drawingview/DrawingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 906677
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 906678
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 906679
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/drawingview/DrawingView;->e:Landroid/graphics/Paint;

    .line 906680
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    .line 906681
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/drawingview/DrawingView;->s:Ljava/util/Queue;

    .line 906682
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/drawingview/DrawingView;->t:F

    .line 906683
    new-instance v0, Lcom/facebook/drawingview/DrawingView$1;

    invoke-direct {v0, p0}, Lcom/facebook/drawingview/DrawingView$1;-><init>(Lcom/facebook/drawingview/DrawingView;)V

    iput-object v0, p0, Lcom/facebook/drawingview/DrawingView;->y:Ljava/lang/Runnable;

    .line 906684
    new-instance v0, Lcom/facebook/drawingview/DrawingView$2;

    invoke-direct {v0, p0}, Lcom/facebook/drawingview/DrawingView$2;-><init>(Lcom/facebook/drawingview/DrawingView;)V

    iput-object v0, p0, Lcom/facebook/drawingview/DrawingView;->z:Ljava/lang/Runnable;

    .line 906685
    new-instance v0, Lcom/facebook/drawingview/DrawingView$3;

    invoke-direct {v0, p0}, Lcom/facebook/drawingview/DrawingView$3;-><init>(Lcom/facebook/drawingview/DrawingView;)V

    iput-object v0, p0, Lcom/facebook/drawingview/DrawingView;->A:Ljava/lang/Runnable;

    .line 906686
    invoke-virtual {p0}, Lcom/facebook/drawingview/DrawingView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->DrawingView:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 906687
    const/16 v1, 0x0

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/drawingview/DrawingView;->k:F

    .line 906688
    const/16 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/drawingview/DrawingView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a019a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/drawingview/DrawingView;->l:I

    .line 906689
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 906690
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 906691
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->e:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/drawingview/DrawingView;->l:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 906692
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 906693
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 906694
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->e:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/drawingview/DrawingView;->k:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 906695
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 906696
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/drawingview/DrawingView;->h:Landroid/graphics/Paint;

    .line 906697
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 906698
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 906699
    invoke-virtual {p0}, Lcom/facebook/drawingview/DrawingView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/facebook/drawingview/DrawingView;->o:Landroid/view/accessibility/AccessibilityManager;

    .line 906700
    return-void
.end method

.method private a(FF)V
    .locals 3

    .prologue
    .line 906701
    new-instance v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;

    iget v1, p0, Lcom/facebook/drawingview/DrawingView;->k:F

    iget v2, p0, Lcom/facebook/drawingview/DrawingView;->l:I

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/facebook/drawingview/DrawingView$DrawPoint;-><init>(FFFI)V

    .line 906702
    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView;->a:Lcom/facebook/drawingview/DrawingView$Stroke;

    invoke-virtual {v1, v0}, Lcom/facebook/drawingview/DrawingView$Stroke;->a(Lcom/facebook/drawingview/DrawingView$DrawPoint;)V

    .line 906703
    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView;->e:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/facebook/drawingview/DrawingView;->g:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1, v2, p0}, Lcom/facebook/drawingview/DrawingView$DrawPoint;->a(Landroid/graphics/Paint;Landroid/graphics/Canvas;Landroid/view/View;)V

    .line 906704
    iput p1, p0, Lcom/facebook/drawingview/DrawingView;->i:F

    .line 906705
    iput p2, p0, Lcom/facebook/drawingview/DrawingView;->j:F

    .line 906706
    return-void
.end method

.method private a(FFFF)V
    .locals 10

    .prologue
    .line 906707
    new-instance v0, Lcom/facebook/drawingview/DrawingView$DrawQuad;

    iget v5, p0, Lcom/facebook/drawingview/DrawingView;->i:F

    iget v6, p0, Lcom/facebook/drawingview/DrawingView;->j:F

    iget v7, p0, Lcom/facebook/drawingview/DrawingView;->k:F

    iget v8, p0, Lcom/facebook/drawingview/DrawingView;->l:I

    const/4 v9, 0x0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v9}, Lcom/facebook/drawingview/DrawingView$DrawQuad;-><init>(FFFFFFFIB)V

    .line 906708
    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView;->a:Lcom/facebook/drawingview/DrawingView$Stroke;

    invoke-virtual {v1, v0}, Lcom/facebook/drawingview/DrawingView$Stroke;->a(Lcom/facebook/drawingview/DrawingView$DrawPoint;)V

    .line 906709
    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView;->e:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/facebook/drawingview/DrawingView;->g:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1, v2, p0}, Lcom/facebook/drawingview/DrawingView$DrawQuad;->a(Landroid/graphics/Paint;Landroid/graphics/Canvas;Landroid/view/View;)V

    .line 906710
    iput p3, p0, Lcom/facebook/drawingview/DrawingView;->i:F

    .line 906711
    iput p4, p0, Lcom/facebook/drawingview/DrawingView;->j:F

    .line 906712
    return-void
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 906713
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 906714
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 906715
    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    .line 906716
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/facebook/drawingview/DrawingView;->g:Landroid/graphics/Canvas;

    .line 906717
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->g:Landroid/graphics/Canvas;

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 906718
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 12

    .prologue
    const/high16 v9, 0x42c80000    # 100.0f

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 906719
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 906720
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 906721
    iget-boolean v4, p0, Lcom/facebook/drawingview/DrawingView;->x:Z

    if-eqz v4, :cond_3

    .line 906722
    iput v0, p0, Lcom/facebook/drawingview/DrawingView;->v:F

    .line 906723
    iput v3, p0, Lcom/facebook/drawingview/DrawingView;->v:F

    .line 906724
    iput-boolean v2, p0, Lcom/facebook/drawingview/DrawingView;->x:Z

    move-object v0, p0

    :goto_0
    move-object v3, v0

    move v0, v2

    .line 906725
    :goto_1
    iput-boolean v0, v3, Lcom/facebook/drawingview/DrawingView;->u:Z

    .line 906726
    :cond_0
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->p:Landroid/view/VelocityTracker;

    const/16 v3, 0x3e8

    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 906727
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v4

    .line 906728
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v5

    .line 906729
    cmpl-float v0, v4, v8

    if-lez v0, :cond_4

    move v3, v1

    .line 906730
    :goto_2
    cmpl-float v0, v5, v8

    if-lez v0, :cond_6

    .line 906731
    :goto_3
    iget-boolean v0, p0, Lcom/facebook/drawingview/DrawingView;->u:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/facebook/drawingview/DrawingView;->q:I

    if-eq v0, v3, :cond_1

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v9

    if-gtz v0, :cond_2

    :cond_1
    iget v0, p0, Lcom/facebook/drawingview/DrawingView;->r:I

    if-eq v0, v1, :cond_9

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v9

    if-lez v0, :cond_9

    .line 906732
    :cond_2
    :goto_4
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->s:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/4 v2, 0x3

    if-lt v0, v2, :cond_8

    .line 906733
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->s:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    goto :goto_4

    .line 906734
    :cond_3
    iget-boolean v4, p0, Lcom/facebook/drawingview/DrawingView;->u:Z

    if-nez v4, :cond_0

    .line 906735
    iget v4, p0, Lcom/facebook/drawingview/DrawingView;->v:F

    sub-float v0, v4, v0

    float-to-double v4, v0

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    iget v0, p0, Lcom/facebook/drawingview/DrawingView;->w:F

    sub-float v0, v3, v0

    float-to-double v6, v0

    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    .line 906736
    iget v0, p0, Lcom/facebook/drawingview/DrawingView;->t:F

    float-to-double v6, v0

    cmpl-double v0, v4, v6

    if-lez v0, :cond_e

    move v0, v1

    move-object v3, p0

    goto :goto_1

    .line 906737
    :cond_4
    cmpg-float v0, v4, v8

    if-gez v0, :cond_5

    const/4 v0, -0x1

    move v3, v0

    goto :goto_2

    :cond_5
    move v3, v2

    goto :goto_2

    .line 906738
    :cond_6
    cmpg-float v0, v5, v8

    if-gez v0, :cond_7

    const/4 v1, -0x1

    goto :goto_3

    :cond_7
    move v1, v2

    goto :goto_3

    .line 906739
    :cond_8
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->s:Ljava/util/Queue;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 906740
    :cond_9
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 906741
    iget-object v2, p0, Lcom/facebook/drawingview/DrawingView;->s:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 906742
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->s:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    .line 906743
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 906744
    sub-long v6, v8, v6

    long-to-float v0, v6

    .line 906745
    :cond_a
    const v2, 0x43bb8000    # 375.0f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_c

    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->s:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_c

    .line 906746
    sget-object v0, LX/5Nd;->PEN_SCRIBBLE:LX/5Nd;

    invoke-static {p0, v0}, Lcom/facebook/drawingview/DrawingView;->setDrawingAudioState(Lcom/facebook/drawingview/DrawingView;LX/5Nd;)V

    .line 906747
    :cond_b
    :goto_5
    iput v3, p0, Lcom/facebook/drawingview/DrawingView;->q:I

    .line 906748
    iput v1, p0, Lcom/facebook/drawingview/DrawingView;->r:I

    .line 906749
    return-void

    .line 906750
    :cond_c
    const/high16 v0, 0x43480000    # 200.0f

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_d

    const/high16 v0, 0x43480000    # 200.0f

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_d

    .line 906751
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->y:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/drawingview/DrawingView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 906752
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->y:Ljava/lang/Runnable;

    const-wide/16 v4, 0x32

    invoke-virtual {p0, v0, v4, v5}, Lcom/facebook/drawingview/DrawingView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_5

    .line 906753
    :cond_d
    iget-boolean v0, p0, Lcom/facebook/drawingview/DrawingView;->u:Z

    if-eqz v0, :cond_b

    .line 906754
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->y:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/drawingview/DrawingView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 906755
    sget-object v0, LX/5Nd;->PEN_STROKE:LX/5Nd;

    invoke-static {p0, v0}, Lcom/facebook/drawingview/DrawingView;->setDrawingAudioState(Lcom/facebook/drawingview/DrawingView;LX/5Nd;)V

    goto :goto_5

    :cond_e
    move-object v0, p0

    goto/16 :goto_0
.end method

.method private b(FF)V
    .locals 8

    .prologue
    .line 906796
    new-instance v0, Lcom/facebook/drawingview/DrawingView$DrawLine;

    iget v3, p0, Lcom/facebook/drawingview/DrawingView;->i:F

    iget v4, p0, Lcom/facebook/drawingview/DrawingView;->j:F

    iget v5, p0, Lcom/facebook/drawingview/DrawingView;->k:F

    iget v6, p0, Lcom/facebook/drawingview/DrawingView;->l:I

    const/4 v7, 0x0

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/facebook/drawingview/DrawingView$DrawLine;-><init>(FFFFFIB)V

    .line 906797
    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView;->a:Lcom/facebook/drawingview/DrawingView$Stroke;

    invoke-virtual {v1, v0}, Lcom/facebook/drawingview/DrawingView$Stroke;->a(Lcom/facebook/drawingview/DrawingView$DrawPoint;)V

    .line 906798
    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView;->e:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/facebook/drawingview/DrawingView;->g:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1, v2, p0}, Lcom/facebook/drawingview/DrawingView$DrawPoint;->a(Landroid/graphics/Paint;Landroid/graphics/Canvas;Landroid/view/View;)V

    .line 906799
    iput p1, p0, Lcom/facebook/drawingview/DrawingView;->i:F

    .line 906800
    iput p2, p0, Lcom/facebook/drawingview/DrawingView;->j:F

    .line 906801
    return-void
.end method

.method public static d(Lcom/facebook/drawingview/DrawingView;)V
    .locals 5

    .prologue
    .line 906756
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawingview/DrawingView$Stroke;

    .line 906757
    iget-object v2, v0, Lcom/facebook/drawingview/DrawingView$Stroke;->a:Ljava/util/LinkedList;

    move-object v0, v2

    .line 906758
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;

    .line 906759
    iget-object v3, p0, Lcom/facebook/drawingview/DrawingView;->e:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/facebook/drawingview/DrawingView;->g:Landroid/graphics/Canvas;

    invoke-virtual {v0, v3, v4, p0}, Lcom/facebook/drawingview/DrawingView$DrawPoint;->a(Landroid/graphics/Paint;Landroid/graphics/Canvas;Landroid/view/View;)V

    goto :goto_0

    .line 906760
    :cond_1
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 906769
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->n:LX/5Ng;

    if-eqz v0, :cond_0

    .line 906770
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->n:LX/5Ng;

    invoke-interface {v0}, LX/5Ng;->a()V

    .line 906771
    :cond_0
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 906772
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->y:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/drawingview/DrawingView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 906773
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->A:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/drawingview/DrawingView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 906774
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->z:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/drawingview/DrawingView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 906775
    return-void
.end method

.method public static setDrawingAudioState(Lcom/facebook/drawingview/DrawingView;LX/5Nd;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x32

    .line 906776
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->B:LX/5Nd;

    if-ne p1, v0, :cond_0

    .line 906777
    :goto_0
    return-void

    .line 906778
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DrawingAudioState: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView;->B:LX/5Nd;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 906779
    sget-object v0, LX/5NZ;->a:[I

    invoke-virtual {p1}, LX/5Nd;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 906780
    :goto_1
    :pswitch_0
    iput-object p1, p0, Lcom/facebook/drawingview/DrawingView;->B:LX/5Nd;

    goto :goto_0

    .line 906781
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->y:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/drawingview/DrawingView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 906782
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->z:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/drawingview/DrawingView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 906783
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->A:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/drawingview/DrawingView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 906784
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->y:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/drawingview/DrawingView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 906785
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->A:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/drawingview/DrawingView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 906786
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->z:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/drawingview/DrawingView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 906787
    :pswitch_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawingview/DrawingView;->u:Z

    .line 906788
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/drawingview/DrawingView;->x:Z

    goto :goto_1

    .line 906789
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->B:LX/5Nd;

    sget-object v1, LX/5Nd;->PEN_DOWN:LX/5Nd;

    if-eq v0, v1, :cond_1

    .line 906790
    :cond_1
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->y:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/drawingview/DrawingView;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(I)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 906791
    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 906792
    :cond_0
    const/4 v0, 0x0

    .line 906793
    :goto_0
    return-object v0

    .line 906794
    :cond_1
    if-gtz p1, :cond_2

    move p1, v0

    .line 906795
    :cond_2
    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/2addr v2, p1

    iget-object v3, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/2addr v3, p1

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 906666
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 906667
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->g:Landroid/graphics/Canvas;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 906668
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->g:Landroid/graphics/Canvas;

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 906669
    invoke-virtual {p0}, Lcom/facebook/drawingview/DrawingView;->invalidate()V

    .line 906670
    :cond_0
    invoke-direct {p0}, Lcom/facebook/drawingview/DrawingView;->e()V

    .line 906671
    invoke-direct {p0}, Lcom/facebook/drawingview/DrawingView;->f()V

    .line 906672
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 906761
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 906762
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    .line 906763
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->g:Landroid/graphics/Canvas;

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 906764
    invoke-static {p0}, Lcom/facebook/drawingview/DrawingView;->d(Lcom/facebook/drawingview/DrawingView;)V

    .line 906765
    :cond_0
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 906766
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/drawingview/DrawingView;->invalidate()V

    .line 906767
    invoke-direct {p0}, Lcom/facebook/drawingview/DrawingView;->e()V

    .line 906768
    :cond_2
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 906559
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDoodleColor()I
    .locals 1

    .prologue
    .line 906568
    iget v0, p0, Lcom/facebook/drawingview/DrawingView;->l:I

    return v0
.end method

.method public getHistorySize()I
    .locals 1

    .prologue
    .line 906569
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method public getStrokeWidth()F
    .locals 1

    .prologue
    .line 906570
    iget v0, p0, Lcom/facebook/drawingview/DrawingView;->k:F

    return v0
.end method

.method public getStrokes()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 906571
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 906572
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawingview/DrawingView$Stroke;

    .line 906573
    iget-object v3, v0, Lcom/facebook/drawingview/DrawingView$Stroke;->a:Ljava/util/LinkedList;

    move-object v3, v3

    .line 906574
    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 906575
    iget-object v3, v0, Lcom/facebook/drawingview/DrawingView$Stroke;->a:Ljava/util/LinkedList;

    move-object v0, v3

    .line 906576
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;

    .line 906577
    const-string v3, "color:%s, width:%s"

    .line 906578
    iget v4, v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->g:I

    move v4, v4

    .line 906579
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 906580
    iget v5, v0, Lcom/facebook/drawingview/DrawingView$DrawPoint;->f:F

    move v0, v5

    .line 906581
    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 906582
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 906583
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 906584
    const/4 v0, 0x0

    .line 906585
    :goto_1
    return-object v0

    :cond_2
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_1
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 906586
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 906587
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->f:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 906588
    :cond_0
    return-void
.end method

.method public final onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 906560
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->o:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 906561
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 906562
    :goto_0
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/facebook/drawingview/DrawingView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 906563
    :goto_1
    return v0

    .line 906564
    :pswitch_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    .line 906565
    :pswitch_2
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    .line 906566
    :pswitch_3
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    .line 906567
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 906589
    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 906590
    check-cast v0, Landroid/os/Bundle;

    const-string v1, "DrawingView_super_state_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 906591
    check-cast p1, Landroid/os/Bundle;

    .line 906592
    const-string v0, "stroke_width_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/facebook/drawingview/DrawingView;->k:F

    .line 906593
    const-string v0, "stroke_color_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/drawingview/DrawingView;->l:I

    .line 906594
    const-string v0, "strokes_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 906595
    iget-object v1, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 906596
    new-instance v0, LX/5NY;

    invoke-direct {v0, p0}, LX/5NY;-><init>(Lcom/facebook/drawingview/DrawingView;)V

    .line 906597
    invoke-virtual {p0}, Lcom/facebook/drawingview/DrawingView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 906598
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 906599
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 906600
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 906601
    const-string v2, "DrawingView_super_state_key"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 906602
    const-string v0, "stroke_width_key"

    iget v2, p0, Lcom/facebook/drawingview/DrawingView;->k:F

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 906603
    const-string v0, "stroke_color_key"

    iget v2, p0, Lcom/facebook/drawingview/DrawingView;->l:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 906604
    const-string v0, "strokes_key"

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 906605
    return-object v1
.end method

.method public final onSizeChanged(IIII)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v0, 0x2c

    const v1, -0x334e8f31    # -9.3030008E7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 906606
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 906607
    :cond_0
    const/16 v1, 0x2d

    const v2, 0xa9a8597

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 906608
    :goto_0
    return-void

    .line 906609
    :cond_1
    sub-int v1, p1, p3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 906610
    sub-int v2, p2, p4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 906611
    if-gt v1, v3, :cond_2

    if-le v2, v3, :cond_3

    .line 906612
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawingview/DrawingView;->a(II)V

    .line 906613
    :cond_3
    const v1, -0x52476dc

    invoke-static {v1, v0}, LX/02F;->g(II)V

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v4, 0x2

    const/4 v0, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    const v2, -0x3c263ec

    invoke-static {v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 906614
    invoke-virtual {p0}, Lcom/facebook/drawingview/DrawingView;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 906615
    const v1, -0x7b3fe683

    invoke-static {v4, v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 906616
    :goto_0
    return v0

    .line 906617
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 906618
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 906619
    iget-object v5, p0, Lcom/facebook/drawingview/DrawingView;->p:Landroid/view/VelocityTracker;

    if-nez v5, :cond_1

    .line 906620
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v5

    iput-object v5, p0, Lcom/facebook/drawingview/DrawingView;->p:Landroid/view/VelocityTracker;

    .line 906621
    :cond_1
    iget-object v5, p0, Lcom/facebook/drawingview/DrawingView;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v5, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 906622
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 906623
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Ignored touch event: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 906624
    const v1, -0x36c7e2d9

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 906625
    :pswitch_0
    iput-boolean v0, p0, Lcom/facebook/drawingview/DrawingView;->u:Z

    .line 906626
    iput v3, p0, Lcom/facebook/drawingview/DrawingView;->v:F

    .line 906627
    iput v4, p0, Lcom/facebook/drawingview/DrawingView;->w:F

    .line 906628
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    .line 906629
    new-instance v5, Lcom/facebook/drawingview/DrawingView$Stroke;

    invoke-direct {v5, v0}, Lcom/facebook/drawingview/DrawingView$Stroke;-><init>(Ljava/util/LinkedList;)V

    iput-object v5, p0, Lcom/facebook/drawingview/DrawingView;->a:Lcom/facebook/drawingview/DrawingView$Stroke;

    .line 906630
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->b:Ljava/util/LinkedList;

    iget-object v5, p0, Lcom/facebook/drawingview/DrawingView;->a:Lcom/facebook/drawingview/DrawingView$Stroke;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 906631
    iput v3, p0, Lcom/facebook/drawingview/DrawingView;->i:F

    .line 906632
    iput v4, p0, Lcom/facebook/drawingview/DrawingView;->j:F

    .line 906633
    invoke-direct {p0, v3, v4}, Lcom/facebook/drawingview/DrawingView;->a(FF)V

    .line 906634
    sget-object v0, LX/5Nd;->PEN_DOWN:LX/5Nd;

    invoke-static {p0, v0}, Lcom/facebook/drawingview/DrawingView;->setDrawingAudioState(Lcom/facebook/drawingview/DrawingView;LX/5Nd;)V

    .line 906635
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->d:LX/5Ne;

    if-eqz v0, :cond_2

    .line 906636
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->d:LX/5Ne;

    invoke-interface {v0}, LX/5Ne;->a()V

    .line 906637
    :cond_2
    :goto_1
    const v0, 0x1b418937

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto :goto_0

    .line 906638
    :pswitch_1
    sget-object v0, LX/5Nd;->PEN_UP:LX/5Nd;

    invoke-static {p0, v0}, Lcom/facebook/drawingview/DrawingView;->setDrawingAudioState(Lcom/facebook/drawingview/DrawingView;LX/5Nd;)V

    .line 906639
    invoke-direct {p0, v3, v4}, Lcom/facebook/drawingview/DrawingView;->b(FF)V

    .line 906640
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 906641
    iput-object v6, p0, Lcom/facebook/drawingview/DrawingView;->p:Landroid/view/VelocityTracker;

    .line 906642
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->d:LX/5Ne;

    if-eqz v0, :cond_2

    .line 906643
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->d:LX/5Ne;

    invoke-interface {v0}, LX/5Ne;->b()V

    goto :goto_1

    .line 906644
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/facebook/drawingview/DrawingView;->a(Landroid/view/MotionEvent;)V

    .line 906645
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v5

    .line 906646
    :goto_2
    if-ge v0, v5, :cond_3

    .line 906647
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v6

    .line 906648
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v7

    .line 906649
    iget v8, p0, Lcom/facebook/drawingview/DrawingView;->i:F

    iget v9, p0, Lcom/facebook/drawingview/DrawingView;->j:F

    iget v10, p0, Lcom/facebook/drawingview/DrawingView;->i:F

    add-float/2addr v6, v10

    div-float/2addr v6, v11

    iget v10, p0, Lcom/facebook/drawingview/DrawingView;->j:F

    add-float/2addr v7, v10

    div-float/2addr v7, v11

    invoke-direct {p0, v8, v9, v6, v7}, Lcom/facebook/drawingview/DrawingView;->a(FFFF)V

    .line 906650
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 906651
    :cond_3
    iget v0, p0, Lcom/facebook/drawingview/DrawingView;->i:F

    iget v5, p0, Lcom/facebook/drawingview/DrawingView;->j:F

    iget v6, p0, Lcom/facebook/drawingview/DrawingView;->i:F

    add-float/2addr v3, v6

    div-float/2addr v3, v11

    iget v6, p0, Lcom/facebook/drawingview/DrawingView;->j:F

    add-float/2addr v4, v6

    div-float/2addr v4, v11

    invoke-direct {p0, v0, v5, v3, v4}, Lcom/facebook/drawingview/DrawingView;->a(FFFF)V

    goto :goto_1

    .line 906652
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 906653
    iput-object v6, p0, Lcom/facebook/drawingview/DrawingView;->p:Landroid/view/VelocityTracker;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setColour(I)V
    .locals 1

    .prologue
    .line 906654
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 906655
    iput p1, p0, Lcom/facebook/drawingview/DrawingView;->l:I

    .line 906656
    return-void
.end method

.method public setDrawingListener(LX/5Ne;)V
    .locals 0

    .prologue
    .line 906657
    iput-object p1, p0, Lcom/facebook/drawingview/DrawingView;->d:LX/5Ne;

    .line 906658
    return-void
.end method

.method public setOnDrawConfirmedListener(LX/5Nf;)V
    .locals 0

    .prologue
    .line 906659
    iput-object p1, p0, Lcom/facebook/drawingview/DrawingView;->m:LX/5Nf;

    .line 906660
    return-void
.end method

.method public setOnDrawingClearedListener(LX/5Ng;)V
    .locals 0

    .prologue
    .line 906661
    iput-object p1, p0, Lcom/facebook/drawingview/DrawingView;->n:LX/5Ng;

    .line 906662
    return-void
.end method

.method public setStrokeWidth(F)V
    .locals 2

    .prologue
    .line 906663
    iput p1, p0, Lcom/facebook/drawingview/DrawingView;->k:F

    .line 906664
    iget-object v0, p0, Lcom/facebook/drawingview/DrawingView;->e:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/drawingview/DrawingView;->k:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 906665
    return-void
.end method
