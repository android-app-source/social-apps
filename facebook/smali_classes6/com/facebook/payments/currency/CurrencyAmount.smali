.class public Lcom/facebook/payments/currency/CurrencyAmount;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/facebook/payments/currency/CurrencyAmount;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/math/BigDecimal;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/math/BigDecimal;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 972393
    new-instance v0, Ljava/math/BigDecimal;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    sput-object v0, Lcom/facebook/payments/currency/CurrencyAmount;->a:Ljava/math/BigDecimal;

    .line 972394
    new-instance v0, LX/5ft;

    invoke-direct {v0}, LX/5ft;-><init>()V

    sput-object v0, Lcom/facebook/payments/currency/CurrencyAmount;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 972395
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    invoke-direct {p0, v1, v0}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 972396
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 972397
    const/16 v0, 0x64

    invoke-static {p2, p3, v0}, Lcom/facebook/payments/currency/CurrencyAmount;->a(JI)Ljava/math/BigDecimal;

    move-result-object v0

    move-object v0, v0

    .line 972398
    invoke-direct {p0, p1, v0}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 972399
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/math/BigDecimal;)V
    .locals 1

    .prologue
    .line 972400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 972401
    invoke-static {p1}, Lcom/facebook/payments/currency/CurrencyAmount;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    .line 972402
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    iput-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    .line 972403
    return-void
.end method

.method public constructor <init>(Ljava/util/Currency;Ljava/math/BigDecimal;)V
    .locals 1

    .prologue
    .line 972404
    invoke-virtual {p1}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 972405
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 4

    .prologue
    .line 972406
    new-instance v0, Lcom/facebook/payments/currency/CurrencyAmount;

    const-wide/16 v2, 0x0

    invoke-direct {v0, p0, v2, v3}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    return-object v0
.end method

.method public static a(Ljava/util/Locale;Ljava/util/Currency;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 3

    .prologue
    .line 972429
    const-string v0, "[^0-9.,]"

    const-string v1, ""

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 972430
    invoke-static {p0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    .line 972431
    new-instance v2, Ljava/math/BigDecimal;

    invoke-virtual {v1, v0}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 972432
    new-instance v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    return-object v0
.end method

.method public static a(JI)Ljava/math/BigDecimal;
    .locals 4

    .prologue
    .line 972407
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p0, p1}, Ljava/math/BigDecimal;-><init>(J)V

    .line 972408
    const/16 v2, 0x64

    if-ne p2, v2, :cond_0

    sget-object v2, Lcom/facebook/payments/currency/CurrencyAmount;->a:Ljava/math/BigDecimal;

    :goto_0
    move-object v1, v2

    .line 972409
    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0

    :cond_0
    int-to-long v2, p2

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v2

    goto :goto_0
.end method

.method public static a(Ljava/math/BigDecimal;)Z
    .locals 1

    .prologue
    .line 972410
    invoke-virtual {p0}, Ljava/math/BigDecimal;->signum()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Ljava/math/BigDecimal;->stripTrailingZeros()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/math/BigDecimal;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 2

    .prologue
    .line 972411
    new-instance v0, Lcom/facebook/payments/currency/CurrencyAmount;

    .line 972412
    iget-object v1, p0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v1, v1

    .line 972413
    invoke-direct {v0, v1, p1}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    return-object v0
.end method

.method public static c(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 972414
    sget-object v0, LX/5fx;->a:LX/0P1;

    move-object v0, v0

    .line 972415
    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    const-string v1, "offset"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 972416
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 972417
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Invalid currency length: %d for currencyCode: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p0, v4, v1

    invoke-static {v0, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 972418
    return-object p0

    :cond_0
    move v0, v2

    .line 972419
    goto :goto_0
.end method

.method public static d(Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 972420
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 972421
    iget-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v0, v0

    .line 972422
    iget-object v1, p1, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v1, v1

    .line 972423
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "%s != $s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 972424
    iget-object v4, p0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v4, v4

    .line 972425
    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 972426
    iget-object v4, p1, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v4, v4

    .line 972427
    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 972428
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/currency/CurrencyAmount;)I
    .locals 2

    .prologue
    .line 972387
    invoke-static {p0, p1}, Lcom/facebook/payments/currency/CurrencyAmount;->d(Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)V

    .line 972388
    iget-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v0, v0

    .line 972389
    iget-object v1, p1, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v1, v1

    .line 972390
    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 2

    .prologue
    .line 972391
    iget-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v0, v0

    .line 972392
    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, p1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/payments/currency/CurrencyAmount;->b(Ljava/math/BigDecimal;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 972331
    iget-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/util/Locale;LX/5fu;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 972332
    invoke-static {p1}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    .line 972333
    iget-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v0, v0

    .line 972334
    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V

    .line 972335
    invoke-virtual {p2}, LX/5fu;->hasCurrencySymbol()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 972336
    check-cast v0, Ljava/text/DecimalFormat;

    .line 972337
    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v2

    .line 972338
    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/text/DecimalFormatSymbols;->setCurrencySymbol(Ljava/lang/String;)V

    .line 972339
    invoke-virtual {v0, v2}, Ljava/text/DecimalFormat;->setDecimalFormatSymbols(Ljava/text/DecimalFormatSymbols;)V

    .line 972340
    :cond_0
    invoke-virtual {p2}, LX/5fu;->hasEmptyDecimals()Z

    move-result v0

    if-nez v0, :cond_1

    .line 972341
    iget-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v0, v0

    .line 972342
    invoke-static {v0}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 972343
    invoke-virtual {v1, v4}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 972344
    invoke-virtual {v1, v4}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 972345
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v0, v0

    .line 972346
    invoke-virtual {v1, v0}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 972347
    const-string v0, "%s - %s"

    sget-object v1, LX/5fu;->NO_EMPTY_DECIMALS:LX/5fu;

    invoke-virtual {p0, p1, v1}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/util/Locale;LX/5fu;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 972348
    iget-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 972349
    sget-object v0, Lcom/facebook/payments/currency/CurrencyAmount;->a:Ljava/math/BigDecimal;

    .line 972350
    iget-object v1, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v1, v1

    .line 972351
    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    return v0
.end method

.method public final c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 2

    .prologue
    .line 972352
    invoke-static {p0, p1}, Lcom/facebook/payments/currency/CurrencyAmount;->d(Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)V

    .line 972353
    iget-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v0, v0

    .line 972354
    iget-object v1, p1, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v1, v1

    .line 972355
    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/payments/currency/CurrencyAmount;->b(Ljava/math/BigDecimal;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 972356
    check-cast p1, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p0, p1}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Lcom/facebook/payments/currency/CurrencyAmount;)I

    move-result v0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 972357
    iget-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v0, v0

    .line 972358
    invoke-static {v0}, LX/5fx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 972359
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 972360
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 972361
    iget-object v1, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v1, v1

    .line 972362
    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 972363
    if-ne p0, p1, :cond_1

    .line 972364
    :cond_0
    :goto_0
    return v0

    .line 972365
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 972366
    goto :goto_0

    .line 972367
    :cond_3
    check-cast p1, Lcom/facebook/payments/currency/CurrencyAmount;

    .line 972368
    iget-object v2, p0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v2, v2

    .line 972369
    iget-object v3, p1, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v3, v3

    .line 972370
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 972371
    iget-object v2, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v2, v2

    .line 972372
    iget-object v3, p1, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v3, v3

    .line 972373
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 972374
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 972375
    iget-object v2, p0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v2, v2

    .line 972376
    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 972377
    iget-object v2, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v2, v2

    .line 972378
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 972379
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "%s%."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 972380
    iget-object v1, p0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v1, v1

    .line 972381
    invoke-static {v1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Currency;->getDefaultFractionDigits()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "f"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/payments/currency/CurrencyAmount;->d()Ljava/lang/String;

    move-result-object v1

    .line 972382
    iget-object v2, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v2, v2

    .line 972383
    invoke-virtual {v2}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 972384
    iget-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 972385
    iget-object v0, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 972386
    return-void
.end method
