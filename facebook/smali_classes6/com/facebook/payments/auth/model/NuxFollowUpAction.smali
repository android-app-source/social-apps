.class public Lcom/facebook/payments/auth/model/NuxFollowUpAction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/auth/model/NuxFollowUpAction;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/payments/auth/model/NuxFollowUpAction;


# instance fields
.field public final b:Z

.field public final c:Z

.field private final d:Z

.field public final e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 972326
    invoke-static {}, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->a()LX/5fs;

    move-result-object v0

    invoke-virtual {v0}, LX/5fs;->a()Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->a:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    .line 972327
    new-instance v0, LX/5fr;

    invoke-direct {v0}, LX/5fr;-><init>()V

    sput-object v0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5fs;)V
    .locals 2

    .prologue
    .line 972318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 972319
    iget-boolean v0, p1, LX/5fs;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, LX/5fs;->b:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "showPinNux and showFingerprintNux can\'t both be true"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 972320
    iget-boolean v0, p1, LX/5fs;->a:Z

    iput-boolean v0, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->b:Z

    .line 972321
    iget-boolean v0, p1, LX/5fs;->b:Z

    iput-boolean v0, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->c:Z

    .line 972322
    iget-boolean v0, p1, LX/5fs;->c:Z

    iput-boolean v0, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->d:Z

    .line 972323
    iget-boolean v0, p1, LX/5fs;->d:Z

    iput-boolean v0, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->e:Z

    .line 972324
    return-void

    .line 972325
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 972312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 972313
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->b:Z

    .line 972314
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->c:Z

    .line 972315
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->d:Z

    .line 972316
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->e:Z

    .line 972317
    return-void
.end method

.method public static a()LX/5fs;
    .locals 1

    .prologue
    .line 972311
    new-instance v0, LX/5fs;

    invoke-direct {v0}, LX/5fs;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 972310
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 972309
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mShowFingerprintNux"

    iget-boolean v2, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->b:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    const-string v1, "mShowPinNux"

    iget-boolean v2, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->c:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    const-string v1, "mShowCardAddedNux"

    iget-boolean v2, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->d:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    const-string v1, "mIsPinPresent"

    iget-boolean v2, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->e:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 972304
    iget-boolean v0, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 972305
    iget-boolean v0, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 972306
    iget-boolean v0, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 972307
    iget-boolean v0, p0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 972308
    return-void
.end method
