.class public Lcom/facebook/apptab/state/NavigationImmersiveConfig;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/apptab/state/NavigationImmersiveConfigDeserializer;
.end annotation


# instance fields
.field public final animationSpeed:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "animation_speed"
    .end annotation
.end field

.field public final buttonAction:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "button_action"
    .end annotation
.end field

.field public final experimentGroupName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "experiment_group_name"
    .end annotation
.end field

.field public final experimentName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "experiment_name"
    .end annotation
.end field

.field public final fbLogoBadgeCount:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fb_logo_badge_count"
    .end annotation
.end field

.field public final fbLogoBadgeStyle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fb_logo_badge_style"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 889718
    const-class v0, Lcom/facebook/apptab/state/NavigationImmersiveConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 889719
    const-class v0, Lcom/facebook/apptab/state/NavigationImmersiveConfigSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 889720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889721
    const-string v0, "unknown_experiment"

    iput-object v0, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->experimentName:Ljava/lang/String;

    .line 889722
    const-string v0, "unknown_experiment_group"

    iput-object v0, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->experimentGroupName:Ljava/lang/String;

    .line 889723
    const-string v0, "none"

    iput-object v0, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->fbLogoBadgeStyle:Ljava/lang/String;

    .line 889724
    const-string v0, "all"

    iput-object v0, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->fbLogoBadgeCount:Ljava/lang/String;

    .line 889725
    const-string v0, "system_back"

    iput-object v0, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->buttonAction:Ljava/lang/String;

    .line 889726
    const-string v0, "medium_speed_animation"

    iput-object v0, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->animationSpeed:Ljava/lang/String;

    .line 889727
    return-void
.end method

.method public constructor <init>(LX/5HE;)V
    .locals 1

    .prologue
    .line 889728
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889729
    iget-object v0, p1, LX/5HE;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->experimentName:Ljava/lang/String;

    .line 889730
    iget-object v0, p1, LX/5HE;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->experimentGroupName:Ljava/lang/String;

    .line 889731
    iget-object v0, p1, LX/5HE;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->fbLogoBadgeStyle:Ljava/lang/String;

    .line 889732
    iget-object v0, p1, LX/5HE;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->fbLogoBadgeCount:Ljava/lang/String;

    .line 889733
    iget-object v0, p1, LX/5HE;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->buttonAction:Ljava/lang/String;

    .line 889734
    iget-object v0, p1, LX/5HE;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->animationSpeed:Ljava/lang/String;

    .line 889735
    return-void
.end method

.method public static a()Lcom/facebook/apptab/state/NavigationImmersiveConfig;
    .locals 2

    .prologue
    .line 889736
    new-instance v0, LX/5HE;

    invoke-direct {v0}, LX/5HE;-><init>()V

    const-string v1, "unknown_experiment"

    .line 889737
    iput-object v1, v0, LX/5HE;->a:Ljava/lang/String;

    .line 889738
    move-object v0, v0

    .line 889739
    const-string v1, "unknown_experiment_group"

    .line 889740
    iput-object v1, v0, LX/5HE;->b:Ljava/lang/String;

    .line 889741
    move-object v0, v0

    .line 889742
    const-string v1, "none"

    .line 889743
    iput-object v1, v0, LX/5HE;->c:Ljava/lang/String;

    .line 889744
    move-object v0, v0

    .line 889745
    const-string v1, "system_back"

    .line 889746
    iput-object v1, v0, LX/5HE;->e:Ljava/lang/String;

    .line 889747
    move-object v0, v0

    .line 889748
    const-string v1, "medium_speed_animation"

    .line 889749
    iput-object v1, v0, LX/5HE;->f:Ljava/lang/String;

    .line 889750
    move-object v0, v0

    .line 889751
    invoke-virtual {v0}, LX/5HE;->a()Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 889752
    instance-of v1, p1, Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    if-nez v1, :cond_1

    .line 889753
    :cond_0
    :goto_0
    return v0

    .line 889754
    :cond_1
    check-cast p1, Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    .line 889755
    iget-object v1, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->experimentName:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->experimentName:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->experimentGroupName:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->experimentGroupName:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->buttonAction:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->buttonAction:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->animationSpeed:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->animationSpeed:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->fbLogoBadgeStyle:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->fbLogoBadgeStyle:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->fbLogoBadgeCount:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->fbLogoBadgeCount:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 889756
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->experimentName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->experimentGroupName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->buttonAction:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->animationSpeed:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->fbLogoBadgeStyle:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->fbLogoBadgeCount:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
