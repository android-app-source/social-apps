.class public Lcom/facebook/apptab/state/NavigationImmersiveConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/apptab/state/NavigationImmersiveConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 889783
    const-class v0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    new-instance v1, Lcom/facebook/apptab/state/NavigationImmersiveConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/apptab/state/NavigationImmersiveConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 889784
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 889785
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/apptab/state/NavigationImmersiveConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 889786
    if-nez p0, :cond_0

    .line 889787
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 889788
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 889789
    invoke-static {p0, p1, p2}, Lcom/facebook/apptab/state/NavigationImmersiveConfigSerializer;->b(Lcom/facebook/apptab/state/NavigationImmersiveConfig;LX/0nX;LX/0my;)V

    .line 889790
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 889791
    return-void
.end method

.method private static b(Lcom/facebook/apptab/state/NavigationImmersiveConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 889792
    const-string v0, "experiment_name"

    iget-object v1, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->experimentName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 889793
    const-string v0, "experiment_group_name"

    iget-object v1, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->experimentGroupName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 889794
    const-string v0, "fb_logo_badge_style"

    iget-object v1, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->fbLogoBadgeStyle:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 889795
    const-string v0, "fb_logo_badge_count"

    iget-object v1, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->fbLogoBadgeCount:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 889796
    const-string v0, "button_action"

    iget-object v1, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->buttonAction:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 889797
    const-string v0, "animation_speed"

    iget-object v1, p0, Lcom/facebook/apptab/state/NavigationImmersiveConfig;->animationSpeed:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 889798
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 889799
    check-cast p1, Lcom/facebook/apptab/state/NavigationImmersiveConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/apptab/state/NavigationImmersiveConfigSerializer;->a(Lcom/facebook/apptab/state/NavigationImmersiveConfig;LX/0nX;LX/0my;)V

    return-void
.end method
