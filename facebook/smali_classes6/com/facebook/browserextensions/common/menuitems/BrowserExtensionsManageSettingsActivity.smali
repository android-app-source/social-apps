.class public Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# static fields
.field private static final a:LX/0Tn;

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/6DO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/6Ef;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field private i:Z

.field public j:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1066670
    sget-object v0, LX/6D7;->b:LX/0Tn;

    const-string v1, "settings"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->a:LX/0Tn;

    .line 1066671
    const-class v0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1066668
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    .line 1066669
    return-void
.end method

.method private a(Landroid/preference/PreferenceScreen;)V
    .locals 1

    .prologue
    .line 1066664
    invoke-direct {p0, p1}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->b(Landroid/preference/PreferenceScreen;)V

    .line 1066665
    iget-boolean v0, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->i:Z

    if-eqz v0, :cond_0

    .line 1066666
    invoke-direct {p0, p1}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->c(Landroid/preference/PreferenceScreen;)V

    .line 1066667
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;LX/03V;LX/6DO;Lcom/facebook/content/SecureContextHelper;LX/6Ef;)V
    .locals 0

    .prologue
    .line 1066663
    iput-object p1, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->c:LX/03V;

    iput-object p2, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->d:LX/6DO;

    iput-object p3, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object p4, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->f:LX/6Ef;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;

    invoke-static {v3}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {v3}, LX/6DO;->a(LX/0QB;)LX/6DO;

    move-result-object v1

    check-cast v1, LX/6DO;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v3}, LX/6Ef;->a(LX/0QB;)LX/6Ef;

    move-result-object v3

    check-cast v3, LX/6Ef;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->a(Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;LX/03V;LX/6DO;Lcom/facebook/content/SecureContextHelper;LX/6Ef;)V

    return-void
.end method

.method private b(Landroid/preference/PreferenceScreen;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1066652
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1066653
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081d6e

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->h:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 1066654
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1066655
    new-instance v0, LX/4ok;

    invoke-direct {v0, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 1066656
    sget-object v1, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 1066657
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081d71

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 1066658
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081d72

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->h:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setSummary(Ljava/lang/CharSequence;)V

    .line 1066659
    iget-object v1, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->f:LX/6Ef;

    invoke-virtual {v1}, LX/6Ef;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/4ok;->setChecked(Z)V

    .line 1066660
    new-instance v1, LX/6Ek;

    invoke-direct {v1, p0}, LX/6Ek;-><init>(Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4ok;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1066661
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1066662
    return-void
.end method

.method private c(Landroid/preference/PreferenceScreen;)V
    .locals 3

    .prologue
    .line 1066644
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1066645
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081d70

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 1066646
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1066647
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1066648
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081d7b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1066649
    new-instance v1, LX/6Em;

    invoke-direct {v1, p0}, LX/6Em;-><init>(Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1066650
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1066651
    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1066625
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 1066626
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1066627
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->j:Landroid/content/Context;

    .line 1066628
    const-string v1, "app_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->g:Ljava/lang/String;

    .line 1066629
    const-string v1, "app_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->h:Ljava/lang/String;

    .line 1066630
    const-string v1, "autofill_setting_enabled"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->i:Z

    .line 1066631
    iget-object v0, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1066632
    :cond_0
    iget-object v0, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->c:LX/03V;

    sget-object v1, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->b:Ljava/lang/String;

    const-string v2, "App ID not provided, failed to start activity"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066633
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "App id must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1066634
    :cond_1
    invoke-static {p0, p0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1066635
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 1066636
    invoke-virtual {p0, v0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1066637
    invoke-direct {p0, v0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->a(Landroid/preference/PreferenceScreen;)V

    .line 1066638
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081d6f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1066639
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x441db305

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1066640
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onResume()V

    .line 1066641
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    sget-object v2, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->a:LX/0Tn;

    invoke-virtual {v2}, LX/0Tn;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, LX/4ok;

    .line 1066642
    iget-object v2, p0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->f:LX/6Ef;

    invoke-virtual {v2}, LX/6Ef;->a()Z

    move-result v2

    invoke-virtual {v0, v2}, LX/4ok;->setChecked(Z)V

    .line 1066643
    const/16 v0, 0x23

    const v2, -0xc9ea1a5

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
