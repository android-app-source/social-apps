.class public Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:Lcom/facebook/http/common/FbHttpRequestProcessor;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/11M;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0lC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/6D1;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1064630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1064631
    return-void
.end method

.method public static a(Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;)LX/15D;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ")",
            "LX/15D",
            "<",
            "LX/0lF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1064636
    new-instance v0, LX/2CN;

    iget-object v1, p0, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->c:LX/0lC;

    iget-object v2, p0, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->b:LX/11M;

    invoke-direct {v0, v1, v2}, LX/2CN;-><init>(LX/0lC;LX/11M;)V

    .line 1064637
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v1

    .line 1064638
    iput-object p1, v1, LX/15E;->c:Ljava/lang/String;

    .line 1064639
    move-object v1, v1

    .line 1064640
    const-class v2, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 1064641
    iput-object v2, v1, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1064642
    move-object v1, v1

    .line 1064643
    iput-object p2, v1, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 1064644
    move-object v1, v1

    .line 1064645
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1064646
    iput-object v2, v1, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1064647
    move-object v1, v1

    .line 1064648
    iput-object v0, v1, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 1064649
    move-object v0, v1

    .line 1064650
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    .line 1064651
    return-object v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;
    .locals 5

    .prologue
    .line 1064632
    new-instance v4, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;

    invoke-direct {v4}, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;-><init>()V

    .line 1064633
    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p0}, LX/11M;->a(LX/0QB;)LX/11M;

    move-result-object v1

    check-cast v1, LX/11M;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v2

    check-cast v2, LX/0lC;

    invoke-static {p0}, LX/6D1;->b(LX/0QB;)LX/6D1;

    move-result-object v3

    check-cast v3, LX/6D1;

    .line 1064634
    iput-object v0, v4, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    iput-object v1, v4, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->b:LX/11M;

    iput-object v2, v4, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->c:LX/0lC;

    iput-object v3, v4, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->d:LX/6D1;

    .line 1064635
    return-object v4
.end method
