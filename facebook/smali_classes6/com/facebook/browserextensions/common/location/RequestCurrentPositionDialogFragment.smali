.class public Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;
.super Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final m:Ljava/lang/String;

.field public static final n:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public o:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public x:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public z:LX/6Ef;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1066502
    const-class v0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->m:Ljava/lang/String;

    .line 1066503
    const-class v0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    const-string v1, "browserextensions_location"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1066504
    invoke-direct {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;-><init>()V

    .line 1066505
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1066506
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->s:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1066507
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1066508
    iget-object v0, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->z:LX/6Ef;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6Ef;->a(Ljava/lang/Boolean;)V

    .line 1066509
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->l()V

    .line 1066510
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->p:LX/6EP;

    .line 1066511
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->w:LX/6ET;

    sget-object v1, LX/6ET;->REQUEST_PERMISSION:LX/6ET;

    if-ne v0, v1, :cond_0

    .line 1066512
    iget-object v0, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->z:LX/6Ef;

    const/4 v1, 0x1

    .line 1066513
    iput-boolean v1, v0, LX/6Ef;->b:Z

    .line 1066514
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1066515
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->s:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1066516
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1066517
    iget-object v0, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->x:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sS;

    .line 1066518
    sget-object v1, LX/6Ef;->a:Lcom/facebook/location/FbLocationOperationParams;

    sget-object v2, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/1sS;->a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1066519
    iget-object v1, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->y:LX/1Ck;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, LX/6Eh;

    invoke-direct {v3, p0}, LX/6Eh;-><init>(Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1066520
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5b04f43c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1066521
    invoke-super {p0, p1}, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1066522
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    const/16 v5, 0xc81

    invoke-static {p1, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {p1}, LX/6Ef;->a(LX/0QB;)LX/6Ef;

    move-result-object p1

    check-cast p1, LX/6Ef;

    iput-object v4, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->o:LX/03V;

    iput-object v1, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->x:LX/0Or;

    iput-object v5, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->y:LX/1Ck;

    iput-object p1, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->z:LX/6Ef;

    .line 1066523
    const/16 v1, 0x2b

    const v2, 0x12716b12

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5db51748

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1066524
    iget-object v1, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->y:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1066525
    invoke-super {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->onDestroyView()V

    .line 1066526
    const/16 v1, 0x2b

    const v2, -0x429a60d8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
