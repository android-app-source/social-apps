.class public Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/6Eg;

.field private final f:LX/0y3;

.field private final g:LX/6Ef;

.field public final h:LX/03V;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1066585
    const-class v0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->a:Ljava/lang/String;

    .line 1066586
    const-class v0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;

    const-string v1, "browserextensions_location"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/6Eg;LX/0y3;LX/6Ef;LX/03V;LX/0Or;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/6Eg;",
            "LX/0y3;",
            "LX/6Ef;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1066575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066576
    iput-object p1, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->c:Landroid/content/Context;

    .line 1066577
    iput-object p2, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1066578
    iput-object p3, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->e:LX/6Eg;

    .line 1066579
    iput-object p4, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->f:LX/0y3;

    .line 1066580
    iput-object p5, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->g:LX/6Ef;

    .line 1066581
    iput-object p6, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->h:LX/03V;

    .line 1066582
    iput-object p7, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->i:LX/0Or;

    .line 1066583
    iput-object p8, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->j:LX/0Uh;

    .line 1066584
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1066527
    const-string v0, "requestCurrentPosition"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 7

    .prologue
    .line 1066528
    check-cast p1, Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;

    .line 1066529
    iget-object v0, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->e:LX/6Eg;

    .line 1066530
    iput-object p1, v0, LX/6Eg;->b:Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;

    .line 1066531
    iget-object v0, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->j:LX/0Uh;

    const/16 v1, 0x50e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 1066532
    if-nez v0, :cond_1

    .line 1066533
    iget-object v0, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->e:LX/6Eg;

    sget-object v1, LX/6Cy;->BROWSER_EXTENSION_USER_DENIED_PERMISSION:LX/6Cy;

    invoke-virtual {v0, v1}, LX/6Eg;->a(LX/6Cy;)V

    .line 1066534
    :cond_0
    :goto_0
    return-void

    .line 1066535
    :cond_1
    iget-object v0, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->f:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v1, :cond_3

    .line 1066536
    invoke-virtual {p1}, Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;->g()Ljava/lang/String;

    .line 1066537
    iget-object v0, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->g:LX/6Ef;

    invoke-virtual {v0}, LX/6Ef;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1066538
    :try_start_0
    iget-object v0, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sS;

    .line 1066539
    sget-object v1, LX/6Ef;->a:Lcom/facebook/location/FbLocationOperationParams;

    sget-object v2, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/1sS;->a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1066540
    invoke-static {v0}, LX/1v3;->b(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    .line 1066541
    move-object v0, v0

    .line 1066542
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1066543
    :try_start_1
    const-string v4, "latitude"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 1066544
    const-string v4, "longitude"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 1066545
    const-string v4, "accuracy"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1066546
    iget-object v4, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->e:LX/6Eg;

    invoke-virtual {v4, v3}, LX/6Eg;->a(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1066547
    :goto_1
    goto :goto_0

    .line 1066548
    :catch_0
    move-exception v0

    .line 1066549
    iget-object v1, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->h:LX/03V;

    sget-object v2, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1066550
    goto :goto_0

    .line 1066551
    :cond_2
    iget-object v0, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->g:LX/6Ef;

    .line 1066552
    iget-boolean v1, v0, LX/6Ef;->b:Z

    move v0, v1

    .line 1066553
    if-nez v0, :cond_0

    .line 1066554
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1066555
    sget-object v1, LX/6D6;->LOCATION:LX/6D6;

    invoke-virtual {v1}, LX/6D6;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1066556
    new-instance v1, LX/6Ej;

    iget-object v2, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/6Ej;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;->g()Ljava/lang/String;

    move-result-object v2

    .line 1066557
    iput-object v2, v1, LX/6EK;->c:Ljava/lang/String;

    .line 1066558
    move-object v1, v1

    .line 1066559
    const-string v2, "JS_BRIDGE_APP_NAME"

    invoke-virtual {p1, v2}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v2, v2

    .line 1066560
    iput-object v2, v1, LX/6EK;->d:Ljava/lang/String;

    .line 1066561
    move-object v1, v1

    .line 1066562
    iput-object v0, v1, LX/6EK;->e:Ljava/util/ArrayList;

    .line 1066563
    move-object v0, v1

    .line 1066564
    sget-object v1, LX/6ET;->REQUEST_PERMISSION:LX/6ET;

    .line 1066565
    iput-object v1, v0, LX/6EK;->a:LX/6ET;

    .line 1066566
    move-object v0, v0

    .line 1066567
    invoke-virtual {v0}, LX/6EK;->b()Landroid/content/Intent;

    move-result-object v0

    .line 1066568
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1066569
    iget-object v1, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->c:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1066570
    :cond_3
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1066571
    :try_start_2
    const-string v1, "error"

    const-string v2, "Location not enabled on this device"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 1066572
    :catch_1
    iget-object v1, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->e:LX/6Eg;

    invoke-virtual {v1, v0}, LX/6Eg;->a(Lorg/json/JSONObject;)V

    goto/16 :goto_0

    .line 1066573
    :catch_2
    move-exception v3

    .line 1066574
    iget-object v4, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->h:LX/03V;

    sget-object v5, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
