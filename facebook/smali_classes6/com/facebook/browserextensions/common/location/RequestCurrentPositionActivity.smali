.class public Lcom/facebook/browserextensions/common/location/RequestCurrentPositionActivity;
.super Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;
.source ""


# instance fields
.field public p:LX/6Eg;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1066425
    invoke-direct {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionActivity;

    invoke-static {v0}, LX/6Eg;->a(LX/0QB;)LX/6Eg;

    move-result-object v0

    check-cast v0, LX/6Eg;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionActivity;->p:LX/6Eg;

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;
    .locals 2

    .prologue
    .line 1066426
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1066427
    new-instance v1, LX/6Ei;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {v1, v0}, LX/6Ei;-><init>(Landroid/os/Bundle;)V

    .line 1066428
    new-instance v0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    invoke-direct {v0}, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;-><init>()V

    .line 1066429
    invoke-virtual {v1}, LX/6EI;->b()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1066430
    move-object v0, v0

    .line 1066431
    return-object v0
.end method

.method public final b()LX/6EB;
    .locals 1

    .prologue
    .line 1066432
    iget-object v0, p0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionActivity;->p:LX/6Eg;

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1066433
    invoke-super {p0, p1}, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->b(Landroid/os/Bundle;)V

    .line 1066434
    invoke-static {p0, p0}, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1066435
    return-void
.end method
