.class public Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;

.field private final q:LX/6DW;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1065239
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1065240
    new-instance v0, LX/6DW;

    invoke-direct {v0, p0}, LX/6DW;-><init>(Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataActivity;)V

    iput-object v0, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataActivity;->q:LX/6DW;

    return-void
.end method

.method private a()Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;
    .locals 4

    .prologue
    .line 1065241
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1065242
    new-instance v1, LX/6DZ;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {v1, v0}, LX/6DZ;-><init>(Landroid/os/Bundle;)V

    .line 1065243
    new-instance v0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;

    invoke-direct {v0}, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;-><init>()V

    .line 1065244
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1065245
    const-string v3, "app_name"

    iget-object p0, v1, LX/6DZ;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1065246
    const-string v3, "autofill_data"

    iget-object p0, v1, LX/6DZ;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1065247
    const-string v3, "js_bridge_call"

    iget-object p0, v1, LX/6DZ;->c:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    invoke-virtual {v2, v3, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1065248
    move-object v2, v2

    .line 1065249
    move-object v2, v2

    .line 1065250
    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1065251
    move-object v0, v0

    .line 1065252
    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1065253
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1065254
    if-eqz p1, :cond_0

    .line 1065255
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "save_autofill_data_dialog_fragment"

    invoke-virtual {v0, p1, v1}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataActivity;->p:Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;

    .line 1065256
    :goto_0
    iget-object v0, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataActivity;->p:Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "save_autofill_data_dialog_fragment"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1065257
    iget-object v0, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataActivity;->p:Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;

    iget-object v1, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataActivity;->q:LX/6DW;

    .line 1065258
    iput-object v1, v0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->n:LX/6DW;

    .line 1065259
    return-void

    .line 1065260
    :cond_0
    invoke-direct {p0}, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataActivity;->a()Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataActivity;->p:Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 1065261
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataActivity;->finish()V

    .line 1065262
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1065263
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1065264
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "save_autofill_data_dialog_fragment"

    iget-object v2, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataActivity;->p:Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;

    invoke-virtual {v0, p1, v1, v2}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 1065265
    return-void
.end method
