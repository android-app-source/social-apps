.class public Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Landroid/app/Dialog;

.field public n:LX/6DW;

.field public o:Landroid/widget/LinearLayout;

.field public p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;",
            ">;"
        }
    .end annotation
.end field

.field public q:Ljava/lang/String;

.field public r:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

.field public s:LX/6DO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/6D0;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:LX/6Cz;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1065307
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1065308
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    .line 1065291
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e07f8

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->m:Landroid/app/Dialog;

    .line 1065292
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03124d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1065293
    iget-object v0, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->p:Ljava/util/ArrayList;

    .line 1065294
    const v2, 0x7f0d2b0b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 1065295
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 1065296
    new-instance v5, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1065297
    const-string v6, "%s %s"

    const p1, 0x7f081d5d

    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, p1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1065298
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1065299
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 1065300
    :cond_0
    const v0, 0x7f0d29ca

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1065301
    new-instance v2, LX/6DX;

    invoke-direct {v2, p0}, LX/6DX;-><init>(Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1065302
    const v0, 0x7f0d29c9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1065303
    new-instance v2, LX/6DY;

    invoke-direct {v2, p0}, LX/6DY;-><init>(Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1065304
    const v0, 0x7f0d2b09

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->o:Landroid/widget/LinearLayout;

    .line 1065305
    iget-object v0, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->m:Landroid/app/Dialog;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 1065306
    iget-object v0, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->m:Landroid/app/Dialog;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6d829195

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1065313
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1065314
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1065315
    const-string v2, "app_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->q:Ljava/lang/String;

    .line 1065316
    const-string v2, "autofill_data"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->p:Ljava/util/ArrayList;

    .line 1065317
    const-string v2, "js_bridge_call"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->r:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    .line 1065318
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;

    invoke-static {p1}, LX/6DO;->a(LX/0QB;)LX/6DO;

    move-result-object v4

    check-cast v4, LX/6DO;

    const-class v0, LX/6D0;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/6D0;

    iput-object v4, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->s:LX/6DO;

    iput-object p1, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->t:LX/6D0;

    .line 1065319
    const/16 v0, 0x2b

    const v2, -0x4c8b497e

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 1065309
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1065310
    iget-object p1, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->n:LX/6DW;

    if-eqz p1, :cond_0

    .line 1065311
    iget-object p1, p0, Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;->n:LX/6DW;

    invoke-virtual {p1}, LX/6DW;->a()V

    .line 1065312
    :cond_0
    return-void
.end method
