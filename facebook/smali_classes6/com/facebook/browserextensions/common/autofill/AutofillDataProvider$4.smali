.class public final Lcom/facebook/browserextensions/common/autofill/AutofillDataProvider$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6DJ;

.field public final synthetic b:LX/6DM;


# direct methods
.method public constructor <init>(LX/6DM;LX/6DJ;)V
    .locals 0

    .prologue
    .line 1064848
    iput-object p1, p0, Lcom/facebook/browserextensions/common/autofill/AutofillDataProvider$4;->b:LX/6DM;

    iput-object p2, p0, Lcom/facebook/browserextensions/common/autofill/AutofillDataProvider$4;->a:LX/6DJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1064849
    iget-object v0, p0, Lcom/facebook/browserextensions/common/autofill/AutofillDataProvider$4;->b:LX/6DM;

    iget-object v0, v0, LX/6DM;->d:LX/6DR;

    iget-object v1, p0, Lcom/facebook/browserextensions/common/autofill/AutofillDataProvider$4;->a:LX/6DJ;

    .line 1064850
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1064851
    iget-object v2, v0, LX/6DR;->b:LX/1Ck;

    const-string v3, "fetch_auto_fill_data"

    invoke-static {v0}, LX/6DR;->b(LX/6DR;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance p0, LX/6DQ;

    invoke-direct {p0, v0, v1}, LX/6DQ;-><init>(LX/6DR;LX/6DJ;)V

    invoke-virtual {v2, v3, v4, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1064852
    return-void
.end method
