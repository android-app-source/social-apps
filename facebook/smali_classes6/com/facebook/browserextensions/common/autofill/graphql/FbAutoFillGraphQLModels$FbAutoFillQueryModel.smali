.class public final Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4a7065bb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1065597
    const-class v0, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1065598
    const-class v0, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1065599
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1065600
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1065601
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1065602
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;->a()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 1065603
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1065604
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1065605
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1065606
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1065607
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1065608
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;->a()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1065609
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;->a()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1065610
    if-eqz v1, :cond_0

    .line 1065611
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;

    .line 1065612
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;->e:LX/3Sb;

    .line 1065613
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1065614
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllAutofillValues"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1065615
    iget-object v0, p0, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, -0x1ad8c9f2

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;->e:LX/3Sb;

    .line 1065616
    iget-object v0, p0, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1065617
    new-instance v0, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;

    invoke-direct {v0}, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;-><init>()V

    .line 1065618
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1065619
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1065620
    const v0, 0x353b7ebf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1065621
    const v0, 0x2f848909

    return v0
.end method
