.class public final Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1065595
    const-class v0, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;

    new-instance v1, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1065596
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1065562
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1065564
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1065565
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1065566
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1065567
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1065568
    if-eqz v2, :cond_5

    .line 1065569
    const-string v3, "all_autofill_values"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1065570
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1065571
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 1065572
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    const/4 p2, 0x3

    .line 1065573
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1065574
    const/4 p0, 0x0

    invoke-virtual {v1, v4, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1065575
    if-eqz p0, :cond_0

    .line 1065576
    const-string v0, "autocomplete_tag"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1065577
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1065578
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v4, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1065579
    if-eqz p0, :cond_1

    .line 1065580
    const-string v0, "field_label"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1065581
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1065582
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v4, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1065583
    if-eqz p0, :cond_2

    .line 1065584
    const-string v0, "field_name"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1065585
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1065586
    :cond_2
    invoke-virtual {v1, v4, p2}, LX/15i;->g(II)I

    move-result p0

    .line 1065587
    if-eqz p0, :cond_3

    .line 1065588
    const-string p0, "values"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1065589
    invoke-virtual {v1, v4, p2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1065590
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1065591
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1065592
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1065593
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1065594
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1065563
    check-cast p1, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel$Serializer;->a(Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
