.class public final Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1065508
    const-class v0, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;

    new-instance v1, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1065509
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1065510
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1065511
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1065512
    const/4 v2, 0x0

    .line 1065513
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1065514
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1065515
    :goto_0
    move v1, v2

    .line 1065516
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1065517
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1065518
    new-instance v1, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;

    invoke-direct {v1}, Lcom/facebook/browserextensions/common/autofill/graphql/FbAutoFillGraphQLModels$FbAutoFillQueryModel;-><init>()V

    .line 1065519
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1065520
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1065521
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1065522
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1065523
    :cond_0
    return-object v1

    .line 1065524
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1065525
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1065526
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1065527
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1065528
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1065529
    const-string v4, "all_autofill_values"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1065530
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1065531
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_3

    .line 1065532
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1065533
    const/4 v4, 0x0

    .line 1065534
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_c

    .line 1065535
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1065536
    :goto_3
    move v3, v4

    .line 1065537
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1065538
    :cond_3
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1065539
    goto :goto_1

    .line 1065540
    :cond_4
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1065541
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1065542
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_5
    move v1, v2

    goto :goto_1

    .line 1065543
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1065544
    :cond_7
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_b

    .line 1065545
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1065546
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1065547
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_7

    if-eqz v8, :cond_7

    .line 1065548
    const-string p0, "autocomplete_tag"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1065549
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_4

    .line 1065550
    :cond_8
    const-string p0, "field_label"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1065551
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 1065552
    :cond_9
    const-string p0, "field_name"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1065553
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_4

    .line 1065554
    :cond_a
    const-string p0, "values"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1065555
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_4

    .line 1065556
    :cond_b
    const/4 v8, 0x4

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1065557
    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 1065558
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 1065559
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1065560
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1065561
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto/16 :goto_3

    :cond_c
    move v3, v4

    move v5, v4

    move v6, v4

    move v7, v4

    goto :goto_4
.end method
