.class public final Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/6Ez;


# direct methods
.method public constructor <init>(LX/6Ez;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1066891
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;->c:LX/6Ez;

    iput-boolean p2, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;->a:Z

    iput-object p3, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1066892
    iget-boolean v0, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;->a:Z

    if-eqz v0, :cond_0

    .line 1066893
    new-instance v0, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;

    iget-object v1, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;->c:LX/6Ez;

    iget-object v1, v1, LX/6Ez;->h:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;-><init>(Ljava/lang/String;LX/0lF;)V

    .line 1066894
    iget-object v1, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;->c:LX/6Ez;

    iget-object v1, v1, LX/6Ez;->g:LX/6Ex;

    invoke-interface {v1, v0}, LX/6Ex;->a(Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V

    .line 1066895
    :goto_0
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;->c:LX/6Ez;

    const/4 v2, 0x0

    .line 1066896
    iput-object v2, v0, LX/6Ez;->h:Ljava/lang/String;

    .line 1066897
    iget-object v1, v0, LX/6Ez;->a:LX/6F6;

    .line 1066898
    iput-object v2, v1, LX/6F6;->c:LX/6Ez;

    .line 1066899
    return-void

    .line 1066900
    :cond_0
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;->b:Ljava/lang/String;

    .line 1066901
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1066902
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;->c:LX/6Ez;

    iget-object v0, v0, LX/6Ez;->c:Landroid/content/Context;

    const v1, 0x7f081d80

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;->c:LX/6Ez;

    iget-object v4, v4, LX/6Ez;->c:Landroid/content/Context;

    iget-object v5, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;->c:LX/6Ez;

    iget-object v5, v5, LX/6Ez;->f:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-static {v4, v5}, LX/6Ez;->a(Landroid/content/Context;Lcom/facebook/payments/checkout/model/CheckoutData;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1066903
    :cond_1
    iget-object v1, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutSender$2;->c:LX/6Ez;

    invoke-static {v1, v0}, LX/6Ez;->b$redex0(LX/6Ez;Ljava/lang/String;)V

    goto :goto_0
.end method
