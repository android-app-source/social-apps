.class public final Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1067246
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1067247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067248
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;->a:Ljava/lang/String;

    .line 1067249
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;->b:Ljava/lang/String;

    .line 1067250
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;
    .locals 2

    .prologue
    .line 1067251
    new-instance v0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    invoke-direct {v0, p0}, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;-><init>(Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;)V

    return-object v0
.end method

.method public setAmount(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "amount"
    .end annotation

    .prologue
    .line 1067252
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;->a:Ljava/lang/String;

    .line 1067253
    return-object p0
.end method

.method public setCurrency(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "currency"
    .end annotation

    .prologue
    .line 1067254
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;->b:Ljava/lang/String;

    .line 1067255
    return-object p0
.end method
