.class public final Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/browserextensions/common/payments/model/OrderInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1067334
    new-instance v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Deserializer;->a:Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1067335
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1067336
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/browserextensions/common/payments/model/OrderInfo;
    .locals 1

    .prologue
    .line 1067337
    sget-object v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Deserializer;->a:Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;

    .line 1067338
    invoke-virtual {v0}, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;->a()Lcom/facebook/browserextensions/common/payments/model/OrderInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1067339
    invoke-static {p1, p2}, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/browserextensions/common/payments/model/OrderInfo;

    move-result-object v0

    return-object v0
.end method
