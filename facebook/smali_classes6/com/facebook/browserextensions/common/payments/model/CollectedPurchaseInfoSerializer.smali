.class public Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1067202
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;

    new-instance v1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1067203
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1067204
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1067205
    if-nez p0, :cond_0

    .line 1067206
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1067207
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1067208
    invoke-static {p0, p1, p2}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfoSerializer;->b(Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;LX/0nX;LX/0my;)V

    .line 1067209
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1067210
    return-void
.end method

.method private static b(Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1067211
    const-string v0, "contact_email"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->getContactEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067212
    const-string v0, "contact_name"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->getContactName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067213
    const-string v0, "contact_phone"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->getContactPhone()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067214
    const-string v0, "shipping_address"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->getShippingAddress()Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1067215
    const-string v0, "shipping_option"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->getShippingOption()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067216
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1067217
    check-cast p1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfoSerializer;->a(Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;LX/0nX;LX/0my;)V

    return-void
.end method
