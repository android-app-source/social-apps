.class public final Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1067328
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1067329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067330
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/browserextensions/common/payments/model/OrderInfo;
    .locals 2

    .prologue
    .line 1067331
    new-instance v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;

    invoke-direct {v0, p0}, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;-><init>(Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;)V

    return-object v0
.end method

.method public setTotal_currency_amount(Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;)Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;
    .locals 0
    .param p1    # Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "total_currency_amount"
    .end annotation

    .prologue
    .line 1067332
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;->a:Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    .line 1067333
    return-object p0
.end method
