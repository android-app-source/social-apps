.class public Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/common/json/FbJsonField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1067379
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;

    new-instance v1, Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;

    invoke-direct {v1}, Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1067380
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1067397
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 1067398
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;

    invoke-virtual {p0, v0}, Lcom/facebook/common/json/FbJsonDeserializer;->init(Ljava/lang/Class;)V

    .line 1067399
    return-void
.end method


# virtual methods
.method public final getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1067381
    const-class v2, Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;

    monitor-enter v2

    .line 1067382
    :try_start_0
    sget-object v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;->a:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 1067383
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;->a:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1067384
    :cond_0
    const/4 v0, -0x1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_1
    :goto_0
    packed-switch v0, :pswitch_data_1

    .line 1067385
    invoke-super {p0, p1}, Lcom/facebook/common/json/FbJsonDeserializer;->getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v2

    .line 1067386
    :goto_1
    return-object v0

    .line 1067387
    :cond_2
    sget-object v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/json/FbJsonField;

    .line 1067388
    if-eqz v0, :cond_0

    .line 1067389
    monitor-exit v2

    goto :goto_1

    .line 1067390
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1067391
    :pswitch_0
    :try_start_3
    const-string v3, "total_currency_amount"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    goto :goto_0

    .line 1067392
    :pswitch_1
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;

    const-string v1, "setTotal_currency_amount"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 1067393
    sget-object v1, Lcom/facebook/browserextensions/common/payments/model/OrderInfo_BuilderDeserializer;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1067394
    monitor-exit v2

    goto :goto_1

    .line 1067395
    :catch_0
    move-exception v0

    .line 1067396
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :pswitch_data_0
    .packed-switch -0x61ba7eb5
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
