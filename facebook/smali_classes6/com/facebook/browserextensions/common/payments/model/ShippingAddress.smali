.class public Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/browserextensions/common/payments/model/ShippingAddressSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1067491
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1067490
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddressSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1067489
    new-instance v0, LX/6FC;

    invoke-direct {v0}, LX/6FC;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1067480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067481
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->a:Ljava/lang/String;

    .line 1067482
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->b:Ljava/lang/String;

    .line 1067483
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->c:Ljava/lang/String;

    .line 1067484
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->d:Ljava/lang/String;

    .line 1067485
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->e:Ljava/lang/String;

    .line 1067486
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->f:Ljava/lang/String;

    .line 1067487
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->g:Ljava/lang/String;

    .line 1067488
    return-void
.end method

.method public constructor <init>(Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;)V
    .locals 1

    .prologue
    .line 1067471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067472
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->a:Ljava/lang/String;

    .line 1067473
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->b:Ljava/lang/String;

    .line 1067474
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->c:Ljava/lang/String;

    .line 1067475
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->d:Ljava/lang/String;

    .line 1067476
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->e:Ljava/lang/String;

    .line 1067477
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->f:Ljava/lang/String;

    .line 1067478
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->g:Ljava/lang/String;

    .line 1067479
    return-void
.end method

.method public static newBuilder()Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;
    .locals 2

    .prologue
    .line 1067470
    new-instance v0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;

    invoke-direct {v0}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1067469
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1067434
    if-ne p0, p1, :cond_1

    .line 1067435
    :cond_0
    :goto_0
    return v0

    .line 1067436
    :cond_1
    instance-of v2, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    if-nez v2, :cond_2

    move v0, v1

    .line 1067437
    goto :goto_0

    .line 1067438
    :cond_2
    check-cast p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    .line 1067439
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1067440
    goto :goto_0

    .line 1067441
    :cond_3
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1067442
    goto :goto_0

    .line 1067443
    :cond_4
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1067444
    goto :goto_0

    .line 1067445
    :cond_5
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1067446
    goto :goto_0

    .line 1067447
    :cond_6
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1067448
    goto :goto_0

    .line 1067449
    :cond_7
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1067450
    goto :goto_0

    .line 1067451
    :cond_8
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1067452
    goto :goto_0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "city"
    .end annotation

    .prologue
    .line 1067468
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getCountry()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "country"
    .end annotation

    .prologue
    .line 1067467
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation

    .prologue
    .line 1067466
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "postal_code"
    .end annotation

    .prologue
    .line 1067465
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getRegion()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "region"
    .end annotation

    .prologue
    .line 1067464
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getStreet1()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "street1"
    .end annotation

    .prologue
    .line 1067463
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getStreet2()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "street2"
    .end annotation

    .prologue
    .line 1067462
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1067461
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1067453
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067454
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067455
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067456
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067457
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067458
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067459
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067460
    return-void
.end method
