.class public final Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browserextensions/common/payments/model/ShippingAddress_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1067424
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1067403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067404
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->a:Ljava/lang/String;

    .line 1067405
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->b:Ljava/lang/String;

    .line 1067406
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->c:Ljava/lang/String;

    .line 1067407
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->d:Ljava/lang/String;

    .line 1067408
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->e:Ljava/lang/String;

    .line 1067409
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->f:Ljava/lang/String;

    .line 1067410
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->g:Ljava/lang/String;

    .line 1067411
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;
    .locals 2

    .prologue
    .line 1067427
    new-instance v0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    invoke-direct {v0, p0}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;-><init>(Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;)V

    return-object v0
.end method

.method public setCity(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "city"
    .end annotation

    .prologue
    .line 1067425
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->a:Ljava/lang/String;

    .line 1067426
    return-object p0
.end method

.method public setCountry(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "country"
    .end annotation

    .prologue
    .line 1067420
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->b:Ljava/lang/String;

    .line 1067421
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation

    .prologue
    .line 1067422
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->c:Ljava/lang/String;

    .line 1067423
    return-object p0
.end method

.method public setPostalCode(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "postal_code"
    .end annotation

    .prologue
    .line 1067418
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->d:Ljava/lang/String;

    .line 1067419
    return-object p0
.end method

.method public setRegion(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "region"
    .end annotation

    .prologue
    .line 1067416
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->e:Ljava/lang/String;

    .line 1067417
    return-object p0
.end method

.method public setStreet1(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "street1"
    .end annotation

    .prologue
    .line 1067414
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->f:Ljava/lang/String;

    .line 1067415
    return-object p0
.end method

.method public setStreet2(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "street2"
    .end annotation

    .prologue
    .line 1067412
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress$Builder;->g:Ljava/lang/String;

    .line 1067413
    return-object p0
.end method
