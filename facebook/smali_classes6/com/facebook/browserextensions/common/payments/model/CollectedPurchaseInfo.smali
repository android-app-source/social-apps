.class public Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfoSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1067193
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1067174
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfoSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1067175
    new-instance v0, LX/6F9;

    invoke-direct {v0}, LX/6F9;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1067176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067177
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1067178
    iput-object v1, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->a:Ljava/lang/String;

    .line 1067179
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1067180
    iput-object v1, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->b:Ljava/lang/String;

    .line 1067181
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1067182
    iput-object v1, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->c:Ljava/lang/String;

    .line 1067183
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 1067184
    iput-object v1, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->d:Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    .line 1067185
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 1067186
    iput-object v1, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->e:Ljava/lang/String;

    .line 1067187
    :goto_4
    return-void

    .line 1067188
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->a:Ljava/lang/String;

    goto :goto_0

    .line 1067189
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->b:Ljava/lang/String;

    goto :goto_1

    .line 1067190
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->c:Ljava/lang/String;

    goto :goto_2

    .line 1067191
    :cond_3
    sget-object v0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->d:Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    goto :goto_3

    .line 1067192
    :cond_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->e:Ljava/lang/String;

    goto :goto_4
.end method

.method public constructor <init>(Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;)V
    .locals 1

    .prologue
    .line 1067194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067195
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->a:Ljava/lang/String;

    .line 1067196
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->b:Ljava/lang/String;

    .line 1067197
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->c:Ljava/lang/String;

    .line 1067198
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->d:Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->d:Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    .line 1067199
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->e:Ljava/lang/String;

    .line 1067200
    return-void
.end method

.method public static newBuilder()Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;
    .locals 2

    .prologue
    .line 1067201
    new-instance v0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;

    invoke-direct {v0}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1067158
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1067159
    if-ne p0, p1, :cond_1

    .line 1067160
    :cond_0
    :goto_0
    return v0

    .line 1067161
    :cond_1
    instance-of v2, p1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;

    if-nez v2, :cond_2

    move v0, v1

    .line 1067162
    goto :goto_0

    .line 1067163
    :cond_2
    check-cast p1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;

    .line 1067164
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1067165
    goto :goto_0

    .line 1067166
    :cond_3
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1067167
    goto :goto_0

    .line 1067168
    :cond_4
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1067169
    goto :goto_0

    .line 1067170
    :cond_5
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->d:Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->d:Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1067171
    goto :goto_0

    .line 1067172
    :cond_6
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1067173
    goto :goto_0
.end method

.method public getContactEmail()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "contact_email"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1067157
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getContactName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "contact_name"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1067156
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getContactPhone()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "contact_phone"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1067155
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getShippingAddress()Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shipping_address"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1067131
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->d:Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    return-object v0
.end method

.method public getShippingOption()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shipping_option"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1067154
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1067153
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->d:Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1067132
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1067133
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067134
    :goto_0
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1067135
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067136
    :goto_1
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1067137
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067138
    :goto_2
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->d:Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    if-nez v0, :cond_3

    .line 1067139
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067140
    :goto_3
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 1067141
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067142
    :goto_4
    return-void

    .line 1067143
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067144
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 1067145
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067146
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 1067147
    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067148
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 1067149
    :cond_3
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067150
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->d:Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_3

    .line 1067151
    :cond_4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067152
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_4
.end method
