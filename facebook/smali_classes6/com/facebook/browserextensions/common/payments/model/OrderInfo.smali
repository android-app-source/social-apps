.class public Lcom/facebook/browserextensions/common/payments/model/OrderInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/browserextensions/common/payments/model/OrderInfoSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/common/payments/model/OrderInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1067365
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1067340
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfoSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1067364
    new-instance v0, LX/6FB;

    invoke-direct {v0}, LX/6FB;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1067359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067360
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1067361
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;->a:Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    .line 1067362
    :goto_0
    return-void

    .line 1067363
    :cond_0
    sget-object v0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;->a:Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;)V
    .locals 1

    .prologue
    .line 1067356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067357
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;->a:Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;->a:Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    .line 1067358
    return-void
.end method

.method public static newBuilder()Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;
    .locals 2

    .prologue
    .line 1067366
    new-instance v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;

    invoke-direct {v0}, Lcom/facebook/browserextensions/common/payments/model/OrderInfo$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1067355
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1067348
    if-ne p0, p1, :cond_1

    .line 1067349
    :cond_0
    :goto_0
    return v0

    .line 1067350
    :cond_1
    instance-of v2, p1, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;

    if-nez v2, :cond_2

    move v0, v1

    .line 1067351
    goto :goto_0

    .line 1067352
    :cond_2
    check-cast p1, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;

    .line 1067353
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;->a:Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;->a:Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1067354
    goto :goto_0
.end method

.method public getCurrencyAmount()Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "total_currency_amount"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1067347
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;->a:Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1067346
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;->a:Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1067341
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;->a:Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    if-nez v0, :cond_0

    .line 1067342
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067343
    :goto_0
    return-void

    .line 1067344
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067345
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;->a:Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0
.end method
