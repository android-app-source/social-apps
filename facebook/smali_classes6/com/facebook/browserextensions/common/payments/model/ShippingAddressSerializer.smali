.class public Lcom/facebook/browserextensions/common/payments/model/ShippingAddressSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1067502
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    new-instance v1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddressSerializer;

    invoke-direct {v1}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddressSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1067503
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1067501
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1067504
    if-nez p0, :cond_0

    .line 1067505
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1067506
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1067507
    invoke-static {p0, p1, p2}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddressSerializer;->b(Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;LX/0nX;LX/0my;)V

    .line 1067508
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1067509
    return-void
.end method

.method private static b(Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1067493
    const-string v0, "city"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->getCity()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067494
    const-string v0, "country"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067495
    const-string v0, "name"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067496
    const-string v0, "postal_code"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->getPostalCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067497
    const-string v0, "region"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->getRegion()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067498
    const-string v0, "street1"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->getStreet1()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067499
    const-string v0, "street2"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;->getStreet2()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067500
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1067492
    check-cast p1, Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    invoke-static {p1, p2, p3}, Lcom/facebook/browserextensions/common/payments/model/ShippingAddressSerializer;->a(Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;LX/0nX;LX/0my;)V

    return-void
.end method
