.class public Lcom/facebook/browserextensions/common/payments/model/CurrencyAmountSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1067290
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    new-instance v1, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmountSerializer;

    invoke-direct {v1}, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmountSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1067291
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1067292
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1067293
    if-nez p0, :cond_0

    .line 1067294
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1067295
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1067296
    invoke-static {p0, p1, p2}, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmountSerializer;->b(Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;LX/0nX;LX/0my;)V

    .line 1067297
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1067298
    return-void
.end method

.method private static b(Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1067299
    const-string v0, "amount"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->getAmount()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067300
    const-string v0, "currency"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->getCurrency()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067301
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1067302
    check-cast p1, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    invoke-static {p1, p2, p3}, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmountSerializer;->a(Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;LX/0nX;LX/0my;)V

    return-void
.end method
