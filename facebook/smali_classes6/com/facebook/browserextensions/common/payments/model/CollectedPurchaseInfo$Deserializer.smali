.class public final Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1067125
    new-instance v0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Deserializer;->a:Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1067126
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1067127
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;
    .locals 1

    .prologue
    .line 1067128
    sget-object v0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Deserializer;->a:Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;

    .line 1067129
    invoke-virtual {v0}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->a()Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1067130
    invoke-static {p1, p2}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;

    move-result-object v0

    return-object v0
.end method
