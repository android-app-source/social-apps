.class public final Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1067124
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1067122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067123
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;
    .locals 2

    .prologue
    .line 1067121
    new-instance v0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;

    invoke-direct {v0, p0}, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo;-><init>(Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;)V

    return-object v0
.end method

.method public setContactEmail(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "contact_email"
    .end annotation

    .prologue
    .line 1067119
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->a:Ljava/lang/String;

    .line 1067120
    return-object p0
.end method

.method public setContactName(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "contact_name"
    .end annotation

    .prologue
    .line 1067117
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->b:Ljava/lang/String;

    .line 1067118
    return-object p0
.end method

.method public setContactPhone(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "contact_phone"
    .end annotation

    .prologue
    .line 1067115
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->c:Ljava/lang/String;

    .line 1067116
    return-object p0
.end method

.method public setShippingAddress(Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;)Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;
    .locals 0
    .param p1    # Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shipping_address"
    .end annotation

    .prologue
    .line 1067111
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->d:Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    .line 1067112
    return-object p0
.end method

.method public setShippingOption(Ljava/lang/String;)Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shipping_option"
    .end annotation

    .prologue
    .line 1067113
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/model/CollectedPurchaseInfo$Builder;->e:Ljava/lang/String;

    .line 1067114
    return-object p0
.end method
