.class public Lcom/facebook/browserextensions/common/payments/model/OrderInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/browserextensions/common/payments/model/OrderInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1067367
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;

    new-instance v1, Lcom/facebook/browserextensions/common/payments/model/OrderInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/browserextensions/common/payments/model/OrderInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1067368
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1067369
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/browserextensions/common/payments/model/OrderInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1067370
    if-nez p0, :cond_0

    .line 1067371
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1067372
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1067373
    invoke-static {p0, p1, p2}, Lcom/facebook/browserextensions/common/payments/model/OrderInfoSerializer;->b(Lcom/facebook/browserextensions/common/payments/model/OrderInfo;LX/0nX;LX/0my;)V

    .line 1067374
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1067375
    return-void
.end method

.method private static b(Lcom/facebook/browserextensions/common/payments/model/OrderInfo;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1067376
    const-string v0, "total_currency_amount"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;->getCurrencyAmount()Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1067377
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1067378
    check-cast p1, Lcom/facebook/browserextensions/common/payments/model/OrderInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/browserextensions/common/payments/model/OrderInfoSerializer;->a(Lcom/facebook/browserextensions/common/payments/model/OrderInfo;LX/0nX;LX/0my;)V

    return-void
.end method
