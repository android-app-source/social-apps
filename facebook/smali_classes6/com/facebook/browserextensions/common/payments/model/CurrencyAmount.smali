.class public Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/browserextensions/common/payments/model/CurrencyAmountSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1067288
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1067287
    const-class v0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmountSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1067266
    new-instance v0, LX/6FA;

    invoke-direct {v0}, LX/6FA;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1067283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067284
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->a:Ljava/lang/String;

    .line 1067285
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->b:Ljava/lang/String;

    .line 1067286
    return-void
.end method

.method public constructor <init>(Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;)V
    .locals 1

    .prologue
    .line 1067279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067280
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->a:Ljava/lang/String;

    .line 1067281
    iget-object v0, p1, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->b:Ljava/lang/String;

    .line 1067282
    return-void
.end method

.method public static newBuilder()Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;
    .locals 2

    .prologue
    .line 1067278
    new-instance v0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;

    invoke-direct {v0}, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1067289
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1067269
    if-ne p0, p1, :cond_1

    .line 1067270
    :cond_0
    :goto_0
    return v0

    .line 1067271
    :cond_1
    instance-of v2, p1, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    if-nez v2, :cond_2

    move v0, v1

    .line 1067272
    goto :goto_0

    .line 1067273
    :cond_2
    check-cast p1, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;

    .line 1067274
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1067275
    goto :goto_0

    .line 1067276
    :cond_3
    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1067277
    goto :goto_0
.end method

.method public getAmount()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "amount"
    .end annotation

    .prologue
    .line 1067268
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrency()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "currency"
    .end annotation

    .prologue
    .line 1067267
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1067265
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1067262
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067263
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/model/CurrencyAmount;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067264
    return-void
.end method
