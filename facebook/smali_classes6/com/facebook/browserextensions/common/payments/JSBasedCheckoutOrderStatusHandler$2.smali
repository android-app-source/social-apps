.class public final Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/6Ew;


# direct methods
.method public constructor <init>(LX/6Ew;Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1066765
    iput-object p1, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$2;->c:LX/6Ew;

    iput-object p2, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$2;->a:Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    iput-object p3, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$2;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1066766
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$2;->a:Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    if-nez v0, :cond_0

    .line 1066767
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$2;->c:LX/6Ew;

    iget-object v1, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$2;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/6Ew;->a$redex0(LX/6Ew;Ljava/lang/String;)V

    .line 1066768
    :goto_0
    return-void

    .line 1066769
    :cond_0
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$2;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1066770
    iget-object v0, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$2;->c:LX/6Ew;

    iget-object v1, p0, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$2;->a:Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    .line 1066771
    iget-object v2, v0, LX/6Ew;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1066772
    iget-object v2, v0, LX/6Ew;->b:LX/6r9;

    iget-object v3, v0, LX/6Ew;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v3}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v2, v3}, LX/6r9;->b(LX/6qw;)LX/6qd;

    move-result-object v2

    iput-object v2, v0, LX/6Ew;->j:LX/6qd;

    .line 1066773
    iget-object v2, v0, LX/6Ew;->j:LX/6qd;

    iget-object v3, v0, LX/6Ew;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    iget-object p0, v0, LX/6Ew;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object p0

    invoke-virtual {p0, v1}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;)Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object p0

    invoke-interface {v2, v3, p0}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    .line 1066774
    goto :goto_0
.end method
