.class public Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;
.super LX/6E7;
.source ""


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1065820
    const-string v0, "https://www.facebook.com/about/privacy"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1065821
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1065822
    invoke-direct {p0}, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->a()V

    .line 1065823
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1065824
    invoke-direct {p0, p1, p2}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1065825
    invoke-direct {p0}, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->a()V

    .line 1065826
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1065827
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1065828
    invoke-direct {p0}, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->a()V

    .line 1065829
    return-void
.end method

.method private a(Landroid/net/Uri;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 1065830
    new-instance v0, LX/6E5;

    invoke-direct {v0, p0, p1}, LX/6E5;-><init>(Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;Landroid/net/Uri;)V

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1065831
    const v0, 0x7f0301df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1065832
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->setOrientation(I)V

    .line 1065833
    const v0, 0x7f0d079a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1065834
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v5, 0x21

    .line 1065835
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081d57

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 1065836
    :cond_0
    new-instance v0, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081d59

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    const-string v1, "[[fb_policies]]"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081d58

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->a:Landroid/net/Uri;

    invoke-direct {p0, v3}, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->a(Landroid/net/Uri;)Landroid/text/style/ClickableSpan;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3, v5}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v0

    const-string v1, "[[page_policies]]"

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081d58

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p2}, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->a(Landroid/net/Uri;)Landroid/text/style/ClickableSpan;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3, v5}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v0

    .line 1065837
    iget-object v1, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1065838
    iget-object v1, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsTermsAndPoliciesView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1065839
    return-void
.end method
