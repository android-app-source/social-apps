.class public Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/checkout/CheckoutParams;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1065645
    new-instance v0, LX/6Dm;

    invoke-direct {v0}, LX/6Dm;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6Dn;)V
    .locals 1

    .prologue
    .line 1065646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065647
    iget-object v0, p1, LX/6Dn;->a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    .line 1065648
    iget-object v0, p1, LX/6Dn;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->b:Ljava/lang/String;

    .line 1065649
    iget-object v0, p1, LX/6Dn;->c:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->c:Landroid/net/Uri;

    .line 1065650
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1065651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065652
    const-class v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    .line 1065653
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->b:Ljava/lang/String;

    .line 1065654
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->c:Landroid/net/Uri;

    .line 1065655
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/checkout/CheckoutCommonParams;
    .locals 1

    .prologue
    .line 1065656
    iget-object v0, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)Lcom/facebook/payments/checkout/CheckoutParams;
    .locals 2

    .prologue
    .line 1065657
    new-instance v0, LX/6Dn;

    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6Dn;-><init>(Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    iget-object v1, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->b:Ljava/lang/String;

    .line 1065658
    iput-object v1, v0, LX/6Dn;->b:Ljava/lang/String;

    .line 1065659
    move-object v0, v0

    .line 1065660
    iget-object v1, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->c:Landroid/net/Uri;

    .line 1065661
    iput-object v1, v0, LX/6Dn;->c:Landroid/net/Uri;

    .line 1065662
    move-object v0, v0

    .line 1065663
    move-object v0, v0

    .line 1065664
    iput-object p1, v0, LX/6Dn;->a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    .line 1065665
    move-object v0, v0

    .line 1065666
    invoke-virtual {v0}, LX/6Dn;->a()Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1065667
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1065668
    iget-object v0, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1065669
    iget-object v0, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1065670
    iget-object v0, p0, Lcom/facebook/browserextensions/common/checkout/BrowserExtensionsCheckoutParams;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1065671
    return-void
.end method
