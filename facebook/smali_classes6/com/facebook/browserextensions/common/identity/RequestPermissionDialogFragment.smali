.class public abstract Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public p:LX/6EP;

.field public q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public s:Landroid/widget/LinearLayout;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:LX/6ET;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1066073
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1066074
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1066075
    new-instance v0, LX/6ES;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e07f8

    invoke-direct {v0, p0, v1, v2}, LX/6ES;-><init>(Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;Landroid/content/Context;I)V

    return-object v0
.end method

.method public abstract a()V
.end method

.method public a(Landroid/view/View;Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1066063
    const v0, 0x7f0d29c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1066064
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1066065
    new-instance v4, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1066066
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/6D6;->valueOf(Ljava/lang/String;)LX/6D6;

    move-result-object v1

    .line 1066067
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v1, v5}, LX/6D6;->permissionDisplayName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1066068
    const-string v5, "%s  %s"

    const v6, 0x7f081d5d

    invoke-virtual {p0, v6}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1066069
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1066070
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1066071
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1066072
    :cond_0
    return-void
.end method

.method public abstract b()V
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1066048
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->p:LX/6EP;

    if-eqz v0, :cond_0

    .line 1066049
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->p:LX/6EP;

    .line 1066050
    iget-object p0, v0, LX/6EP;->a:Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;

    invoke-static {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->l(Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;)V

    .line 1066051
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x22b977bb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1066055
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1066056
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1066057
    const-string v2, "app_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->t:Ljava/lang/String;

    .line 1066058
    const-string v2, "app_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->u:Ljava/lang/String;

    .line 1066059
    const-string v2, "permission"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->q:Ljava/util/ArrayList;

    .line 1066060
    const-string v2, "calling_url"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->v:Ljava/lang/String;

    .line 1066061
    const-string v2, "request_source"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/6ET;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->w:LX/6ET;

    .line 1066062
    const/16 v0, 0x2b

    const v2, -0x565db04f

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 1066052
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1066053
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->l()V

    .line 1066054
    return-void
.end method
