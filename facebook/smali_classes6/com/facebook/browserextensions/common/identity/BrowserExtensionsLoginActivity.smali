.class public Lcom/facebook/browserextensions/common/identity/BrowserExtensionsLoginActivity;
.super Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;
.source ""


# instance fields
.field public p:LX/6EW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1065881
    invoke-direct {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/browserextensions/common/identity/BrowserExtensionsLoginActivity;

    invoke-static {v0}, LX/6EW;->a(LX/0QB;)LX/6EW;

    move-result-object v0

    check-cast v0, LX/6EW;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/identity/BrowserExtensionsLoginActivity;->p:LX/6EW;

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;
    .locals 2

    .prologue
    .line 1065871
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/identity/BrowserExtensionsLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1065872
    new-instance v1, LX/6EJ;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {v1, v0}, LX/6EJ;-><init>(Landroid/os/Bundle;)V

    .line 1065873
    new-instance v0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    invoke-direct {v0}, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;-><init>()V

    .line 1065874
    invoke-virtual {v1}, LX/6EI;->b()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1065875
    move-object v0, v0

    .line 1065876
    return-object v0
.end method

.method public final b()LX/6EB;
    .locals 1

    .prologue
    .line 1065880
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/BrowserExtensionsLoginActivity;->p:LX/6EW;

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1065877
    invoke-super {p0, p1}, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->b(Landroid/os/Bundle;)V

    .line 1065878
    invoke-static {p0, p0}, Lcom/facebook/browserextensions/common/identity/BrowserExtensionsLoginActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1065879
    return-void
.end method
