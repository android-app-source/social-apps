.class public Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;
.super Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final A:Landroid/net/Uri;

.field public static final x:Ljava/lang/String;


# instance fields
.field public B:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public C:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/6EF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public z:Landroid/support/v7/widget/CardView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1066076
    const-class v0, Lcom/facebook/browserextensions/common/identity/RequestUserInfoFieldDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->x:Ljava/lang/String;

    .line 1066077
    const-string v0, "https://m.facebook.com/help/203805466323736"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->A:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1066110
    invoke-direct {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;-><init>()V

    .line 1066111
    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1066105
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->z:Landroid/support/v7/widget/CardView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/CardView;->setRadius(F)V

    .line 1066106
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->z:Landroid/support/v7/widget/CardView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->setUseCompatPadding(Z)V

    .line 1066107
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->z:Landroid/support/v7/widget/CardView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/CardView;->setCardElevation(F)V

    .line 1066108
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->s:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1066109
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1066104
    new-instance v0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e07f8

    invoke-direct {v0, p0, v1, v2}, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;-><init>(Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;Landroid/content/Context;I)V

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1066112
    invoke-direct {p0}, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->m()V

    .line 1066113
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1066114
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1066115
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->l()V

    .line 1066116
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1066086
    const v0, 0x7f0d1a56

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1066087
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1066088
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f081d62

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->u:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1066089
    sget-object v3, LX/6D6;->PUBLIC_PROFILE:LX/6D6;

    invoke-virtual {v3}, LX/6D6;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1066090
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1066091
    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1066092
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/6D6;->valueOf(Ljava/lang/String;)LX/6D6;

    move-result-object v1

    .line 1066093
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f081d61

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->u:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/6D6;->loginPermissionDisplayName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1066094
    :cond_0
    new-instance v3, LX/47x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v3, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v1

    const-string v3, "[[fb_profile_policies]]"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f081d60

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->A:Landroid/net/Uri;

    .line 1066095
    new-instance v6, LX/6EH;

    invoke-direct {v6, p0, v5}, LX/6EH;-><init>(Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;Landroid/net/Uri;)V

    move-object v5, v6

    .line 1066096
    const/16 v6, 0x21

    invoke-virtual {v1, v3, v4, v5, v6}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v1

    .line 1066097
    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1066098
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1066099
    const/16 v1, 0x11

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 1066100
    const-string v1, "roboto"

    invoke-static {v1, v7}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1066101
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a066d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1066102
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1066103
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1066081
    invoke-direct {p0}, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->m()V

    .line 1066082
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1066083
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->n:LX/6EF;

    iget-object v1, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->t:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->q:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/6EF;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1066084
    new-instance v1, LX/6EG;

    invoke-direct {v1, p0}, LX/6EG;-><init>(Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;)V

    iget-object v2, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->B:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1066085
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x347c74b8    # -1.7241744E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1066078
    invoke-super {p0, p1}, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1066079
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v4, p0

    check-cast v4, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    const/16 v5, 0x12cb

    invoke-static {v1, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v1}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v1}, LX/6EF;->b(LX/0QB;)LX/6EF;

    move-result-object p1

    check-cast p1, LX/6EF;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v4, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->m:LX/0Or;

    iput-object v6, v4, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->B:Ljava/util/concurrent/Executor;

    iput-object v7, v4, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->C:LX/03V;

    iput-object p1, v4, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->n:LX/6EF;

    iput-object v1, v4, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->o:Lcom/facebook/content/SecureContextHelper;

    .line 1066080
    const/16 v1, 0x2b

    const v2, 0xc06e287

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
