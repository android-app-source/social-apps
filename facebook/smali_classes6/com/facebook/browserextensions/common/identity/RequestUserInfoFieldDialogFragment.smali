.class public Lcom/facebook/browserextensions/common/identity/RequestUserInfoFieldDialogFragment;
.super Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final n:Ljava/lang/String;


# instance fields
.field public m:LX/6EF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1066267
    const-class v0, Lcom/facebook/browserextensions/common/identity/RequestUserInfoFieldDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/common/identity/RequestUserInfoFieldDialogFragment;->n:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1066268
    invoke-direct {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;-><init>()V

    .line 1066269
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1066270
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->s:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1066271
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1066272
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1066273
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->l()V

    .line 1066274
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1066275
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->s:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1066276
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1066277
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestUserInfoFieldDialogFragment;->m:LX/6EF;

    iget-object v1, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->t:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->q:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/6EF;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1066278
    new-instance v1, LX/6EX;

    invoke-direct {v1, p0}, LX/6EX;-><init>(Lcom/facebook/browserextensions/common/identity/RequestUserInfoFieldDialogFragment;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1066279
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3cc58a6c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1066280
    invoke-super {p0, p1}, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1066281
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/browserextensions/common/identity/RequestUserInfoFieldDialogFragment;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p1

    check-cast p1, LX/03V;

    invoke-static {v1}, LX/6EF;->b(LX/0QB;)LX/6EF;

    move-result-object v1

    check-cast v1, LX/6EF;

    iput-object p1, p0, Lcom/facebook/browserextensions/common/identity/RequestUserInfoFieldDialogFragment;->o:LX/03V;

    iput-object v1, p0, Lcom/facebook/browserextensions/common/identity/RequestUserInfoFieldDialogFragment;->m:LX/6EF;

    .line 1066282
    const/16 v1, 0x2b

    const v2, -0x68298f26

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
