.class public Lcom/facebook/browserextensions/common/identity/RequestUserInfoActivity;
.super Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;
.source ""


# static fields
.field private static final p:Ljava/lang/String;


# instance fields
.field public q:LX/6EW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1066213
    const-class v0, Lcom/facebook/browserextensions/common/identity/RequestUserInfoActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/common/identity/RequestUserInfoActivity;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1066214
    invoke-direct {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/browserextensions/common/identity/RequestUserInfoActivity;

    invoke-static {v0}, LX/6EW;->a(LX/0QB;)LX/6EW;

    move-result-object v0

    check-cast v0, LX/6EW;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestUserInfoActivity;->q:LX/6EW;

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;
    .locals 2

    .prologue
    .line 1066215
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/identity/RequestUserInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1066216
    new-instance v1, LX/6EY;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {v1, v0}, LX/6EY;-><init>(Landroid/os/Bundle;)V

    .line 1066217
    new-instance v0, Lcom/facebook/browserextensions/common/identity/RequestUserInfoFieldDialogFragment;

    invoke-direct {v0}, Lcom/facebook/browserextensions/common/identity/RequestUserInfoFieldDialogFragment;-><init>()V

    .line 1066218
    invoke-virtual {v1}, LX/6EI;->b()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1066219
    move-object v0, v0

    .line 1066220
    return-object v0
.end method

.method public final b()LX/6EB;
    .locals 1

    .prologue
    .line 1066221
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestUserInfoActivity;->q:LX/6EW;

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1066222
    invoke-super {p0, p1}, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->b(Landroid/os/Bundle;)V

    .line 1066223
    invoke-static {p0, p0}, Lcom/facebook/browserextensions/common/identity/RequestUserInfoActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1066224
    return-void
.end method
