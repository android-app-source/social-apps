.class public final Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Result;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1066133
    new-instance v0, LX/6EO;

    invoke-direct {v0}, LX/6EO;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1066134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066135
    iput-object p1, p0, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Result;->a:Ljava/util/ArrayList;

    .line 1066136
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1066137
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1066138
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Result;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1066139
    return-void
.end method
