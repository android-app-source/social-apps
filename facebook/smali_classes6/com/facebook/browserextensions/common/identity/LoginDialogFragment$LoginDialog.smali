.class public final Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;
.super LX/4mf;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;Landroid/content/Context;I)V
    .locals 5

    .prologue
    .line 1066026
    iput-object p1, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    .line 1066027
    invoke-direct {p0, p1, p2, p3}, LX/4mf;-><init>(Lcom/facebook/ui/dialogs/FbDialogFragment;Landroid/content/Context;I)V

    .line 1066028
    const/4 p3, 0x1

    const/4 p2, 0x0

    .line 1066029
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030a58

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1066030
    iget-object v2, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    move-object v0, v1

    check-cast v0, Landroid/support/v7/widget/CardView;

    .line 1066031
    iput-object v0, v2, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->z:Landroid/support/v7/widget/CardView;

    .line 1066032
    iget-object v2, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    const v0, 0x7f0d1a54

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1066033
    iput-object v0, v2, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1066034
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iget-object v0, v0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b138d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/user/model/PicSquare;->a(I)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v0

    .line 1066035
    iget-object v2, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iget-object v2, v2, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, v0, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-class v3, Lcom/facebook/browserextensions/common/identity/RequestUserInfoFieldDialogFragment;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1066036
    const v0, 0x7f0d1a55

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1066037
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f081d6a

    new-array p1, p3, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iget-object v2, v2, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->m:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v2

    aput-object v2, p1, p2

    invoke-virtual {v3, v4, p1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1066038
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1066039
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iget-object v2, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iget-object v2, v2, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->a(Landroid/view/View;Ljava/util/ArrayList;)V

    .line 1066040
    const v0, 0x7f0d1a57

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1066041
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f081d69

    new-array p1, p3, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iget-object v2, v2, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->m:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v2

    aput-object v2, p1, p2

    invoke-virtual {v3, v4, p1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1066042
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1066043
    new-instance v2, LX/6EM;

    invoke-direct {v2, p0}, LX/6EM;-><init>(Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1066044
    iget-object v2, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    const v0, 0x7f0d1a51    # 1.875578E38f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, v2, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1066045
    iget-object v2, p0, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->b:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    const v0, 0x7f0d1a52

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->s:Landroid/widget/LinearLayout;

    .line 1066046
    invoke-virtual {p0, v1}, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment$LoginDialog;->setContentView(Landroid/view/View;)V

    .line 1066047
    return-void
.end method
