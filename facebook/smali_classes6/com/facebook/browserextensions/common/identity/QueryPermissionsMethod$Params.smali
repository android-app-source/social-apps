.class public final Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1066129
    new-instance v0, LX/6EN;

    invoke-direct {v0}, LX/6EN;-><init>()V

    sput-object v0, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1066126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066127
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Params;->a:Ljava/lang/String;

    .line 1066128
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1066123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066124
    iput-object p1, p0, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Params;->a:Ljava/lang/String;

    .line 1066125
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1066122
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1066120
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1066121
    return-void
.end method
