.class public abstract Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final p:Ljava/lang/String;


# instance fields
.field private q:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

.field public r:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final s:LX/6EP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1065851
    const-class v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1065852
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1065853
    new-instance v0, LX/6EP;

    invoke-direct {v0, p0}, LX/6EP;-><init>(Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;)V

    iput-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->s:LX/6EP;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->r:LX/03V;

    return-void
.end method

.method public static l(Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;)V
    .locals 2

    .prologue
    .line 1065854
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->b()LX/6EB;

    move-result-object v0

    sget-object v1, LX/6Cy;->BROWSER_EXTENSION_USER_DENIED_PERMISSION:LX/6Cy;

    invoke-interface {v0, v1}, LX/6EB;->a(LX/6Cy;)V

    .line 1065855
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->finish()V

    .line 1065856
    return-void
.end method


# virtual methods
.method public abstract a()Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;
.end method

.method public abstract b()LX/6EB;
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1065857
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1065858
    invoke-static {p0, p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1065859
    if-eqz p1, :cond_0

    .line 1065860
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "request_permission_dialog_fragment"

    invoke-virtual {v0, p1, v1}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    iput-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->q:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    .line 1065861
    :goto_0
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->q:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "request_permission_dialog_fragment"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1065862
    iget-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->q:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    iget-object v1, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->s:LX/6EP;

    .line 1065863
    iput-object v1, v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->p:LX/6EP;

    .line 1065864
    return-void

    .line 1065865
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->a()Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->q:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 1065866
    invoke-static {p0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->l(Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;)V

    .line 1065867
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1065868
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1065869
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "request_permission_dialog_fragment"

    iget-object v2, p0, Lcom/facebook/browserextensions/common/identity/RequestPermissionActivity;->q:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    invoke-virtual {v0, p1, v1, v2}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 1065870
    return-void
.end method
