.class public final Lcom/facebook/browserextensions/commerce/cart/CommerceCartMutationModels$CommerceCartMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28535e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browserextensions/commerce/cart/CommerceCartMutationModels$CommerceCartMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/browserextensions/commerce/cart/CommerceCartMutationModels$CommerceCartMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1063795
    const-class v0, Lcom/facebook/browserextensions/commerce/cart/CommerceCartMutationModels$CommerceCartMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1063794
    const-class v0, Lcom/facebook/browserextensions/commerce/cart/CommerceCartMutationModels$CommerceCartMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1063792
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1063793
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1063790
    iget-object v0, p0, Lcom/facebook/browserextensions/commerce/cart/CommerceCartMutationModels$CommerceCartMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/commerce/cart/CommerceCartMutationModels$CommerceCartMutationModel;->e:Ljava/lang/String;

    .line 1063791
    iget-object v0, p0, Lcom/facebook/browserextensions/commerce/cart/CommerceCartMutationModels$CommerceCartMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1063796
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1063797
    invoke-direct {p0}, Lcom/facebook/browserextensions/commerce/cart/CommerceCartMutationModels$CommerceCartMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1063798
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1063799
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1063800
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1063801
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1063787
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1063788
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1063789
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1063784
    new-instance v0, Lcom/facebook/browserextensions/commerce/cart/CommerceCartMutationModels$CommerceCartMutationModel;

    invoke-direct {v0}, Lcom/facebook/browserextensions/commerce/cart/CommerceCartMutationModels$CommerceCartMutationModel;-><init>()V

    .line 1063785
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1063786
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1063783
    const v0, 0x3c2f8979

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1063782
    const v0, 0x1ae7b340

    return v0
.end method
