.class public final Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1064167
    const-class v0, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;

    new-instance v1, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1064168
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1064166
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1064109
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1064110
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 1064111
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1064112
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1064113
    if-eqz v2, :cond_0

    .line 1064114
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1064115
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1064116
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1064117
    if-eqz v2, :cond_1

    .line 1064118
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1064119
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1064120
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1064121
    if-eqz v2, :cond_a

    .line 1064122
    const-string v3, "messenger_extension"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1064123
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1064124
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1064125
    if-eqz v3, :cond_9

    .line 1064126
    const-string v4, "favorite_list"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1064127
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1064128
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_8

    .line 1064129
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 1064130
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1064131
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1064132
    if-eqz v2, :cond_2

    .line 1064133
    const-string p2, "description"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1064134
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1064135
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {v1, p0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1064136
    if-eqz v2, :cond_3

    .line 1064137
    const-string p2, "domain"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1064138
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1064139
    :cond_3
    const/4 v2, 0x2

    invoke-virtual {v1, p0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1064140
    if-eqz v2, :cond_4

    .line 1064141
    const-string p2, "image_url"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1064142
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1064143
    :cond_4
    const/4 v2, 0x3

    invoke-virtual {v1, p0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1064144
    if-eqz v2, :cond_5

    .line 1064145
    const-string p2, "native_url"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1064146
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1064147
    :cond_5
    const/4 v2, 0x4

    invoke-virtual {v1, p0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1064148
    if-eqz v2, :cond_6

    .line 1064149
    const-string p2, "target_url"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1064150
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1064151
    :cond_6
    const/4 v2, 0x5

    invoke-virtual {v1, p0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1064152
    if-eqz v2, :cond_7

    .line 1064153
    const-string p2, "title"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1064154
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1064155
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1064156
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1064157
    :cond_8
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1064158
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1064159
    :cond_a
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1064160
    if-eqz v2, :cond_b

    .line 1064161
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1064162
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1064163
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1064164
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1064165
    check-cast p1, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel$Serializer;->a(Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
