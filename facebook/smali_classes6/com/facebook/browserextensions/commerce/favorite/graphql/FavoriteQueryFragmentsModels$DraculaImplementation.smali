.class public final Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1064010
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1064011
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1064012
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1064013
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 1064014
    if-nez p1, :cond_0

    .line 1064015
    :goto_0
    return v0

    .line 1064016
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1064017
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1064018
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1064019
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1064020
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1064021
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1064022
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1064023
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1064024
    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1064025
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1064026
    invoke-virtual {p0, p1, v11}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1064027
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1064028
    const/4 v6, 0x5

    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1064029
    invoke-virtual {p3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1064030
    const/4 v7, 0x6

    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1064031
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1064032
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 1064033
    invoke-virtual {p3, v9, v3}, LX/186;->b(II)V

    .line 1064034
    invoke-virtual {p3, v10, v4}, LX/186;->b(II)V

    .line 1064035
    invoke-virtual {p3, v11, v5}, LX/186;->b(II)V

    .line 1064036
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 1064037
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1064038
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1064039
    const v2, 0x249d2f5f

    const/4 v4, 0x0

    .line 1064040
    if-nez v1, :cond_1

    move v3, v4

    .line 1064041
    :goto_1
    move v1, v3

    .line 1064042
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1064043
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1064044
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1064045
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v5

    .line 1064046
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 1064047
    :goto_2
    if-ge v4, v5, :cond_3

    .line 1064048
    invoke-virtual {p0, v1, v4}, LX/15i;->q(II)I

    move-result v6

    .line 1064049
    invoke-static {p0, v6, v2, p3}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v6

    aput v6, v3, v4

    .line 1064050
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1064051
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 1064052
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1a8bc543 -> :sswitch_1
        0x249d2f5f -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1064071
    new-instance v0, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1064053
    sparse-switch p2, :sswitch_data_0

    .line 1064054
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1064055
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1064056
    const v1, 0x249d2f5f

    .line 1064057
    if-eqz v0, :cond_0

    .line 1064058
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1064059
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1064060
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1064061
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1064062
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1064063
    :cond_0
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1a8bc543 -> :sswitch_0
        0x249d2f5f -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1064064
    if-eqz p1, :cond_0

    .line 1064065
    invoke-static {p0, p1, p2}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;

    move-result-object v1

    .line 1064066
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;

    .line 1064067
    if-eq v0, v1, :cond_0

    .line 1064068
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1064069
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1064070
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1064003
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1064004
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1064005
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1064006
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1064007
    :cond_0
    iput-object p1, p0, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;->a:LX/15i;

    .line 1064008
    iput p2, p0, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;->b:I

    .line 1064009
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1064002
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1064001
    new-instance v0, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1063998
    iget v0, p0, LX/1vt;->c:I

    .line 1063999
    move v0, v0

    .line 1064000
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1063995
    iget v0, p0, LX/1vt;->c:I

    .line 1063996
    move v0, v0

    .line 1063997
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1063992
    iget v0, p0, LX/1vt;->b:I

    .line 1063993
    move v0, v0

    .line 1063994
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1063989
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1063990
    move-object v0, v0

    .line 1063991
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1063980
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1063981
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1063982
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1063983
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1063984
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1063985
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1063986
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1063987
    invoke-static {v3, v9, v2}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1063988
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1063977
    iget v0, p0, LX/1vt;->c:I

    .line 1063978
    move v0, v0

    .line 1063979
    return v0
.end method
