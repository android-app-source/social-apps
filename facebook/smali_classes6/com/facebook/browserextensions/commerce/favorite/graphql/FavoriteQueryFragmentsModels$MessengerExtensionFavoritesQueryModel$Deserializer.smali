.class public final Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1064072
    const-class v0, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;

    new-instance v1, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1064073
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1064074
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1064075
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1064076
    const/4 v2, 0x0

    .line 1064077
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_8

    .line 1064078
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1064079
    :goto_0
    move v1, v2

    .line 1064080
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1064081
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1064082
    new-instance v1, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;

    invoke-direct {v1}, Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;-><init>()V

    .line 1064083
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1064084
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1064085
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1064086
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1064087
    :cond_0
    return-object v1

    .line 1064088
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1064089
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_7

    .line 1064090
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1064091
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1064092
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v6, :cond_2

    .line 1064093
    const-string p0, "__type__"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1064094
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v5

    goto :goto_1

    .line 1064095
    :cond_4
    const-string p0, "id"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1064096
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1064097
    :cond_5
    const-string p0, "messenger_extension"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1064098
    invoke-static {p1, v0}, LX/6Cf;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1064099
    :cond_6
    const-string p0, "name"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1064100
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 1064101
    :cond_7
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1064102
    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1064103
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1064104
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1064105
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1064106
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    goto :goto_1
.end method
