.class public Lcom/facebook/ffmpeg/FFMpegBufferInfo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# instance fields
.field public flags:I
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end field

.field public offset:I
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end field

.field public presentationTimeUs:J
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end field

.field public size:I
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 909624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 909625
    return-void
.end method


# virtual methods
.method public final a(IIJI)V
    .locals 1

    .prologue
    .line 909626
    iput p1, p0, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->offset:I

    .line 909627
    iput p2, p0, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->size:I

    .line 909628
    iput-wide p3, p0, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->presentationTimeUs:J

    .line 909629
    iput p5, p0, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->flags:I

    .line 909630
    return-void
.end method

.method public final a(Landroid/media/MediaCodec$BufferInfo;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 909631
    iget v0, p1, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iput v0, p0, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->offset:I

    .line 909632
    iget v0, p1, Landroid/media/MediaCodec$BufferInfo;->size:I

    iput v0, p0, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->size:I

    .line 909633
    iget-wide v0, p1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v0, p0, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->presentationTimeUs:J

    .line 909634
    iget v0, p1, Landroid/media/MediaCodec$BufferInfo;->flags:I

    iput v0, p0, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->flags:I

    .line 909635
    return-void
.end method
