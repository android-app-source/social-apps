.class public Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2MY;

.field private final b:Ljava/lang/String;

.field private mNativeContext:J
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2MY;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 909757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 909758
    iput-object p1, p0, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->a:LX/2MY;

    .line 909759
    iput-object p2, p0, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->b:Ljava/lang/String;

    .line 909760
    return-void
.end method

.method private native nativeFinalize()V
.end method

.method private native nativeGetAudioBitRate()I
.end method

.method private native nativeGetBitRate()I
.end method

.method private native nativeGetDurationMs()J
.end method

.method private native nativeGetHeight()I
.end method

.method private native nativeGetRotation()I
.end method

.method private native nativeGetSphericalMetadataXml()Ljava/lang/String;
.end method

.method private native nativeGetWidth()I
.end method

.method private native nativeInit(Ljava/lang/String;)V
.end method

.method private native nativeRelease()V
.end method


# virtual methods
.method public final a()Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;
    .locals 1

    .prologue
    .line 909754
    iget-object v0, p0, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->a:LX/2MY;

    invoke-virtual {v0}, LX/03m;->b()V

    .line 909755
    iget-object v0, p0, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->nativeInit(Ljava/lang/String;)V

    .line 909756
    return-object p0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 909752
    invoke-direct {p0}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->nativeRelease()V

    .line 909753
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 909751
    invoke-direct {p0}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->nativeGetRotation()I

    move-result v0

    return v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 909750
    invoke-direct {p0}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->nativeGetDurationMs()J

    move-result-wide v0

    return-wide v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 909749
    invoke-direct {p0}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->nativeGetWidth()I

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 909748
    invoke-direct {p0}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->nativeGetHeight()I

    move-result v0

    return v0
.end method

.method public final finalize()V
    .locals 0

    .prologue
    .line 909745
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 909746
    invoke-direct {p0}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->nativeFinalize()V

    .line 909747
    return-void
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 909744
    invoke-direct {p0}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->nativeGetBitRate()I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 909742
    invoke-direct {p0}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->nativeGetAudioBitRate()I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 909743
    invoke-direct {p0}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->nativeGetSphericalMetadataXml()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
