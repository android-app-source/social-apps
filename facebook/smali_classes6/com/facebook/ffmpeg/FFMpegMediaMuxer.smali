.class public Lcom/facebook/ffmpeg/FFMpegMediaMuxer;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/2MY;

.field private mNativeContext:J
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2MY;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 909776
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 909777
    iput-object p1, p0, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->b:LX/2MY;

    .line 909778
    iput-object p2, p0, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->a:Ljava/lang/String;

    .line 909779
    return-void
.end method

.method private native nativeAddStream(Lcom/facebook/ffmpeg/FFMpegMediaFormat;)Lcom/facebook/ffmpeg/FFMpegAVStream;
.end method

.method private native nativeFinalize()V
.end method

.method private native nativeInit(Ljava/lang/String;)V
.end method

.method private native nativeStart()V
.end method

.method private native nativeStop()V
.end method


# virtual methods
.method public final a(Lcom/facebook/ffmpeg/FFMpegMediaFormat;)Lcom/facebook/ffmpeg/FFMpegAVStream;
    .locals 1

    .prologue
    .line 909765
    invoke-direct {p0, p1}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->nativeAddStream(Lcom/facebook/ffmpeg/FFMpegMediaFormat;)Lcom/facebook/ffmpeg/FFMpegAVStream;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/facebook/ffmpeg/FFMpegMediaMuxer;
    .locals 1

    .prologue
    .line 909773
    iget-object v0, p0, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->b:LX/2MY;

    invoke-virtual {v0}, LX/03m;->b()V

    .line 909774
    iget-object v0, p0, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->nativeInit(Ljava/lang/String;)V

    .line 909775
    return-object p0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 909771
    invoke-direct {p0}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->nativeStart()V

    .line 909772
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 909769
    invoke-direct {p0}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->nativeStop()V

    .line 909770
    return-void
.end method

.method public final finalize()V
    .locals 0

    .prologue
    .line 909766
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 909767
    invoke-direct {p0}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->nativeFinalize()V

    .line 909768
    return-void
.end method
