.class public Lcom/facebook/assetdownload/AssetDownloadConfiguration;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:LX/6BU;

.field public static final c:LX/6BV;

.field public static final d:Ljava/io/File;

.field public static final e:Ljava/lang/String;

.field public static final f:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private g:Landroid/net/Uri;

.field private h:Ljava/io/File;

.field public mAnalyticsTag:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "analytics_tag"
    .end annotation
.end field

.field public mConnectionConstraint:LX/6BU;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "connection_constraint"
    .end annotation
.end field

.field public mHttpHeaders:LX/0P1;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "http_headers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mIdentifier:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "identifier"
    .end annotation
.end field

.field public mNamespace:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "namespace"
    .end annotation
.end field

.field public mPriority:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "priority"
    .end annotation
.end field

.field public mStorageConstraint:LX/6BV;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "storage_constraint"
    .end annotation
.end field

.field public mStringCustomLocation:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "custom_location"
    .end annotation
.end field

.field public mStringSource:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "source"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1062704
    const-class v0, Lcom/facebook/assetdownload/AssetDownloadConfigurationDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1062705
    const-class v0, Lcom/facebook/assetdownload/AssetDownloadConfigurationSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1062706
    sput-object v1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->a:Landroid/net/Uri;

    .line 1062707
    sget-object v0, LX/6BU;->CAN_BE_ANY:LX/6BU;

    sput-object v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->b:LX/6BU;

    .line 1062708
    sget-object v0, LX/6BV;->CAN_BE_EXTERNAL:LX/6BV;

    sput-object v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->c:LX/6BV;

    .line 1062709
    sput-object v1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->d:Ljava/io/File;

    .line 1062710
    sput-object v1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->e:Ljava/lang/String;

    .line 1062711
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1062712
    sput-object v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->f:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 10

    .prologue
    .line 1062713
    const-string v1, ""

    sget-object v2, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->a:Landroid/net/Uri;

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->b:LX/6BU;

    sget-object v5, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->c:LX/6BV;

    const-string v6, "default_asset_download"

    sget-object v7, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->d:Ljava/io/File;

    sget-object v8, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->e:Ljava/lang/String;

    sget-object v9, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->f:LX/0P1;

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;-><init>(Ljava/lang/String;Landroid/net/Uri;ILX/6BU;LX/6BV;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/util/Map;)V

    .line 1062714
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;ILX/6BU;LX/6BV;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "I",
            "LX/6BU;",
            "LX/6BV;",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1062715
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1062716
    const-string v0, "identifier must not be null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1062717
    iput-object p1, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    .line 1062718
    iput-object p2, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->g:Landroid/net/Uri;

    .line 1062719
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStringSource:Ljava/lang/String;

    .line 1062720
    iput p3, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mPriority:I

    .line 1062721
    iput-object p4, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mConnectionConstraint:LX/6BU;

    .line 1062722
    iput-object p5, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStorageConstraint:LX/6BV;

    .line 1062723
    iput-object p6, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mAnalyticsTag:Ljava/lang/String;

    .line 1062724
    iput-object p7, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->h:Ljava/io/File;

    .line 1062725
    iget-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->h:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->h:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    :cond_0
    iput-object v1, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStringCustomLocation:Ljava/lang/String;

    .line 1062726
    iput-object p8, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mNamespace:Ljava/lang/String;

    .line 1062727
    invoke-static {p9}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mHttpHeaders:LX/0P1;

    .line 1062728
    return-void

    :cond_1
    move-object v0, v1

    .line 1062729
    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1062730
    iget-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->g:Landroid/net/Uri;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStringSource:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1062731
    iget-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStringSource:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->g:Landroid/net/Uri;

    .line 1062732
    :cond_0
    iget-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->g:Landroid/net/Uri;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1062733
    iget-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1062734
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 1062735
    :goto_0
    return v0

    .line 1062736
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1062737
    :cond_2
    check-cast p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    .line 1062738
    iget-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final g()Ljava/io/File;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1062739
    iget-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->h:Ljava/io/File;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStringCustomLocation:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1062740
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStringCustomLocation:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->h:Ljava/io/File;

    .line 1062741
    :cond_0
    iget-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->h:Ljava/io/File;

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1062742
    iget-object v0, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1062743
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "identifier"

    iget-object v2, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {p0}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "priority"

    iget v2, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mPriority:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "connectionConstraint"

    iget-object v2, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mConnectionConstraint:LX/6BU;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "storageConstraint"

    iget-object v2, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStorageConstraint:LX/6BV;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "analyticsTag"

    iget-object v2, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mAnalyticsTag:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "customLocation"

    invoke-virtual {p0}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->g()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "namespace"

    iget-object v2, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mNamespace:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
