.class public Lcom/facebook/assetdownload/AssetDownloadConfigurationSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/assetdownload/AssetDownloadConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1062773
    const-class v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    new-instance v1, Lcom/facebook/assetdownload/AssetDownloadConfigurationSerializer;

    invoke-direct {v1}, Lcom/facebook/assetdownload/AssetDownloadConfigurationSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1062774
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1062775
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1062776
    if-nez p0, :cond_0

    .line 1062777
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1062778
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1062779
    invoke-static {p0, p1, p2}, Lcom/facebook/assetdownload/AssetDownloadConfigurationSerializer;->b(Lcom/facebook/assetdownload/AssetDownloadConfiguration;LX/0nX;LX/0my;)V

    .line 1062780
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1062781
    return-void
.end method

.method private static b(Lcom/facebook/assetdownload/AssetDownloadConfiguration;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1062782
    const-string v0, "identifier"

    iget-object v1, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1062783
    const-string v0, "source"

    iget-object v1, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStringSource:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1062784
    const-string v0, "priority"

    iget v1, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mPriority:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1062785
    const-string v0, "connection_constraint"

    iget-object v1, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mConnectionConstraint:LX/6BU;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1062786
    const-string v0, "storage_constraint"

    iget-object v1, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStorageConstraint:LX/6BV;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1062787
    const-string v0, "analytics_tag"

    iget-object v1, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mAnalyticsTag:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1062788
    const-string v0, "custom_location"

    iget-object v1, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStringCustomLocation:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1062789
    const-string v0, "namespace"

    iget-object v1, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mNamespace:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1062790
    const-string v0, "http_headers"

    iget-object v1, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mHttpHeaders:LX/0P1;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1062791
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1062792
    check-cast p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    invoke-static {p1, p2, p3}, Lcom/facebook/assetdownload/AssetDownloadConfigurationSerializer;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;LX/0nX;LX/0my;)V

    return-void
.end method
