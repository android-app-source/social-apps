.class public final Lcom/facebook/assetdownload/remote/DownloadExecutor$DownloadCallable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/6Bq;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6Bp;

.field private final b:LX/6Bo;


# direct methods
.method public constructor <init>(LX/6Bp;LX/6Bo;)V
    .locals 0

    .prologue
    .line 1063202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063203
    iput-object p1, p0, Lcom/facebook/assetdownload/remote/DownloadExecutor$DownloadCallable;->a:LX/6Bp;

    .line 1063204
    iput-object p2, p0, Lcom/facebook/assetdownload/remote/DownloadExecutor$DownloadCallable;->b:LX/6Bo;

    .line 1063205
    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1063206
    iget-object v0, p0, Lcom/facebook/assetdownload/remote/DownloadExecutor$DownloadCallable;->a:LX/6Bp;

    .line 1063207
    iget-object v1, v0, LX/6Bp;->a:Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    invoke-virtual {v1}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->a()Landroid/net/Uri;

    move-result-object v1

    move-object v1, v1

    .line 1063208
    iget-object v0, p0, Lcom/facebook/assetdownload/remote/DownloadExecutor$DownloadCallable;->a:LX/6Bp;

    invoke-virtual {v0}, LX/6Bp;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, LX/6Bs;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 1063209
    :goto_0
    new-instance v2, LX/6Br;

    iget-object v3, p0, Lcom/facebook/assetdownload/remote/DownloadExecutor$DownloadCallable;->a:LX/6Bp;

    invoke-direct {v2, v3}, LX/6Br;-><init>(LX/6Bp;)V

    .line 1063210
    new-instance v3, LX/34X;

    iget-object v4, p0, Lcom/facebook/assetdownload/remote/DownloadExecutor$DownloadCallable;->a:LX/6Bp;

    .line 1063211
    iget-object v5, v4, LX/6Bp;->a:Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    move-object v4, v5

    .line 1063212
    iget-object v5, v4, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mHttpHeaders:LX/0P1;

    move-object v4, v5

    .line 1063213
    invoke-direct {v3, v1, v2, v0, v4}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;LX/0P1;)V

    .line 1063214
    iget-object v0, p0, Lcom/facebook/assetdownload/remote/DownloadExecutor$DownloadCallable;->b:LX/6Bo;

    invoke-virtual {v0, v3}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Bq;

    return-object v0

    .line 1063215
    :cond_0
    const-class v0, LX/6Bs;

    iget-object v2, p0, Lcom/facebook/assetdownload/remote/DownloadExecutor$DownloadCallable;->a:LX/6Bp;

    invoke-virtual {v2}, LX/6Bp;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    goto :goto_0
.end method
