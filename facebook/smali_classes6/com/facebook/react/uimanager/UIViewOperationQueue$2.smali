.class public final Lcom/facebook/react/uimanager/UIViewOperationQueue$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:[LX/5rT;

.field public final synthetic c:Ljava/util/ArrayList;

.field public final synthetic d:LX/5rl;


# direct methods
.method public constructor <init>(LX/5rl;I[LX/5rT;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 1011818
    iput-object p1, p0, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;->d:LX/5rl;

    iput p2, p0, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;->a:I

    iput-object p3, p0, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;->b:[LX/5rT;

    iput-object p4, p0, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;->c:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const-wide/16 v6, 0x2000

    .line 1011819
    const-string v1, "DispatchUI"

    invoke-static {v6, v7, v1}, LX/0BL;->a(JLjava/lang/String;)LX/0BN;

    move-result-object v1

    const-string v2, "BatchId"

    iget v3, p0, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;->a:I

    invoke-virtual {v1, v2, v3}, LX/0BN;->a(Ljava/lang/String;I)LX/0BN;

    move-result-object v1

    invoke-virtual {v1}, LX/0BN;->a()V

    .line 1011820
    :try_start_0
    iget-object v1, p0, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;->b:[LX/5rT;

    if-eqz v1, :cond_0

    .line 1011821
    iget-object v2, p0, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;->b:[LX/5rT;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1011822
    invoke-interface {v4}, LX/5rT;->a()V

    .line 1011823
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1011824
    :cond_0
    iget-object v1, p0, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    move v1, v0

    .line 1011825
    :goto_1
    iget-object v0, p0, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1011826
    iget-object v0, p0, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5rT;

    invoke-interface {v0}, LX/5rT;->a()V

    .line 1011827
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1011828
    :cond_1
    iget-object v0, p0, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;->d:LX/5rl;

    iget-object v0, v0, LX/5rl;->b:LX/5qw;

    invoke-virtual {v0}, LX/5qw;->c()V

    .line 1011829
    iget-object v0, p0, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;->d:LX/5rl;

    iget-object v0, v0, LX/5rl;->k:LX/5qU;

    if-eqz v0, :cond_2

    .line 1011830
    iget-object v0, p0, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;->d:LX/5rl;

    iget-object v0, v0, LX/5rl;->k:LX/5qU;

    invoke-interface {v0}, LX/5qU;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1011831
    :cond_2
    invoke-static {v6, v7}, LX/018;->a(J)V

    .line 1011832
    return-void

    .line 1011833
    :catchall_0
    move-exception v0

    invoke-static {v6, v7}, LX/018;->a(J)V

    throw v0
.end method
