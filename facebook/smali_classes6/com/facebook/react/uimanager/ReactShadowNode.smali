.class public Lcom/facebook/react/uimanager/ReactShadowNode;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/react/uimanager/annotations/ReactPropertyHolder;
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/react/uimanager/ReactShadowNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/5rJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field public f:Z

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/react/uimanager/ReactShadowNode;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/react/uimanager/ReactShadowNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:I

.field public k:Lcom/facebook/react/uimanager/ReactShadowNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/react/uimanager/ReactShadowNode;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private final q:LX/5rI;

.field private final r:LX/5rI;

.field private final s:Lcom/facebook/csslayout/YogaNode;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1009990
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1009991
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->f:Z

    .line 1009992
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->j:I

    .line 1009993
    new-instance v0, LX/5rI;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/5rI;-><init>(F)V

    iput-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->q:LX/5rI;

    .line 1009994
    new-instance v0, LX/5rI;

    const/high16 v1, 0x7fc00000    # NaNf

    invoke-direct {v0, v1}, LX/5rI;-><init>(F)V

    iput-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->r:LX/5rI;

    .line 1009995
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1009996
    invoke-static {}, LX/5s3;->a()LX/5po;

    move-result-object v0

    invoke-virtual {v0}, LX/5po;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaNode;

    .line 1009997
    if-nez v0, :cond_0

    .line 1009998
    new-instance v0, Lcom/facebook/csslayout/YogaNode;

    invoke-direct {v0}, Lcom/facebook/csslayout/YogaNode;-><init>()V

    .line 1009999
    :cond_0
    iput-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    .line 1010000
    :goto_0
    return-void

    .line 1010001
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    goto :goto_0
.end method

.method private H()Z
    .locals 1

    .prologue
    .line 1009966
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaNode;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private J()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 1009952
    const/4 v0, 0x0

    :goto_0
    if-gt v0, v4, :cond_7

    .line 1009953
    if-eqz v0, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 1009954
    :cond_0
    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->r:LX/5rI;

    invoke-virtual {v1, v0}, LX/5rI;->b(I)F

    move-result v1

    invoke-static {v1}, LX/1mo;->a(F)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->r:LX/5rI;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, LX/5rI;->b(I)F

    move-result v1

    invoke-static {v1}, LX/1mo;->a(F)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->r:LX/5rI;

    invoke-virtual {v1, v4}, LX/5rI;->b(I)F

    move-result v1

    invoke-static {v1}, LX/1mo;->a(F)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1009955
    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-static {v0}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->q:LX/5rI;

    invoke-virtual {v3, v0}, LX/5rI;->b(I)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/csslayout/YogaNode;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 1009956
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1009957
    :cond_1
    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-static {v0}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->r:LX/5rI;

    invoke-virtual {v3, v0}, LX/5rI;->b(I)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/csslayout/YogaNode;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    goto :goto_1

    .line 1009958
    :cond_2
    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 1009959
    :cond_3
    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->r:LX/5rI;

    invoke-virtual {v1, v0}, LX/5rI;->b(I)F

    move-result v1

    invoke-static {v1}, LX/1mo;->a(F)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->r:LX/5rI;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, LX/5rI;->b(I)F

    move-result v1

    invoke-static {v1}, LX/1mo;->a(F)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->r:LX/5rI;

    invoke-virtual {v1, v4}, LX/5rI;->b(I)F

    move-result v1

    invoke-static {v1}, LX/1mo;->a(F)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1009960
    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-static {v0}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->q:LX/5rI;

    invoke-virtual {v3, v0}, LX/5rI;->b(I)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/csslayout/YogaNode;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    goto :goto_1

    .line 1009961
    :cond_4
    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-static {v0}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->r:LX/5rI;

    invoke-virtual {v3, v0}, LX/5rI;->b(I)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/csslayout/YogaNode;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    goto :goto_1

    .line 1009962
    :cond_5
    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->r:LX/5rI;

    invoke-virtual {v1, v0}, LX/5rI;->b(I)F

    move-result v1

    invoke-static {v1}, LX/1mo;->a(F)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1009963
    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-static {v0}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->q:LX/5rI;

    invoke-virtual {v3, v0}, LX/5rI;->b(I)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/csslayout/YogaNode;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    goto :goto_1

    .line 1009964
    :cond_6
    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-static {v0}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->r:LX/5rI;

    invoke-virtual {v3, v0}, LX/5rI;->b(I)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/csslayout/YogaNode;->b(Lcom/facebook/csslayout/YogaEdge;F)V

    goto/16 :goto_1

    .line 1009965
    :cond_7
    return-void
.end method

.method private f(I)V
    .locals 2

    .prologue
    .line 1009944
    iget-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    if-eqz v0, :cond_0

    .line 1009945
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v0

    .line 1009946
    :goto_0
    if-eqz v0, :cond_0

    .line 1009947
    iget v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->j:I

    add-int/2addr v1, p1

    iput v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->j:I

    .line 1009948
    iget-boolean v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    if-eqz v1, :cond_0

    .line 1009949
    iget-object v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v1

    .line 1009950
    goto :goto_0

    .line 1009951
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()F
    .locals 1

    .prologue
    .line 1009943
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaNode;->n()F

    move-result v0

    return v0
.end method

.method public B()I
    .locals 1

    .prologue
    .line 1009942
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->x()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public C()I
    .locals 1

    .prologue
    .line 1009941
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->y()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public D()I
    .locals 2

    .prologue
    .line 1009940
    iget v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->o:F

    iget v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->m:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public E()I
    .locals 2

    .prologue
    .line 1009910
    iget v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->p:F

    iget v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->n:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public final F()Lcom/facebook/csslayout/YogaDirection;
    .locals 1

    .prologue
    .line 1009937
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaNode;->o()Lcom/facebook/csslayout/YogaDirection;

    move-result-object v0

    return-object v0
.end method

.method public final G()V
    .locals 2

    .prologue
    .line 1009933
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    if-eqz v0, :cond_0

    .line 1009934
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaNode;->a()V

    .line 1009935
    invoke-static {}, LX/5s3;->a()LX/5po;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, v1}, LX/5po;->a(Ljava/lang/Object;)Z

    .line 1009936
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/react/uimanager/ReactShadowNode;)I
    .locals 1

    .prologue
    .line 1009932
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 3

    .prologue
    .line 1009920
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1009921
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " out of bounds: node has no children"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1009922
    :cond_0
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 1009923
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 1009924
    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v1}, Lcom/facebook/csslayout/YogaNode;->p()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1009925
    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v1, p1}, Lcom/facebook/csslayout/YogaNode;->a(I)Lcom/facebook/csslayout/YogaNode;

    .line 1009926
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 1009927
    iget-boolean v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    if-eqz v1, :cond_2

    iget v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->j:I

    .line 1009928
    :goto_0
    iget v2, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->j:I

    sub-int/2addr v2, v1

    iput v2, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->j:I

    .line 1009929
    neg-int v1, v1

    invoke-direct {p0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->f(I)V

    .line 1009930
    return-object v0

    .line 1009931
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 1009918
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->e(F)V

    .line 1009919
    return-void
.end method

.method public final a(IF)V
    .locals 2

    .prologue
    .line 1009916
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-static {p1}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/facebook/csslayout/YogaNode;->a(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 1009917
    return-void
.end method

.method public a(LX/5rJ;)V
    .locals 0

    .prologue
    .line 1009914
    iput-object p1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->d:LX/5rJ;

    .line 1009915
    return-void
.end method

.method public a(LX/5rl;)V
    .locals 0

    .prologue
    .line 1009913
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaAlign;)V
    .locals 1

    .prologue
    .line 1009911
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->b(Lcom/facebook/csslayout/YogaAlign;)V

    .line 1009912
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaDirection;)V
    .locals 1

    .prologue
    .line 1010051
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->a(Lcom/facebook/csslayout/YogaDirection;)V

    .line 1010052
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaFlexDirection;)V
    .locals 1

    .prologue
    .line 1009968
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->a(Lcom/facebook/csslayout/YogaFlexDirection;)V

    .line 1009969
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaJustify;)V
    .locals 1

    .prologue
    .line 1010049
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->a(Lcom/facebook/csslayout/YogaJustify;)V

    .line 1010050
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaMeasureFunction;)V
    .locals 2

    .prologue
    .line 1010044
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v1}, Lcom/facebook/csslayout/YogaNode;->p()Z

    move-result v1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1010045
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Since a node with a measure function does not add any native CSSLayout children, it\'s not safe to transition to/from having a measure function unless a node has no children"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010046
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1010047
    :cond_1
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->a(Lcom/facebook/csslayout/YogaMeasureFunction;)V

    .line 1010048
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaOverflow;)V
    .locals 1

    .prologue
    .line 1010042
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->a(Lcom/facebook/csslayout/YogaOverflow;)V

    .line 1010043
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaPositionType;)V
    .locals 1

    .prologue
    .line 1010040
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->a(Lcom/facebook/csslayout/YogaPositionType;)V

    .line 1010041
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaWrap;)V
    .locals 1

    .prologue
    .line 1010038
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->a(Lcom/facebook/csslayout/YogaWrap;)V

    .line 1010039
    return-void
.end method

.method public a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V
    .locals 2

    .prologue
    .line 1010021
    iget-object v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    if-eqz v0, :cond_0

    .line 1010022
    new-instance v0, LX/5qo;

    const-string v1, "Tried to add child that already has a parent! Remove it from its parent first."

    invoke-direct {v0, v1}, LX/5qo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010023
    :cond_0
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1010024
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->g:Ljava/util/ArrayList;

    .line 1010025
    :cond_1
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1010026
    iput-object p0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 1010027
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaNode;->p()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1010028
    iget-object v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    .line 1010029
    if-nez v0, :cond_2

    .line 1010030
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot add a child that doesn\'t have a CSS node to a node without a measure function!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1010031
    :cond_2
    iget-object v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v1, v0, p2}, Lcom/facebook/csslayout/YogaNode;->a(Lcom/facebook/csslayout/YogaNode;I)V

    .line 1010032
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 1010033
    iget-boolean v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    if-eqz v0, :cond_4

    iget v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->j:I

    .line 1010034
    :goto_0
    iget v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->j:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->j:I

    .line 1010035
    invoke-direct {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f(I)V

    .line 1010036
    return-void

    .line 1010037
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1010012
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v0

    .line 1010013
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must remove from no opt parent first"

    invoke-static {v0, v3}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1010014
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->k:Lcom/facebook/react/uimanager/ReactShadowNode;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Must remove from native parent first"

    invoke-static {v0, v3}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1010015
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->u()I

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    const-string v0, "Must remove all native children first"

    invoke-static {v1, v0}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1010016
    iput-boolean p1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    .line 1010017
    return-void

    :cond_0
    move v0, v2

    .line 1010018
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1010019
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1010020
    goto :goto_2
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 1010053
    const/4 v0, 0x0

    return v0
.end method

.method public final a(FFLX/5rl;LX/5qy;)Z
    .locals 2

    .prologue
    .line 1010002
    iget-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->f:Z

    if-eqz v0, :cond_0

    .line 1010003
    invoke-virtual {p0, p3}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(LX/5rl;)V

    .line 1010004
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1010005
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->x()F

    move-result v0

    add-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->m:F

    .line 1010006
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->y()F

    move-result v0

    add-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->n:F

    .line 1010007
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->x()F

    move-result v0

    add-float/2addr v0, p1

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->z()F

    move-result v1

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->o:F

    .line 1010008
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->y()F

    move-result v0

    add-float/2addr v0, p2

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->A()F

    move-result v1

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->p:F

    .line 1010009
    invoke-static {p4, p0}, LX/5qy;->c(LX/5qy;Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 1010010
    const/4 v0, 0x1

    .line 1010011
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 3

    .prologue
    .line 1009987
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1009988
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " out of bounds: node has no children"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1009989
    :cond_0
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/ReactShadowNode;

    return-object v0
.end method

.method public final b(F)V
    .locals 1

    .prologue
    .line 1009985
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->g(F)V

    .line 1009986
    return-void
.end method

.method public final b(IF)V
    .locals 1

    .prologue
    .line 1009982
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->q:LX/5rI;

    invoke-virtual {v0, p1, p2}, LX/5rI;->a(IF)Z

    .line 1009983
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->J()V

    .line 1009984
    return-void
.end method

.method public final b(Lcom/facebook/csslayout/YogaAlign;)V
    .locals 1

    .prologue
    .line 1009980
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->a(Lcom/facebook/csslayout/YogaAlign;)V

    .line 1009981
    return-void
.end method

.method public final b(Lcom/facebook/react/uimanager/ReactShadowNode;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1009971
    iget-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0nE;->a(Z)V

    .line 1009972
    iget-boolean v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    if-nez v0, :cond_2

    :goto_1
    invoke-static {v1}, LX/0nE;->a(Z)V

    .line 1009973
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1009974
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->l:Ljava/util/ArrayList;

    .line 1009975
    :cond_0
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1009976
    iput-object p0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->k:Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 1009977
    return-void

    :cond_1
    move v0, v2

    .line 1009978
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1009979
    goto :goto_1
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 1009970
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Lcom/facebook/react/uimanager/ReactShadowNode;)I
    .locals 1

    .prologue
    .line 1009938
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->l:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1009939
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1009967
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final c(F)V
    .locals 1

    .prologue
    .line 1009877
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->i(F)V

    .line 1009878
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 1009875
    iput p1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    .line 1009876
    return-void
.end method

.method public c(IF)V
    .locals 1

    .prologue
    .line 1009872
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->r:LX/5rI;

    invoke-virtual {v0, p1, p2}, LX/5rI;->a(IF)Z

    .line 1009873
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->J()V

    .line 1009874
    return-void
.end method

.method public final d(Lcom/facebook/react/uimanager/ReactShadowNode;)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1009860
    move v0, v1

    move v2, v1

    .line 1009861
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 1009862
    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v3

    .line 1009863
    if-ne p1, v3, :cond_0

    .line 1009864
    :goto_1
    if-nez v4, :cond_2

    .line 1009865
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Child "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not a child of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1009866
    :cond_0
    iget-boolean v5, v3, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    if-eqz v5, :cond_1

    .line 1009867
    iget v5, v3, Lcom/facebook/react/uimanager/ReactShadowNode;->j:I

    move v3, v5

    .line 1009868
    :goto_2
    add-int/2addr v2, v3

    .line 1009869
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v3, v4

    .line 1009870
    goto :goto_2

    .line 1009871
    :cond_2
    return v2

    :cond_3
    move v4, v1

    goto :goto_1
.end method

.method public final d(I)Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 2

    .prologue
    .line 1009856
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->l:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1009857
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 1009858
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->k:Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 1009859
    return-object v0
.end method

.method public d(F)V
    .locals 1

    .prologue
    .line 1009854
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->f(F)V

    .line 1009855
    return-void
.end method

.method public d(IF)V
    .locals 2

    .prologue
    .line 1009852
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-static {p1}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/facebook/csslayout/YogaNode;->c(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 1009853
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1009851
    iget-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->H()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(I)F
    .locals 2

    .prologue
    .line 1009881
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-static {p1}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/csslayout/YogaNode;->a(Lcom/facebook/csslayout/YogaEdge;)F

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1009847
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->f:Z

    .line 1009848
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1009849
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->s()V

    .line 1009850
    :cond_0
    return-void
.end method

.method public final e(F)V
    .locals 1

    .prologue
    .line 1009812
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->h(F)V

    .line 1009813
    return-void
.end method

.method public final e(IF)V
    .locals 2

    .prologue
    .line 1009814
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-static {p1}, Lcom/facebook/csslayout/YogaEdge;->fromInt(I)Lcom/facebook/csslayout/YogaEdge;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/facebook/csslayout/YogaNode;->d(Lcom/facebook/csslayout/YogaEdge;F)V

    .line 1009815
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 1009816
    iget-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->f:Z

    if-eqz v0, :cond_1

    .line 1009817
    :cond_0
    :goto_0
    return-void

    .line 1009818
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->f:Z

    .line 1009819
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v0

    .line 1009820
    if-eqz v0, :cond_0

    .line 1009821
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    goto :goto_0
.end method

.method public final f(F)V
    .locals 1

    .prologue
    .line 1009822
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->j(F)V

    .line 1009823
    return-void
.end method

.method public final g(F)V
    .locals 1

    .prologue
    .line 1009824
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->k(F)V

    .line 1009825
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1009826
    iget-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->f:Z

    return v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1009827
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1009828
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaNode;->e()V

    .line 1009829
    :cond_0
    return-void
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 1009830
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final j()V
    .locals 4

    .prologue
    .line 1009831
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v0

    if-nez v0, :cond_0

    .line 1009832
    :goto_0
    return-void

    .line 1009833
    :cond_0
    const/4 v1, 0x0

    .line 1009834
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_3

    .line 1009835
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaNode;->p()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1009836
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, v1}, Lcom/facebook/csslayout/YogaNode;->a(I)Lcom/facebook/csslayout/YogaNode;

    .line 1009837
    :cond_1
    invoke-virtual {p0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 1009838
    const/4 v3, 0x0

    iput-object v3, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 1009839
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->G()V

    .line 1009840
    iget-boolean v3, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->i:Z

    if-eqz v3, :cond_2

    iget v0, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->j:I

    :goto_2
    add-int/2addr v2, v0

    .line 1009841
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 1009842
    :cond_2
    const/4 v0, 0x1

    goto :goto_2

    .line 1009843
    :cond_3
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->g:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1009844
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 1009845
    iget v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->j:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->j:I

    .line 1009846
    neg-int v0, v2

    invoke-direct {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f(I)V

    goto :goto_0
.end method

.method public k()V
    .locals 0

    .prologue
    .line 1009897
    return-void
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 1009909
    iget v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    return v0
.end method

.method public final m()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 1009908
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->c:Lcom/facebook/react/uimanager/ReactShadowNode;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/ReactShadowNode;

    return-object v0
.end method

.method public final o()LX/5rJ;
    .locals 1

    .prologue
    .line 1009907
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->d:LX/5rJ;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5rJ;

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 1009906
    iget-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->e:Z

    return v0
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 1009904
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaNode;->c()V

    .line 1009905
    return-void
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 1009903
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaNode;->d()Z

    move-result v0

    goto :goto_0
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 1009900
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    if-eqz v0, :cond_0

    .line 1009901
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaNode;->g()V

    .line 1009902
    :cond_0
    return-void
.end method

.method public setFlex(F)V
    .locals 1

    .prologue
    .line 1009898
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->a(F)V

    .line 1009899
    return-void
.end method

.method public setFlexBasis(F)V
    .locals 1

    .prologue
    .line 1009879
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->d(F)V

    .line 1009880
    return-void
.end method

.method public setFlexGrow(F)V
    .locals 1

    .prologue
    .line 1009895
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->b(F)V

    .line 1009896
    return-void
.end method

.method public setFlexShrink(F)V
    .locals 1

    .prologue
    .line 1009893
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0, p1}, Lcom/facebook/csslayout/YogaNode;->c(F)V

    .line 1009894
    return-void
.end method

.method public final t()V
    .locals 3

    .prologue
    .line 1009887
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1009888
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1009889
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/ReactShadowNode;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->k:Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 1009890
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1009891
    :cond_0
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1009892
    :cond_1
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1009886
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()I
    .locals 1

    .prologue
    .line 1009885
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final x()F
    .locals 1

    .prologue
    .line 1009884
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaNode;->k()F

    move-result v0

    return v0
.end method

.method public final y()F
    .locals 1

    .prologue
    .line 1009883
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaNode;->l()F

    move-result v0

    return v0
.end method

.method public final z()F
    .locals 1

    .prologue
    .line 1009882
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->s:Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaNode;->m()F

    move-result v0

    return v0
.end method
