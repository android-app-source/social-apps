.class public abstract Lcom/facebook/react/uimanager/BaseViewManager;
.super Lcom/facebook/react/uimanager/ViewManager;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        "C:",
        "Lcom/facebook/react/uimanager/LayoutShadowNode;",
        ">",
        "Lcom/facebook/react/uimanager/ViewManager",
        "<TT;TC;>;"
    }
.end annotation


# static fields
.field private static a:LX/5qq;

.field private static b:[D


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1009625
    new-instance v0, LX/5qq;

    invoke-direct {v0}, LX/5qq;-><init>()V

    sput-object v0, Lcom/facebook/react/uimanager/BaseViewManager;->a:LX/5qq;

    .line 1009626
    const/16 v0, 0x10

    new-array v0, v0, [D

    sput-object v0, Lcom/facebook/react/uimanager/BaseViewManager;->b:[D

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1009624
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ViewManager;-><init>()V

    return-void
.end method

.method private static a(Landroid/view/View;LX/5pC;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1009614
    sget-object v0, Lcom/facebook/react/uimanager/BaseViewManager;->b:[D

    invoke-static {p1, v0}, LX/5rM;->a(LX/5pC;[D)V

    .line 1009615
    sget-object v0, Lcom/facebook/react/uimanager/BaseViewManager;->b:[D

    sget-object v1, Lcom/facebook/react/uimanager/BaseViewManager;->a:LX/5qq;

    invoke-static {v0, v1}, LX/5qr;->a([DLX/5qq;)V

    .line 1009616
    sget-object v0, Lcom/facebook/react/uimanager/BaseViewManager;->a:LX/5qq;

    iget-object v0, v0, LX/5qq;->e:[D

    aget-wide v0, v0, v2

    double-to-float v0, v0

    invoke-static {v0}, LX/5r2;->a(F)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 1009617
    sget-object v0, Lcom/facebook/react/uimanager/BaseViewManager;->a:LX/5qq;

    iget-object v0, v0, LX/5qq;->e:[D

    aget-wide v0, v0, v3

    double-to-float v0, v0

    invoke-static {v0}, LX/5r2;->a(F)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1009618
    sget-object v0, Lcom/facebook/react/uimanager/BaseViewManager;->a:LX/5qq;

    iget-object v0, v0, LX/5qq;->f:[D

    const/4 v1, 0x2

    aget-wide v0, v0, v1

    double-to-float v0, v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setRotation(F)V

    .line 1009619
    sget-object v0, Lcom/facebook/react/uimanager/BaseViewManager;->a:LX/5qq;

    iget-object v0, v0, LX/5qq;->f:[D

    aget-wide v0, v0, v2

    double-to-float v0, v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setRotationX(F)V

    .line 1009620
    sget-object v0, Lcom/facebook/react/uimanager/BaseViewManager;->a:LX/5qq;

    iget-object v0, v0, LX/5qq;->f:[D

    aget-wide v0, v0, v3

    double-to-float v0, v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setRotationY(F)V

    .line 1009621
    sget-object v0, Lcom/facebook/react/uimanager/BaseViewManager;->a:LX/5qq;

    iget-object v0, v0, LX/5qq;->c:[D

    aget-wide v0, v0, v2

    double-to-float v0, v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setScaleX(F)V

    .line 1009622
    sget-object v0, Lcom/facebook/react/uimanager/BaseViewManager;->a:LX/5qq;

    iget-object v0, v0, LX/5qq;->c:[D

    aget-wide v0, v0, v3

    double-to-float v0, v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setScaleY(F)V

    .line 1009623
    return-void
.end method

.method private static c(Landroid/view/View;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1009606
    invoke-static {v1}, LX/5r2;->a(F)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 1009607
    invoke-static {v1}, LX/5r2;->a(F)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1009608
    invoke-virtual {p0, v1}, Landroid/view/View;->setRotation(F)V

    .line 1009609
    invoke-virtual {p0, v1}, Landroid/view/View;->setRotationX(F)V

    .line 1009610
    invoke-virtual {p0, v1}, Landroid/view/View;->setRotationY(F)V

    .line 1009611
    invoke-virtual {p0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 1009612
    invoke-virtual {p0, v2}, Landroid/view/View;->setScaleY(F)V

    .line 1009613
    return-void
.end method


# virtual methods
.method public setAccessibilityComponentType(Landroid/view/View;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "accessibilityComponentType"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1009604
    invoke-static {p1, p2}, LX/5qk;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 1009605
    return-void
.end method

.method public setAccessibilityLabel(Landroid/view/View;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "accessibilityLabel"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1009602
    invoke-virtual {p1, p2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1009603
    return-void
.end method

.method public setAccessibilityLiveRegion(Landroid/view/View;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "accessibilityLiveRegion"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1009594
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 1009595
    if-eqz p2, :cond_0

    const-string v0, "none"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1009596
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityLiveRegion(I)V

    .line 1009597
    :cond_1
    :goto_0
    return-void

    .line 1009598
    :cond_2
    const-string v0, "polite"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1009599
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityLiveRegion(I)V

    goto :goto_0

    .line 1009600
    :cond_3
    const-string v0, "assertive"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1009601
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityLiveRegion(I)V

    goto :goto_0
.end method

.method public setBackgroundColor(Landroid/view/View;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x0
        customType = "Color"
        name = "backgroundColor"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    .prologue
    .line 1009592
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1009593
    return-void
.end method

.method public setElevation(Landroid/view/View;F)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "elevation"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;F)V"
        }
    .end annotation

    .prologue
    .line 1009589
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 1009590
    invoke-static {p2}, LX/5r2;->a(F)F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setElevation(F)V

    .line 1009591
    :cond_0
    return-void
.end method

.method public setImportantForAccessibility(Landroid/view/View;Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "importantForAccessibility"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1009627
    if-eqz p2, :cond_0

    const-string v0, "auto"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1009628
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 1009629
    :cond_1
    :goto_0
    return-void

    .line 1009630
    :cond_2
    const-string v0, "yes"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1009631
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setImportantForAccessibility(I)V

    goto :goto_0

    .line 1009632
    :cond_3
    const-string v0, "no"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1009633
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/view/View;->setImportantForAccessibility(I)V

    goto :goto_0

    .line 1009634
    :cond_4
    const-string v0, "no-hide-descendants"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1009635
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setImportantForAccessibility(I)V

    goto :goto_0
.end method

.method public setOpacity(Landroid/view/View;F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 1.0f
        name = "opacity"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;F)V"
        }
    .end annotation

    .prologue
    .line 1009587
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 1009588
    return-void
.end method

.method public setRenderToHardwareTexture(Landroid/view/View;Z)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "renderToHardwareTextureAndroid"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .prologue
    .line 1009584
    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1009585
    return-void

    .line 1009586
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRotation(Landroid/view/View;F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "rotation"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;F)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1009582
    invoke-virtual {p1, p2}, Landroid/view/View;->setRotation(F)V

    .line 1009583
    return-void
.end method

.method public setScaleX(Landroid/view/View;F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 1.0f
        name = "scaleX"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;F)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1009580
    invoke-virtual {p1, p2}, Landroid/view/View;->setScaleX(F)V

    .line 1009581
    return-void
.end method

.method public setScaleY(Landroid/view/View;F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 1.0f
        name = "scaleY"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;F)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1009578
    invoke-virtual {p1, p2}, Landroid/view/View;->setScaleY(F)V

    .line 1009579
    return-void
.end method

.method public setTestId(Landroid/view/View;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "testID"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1009576
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1009577
    return-void
.end method

.method public setTransform(Landroid/view/View;LX/5pC;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "transform"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/5pC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1009572
    if-nez p2, :cond_0

    .line 1009573
    invoke-static {p1}, Lcom/facebook/react/uimanager/BaseViewManager;->c(Landroid/view/View;)V

    .line 1009574
    :goto_0
    return-void

    .line 1009575
    :cond_0
    invoke-static {p1, p2}, Lcom/facebook/react/uimanager/BaseViewManager;->a(Landroid/view/View;LX/5pC;)V

    goto :goto_0
.end method

.method public setTranslateX(Landroid/view/View;F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 0.0f
        name = "translateX"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;F)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1009570
    invoke-static {p2}, LX/5r2;->a(F)F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 1009571
    return-void
.end method

.method public setTranslateY(Landroid/view/View;F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 0.0f
        name = "translateY"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;F)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1009568
    invoke-static {p2}, LX/5r2;->a(F)F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1009569
    return-void
.end method

.method public setZIndex(Landroid/view/View;F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "zIndex"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;F)V"
        }
    .end annotation

    .prologue
    .line 1009565
    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1009566
    invoke-static {p1, v0}, Lcom/facebook/react/uimanager/ViewGroupManager;->a(Landroid/view/View;I)V

    .line 1009567
    return-void
.end method
