.class public final Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/5s9;


# direct methods
.method public constructor <init>(LX/5s9;)V
    .locals 0

    .prologue
    .line 1012446
    iput-object p1, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/5s9;B)V
    .locals 0

    .prologue
    .line 1012447
    invoke-direct {p0, p1}, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;-><init>(LX/5s9;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const-wide/16 v8, 0x2000

    .line 1012448
    const-string v1, "DispatchEventsRunnable"

    invoke-static {v8, v9, v1}, LX/018;->a(JLjava/lang/String;)V

    .line 1012449
    const-wide/16 v2, 0x2000

    :try_start_0
    const-string v1, "ScheduleDispatchFrameCallback"

    iget-object v4, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    iget v4, v4, LX/5s9;->p:I

    invoke-static {v2, v3, v1, v4}, LX/018;->e(JLjava/lang/String;I)V

    .line 1012450
    iget-object v1, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    const/4 v2, 0x0

    .line 1012451
    iput-boolean v2, v1, LX/5s9;->o:Z

    .line 1012452
    iget-object v1, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    invoke-static {v1}, LX/5s9;->h(LX/5s9;)I

    .line 1012453
    iget-object v1, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    iget-object v1, v1, LX/5s9;->l:Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1012454
    iget-object v1, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    iget-object v1, v1, LX/5s9;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1012455
    :try_start_1
    iget-object v2, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    iget v2, v2, LX/5s9;->k:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 1012456
    iget-object v2, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    iget-object v2, v2, LX/5s9;->j:[LX/5r0;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    iget v4, v4, LX/5s9;->k:I

    sget-object v5, LX/5s9;->a:Ljava/util/Comparator;

    invoke-static {v2, v3, v4, v5}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 1012457
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    iget v2, v2, LX/5s9;->k:I

    if-ge v0, v2, :cond_2

    .line 1012458
    iget-object v2, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    iget-object v2, v2, LX/5s9;->j:[LX/5r0;

    aget-object v2, v2, v0

    .line 1012459
    if-eqz v2, :cond_1

    .line 1012460
    const-wide/16 v4, 0x2000

    invoke-virtual {v2}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v3

    .line 1012461
    iget v6, v2, LX/5r0;->e:I

    move v6, v6

    .line 1012462
    invoke-static {v4, v5, v3, v6}, LX/018;->e(JLjava/lang/String;I)V

    .line 1012463
    iget-object v3, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    iget-object v3, v3, LX/5s9;->l:Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    invoke-virtual {v2, v3}, LX/5r0;->a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V

    .line 1012464
    invoke-virtual {v2}, LX/5r0;->i()V

    .line 1012465
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1012466
    :cond_2
    iget-object v0, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    invoke-static {v0}, LX/5s9;->h(LX/5s9;)V

    .line 1012467
    iget-object v0, p0, Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;->a:LX/5s9;

    iget-object v0, v0, LX/5s9;->e:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 1012468
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1012469
    invoke-static {v8, v9}, LX/018;->a(J)V

    .line 1012470
    return-void

    .line 1012471
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1012472
    :catchall_1
    move-exception v0

    invoke-static {v8, v9}, LX/018;->a(J)V

    throw v0
.end method
