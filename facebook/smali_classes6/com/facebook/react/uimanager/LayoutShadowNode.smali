.class public Lcom/facebook/react/uimanager/LayoutShadowNode;
.super Lcom/facebook/react/uimanager/ReactShadowNode;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1010110
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;-><init>()V

    return-void
.end method


# virtual methods
.method public setAlignItems(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "alignItems"
    .end annotation

    .prologue
    .line 1010099
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010100
    :goto_0
    return-void

    .line 1010101
    :cond_0
    if-nez p1, :cond_1

    sget-object v0, Lcom/facebook/csslayout/YogaAlign;->STRETCH:Lcom/facebook/csslayout/YogaAlign;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(Lcom/facebook/csslayout/YogaAlign;)V

    goto :goto_0

    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/csslayout/YogaAlign;->valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaAlign;

    move-result-object v0

    goto :goto_1
.end method

.method public setAlignSelf(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "alignSelf"
    .end annotation

    .prologue
    .line 1010102
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010103
    :goto_0
    return-void

    .line 1010104
    :cond_0
    if-nez p1, :cond_1

    sget-object v0, Lcom/facebook/csslayout/YogaAlign;->AUTO:Lcom/facebook/csslayout/YogaAlign;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaAlign;)V

    goto :goto_0

    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/csslayout/YogaAlign;->valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaAlign;

    move-result-object v0

    goto :goto_1
.end method

.method public setAspectRatio(F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = NaNf
        name = "aspectRatio"
    .end annotation

    .prologue
    .line 1010105
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->g(F)V

    .line 1010106
    return-void
.end method

.method public setBorderWidths(IF)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "borderWidth",
            "borderLeftWidth",
            "borderRightWidth",
            "borderTopWidth",
            "borderBottomWidth"
        }
        b = NaNf
    .end annotation

    .prologue
    .line 1010107
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010108
    :goto_0
    return-void

    .line 1010109
    :cond_0
    sget-object v0, LX/5s2;->a:[I

    aget v0, v0, p1

    invoke-static {p2}, LX/5r2;->a(F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->d(IF)V

    goto :goto_0
.end method

.method public setFlex(F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 0.0f
        name = "flex"
    .end annotation

    .prologue
    .line 1010111
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010112
    :goto_0
    return-void

    .line 1010113
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlex(F)V

    goto :goto_0
.end method

.method public setFlexBasis(F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 0.0f
        name = "flexBasis"
    .end annotation

    .prologue
    .line 1010114
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010115
    :goto_0
    return-void

    .line 1010116
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlexBasis(F)V

    goto :goto_0
.end method

.method public setFlexDirection(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "flexDirection"
    .end annotation

    .prologue
    .line 1010117
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010118
    :goto_0
    return-void

    .line 1010119
    :cond_0
    if-nez p1, :cond_1

    sget-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->COLUMN:Lcom/facebook/csslayout/YogaFlexDirection;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaFlexDirection;)V

    goto :goto_0

    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/csslayout/YogaFlexDirection;->valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaFlexDirection;

    move-result-object v0

    goto :goto_1
.end method

.method public setFlexGrow(F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 0.0f
        name = "flexGrow"
    .end annotation

    .prologue
    .line 1010120
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010121
    :goto_0
    return-void

    .line 1010122
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlexGrow(F)V

    goto :goto_0
.end method

.method public setFlexShrink(F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 0.0f
        name = "flexShrink"
    .end annotation

    .prologue
    .line 1010123
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010124
    :goto_0
    return-void

    .line 1010125
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlexShrink(F)V

    goto :goto_0
.end method

.method public setFlexWrap(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "flexWrap"
    .end annotation

    .prologue
    .line 1010089
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010090
    :goto_0
    return-void

    .line 1010091
    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "nowrap"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1010092
    :cond_1
    sget-object v0, Lcom/facebook/csslayout/YogaWrap;->NO_WRAP:Lcom/facebook/csslayout/YogaWrap;

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaWrap;)V

    goto :goto_0

    .line 1010093
    :cond_2
    const-string v0, "wrap"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1010094
    sget-object v0, Lcom/facebook/csslayout/YogaWrap;->WRAP:Lcom/facebook/csslayout/YogaWrap;

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaWrap;)V

    goto :goto_0

    .line 1010095
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown flexWrap value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setHeight(F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = NaNf
        name = "height"
    .end annotation

    .prologue
    .line 1010096
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010097
    :goto_0
    return-void

    .line 1010098
    :cond_0
    invoke-static {p1}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->d(F)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, LX/5r2;->a(F)F

    move-result p1

    goto :goto_1
.end method

.method public setJustifyContent(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "justifyContent"
    .end annotation

    .prologue
    .line 1010054
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010055
    :goto_0
    return-void

    .line 1010056
    :cond_0
    if-nez p1, :cond_1

    sget-object v0, Lcom/facebook/csslayout/YogaJustify;->FLEX_START:Lcom/facebook/csslayout/YogaJustify;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaJustify;)V

    goto :goto_0

    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/csslayout/YogaJustify;->valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaJustify;

    move-result-object v0

    goto :goto_1
.end method

.method public setMargins(IF)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "margin",
            "marginVertical",
            "marginHorizontal",
            "marginLeft",
            "marginRight",
            "marginTop",
            "marginBottom"
        }
        b = NaNf
    .end annotation

    .prologue
    .line 1010057
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010058
    :goto_0
    return-void

    .line 1010059
    :cond_0
    sget-object v0, LX/5s2;->b:[I

    aget v0, v0, p1

    invoke-static {p2}, LX/5r2;->a(F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(IF)V

    goto :goto_0
.end method

.method public setMaxHeight(F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = NaNf
        name = "maxHeight"
    .end annotation

    .prologue
    .line 1010060
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010061
    :goto_0
    return-void

    .line 1010062
    :cond_0
    invoke-static {p1}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->f(F)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, LX/5r2;->a(F)F

    move-result p1

    goto :goto_1
.end method

.method public setMaxWidth(F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = NaNf
        name = "maxWidth"
    .end annotation

    .prologue
    .line 1010063
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010064
    :goto_0
    return-void

    .line 1010065
    :cond_0
    invoke-static {p1}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c(F)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, LX/5r2;->a(F)F

    move-result p1

    goto :goto_1
.end method

.method public setMinHeight(F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = NaNf
        name = "minHeight"
    .end annotation

    .prologue
    .line 1010066
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010067
    :goto_0
    return-void

    .line 1010068
    :cond_0
    invoke-static {p1}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(F)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, LX/5r2;->a(F)F

    move-result p1

    goto :goto_1
.end method

.method public setMinWidth(F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = NaNf
        name = "minWidth"
    .end annotation

    .prologue
    .line 1010069
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010070
    :goto_0
    return-void

    .line 1010071
    :cond_0
    invoke-static {p1}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(F)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, LX/5r2;->a(F)F

    move-result p1

    goto :goto_1
.end method

.method public setOverflow(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "overflow"
    .end annotation

    .prologue
    .line 1010086
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010087
    :goto_0
    return-void

    .line 1010088
    :cond_0
    if-nez p1, :cond_1

    sget-object v0, Lcom/facebook/csslayout/YogaOverflow;->VISIBLE:Lcom/facebook/csslayout/YogaOverflow;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaOverflow;)V

    goto :goto_0

    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/csslayout/YogaOverflow;->valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaOverflow;

    move-result-object v0

    goto :goto_1
.end method

.method public setPaddings(IF)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "padding",
            "paddingVertical",
            "paddingHorizontal",
            "paddingLeft",
            "paddingRight",
            "paddingTop",
            "paddingBottom"
        }
        b = NaNf
    .end annotation

    .prologue
    .line 1010072
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010073
    :goto_0
    return-void

    .line 1010074
    :cond_0
    sget-object v0, LX/5s2;->b:[I

    aget v0, v0, p1

    invoke-static {p2}, LX/1mo;->a(F)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    invoke-virtual {p0, v0, p2}, Lcom/facebook/react/uimanager/ReactShadowNode;->c(IF)V

    goto :goto_0

    :cond_1
    invoke-static {p2}, LX/5r2;->a(F)F

    move-result p2

    goto :goto_1
.end method

.method public setPosition(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "position"
    .end annotation

    .prologue
    .line 1010075
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010076
    :goto_0
    return-void

    .line 1010077
    :cond_0
    if-nez p1, :cond_1

    sget-object v0, Lcom/facebook/csslayout/YogaPositionType;->RELATIVE:Lcom/facebook/csslayout/YogaPositionType;

    .line 1010078
    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaPositionType;)V

    goto :goto_0

    .line 1010079
    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/csslayout/YogaPositionType;->valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaPositionType;

    move-result-object v0

    goto :goto_1
.end method

.method public setPositionValues(IF)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "left",
            "right",
            "top",
            "bottom"
        }
        b = NaNf
    .end annotation

    .prologue
    .line 1010080
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010081
    :goto_0
    return-void

    .line 1010082
    :cond_0
    sget-object v0, LX/5s2;->c:[I

    aget v0, v0, p1

    invoke-static {p2}, LX/1mo;->a(F)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    invoke-virtual {p0, v0, p2}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(IF)V

    goto :goto_0

    :cond_1
    invoke-static {p2}, LX/5r2;->a(F)F

    move-result p2

    goto :goto_1
.end method

.method public setWidth(F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = NaNf
        name = "width"
    .end annotation

    .prologue
    .line 1010083
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010084
    :goto_0
    return-void

    .line 1010085
    :cond_0
    invoke-static {p1}, LX/1mo;->a(F)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(F)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, LX/5r2;->a(F)F

    move-result p1

    goto :goto_1
.end method
