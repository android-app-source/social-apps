.class public abstract Lcom/facebook/react/uimanager/ViewManager;
.super LX/5p5;
.source ""


# annotations
.annotation build Lcom/facebook/react/uimanager/annotations/ReactPropertyHolder;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        "C:",
        "Lcom/facebook/react/uimanager/ReactShadowNode;",
        ">",
        "LX/5p5;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1009553
    invoke-direct {p0}, LX/5p5;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(LX/5rJ;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5rJ;",
            ")TT;"
        }
    .end annotation
.end method

.method public final a(LX/5rJ;LX/5qe;)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5rJ;",
            "LX/5qe;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 1009554
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ViewManager;->a(LX/5rJ;)Landroid/view/View;

    move-result-object v1

    .line 1009555
    invoke-virtual {p0, p1, v1}, Lcom/facebook/react/uimanager/ViewManager;->a(LX/5rJ;Landroid/view/View;)V

    .line 1009556
    instance-of v0, v1, LX/5qg;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1009557
    check-cast v0, LX/5qg;

    invoke-interface {v0, p2}, LX/5qg;->setOnInterceptTouchEventListener(LX/5qd;)V

    .line 1009558
    :cond_0
    return-object v1
.end method

.method public a(LX/5rJ;Landroid/view/View;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5rJ;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 1009559
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 1009560
    return-void
.end method

.method public a(Landroid/view/View;ILX/5pC;)V
    .locals 0
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I",
            "LX/5pC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1009561
    return-void
.end method

.method public final a(Landroid/view/View;LX/5rC;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/5rC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1009562
    invoke-static {p0, p1, p2}, LX/5rp;->a(Lcom/facebook/react/uimanager/ViewManager;Landroid/view/View;LX/5rC;)V

    .line 1009563
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ViewManager;->b(Landroid/view/View;)V

    .line 1009564
    return-void
.end method

.method public abstract a(Landroid/view/View;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation
.end method

.method public b(Landroid/view/View;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 1009552
    return-void
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract i()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+TC;>;"
        }
    .end annotation
.end method

.method public abstract s()Lcom/facebook/react/uimanager/ReactShadowNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TC;"
        }
    .end annotation
.end method

.method public v()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1009547
    const/4 v0, 0x0

    return-object v0
.end method

.method public w()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1009548
    const/4 v0, 0x0

    return-object v0
.end method

.method public x()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1009549
    const/4 v0, 0x0

    return-object v0
.end method

.method public y()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1009550
    const/4 v0, 0x0

    return-object v0
.end method

.method public final z()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1009551
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ViewManager;->i()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, LX/5rp;->a(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
