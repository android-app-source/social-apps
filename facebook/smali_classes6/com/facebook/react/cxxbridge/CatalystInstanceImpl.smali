.class public Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/bridge/CatalystInstance;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final b:LX/5pl;

.field private final c:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "LX/5pU;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final e:Ljava/lang/String;

.field private volatile f:Z

.field private final g:LX/00j;

.field private final h:LX/5pM;

.field private final i:LX/346;

.field private final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/5q4;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/lang/Object;

.field private l:Lcom/facebook/react/bridge/ExecutorToken;

.field public final m:LX/5qE;

.field private final mHybridData:Lcom/facebook/jni/HybridData;

.field private final n:LX/0o1;

.field private o:Z

.field private volatile p:Z

.field private q:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1008858
    const-string v0, "reactnativejnifb"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1008859
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private constructor <init>(LX/5pn;Lcom/facebook/react/cxxbridge/JavaScriptExecutor;LX/5qE;LX/5pM;LX/346;LX/0o1;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1008860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008861
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1008862
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pending_js_calls_instance"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->e:Ljava/lang/String;

    .line 1008863
    iput-boolean v2, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->f:Z

    .line 1008864
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->j:Ljava/util/ArrayList;

    .line 1008865
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->k:Ljava/lang/Object;

    .line 1008866
    iput-boolean v2, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->o:Z

    .line 1008867
    iput-boolean v2, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->p:Z

    .line 1008868
    invoke-static {}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->initHybrid()Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1008869
    new-instance v0, LX/5q3;

    invoke-direct {v0, p0}, LX/5q3;-><init>(Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;)V

    invoke-static {p1, v0}, LX/5pl;->a(LX/5pn;LX/5pj;)LX/5pl;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->b:LX/5pl;

    .line 1008870
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 1008871
    iput-object p3, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->m:LX/5qE;

    .line 1008872
    iput-object p4, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->h:LX/5pM;

    .line 1008873
    iput-object p5, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->i:LX/346;

    .line 1008874
    iput-object p6, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->n:LX/0o1;

    .line 1008875
    new-instance v0, LX/5q2;

    invoke-direct {v0, p0}, LX/5q2;-><init>(Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;)V

    iput-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->g:LX/00j;

    .line 1008876
    new-instance v1, LX/5q0;

    invoke-direct {v1, p0}, LX/5q0;-><init>(Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;)V

    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->b:LX/5pl;

    invoke-virtual {v0}, LX/5pl;->c()Lcom/facebook/react/bridge/queue/MessageQueueThread;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->b:LX/5pl;

    invoke-virtual {v0}, LX/5pl;->b()Lcom/facebook/react/bridge/queue/MessageQueueThread;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->m:LX/5qE;

    invoke-virtual {v0, p0}, LX/5qE;->a(Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;)Lcom/facebook/react/cxxbridge/ModuleRegistryHolder;

    move-result-object v5

    move-object v0, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->initializeBridge(Lcom/facebook/react/cxxbridge/ReactCallback;Lcom/facebook/react/cxxbridge/JavaScriptExecutor;Lcom/facebook/react/bridge/queue/MessageQueueThread;Lcom/facebook/react/bridge/queue/MessageQueueThread;Lcom/facebook/react/cxxbridge/ModuleRegistryHolder;)V

    .line 1008877
    invoke-direct {p0}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->getMainExecutorToken()Lcom/facebook/react/bridge/ExecutorToken;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->l:Lcom/facebook/react/bridge/ExecutorToken;

    .line 1008878
    return-void
.end method

.method public synthetic constructor <init>(LX/5pn;Lcom/facebook/react/cxxbridge/JavaScriptExecutor;LX/5qE;LX/5pM;LX/346;LX/0o1;B)V
    .locals 0

    .prologue
    .line 1008879
    invoke-direct/range {p0 .. p6}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;-><init>(LX/5pn;Lcom/facebook/react/cxxbridge/JavaScriptExecutor;LX/5qE;LX/5pM;LX/346;LX/0o1;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1008880
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->n:LX/0o1;

    invoke-interface {v0, p1}, LX/0o1;->a(Ljava/lang/Exception;)V

    .line 1008881
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->b:LX/5pl;

    invoke-virtual {v0}, LX/5pl;->a()Lcom/facebook/react/bridge/queue/MessageQueueThread;

    move-result-object v0

    new-instance v1, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl$1;

    invoke-direct {v1, p0}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl$1;-><init>(Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;)V

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/queue/MessageQueueThread;->runOnQueue(Ljava/lang/Runnable;)V

    .line 1008882
    return-void
.end method

.method private native callJSCallback(Lcom/facebook/react/bridge/ExecutorToken;ILcom/facebook/react/bridge/NativeArray;)V
.end method

.method private native callJSFunction(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/NativeArray;)V
.end method

.method public static g(Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;)V
    .locals 5

    .prologue
    .line 1008883
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    .line 1008884
    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 1008885
    :goto_0
    const-wide/16 v2, 0x2000

    iget-object v4, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->e:Ljava/lang/String;

    add-int/lit8 v1, v1, 0x1

    invoke-static {v2, v3, v4, v1}, LX/018;->a(JLjava/lang/String;I)V

    .line 1008886
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1008887
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pU;

    .line 1008888
    invoke-interface {v0}, LX/5pU;->b()V

    goto :goto_1

    .line 1008889
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1008890
    :cond_1
    return-void
.end method

.method private native getMainExecutorToken()Lcom/facebook/react/bridge/ExecutorToken;
.end method

.method public static h(Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;)V
    .locals 5

    .prologue
    .line 1008891
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    .line 1008892
    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 1008893
    :goto_0
    const-wide/16 v2, 0x2000

    iget-object v4, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->e:Ljava/lang/String;

    invoke-static {v2, v3, v4, v1}, LX/018;->a(JLjava/lang/String;I)V

    .line 1008894
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1008895
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pU;

    .line 1008896
    invoke-interface {v0}, LX/5pU;->a()V

    goto :goto_1

    .line 1008897
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1008898
    :cond_1
    return-void
.end method

.method private native handleMemoryPressureCritical()V
.end method

.method private native handleMemoryPressureModerate()V
.end method

.method private native handleMemoryPressureUiHidden()V
.end method

.method private static native initHybrid()Lcom/facebook/jni/HybridData;
.end method

.method private native initializeBridge(Lcom/facebook/react/cxxbridge/ReactCallback;Lcom/facebook/react/cxxbridge/JavaScriptExecutor;Lcom/facebook/react/bridge/queue/MessageQueueThread;Lcom/facebook/react/bridge/queue/MessageQueueThread;Lcom/facebook/react/cxxbridge/ModuleRegistryHolder;)V
.end method


# virtual methods
.method public final a(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/react/bridge/JavaScriptModule;",
            ">(",
            "Lcom/facebook/react/bridge/ExecutorToken;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1008899
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->h:LX/5pM;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pM;

    invoke-virtual {v0, p0, p1, p2}, LX/5pM;->a(Lcom/facebook/react/bridge/CatalystInstance;Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/react/bridge/JavaScriptModule;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1008900
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->l:Lcom/facebook/react/bridge/ExecutorToken;

    invoke-virtual {p0, v0, p1}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->a(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1008839
    iget-boolean v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->q:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "JS bundle was already loaded!"

    invoke-static {v0, v2}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1008840
    iput-boolean v1, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->q:Z

    .line 1008841
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->i:LX/346;

    invoke-virtual {v0, p0}, LX/346;->a(Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;)V

    .line 1008842
    iget-object v1, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 1008843
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->p:Z

    .line 1008844
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5q4;

    .line 1008845
    iget-object v3, v0, LX/5q4;->a:Lcom/facebook/react/bridge/ExecutorToken;

    iget-object v4, v0, LX/5q4;->b:Ljava/lang/String;

    iget-object v5, v0, LX/5q4;->c:Ljava/lang/String;

    iget-object v0, v0, LX/5q4;->d:Lcom/facebook/react/bridge/NativeArray;

    invoke-direct {p0, v3, v4, v5, v0}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->callJSFunction(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/NativeArray;)V

    goto :goto_1

    .line 1008846
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1008847
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1008848
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1008849
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1008850
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->g:LX/00j;

    invoke-static {v0}, LX/00k;->a(LX/00j;)V

    .line 1008851
    return-void
.end method

.method public final a(LX/5pR;)V
    .locals 2

    .prologue
    .line 1008852
    iget-boolean v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->f:Z

    if-eqz v0, :cond_0

    .line 1008853
    :goto_0
    return-void

    .line 1008854
    :cond_0
    sget-object v0, LX/5pz;->a:[I

    invoke-virtual {p1}, LX/5pR;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1008855
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->handleMemoryPressureUiHidden()V

    goto :goto_0

    .line 1008856
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->handleMemoryPressureModerate()V

    goto :goto_0

    .line 1008857
    :pswitch_2
    invoke-direct {p0}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->handleMemoryPressureCritical()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/5pU;)V
    .locals 1

    .prologue
    .line 1008800
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 1008801
    return-void
.end method

.method public final b(Ljava/lang/Class;)LX/5p4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/5p4;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1008802
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->m:LX/5qE;

    invoke-virtual {v0, p1}, LX/5qE;->a(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1008803
    invoke-static {}, LX/5qG;->b()V

    .line 1008804
    iget-boolean v2, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->f:Z

    if-eqz v2, :cond_0

    .line 1008805
    :goto_0
    return-void

    .line 1008806
    :cond_0
    iput-boolean v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->f:Z

    .line 1008807
    iget-object v2, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->mHybridData:Lcom/facebook/jni/HybridData;

    invoke-virtual {v2}, Lcom/facebook/jni/HybridData;->resetNative()V

    .line 1008808
    iget-object v2, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->m:LX/5qE;

    invoke-virtual {v2}, LX/5qE;->a()V

    .line 1008809
    iget-object v2, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    move-result v2

    if-nez v2, :cond_1

    .line 1008810
    :goto_1
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1008811
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pU;

    .line 1008812
    invoke-interface {v0}, LX/5pU;->a()V

    goto :goto_2

    :cond_1
    move v0, v1

    .line 1008813
    goto :goto_1

    .line 1008814
    :cond_2
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->g:LX/00j;

    .line 1008815
    sget-object v1, LX/00k;->a:LX/013;

    invoke-virtual {v1, v0}, LX/013;->b(LX/00j;)V

    .line 1008816
    goto :goto_0
.end method

.method public final b(LX/5pU;)V
    .locals 1

    .prologue
    .line 1008817
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1008818
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1008819
    iget-boolean v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->f:Z

    return v0
.end method

.method public final callFunction(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/NativeArray;)V
    .locals 3

    .prologue
    .line 1008820
    iget-boolean v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->f:Z

    if-eqz v0, :cond_0

    .line 1008821
    const-string v0, "React"

    const-string v1, "Calling JS function after bridge has been destroyed."

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008822
    :goto_0
    return-void

    .line 1008823
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->p:Z

    if-nez v0, :cond_2

    .line 1008824
    iget-object v1, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 1008825
    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->p:Z

    if-nez v0, :cond_1

    .line 1008826
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->j:Ljava/util/ArrayList;

    new-instance v2, LX/5q4;

    invoke-direct {v2, p1, p2, p3, p4}, LX/5q4;-><init>(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/NativeArray;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1008827
    monitor-exit v1

    goto :goto_0

    .line 1008828
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1008829
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->callJSFunction(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/NativeArray;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3
    .annotation build Lcom/facebook/react/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1008830
    invoke-static {}, LX/5qG;->b()V

    .line 1008831
    iget-boolean v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->o:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "This catalyst instance has already been initialized"

    invoke-static {v0, v2}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1008832
    iget-boolean v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->p:Z

    const-string v2, "RunJSBundle hasn\'t completed."

    invoke-static {v0, v2}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1008833
    iput-boolean v1, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->o:Z

    .line 1008834
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->m:LX/5qE;

    invoke-virtual {v0}, LX/5qE;->b()V

    .line 1008835
    return-void

    .line 1008836
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()LX/5pk;
    .locals 1

    .prologue
    .line 1008837
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->b:LX/5pl;

    return-object v0
.end method

.method public final f()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LX/5p4;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1008838
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->m:LX/5qE;

    invoke-virtual {v0}, LX/5qE;->d()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public native getJavaScriptContext()J
.end method

.method public final invokeCallback(Lcom/facebook/react/bridge/ExecutorToken;ILcom/facebook/react/bridge/NativeArray;)V
    .locals 2

    .prologue
    .line 1008796
    iget-boolean v0, p0, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->f:Z

    if-eqz v0, :cond_0

    .line 1008797
    const-string v0, "React"

    const-string v1, "Invoking JS callback after bridge has been destroyed."

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008798
    :goto_0
    return-void

    .line 1008799
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;->callJSCallback(Lcom/facebook/react/bridge/ExecutorToken;ILcom/facebook/react/bridge/NativeArray;)V

    goto :goto_0
.end method

.method public native loadScriptFromAssets(Landroid/content/res/AssetManager;Ljava/lang/String;)V
.end method

.method public native loadScriptFromFile(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public native loadScriptFromOptimizedBundle(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public native setGlobalVariable(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public native startProfiler(Ljava/lang/String;)V
.end method

.method public native stopProfiler(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public native supportsProfiling()Z
.end method
