.class public Lcom/facebook/react/cxxbridge/CxxModuleWrapper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5p4;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1008904
    const-string v0, "reactnativejnifb"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1008905
    return-void
.end method

.method public constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0

    .prologue
    .line 1008906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008907
    iput-object p1, p0, Lcom/facebook/react/cxxbridge/CxxModuleWrapper;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1008908
    return-void
.end method

.method private native initHybrid(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public final d()V
    .locals 0

    .prologue
    .line 1008909
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1008910
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1008911
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/CxxModuleWrapper;->mHybridData:Lcom/facebook/jni/HybridData;

    invoke-virtual {v0}, Lcom/facebook/jni/HybridData;->resetNative()V

    .line 1008912
    return-void
.end method

.method public native getConstantsJson()Ljava/lang/String;
.end method

.method public native getMethods()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/5p1;",
            ">;"
        }
    .end annotation
.end method

.method public native getName()Ljava/lang/String;
.end method
