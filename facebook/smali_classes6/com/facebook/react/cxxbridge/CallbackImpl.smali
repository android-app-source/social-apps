.class public Lcom/facebook/react/cxxbridge/CallbackImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/bridge/Callback;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1008744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008745
    iput-object p1, p0, Lcom/facebook/react/cxxbridge/CallbackImpl;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1008746
    return-void
.end method

.method private native nativeInvoke(Lcom/facebook/react/bridge/NativeArray;)V
.end method


# virtual methods
.method public final varargs a([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1008747
    invoke-static {p1}, LX/5op;->a([Ljava/lang/Object;)Lcom/facebook/react/bridge/WritableNativeArray;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/react/cxxbridge/CallbackImpl;->nativeInvoke(Lcom/facebook/react/bridge/NativeArray;)V

    .line 1008748
    return-void
.end method
