.class public Lcom/facebook/react/cxxbridge/JavaModuleWrapper;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final a:Lcom/facebook/react/bridge/CatalystInstance;

.field private final b:LX/5qC;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/5p2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/CatalystInstance;LX/5qC;)V
    .locals 1

    .prologue
    .line 1008989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008990
    iput-object p1, p0, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->a:Lcom/facebook/react/bridge/CatalystInstance;

    .line 1008991
    iput-object p2, p0, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->b:LX/5qC;

    .line 1008992
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->c:Ljava/util/ArrayList;

    .line 1008993
    return-void
.end method


# virtual methods
.method public getConstants()Lcom/facebook/react/bridge/NativeArray;
    .locals 6
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const-wide/16 v4, 0x2000

    .line 1008979
    const-string v0, "Map constants"

    invoke-static {v4, v5, v0}, LX/0BL;->a(JLjava/lang/String;)LX/0BN;

    move-result-object v0

    const-string v1, "moduleName"

    invoke-virtual {p0}, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    move-result-object v0

    invoke-virtual {v0}, LX/0BN;->a()V

    .line 1008980
    invoke-virtual {p0}, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->getModule()LX/5p5;

    move-result-object v0

    invoke-virtual {v0}, LX/5p5;->b()Ljava/util/Map;

    move-result-object v0

    .line 1008981
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1008982
    const-string v1, "WritableNativeMap constants"

    invoke-static {v4, v5, v1}, LX/0BL;->a(JLjava/lang/String;)LX/0BN;

    move-result-object v1

    const-string v2, "moduleName"

    invoke-virtual {p0}, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    move-result-object v1

    invoke-virtual {v1}, LX/0BN;->a()V

    .line 1008983
    :try_start_0
    invoke-static {v0}, LX/5py;->a(Ljava/util/Map;)Lcom/facebook/react/bridge/WritableNativeMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1008984
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1008985
    new-instance v1, Lcom/facebook/react/bridge/WritableNativeArray;

    invoke-direct {v1}, Lcom/facebook/react/bridge/WritableNativeArray;-><init>()V

    .line 1008986
    invoke-virtual {v1, v0}, Lcom/facebook/react/bridge/WritableNativeArray;->a(LX/5pH;)V

    .line 1008987
    return-object v1

    .line 1008988
    :catchall_0
    move-exception v0

    invoke-static {v4, v5}, LX/018;->a(J)V

    throw v0
.end method

.method public getMethodDescriptors()Ljava/util/List;
    .locals 5
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1008970
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1008971
    invoke-virtual {p0}, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->getModule()LX/5p5;

    move-result-object v0

    invoke-virtual {v0}, LX/5p5;->gh_()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1008972
    new-instance v4, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;

    invoke-direct {v4, p0}, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;-><init>(Lcom/facebook/react/cxxbridge/JavaModuleWrapper;)V

    .line 1008973
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v4, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;->name:Ljava/lang/String;

    .line 1008974
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5p1;

    invoke-interface {v1}, LX/5p1;->getType()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;->type:Ljava/lang/String;

    .line 1008975
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5p2;

    .line 1008976
    iget-object v1, p0, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1008977
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1008978
    :cond_0
    return-object v2
.end method

.method public getModule()LX/5p5;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1008994
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->b:LX/5qC;

    invoke-virtual {v0}, LX/5qC;->d()LX/5p4;

    move-result-object v0

    check-cast v0, LX/5p5;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1008941
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->b:LX/5qC;

    .line 1008942
    iget-object p0, v0, LX/5qC;->a:LX/5qA;

    move-object v0, p0

    .line 1008943
    invoke-interface {v0}, LX/5qA;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public invoke(Lcom/facebook/react/bridge/ExecutorToken;ILcom/facebook/react/bridge/ReadableNativeArray;)V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1008967
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 1008968
    :cond_0
    :goto_0
    return-void

    .line 1008969
    :cond_1
    iget-object v0, p0, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5p2;

    iget-object v1, p0, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->a:Lcom/facebook/react/bridge/CatalystInstance;

    invoke-virtual {v0, v1, p1, p3}, LX/5p2;->a(Lcom/facebook/react/bridge/CatalystInstance;Lcom/facebook/react/bridge/ExecutorToken;Lcom/facebook/react/bridge/ReadableNativeArray;)V

    goto :goto_0
.end method

.method public newGetMethodDescriptors()Ljava/util/List;
    .locals 5
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1008945
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1008946
    invoke-virtual {p0}, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->getModule()LX/5p5;

    move-result-object v0

    invoke-virtual {v0}, LX/5p5;->gh_()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1008947
    new-instance v4, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;

    invoke-direct {v4, p0}, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;-><init>(Lcom/facebook/react/cxxbridge/JavaModuleWrapper;)V

    .line 1008948
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v4, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;->name:Ljava/lang/String;

    .line 1008949
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5p1;

    invoke-interface {v1}, LX/5p1;->getType()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;->type:Ljava/lang/String;

    .line 1008950
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5p2;

    .line 1008951
    iget-object v1, v0, LX/5p2;->b:Ljava/lang/reflect/Method;

    move-object v1, v1

    .line 1008952
    iput-object v1, v4, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;->method:Ljava/lang/reflect/Method;

    .line 1008953
    iget-object v1, v0, LX/5p2;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1008954
    iput-object v0, v4, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;->signature:Ljava/lang/String;

    .line 1008955
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1008956
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->getModule()LX/5p5;

    move-result-object v0

    invoke-virtual {v0}, LX/5p5;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1008957
    new-instance v4, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;

    invoke-direct {v4, p0}, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;-><init>(Lcom/facebook/react/cxxbridge/JavaModuleWrapper;)V

    .line 1008958
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v4, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;->name:Ljava/lang/String;

    .line 1008959
    const-string v1, "sync"

    iput-object v1, v4, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;->type:Ljava/lang/String;

    .line 1008960
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5p3;

    .line 1008961
    iget-object v1, v0, LX/5p3;->b:Ljava/lang/reflect/Method;

    move-object v1, v1

    .line 1008962
    iput-object v1, v4, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;->method:Ljava/lang/reflect/Method;

    .line 1008963
    iget-object v1, v0, LX/5p3;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1008964
    iput-object v0, v4, Lcom/facebook/react/cxxbridge/JavaModuleWrapper$MethodDescriptor;->signature:Ljava/lang/String;

    .line 1008965
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1008966
    :cond_1
    return-object v2
.end method

.method public supportsWebWorkers()Z
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1008944
    invoke-virtual {p0}, Lcom/facebook/react/cxxbridge/JavaModuleWrapper;->getModule()LX/5p5;

    move-result-object v0

    invoke-virtual {v0}, LX/5p5;->bP_()Z

    move-result v0

    return v0
.end method
