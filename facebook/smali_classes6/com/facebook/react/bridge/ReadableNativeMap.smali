.class public Lcom/facebook/react/bridge/ReadableNativeMap;
.super Lcom/facebook/react/bridge/NativeMap;
.source ""

# interfaces
.implements LX/5pG;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    .prologue
    .line 1008316
    invoke-static {}, LX/5pZ;->a()V

    .line 1008317
    return-void
.end method

.method public constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0

    .prologue
    .line 1008314
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/NativeMap;-><init>(Lcom/facebook/jni/HybridData;)V

    .line 1008315
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)LX/5pG;
    .locals 1

    .prologue
    .line 1008313
    invoke-virtual {p0, p1}, Lcom/facebook/react/bridge/ReadableNativeMap;->getMap(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableNativeMap;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;
    .locals 1

    .prologue
    .line 1008312
    new-instance v0, Lcom/facebook/react/bridge/ReadableNativeMap$ReadableNativeMapKeySetIterator;

    invoke-direct {v0, p0}, Lcom/facebook/react/bridge/ReadableNativeMap$ReadableNativeMapKeySetIterator;-><init>(Lcom/facebook/react/bridge/ReadableNativeMap;)V

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/String;)LX/5pC;
    .locals 1

    .prologue
    .line 1008311
    invoke-virtual {p0, p1}, Lcom/facebook/react/bridge/ReadableNativeMap;->getArray(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableNativeArray;

    move-result-object v0

    return-object v0
.end method

.method public native getArray(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableNativeArray;
.end method

.method public native getBoolean(Ljava/lang/String;)Z
.end method

.method public native getDouble(Ljava/lang/String;)D
.end method

.method public native getInt(Ljava/lang/String;)I
.end method

.method public native getMap(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableNativeMap;
.end method

.method public native getString(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public native getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;
.end method

.method public native hasKey(Ljava/lang/String;)Z
.end method

.method public native isNull(Ljava/lang/String;)Z
.end method
