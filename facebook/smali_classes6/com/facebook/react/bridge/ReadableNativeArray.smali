.class public Lcom/facebook/react/bridge/ReadableNativeArray;
.super Lcom/facebook/react/bridge/NativeArray;
.source ""

# interfaces
.implements LX/5pC;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    .prologue
    .line 1008305
    invoke-static {}, LX/5pZ;->a()V

    .line 1008306
    return-void
.end method

.method public constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0

    .prologue
    .line 1008303
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/NativeArray;-><init>(Lcom/facebook/jni/HybridData;)V

    .line 1008304
    return-void
.end method


# virtual methods
.method public final synthetic a(I)LX/5pG;
    .locals 1

    .prologue
    .line 1008301
    invoke-virtual {p0, p1}, Lcom/facebook/react/bridge/ReadableNativeArray;->getMap(I)Lcom/facebook/react/bridge/ReadableNativeMap;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(I)LX/5pC;
    .locals 1

    .prologue
    .line 1008302
    invoke-virtual {p0, p1}, Lcom/facebook/react/bridge/ReadableNativeArray;->getArray(I)Lcom/facebook/react/bridge/ReadableNativeArray;

    move-result-object v0

    return-object v0
.end method

.method public native getArray(I)Lcom/facebook/react/bridge/ReadableNativeArray;
.end method

.method public native getBoolean(I)Z
.end method

.method public native getDouble(I)D
.end method

.method public native getInt(I)I
.end method

.method public native getMap(I)Lcom/facebook/react/bridge/ReadableNativeMap;
.end method

.method public native getString(I)Ljava/lang/String;
.end method

.method public native getType(I)Lcom/facebook/react/bridge/ReadableType;
.end method

.method public native isNull(I)Z
.end method

.method public native size()I
.end method
