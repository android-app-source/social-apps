.class public Lcom/facebook/react/bridge/PromiseImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5pW;


# instance fields
.field private a:Lcom/facebook/react/bridge/Callback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Lcom/facebook/react/bridge/Callback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 0
    .param p1    # Lcom/facebook/react/bridge/Callback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/react/bridge/Callback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1008187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008188
    iput-object p1, p0, Lcom/facebook/react/bridge/PromiseImpl;->a:Lcom/facebook/react/bridge/Callback;

    .line 1008189
    iput-object p2, p0, Lcom/facebook/react/bridge/PromiseImpl;->b:Lcom/facebook/react/bridge/Callback;

    .line 1008190
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1008184
    iget-object v0, p0, Lcom/facebook/react/bridge/PromiseImpl;->a:Lcom/facebook/react/bridge/Callback;

    if-eqz v0, :cond_0

    .line 1008185
    iget-object v0, p0, Lcom/facebook/react/bridge/PromiseImpl;->a:Lcom/facebook/react/bridge/Callback;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1008186
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1008182
    invoke-virtual {p0, p1, p2}, Lcom/facebook/react/bridge/PromiseImpl;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008183
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1008180
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/bridge/PromiseImpl;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008181
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1008178
    const-string v0, "EUNSPECIFIED"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/bridge/PromiseImpl;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008179
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1008170
    iget-object v0, p0, Lcom/facebook/react/bridge/PromiseImpl;->b:Lcom/facebook/react/bridge/Callback;

    if-eqz v0, :cond_1

    .line 1008171
    if-nez p1, :cond_0

    .line 1008172
    const-string p1, "EUNSPECIFIED"

    .line 1008173
    :cond_0
    new-instance v0, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v0}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 1008174
    const-string v1, "code"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/react/bridge/WritableNativeMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008175
    const-string v1, "message"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/react/bridge/WritableNativeMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008176
    iget-object v1, p0, Lcom/facebook/react/bridge/PromiseImpl;->b:Lcom/facebook/react/bridge/Callback;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1008177
    :cond_1
    return-void
.end method
