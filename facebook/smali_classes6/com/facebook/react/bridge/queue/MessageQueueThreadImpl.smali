.class public Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/bridge/queue/MessageQueueThread;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Landroid/os/Looper;

.field private final c:LX/5pf;

.field private final d:Ljava/lang/String;

.field private volatile e:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;Landroid/os/Looper;LX/5pj;)V
    .locals 2

    .prologue
    .line 1008447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008448
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->e:Z

    .line 1008449
    iput-object p1, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->a:Ljava/lang/String;

    .line 1008450
    iput-object p2, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->b:Landroid/os/Looper;

    .line 1008451
    new-instance v0, LX/5pf;

    invoke-direct {v0, p2, p3}, LX/5pf;-><init>(Landroid/os/Looper;LX/5pj;)V

    iput-object v0, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->c:LX/5pf;

    .line 1008452
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Expected to be called from the \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1008453
    iget-object v1, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1008454
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' thread!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->d:Ljava/lang/String;

    .line 1008455
    return-void
.end method

.method public static a(LX/5pi;LX/5pj;)Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;
    .locals 6

    .prologue
    .line 1008402
    sget-object v0, LX/5pg;->a:[I

    .line 1008403
    iget-object v1, p0, LX/5pi;->b:LX/5ph;

    move-object v1, v1

    .line 1008404
    invoke-virtual {v1}, LX/5ph;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1008405
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown thread type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1008406
    iget-object v2, p0, LX/5pi;->b:LX/5ph;

    move-object v2, v2

    .line 1008407
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008408
    :pswitch_0
    iget-object v0, p0, LX/5pi;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1008409
    invoke-static {v0, p1}, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->a(Ljava/lang/String;LX/5pj;)Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    move-result-object v0

    .line 1008410
    :goto_0
    return-object v0

    .line 1008411
    :pswitch_1
    iget-object v0, p0, LX/5pi;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1008412
    iget-wide v4, p0, LX/5pi;->d:J

    move-wide v2, v4

    .line 1008413
    invoke-static {v0, v2, v3, p1}, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->a(Ljava/lang/String;JLX/5pj;)Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;JLX/5pj;)Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;
    .locals 9

    .prologue
    .line 1008439
    new-instance v0, LX/5pw;

    invoke-direct {v0}, LX/5pw;-><init>()V

    .line 1008440
    new-instance v7, LX/5pw;

    invoke-direct {v7}, LX/5pw;-><init>()V

    .line 1008441
    const/4 v1, 0x0

    new-instance v2, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl$3;

    invoke-direct {v2, v0, v7}, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl$3;-><init>(LX/5pw;LX/5pw;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mqt_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const v6, -0x1606871

    move-wide v4, p1

    invoke-static/range {v1 .. v6}, LX/00l;->a(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;JI)Ljava/lang/Thread;

    move-result-object v1

    .line 1008442
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1008443
    invoke-virtual {v0}, LX/5pw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    .line 1008444
    new-instance v1, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    invoke-direct {v1, p0, v0, p3}, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;-><init>(Ljava/lang/String;Landroid/os/Looper;LX/5pj;)V

    .line 1008445
    invoke-virtual {v7, v1}, LX/5pw;->a(Ljava/lang/Object;)V

    .line 1008446
    return-object v1
.end method

.method private static a(Ljava/lang/String;LX/5pj;)Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;
    .locals 2

    .prologue
    .line 1008432
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    .line 1008433
    new-instance v1, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;

    invoke-direct {v1, p0, v0, p1}, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;-><init>(Ljava/lang/String;Landroid/os/Looper;LX/5pj;)V

    .line 1008434
    invoke-static {}, LX/5pe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1008435
    const/4 v0, -0x4

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 1008436
    invoke-static {v1}, Lcom/facebook/react/bridge/queue/MessageQueueThreadRegistry;->a(Lcom/facebook/react/bridge/queue/MessageQueueThread;)V

    .line 1008437
    :goto_0
    return-object v1

    .line 1008438
    :cond_0
    new-instance v0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl$2;

    invoke-direct {v0, v1}, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl$2;-><init>(Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;)V

    invoke-static {v0}, LX/5pe;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public assertIsOnThread()V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1008430
    invoke-virtual {p0}, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->isOnThread()Z

    move-result v0

    iget-object v1, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->d:Ljava/lang/String;

    invoke-static {v0, v1}, LX/5pd;->a(ZLjava/lang/String;)V

    .line 1008431
    return-void
.end method

.method public callOnQueue(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Ljava/util/concurrent/Future",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1008427
    new-instance v0, LX/5pw;

    invoke-direct {v0}, LX/5pw;-><init>()V

    .line 1008428
    new-instance v1, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl$1;-><init>(Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;LX/5pw;Ljava/util/concurrent/Callable;)V

    invoke-virtual {p0, v1}, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->runOnQueue(Ljava/lang/Runnable;)V

    .line 1008429
    return-object v0
.end method

.method public isOnThread()Z
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1008426
    iget-object v0, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->b:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public quitSynchronous()V
    .locals 3
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1008420
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->e:Z

    .line 1008421
    iget-object v0, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->b:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 1008422
    iget-object v0, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->b:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1008423
    :try_start_0
    iget-object v0, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->b:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1008424
    :cond_0
    return-void

    .line 1008425
    :catch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got interrupted waiting to join thread "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public runOnQueue(Ljava/lang/Runnable;)V
    .locals 3
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1008414
    iget-boolean v0, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->e:Z

    if-eqz v0, :cond_0

    .line 1008415
    const-string v0, "React"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tried to enqueue runnable on already finished thread: \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1008416
    iget-object v2, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1008417
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "... dropping Runnable."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008418
    :cond_0
    iget-object v0, p0, Lcom/facebook/react/bridge/queue/MessageQueueThreadImpl;->c:LX/5pf;

    const v1, -0x412802a1

    invoke-static {v0, p1, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1008419
    return-void
.end method
