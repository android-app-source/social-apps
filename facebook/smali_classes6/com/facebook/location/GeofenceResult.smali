.class public Lcom/facebook/location/GeofenceResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/location/GeofenceResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/location/ImmutableLocation;

.field public final b:J

.field public final c:J

.field public final d:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1111070
    new-instance v0, LX/6ZJ;

    invoke-direct {v0}, LX/6ZJ;-><init>()V

    sput-object v0, Lcom/facebook/location/GeofenceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1111064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1111065
    const-class v0, Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    iput-object v0, p0, Lcom/facebook/location/GeofenceResult;->a:Lcom/facebook/location/ImmutableLocation;

    .line 1111066
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/location/GeofenceResult;->b:J

    .line 1111067
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/location/GeofenceResult;->c:J

    .line 1111068
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/location/GeofenceResult;->d:F

    .line 1111069
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1111063
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1111058
    iget-object v0, p0, Lcom/facebook/location/GeofenceResult;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1111059
    iget-wide v0, p0, Lcom/facebook/location/GeofenceResult;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1111060
    iget-wide v0, p0, Lcom/facebook/location/GeofenceResult;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1111061
    iget v0, p0, Lcom/facebook/location/GeofenceResult;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1111062
    return-void
.end method
