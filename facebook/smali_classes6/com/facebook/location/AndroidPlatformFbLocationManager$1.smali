.class public final Lcom/facebook/location/AndroidPlatformFbLocationManager$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Rf;

.field public final synthetic b:LX/6Z8;


# direct methods
.method public constructor <init>(LX/6Z8;LX/0Rf;)V
    .locals 0

    .prologue
    .line 1110938
    iput-object p1, p0, Lcom/facebook/location/AndroidPlatformFbLocationManager$1;->b:LX/6Z8;

    iput-object p2, p0, Lcom/facebook/location/AndroidPlatformFbLocationManager$1;->a:LX/0Rf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1110939
    iget-object v6, p0, Lcom/facebook/location/AndroidPlatformFbLocationManager$1;->b:LX/6Z8;

    monitor-enter v6

    .line 1110940
    :try_start_0
    iget-object v0, p0, Lcom/facebook/location/AndroidPlatformFbLocationManager$1;->b:LX/6Z8;

    iget-object v0, v0, LX/6Z8;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1110941
    monitor-exit v6

    .line 1110942
    :goto_0
    return-void

    .line 1110943
    :cond_0
    iget-object v0, p0, Lcom/facebook/location/AndroidPlatformFbLocationManager$1;->a:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1110944
    iget-object v0, p0, Lcom/facebook/location/AndroidPlatformFbLocationManager$1;->b:LX/6Z8;

    iget-object v0, v0, LX/6Z8;->c:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/facebook/location/AndroidPlatformFbLocationManager$1;->b:LX/6Z8;

    iget-object v2, v2, LX/6Z8;->d:LX/2vk;

    iget-wide v2, v2, LX/2vk;->e:J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/location/AndroidPlatformFbLocationManager$1;->b:LX/6Z8;

    iget-object v5, v5, LX/6Z8;->f:LX/6Z7;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    goto :goto_1

    .line 1110945
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
