.class public Lcom/facebook/location/Coordinates;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/location/Coordinates;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:D

.field public final b:D

.field public final c:Ljava/lang/Float;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1111014
    new-instance v0, LX/6ZC;

    invoke-direct {v0}, LX/6ZC;-><init>()V

    sput-object v0, Lcom/facebook/location/Coordinates;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1111015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1111016
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/location/Coordinates;->a:D

    .line 1111017
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/location/Coordinates;->b:D

    .line 1111018
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    iput-object v0, p0, Lcom/facebook/location/Coordinates;->c:Ljava/lang/Float;

    .line 1111019
    return-void
.end method

.method public static newBuilder()LX/6ZD;
    .locals 1

    .prologue
    .line 1111020
    new-instance v0, LX/6ZD;

    invoke-direct {v0}, LX/6ZD;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1111021
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1111022
    iget-wide v0, p0, Lcom/facebook/location/Coordinates;->a:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1111023
    iget-wide v0, p0, Lcom/facebook/location/Coordinates;->b:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1111024
    iget-object v0, p0, Lcom/facebook/location/Coordinates;->c:Ljava/lang/Float;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1111025
    return-void
.end method
