.class public final Lcom/facebook/location/MockStaticMpkFbLocationManager$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/35T;


# direct methods
.method public constructor <init>(LX/35T;)V
    .locals 0

    .prologue
    .line 1111116
    iput-object p1, p0, Lcom/facebook/location/MockStaticMpkFbLocationManager$1;->a:LX/35T;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1111117
    iget-object v0, p0, Lcom/facebook/location/MockStaticMpkFbLocationManager$1;->a:LX/35T;

    iget-object v0, v0, LX/35T;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1111118
    iget-object v0, p0, Lcom/facebook/location/MockStaticMpkFbLocationManager$1;->a:LX/35T;

    const-wide v2, 0x4042be146a1a500dL    # 37.484998

    const-wide v4, -0x3fa17683be6601bdL    # -122.148209

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/location/ImmutableLocation;->a(DD)LX/0z7;

    move-result-object v1

    sget-object v2, LX/35T;->a:Ljava/lang/String;

    .line 1111119
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111120
    iget-object v3, v1, LX/0z7;->a:Landroid/location/Location;

    invoke-virtual {v3, v2}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    .line 1111121
    move-object v1, v1

    .line 1111122
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, LX/0z7;->b(F)LX/0z7;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/location/MockStaticMpkFbLocationManager$1;->a:LX/35T;

    iget-object v2, v2, LX/35T;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/0z7;->c(J)LX/0z7;

    move-result-object v1

    invoke-virtual {v1}, LX/0z7;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/ImmutableLocation;)V

    .line 1111123
    iget-object v0, p0, Lcom/facebook/location/MockStaticMpkFbLocationManager$1;->a:LX/35T;

    iget-object v1, p0, Lcom/facebook/location/MockStaticMpkFbLocationManager$1;->a:LX/35T;

    iget-wide v2, v1, LX/35T;->d:J

    invoke-static {v0, v2, v3}, LX/35T;->a$redex0(LX/35T;J)V

    .line 1111124
    :cond_0
    return-void
.end method
