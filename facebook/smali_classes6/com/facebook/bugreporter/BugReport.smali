.class public Lcom/facebook/bugreporter/BugReport;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/flatbuffers/Flattenable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/bugreporter/BugReport;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Ljava/lang/String;

.field public c:Landroid/net/Uri;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:LX/6Fb;

.field public r:Ljava/lang/String;

.field public s:I

.field public t:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1067830
    new-instance v0, LX/6FP;

    invoke-direct {v0}, LX/6FP;-><init>()V

    sput-object v0, Lcom/facebook/bugreporter/BugReport;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6FU;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1067831
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067832
    iget-object v1, p1, LX/6FU;->a:Landroid/net/Uri;

    move-object v1, v1

    .line 1067833
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->a:Landroid/net/Uri;

    .line 1067834
    iget-object v1, p1, LX/6FU;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1067835
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->b:Ljava/lang/String;

    .line 1067836
    iget-object v1, p1, LX/6FU;->c:Landroid/net/Uri;

    move-object v1, v1

    .line 1067837
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->c:Landroid/net/Uri;

    .line 1067838
    invoke-virtual {p1}, LX/6FU;->d()LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->d:LX/0Px;

    .line 1067839
    iget-object v1, p1, LX/6FU;->e:LX/0P1;

    move-object v1, v1

    .line 1067840
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->e:LX/0P1;

    .line 1067841
    iget-object v1, p1, LX/6FU;->f:LX/0P1;

    move-object v1, v1

    .line 1067842
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->t:LX/0P1;

    .line 1067843
    iget-object v1, p1, LX/6FU;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1067844
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->g:Ljava/lang/String;

    .line 1067845
    iget-object v1, p1, LX/6FU;->i:Ljava/lang/String;

    move-object v1, v1

    .line 1067846
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->h:Ljava/lang/String;

    .line 1067847
    iget-object v1, p1, LX/6FU;->j:Ljava/lang/String;

    move-object v1, v1

    .line 1067848
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->i:Ljava/lang/String;

    .line 1067849
    iget-object v1, p1, LX/6FU;->k:Ljava/lang/String;

    move-object v1, v1

    .line 1067850
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->j:Ljava/lang/String;

    .line 1067851
    iget-object v1, p1, LX/6FU;->l:Ljava/lang/String;

    move-object v1, v1

    .line 1067852
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->k:Ljava/lang/String;

    .line 1067853
    iget-object v1, p1, LX/6FU;->m:Ljava/lang/String;

    move-object v1, v1

    .line 1067854
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->l:Ljava/lang/String;

    .line 1067855
    iget-object v1, p1, LX/6FU;->n:Ljava/lang/String;

    move-object v1, v1

    .line 1067856
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->m:Ljava/lang/String;

    .line 1067857
    iget-object v1, p1, LX/6FU;->o:Ljava/lang/String;

    move-object v1, v1

    .line 1067858
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->n:Ljava/lang/String;

    .line 1067859
    iget-object v1, p1, LX/6FU;->p:Ljava/lang/String;

    move-object v1, v1

    .line 1067860
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->o:Ljava/lang/String;

    .line 1067861
    iget-object v1, p1, LX/6FU;->q:Ljava/lang/String;

    move-object v1, v1

    .line 1067862
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->p:Ljava/lang/String;

    .line 1067863
    iget-object v1, p1, LX/6FU;->r:LX/6Fb;

    move-object v1, v1

    .line 1067864
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->q:LX/6Fb;

    .line 1067865
    iget-object v1, p1, LX/6FU;->g:LX/0P1;

    move-object v1, v1

    .line 1067866
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->f:LX/0P1;

    .line 1067867
    iget-object v1, p1, LX/6FU;->s:Ljava/lang/String;

    move-object v1, v1

    .line 1067868
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->r:Ljava/lang/String;

    .line 1067869
    iget v1, p1, LX/6FU;->t:I

    move v1, v1

    .line 1067870
    iput v1, p0, Lcom/facebook/bugreporter/BugReport;->s:I

    .line 1067871
    iget-object v1, p1, LX/6FU;->u:Ljava/lang/String;

    move-object v1, v1

    .line 1067872
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->u:Ljava/lang/String;

    .line 1067873
    iget-object v1, p1, LX/6FU;->v:Ljava/lang/String;

    move-object v1, v1

    .line 1067874
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->v:Ljava/lang/String;

    .line 1067875
    iget-object v1, p1, LX/6FU;->w:Ljava/lang/String;

    move-object v1, v1

    .line 1067876
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->w:Ljava/lang/String;

    .line 1067877
    iget-boolean v1, p1, LX/6FU;->x:Z

    move v1, v1

    .line 1067878
    iput-boolean v1, p0, Lcom/facebook/bugreporter/BugReport;->x:Z

    .line 1067879
    iget-object v1, p0, Lcom/facebook/bugreporter/BugReport;->a:Landroid/net/Uri;

    invoke-static {v1, v0}, Lcom/facebook/bugreporter/BugReport;->a(Landroid/net/Uri;Z)V

    .line 1067880
    iget-object v1, p0, Lcom/facebook/bugreporter/BugReport;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1067881
    invoke-static {v0, v3}, Lcom/facebook/bugreporter/BugReport;->a(Landroid/net/Uri;Z)V

    .line 1067882
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1067883
    :cond_0
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->c:Landroid/net/Uri;

    invoke-static {v0, v3}, Lcom/facebook/bugreporter/BugReport;->a(Landroid/net/Uri;Z)V

    .line 1067884
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1067885
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1067886
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067887
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->a:Landroid/net/Uri;

    .line 1067888
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->b:Ljava/lang/String;

    .line 1067889
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->c:Landroid/net/Uri;

    .line 1067890
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->g:Ljava/lang/String;

    .line 1067891
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->h:Ljava/lang/String;

    .line 1067892
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->i:Ljava/lang/String;

    .line 1067893
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->j:Ljava/lang/String;

    .line 1067894
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->k:Ljava/lang/String;

    .line 1067895
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->l:Ljava/lang/String;

    .line 1067896
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->m:Ljava/lang/String;

    .line 1067897
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->n:Ljava/lang/String;

    .line 1067898
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->o:Ljava/lang/String;

    .line 1067899
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->p:Ljava/lang/String;

    .line 1067900
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/6Fb;

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->q:LX/6Fb;

    .line 1067901
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->r:Ljava/lang/String;

    .line 1067902
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->w:Ljava/lang/String;

    .line 1067903
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    .line 1067904
    sget-object v1, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1067905
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->d:LX/0Px;

    .line 1067906
    invoke-static {p1}, Lcom/facebook/bugreporter/BugReport;->a(Landroid/os/Parcel;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->e:LX/0P1;

    .line 1067907
    invoke-static {p1}, Lcom/facebook/bugreporter/BugReport;->a(Landroid/os/Parcel;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->t:LX/0P1;

    .line 1067908
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1067909
    const-class v1, Lcom/facebook/bugreporter/BugReport;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1067910
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->f:LX/0P1;

    .line 1067911
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bugreporter/BugReport;->s:I

    .line 1067912
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->u:Ljava/lang/String;

    .line 1067913
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->v:Ljava/lang/String;

    .line 1067914
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/bugreporter/BugReport;->x:Z

    .line 1067915
    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    .line 1067916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067917
    invoke-static {p1}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/bugreporter/BugReport;->a(Ljava/nio/ByteBuffer;I)V

    .line 1067918
    return-void
.end method

.method private static a(Landroid/os/Parcel;)LX/0P1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1067919
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1067920
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1067921
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 1067922
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1067923
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1067924
    invoke-virtual {v1, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1067925
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1067926
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/net/Uri;Z)V
    .locals 2

    .prologue
    .line 1067927
    if-nez p1, :cond_0

    .line 1067928
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1067929
    :cond_0
    if-eqz p0, :cond_1

    .line 1067930
    const-string v0, "file"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1067931
    invoke-virtual {p0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1067932
    :cond_1
    return-void
.end method

.method private static a(Landroid/os/Parcel;LX/0P1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1067933
    if-eqz p1, :cond_0

    .line 1067934
    invoke-virtual {p1}, LX/0P1;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067935
    invoke-virtual {p1}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1067936
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067937
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 1067938
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067939
    :cond_1
    return-void
.end method

.method public static newBuilder()LX/6FU;
    .locals 1

    .prologue
    .line 1067940
    new-instance v0, LX/6FU;

    invoke-direct {v0}, LX/6FU;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 1

    .prologue
    .line 1067941
    invoke-static {p0, p1}, LX/7YH;->a(Lcom/facebook/bugreporter/BugReport;LX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/15i;I)V
    .locals 2

    .prologue
    .line 1067828
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "initFromMutableFlatBuffer is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 4

    .prologue
    .line 1067942
    const/4 v1, 0x0

    .line 1067943
    const/4 v0, 0x1

    sget-object v2, LX/4Bw;->a:LX/4Bw;

    invoke-static {p1, p2, v0, v2}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILX/4Bv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->a:Landroid/net/Uri;

    .line 1067944
    const/4 v0, 0x2

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->b:Ljava/lang/String;

    .line 1067945
    const/4 v0, 0x3

    sget-object v2, LX/4Bw;->a:LX/4Bw;

    invoke-static {p1, p2, v0, v2}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILX/4Bv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->c:Landroid/net/Uri;

    .line 1067946
    const/4 v0, 0x4

    const-class v2, Ljava/util/ArrayList;

    sget-object v3, LX/4Bw;->a:LX/4Bw;

    invoke-static {p1, p2, v0, v2, v3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILjava/lang/Class;LX/4Bv;)Ljava/util/List;

    move-result-object v0

    .line 1067947
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->d:LX/0Px;

    .line 1067948
    const/4 v0, 0x5

    const-class v2, Ljava/util/HashMap;

    invoke-static {p1, p2, v0, v2}, LX/0ah;->g(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/util/Map;

    move-result-object v0

    .line 1067949
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->e:LX/0P1;

    .line 1067950
    const/4 v0, 0x6

    const-class v2, Ljava/util/HashMap;

    invoke-static {p1, p2, v0, v2}, LX/0ah;->g(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/util/Map;

    move-result-object v0

    .line 1067951
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->f:LX/0P1;

    .line 1067952
    const/4 v0, 0x7

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->g:Ljava/lang/String;

    .line 1067953
    const/16 v0, 0x8

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->h:Ljava/lang/String;

    .line 1067954
    const/16 v0, 0x9

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->i:Ljava/lang/String;

    .line 1067955
    const/16 v0, 0xa

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->j:Ljava/lang/String;

    .line 1067956
    const/16 v0, 0xb

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->k:Ljava/lang/String;

    .line 1067957
    const/16 v0, 0xc

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->l:Ljava/lang/String;

    .line 1067958
    const/16 v0, 0xd

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->m:Ljava/lang/String;

    .line 1067959
    const/16 v0, 0xe

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->n:Ljava/lang/String;

    .line 1067960
    const/16 v0, 0xf

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->o:Ljava/lang/String;

    .line 1067961
    const/16 v0, 0x10

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->p:Ljava/lang/String;

    .line 1067962
    const/16 v0, 0x11

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    .line 1067963
    if-nez v0, :cond_4

    .line 1067964
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->q:LX/6Fb;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1067965
    :goto_3
    const/16 v0, 0x12

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->r:Ljava/lang/String;

    .line 1067966
    const/16 v0, 0x13

    const/4 v2, 0x0

    invoke-static {p1, p2, v0, v2}, LX/0ah;->a(Ljava/nio/ByteBuffer;III)I

    move-result v0

    iput v0, p0, Lcom/facebook/bugreporter/BugReport;->s:I

    .line 1067967
    const/16 v0, 0x14

    const-class v2, Ljava/util/HashMap;

    invoke-static {p1, p2, v0, v2}, LX/0ah;->g(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/util/Map;

    move-result-object v0

    .line 1067968
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    :cond_0
    iput-object v1, p0, Lcom/facebook/bugreporter/BugReport;->t:LX/0P1;

    .line 1067969
    const/16 v0, 0x15

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->u:Ljava/lang/String;

    .line 1067970
    const/16 v0, 0x16

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->v:Ljava/lang/String;

    .line 1067971
    const/16 v0, 0x17

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->w:Ljava/lang/String;

    .line 1067972
    const/16 v0, 0x18

    invoke-static {p1, p2, v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/bugreporter/BugReport;->x:Z

    .line 1067973
    return-void

    :cond_1
    move-object v0, v1

    .line 1067974
    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    .line 1067975
    goto/16 :goto_1

    :cond_3
    move-object v0, v1

    .line 1067976
    goto/16 :goto_2

    .line 1067977
    :cond_4
    :try_start_1
    const-class v2, LX/6Fb;

    invoke-static {v2, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Fb;

    iput-object v0, p0, Lcom/facebook/bugreporter/BugReport;->q:LX/6Fb;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    goto :goto_3
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067978
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1067979
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->c:Landroid/net/Uri;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1067980
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->d:LX/0Px;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1067981
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1067829
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->e:LX/0P1;

    return-object v0
.end method

.method public final f()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1067982
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->t:LX/0P1;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067789
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067790
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067791
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067792
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067793
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1067794
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1067795
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067796
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067788
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067797
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final q()LX/6Fb;
    .locals 1

    .prologue
    .line 1067798
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->q:LX/6Fb;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067799
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 1067800
    iget v0, p0, Lcom/facebook/bugreporter/BugReport;->s:I

    return v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067801
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1067802
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1067803
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067804
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1067805
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067806
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067807
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067808
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067809
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067810
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067811
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067812
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067813
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067814
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067815
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->q:LX/6Fb;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1067816
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067817
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067818
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1067819
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->e:LX/0P1;

    invoke-static {p1, v0}, Lcom/facebook/bugreporter/BugReport;->a(Landroid/os/Parcel;LX/0P1;)V

    .line 1067820
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->t:LX/0P1;

    invoke-static {p1, v0}, Lcom/facebook/bugreporter/BugReport;->a(Landroid/os/Parcel;LX/0P1;)V

    .line 1067821
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->f:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1067822
    iget v0, p0, Lcom/facebook/bugreporter/BugReport;->s:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1067823
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067824
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1067825
    iget-boolean v0, p0, Lcom/facebook/bugreporter/BugReport;->x:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1067826
    return-void
.end method

.method public final x()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1067827
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->f:LX/0P1;

    return-object v0
.end method
