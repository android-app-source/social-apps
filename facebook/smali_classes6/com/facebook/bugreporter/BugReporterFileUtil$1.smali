.class public final Lcom/facebook/bugreporter/BugReporterFileUtil$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:LX/6G3;


# direct methods
.method public constructor <init>(LX/6G3;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1069746
    iput-object p1, p0, Lcom/facebook/bugreporter/BugReporterFileUtil$1;->b:LX/6G3;

    iput-object p2, p0, Lcom/facebook/bugreporter/BugReporterFileUtil$1;->a:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1069747
    iget-object v1, p0, Lcom/facebook/bugreporter/BugReporterFileUtil$1;->a:Landroid/net/Uri;

    .line 1069748
    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/net/URI;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 1069749
    invoke-static {v0}, LX/6G3;->a(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1069750
    :goto_0
    return-void

    .line 1069751
    :catch_0
    move-exception v0

    .line 1069752
    sget-object v2, LX/6G3;->a:Ljava/lang/Class;

    const-string p0, "Cannot parse Bug Report Directory URI"

    invoke-static {v2, p0, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
