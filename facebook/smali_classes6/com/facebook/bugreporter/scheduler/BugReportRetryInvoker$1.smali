.class public final Lcom/facebook/bugreporter/scheduler/BugReportRetryInvoker$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6H2;


# direct methods
.method public constructor <init>(LX/6H2;)V
    .locals 0

    .prologue
    .line 1071207
    iput-object p1, p0, Lcom/facebook/bugreporter/scheduler/BugReportRetryInvoker$1;->a:LX/6H2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1071208
    iget-object v0, p0, Lcom/facebook/bugreporter/scheduler/BugReportRetryInvoker$1;->a:LX/6H2;

    iget-object v0, v0, LX/6H2;->a:LX/6Fa;

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1071209
    iget-object v1, v0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/6Fa;->b:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v1

    .line 1071210
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v4

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1071211
    new-instance v7, Ljava/io/File;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v7, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1071212
    invoke-static {v0, v7}, LX/6Fa;->a(LX/6Fa;Ljava/io/File;)Z

    move-result v2

    .line 1071213
    if-eqz v2, :cond_0

    .line 1071214
    iget-object v7, v0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Tn;

    invoke-interface {v7, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1071215
    :cond_0
    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    move v1, v4

    :goto_1
    move v3, v1

    .line 1071216
    goto :goto_0

    :cond_1
    move v1, v5

    .line 1071217
    goto :goto_1

    .line 1071218
    :cond_2
    invoke-static {v0}, LX/6Fa;->d(LX/6Fa;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v3, :cond_4

    .line 1071219
    :goto_2
    move v0, v4

    .line 1071220
    if-nez v0, :cond_3

    .line 1071221
    iget-object v0, p0, Lcom/facebook/bugreporter/scheduler/BugReportRetryInvoker$1;->a:LX/6H2;

    iget-object v0, v0, LX/6H2;->c:LX/2E5;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, LX/2E5;->a(J)Z

    .line 1071222
    :cond_3
    return-void

    :cond_4
    move v4, v5

    .line 1071223
    goto :goto_2
.end method
