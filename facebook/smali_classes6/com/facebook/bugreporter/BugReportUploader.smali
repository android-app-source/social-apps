.class public Lcom/facebook/bugreporter/BugReportUploader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile j:Lcom/facebook/bugreporter/BugReportUploader;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/11H;

.field private final d:LX/6Fd;

.field private final e:LX/6Fc;

.field private final f:LX/03V;

.field private final g:LX/0Uh;

.field private final h:LX/0SI;

.field private final i:LX/6Fe;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1068886
    const-class v0, Lcom/facebook/bugreporter/BugReportUploader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/bugreporter/BugReportUploader;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/11H;LX/6Fd;LX/6Fc;LX/03V;LX/0Uh;LX/0SI;LX/6Fe;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1068876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1068877
    iput-object p1, p0, Lcom/facebook/bugreporter/BugReportUploader;->b:Landroid/content/Context;

    .line 1068878
    iput-object p2, p0, Lcom/facebook/bugreporter/BugReportUploader;->c:LX/11H;

    .line 1068879
    iput-object p3, p0, Lcom/facebook/bugreporter/BugReportUploader;->d:LX/6Fd;

    .line 1068880
    iput-object p4, p0, Lcom/facebook/bugreporter/BugReportUploader;->e:LX/6Fc;

    .line 1068881
    iput-object p5, p0, Lcom/facebook/bugreporter/BugReportUploader;->f:LX/03V;

    .line 1068882
    iput-object p6, p0, Lcom/facebook/bugreporter/BugReportUploader;->g:LX/0Uh;

    .line 1068883
    iput-object p7, p0, Lcom/facebook/bugreporter/BugReportUploader;->h:LX/0SI;

    .line 1068884
    iput-object p8, p0, Lcom/facebook/bugreporter/BugReportUploader;->i:LX/6Fe;

    .line 1068885
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/bugreporter/BugReportUploader;
    .locals 15

    .prologue
    .line 1068785
    sget-object v0, Lcom/facebook/bugreporter/BugReportUploader;->j:Lcom/facebook/bugreporter/BugReportUploader;

    if-nez v0, :cond_1

    .line 1068786
    const-class v1, Lcom/facebook/bugreporter/BugReportUploader;

    monitor-enter v1

    .line 1068787
    :try_start_0
    sget-object v0, Lcom/facebook/bugreporter/BugReportUploader;->j:Lcom/facebook/bugreporter/BugReportUploader;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1068788
    if-eqz v2, :cond_0

    .line 1068789
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1068790
    new-instance v3, Lcom/facebook/bugreporter/BugReportUploader;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v5

    check-cast v5, LX/11H;

    .line 1068791
    new-instance v9, LX/6Fd;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v6

    check-cast v6, LX/0W9;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v7

    check-cast v7, LX/0W3;

    invoke-static {v0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v8

    check-cast v8, LX/0tK;

    invoke-direct {v9, v6, v7, v8}, LX/6Fd;-><init>(LX/0W9;LX/0W3;LX/0tK;)V

    .line 1068792
    move-object v6, v9

    .line 1068793
    check-cast v6, LX/6Fd;

    .line 1068794
    new-instance v10, LX/6Fc;

    const/16 v7, 0x15e7

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v7

    check-cast v7, LX/0W9;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v8

    check-cast v8, LX/0W3;

    invoke-static {v0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v9

    check-cast v9, LX/0tK;

    invoke-direct {v10, v11, v7, v8, v9}, LX/6Fc;-><init>(LX/0Or;LX/0W9;LX/0W3;LX/0tK;)V

    .line 1068795
    move-object v7, v10

    .line 1068796
    check-cast v7, LX/6Fc;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v10

    check-cast v10, LX/0SI;

    .line 1068797
    invoke-static {v0}, LX/0XD;->b(LX/0QB;)LX/03R;

    move-result-object v11

    check-cast v11, LX/03R;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v12

    check-cast v12, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v13

    check-cast v13, LX/0lB;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v14

    check-cast v14, LX/0SG;

    invoke-static {v11, v12, v13, v14}, LX/0kO;->a(LX/03R;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lB;LX/0SG;)LX/6Fe;

    move-result-object v11

    move-object v11, v11

    .line 1068798
    check-cast v11, LX/6Fe;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/bugreporter/BugReportUploader;-><init>(Landroid/content/Context;LX/11H;LX/6Fd;LX/6Fc;LX/03V;LX/0Uh;LX/0SI;LX/6Fe;)V

    .line 1068799
    move-object v0, v3

    .line 1068800
    sput-object v0, Lcom/facebook/bugreporter/BugReportUploader;->j:Lcom/facebook/bugreporter/BugReportUploader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1068801
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1068802
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1068803
    :cond_1
    sget-object v0, Lcom/facebook/bugreporter/BugReportUploader;->j:Lcom/facebook/bugreporter/BugReportUploader;

    return-object v0

    .line 1068804
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1068805
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1068860
    if-nez p0, :cond_0

    .line 1068861
    const-string v0, ""

    .line 1068862
    :goto_0
    return-object v0

    .line 1068863
    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/net/URI;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 1068864
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v1

    .line 1068865
    invoke-static {v0}, LX/1t3;->a(Ljava/io/File;)LX/1vI;

    move-result-object v2

    .line 1068866
    new-instance p0, LX/521;

    invoke-direct {p0, v2, v1}, LX/521;-><init>(LX/1vI;Ljava/nio/charset/Charset;)V

    move-object v2, p0

    .line 1068867
    move-object v2, v2

    .line 1068868
    invoke-virtual {v2}, LX/520;->b()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1068869
    goto :goto_0

    .line 1068870
    :catch_0
    move-exception v0

    .line 1068871
    sget-object v1, Lcom/facebook/bugreporter/BugReportUploader;->a:Ljava/lang/String;

    const-string v2, "Invalid file"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1068872
    const-string v0, ""

    goto :goto_0

    .line 1068873
    :catch_1
    move-exception v0

    .line 1068874
    sget-object v1, Lcom/facebook/bugreporter/BugReportUploader;->a:Ljava/lang/String;

    const-string v2, "Failed to laod file"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1068875
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1068855
    const-string v0, "%s - %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v3

    aput-object p1, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1068856
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 1068857
    if-eqz v1, :cond_0

    .line 1068858
    const-string v1, "%s - beta"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1068859
    :cond_0
    return-object v0
.end method

.method private static a(Lcom/facebook/bugreporter/BugReportUploader;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/content/Intent;I)V
    .locals 4

    .prologue
    .line 1068848
    new-instance v0, LX/2HB;

    iget-object v1, p0, Lcom/facebook/bugreporter/BugReportUploader;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    new-instance v1, LX/3pe;

    invoke-direct {v1}, LX/3pe;-><init>()V

    invoke-virtual {v1, p2}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/bugreporter/BugReportUploader;->b:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {v1, v2, p5, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1068849
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1068850
    move-object v0, v0

    .line 1068851
    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v1

    .line 1068852
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReportUploader;->b:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1068853
    invoke-virtual {v0, p6, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1068854
    return-void
.end method

.method private static b(Lcom/facebook/bugreporter/BugReportUploader;Lcom/facebook/bugreporter/BugReport;)Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1068835
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReportUploader;->h:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 1068836
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1068837
    iget-object v1, p1, Lcom/facebook/bugreporter/BugReport;->u:Ljava/lang/String;

    move-object v1, v1

    .line 1068838
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1068839
    const/4 v0, 0x0

    .line 1068840
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/facebook/auth/viewercontext/ViewerContext;->newBuilder()LX/0SK;

    move-result-object v0

    .line 1068841
    iget-object v1, p1, Lcom/facebook/bugreporter/BugReport;->u:Ljava/lang/String;

    move-object v1, v1

    .line 1068842
    iput-object v1, v0, LX/0SK;->a:Ljava/lang/String;

    .line 1068843
    move-object v0, v0

    .line 1068844
    iget-object v1, p1, Lcom/facebook/bugreporter/BugReport;->v:Ljava/lang/String;

    move-object v1, v1

    .line 1068845
    iput-object v1, v0, LX/0SK;->b:Ljava/lang/String;

    .line 1068846
    move-object v0, v0

    .line 1068847
    invoke-virtual {v0}, LX/0SK;->h()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/bugreporter/BugReport;)Ljava/lang/String;
    .locals 23

    .prologue
    .line 1068806
    new-instance v7, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 1068807
    const-string v1, "log"

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/bugreporter/BugReportUploader;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068808
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v4

    .line 1068809
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->d()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1068810
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->d()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    .line 1068811
    :cond_0
    new-instance v1, LX/6Fg;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->e()LX/0P1;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->f()LX/0P1;

    move-result-object v6

    invoke-virtual {v7}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->h()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->i()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->j()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->k()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->l()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->m()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->n()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->o()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->p()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->q()LX/6Fb;

    move-result-object v17

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->x()LX/0P1;

    move-result-object v18

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->s()I

    move-result v19

    if-lez v19, :cond_2

    const/16 v19, 0x1

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->r()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/bugreporter/BugReport;->v()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v1 .. v21}, LX/6Fg;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0P1;LX/0P1;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Fb;LX/0P1;ZLjava/lang/String;Ljava/lang/String;)V

    .line 1068812
    const/4 v3, 0x0

    .line 1068813
    invoke-static/range {p0 .. p1}, Lcom/facebook/bugreporter/BugReportUploader;->b(Lcom/facebook/bugreporter/BugReportUploader;Lcom/facebook/bugreporter/BugReport;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    .line 1068814
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/bugreporter/BugReportUploader;->h:LX/0SI;

    invoke-interface {v4, v2}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v5

    const/4 v4, 0x0

    .line 1068815
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/bugreporter/BugReportUploader;->g:LX/0Uh;

    const/16 v6, 0x1f

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1068816
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/bugreporter/BugReportUploader;->c:LX/11H;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/bugreporter/BugReportUploader;->e:LX/6Fc;

    const-class v7, Lcom/facebook/bugreporter/BugReportUploader;

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    invoke-virtual {v2, v6, v1, v7}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Fh;

    .line 1068817
    :goto_1
    invoke-virtual {v2}, LX/6Fh;->a()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1068818
    invoke-virtual {v2}, LX/6Fh;->b()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 1068819
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/bugreporter/BugReportUploader;->i:LX/6Fe;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, LX/6Fe;->a(Lcom/facebook/bugreporter/BugReport;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object v4, v3

    .line 1068820
    :goto_2
    if-eqz v5, :cond_7

    :try_start_3
    invoke-interface {v5}, LX/1mW;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-object v8, v4

    .line 1068821
    :goto_3
    if-eqz v8, :cond_1

    .line 1068822
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/bugreporter/BugReportUploader;->b:Landroid/content/Context;

    const v3, 0x7f0818e4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, LX/6Fg;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/facebook/bugreporter/BugReportUploader;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/bugreporter/BugReportUploader;->b:Landroid/content/Context;

    const v3, 0x7f0818df

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x1080089

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/bugreporter/BugReportUploader;->b:Landroid/content/Context;

    const v5, 0x7f0818e3

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const v7, 0xc351

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v7}, Lcom/facebook/bugreporter/BugReportUploader;->a(Lcom/facebook/bugreporter/BugReportUploader;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/content/Intent;I)V

    .line 1068823
    :cond_1
    return-object v8

    .line 1068824
    :cond_2
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 1068825
    :cond_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/bugreporter/BugReportUploader;->c:LX/11H;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/bugreporter/BugReportUploader;->d:LX/6Fd;

    const-class v7, Lcom/facebook/bugreporter/BugReportUploader;

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    invoke-virtual {v2, v6, v1, v7}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Fh;

    goto :goto_1

    .line 1068826
    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/bugreporter/BugReportUploader;->i:LX/6Fe;

    invoke-virtual {v2}, LX/6Fh;->c()I

    move-result v7

    invoke-virtual {v2}, LX/6Fh;->d()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v6, v0, v7, v2}, LX/6Fe;->a(Lcom/facebook/bugreporter/BugReport;ILjava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object v4, v3

    goto :goto_2

    .line 1068827
    :catch_0
    move-exception v2

    :try_start_5
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1068828
    :catchall_0
    move-exception v4

    move-object/from16 v22, v4

    move-object v4, v3

    move-object v3, v2

    move-object/from16 v2, v22

    :goto_4
    if-eqz v5, :cond_5

    if-eqz v3, :cond_6

    :try_start_6
    invoke-interface {v5}, LX/1mW;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :cond_5
    :goto_5
    :try_start_7
    throw v2
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    :catch_1
    move-exception v2

    .line 1068829
    :goto_6
    sget-object v3, Lcom/facebook/bugreporter/BugReportUploader;->a:Ljava/lang/String;

    const-string v5, "Unable to upload bug report."

    invoke-static {v3, v5, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1068830
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/bugreporter/BugReportUploader;->f:LX/03V;

    sget-object v5, Lcom/facebook/bugreporter/BugReportUploader;->a:Ljava/lang/String;

    invoke-virtual {v3, v5, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1068831
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/bugreporter/BugReportUploader;->i:LX/6Fe;

    move-object/from16 v0, p1

    invoke-interface {v3, v0, v2}, LX/6Fe;->a(Lcom/facebook/bugreporter/BugReport;Ljava/lang/Exception;)V

    move-object v8, v4

    goto/16 :goto_3

    .line 1068832
    :catch_2
    move-exception v5

    :try_start_8
    invoke-static {v3, v5}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_6
    invoke-interface {v5}, LX/1mW;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_5

    :cond_7
    move-object v8, v4

    .line 1068833
    goto/16 :goto_3

    .line 1068834
    :catch_3
    move-exception v2

    move-object v4, v3

    goto :goto_6

    :catchall_1
    move-exception v2

    move-object/from16 v22, v4

    move-object v4, v3

    move-object/from16 v3, v22

    goto :goto_4

    :catchall_2
    move-exception v2

    move-object/from16 v22, v4

    move-object v4, v3

    move-object/from16 v3, v22

    goto :goto_4
.end method
