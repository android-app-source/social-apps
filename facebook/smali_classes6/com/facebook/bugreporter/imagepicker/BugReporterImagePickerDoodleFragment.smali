.class public Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final r:Lcom/facebook/common/callercontext/CallerContext;

.field public static final s:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public m:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/1Er;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/476;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:Lcom/facebook/drawingview/DrawingView;

.field private u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public v:LX/6Gq;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private w:Landroid/view/View;

.field public x:Landroid/widget/FrameLayout;

.field private y:LX/475;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1071005
    const-class v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->r:Lcom/facebook/common/callercontext/CallerContext;

    .line 1071006
    const-class v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    sput-object v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->s:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1071003
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1071004
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 1070999
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 1071000
    const v1, 0x7f081932

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1071001
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 1071002
    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1070969
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->c()V

    .line 1070970
    iget-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->y:LX/475;

    invoke-virtual {v0}, LX/475;->b()V

    .line 1070971
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3b0bdce7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1070988
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1070989
    const v0, 0x7f0d0802

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1070990
    iget-object v2, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1070991
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1070992
    const-string v3, "arg_screenshot_bitmap_uri"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    sget-object v3, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->r:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1070993
    const v0, 0x7f0d0803

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawingview/DrawingView;

    iput-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->t:Lcom/facebook/drawingview/DrawingView;

    .line 1070994
    iget-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->t:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/drawingview/DrawingView;->setColour(I)V

    .line 1070995
    const v0, 0x7f0d0804

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->w:Landroid/view/View;

    .line 1070996
    iget-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->w:Landroid/view/View;

    new-instance v2, LX/6Gn;

    invoke-direct {v2, p0}, LX/6Gn;-><init>(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1070997
    const v0, 0x7f0d0801

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->x:Landroid/widget/FrameLayout;

    .line 1070998
    const/16 v0, 0x2b

    const v2, -0x25989f10

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x76e10d72

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1070985
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1070986
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v4, p0

    check-cast v4, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    invoke-static {p1}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-static {p1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {p1}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v7

    check-cast v7, LX/1Er;

    invoke-static {p1}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v8

    check-cast v8, LX/0kL;

    const-class v1, LX/476;

    invoke-interface {p1, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/476;

    iput-object v5, v4, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->m:LX/0TD;

    iput-object v6, v4, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->n:Ljava/util/concurrent/Executor;

    iput-object v7, v4, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->o:LX/1Er;

    iput-object v8, v4, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->p:LX/0kL;

    iput-object p1, v4, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->q:LX/476;

    .line 1070987
    const/16 v1, 0x2b

    const v2, -0x5f3668a8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1dbf7a77

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1070984
    const v1, 0x7f030205

    invoke-virtual {p1, v1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x5711f663

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x79f1e025

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1070981
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroy()V

    .line 1070982
    iget-object v1, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->y:LX/475;

    invoke-virtual {v1}, LX/475;->b()V

    .line 1070983
    const/16 v1, 0x2b

    const v2, -0x42d511b1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x255f535b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1070975
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1070976
    iget-object v1, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->q:LX/476;

    .line 1070977
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1070978
    invoke-virtual {v1, v2}, LX/476;->a(Landroid/view/View;)LX/475;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->y:LX/475;

    .line 1070979
    iget-object v1, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->y:LX/475;

    invoke-virtual {v1}, LX/475;->a()V

    .line 1070980
    const/16 v1, 0x2b

    const v2, -0x3d76bc54

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x53cf6986

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1070972
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStop()V

    .line 1070973
    iget-object v1, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->y:LX/475;

    invoke-virtual {v1}, LX/475;->b()V

    .line 1070974
    const/16 v1, 0x2b

    const v2, -0x472e62fd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
