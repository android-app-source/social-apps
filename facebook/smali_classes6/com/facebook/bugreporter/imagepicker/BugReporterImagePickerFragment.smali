.class public Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/view/View;

.field private c:LX/6GN;

.field public d:Landroid/widget/LinearLayout;

.field public final e:LX/6Gq;

.field public f:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

.field private g:Landroid/view/View;

.field public h:I

.field private i:Lcom/facebook/content/SecureContextHelper;

.field private j:Ljava/util/concurrent/Executor;

.field private k:LX/0kL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1071122
    const-class v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1071123
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1071124
    new-instance v0, LX/6Gq;

    invoke-direct {v0, p0}, LX/6Gq;-><init>(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;)V

    iput-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->e:LX/6Gq;

    .line 1071125
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->h:I

    .line 1071126
    return-void
.end method

.method private a(LX/0kL;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1071127
    iput-object p2, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->i:Lcom/facebook/content/SecureContextHelper;

    .line 1071128
    iput-object p3, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->j:Ljava/util/concurrent/Executor;

    .line 1071129
    iput-object p1, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->k:LX/0kL;

    .line 1071130
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    invoke-static {v2}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    check-cast v0, LX/0kL;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->a(LX/0kL;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1071149
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->h:I

    .line 1071150
    invoke-direct {p0}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->l()V

    .line 1071151
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1071152
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1071153
    invoke-direct {p0, v0}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->b(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1071154
    :cond_0
    invoke-static {v1}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/6Gx;

    invoke-direct {v1, p0}, LX/6Gx;-><init>(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;)V

    iget-object v2, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->j:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1071155
    return-void
.end method

.method public static a$redex0(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;I)V
    .locals 2

    .prologue
    .line 1071131
    iget-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->k:LX/0kL;

    new-instance v1, LX/27k;

    invoke-direct {v1, p1}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1071132
    return-void
.end method

.method public static a$redex0(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;Landroid/net/Uri;)V
    .locals 3
    .param p0    # Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1071133
    iget v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->h:I

    .line 1071134
    invoke-direct {p0}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->l()V

    .line 1071135
    const/4 v0, 0x0

    .line 1071136
    iget-object v1, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->c:LX/6GN;

    if-eqz v1, :cond_0

    .line 1071137
    iget-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->c:LX/6GN;

    invoke-interface {v0, p1}, LX/6GN;->a(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1071138
    :cond_0
    if-eqz v0, :cond_1

    .line 1071139
    new-instance v1, LX/6Gs;

    invoke-direct {v1, p0}, LX/6Gs;-><init>(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;)V

    iget-object v2, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->j:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1071140
    :cond_1
    return-void
.end method

.method public static a$redex0(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;Landroid/net/Uri;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1071141
    iget-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->c:LX/6GN;

    if-eqz v0, :cond_0

    .line 1071142
    iget-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->c:LX/6GN;

    invoke-interface {v0, p1}, LX/6GN;->b(Landroid/net/Uri;)V

    .line 1071143
    :cond_0
    iget-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1071144
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1071145
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/activity/BugReportActivity;

    invoke-virtual {v0, p1}, Lcom/facebook/bugreporter/activity/BugReportActivity;->b(Landroid/net/Uri;)V

    .line 1071146
    :cond_1
    iget v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->h:I

    .line 1071147
    invoke-direct {p0}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->l()V

    .line 1071148
    return-void
.end method

.method private b(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/6H0;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1071110
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1071111
    const v0, 0x7f081930

    invoke-static {p0, v0}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->a$redex0(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;I)V

    .line 1071112
    const/4 v0, 0x0

    .line 1071113
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/activity/BugReportActivity;

    invoke-virtual {v0, p1}, Lcom/facebook/bugreporter/activity/BugReportActivity;->a(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/6Gv;

    invoke-direct {v1, p0, p1}, LX/6Gv;-><init>(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;Landroid/net/Uri;)V

    iget-object v2, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->j:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method private c()V
    .locals 6

    .prologue
    .line 1071114
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 1071115
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1071116
    if-eqz v0, :cond_0

    instance-of v2, v0, LX/6GN;

    if-eqz v2, :cond_0

    .line 1071117
    check-cast v0, LX/6GN;

    iput-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->c:LX/6GN;

    .line 1071118
    :goto_0
    return-void

    .line 1071119
    :cond_0
    instance-of v0, v1, LX/6GN;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 1071120
    check-cast v0, LX/6GN;

    iput-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->c:LX/6GN;

    goto :goto_0

    .line 1071121
    :cond_1
    sget-object v2, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->a:Ljava/lang/String;

    const-string v3, "BugReporterImagePickerFragment should be embedded in contexts that implement the ImagePickerContainer interface. Currently `%s`."

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    const-string v0, "null"

    goto :goto_1
.end method

.method public static c(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;Landroid/net/Uri;)V
    .locals 3
    .param p0    # Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1071108
    invoke-direct {p0, p1}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->b(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/6Gw;

    invoke-direct {v1, p0}, LX/6Gw;-><init>(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;)V

    iget-object v2, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->j:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1071109
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1071105
    iget-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->g:Landroid/view/View;

    const v1, 0x7f0d16e5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->b:Landroid/view/View;

    .line 1071106
    iget-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->b:Landroid/view/View;

    new-instance v1, LX/6Gr;

    invoke-direct {v1, p0}, LX/6Gr;-><init>(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1071107
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1071103
    iget-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->g:Landroid/view/View;

    const v1, 0x7f0d16e4    # 1.8754E38f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->d:Landroid/widget/LinearLayout;

    .line 1071104
    return-void
.end method

.method public static k(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;)V
    .locals 3

    .prologue
    .line 1071097
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1071098
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1071099
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1071100
    iget-object v1, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->i:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1071101
    :goto_0
    return-void

    .line 1071102
    :cond_0
    sget-object v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->a:Ljava/lang/String;

    const-string v1, "Unable to start a media-picker."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1071093
    iget v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->h:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 1071094
    iget-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->b:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 1071095
    :goto_0
    return-void

    .line 1071096
    :cond_0
    iget-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1071089
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1071090
    const-class v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    invoke-static {v0, p0}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1071091
    invoke-direct {p0}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->c()V

    .line 1071092
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5090366b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1071086
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1071087
    iget-object v1, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->c:LX/6GN;

    invoke-interface {v1}, LX/6GN;->b()LX/0Px;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->a(Ljava/util/List;)V

    .line 1071088
    const/16 v1, 0x2b

    const v2, -0xf5d2bd9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1071082
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1071083
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1071084
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->a$redex0(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;Landroid/net/Uri;)V

    .line 1071085
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x8c8c381

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1071078
    const v1, 0x7f0308d2

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->g:Landroid/view/View;

    .line 1071079
    invoke-direct {p0}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->d()V

    .line 1071080
    invoke-direct {p0}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->e()V

    .line 1071081
    iget-object v1, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->g:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, 0x148729bd

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2a33b54f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1071073
    iget-object v1, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->f:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    if-eqz v1, :cond_0

    .line 1071074
    iget-object v1, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->f:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    .line 1071075
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->v:LX/6Gq;

    .line 1071076
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1071077
    const/16 v1, 0x2b

    const v2, -0x7aed11a9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
