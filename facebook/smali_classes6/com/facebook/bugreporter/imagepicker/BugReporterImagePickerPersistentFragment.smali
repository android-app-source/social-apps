.class public Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/43C;

.field public c:LX/0TD;

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1071165
    const-class v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1071166
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1071167
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;->d:Ljava/util/Map;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    invoke-static {p0}, LX/43D;->b(LX/0QB;)LX/43C;

    move-result-object v1

    check-cast v1, LX/43C;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object p0

    check-cast p0, LX/0TD;

    iput-object v1, p1, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;->b:LX/43C;

    iput-object p0, p1, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;->c:LX/0TD;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1071168
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1071169
    const-class v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    invoke-static {v0, p0}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1071170
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 1071171
    return-void
.end method
