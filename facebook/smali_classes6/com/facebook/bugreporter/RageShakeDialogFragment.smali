.class public Lcom/facebook/bugreporter/RageShakeDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field private static final n:Ljava/lang/Class;


# instance fields
.field public m:Lcom/facebook/content/SecureContextHelper;

.field public o:Landroid/content/ComponentName;

.field public p:LX/0kK;

.field public q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6G2;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/6Uq;

.field public s:LX/6V3;

.field public t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/bugreporter/RageShakeActionItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1070020
    const-class v0, Lcom/facebook/bugreporter/RageShakeDialogFragment;

    sput-object v0, Lcom/facebook/bugreporter/RageShakeDialogFragment;->n:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1070021
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v1, p1

    check-cast v1, Lcom/facebook/bugreporter/RageShakeDialogFragment;

    invoke-static {v8}, LX/6Uq;->a(LX/0QB;)LX/6Uq;

    move-result-object v2

    check-cast v2, LX/6Uq;

    const/16 v3, 0x186a

    invoke-static {v8, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    new-instance v7, LX/6V3;

    invoke-static {v8}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v8}, LX/6V0;->b(LX/0QB;)LX/6V0;

    move-result-object v5

    check-cast v5, LX/6V0;

    invoke-static {v8}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v6

    check-cast v6, LX/1Er;

    invoke-direct {v7, v4, v5, v6}, LX/6V3;-><init>(Lcom/facebook/content/SecureContextHelper;LX/6V0;LX/1Er;)V

    move-object v4, v7

    check-cast v4, LX/6V3;

    invoke-static {v8}, LX/0kK;->a(LX/0QB;)LX/0kK;

    move-result-object v5

    check-cast v5, LX/0kK;

    invoke-static {v8}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v8}, LX/38K;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v7

    check-cast v7, Landroid/content/ComponentName;

    new-instance p0, LX/0U8;

    invoke-interface {v8}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p1

    new-instance v0, LX/43V;

    invoke-direct {v0, v8}, LX/43V;-><init>(LX/0QB;)V

    invoke-direct {p0, p1, v0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v8, p0

    iput-object v2, v1, Lcom/facebook/bugreporter/RageShakeDialogFragment;->r:LX/6Uq;

    iput-object v3, v1, Lcom/facebook/bugreporter/RageShakeDialogFragment;->q:LX/0Or;

    iput-object v4, v1, Lcom/facebook/bugreporter/RageShakeDialogFragment;->s:LX/6V3;

    iput-object v5, v1, Lcom/facebook/bugreporter/RageShakeDialogFragment;->p:LX/0kK;

    iput-object v6, v1, Lcom/facebook/bugreporter/RageShakeDialogFragment;->m:Lcom/facebook/content/SecureContextHelper;

    iput-object v7, v1, Lcom/facebook/bugreporter/RageShakeDialogFragment;->o:Landroid/content/ComponentName;

    invoke-static {v8}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p0

    iput-object p0, v1, Lcom/facebook/bugreporter/RageShakeDialogFragment;->t:LX/0Px;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1070022
    const-class v0, Lcom/facebook/bugreporter/RageShakeDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/bugreporter/RageShakeDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1070023
    new-instance v3, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 1070024
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1070025
    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1070026
    :try_start_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1070027
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    .line 1070028
    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1070029
    if-eqz v1, :cond_0

    .line 1070030
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ": "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1070031
    :cond_0
    :goto_0
    invoke-virtual {v3, v0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1070032
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1070033
    const-string v1, "items"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequenceArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 1070034
    iget-object v0, p0, Lcom/facebook/bugreporter/RageShakeDialogFragment;->o:Landroid/content/ComponentName;

    if-eqz v0, :cond_1

    const v0, 0x7f0818f5

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1070035
    const v0, 0x7f0818f5

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1070036
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 1070037
    new-instance v6, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/facebook/bugreporter/RageShakeDialogFragment;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1070038
    iget-object v0, p0, Lcom/facebook/bugreporter/RageShakeDialogFragment;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v7

    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_3

    iget-object v0, p0, Lcom/facebook/bugreporter/RageShakeDialogFragment;->t:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E1Z;

    .line 1070039
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    const/4 p1, 0x0

    .line 1070040
    iget-object v2, v0, LX/E1Z;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03R;

    invoke-virtual {v2, p1}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v0, LX/E1Z;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 1070041
    if-eqz v2, :cond_2

    .line 1070042
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1070043
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1070044
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const p1, 0x7f083127

    invoke-virtual {v8, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object v0, v8

    .line 1070045
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1070046
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1070047
    :catch_0
    move-exception v1

    .line 1070048
    sget-object v4, Lcom/facebook/bugreporter/RageShakeDialogFragment;->n:Ljava/lang/Class;

    const-string v5, ""

    invoke-static {v4, v5, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1070049
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    new-instance v1, LX/6GD;

    invoke-direct {v1, p0, v5, v6}, LX/6GD;-><init>(Lcom/facebook/bugreporter/RageShakeDialogFragment;ILjava/util/List;)V

    invoke-virtual {v3, v0, v1}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1070050
    const v0, 0x7f080017

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/6GE;

    invoke-direct {v1, p0}, LX/6GE;-><init>(Lcom/facebook/bugreporter/RageShakeDialogFragment;)V

    invoke-virtual {v3, v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1070051
    invoke-virtual {v3}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 1070052
    new-instance v1, LX/6GF;

    invoke-direct {v1, p0}, LX/6GF;-><init>(Lcom/facebook/bugreporter/RageShakeDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1070053
    return-object v0

    :cond_4
    move v2, p1

    goto :goto_2
.end method
