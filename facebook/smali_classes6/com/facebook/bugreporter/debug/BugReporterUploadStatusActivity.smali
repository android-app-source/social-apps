.class public Lcom/facebook/bugreporter/debug/BugReporterUploadStatusActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/6Ge;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1070917
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1070918
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1070919
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1070920
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/bugreporter/debug/BugReporterUploadStatusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1070921
    new-instance v1, LX/6Gh;

    invoke-direct {v1, p0}, LX/6Gh;-><init>(Lcom/facebook/bugreporter/debug/BugReporterUploadStatusActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1070922
    const v1, 0x7f081937

    invoke-virtual {p0, v1}, Lcom/facebook/bugreporter/debug/BugReporterUploadStatusActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1070923
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/bugreporter/debug/BugReporterUploadStatusActivity;

    new-instance p1, LX/6Ge;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v2

    check-cast v2, LX/0lB;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p1, v1, v2, v3}, LX/6Ge;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lB;LX/0SG;)V

    move-object v0, p1

    check-cast v0, LX/6Ge;

    iput-object v0, p0, Lcom/facebook/bugreporter/debug/BugReporterUploadStatusActivity;->p:LX/6Ge;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1070924
    new-instance v1, LX/6Gj;

    iget-object v0, p0, Lcom/facebook/bugreporter/debug/BugReporterUploadStatusActivity;->p:LX/6Ge;

    invoke-virtual {v0}, LX/6Ge;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, p0, v0}, LX/6Gj;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 1070925
    const v0, 0x7f0d0c29

    invoke-virtual {p0, v0}, Lcom/facebook/bugreporter/debug/BugReporterUploadStatusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 1070926
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1070927
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1070928
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1070929
    invoke-static {p0, p0}, Lcom/facebook/bugreporter/debug/BugReporterUploadStatusActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1070930
    const v0, 0x7f0303ee

    invoke-virtual {p0, v0}, Lcom/facebook/bugreporter/debug/BugReporterUploadStatusActivity;->setContentView(I)V

    .line 1070931
    invoke-direct {p0}, Lcom/facebook/bugreporter/debug/BugReporterUploadStatusActivity;->a()V

    .line 1070932
    invoke-direct {p0}, Lcom/facebook/bugreporter/debug/BugReporterUploadStatusActivity;->b()V

    .line 1070933
    return-void
.end method
