.class public Lcom/facebook/bugreporter/debug/BugReportUploadStatus;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# instance fields
.field public creationTime:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "creationTime"
    .end annotation
.end field

.field public description:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "description"
    .end annotation
.end field

.field public failedUploadAttempts:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "failedUploadAttempts"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public isSuccessfullyUploaded:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isSuccessfullyUploaded"
    .end annotation
.end field

.field public networkType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "networkType"
    .end annotation
.end field

.field public reportId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reportId"
    .end annotation
.end field

.field public wallTimeOfLastUpdateOfStatus:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "wallTimeOfLastUpdateOfStatus"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1070810
    const-class v0, Lcom/facebook/bugreporter/debug/BugReportUploadStatusDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1070819
    const-class v0, Lcom/facebook/bugreporter/debug/BugReportUploadStatusSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1070817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1070818
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1070820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1070821
    iput-object p1, p0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->reportId:Ljava/lang/String;

    .line 1070822
    iput-object p2, p0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->creationTime:Ljava/lang/String;

    .line 1070823
    iput-object p3, p0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->description:Ljava/lang/String;

    .line 1070824
    iput-object p4, p0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->networkType:Ljava/lang/String;

    .line 1070825
    iput-boolean p5, p0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->isSuccessfullyUploaded:Z

    .line 1070826
    iput-object p6, p0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->failedUploadAttempts:Ljava/util/List;

    .line 1070827
    return-void
.end method

.method public static a(LX/0lC;Ljava/lang/String;)Lcom/facebook/bugreporter/debug/BugReportUploadStatus;
    .locals 2

    .prologue
    .line 1070814
    :try_start_0
    const-class v0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;

    invoke-virtual {p0, p1, v0}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1070815
    :catch_0
    move-exception v0

    .line 1070816
    new-instance v1, LX/462;

    invoke-direct {v1, v0}, LX/462;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(LX/0lC;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1070811
    :try_start_0
    invoke-virtual {p1, p0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1070812
    :catch_0
    move-exception v0

    .line 1070813
    new-instance v1, LX/462;

    invoke-direct {v1, v0}, LX/462;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
