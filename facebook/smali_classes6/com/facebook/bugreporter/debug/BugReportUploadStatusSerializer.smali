.class public Lcom/facebook/bugreporter/debug/BugReportUploadStatusSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/bugreporter/debug/BugReportUploadStatus;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1070855
    const-class v0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;

    new-instance v1, Lcom/facebook/bugreporter/debug/BugReportUploadStatusSerializer;

    invoke-direct {v1}, Lcom/facebook/bugreporter/debug/BugReportUploadStatusSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1070856
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1070857
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/bugreporter/debug/BugReportUploadStatus;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1070858
    if-nez p0, :cond_0

    .line 1070859
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1070860
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1070861
    invoke-static {p0, p1, p2}, Lcom/facebook/bugreporter/debug/BugReportUploadStatusSerializer;->b(Lcom/facebook/bugreporter/debug/BugReportUploadStatus;LX/0nX;LX/0my;)V

    .line 1070862
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1070863
    return-void
.end method

.method private static b(Lcom/facebook/bugreporter/debug/BugReportUploadStatus;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1070864
    const-string v0, "reportId"

    iget-object v1, p0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->reportId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1070865
    const-string v0, "creationTime"

    iget-object v1, p0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->creationTime:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1070866
    const-string v0, "description"

    iget-object v1, p0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->description:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1070867
    const-string v0, "networkType"

    iget-object v1, p0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->networkType:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1070868
    const-string v0, "isSuccessfullyUploaded"

    iget-boolean v1, p0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->isSuccessfullyUploaded:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1070869
    const-string v0, "wallTimeOfLastUpdateOfStatus"

    iget-wide v2, p0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->wallTimeOfLastUpdateOfStatus:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1070870
    const-string v0, "failedUploadAttempts"

    iget-object v1, p0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->failedUploadAttempts:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1070871
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1070872
    check-cast p1, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;

    invoke-static {p1, p2, p3}, Lcom/facebook/bugreporter/debug/BugReportUploadStatusSerializer;->a(Lcom/facebook/bugreporter/debug/BugReportUploadStatus;LX/0nX;LX/0my;)V

    return-void
.end method
