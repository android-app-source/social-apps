.class public final Lcom/facebook/bugreporter/RageShakeDetector$AsyncSensorRegistration$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6GC;

.field private b:Z


# direct methods
.method public constructor <init>(LX/6GC;)V
    .locals 0

    .prologue
    .line 1069931
    iput-object p1, p0, Lcom/facebook/bugreporter/RageShakeDetector$AsyncSensorRegistration$1;->a:LX/6GC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1069932
    iget-object v0, p0, Lcom/facebook/bugreporter/RageShakeDetector$AsyncSensorRegistration$1;->a:LX/6GC;

    iget-object v0, v0, LX/6GC;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 1069933
    :goto_0
    iget-boolean v1, p0, Lcom/facebook/bugreporter/RageShakeDetector$AsyncSensorRegistration$1;->b:Z

    .line 1069934
    if-eq v1, v0, :cond_0

    .line 1069935
    if-eqz v0, :cond_2

    .line 1069936
    iget-object v1, p0, Lcom/facebook/bugreporter/RageShakeDetector$AsyncSensorRegistration$1;->a:LX/6GC;

    .line 1069937
    invoke-static {v1}, LX/6GC;->c(LX/6GC;)Landroid/hardware/SensorManager;

    move-result-object v2

    .line 1069938
    iget-object v3, v1, LX/6GC;->c:Landroid/hardware/SensorEventListener;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v2, v3, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 1069939
    :goto_1
    iput-boolean v0, p0, Lcom/facebook/bugreporter/RageShakeDetector$AsyncSensorRegistration$1;->b:Z

    .line 1069940
    :cond_0
    return-void

    .line 1069941
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1069942
    :cond_2
    iget-object v1, p0, Lcom/facebook/bugreporter/RageShakeDetector$AsyncSensorRegistration$1;->a:LX/6GC;

    .line 1069943
    invoke-static {v1}, LX/6GC;->c(LX/6GC;)Landroid/hardware/SensorManager;

    move-result-object v2

    iget-object v3, v1, LX/6GC;->c:Landroid/hardware/SensorEventListener;

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 1069944
    goto :goto_1
.end method
