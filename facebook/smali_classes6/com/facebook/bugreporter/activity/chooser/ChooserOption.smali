.class public Lcom/facebook/bugreporter/activity/chooser/ChooserOption;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/bugreporter/activity/chooser/ChooserOption;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:I

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:LX/6GY;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1070608
    const-string v0, "bugreport"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->a:Ljava/lang/String;

    .line 1070609
    new-instance v0, LX/6GV;

    invoke-direct {v0}, LX/6GV;-><init>()V

    sput-object v0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;LX/6GY;)V
    .locals 0
    .param p4    # LX/6GY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1070610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1070611
    iput p1, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->b:I

    .line 1070612
    iput p2, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->c:I

    .line 1070613
    iput-object p3, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->d:Ljava/lang/String;

    .line 1070614
    iput-object p4, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->e:LX/6GY;

    .line 1070615
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1070616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1070617
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->b:I

    .line 1070618
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->c:I

    .line 1070619
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->d:Ljava/lang/String;

    .line 1070620
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/6GY;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->e:LX/6GY;

    .line 1070621
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1070622
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1070623
    iget v0, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1070624
    iget v0, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1070625
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1070626
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->e:LX/6GY;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1070627
    return-void
.end method
