.class public Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/6G2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/6GZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/6GU;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1070585
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1070586
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;

    invoke-static {p0}, LX/6G2;->a(LX/0QB;)LX/6G2;

    move-result-object v1

    check-cast v1, LX/6G2;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/6GZ;->b(LX/0QB;)LX/6GZ;

    move-result-object p0

    check-cast p0, LX/6GZ;

    iput-object v1, p1, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;->m:LX/6G2;

    iput-object v2, p1, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;->n:Lcom/facebook/content/SecureContextHelper;

    iput-object p0, p1, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;->o:LX/6GZ;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1070587
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0818ee

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;->p:LX/6GU;

    new-instance v2, LX/6GT;

    invoke-direct {v2, p0}, LX/6GT;-><init>(Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;)V

    .line 1070588
    iget-object p0, v0, LX/0ju;->a:LX/31a;

    iput-object v1, p0, LX/31a;->v:Landroid/widget/ListAdapter;

    .line 1070589
    iget-object p0, v0, LX/0ju;->a:LX/31a;

    iput-object v2, p0, LX/31a;->w:Landroid/content/DialogInterface$OnClickListener;

    .line 1070590
    move-object v0, v0

    .line 1070591
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x2ec5f30c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1070592
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1070593
    const-class v1, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;

    invoke-static {v1, p0}, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1070594
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1070595
    const-string v2, "CHOOSER_OPTIONS"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1070596
    new-instance v2, LX/6GU;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v2, v1}, LX/6GU;-><init>(LX/0Px;)V

    iput-object v2, p0, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;->p:LX/6GU;

    .line 1070597
    const/16 v1, 0x2b

    const v2, -0x5d33de3b

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
