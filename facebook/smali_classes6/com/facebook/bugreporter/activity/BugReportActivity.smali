.class public Lcom/facebook/bugreporter/activity/BugReportActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/6G5;
.implements LX/0l6;


# static fields
.field private static final p:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private q:Lcom/facebook/bugreporter/ConstBugReporterConfig;

.field public r:LX/6G3;

.field private s:LX/42n;

.field private t:LX/6FU;

.field private u:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1070153
    const-class v0, Lcom/facebook/bugreporter/activity/BugReportActivity;

    sput-object v0, Lcom/facebook/bugreporter/activity/BugReportActivity;->p:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1070133
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/bugreporter/BugReport;LX/6G0;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1070134
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/bugreporter/activity/BugReportActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1070135
    const-string v1, "report"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1070136
    const-string v1, "reporter_config"

    .line 1070137
    instance-of v2, p2, Lcom/facebook/bugreporter/ConstBugReporterConfig;

    if-eqz v2, :cond_0

    .line 1070138
    check-cast p2, Lcom/facebook/bugreporter/ConstBugReporterConfig;

    .line 1070139
    :goto_0
    move-object v2, p2

    .line 1070140
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1070141
    return-object v0

    :cond_0
    new-instance v2, Lcom/facebook/bugreporter/ConstBugReporterConfig;

    invoke-direct {v2, p2}, Lcom/facebook/bugreporter/ConstBugReporterConfig;-><init>(LX/6G0;)V

    move-object p2, v2

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/bugreporter/activity/BugReportActivity;

    invoke-static {v0}, LX/6G3;->a(LX/0QB;)LX/6G3;

    move-result-object v0

    check-cast v0, LX/6G3;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->r:LX/6G3;

    return-void
.end method

.method private a(ZLcom/facebook/bugreporter/BugReport;)V
    .locals 4

    .prologue
    .line 1070177
    invoke-direct {p0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->b()Landroid/os/Bundle;

    move-result-object v0

    .line 1070178
    const-string v1, "report"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1070179
    new-instance v1, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    invoke-direct {v1}, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;-><init>()V

    .line 1070180
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1070181
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->s:LX/42n;

    invoke-virtual {v1, v0}, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->a(LX/42n;)V

    .line 1070182
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 1070183
    const v2, 0x7f0d0553

    const-string v3, "bugReport"

    invoke-virtual {v0, v2, v1, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 1070184
    if-eqz p1, :cond_0

    .line 1070185
    const-string v1, "bugReport_bs"

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    .line 1070186
    :cond_0
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1070187
    return-void
.end method

.method private a(ZZ)V
    .locals 4

    .prologue
    .line 1070142
    invoke-direct {p0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->b()Landroid/os/Bundle;

    move-result-object v0

    .line 1070143
    const-string v1, "retry"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1070144
    new-instance v1, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;

    invoke-direct {v1}, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;-><init>()V

    .line 1070145
    iget-object v2, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->s:LX/42n;

    invoke-virtual {v1, v2}, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->a(LX/42n;)V

    .line 1070146
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1070147
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 1070148
    const v2, 0x7f0d0553

    const-string v3, "categoryList"

    invoke-virtual {v0, v2, v1, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 1070149
    if-eqz p1, :cond_0

    .line 1070150
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    .line 1070151
    :cond_0
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1070152
    return-void
.end method

.method public static a(ILandroid/content/Intent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1070058
    const/16 v1, 0x4693

    if-ne p0, v1, :cond_0

    const-string v1, "from_bug_report_activity"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static a$redex0(Lcom/facebook/bugreporter/activity/BugReportActivity;Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1070154
    if-nez p2, :cond_1

    .line 1070155
    invoke-direct {p0, v1}, Lcom/facebook/bugreporter/activity/BugReportActivity;->c(Z)V

    .line 1070156
    :cond_0
    :goto_0
    return-void

    .line 1070157
    :cond_1
    const-string v0, "isSendClickedFlag"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1070158
    const-string v0, "isSendClickedFlag"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->c(Z)V

    goto :goto_0

    .line 1070159
    :cond_2
    instance-of v0, p1, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;

    if-eqz v0, :cond_5

    .line 1070160
    const-string v0, "retry"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1070161
    if-nez v0, :cond_4

    .line 1070162
    const-string v0, "category_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1070163
    if-nez v0, :cond_3

    const-string v0, "100977986739334"

    .line 1070164
    :cond_3
    iget-object v1, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->t:LX/6FU;

    .line 1070165
    iput-object v0, v1, LX/6FU;->i:Ljava/lang/String;

    .line 1070166
    :cond_4
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->t:LX/6FU;

    invoke-virtual {v1}, LX/6FU;->y()Lcom/facebook/bugreporter/BugReport;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/bugreporter/activity/BugReportActivity;->a(ZLcom/facebook/bugreporter/BugReport;)V

    goto :goto_0

    .line 1070167
    :cond_5
    instance-of v0, p1, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    if-eqz v0, :cond_0

    .line 1070168
    const-string v0, "bug_desc"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1070169
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1070170
    iget-object v1, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->t:LX/6FU;

    .line 1070171
    iput-object v0, v1, LX/6FU;->b:Ljava/lang/String;

    .line 1070172
    :cond_6
    const-string v0, "bug_shots"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1070173
    if-eqz v0, :cond_0

    .line 1070174
    iget-object v1, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->t:LX/6FU;

    .line 1070175
    iput-object v0, v1, LX/6FU;->d:Ljava/util/List;

    .line 1070176
    goto :goto_0
.end method

.method private b()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 1070118
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1070119
    const-string v1, "reporter_config"

    iget-object v2, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->q:Lcom/facebook/bugreporter/ConstBugReporterConfig;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1070120
    return-object v0
.end method

.method private b(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1070121
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->q:Lcom/facebook/bugreporter/ConstBugReporterConfig;

    invoke-virtual {v0}, Lcom/facebook/bugreporter/ConstBugReporterConfig;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1070122
    if-le v0, v1, :cond_0

    .line 1070123
    invoke-direct {p0, v2, p1}, Lcom/facebook/bugreporter/activity/BugReportActivity;->a(ZZ)V

    .line 1070124
    :goto_0
    return-void

    .line 1070125
    :cond_0
    if-ne v0, v1, :cond_1

    .line 1070126
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->q:Lcom/facebook/bugreporter/ConstBugReporterConfig;

    invoke-virtual {v0}, Lcom/facebook/bugreporter/ConstBugReporterConfig;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    .line 1070127
    iget-wide v3, v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->c:J

    move-wide v0, v3

    .line 1070128
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 1070129
    iget-object v1, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->t:LX/6FU;

    .line 1070130
    iput-object v0, v1, LX/6FU;->i:Ljava/lang/String;

    .line 1070131
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->t:LX/6FU;

    invoke-virtual {v0}, LX/6FU;->y()Lcom/facebook/bugreporter/BugReport;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->a(ZLcom/facebook/bugreporter/BugReport;)V

    goto :goto_0

    .line 1070132
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->finish()V

    goto :goto_0
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 1070112
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1070113
    const-string v1, "from_bug_report_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1070114
    const-string v1, "isSendClickedFlag"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1070115
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->setResult(ILandroid/content/Intent;)V

    .line 1070116
    invoke-virtual {p0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->finish()V

    .line 1070117
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1070106
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->u:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    .line 1070107
    iget-object v1, v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;->d:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 1070108
    if-eqz v1, :cond_0

    .line 1070109
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1070110
    :goto_0
    move-object v0, v1

    .line 1070111
    return-object v0

    :cond_0
    iget-object v1, v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;->c:LX/0TD;

    new-instance p0, LX/6Gy;

    invoke-direct {p0, v0, p1}, LX/6Gy;-><init>(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;Landroid/net/Uri;)V

    invoke-interface {v1, p0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1070105
    const-string v0, "bug_report"

    return-object v0
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1070102
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->u:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    .line 1070103
    iget-object p0, v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;->d:Ljava/util/Map;

    invoke-interface {p0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1070104
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1070073
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1070074
    invoke-static {p0, p0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1070075
    const v0, 0x7f030207

    invoke-virtual {p0, v0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->setContentView(I)V

    .line 1070076
    new-instance v0, LX/6GG;

    invoke-direct {v0, p0}, LX/6GG;-><init>(Lcom/facebook/bugreporter/activity/BugReportActivity;)V

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->s:LX/42n;

    .line 1070077
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 1070078
    const-string v0, "categoryList"

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;

    .line 1070079
    if-eqz v0, :cond_0

    .line 1070080
    iget-object v2, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->s:LX/42n;

    invoke-virtual {v0, v2}, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->a(LX/42n;)V

    .line 1070081
    :cond_0
    const-string v0, "bugReport"

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    .line 1070082
    if-eqz v0, :cond_1

    .line 1070083
    iget-object v2, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->s:LX/42n;

    invoke-virtual {v0, v2}, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->a(LX/42n;)V

    .line 1070084
    :cond_1
    const-string v0, "persistent_fragment"

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->u:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    .line 1070085
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->u:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    if-nez v0, :cond_2

    .line 1070086
    new-instance v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    invoke-direct {v0}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->u:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    .line 1070087
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->u:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    const-string v2, "persistent_fragment"

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1070088
    :cond_2
    if-eqz p1, :cond_3

    .line 1070089
    const-string v0, "report"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/BugReport;

    .line 1070090
    invoke-static {}, Lcom/facebook/bugreporter/BugReport;->newBuilder()LX/6FU;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/6FU;->a(Lcom/facebook/bugreporter/BugReport;)LX/6FU;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->t:LX/6FU;

    .line 1070091
    const-string v0, "reporter_config"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/ConstBugReporterConfig;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->q:Lcom/facebook/bugreporter/ConstBugReporterConfig;

    .line 1070092
    :goto_0
    return-void

    .line 1070093
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1070094
    const-string v0, "retry"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 1070095
    const-string v0, "report"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/BugReport;

    .line 1070096
    if-nez v0, :cond_4

    .line 1070097
    sget-object v0, Lcom/facebook/bugreporter/activity/BugReportActivity;->p:Ljava/lang/Class;

    const-string v1, "Missing bug report in intent"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1070098
    invoke-virtual {p0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->finish()V

    goto :goto_0

    .line 1070099
    :cond_4
    invoke-static {}, Lcom/facebook/bugreporter/BugReport;->newBuilder()LX/6FU;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/6FU;->a(Lcom/facebook/bugreporter/BugReport;)LX/6FU;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->t:LX/6FU;

    .line 1070100
    const-string v0, "reporter_config"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/ConstBugReporterConfig;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->q:Lcom/facebook/bugreporter/ConstBugReporterConfig;

    .line 1070101
    invoke-direct {p0, v2}, Lcom/facebook/bugreporter/activity/BugReportActivity;->b(Z)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 5

    .prologue
    .line 1070067
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1070068
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->r:LX/6G3;

    iget-object v1, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->t:LX/6FU;

    .line 1070069
    iget-object v2, v1, LX/6FU;->a:Landroid/net/Uri;

    move-object v1, v2

    .line 1070070
    iget-object v2, v0, LX/6G3;->c:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/facebook/bugreporter/BugReporterFileUtil$1;

    invoke-direct {v3, v0, v1}, Lcom/facebook/bugreporter/BugReporterFileUtil$1;-><init>(LX/6G3;Landroid/net/Uri;)V

    const v4, 0x544dded0

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1070071
    invoke-virtual {p0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->finish()V

    .line 1070072
    :cond_0
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 1070063
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 1070064
    invoke-virtual {p0}, Lcom/facebook/bugreporter/activity/BugReportActivity;->finish()V

    .line 1070065
    const/4 v0, 0x1

    .line 1070066
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1070059
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1070060
    const-string v0, "reporter_config"

    iget-object v1, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->q:Lcom/facebook/bugreporter/ConstBugReporterConfig;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1070061
    const-string v0, "report"

    iget-object v1, p0, Lcom/facebook/bugreporter/activity/BugReportActivity;->t:LX/6FU;

    invoke-virtual {v1}, LX/6FU;->y()Lcom/facebook/bugreporter/BugReport;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1070062
    return-void
.end method
