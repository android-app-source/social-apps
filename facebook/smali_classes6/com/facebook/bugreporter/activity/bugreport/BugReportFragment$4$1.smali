.class public final Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment$4$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/bugreporter/BugReport;

.field public final synthetic b:LX/6GK;


# direct methods
.method public constructor <init>(LX/6GK;Lcom/facebook/bugreporter/BugReport;)V
    .locals 0

    .prologue
    .line 1070208
    iput-object p1, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment$4$1;->b:LX/6GK;

    iput-object p2, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment$4$1;->a:Lcom/facebook/bugreporter/BugReport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1070209
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment$4$1;->b:LX/6GK;

    iget-object v0, v0, LX/6GK;->d:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    iget-object v1, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment$4$1;->a:Lcom/facebook/bugreporter/BugReport;

    .line 1070210
    :try_start_0
    invoke-static {v1}, LX/6G3;->b(Lcom/facebook/bugreporter/BugReport;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1070211
    :goto_0
    iget-object v2, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->k:LX/6Fa;

    invoke-virtual {v2, v1}, LX/6Fa;->a(Lcom/facebook/bugreporter/BugReport;)V

    .line 1070212
    return-void

    .line 1070213
    :catch_0
    move-exception v2

    .line 1070214
    iget-object v3, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->i:LX/6GZ;

    sget-object v4, LX/6GY;->BUG_REPORT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v3, v4}, LX/6GZ;->a(LX/6GY;)V

    .line 1070215
    sget-object v3, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->a:Ljava/lang/Class;

    const-string v4, "Failed to persist serialized bug report."

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, p0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
