.class public Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/base/fragment/NavigableFragment;
.implements LX/6GN;


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/6Ft;

.field public B:LX/0W3;

.field public b:LX/0WV;

.field public c:LX/0TD;

.field private d:Ljava/util/concurrent/Executor;

.field public e:LX/1gy;

.field public f:LX/01U;

.field public g:LX/0kL;

.field public h:Lcom/facebook/content/SecureContextHelper;

.field public i:LX/6GZ;

.field public j:LX/6G3;

.field public k:LX/6Fa;

.field public l:LX/0SG;

.field public m:LX/6FU;

.field public n:LX/42n;

.field public o:Landroid/widget/EditText;

.field private p:Landroid/widget/TextView;

.field private q:Z

.field public r:Z

.field private s:Z

.field private t:Z

.field private u:LX/67a;

.field private v:LX/0h5;

.field private w:LX/63L;

.field private x:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/67a;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public z:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1070415
    const-class v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    sput-object v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1070414
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;LX/63V;LX/0Or;LX/0WV;LX/6Fa;LX/0TD;Ljava/util/concurrent/Executor;LX/1gy;LX/01U;LX/0kL;Lcom/facebook/content/SecureContextHelper;LX/6GZ;LX/6G3;LX/0SG;LX/03R;LX/0Or;LX/6Ft;LX/0W3;)V
    .locals 2
    .param p4    # LX/6Fa;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p13    # LX/0SG;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p14    # LX/03R;
        .annotation runtime Lcom/facebook/bugreporter/annotations/IsNotSendBugReportEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/63V;",
            "LX/0Or",
            "<",
            "LX/67a;",
            ">;",
            "LX/0WV;",
            "LX/6Fa;",
            "LX/0TD;",
            "Ljava/util/concurrent/Executor;",
            "LX/1gy;",
            "LX/01U;",
            "LX/0kL;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/6GZ;",
            "LX/6G3;",
            "LX/0SG;",
            "LX/03R;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/6Ft;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1070396
    iput-object p2, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->x:LX/0Or;

    .line 1070397
    iput-object p3, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->b:LX/0WV;

    .line 1070398
    iput-object p4, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->k:LX/6Fa;

    .line 1070399
    iput-object p5, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->c:LX/0TD;

    .line 1070400
    iput-object p6, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->d:Ljava/util/concurrent/Executor;

    .line 1070401
    iput-object p7, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->e:LX/1gy;

    .line 1070402
    iput-object p8, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->f:LX/01U;

    .line 1070403
    iput-object p9, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->g:LX/0kL;

    .line 1070404
    iput-object p10, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->h:Lcom/facebook/content/SecureContextHelper;

    .line 1070405
    iput-object p11, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->i:LX/6GZ;

    .line 1070406
    iput-object p12, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->j:LX/6G3;

    .line 1070407
    iput-object p13, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->l:LX/0SG;

    .line 1070408
    invoke-virtual {p1}, LX/63V;->a()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->t:Z

    .line 1070409
    const/4 v1, 0x0

    move-object/from16 v0, p14

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->s:Z

    .line 1070410
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->y:LX/0Or;

    .line 1070411
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->A:LX/6Ft;

    .line 1070412
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->B:LX/0W3;

    .line 1070413
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 20

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v19

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    invoke-static/range {v19 .. v19}, LX/63V;->a(LX/0QB;)LX/63V;

    move-result-object v3

    check-cast v3, LX/63V;

    const/16 v4, 0x1678

    move-object/from16 v0, v19

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {v19 .. v19}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v5

    check-cast v5, LX/0WV;

    invoke-static/range {v19 .. v19}, LX/6Fa;->a(LX/0QB;)LX/6Fa;

    move-result-object v6

    check-cast v6, LX/6Fa;

    invoke-static/range {v19 .. v19}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static/range {v19 .. v19}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-static/range {v19 .. v19}, LX/1Mn;->a(LX/0QB;)LX/1gy;

    move-result-object v9

    check-cast v9, LX/1gy;

    invoke-static/range {v19 .. v19}, LX/0XV;->a(LX/0QB;)LX/01U;

    move-result-object v10

    check-cast v10, LX/01U;

    invoke-static/range {v19 .. v19}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v11

    check-cast v11, LX/0kL;

    invoke-static/range {v19 .. v19}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v12

    check-cast v12, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v19 .. v19}, LX/6GZ;->a(LX/0QB;)LX/6GZ;

    move-result-object v13

    check-cast v13, LX/6GZ;

    invoke-static/range {v19 .. v19}, LX/6G3;->a(LX/0QB;)LX/6G3;

    move-result-object v14

    check-cast v14, LX/6G3;

    invoke-static/range {v19 .. v19}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v15

    check-cast v15, LX/0SG;

    invoke-static/range {v19 .. v19}, LX/0XD;->a(LX/0QB;)LX/03R;

    move-result-object v16

    check-cast v16, LX/03R;

    const/16 v17, 0x1458

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {v19 .. v19}, LX/6Ft;->a(LX/0QB;)LX/6Ft;

    move-result-object v18

    check-cast v18, LX/6Ft;

    invoke-static/range {v19 .. v19}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v19

    check-cast v19, LX/0W3;

    invoke-static/range {v2 .. v19}, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->a(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;LX/63V;LX/0Or;LX/0WV;LX/6Fa;LX/0TD;Ljava/util/concurrent/Executor;LX/1gy;LX/01U;LX/0kL;Lcom/facebook/content/SecureContextHelper;LX/6GZ;LX/6G3;LX/0SG;LX/03R;LX/0Or;LX/6Ft;LX/0W3;)V

    return-void
.end method

.method public static e$redex0(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;)V
    .locals 2

    .prologue
    .line 1070392
    iget-boolean v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->o:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1070393
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->i:LX/6GZ;

    sget-object v1, LX/6GY;->BUG_REPORT_DID_ENTER_DESCRIPTION:LX/6GY;

    invoke-virtual {v0, v1}, LX/6GZ;->a(LX/6GY;)V

    .line 1070394
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->q:Z

    .line 1070395
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1070388
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->i:LX/6GZ;

    sget-object v1, LX/6GY;->BUG_REPORT_DID_ATTACH_SCREENSHOT:LX/6GY;

    invoke-virtual {v0, v1}, LX/6GZ;->a(LX/6GY;)V

    .line 1070389
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->c:LX/0TD;

    new-instance v1, LX/6GL;

    invoke-direct {v1, p0, p1}, LX/6GL;-><init>(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;Landroid/net/Uri;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1070390
    new-instance v1, LX/6GM;

    invoke-direct {v1, p0}, LX/6GM;-><init>(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;)V

    iget-object v2, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1070391
    return-object v0
.end method

.method public final a(LX/42n;)V
    .locals 0

    .prologue
    .line 1070386
    iput-object p1, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->n:LX/42n;

    .line 1070387
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1070368
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1070369
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1070370
    iget-boolean v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->t:Z

    if-eqz v0, :cond_0

    .line 1070371
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->x:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67a;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->u:LX/67a;

    .line 1070372
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->u:LX/67a;

    new-instance v1, LX/67b;

    invoke-direct {v1, p0}, LX/67b;-><init>(Landroid/support/v4/app/Fragment;)V

    .line 1070373
    iput-object v1, v0, LX/67a;->b:LX/67V;

    .line 1070374
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->u:LX/67a;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 1070375
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->u:LX/67a;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/67a;->a(I)Z

    .line 1070376
    :cond_0
    if-eqz p1, :cond_1

    .line 1070377
    const-string v0, "report"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/BugReport;

    .line 1070378
    :goto_0
    if-nez v0, :cond_2

    .line 1070379
    sget-object v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->a:Ljava/lang/Class;

    const-string v1, "Missing bug report in intent"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1070380
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->n:LX/42n;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, LX/42n;->a(Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V

    .line 1070381
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->r:Z

    .line 1070382
    :goto_1
    return-void

    .line 1070383
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1070384
    const-string v1, "report"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/BugReport;

    goto :goto_0

    .line 1070385
    :cond_2
    invoke-static {}, Lcom/facebook/bugreporter/BugReport;->newBuilder()LX/6FU;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/6FU;->a(Lcom/facebook/bugreporter/BugReport;)LX/6FU;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->m:LX/6FU;

    goto :goto_1
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1070367
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->m:LX/6FU;

    invoke-virtual {v0}, LX/6FU;->d()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1070362
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->i:LX/6GZ;

    sget-object v1, LX/6GY;->BUG_REPORT_DID_DETACH_SCREENSHOT:LX/6GY;

    invoke-virtual {v0, v1}, LX/6GZ;->a(LX/6GY;)V

    .line 1070363
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->m:LX/6FU;

    .line 1070364
    iget-object v1, v0, LX/6FU;->d:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 1070365
    iget-object v1, v0, LX/6FU;->d:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1070366
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x1a9f5ce1

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1070318
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1070319
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->c:LX/0TD;

    new-instance v2, LX/6GH;

    invoke-direct {v2, p0}, LX/6GH;-><init>(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;)V

    invoke-interface {v0, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->z:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1070320
    iget-boolean v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->t:Z

    if-eqz v0, :cond_2

    .line 1070321
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->u:LX/67a;

    invoke-virtual {v0}, LX/67a;->f()LX/3u1;

    move-result-object v0

    .line 1070322
    new-instance v2, LX/63L;

    invoke-direct {v2, p0, v0}, LX/63L;-><init>(LX/0ew;LX/3u1;)V

    iput-object v2, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->w:LX/63L;

    .line 1070323
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->w:LX/63L;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->v:LX/0h5;

    .line 1070324
    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 1070325
    :goto_0
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->v:LX/0h5;

    const v2, 0x7f0818da

    invoke-interface {v0, v2}, LX/0h5;->setTitle(I)V

    .line 1070326
    const v0, 0x7f0d02a7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->o:Landroid/widget/EditText;

    .line 1070327
    iget-boolean v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->s:Z

    if-eqz v0, :cond_0

    .line 1070328
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->o:Landroid/widget/EditText;

    const-string v2, "May others login as you to debug? How do you reproduce the issue?"

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1070329
    :cond_0
    const v0, 0x7f0d0806

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->p:Landroid/widget/TextView;

    .line 1070330
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    .line 1070331
    iput v3, v0, LX/108;->a:I

    .line 1070332
    move-object v0, v0

    .line 1070333
    const v2, 0x7f0818ef

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1070334
    iput-object v2, v0, LX/108;->g:Ljava/lang/String;

    .line 1070335
    move-object v0, v0

    .line 1070336
    const/4 v2, -0x2

    .line 1070337
    iput v2, v0, LX/108;->h:I

    .line 1070338
    move-object v0, v0

    .line 1070339
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 1070340
    iget-object v2, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->v:LX/0h5;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v2, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1070341
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->v:LX/0h5;

    new-instance v2, LX/6GI;

    invoke-direct {v2, p0}, LX/6GI;-><init>(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;)V

    invoke-interface {v0, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1070342
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->m:LX/6FU;

    .line 1070343
    iget-object v2, v0, LX/6FU;->b:Ljava/lang/String;

    move-object v0, v2

    .line 1070344
    if-eqz v0, :cond_1

    .line 1070345
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->o:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->m:LX/6FU;

    .line 1070346
    iget-object v4, v2, LX/6FU;->b:Ljava/lang/String;

    move-object v2, v4

    .line 1070347
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1070348
    iput-boolean v3, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->q:Z

    .line 1070349
    :cond_1
    new-instance v0, LX/63A;

    invoke-direct {v0}, LX/63A;-><init>()V

    .line 1070350
    new-instance v2, LX/6GJ;

    invoke-direct {v2, p0}, LX/6GJ;-><init>(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;)V

    .line 1070351
    iput-object v2, v0, LX/63A;->a:LX/639;

    .line 1070352
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1070353
    new-instance v3, LX/47x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1070354
    const v4, 0x7f0818db

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1070355
    const-string v4, "[[link]]"

    const v5, 0x7f0818dc

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v5, 0x21

    invoke-virtual {v3, v4, v2, v0, v5}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 1070356
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->p:Landroid/widget/TextView;

    invoke-virtual {v3}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1070357
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->p:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1070358
    const v0, 0x27844f3

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 1070359
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1070360
    invoke-static {v0}, LX/63Z;->a(Landroid/view/View;)Z

    .line 1070361
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->v:LX/0h5;

    goto/16 :goto_0
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 1070287
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 1070288
    iget-boolean v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->t:Z

    if-eqz v0, :cond_0

    .line 1070289
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->w:LX/63L;

    invoke-virtual {v0, p1}, LX/63L;->a(Landroid/view/Menu;)V

    .line 1070290
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x58234610

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1070317
    const v1, 0x7f030208

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x230be28a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDetach()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5f1c279

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1070310
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDetach()V

    .line 1070311
    iget-boolean v1, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->r:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->n:LX/42n;

    if-eqz v1, :cond_0

    .line 1070312
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1070313
    const-string v2, "bug_desc"

    iget-object v3, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->o:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1070314
    const-string v2, "bug_shots"

    iget-object v3, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->m:LX/6FU;

    invoke-virtual {v3}, LX/6FU;->d()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1070315
    iget-object v2, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->n:LX/42n;

    invoke-interface {v2, p0, v1}, LX/42n;->a(Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V

    .line 1070316
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x2e4935d3

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 1070306
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 1070307
    if-nez v0, :cond_0

    iget-boolean v1, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->t:Z

    if-eqz v1, :cond_0

    .line 1070308
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->w:LX/63L;

    invoke-virtual {v0, p1}, LX/63L;->a(Landroid/view/MenuItem;)Z

    move-result v0

    .line 1070309
    :cond_0
    return v0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xc1f8716

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1070302
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1070303
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 1070304
    invoke-static {p0}, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->e$redex0(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;)V

    .line 1070305
    const/16 v1, 0x2b

    const v2, 0x6e5e524b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 1070300
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1070301
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x64d2ccd5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1070296
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1070297
    iget-object v1, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->o:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 1070298
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->o:Landroid/widget/EditText;

    invoke-static {v1, v2}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 1070299
    const/16 v1, 0x2b

    const v2, -0xb19371a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1070291
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1070292
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->m:LX/6FU;

    iget-object v1, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->o:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1070293
    iput-object v1, v0, LX/6FU;->b:Ljava/lang/String;

    .line 1070294
    const-string v0, "report"

    iget-object v1, p0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->m:LX/6FU;

    invoke-virtual {v1}, LX/6FU;->y()Lcom/facebook/bugreporter/BugReport;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1070295
    return-void
.end method
