.class public Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/base/fragment/NavigableFragment;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/6GQ;

.field public c:LX/6GZ;

.field public d:LX/42n;

.field private e:Landroid/widget/ListView;

.field public f:Z

.field private g:LX/67a;

.field private h:LX/0h5;

.field private i:LX/63L;

.field public j:LX/6GP;

.field public k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/67a;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1070487
    const-class v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;

    sput-object v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1070488
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;

    invoke-static {p0}, LX/63V;->a(LX/0QB;)LX/63V;

    move-result-object v2

    check-cast v2, LX/63V;

    const/16 v3, 0x1678

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0XD;->b(LX/0QB;)LX/03R;

    move-result-object v4

    check-cast v4, LX/03R;

    new-instance p1, LX/6GQ;

    const-class v5, Landroid/content/Context;

    invoke-interface {p0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {p0}, LX/0XD;->b(LX/0QB;)LX/03R;

    move-result-object v6

    check-cast v6, LX/03R;

    invoke-direct {p1, v5, v6}, LX/6GQ;-><init>(Landroid/content/Context;LX/03R;)V

    move-object v5, p1

    check-cast v5, LX/6GQ;

    new-instance v0, LX/6GP;

    invoke-static {p0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v6

    check-cast v6, Ljava/util/Locale;

    invoke-static {v6}, LX/3hO;->a(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v6

    move-object v6, v6

    check-cast v6, Ljava/text/Collator;

    invoke-static {p0}, LX/0XD;->b(LX/0QB;)LX/03R;

    move-result-object p1

    check-cast p1, LX/03R;

    invoke-direct {v0, v6, p1}, LX/6GP;-><init>(Ljava/text/Collator;LX/03R;)V

    move-object v6, v0

    check-cast v6, LX/6GP;

    invoke-static {p0}, LX/6GZ;->b(LX/0QB;)LX/6GZ;

    move-result-object p0

    check-cast p0, LX/6GZ;

    iput-object v5, v1, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->b:LX/6GQ;

    iput-object p0, v1, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->c:LX/6GZ;

    invoke-virtual {v2}, LX/63V;->a()Z

    move-result p1

    iput-boolean p1, v1, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->f:Z

    iput-object v3, v1, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->k:LX/0Or;

    iput-object v4, v1, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->l:LX/03R;

    iput-object v6, v1, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->j:LX/6GP;

    return-void
.end method


# virtual methods
.method public final a(LX/42n;)V
    .locals 0

    .prologue
    .line 1070489
    iput-object p1, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->d:LX/42n;

    .line 1070490
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1070491
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1070492
    const-class v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;

    invoke-static {v0, p0}, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1070493
    iget-boolean v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->f:Z

    if-eqz v0, :cond_0

    .line 1070494
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67a;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->g:LX/67a;

    .line 1070495
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->g:LX/67a;

    new-instance v1, LX/67b;

    invoke-direct {v1, p0}, LX/67b;-><init>(Landroid/support/v4/app/Fragment;)V

    .line 1070496
    iput-object v1, v0, LX/67a;->b:LX/67V;

    .line 1070497
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->g:LX/67a;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 1070498
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->g:LX/67a;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/67a;->a(I)Z

    .line 1070499
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v3, -0x329fc4da

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1070500
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1070501
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1070502
    const-string v1, "reporter_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/ConstBugReporterConfig;

    .line 1070503
    new-instance v4, LX/4yi;

    iget-object v1, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->j:LX/6GP;

    invoke-direct {v4, v1}, LX/4yi;-><init>(Ljava/util/Comparator;)V

    .line 1070504
    invoke-virtual {v0}, Lcom/facebook/bugreporter/ConstBugReporterConfig;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_0
    if-ge v1, v6, :cond_2

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    .line 1070505
    sget-object v7, LX/03R;->YES:LX/03R;

    iget-object v8, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->l:LX/03R;

    invoke-virtual {v7, v8}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1070506
    iget-boolean v7, v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->d:Z

    move v7, v7

    .line 1070507
    if-eqz v7, :cond_1

    .line 1070508
    :cond_0
    invoke-virtual {v4, v0}, LX/4yi;->d(Ljava/lang/Object;)LX/4yi;

    .line 1070509
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1070510
    :cond_2
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->b:LX/6GQ;

    invoke-virtual {v4}, LX/4yi;->c()LX/0dW;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->asList()LX/0Px;

    move-result-object v1

    .line 1070511
    iput-object v1, v0, LX/6GQ;->c:LX/0Px;

    .line 1070512
    const v4, 0x7e1c7f42

    invoke-static {v0, v4}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1070513
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->e:Landroid/widget/ListView;

    .line 1070514
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->b:LX/6GQ;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1070515
    iget-boolean v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->f:Z

    if-eqz v0, :cond_4

    .line 1070516
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->g:LX/67a;

    invoke-virtual {v0}, LX/67a;->f()LX/3u1;

    move-result-object v0

    .line 1070517
    new-instance v1, LX/63L;

    invoke-direct {v1, p0, v0}, LX/63L;-><init>(LX/0ew;LX/3u1;)V

    iput-object v1, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->i:LX/63L;

    .line 1070518
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->i:LX/63L;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->h:LX/0h5;

    .line 1070519
    invoke-virtual {p0, v9}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 1070520
    :goto_1
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->h:LX/0h5;

    const v1, 0x7f0818f9

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 1070521
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->e:Landroid/widget/ListView;

    new-instance v1, LX/6GR;

    invoke-direct {v1, p0}, LX/6GR;-><init>(Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1070522
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1070523
    const-string v1, "retry"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1070524
    if-eqz v0, :cond_3

    .line 1070525
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1070526
    const-string v1, "retry"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1070527
    iget-object v1, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->d:LX/42n;

    invoke-interface {v1, p0, v0}, LX/42n;->a(Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V

    .line 1070528
    :cond_3
    const v0, 0x711598ae

    invoke-static {v0, v3}, LX/02F;->f(II)V

    return-void

    .line 1070529
    :cond_4
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1070530
    invoke-static {v0}, LX/63Z;->a(Landroid/view/View;)Z

    .line 1070531
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->h:LX/0h5;

    goto :goto_1
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 1070532
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 1070533
    iget-boolean v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->f:Z

    if-eqz v0, :cond_0

    .line 1070534
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->i:LX/63L;

    invoke-virtual {v0, p1}, LX/63L;->a(Landroid/view/Menu;)V

    .line 1070535
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5126bac

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1070536
    const v1, 0x7f03025b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x6880041e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 1070537
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 1070538
    if-nez v0, :cond_0

    iget-boolean v1, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->f:Z

    if-eqz v1, :cond_0

    .line 1070539
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->i:LX/63L;

    invoke-virtual {v0, p1}, LX/63L;->a(Landroid/view/MenuItem;)Z

    move-result v0

    .line 1070540
    :cond_0
    return v0
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 1070541
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1070542
    return-void
.end method
