.class public Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field public final c:J

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1070435
    new-instance v0, LX/6GO;

    invoke-direct {v0}, LX/6GO;-><init>()V

    sput-object v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1070438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1070439
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a:Ljava/lang/String;

    .line 1070440
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b:Ljava/lang/String;

    .line 1070441
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->c:J

    .line 1070442
    const/4 v0, 0x1

    new-array v0, v0, [Z

    .line 1070443
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 1070444
    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->d:Z

    .line 1070445
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;JZ)V
    .locals 8

    .prologue
    .line 1070436
    move-object v1, p0

    move-object v2, p1

    move-object v3, p1

    move-wide v4, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;-><init>(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 1070437
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 1

    .prologue
    .line 1070427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1070428
    iput-object p1, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a:Ljava/lang/String;

    .line 1070429
    iput-object p2, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b:Ljava/lang/String;

    .line 1070430
    iput-wide p3, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->c:J

    .line 1070431
    iput-boolean p5, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->d:Z

    .line 1070432
    return-void
.end method

.method public static a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;
    .locals 3

    .prologue
    .line 1070433
    new-instance v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;-><init>(Ljava/lang/String;JZ)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;
    .locals 8

    .prologue
    .line 1070434
    new-instance v1, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    const/4 v6, 0x1

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;-><init>(Ljava/lang/String;Ljava/lang/String;JZ)V

    return-object v1
.end method

.method public static b(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;
    .locals 3

    .prologue
    .line 1070419
    new-instance v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;-><init>(Ljava/lang/String;JZ)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/03R;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1070420
    sget-object v0, LX/03R;->YES:LX/03R;

    invoke-virtual {v0, p1}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1070421
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1070422
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1070423
    iget-object v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1070424
    iget-wide v0, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1070425
    const/4 v0, 0x1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->d:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 1070426
    return-void
.end method
