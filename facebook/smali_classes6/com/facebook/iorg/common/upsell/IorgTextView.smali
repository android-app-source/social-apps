.class public Lcom/facebook/iorg/common/upsell/IorgTextView;
.super Landroid/widget/TextView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1109130
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1109131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1109118
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1109119
    invoke-direct {p0, p1, p2}, Lcom/facebook/iorg/common/upsell/IorgTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1109120
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1109127
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1109128
    invoke-direct {p0, p1, p2}, Lcom/facebook/iorg/common/upsell/IorgTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1109129
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1109121
    sget-object v0, LX/03r;->IorgTextView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1109122
    const/16 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1109123
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1109124
    if-eqz v1, :cond_0

    .line 1109125
    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/iorg/common/upsell/IorgTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1109126
    :cond_0
    return-void
.end method
