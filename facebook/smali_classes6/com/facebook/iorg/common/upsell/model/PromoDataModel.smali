.class public Lcom/facebook/iorg/common/upsell/model/PromoDataModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/iorg/common/upsell/model/PromoDataModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:LX/6Y4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1109135
    new-instance v0, LX/6Y3;

    invoke-direct {v0}, LX/6Y3;-><init>()V

    sput-object v0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1109136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109137
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->a:Ljava/lang/String;

    .line 1109138
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->b:Ljava/lang/String;

    .line 1109139
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->c:Ljava/lang/String;

    .line 1109140
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->d:Ljava/lang/String;

    .line 1109141
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->e:Ljava/lang/String;

    .line 1109142
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->f:Ljava/lang/String;

    .line 1109143
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->g:Ljava/lang/String;

    .line 1109144
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->h:Ljava/lang/String;

    .line 1109145
    sget-object v0, LX/6Y4;->UNKNOWN:LX/6Y4;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->i:LX/6Y4;

    .line 1109146
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1109147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->a:Ljava/lang/String;

    .line 1109149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->b:Ljava/lang/String;

    .line 1109150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->c:Ljava/lang/String;

    .line 1109151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->d:Ljava/lang/String;

    .line 1109152
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->e:Ljava/lang/String;

    .line 1109153
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->f:Ljava/lang/String;

    .line 1109154
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->g:Ljava/lang/String;

    .line 1109155
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->h:Ljava/lang/String;

    .line 1109156
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6Y4;->fromString(Ljava/lang/String;)LX/6Y4;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->i:LX/6Y4;

    .line 1109157
    return-void
.end method

.method public constructor <init>(Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;)V
    .locals 2

    .prologue
    .line 1109158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109159
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->e:Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;

    move-object v0, v0

    .line 1109160
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1109161
    iput-object v1, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->b:Ljava/lang/String;

    .line 1109162
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1109163
    iput-object v1, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->c:Ljava/lang/String;

    .line 1109164
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1109165
    iput-object v1, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->d:Ljava/lang/String;

    .line 1109166
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1109167
    iput-object v1, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->f:Ljava/lang/String;

    .line 1109168
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->e:Ljava/lang/String;

    move-object v0, v1

    .line 1109169
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->g:Ljava/lang/String;

    .line 1109170
    sget-object v0, LX/6Y4;->UNKNOWN:LX/6Y4;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->i:LX/6Y4;

    .line 1109171
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Y4;)V
    .locals 0

    .prologue
    .line 1109172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109173
    iput-object p1, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->a:Ljava/lang/String;

    .line 1109174
    iput-object p2, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->b:Ljava/lang/String;

    .line 1109175
    iput-object p3, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->c:Ljava/lang/String;

    .line 1109176
    iput-object p4, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->d:Ljava/lang/String;

    .line 1109177
    iput-object p5, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->e:Ljava/lang/String;

    .line 1109178
    iput-object p6, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->f:Ljava/lang/String;

    .line 1109179
    iput-object p7, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->g:Ljava/lang/String;

    .line 1109180
    iput-object p8, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->h:Ljava/lang/String;

    .line 1109181
    iput-object p9, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->i:LX/6Y4;

    .line 1109182
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1109183
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1109184
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109185
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109186
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109187
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109188
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109189
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109190
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109191
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109192
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->i:LX/6Y4;

    invoke-virtual {v0}, LX/6Y4;->getParamName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109193
    return-void
.end method
