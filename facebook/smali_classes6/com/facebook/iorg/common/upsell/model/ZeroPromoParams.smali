.class public Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:LX/6Y4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1109271
    new-instance v0, LX/6Y8;

    invoke-direct {v0}, LX/6Y8;-><init>()V

    sput-object v0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1109266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109267
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->a:Ljava/lang/String;

    .line 1109268
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->b:Ljava/lang/String;

    .line 1109269
    sget-object v0, LX/6Y4;->UNKNOWN:LX/6Y4;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->c:LX/6Y4;

    .line 1109270
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1109250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109251
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->a:Ljava/lang/String;

    .line 1109252
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->b:Ljava/lang/String;

    .line 1109253
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6Y4;->fromString(Ljava/lang/String;)LX/6Y4;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->c:LX/6Y4;

    .line 1109254
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/6Y4;)V
    .locals 0

    .prologue
    .line 1109261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109262
    iput-object p1, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->a:Ljava/lang/String;

    .line 1109263
    iput-object p2, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->b:Ljava/lang/String;

    .line 1109264
    iput-object p3, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->c:LX/6Y4;

    .line 1109265
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1109260
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 1109259
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ZeroPromoParams{mEncodedPhone=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPromoId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->c:LX/6Y4;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1109255
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109256
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109257
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;->c:LX/6Y4;

    invoke-virtual {v0}, LX/6Y4;->getParamName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109258
    return-void
.end method
