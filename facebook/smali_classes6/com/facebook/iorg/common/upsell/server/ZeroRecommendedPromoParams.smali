.class public Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Z

.field private d:LX/6Y4;

.field private e:LX/0yY;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1109541
    new-instance v0, LX/6YF;

    invoke-direct {v0}, LX/6YF;-><init>()V

    sput-object v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1109534
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109535
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->a:I

    .line 1109536
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->b:Ljava/lang/String;

    .line 1109537
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->c:Z

    .line 1109538
    sget-object v0, LX/6Y4;->UNKNOWN:LX/6Y4;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->d:LX/6Y4;

    .line 1109539
    sget-object v0, LX/0yY;->UNKNOWN:LX/0yY;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->e:LX/0yY;

    .line 1109540
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;ZLX/6Y4;LX/0yY;)V
    .locals 0

    .prologue
    .line 1109527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109528
    iput p1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->a:I

    .line 1109529
    iput-object p2, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->b:Ljava/lang/String;

    .line 1109530
    iput-boolean p3, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->c:Z

    .line 1109531
    iput-object p4, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->d:LX/6Y4;

    .line 1109532
    iput-object p5, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->e:LX/0yY;

    .line 1109533
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1109519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109520
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->a:I

    .line 1109521
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->b:Ljava/lang/String;

    .line 1109522
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->c:Z

    .line 1109523
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6Y4;->fromString(Ljava/lang/String;)LX/6Y4;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->d:LX/6Y4;

    .line 1109524
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0yY;->fromString(Ljava/lang/String;)LX/0yY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->e:LX/0yY;

    .line 1109525
    return-void

    .line 1109526
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109542
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->d:LX/6Y4;

    invoke-virtual {v0}, LX/6Y4;->getParamName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109518
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->e:LX/0yY;

    iget-object v0, v0, LX/0yY;->prefString:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1109517
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1109516
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ZeroRecommendedPromoParams{limit="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", scale=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", refresh="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->d:LX/6Y4;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", zeroFeatureKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->e:LX/0yY;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1109509
    iget v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1109510
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109511
    iget-boolean v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1109512
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->d:LX/6Y4;

    invoke-virtual {v0}, LX/6Y4;->getParamName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109513
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->e:LX/0yY;

    iget-object v0, v0, LX/0yY;->prefString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109514
    return-void

    .line 1109515
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
