.class public Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final B:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final C:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final D:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/UpsellPromo;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final h:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final j:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final k:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final l:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final m:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final n:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final o:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final p:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final q:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final r:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final s:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final t:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final u:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/UpsellPromo;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final w:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final x:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final y:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final z:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1109631
    new-instance v0, LX/6YG;

    invoke-direct {v0}, LX/6YG;-><init>()V

    sput-object v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 34

    .prologue
    .line 1109629
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, ""

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, 0x0

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v33}, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;JLX/0Px;Ljava/lang/String;Ljava/lang/String;IILX/0Px;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1109630
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1109592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109593
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->a:Ljava/lang/String;

    .line 1109594
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->b:Ljava/lang/String;

    .line 1109595
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->d:Ljava/lang/String;

    .line 1109596
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->g:Ljava/lang/String;

    .line 1109597
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->h:Ljava/lang/String;

    .line 1109598
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->i:Ljava/lang/String;

    .line 1109599
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->m:Ljava/lang/String;

    .line 1109600
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->n:Ljava/lang/String;

    .line 1109601
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->o:J

    .line 1109602
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->j:Ljava/lang/String;

    .line 1109603
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->p:Ljava/lang/String;

    .line 1109604
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->q:J

    .line 1109605
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1109606
    sget-object v1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1109607
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->c:LX/0Px;

    .line 1109608
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->r:Ljava/lang/String;

    .line 1109609
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->s:Ljava/lang/String;

    .line 1109610
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->t:I

    .line 1109611
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->u:I

    .line 1109612
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1109613
    sget-object v1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1109614
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->v:LX/0Px;

    .line 1109615
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->w:Ljava/lang/String;

    .line 1109616
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->x:Ljava/lang/String;

    .line 1109617
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->y:Z

    .line 1109618
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->z:Ljava/lang/String;

    .line 1109619
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->A:Ljava/lang/String;

    .line 1109620
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->B:Ljava/lang/String;

    .line 1109621
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->C:Ljava/lang/String;

    .line 1109622
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->k:Ljava/lang/String;

    .line 1109623
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->l:Ljava/lang/String;

    .line 1109624
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->D:Ljava/lang/String;

    .line 1109625
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->e:Ljava/lang/String;

    .line 1109626
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->f:Ljava/lang/String;

    .line 1109627
    return-void

    .line 1109628
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;JLX/0Px;Ljava/lang/String;Ljava/lang/String;IILX/0Px;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "LX/0Px",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/UpsellPromo;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "LX/0Px",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/UpsellPromo;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1109632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109633
    iput-object p1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->a:Ljava/lang/String;

    .line 1109634
    iput-object p2, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->b:Ljava/lang/String;

    .line 1109635
    iput-object p3, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->d:Ljava/lang/String;

    .line 1109636
    iput-object p4, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->g:Ljava/lang/String;

    .line 1109637
    iput-object p5, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->h:Ljava/lang/String;

    .line 1109638
    iput-object p6, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->i:Ljava/lang/String;

    .line 1109639
    iput-object p7, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->m:Ljava/lang/String;

    .line 1109640
    iput-object p8, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->n:Ljava/lang/String;

    .line 1109641
    iput-wide p9, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->o:J

    .line 1109642
    iput-object p11, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->j:Ljava/lang/String;

    .line 1109643
    iput-object p12, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->p:Ljava/lang/String;

    .line 1109644
    iput-wide p13, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->q:J

    .line 1109645
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->c:LX/0Px;

    .line 1109646
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->r:Ljava/lang/String;

    .line 1109647
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->s:Ljava/lang/String;

    .line 1109648
    move/from16 v0, p18

    iput v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->t:I

    .line 1109649
    move/from16 v0, p19

    iput v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->u:I

    .line 1109650
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->v:LX/0Px;

    .line 1109651
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->w:Ljava/lang/String;

    .line 1109652
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->x:Ljava/lang/String;

    .line 1109653
    move/from16 v0, p23

    iput-boolean v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->y:Z

    .line 1109654
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->z:Ljava/lang/String;

    .line 1109655
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->A:Ljava/lang/String;

    .line 1109656
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->B:Ljava/lang/String;

    .line 1109657
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->C:Ljava/lang/String;

    .line 1109658
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->k:Ljava/lang/String;

    .line 1109659
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->l:Ljava/lang/String;

    .line 1109660
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->D:Ljava/lang/String;

    .line 1109661
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->e:Ljava/lang/String;

    .line 1109662
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->f:Ljava/lang/String;

    .line 1109663
    return-void
.end method

.method private static a(Lorg/json/JSONArray;)LX/0Px;
    .locals 3
    .param p0    # Lorg/json/JSONArray;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/UpsellPromo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1109582
    if-nez p0, :cond_0

    .line 1109583
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1109584
    :goto_0
    return-object v0

    .line 1109585
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1109586
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1109587
    invoke-virtual {p0, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->a(Lorg/json/JSONObject;)Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    move-result-object v2

    .line 1109588
    if-eqz v2, :cond_1

    .line 1109589
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1109590
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1109591
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;
    .locals 36
    .param p0    # Lorg/json/JSONObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1109579
    if-nez p0, :cond_0

    .line 1109580
    new-instance v3, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    invoke-direct {v3}, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;-><init>()V

    .line 1109581
    :goto_0
    return-object v3

    :cond_0
    new-instance v3, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    const-string v2, "title"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v2, "top_message"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v2, "carrierLogoUrl"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v2, "button_text"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v2, "accountType"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v2, "message"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v2, "errorMessage"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v2, "loadBalance"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v2, "balanceUpdatedAt"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v12

    const-string v2, "promoName"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v2, "promoRemaining"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v2, "promoExpiry"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v16

    const-string v2, "promos"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->a(Lorg/json/JSONArray;)LX/0Px;

    move-result-object v18

    const-string v2, "phone"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    const-string v2, "bannerImage"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    const-string v2, "startBannerColor"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v21

    const-string v2, "endBannerColor"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v22

    const-string v2, "outstandingLoans"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->a(Lorg/json/JSONArray;)LX/0Px;

    move-result-object v23

    const-string v2, "carrierManagerTitle"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    const-string v2, "campaignExpiryText"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    const-string v2, "createShortcut"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v26

    const-string v2, "shortcutIcon"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    const-string v2, "shortcutName"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    const-string v2, "shortcutAlertIcon"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    const-string v2, "shortcutAlertText"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    const-string v2, "currentPromoTitle"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    const-string v2, "additionalPromoTitle"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    const-string v2, "buy_url"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    const-string v2, "buy_text"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    const-string v2, "promoScreenTitle"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v3 .. v35}, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;JLX/0Px;Ljava/lang/String;Ljava/lang/String;IILX/0Px;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1109546
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1109547
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109548
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109549
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109550
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109551
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109552
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109553
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109554
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109555
    iget-wide v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->o:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1109556
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109557
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109558
    iget-wide v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->q:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1109559
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1109560
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109561
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109562
    iget v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->t:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1109563
    iget v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->u:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1109564
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->v:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1109565
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109566
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->x:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109567
    iget-boolean v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->y:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1109568
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109569
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109570
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->B:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109571
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->C:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109572
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109573
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109574
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->D:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109575
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109576
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109577
    return-void

    .line 1109578
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
