.class public Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field public final c:Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1109505
    new-instance v0, LX/6YC;

    invoke-direct {v0}, LX/6YC;-><init>()V

    sput-object v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1109497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109498
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->a:Ljava/lang/String;

    .line 1109499
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->b:Ljava/lang/String;

    .line 1109500
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->e:Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;

    .line 1109501
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->c:Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    .line 1109502
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1109503
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->d:LX/0Px;

    .line 1109504
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1109453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109454
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->a:Ljava/lang/String;

    .line 1109455
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->b:Ljava/lang/String;

    .line 1109456
    const-class v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->e:Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;

    .line 1109457
    const-class v0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->c:Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    .line 1109458
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1109459
    sget-object v1, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1109460
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->d:LX/0Px;

    .line 1109461
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;Lcom/facebook/iorg/common/upsell/server/UpsellPromo;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;",
            "Lcom/facebook/iorg/common/upsell/server/UpsellPromo;",
            "LX/0Px",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1109490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109491
    iput-object p1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->a:Ljava/lang/String;

    .line 1109492
    iput-object p2, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->b:Ljava/lang/String;

    .line 1109493
    iput-object p3, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->e:Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;

    .line 1109494
    iput-object p4, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->c:Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    .line 1109495
    iput-object p5, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->d:LX/0Px;

    .line 1109496
    return-void
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;
    .locals 15
    .param p0    # Lorg/json/JSONObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1109470
    if-nez p0, :cond_0

    .line 1109471
    new-instance v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;

    invoke-direct {v0}, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;-><init>()V

    .line 1109472
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;

    const-string v1, "status"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "description"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "page"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 1109473
    if-nez v3, :cond_1

    .line 1109474
    new-instance v6, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;

    invoke-direct {v6}, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;-><init>()V

    .line 1109475
    :goto_1
    move-object v3, v6

    .line 1109476
    const-string v4, "loan"

    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->a(Lorg/json/JSONObject;)Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    move-result-object v4

    const-string v5, "pages"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 1109477
    if-nez v5, :cond_2

    .line 1109478
    sget-object v6, LX/0Q7;->a:LX/0Px;

    move-object v6, v6

    .line 1109479
    :goto_2
    move-object v5, v6

    .line 1109480
    invoke-direct/range {v0 .. v5}, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;Lcom/facebook/iorg/common/upsell/server/UpsellPromo;LX/0Px;)V

    goto :goto_0

    :cond_1
    new-instance v6, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;

    const-string v7, "title"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "content"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "top_message"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "message"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "button_text"

    invoke-virtual {v3, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v6 .. v11}, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1109481
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1109482
    const/4 v6, 0x0

    :goto_3
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v6, v8, :cond_3

    .line 1109483
    invoke-virtual {v5, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 1109484
    if-nez v8, :cond_4

    .line 1109485
    new-instance v9, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;

    invoke-direct {v9}, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;-><init>()V

    .line 1109486
    :goto_4
    move-object v8, v9

    .line 1109487
    invoke-virtual {v7, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1109488
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1109489
    :cond_3
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    goto :goto_2

    :cond_4
    new-instance v9, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;

    const-string v10, "title"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "content"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "primary_button"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "secondary_button"

    invoke-virtual {v8, v13}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "third_button"

    invoke-virtual {v8, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-direct/range {v9 .. v14}, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method


# virtual methods
.method public final c()Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;
    .locals 2

    .prologue
    .line 1109469
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->d:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1109468
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1109462
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109463
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109464
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->e:Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1109465
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->c:Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1109466
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1109467
    return-void
.end method
