.class public final Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1109424
    new-instance v0, LX/6YE;

    invoke-direct {v0}, LX/6YE;-><init>()V

    sput-object v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1109425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109426
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->a:Ljava/lang/String;

    .line 1109427
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->b:Ljava/lang/String;

    .line 1109428
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->c:Ljava/lang/String;

    .line 1109429
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->d:Ljava/lang/String;

    .line 1109430
    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->e:Ljava/lang/String;

    .line 1109431
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1109432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109433
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->a:Ljava/lang/String;

    .line 1109434
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->b:Ljava/lang/String;

    .line 1109435
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->c:Ljava/lang/String;

    .line 1109436
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->d:Ljava/lang/String;

    .line 1109437
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->e:Ljava/lang/String;

    .line 1109438
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1109439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109440
    iput-object p1, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->a:Ljava/lang/String;

    .line 1109441
    iput-object p2, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->b:Ljava/lang/String;

    .line 1109442
    iput-object p3, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->c:Ljava/lang/String;

    .line 1109443
    iput-object p4, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->d:Ljava/lang/String;

    .line 1109444
    iput-object p5, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->e:Ljava/lang/String;

    .line 1109445
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1109446
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1109447
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109448
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109449
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109450
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109451
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109452
    return-void
.end method
