.class public Lcom/facebook/iorg/common/upsell/server/UpsellPromo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/UpsellPromo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Z

.field public final h:Ljava/lang/String;

.field public final i:Z

.field public final j:Z

.field public final k:Ljava/lang/String;

.field private final l:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final m:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final n:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final o:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1109331
    new-instance v0, LX/6YA;

    invoke-direct {v0}, LX/6YA;-><init>()V

    sput-object v0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 16

    .prologue
    .line 1109403
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v15}, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;LX/0Px;)V

    .line 1109404
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1109380
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109381
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->b:Ljava/lang/String;

    .line 1109382
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->c:Ljava/lang/String;

    .line 1109383
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->d:Ljava/lang/String;

    .line 1109384
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->e:Ljava/lang/String;

    .line 1109385
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->f:Ljava/lang/String;

    .line 1109386
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->g:Z

    .line 1109387
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->m:Ljava/lang/String;

    .line 1109388
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->k:Ljava/lang/String;

    .line 1109389
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->i:Z

    .line 1109390
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->n:Ljava/lang/String;

    .line 1109391
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->l:Z

    .line 1109392
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->j:Z

    .line 1109393
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->h:Ljava/lang/String;

    .line 1109394
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->o:Ljava/lang/String;

    .line 1109395
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1109396
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1109397
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->a:LX/0Px;

    .line 1109398
    return-void

    :cond_0
    move v0, v2

    .line 1109399
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1109400
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1109401
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1109402
    goto :goto_3
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1109363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109364
    iput-object p1, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->b:Ljava/lang/String;

    .line 1109365
    iput-object p2, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->c:Ljava/lang/String;

    .line 1109366
    iput-object p3, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->d:Ljava/lang/String;

    .line 1109367
    iput-object p4, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->e:Ljava/lang/String;

    .line 1109368
    iput-object p5, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->f:Ljava/lang/String;

    .line 1109369
    iput-boolean p6, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->g:Z

    .line 1109370
    iput-object p7, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->m:Ljava/lang/String;

    .line 1109371
    iput-object p8, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->k:Ljava/lang/String;

    .line 1109372
    iput-boolean p9, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->i:Z

    .line 1109373
    iput-object p10, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->n:Ljava/lang/String;

    .line 1109374
    iput-boolean p11, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->l:Z

    .line 1109375
    iput-boolean p12, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->j:Z

    .line 1109376
    iput-object p13, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->h:Ljava/lang/String;

    .line 1109377
    iput-object p14, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->o:Ljava/lang/String;

    .line 1109378
    iput-object p15, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->a:LX/0Px;

    .line 1109379
    return-void
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/facebook/iorg/common/upsell/server/UpsellPromo;
    .locals 17
    .param p0    # Lorg/json/JSONObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1109353
    if-nez p0, :cond_0

    .line 1109354
    const/4 v1, 0x0

    .line 1109355
    :goto_0
    return-object v1

    .line 1109356
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v16

    .line 1109357
    const-string v1, "types"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1109358
    if-eqz v2, :cond_1

    .line 1109359
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1109360
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1109361
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1109362
    :cond_1
    new-instance v1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    const-string v2, "name"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "short_name"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "desc"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "code"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "price"

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "is_loan"

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v7

    const-string v8, "message"

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "sms_number"

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "current_promo"

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v10

    const-string v11, "buy_url"

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "is_selected"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v12

    const-string v13, "is_enabled"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v13

    const-string v14, "purchase_button_text"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "extra_confirm_text"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {v16 .. v16}, LX/0Pz;->b()LX/0Px;

    move-result-object v16

    invoke-direct/range {v1 .. v16}, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;LX/0Px;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1109352
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1109332
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109333
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109334
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109335
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109336
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109337
    iget-boolean v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->g:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1109338
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109339
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109340
    iget-boolean v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->i:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1109341
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109342
    iget-boolean v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->l:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1109343
    iget-boolean v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->j:Z

    if-eqz v0, :cond_3

    :goto_3
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1109344
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109345
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109346
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1109347
    return-void

    :cond_0
    move v0, v2

    .line 1109348
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1109349
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1109350
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1109351
    goto :goto_3
.end method
