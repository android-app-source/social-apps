.class public Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1109296
    new-instance v0, LX/6Y9;

    invoke-direct {v0}, LX/6Y9;-><init>()V

    sput-object v0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1109289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109290
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->a:Ljava/lang/String;

    .line 1109291
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->b:Ljava/lang/String;

    .line 1109292
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->c:Ljava/lang/String;

    .line 1109293
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->d:Ljava/lang/String;

    .line 1109294
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->e:Ljava/lang/String;

    .line 1109295
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1109297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109298
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->a:Ljava/lang/String;

    .line 1109299
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->b:Ljava/lang/String;

    .line 1109300
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->c:Ljava/lang/String;

    .line 1109301
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->d:Ljava/lang/String;

    .line 1109302
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->e:Ljava/lang/String;

    .line 1109303
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1109282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109283
    iput-object p1, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->a:Ljava/lang/String;

    .line 1109284
    iput-object p2, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->b:Ljava/lang/String;

    .line 1109285
    iput-object p3, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->c:Ljava/lang/String;

    .line 1109286
    iput-object p4, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->d:Ljava/lang/String;

    .line 1109287
    iput-object p5, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->e:Ljava/lang/String;

    .line 1109288
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1109281
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1109275
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109276
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109277
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109278
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109279
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1109280
    return-void
.end method
