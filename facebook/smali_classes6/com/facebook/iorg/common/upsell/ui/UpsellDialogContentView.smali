.class public Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1109863
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1109864
    invoke-direct {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->a()V

    .line 1109865
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1109860
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1109861
    invoke-direct {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->a()V

    .line 1109862
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1109813
    invoke-virtual {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1109814
    const v1, 0x7f031555

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1109815
    const v0, 0x7f0d3006

    invoke-virtual {p0, v0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->a:Landroid/widget/TextView;

    .line 1109816
    const v0, 0x7f0d3007

    invoke-virtual {p0, v0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->b:Landroid/widget/TextView;

    .line 1109817
    const v0, 0x7f0d3008

    invoke-virtual {p0, v0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->c:Landroid/widget/TextView;

    .line 1109818
    const v0, 0x7f0d3009

    invoke-virtual {p0, v0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->d:Landroid/widget/TextView;

    .line 1109819
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->setOrientation(I)V

    .line 1109820
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->setVisibility(I)V

    .line 1109821
    return-void
.end method


# virtual methods
.method public final a(LX/6Y5;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1109822
    iget-object v0, p1, LX/6Y5;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1109823
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1109824
    iget-object v0, p1, LX/6Y5;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1109825
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1109826
    :cond_0
    iget-object v0, p1, LX/6Y5;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1109827
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1109828
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->b:Landroid/widget/TextView;

    .line 1109829
    iget-object v1, p1, LX/6Y5;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1109830
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1109831
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->b:Landroid/widget/TextView;

    .line 1109832
    iget-object v1, p1, LX/6Y5;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1109833
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1109834
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1109835
    :cond_1
    iget-object v0, p1, LX/6Y5;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1109836
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1109837
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->c:Landroid/widget/TextView;

    .line 1109838
    iget-object v1, p1, LX/6Y5;->g:Ljava/lang/String;

    move-object v1, v1

    .line 1109839
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1109840
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->c:Landroid/widget/TextView;

    .line 1109841
    iget-object v1, p1, LX/6Y5;->g:Ljava/lang/String;

    move-object v1, v1

    .line 1109842
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1109843
    :cond_2
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->a:Landroid/widget/TextView;

    .line 1109844
    iget-object v1, p1, LX/6Y5;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1109845
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1109846
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->a:Landroid/widget/TextView;

    .line 1109847
    iget-object v1, p1, LX/6Y5;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1109848
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1109849
    invoke-virtual {p0, v2}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->setVisibility(I)V

    .line 1109850
    :cond_3
    iget-object v0, p1, LX/6Y5;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1109851
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1109852
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->d:Landroid/widget/TextView;

    .line 1109853
    iget-object v1, p1, LX/6Y5;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1109854
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1109855
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->d:Landroid/widget/TextView;

    .line 1109856
    iget-object v1, p1, LX/6Y5;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1109857
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1109858
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1109859
    :cond_4
    return-void
.end method
