.class public Lcom/facebook/iorg/common/upsell/ui/SpinnerDialogFragment;
.super Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;
.source ""


# static fields
.field public static t:Z

.field public static u:Ljava/lang/String;

.field public static v:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1109691
    invoke-direct {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;-><init>()V

    .line 1109692
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 1109671
    invoke-super {p0, p1}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 1109672
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 1109673
    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109690
    const-string v0, "zero_extra_charges_dialog_open"

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109689
    const-string v0, "zero_extra_charges_dialog_confirm"

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x639a3343

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1109686
    invoke-super {p0, p1}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1109687
    const/4 v1, 0x0

    const v2, 0x7f0e03f3

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1109688
    const/16 v1, 0x2b

    const v2, 0x13dd5c84

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x1e89113b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1109675
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1109676
    new-instance v2, LX/6YP;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/6YP;-><init>(Landroid/content/Context;)V

    .line 1109677
    sget-boolean v3, Lcom/facebook/iorg/common/upsell/ui/SpinnerDialogFragment;->t:Z

    if-eqz v3, :cond_0

    .line 1109678
    invoke-virtual {v2}, LX/6YP;->a()V

    .line 1109679
    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1109680
    const v2, -0x6208d3a

    invoke-static {v2, v0}, LX/02F;->f(II)V

    return-object v1

    .line 1109681
    :cond_0
    new-instance v3, LX/6Y5;

    invoke-direct {v3}, LX/6Y5;-><init>()V

    sget-object v4, Lcom/facebook/iorg/common/upsell/ui/SpinnerDialogFragment;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/6Y5;->a(Ljava/lang/String;)LX/6Y5;

    move-result-object v3

    sget-object v4, Lcom/facebook/iorg/common/upsell/ui/SpinnerDialogFragment;->v:Ljava/lang/String;

    .line 1109682
    iput-object v4, v3, LX/6Y5;->c:Ljava/lang/String;

    .line 1109683
    move-object v3, v3

    .line 1109684
    const v4, 0x7f080e0f

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/6YI;

    invoke-direct {v5, p0}, LX/6YI;-><init>(Lcom/facebook/iorg/common/upsell/ui/SpinnerDialogFragment;)V

    invoke-virtual {v3, v4, v5}, LX/6Y5;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v3

    .line 1109685
    invoke-virtual {v2, v3}, LX/6YP;->a(LX/6Y5;)V

    goto :goto_0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109674
    const-string v0, "zero_extra_charges_dialog_cancel"

    return-object v0
.end method
