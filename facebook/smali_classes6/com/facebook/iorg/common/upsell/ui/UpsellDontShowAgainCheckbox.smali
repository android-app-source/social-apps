.class public Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/CheckBox;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1110186
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1110187
    invoke-direct {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->a()V

    .line 1110188
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1110183
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1110184
    invoke-direct {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->a()V

    .line 1110185
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 1110178
    invoke-virtual {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031557

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1110179
    const v0, 0x7f0d300c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->b:Landroid/widget/TextView;

    .line 1110180
    const v0, 0x7f0d300b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->a:Landroid/widget/CheckBox;

    .line 1110181
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->a:Landroid/widget/CheckBox;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "btn_check_holo_light"

    const-string v3, "drawable"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setButtonDrawable(I)V

    .line 1110182
    return-void
.end method


# virtual methods
.method public setCheckListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 1110176
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1110177
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 1110170
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1110171
    return-void
.end method

.method public setText(I)V
    .locals 1

    .prologue
    .line 1110174
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 1110175
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1110172
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1110173
    return-void
.end method
