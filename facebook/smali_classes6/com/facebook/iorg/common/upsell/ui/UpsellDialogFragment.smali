.class public Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;
.super Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;
.source ""


# instance fields
.field private A:Landroid/os/Handler;

.field public t:Landroid/widget/LinearLayout;

.field public u:Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/6YN;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment$ScreenController;",
            ">;>;"
        }
    .end annotation
.end field

.field public x:Z

.field private y:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/6YN;",
            "LX/6YO;",
            ">;"
        }
    .end annotation
.end field

.field private z:LX/7Y7;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1109996
    invoke-direct {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;-><init>()V

    .line 1109997
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->x:Z

    .line 1109998
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->y:Ljava/util/Map;

    .line 1109999
    iput-object v1, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->z:LX/7Y7;

    .line 1110000
    iput-object v1, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->A:Landroid/os/Handler;

    .line 1110001
    return-void
.end method

.method public static a(LX/0yY;Ljava/lang/Object;LX/6YN;ILjava/lang/Object;LX/4g1;)Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;
    .locals 7
    .param p4    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1110002
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/0yY;Ljava/lang/Object;LX/6YN;ILjava/lang/Object;LX/4g1;LX/3J6;)Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/0yY;Ljava/lang/Object;LX/6YN;ILjava/lang/Object;LX/4g1;LX/3J6;)Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;
    .locals 7
    .param p4    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/3J6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1110003
    new-instance v6, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-direct {v6}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;-><init>()V

    .line 1110004
    if-eqz p6, :cond_0

    iget-object v5, p6, LX/3J6;->h:Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;

    :goto_0
    move-object v0, p0

    move-object v2, v1

    move-object v3, p4

    move-object v4, p5

    .line 1110005
    invoke-static/range {v0 .. v5}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->a(LX/0yY;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;LX/4g1;Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 1110006
    const-string v1, "current_screen"

    invoke-virtual {p2}, LX/6YN;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1110007
    const-string v1, "title_extra_image_resource_id"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1110008
    const-string v1, "promo_data_model"

    check-cast p1, Landroid/os/Parcelable;

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1110009
    invoke-virtual {v6, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1110010
    return-object v6

    :cond_0
    move-object v5, v1

    .line 1110011
    goto :goto_0
.end method

.method private static a(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;LX/7Y7;Landroid/os/Handler;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 3
    .param p1    # LX/7Y7;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p12    # LX/0Ot;
        .annotation build Lcom/facebook/iorg/common/upsell/annotations/UpsellDataControlActiveController;
        .end annotation
    .end param
    .param p13    # LX/0Ot;
        .annotation build Lcom/facebook/iorg/common/upsell/annotations/ZeroBalanceSpinnerController;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/iorg/common/zero/interfaces/IorgAndroidThreadUtil;",
            "Landroid/os/Handler;",
            "LX/0Ot",
            "<",
            "LX/6Yk;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Yr;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Yf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Yi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Yw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Yn;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Yh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Yg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Yq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Yd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Ya;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Ya;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110012
    iput-object p1, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->z:LX/7Y7;

    .line 1110013
    iput-object p2, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->A:Landroid/os/Handler;

    .line 1110014
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    sget-object v2, LX/6YN;->STANDARD_DATA_CHARGES_APPLY:LX/6YN;

    invoke-virtual {v1, v2, p4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/6YN;->FETCH_UPSELL:LX/6YN;

    invoke-virtual {v1, v2, p3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/6YN;->USE_DATA_OR_STAY_IN_FREE:LX/6YN;

    invoke-virtual {v1, v2, p7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/6YN;->PROMOS_LIST:LX/6YN;

    invoke-virtual {v1, v2, p8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/6YN;->BUY_CONFIRM:LX/6YN;

    invoke-virtual {v1, v2, p5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/6YN;->BUY_SUCCESS:LX/6YN;

    invoke-virtual {v1, v2, p6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/6YN;->BUY_MAYBE:LX/6YN;

    invoke-virtual {v1, v2, p9}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/6YN;->BUY_FAILURE:LX/6YN;

    invoke-virtual {v1, v2, p10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/6YN;->SHOW_LOAN:LX/6YN;

    invoke-virtual {v1, v2, p11}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/6YN;->BORROW_LOAN_CONFIRM:LX/6YN;

    invoke-virtual {v1, v2, p12}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/6YN;->VPN_CALL_TO_HANDLE:LX/6YN;

    move-object/from16 v0, p13

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    sget-object v2, LX/6YN;->ZERO_BALANCE_SPINNER:LX/6YN;

    move-object/from16 v0, p14

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->w:LX/0P1;

    .line 1110015
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-static {v14}, LX/7Y7;->a(LX/0QB;)LX/7Y7;

    move-result-object v1

    check-cast v1, LX/7Y7;

    invoke-static {v14}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    const/16 v3, 0x2576

    invoke-static {v14, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2579

    invoke-static {v14, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2572

    invoke-static {v14, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2575

    invoke-static {v14, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x257a

    invoke-static {v14, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2577

    invoke-static {v14, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2574

    invoke-static {v14, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2573

    invoke-static {v14, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2578

    invoke-static {v14, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2571

    invoke-static {v14, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x3944

    invoke-static {v14, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v15, 0x3943

    invoke-static {v14, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {v0 .. v14}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;LX/7Y7;Landroid/os/Handler;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method public static b(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;LX/6YN;)V
    .locals 3

    .prologue
    .line 1110037
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1110038
    const-string v1, "current_screen"

    invoke-virtual {p1}, LX/6YN;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1110039
    return-void
.end method

.method public static c(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;LX/6YN;)LX/6YO;
    .locals 2

    .prologue
    .line 1110016
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->y:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6YO;

    .line 1110017
    if-nez v0, :cond_0

    .line 1110018
    new-instance v1, LX/6YO;

    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->w:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-direct {v1, p0, v0}, LX/6YO;-><init>(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;LX/0Ot;)V

    .line 1110019
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->y:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 1110020
    :cond_0
    return-object v0
.end method

.method private u()LX/6YN;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1110021
    invoke-static {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->v(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;)Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1110022
    sget-object v0, LX/6YN;->BUY_FAILURE:LX/6YN;

    .line 1110023
    :goto_0
    return-object v0

    .line 1110024
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1110025
    const-string v1, "current_screen"

    sget-object v2, LX/6YN;->FETCH_UPSELL:LX/6YN;

    invoke-virtual {v2}, LX/6YN;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, LX/6YN;->fromInt(I)LX/6YN;

    move-result-object v0

    goto :goto_0
.end method

.method public static v(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;)Lcom/facebook/iorg/common/upsell/model/PromoDataModel;
    .locals 2

    .prologue
    .line 1110026
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1110027
    const-string v1, "promo_data_model"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    return-object v0
.end method

.method private w()LX/6YO;
    .locals 1

    .prologue
    .line 1110028
    invoke-direct {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->u()LX/6YN;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->c(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;LX/6YN;)LX/6YO;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 1110029
    invoke-super {p0, p1}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    .line 1110030
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 1110031
    if-eqz p1, :cond_0

    .line 1110032
    const-string v0, "current_screen"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1110033
    invoke-static {v0}, LX/6YN;->valueOf(Ljava/lang/String;)LX/6YN;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->b(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;LX/6YN;)V

    .line 1110034
    const-string v0, "promo_data_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110035
    invoke-virtual {p0, v0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(Lcom/facebook/iorg/common/upsell/model/PromoDataModel;)V

    .line 1110036
    :cond_0
    return-object v1
.end method

.method public final a(LX/6YN;)V
    .locals 3

    .prologue
    .line 1109982
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->z:LX/7Y7;

    if-nez v0, :cond_0

    .line 1109983
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called go to screen before Fragment.onCreate was called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1109984
    :cond_0
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->z:LX/7Y7;

    .line 1109985
    iget-object v1, v0, LX/7Y7;->a:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 1109986
    iget-boolean v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->x:Z

    move v0, v0

    .line 1109987
    if-nez v0, :cond_2

    .line 1109988
    :cond_1
    :goto_0
    return-void

    .line 1109989
    :cond_2
    invoke-direct {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->u()LX/6YN;

    move-result-object v0

    .line 1109990
    if-eq v0, p1, :cond_1

    .line 1109991
    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->A:Landroid/os/Handler;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1109992
    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->A:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment$2;

    invoke-direct {v2, p0, p1, v0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment$2;-><init>(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;LX/6YN;LX/6YN;)V

    const v0, -0x4eb8b5d3    # -2.9000475E-9f

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final a(Lcom/facebook/iorg/common/upsell/model/PromoDataModel;)V
    .locals 2

    .prologue
    .line 1109993
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1109994
    const-string v1, "promo_data_model"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1109995
    return-void
.end method

.method public final m()LX/4g6;
    .locals 1

    .prologue
    .line 1109941
    sget-object v0, LX/4g6;->l:LX/4g6;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109945
    const-string v0, "upsell_dialog_open"

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109946
    const-string v0, "upsell_dialog_confirm"

    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 1109947
    invoke-virtual {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->k()V

    .line 1109948
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x36ed1611

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1109949
    invoke-super {p0, p1}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1109950
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1109951
    const/4 v1, 0x1

    const v2, 0x7f0e03f3

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1109952
    const/16 v1, 0x2b

    const v2, -0x492e69c7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x7d22201

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1109953
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->t:Landroid/widget/LinearLayout;

    .line 1109954
    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->t:Landroid/widget/LinearLayout;

    new-instance v2, LX/6YM;

    invoke-direct {v2, p0}, LX/6YM;-><init>(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1109955
    invoke-direct {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->w()LX/6YO;

    move-result-object v1

    .line 1109956
    invoke-virtual {v1}, LX/6YO;->a()Landroid/view/View;

    move-result-object v1

    .line 1109957
    if-eqz v1, :cond_0

    .line 1109958
    iget-object v2, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1109959
    :cond_0
    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->t:Landroid/widget/LinearLayout;

    const/16 v2, 0x2b

    const v3, 0xe51d980

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x7962b389

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1109960
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->y:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6YO;

    .line 1109961
    iget-object v3, v0, LX/6YO;->c:LX/6Ya;

    if-eqz v3, :cond_0

    .line 1109962
    iget-object v3, v0, LX/6YO;->c:LX/6Ya;

    .line 1109963
    const/4 v4, 0x0

    iput-object v4, v3, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1109964
    :cond_0
    const/4 v3, 0x0

    iput-object v3, v0, LX/6YO;->c:LX/6Ya;

    .line 1109965
    goto :goto_0

    .line 1109966
    :cond_1
    invoke-super {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->onDestroy()V

    .line 1109967
    const v0, 0x31fee8a6

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x568ad4b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1109968
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->x:Z

    .line 1109969
    invoke-direct {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->w()LX/6YO;

    move-result-object v1

    .line 1109970
    iget-object v2, v1, LX/6YO;->c:LX/6Ya;

    if-eqz v2, :cond_0

    .line 1109971
    iget-object v2, v1, LX/6YO;->c:LX/6Ya;

    invoke-virtual {v2}, LX/6Ya;->a()V

    .line 1109972
    :cond_0
    const/4 v2, 0x0

    iput-object v2, v1, LX/6YO;->d:Landroid/view/View;

    .line 1109973
    invoke-super {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->onDestroyView()V

    .line 1109974
    const/16 v1, 0x2b

    const v2, 0x1925da76

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1109975
    invoke-super {p0, p1}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1109976
    const-string v0, "current_screen"

    invoke-direct {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->u()LX/6YN;

    move-result-object v1

    invoke-virtual {v1}, LX/6YN;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109977
    const-string v0, "promo_data_model"

    invoke-static {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->v(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;)Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1109978
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1109942
    invoke-super {p0, p1, p2}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1109943
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->x:Z

    .line 1109944
    return-void
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109979
    const-string v0, "upsell_dialog_cancel"

    return-object v0
.end method

.method public final q()I
    .locals 2

    .prologue
    .line 1109980
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1109981
    const-string v1, "title_extra_image_resource_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
