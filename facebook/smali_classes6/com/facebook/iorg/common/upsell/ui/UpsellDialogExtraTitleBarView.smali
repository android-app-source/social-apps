.class public Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/picassolike/fresco/FrescoImpl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1109866
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1109867
    invoke-direct {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->a()V

    .line 1109868
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1109869
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1109870
    invoke-direct {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->a()V

    .line 1109871
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1109872
    const-class v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;

    invoke-static {v0, p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1109873
    invoke-virtual {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1109874
    const v1, 0x7f031556

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1109875
    const v0, 0x7f0d300a

    invoke-virtual {p0, v0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/picassolike/PicassoLikeViewStub;

    .line 1109876
    invoke-virtual {v0}, Lcom/facebook/picassolike/PicassoLikeViewStub;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->b:Landroid/view/View;

    .line 1109877
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;

    invoke-static {v0}, Lcom/facebook/picassolike/fresco/FrescoImpl;->a(LX/0QB;)Lcom/facebook/picassolike/fresco/FrescoImpl;

    move-result-object v0

    check-cast v0, Lcom/facebook/picassolike/fresco/FrescoImpl;

    iput-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->a:Lcom/facebook/picassolike/fresco/FrescoImpl;

    return-void
.end method


# virtual methods
.method public setTitleImageByUrl(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1109878
    iget-object v0, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1109879
    invoke-virtual {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b06ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1109880
    invoke-virtual {p0}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->getContext()Landroid/content/Context;

    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->b:Landroid/view/View;

    .line 1109881
    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1109882
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object p0, Lcom/facebook/picassolike/fresco/FrescoImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1109883
    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v2

    check-cast v2, LX/1af;

    sget-object p0, LX/1Up;->b:LX/1Up;

    invoke-virtual {v2, p0}, LX/1af;->a(LX/1Up;)V

    .line 1109884
    return-void
.end method

.method public setTitleImageResource(I)V
    .locals 3

    .prologue
    .line 1109885
    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->b:Landroid/view/View;

    .line 1109886
    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1109887
    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v2

    check-cast v2, LX/1af;

    invoke-virtual {v2, p1}, LX/1af;->b(I)V

    .line 1109888
    const/4 v2, 0x0

    sget-object p0, Lcom/facebook/picassolike/fresco/FrescoImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1109889
    return-void
.end method
