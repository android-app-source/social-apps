.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6085c1be
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$PinnedCommentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1091992
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1091991
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1091989
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1091990
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1091987
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->e:Ljava/lang/String;

    .line 1091988
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$PinnedCommentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1091985
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$PinnedCommentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$PinnedCommentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$PinnedCommentModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$PinnedCommentModel;

    .line 1091986
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$PinnedCommentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1091959
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1091960
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1091961
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$PinnedCommentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1091962
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1091963
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1091964
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1091965
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->g:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1091966
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1091967
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1091977
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1091978
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$PinnedCommentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1091979
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$PinnedCommentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$PinnedCommentModel;

    .line 1091980
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$PinnedCommentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1091981
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;

    .line 1091982
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel$PinnedCommentModel;

    .line 1091983
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1091984
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1091976
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1091973
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1091974
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;->g:I

    .line 1091975
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1091970
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DeletePinnedCommentEventMutationModel$PinEventModel;-><init>()V

    .line 1091971
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1091972
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1091969
    const v0, -0x2c1ab511

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1091968
    const v0, -0x725a60c

    return v0
.end method
