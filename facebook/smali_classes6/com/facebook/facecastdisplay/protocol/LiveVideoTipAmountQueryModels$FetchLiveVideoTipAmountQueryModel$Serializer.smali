.class public final Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1099703
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;

    new-instance v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1099704
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1099705
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 1099706
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1099707
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    .line 1099708
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1099709
    invoke-virtual {v1, v0, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1099710
    if-eqz v2, :cond_0

    .line 1099711
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1099712
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1099713
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1099714
    cmpl-double v4, v2, v4

    if-eqz v4, :cond_1

    .line 1099715
    const-string v4, "total_tip_amount"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1099716
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 1099717
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 1099718
    if-eqz v2, :cond_2

    .line 1099719
    const-string v3, "unique_tip_giver_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1099720
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1099721
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1099722
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1099723
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel$Serializer;->a(Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
