.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1095901
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel;

    new-instance v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1095902
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1095886
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1095888
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1095889
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1095890
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1095891
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1095892
    if-eqz v2, :cond_0

    .line 1095893
    const-string p0, "feedback"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1095894
    invoke-static {v1, v2, p1, p2}, LX/6TJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1095895
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1095896
    if-eqz v2, :cond_1

    .line 1095897
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1095898
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1095899
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1095900
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1095887
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$Serializer;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
