.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3d71e146
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1096019
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1095984
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1096017
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1096018
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1096008
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1096009
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1096010
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1096011
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1096012
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1096013
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1096014
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->g:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1096015
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1096016
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1096000
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1096001
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1096002
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    .line 1096003
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1096004
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;

    .line 1096005
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    .line 1096006
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1096007
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1095999
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1095996
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1095997
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->g:I

    .line 1095998
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1095993
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;-><init>()V

    .line 1095994
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1095995
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1095992
    const v0, 0x5c11a093

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1095991
    const v0, -0x725a60c

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1095989
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->e:Ljava/lang/String;

    .line 1095990
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPinnedComment"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1095987
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    .line 1095988
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    return-object v0
.end method

.method public final l()I
    .locals 2

    .prologue
    .line 1095985
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1095986
    iget v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->g:I

    return v0
.end method
