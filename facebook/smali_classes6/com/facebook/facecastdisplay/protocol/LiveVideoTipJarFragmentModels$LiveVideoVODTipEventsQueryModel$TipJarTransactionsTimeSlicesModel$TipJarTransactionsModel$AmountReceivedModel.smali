.class public final Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5694a845
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1100753
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1100726
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1100751
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1100752
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100749
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->g:Ljava/lang/String;

    .line 1100750
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1100739
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1100740
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1100741
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1100742
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1100743
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1100744
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1100745
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1100746
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1100747
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1100748
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1100736
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1100737
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1100738
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100734
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->e:Ljava/lang/String;

    .line 1100735
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1100731
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;-><init>()V

    .line 1100732
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1100733
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1100730
    const v0, -0x7110f639

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1100729
    const v0, -0x6db81817

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100727
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->f:Ljava/lang/String;

    .line 1100728
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->f:Ljava/lang/String;

    return-object v0
.end method
