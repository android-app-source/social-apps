.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2449eafa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1090728
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1090727
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1090725
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1090726
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1090723
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;->e:Ljava/lang/String;

    .line 1090724
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1090716
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1090717
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1090718
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1090719
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1090720
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1090721
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1090722
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1090702
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1090703
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1090704
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1090715
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1090712
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1090713
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;->f:I

    .line 1090714
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1090709
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;-><init>()V

    .line 1090710
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1090711
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1090708
    const v0, 0x15506e9e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1090707
    const v0, -0x629d3544

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1090705
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1090706
    iget v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;->f:I

    return v0
.end method
