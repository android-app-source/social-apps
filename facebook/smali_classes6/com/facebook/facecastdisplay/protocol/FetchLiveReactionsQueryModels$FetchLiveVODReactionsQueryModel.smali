.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x399bb539
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1090645
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1090628
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1090648
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1090649
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1090646
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->f:Ljava/lang/String;

    .line 1090647
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1090629
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1090630
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1090631
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1090632
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1090633
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1090634
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1090635
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1090636
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1090637
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1090638
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1090639
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;

    .line 1090640
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1090641
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;

    .line 1090642
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;

    .line 1090643
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1090644
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1090624
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1090621
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;-><init>()V

    .line 1090622
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1090623
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1090620
    const v0, 0x22f5a77

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1090625
    const v0, 0x4ed245b

    return v0
.end method

.method public final j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1090626
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;

    .line 1090627
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;

    return-object v0
.end method
