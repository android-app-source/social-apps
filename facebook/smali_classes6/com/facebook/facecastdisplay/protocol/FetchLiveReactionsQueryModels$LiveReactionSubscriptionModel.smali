.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x38f2afe8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1090851
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1090854
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1090852
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1090853
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1090823
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1090824
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1090825
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1090826
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1090827
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1090828
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1090829
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1090830
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1090831
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1090832
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1090833
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1090834
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1090835
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;

    .line 1090836
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1090837
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;

    .line 1090838
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;

    .line 1090839
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1090840
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;

    .line 1090841
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1090842
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;

    .line 1090843
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;

    .line 1090844
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1090845
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;

    .line 1090846
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1090847
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;

    .line 1090848
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->g:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;

    .line 1090849
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1090850
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1090821
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;

    .line 1090822
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1090818
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;-><init>()V

    .line 1090819
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1090820
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1090817
    const v0, 0x57809953

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1090816
    const v0, -0x4441b742

    return v0
.end method

.method public final j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1090814
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;

    .line 1090815
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactionInfoModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1090812
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->g:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->g:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;

    .line 1090813
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;->g:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;

    return-object v0
.end method
