.class public final Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3b0f5926
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099859
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099858
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1099856
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1099857
    return-void
.end method

.method private a()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099854
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;

    .line 1099855
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1099860
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099861
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel;->a()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1099862
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1099863
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1099864
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099865
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1099846
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099847
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel;->a()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1099848
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel;->a()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;

    .line 1099849
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel;->a()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1099850
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel;

    .line 1099851
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;

    .line 1099852
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099853
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1099843
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel;-><init>()V

    .line 1099844
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1099845
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1099842
    const v0, -0x47a1da28

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1099841
    const v0, 0x60fe2903

    return v0
.end method
