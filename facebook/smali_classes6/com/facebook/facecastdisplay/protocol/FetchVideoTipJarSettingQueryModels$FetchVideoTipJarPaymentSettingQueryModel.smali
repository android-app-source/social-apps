.class public final Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4d7bc9f0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1098736
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1098737
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1098705
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1098706
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1098734
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->e:Ljava/lang/String;

    .line 1098735
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1098726
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1098727
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1098728
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1098729
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1098730
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1098731
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1098732
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1098733
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1098718
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1098719
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1098720
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    .line 1098721
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1098722
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;

    .line 1098723
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    .line 1098724
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1098725
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1098717
    new-instance v0, LX/6Th;

    invoke-direct {v0, p1}, LX/6Th;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1098738
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1098715
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1098716
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1098714
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1098711
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;-><init>()V

    .line 1098712
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1098713
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1098710
    const v0, 0x12753cdb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1098709
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1098707
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    .line 1098708
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    return-object v0
.end method
