.class public final Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x63450c9c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:D

.field private i:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1100419
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1100420
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1100421
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1100422
    return-void
.end method

.method private j()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100427
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;

    .line 1100428
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100423
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->f:Ljava/lang/String;

    .line 1100424
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100425
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->g:Ljava/lang/String;

    .line 1100426
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100404
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->i:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->i:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    .line 1100405
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->i:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1100406
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1100407
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->j()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1100408
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1100409
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1100410
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->m()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1100411
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1100412
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1100413
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1100414
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1100415
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->h:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1100416
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1100417
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1100418
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1100391
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1100392
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->j()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1100393
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->j()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;

    .line 1100394
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->j()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1100395
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    .line 1100396
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;

    .line 1100397
    :cond_0
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->m()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1100398
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->m()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    .line 1100399
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->m()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1100400
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    .line 1100401
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->i:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    .line 1100402
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1100403
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100390
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1100387
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1100388
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->h:D

    .line 1100389
    return-void
.end method

.method public final synthetic b()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100386
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->j()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1100383
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;-><init>()V

    .line 1100384
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1100385
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100382
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->m()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1100381
    const v0, 0x711b1274

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1100380
    const v0, 0x4677331e

    return v0
.end method
