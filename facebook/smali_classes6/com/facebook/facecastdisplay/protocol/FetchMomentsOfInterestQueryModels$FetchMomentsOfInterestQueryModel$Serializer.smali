.class public final Lcom/facebook/facecastdisplay/protocol/FetchMomentsOfInterestQueryModels$FetchMomentsOfInterestQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchMomentsOfInterestQueryModels$FetchMomentsOfInterestQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1097964
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchMomentsOfInterestQueryModels$FetchMomentsOfInterestQueryModel;

    new-instance v1, Lcom/facebook/facecastdisplay/protocol/FetchMomentsOfInterestQueryModels$FetchMomentsOfInterestQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/protocol/FetchMomentsOfInterestQueryModels$FetchMomentsOfInterestQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1097965
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1097966
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/protocol/FetchMomentsOfInterestQueryModels$FetchMomentsOfInterestQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1097967
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1097968
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x1

    .line 1097969
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1097970
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1097971
    if-eqz v2, :cond_0

    .line 1097972
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097973
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1097974
    :cond_0
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result v2

    .line 1097975
    if-eqz v2, :cond_1

    .line 1097976
    const-string v2, "moments_of_interest"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097977
    invoke-virtual {v1, v0, p2}, LX/15i;->e(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->c(Ljava/util/Iterator;LX/0nX;)V

    .line 1097978
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1097979
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1097980
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchMomentsOfInterestQueryModels$FetchMomentsOfInterestQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/protocol/FetchMomentsOfInterestQueryModels$FetchMomentsOfInterestQueryModel$Serializer;->a(Lcom/facebook/facecastdisplay/protocol/FetchMomentsOfInterestQueryModels$FetchMomentsOfInterestQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
