.class public final Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1006887e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:D

.field private g:I

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099827
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099826
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1099824
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1099825
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099822
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;->e:Ljava/lang/String;

    .line 1099823
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099820
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;->h:Ljava/lang/String;

    .line 1099821
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1099810
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099811
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1099812
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1099813
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1099814
    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1099815
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;->f:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1099816
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;->g:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 1099817
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1099818
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099819
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1099807
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099808
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099809
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099806
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1099802
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1099803
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;->f:D

    .line 1099804
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;->g:I

    .line 1099805
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1099797
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$LiveVideoGiveTipMutationModel$ReceivedTipsModel;-><init>()V

    .line 1099798
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1099799
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1099801
    const v0, -0x23c10963

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1099800
    const v0, -0x23fad327

    return v0
.end method
