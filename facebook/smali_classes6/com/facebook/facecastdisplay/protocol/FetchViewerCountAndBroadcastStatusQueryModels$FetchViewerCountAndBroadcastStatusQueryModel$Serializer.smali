.class public final Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1099307
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;

    new-instance v1, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1099308
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1099309
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1099310
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1099311
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 1099312
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1099313
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result v2

    .line 1099314
    if-eqz v2, :cond_0

    .line 1099315
    const-string v2, "broadcast_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1099316
    invoke-virtual {v1, v0, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1099317
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1099318
    if-eqz v2, :cond_1

    .line 1099319
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1099320
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1099321
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, p2}, LX/15i;->a(III)I

    move-result v2

    .line 1099322
    if-eqz v2, :cond_2

    .line 1099323
    const-string p0, "live_viewer_count"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1099324
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1099325
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, p2}, LX/15i;->a(III)I

    move-result v2

    .line 1099326
    if-eqz v2, :cond_3

    .line 1099327
    const-string p0, "live_viewer_count_read_only"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1099328
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1099329
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1099330
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1099331
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel$Serializer;->a(Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
