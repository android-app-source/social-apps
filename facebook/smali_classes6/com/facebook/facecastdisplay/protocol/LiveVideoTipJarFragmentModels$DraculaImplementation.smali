.class public final Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1099989
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1099990
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1099991
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1099992
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v11, 0x3

    const/4 v1, 0x2

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 1099993
    if-nez p1, :cond_0

    move v0, v6

    .line 1099994
    :goto_0
    return v0

    .line 1099995
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1099996
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1099997
    :sswitch_0
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v0

    .line 1099998
    const v2, 0x21cb8f9d

    const/4 v4, 0x0

    .line 1099999
    if-nez v0, :cond_1

    move v3, v4

    .line 1100000
    :goto_1
    move v0, v3

    .line 1100001
    invoke-virtual {p0, p1, v10}, LX/15i;->p(II)I

    move-result v2

    .line 1100002
    const v3, -0x68920b47

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1100003
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1100004
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1100005
    invoke-virtual {p3, v10, v2}, LX/186;->b(II)V

    .line 1100006
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1100007
    :sswitch_1
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v0

    .line 1100008
    const v1, 0x5a800726

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1100009
    invoke-virtual {p3, v10}, LX/186;->c(I)V

    .line 1100010
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1100011
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1100012
    :sswitch_2
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$AmountReceivedModel;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$AmountReceivedModel;

    .line 1100013
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1100014
    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1100015
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1100016
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1100017
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$TipGiverModel;

    invoke-virtual {p0, p1, v11, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$TipGiverModel;

    .line 1100018
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1100019
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 1100020
    invoke-virtual {p3, v6, v7}, LX/186;->b(II)V

    .line 1100021
    invoke-virtual {p3, v10, v8}, LX/186;->b(II)V

    move-object v0, p3

    .line 1100022
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1100023
    invoke-virtual {p3, v11, v9}, LX/186;->b(II)V

    .line 1100024
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1100025
    :sswitch_3
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1100026
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1100027
    invoke-virtual {p3, v10}, LX/186;->c(I)V

    .line 1100028
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1100029
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1100030
    :sswitch_4
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1100031
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1100032
    invoke-virtual {p0, p1, v10}, LX/15i;->b(II)Z

    move-result v2

    .line 1100033
    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v3

    .line 1100034
    invoke-virtual {p0, p1, v11}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1100035
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1100036
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1100037
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1100038
    invoke-virtual {p3, v10, v2}, LX/186;->a(IZ)V

    .line 1100039
    invoke-virtual {p3, v1, v3}, LX/186;->a(IZ)V

    .line 1100040
    invoke-virtual {p3, v11, v4}, LX/186;->b(II)V

    .line 1100041
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1100042
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v5

    .line 1100043
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 1100044
    :goto_2
    if-ge v4, v5, :cond_3

    .line 1100045
    invoke-virtual {p0, v0, v4}, LX/15i;->q(II)I

    move-result v7

    .line 1100046
    invoke-static {p0, v7, v2, p3}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v7

    aput v7, v3, v4

    .line 1100047
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1100048
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 1100049
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x68920b47 -> :sswitch_4
        -0x47f62b9f -> :sswitch_0
        0x21cb8f9d -> :sswitch_1
        0x5a800726 -> :sswitch_2
        0x6031563b -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1100050
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1100077
    if-eqz p0, :cond_0

    .line 1100078
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1100079
    if-eq v0, p0, :cond_0

    .line 1100080
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1100081
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1100051
    sparse-switch p2, :sswitch_data_0

    .line 1100052
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1100053
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1100054
    const v1, 0x21cb8f9d

    .line 1100055
    if-eqz v0, :cond_0

    .line 1100056
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result v3

    .line 1100057
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1100058
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1100059
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1100060
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1100061
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1100062
    const v1, -0x68920b47

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1100063
    :goto_1
    :sswitch_1
    return-void

    .line 1100064
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1100065
    const v1, 0x5a800726

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    .line 1100066
    :sswitch_3
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$AmountReceivedModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$AmountReceivedModel;

    .line 1100067
    invoke-static {v0, p3}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1100068
    const/4 v0, 0x3

    const-class v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$TipGiverModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$TipGiverModel;

    .line 1100069
    invoke-static {v0, p3}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x68920b47 -> :sswitch_1
        -0x47f62b9f -> :sswitch_0
        0x21cb8f9d -> :sswitch_2
        0x5a800726 -> :sswitch_3
        0x6031563b -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1100070
    if-eqz p1, :cond_0

    .line 1100071
    invoke-static {p0, p1, p2}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;

    move-result-object v1

    .line 1100072
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;

    .line 1100073
    if-eq v0, v1, :cond_0

    .line 1100074
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1100075
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1100076
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1099987
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1099988
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1099982
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1099983
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1099984
    :cond_0
    iput-object p1, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->a:LX/15i;

    .line 1099985
    iput p2, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->b:I

    .line 1099986
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1099956
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1099981
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1099978
    iget v0, p0, LX/1vt;->c:I

    .line 1099979
    move v0, v0

    .line 1099980
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1099975
    iget v0, p0, LX/1vt;->c:I

    .line 1099976
    move v0, v0

    .line 1099977
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1099972
    iget v0, p0, LX/1vt;->b:I

    .line 1099973
    move v0, v0

    .line 1099974
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099969
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1099970
    move-object v0, v0

    .line 1099971
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1099960
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1099961
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1099962
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1099963
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1099964
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1099965
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1099966
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1099967
    invoke-static {v3, v9, v2}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1099968
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1099957
    iget v0, p0, LX/1vt;->c:I

    .line 1099958
    move v0, v0

    .line 1099959
    return v0
.end method
