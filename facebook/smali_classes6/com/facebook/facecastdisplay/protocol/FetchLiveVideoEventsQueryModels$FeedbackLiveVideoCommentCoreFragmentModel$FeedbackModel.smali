.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x273eb04c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1092232
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1092233
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1092234
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1092235
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1092236
    iput-boolean p1, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->e:Z

    .line 1092237
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1092238
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1092239
    if-eqz v0, :cond_0

    .line 1092240
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1092241
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1092242
    iput-boolean p1, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->f:Z

    .line 1092243
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1092244
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1092245
    if-eqz v0, :cond_0

    .line 1092246
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1092247
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1092248
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1092249
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1092250
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1092251
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1092252
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1092253
    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->f:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1092254
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1092255
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1092256
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1092257
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1092258
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1092259
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1092260
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1092261
    new-instance v0, LX/6SP;

    invoke-direct {v0, p1}, LX/6SP;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1092231
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1092262
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1092263
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->e:Z

    .line 1092264
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->f:Z

    .line 1092265
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1092221
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1092222
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1092223
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1092224
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1092225
    :goto_0
    return-void

    .line 1092226
    :cond_0
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1092227
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1092228
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1092229
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1092230
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1092216
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1092217
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->a(Z)V

    .line 1092218
    :cond_0
    :goto_0
    return-void

    .line 1092219
    :cond_1
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1092220
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->b(Z)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1092203
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;-><init>()V

    .line 1092204
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1092205
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1092215
    const v0, 0x50a01d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1092214
    const v0, -0x78fb05b

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1092212
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1092213
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->e:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1092210
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1092211
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->f:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1092208
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->g:Ljava/lang/String;

    .line 1092209
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1092206
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->h:Ljava/lang/String;

    .line 1092207
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->h:Ljava/lang/String;

    return-object v0
.end method
