.class public final Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1098399
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;

    new-instance v1, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1098400
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1098401
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1098402
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1098403
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1098404
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_b

    .line 1098405
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1098406
    :goto_0
    move v1, v2

    .line 1098407
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1098408
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1098409
    new-instance v1, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;-><init>()V

    .line 1098410
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1098411
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1098412
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1098413
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1098414
    :cond_0
    return-object v1

    .line 1098415
    :cond_1
    const-string p0, "is_live_streaming"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1098416
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v5

    move v8, v5

    move v5, v3

    .line 1098417
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_7

    .line 1098418
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1098419
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1098420
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 1098421
    const-string p0, "broadcast_status"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1098422
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto :goto_1

    .line 1098423
    :cond_3
    const-string p0, "id"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1098424
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1098425
    :cond_4
    const-string p0, "live_viewer_count"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1098426
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v4

    move v7, v4

    move v4, v3

    goto :goto_1

    .line 1098427
    :cond_5
    const-string p0, "live_viewer_count_read_only"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1098428
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v3

    goto :goto_1

    .line 1098429
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1098430
    :cond_7
    const/4 v11, 0x5

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1098431
    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1098432
    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1098433
    if-eqz v5, :cond_8

    .line 1098434
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v8}, LX/186;->a(IZ)V

    .line 1098435
    :cond_8
    if-eqz v4, :cond_9

    .line 1098436
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v7, v2}, LX/186;->a(III)V

    .line 1098437
    :cond_9
    if-eqz v1, :cond_a

    .line 1098438
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6, v2}, LX/186;->a(III)V

    .line 1098439
    :cond_a
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    goto/16 :goto_1
.end method
