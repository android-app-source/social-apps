.class public final Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastLobbyFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastLobbyFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1098347
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastLobbyFragmentModel;

    new-instance v1, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastLobbyFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastLobbyFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1098348
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1098349
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastLobbyFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1098350
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1098351
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 1098352
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1098353
    invoke-virtual {v1, v0, p2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1098354
    if-eqz v2, :cond_0

    .line 1098355
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098356
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1098357
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, p2}, LX/15i;->a(III)I

    move-result v2

    .line 1098358
    if-eqz v2, :cond_1

    .line 1098359
    const-string p0, "live_lobby_viewer_count"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098360
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1098361
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, p2}, LX/15i;->a(III)I

    move-result v2

    .line 1098362
    if-eqz v2, :cond_2

    .line 1098363
    const-string p0, "live_lobby_viewer_count_read_only"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098364
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1098365
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1098366
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1098367
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastLobbyFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastLobbyFragmentModel$Serializer;->a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastLobbyFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
