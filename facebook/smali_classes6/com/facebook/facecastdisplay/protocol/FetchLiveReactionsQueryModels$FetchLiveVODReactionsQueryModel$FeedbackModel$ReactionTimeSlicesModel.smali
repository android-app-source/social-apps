.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5b88e00a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1090558
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1090557
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1090555
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1090556
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 1090547
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1090548
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 1090549
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1090550
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->e:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1090551
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1090552
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->g:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1090553
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1090554
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1090528
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1090529
    iget-wide v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->e:J

    return-wide v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1090539
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1090540
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1090541
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1090542
    if-eqz v1, :cond_0

    .line 1090543
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;

    .line 1090544
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->f:Ljava/util/List;

    .line 1090545
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1090546
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1090559
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1090560
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->e:J

    .line 1090561
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->g:J

    .line 1090562
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1090536
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;-><init>()V

    .line 1090537
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1090538
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1090535
    const v0, -0x221da99d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1090534
    const v0, 0x74c6967c    # 1.2586999E32f

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1090532
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->f:Ljava/util/List;

    .line 1090533
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 1090530
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1090531
    iget-wide v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->g:J

    return-wide v0
.end method
