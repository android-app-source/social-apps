.class public Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1099551
    new-instance v0, LX/6Tw;

    invoke-direct {v0}, LX/6Tw;-><init>()V

    sput-object v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1099552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1099553
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;->a:Ljava/lang/String;

    .line 1099554
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;->b:LX/0Px;

    .line 1099555
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1099556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1099557
    iput-object p1, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;->a:Ljava/lang/String;

    .line 1099558
    iput-object p2, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;->b:LX/0Px;

    .line 1099559
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1099560
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1099561
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1099562
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1099563
    return-void
.end method
