.class public final Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x73f5a105
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$TipJarPaymentSettingOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1098968
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1098969
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1098972
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1098973
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1098970
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->f:Ljava/lang/String;

    .line 1098971
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$TipJarPaymentSettingOwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1098957
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->g:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$TipJarPaymentSettingOwnerModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$TipJarPaymentSettingOwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$TipJarPaymentSettingOwnerModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->g:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$TipJarPaymentSettingOwnerModel;

    .line 1098958
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->g:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$TipJarPaymentSettingOwnerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1098959
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1098960
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1098961
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->l()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$TipJarPaymentSettingOwnerModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1098962
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1098963
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1098964
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1098965
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1098966
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1098967
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1098949
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1098950
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->l()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$TipJarPaymentSettingOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1098951
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->l()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$TipJarPaymentSettingOwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$TipJarPaymentSettingOwnerModel;

    .line 1098952
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->l()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$TipJarPaymentSettingOwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1098953
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    .line 1098954
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->g:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel$TipJarPaymentSettingOwnerModel;

    .line 1098955
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1098956
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1098948
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1098945
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1098946
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->e:Z

    .line 1098947
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1098942
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;-><init>()V

    .line 1098943
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1098944
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1098941
    const v0, 0x2cc0e5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1098940
    const v0, 0x2830f92a

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1098938
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1098939
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->e:Z

    return v0
.end method
