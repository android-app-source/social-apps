.class public final Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28bdb0e0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099121
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099120
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1099118
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1099119
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099116
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;->e:Ljava/lang/String;

    .line 1099117
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1099109
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099110
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1099111
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1099112
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1099113
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1099114
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099115
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1099106
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099107
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099108
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099105
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1099097
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1099098
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;->f:Z

    .line 1099099
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1099102
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;-><init>()V

    .line 1099103
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1099104
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1099101
    const v0, 0x546eb6ac

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1099100
    const v0, 0x14539615

    return v0
.end method
