.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1090603
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;

    new-instance v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1090604
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1090605
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1090606
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1090607
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1090608
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1090609
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1090610
    if-eqz v2, :cond_0

    .line 1090611
    const-string p0, "feedback"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1090612
    invoke-static {v1, v2, p1, p2}, LX/6Ry;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1090613
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1090614
    if-eqz v2, :cond_1

    .line 1090615
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1090616
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1090617
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1090618
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1090619
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$Serializer;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
