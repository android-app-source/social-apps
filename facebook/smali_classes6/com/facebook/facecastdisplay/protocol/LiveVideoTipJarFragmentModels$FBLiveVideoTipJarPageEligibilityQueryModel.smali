.class public final Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x538e22df
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1100164
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1100172
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1100170
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1100171
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100167
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1100168
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1100169
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100165
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;->f:Ljava/lang/String;

    .line 1100166
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1100155
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1100156
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1100157
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1100158
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1100159
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1100160
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1100161
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1100162
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1100163
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1100152
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1100153
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1100154
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1100173
    new-instance v0, LX/6U9;

    invoke-direct {v0, p1}, LX/6U9;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100151
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1100148
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1100149
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;->g:Z

    .line 1100150
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1100146
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1100147
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1100138
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1100143
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;-><init>()V

    .line 1100144
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1100145
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1100142
    const v0, 0x37bb2e28

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1100141
    const v0, 0x252222

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 1100139
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1100140
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;->g:Z

    return v0
.end method
