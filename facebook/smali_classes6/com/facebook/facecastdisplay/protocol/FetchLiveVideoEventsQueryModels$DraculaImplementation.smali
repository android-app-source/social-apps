.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1092161
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1092162
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1092159
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1092160
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 11

    .prologue
    .line 1092081
    if-nez p1, :cond_0

    .line 1092082
    const/4 v0, 0x0

    .line 1092083
    :goto_0
    return v0

    .line 1092084
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1092085
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1092086
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1092087
    const v1, -0x39e94aaf

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1092088
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v1

    .line 1092089
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1092090
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1092091
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1092092
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1092093
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1092094
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1092095
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1092096
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1092097
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1092098
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1092099
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1092100
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1092101
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1092102
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1092103
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1092104
    :sswitch_3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1092105
    const v1, -0x38cd6

    const/4 v3, 0x0

    .line 1092106
    if-nez v0, :cond_1

    move v2, v3

    .line 1092107
    :goto_1
    move v0, v2

    .line 1092108
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1092109
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1092110
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1092111
    :sswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1092112
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1092113
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1092114
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1092115
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1092116
    :sswitch_5
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1092117
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1092118
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1092119
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1092120
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1092121
    :sswitch_6
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1092122
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1092123
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1092124
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1092125
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1092126
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1092127
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 1092128
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1092129
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1092130
    const/4 v5, 0x5

    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1092131
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1092132
    const/4 v6, 0x6

    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1092133
    invoke-virtual {p3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1092134
    const/4 v7, 0x7

    const/4 v8, 0x0

    invoke-virtual {p0, p1, v7, v8}, LX/15i;->a(III)I

    move-result v7

    .line 1092135
    const/16 v8, 0x8

    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 1092136
    invoke-virtual {p3, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1092137
    const/16 v9, 0x9

    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v9

    .line 1092138
    invoke-virtual {p3, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1092139
    const/16 v10, 0xa

    invoke-virtual {p3, v10}, LX/186;->c(I)V

    .line 1092140
    const/4 v10, 0x0

    invoke-virtual {p3, v10, v0}, LX/186;->b(II)V

    .line 1092141
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1092142
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1092143
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->a(IZ)V

    .line 1092144
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1092145
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v5}, LX/186;->b(II)V

    .line 1092146
    const/4 v0, 0x6

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 1092147
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v7, v1}, LX/186;->a(III)V

    .line 1092148
    const/16 v0, 0x8

    invoke-virtual {p3, v0, v8}, LX/186;->b(II)V

    .line 1092149
    const/16 v0, 0x9

    invoke-virtual {p3, v0, v9}, LX/186;->b(II)V

    .line 1092150
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1092151
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    .line 1092152
    if-nez v4, :cond_2

    const/4 v2, 0x0

    .line 1092153
    :goto_2
    if-ge v3, v4, :cond_3

    .line 1092154
    invoke-virtual {p0, v0, v3}, LX/15i;->q(II)I

    move-result v5

    .line 1092155
    invoke-static {p0, v5, v1, p3}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v5

    aput v5, v2, v3

    .line 1092156
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1092157
    :cond_2
    new-array v2, v4, [I

    goto :goto_2

    .line 1092158
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p3, v2, v3}, LX/186;->a([IZ)I

    move-result v2

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x39e94aaf -> :sswitch_1
        -0x38cd6 -> :sswitch_6
        0x1563b951 -> :sswitch_4
        0x5c061cb6 -> :sswitch_5
        0x75cee060 -> :sswitch_3
        0x7803c6ec -> :sswitch_2
        0x7935fc1e -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1092080
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1092066
    sparse-switch p2, :sswitch_data_0

    .line 1092067
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1092068
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1092069
    const v1, -0x39e94aaf

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1092070
    :goto_0
    :sswitch_1
    return-void

    .line 1092071
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1092072
    const v1, -0x38cd6

    .line 1092073
    if-eqz v0, :cond_0

    .line 1092074
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1092075
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 1092076
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1092077
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1092078
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1092079
    :cond_0
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x39e94aaf -> :sswitch_1
        -0x38cd6 -> :sswitch_1
        0x1563b951 -> :sswitch_1
        0x5c061cb6 -> :sswitch_1
        0x75cee060 -> :sswitch_2
        0x7803c6ec -> :sswitch_1
        0x7935fc1e -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1092060
    if-eqz p1, :cond_0

    .line 1092061
    invoke-static {p0, p1, p2}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1092062
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;

    .line 1092063
    if-eq v0, v1, :cond_0

    .line 1092064
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1092065
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1092059
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1092057
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1092058
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1092163
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1092164
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1092165
    :cond_0
    iput-object p1, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1092166
    iput p2, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;->b:I

    .line 1092167
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1092056
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1092055
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1092052
    iget v0, p0, LX/1vt;->c:I

    .line 1092053
    move v0, v0

    .line 1092054
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1092031
    iget v0, p0, LX/1vt;->c:I

    .line 1092032
    move v0, v0

    .line 1092033
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1092049
    iget v0, p0, LX/1vt;->b:I

    .line 1092050
    move v0, v0

    .line 1092051
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1092046
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1092047
    move-object v0, v0

    .line 1092048
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1092037
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1092038
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1092039
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1092040
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1092041
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1092042
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1092043
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1092044
    invoke-static {v3, v9, v2}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1092045
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1092034
    iget v0, p0, LX/1vt;->c:I

    .line 1092035
    move v0, v0

    .line 1092036
    return v0
.end method
