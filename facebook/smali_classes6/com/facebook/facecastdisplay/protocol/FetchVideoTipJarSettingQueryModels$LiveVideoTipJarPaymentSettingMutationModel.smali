.class public final Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3ee75eb7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099031
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099030
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1099028
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1099029
    return-void
.end method

.method private a()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099026
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    .line 1099027
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1099032
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099033
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1099034
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1099035
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1099036
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099037
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1099018
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099019
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1099020
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    .line 1099021
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1099022
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel;

    .line 1099023
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    .line 1099024
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099025
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1099015
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingMutationModel;-><init>()V

    .line 1099016
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1099017
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1099014
    const v0, -0x1c182c83

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1099013
    const v0, 0x92f1401

    return v0
.end method
