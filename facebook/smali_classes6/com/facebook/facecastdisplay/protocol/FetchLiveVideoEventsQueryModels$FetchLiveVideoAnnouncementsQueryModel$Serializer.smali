.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoAnnouncementsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoAnnouncementsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1092920
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoAnnouncementsQueryModel;

    new-instance v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoAnnouncementsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoAnnouncementsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1092921
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1092919
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoAnnouncementsQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1092855
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1092856
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1092857
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1092858
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1092859
    if-eqz v2, :cond_c

    .line 1092860
    const-string v3, "announcements"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1092861
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1092862
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1092863
    if-eqz v3, :cond_b

    .line 1092864
    const-string v4, "nodes"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1092865
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1092866
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_a

    .line 1092867
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    const/4 p2, 0x0

    .line 1092868
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1092869
    invoke-virtual {v1, v5, p2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1092870
    if-eqz p0, :cond_0

    .line 1092871
    const-string v2, "accent_color"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1092872
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1092873
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1092874
    if-eqz p0, :cond_1

    .line 1092875
    const-string v2, "cta_action"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1092876
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1092877
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1092878
    if-eqz p0, :cond_2

    .line 1092879
    const-string v2, "cta_glyph"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1092880
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1092881
    :cond_2
    const/4 p0, 0x3

    invoke-virtual {v1, v5, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1092882
    if-eqz p0, :cond_3

    .line 1092883
    const-string v2, "cta_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1092884
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1092885
    :cond_3
    const/4 p0, 0x4

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1092886
    if-eqz p0, :cond_4

    .line 1092887
    const-string v2, "cta_text_off"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1092888
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1092889
    :cond_4
    const/4 p0, 0x5

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1092890
    if-eqz p0, :cond_5

    .line 1092891
    const-string v2, "cta_text_on"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1092892
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1092893
    :cond_5
    const/4 p0, 0x6

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1092894
    if-eqz p0, :cond_6

    .line 1092895
    const-string v2, "glpyh"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1092896
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1092897
    :cond_6
    const/4 p0, 0x7

    invoke-virtual {v1, v5, p0, p2}, LX/15i;->a(III)I

    move-result p0

    .line 1092898
    if-eqz p0, :cond_7

    .line 1092899
    const-string v2, "level"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1092900
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 1092901
    :cond_7
    const/16 p0, 0x8

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1092902
    if-eqz p0, :cond_8

    .line 1092903
    const-string v2, "message"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1092904
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1092905
    :cond_8
    const/16 p0, 0x9

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1092906
    if-eqz p0, :cond_9

    .line 1092907
    const-string v2, "name"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1092908
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1092909
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1092910
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 1092911
    :cond_a
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1092912
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1092913
    :cond_c
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1092914
    if-eqz v2, :cond_d

    .line 1092915
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1092916
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1092917
    :cond_d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1092918
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1092854
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoAnnouncementsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoAnnouncementsQueryModel$Serializer;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoAnnouncementsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
