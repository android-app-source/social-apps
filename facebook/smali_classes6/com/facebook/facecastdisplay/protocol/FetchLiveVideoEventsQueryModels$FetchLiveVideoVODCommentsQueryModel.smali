.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6dac320a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1093824
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1093823
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1093821
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1093822
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1093819
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->f:Ljava/lang/String;

    .line 1093820
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1093811
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1093812
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1093813
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1093814
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1093815
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1093816
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1093817
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1093818
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1093825
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1093826
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1093827
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    .line 1093828
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1093829
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;

    .line 1093830
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    .line 1093831
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1093832
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1093810
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1093807
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;-><init>()V

    .line 1093808
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1093809
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1093806
    const v0, -0x51daf8eb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1093805
    const v0, 0x4ed245b

    return v0
.end method

.method public final j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFeedback"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1093803
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    .line 1093804
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    return-object v0
.end method
