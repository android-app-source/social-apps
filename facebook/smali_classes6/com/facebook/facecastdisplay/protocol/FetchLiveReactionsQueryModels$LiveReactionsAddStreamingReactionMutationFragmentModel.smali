.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3d56887b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$FeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1091041
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1091042
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1091043
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1091044
    return-void
.end method

.method private a()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$FeedbackModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFeedback"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1091045
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$FeedbackModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$FeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$FeedbackModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$FeedbackModel;

    .line 1091046
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$FeedbackModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1091047
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1091048
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$FeedbackModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1091049
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1091050
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1091051
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1091052
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1091053
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1091054
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1091055
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$FeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$FeedbackModel;

    .line 1091056
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$FeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1091057
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;

    .line 1091058
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel$FeedbackModel;

    .line 1091059
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1091060
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1091061
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsAddStreamingReactionMutationFragmentModel;-><init>()V

    .line 1091062
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1091063
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1091064
    const v0, -0x48339902

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1091065
    const v0, -0x4441b742

    return v0
.end method
