.class public final Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1101602
    new-instance v0, LX/6US;

    invoke-direct {v0}, LX/6US;-><init>()V

    sput-object v0, Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1101603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1101604
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;->a:Ljava/lang/String;

    .line 1101605
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;->b:Ljava/lang/String;

    .line 1101606
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1101607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1101608
    iput-object p1, p0, Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;->a:Ljava/lang/String;

    .line 1101609
    iput-object p2, p0, Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;->b:Ljava/lang/String;

    .line 1101610
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1101611
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1101612
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1101613
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1101614
    return-void
.end method
