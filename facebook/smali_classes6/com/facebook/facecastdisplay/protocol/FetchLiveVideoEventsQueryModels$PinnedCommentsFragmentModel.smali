.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x143cb083
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1096061
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1096060
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1096058
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1096059
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1096056
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;->e:Ljava/lang/String;

    .line 1096057
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPinnedCommentEvents"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1096054
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;->f:Ljava/util/List;

    .line 1096055
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1096046
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1096047
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1096048
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1096049
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1096050
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1096051
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1096052
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1096053
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1096062
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1096063
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1096064
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1096065
    if-eqz v1, :cond_0

    .line 1096066
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;

    .line 1096067
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;->f:Ljava/util/List;

    .line 1096068
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1096069
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1096045
    new-instance v0, LX/6Sc;

    invoke-direct {v0, p1}, LX/6Sc;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1096043
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1096044
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1096042
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1096039
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel;-><init>()V

    .line 1096040
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1096041
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1096038
    const v0, 0x781acdb1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1096037
    const v0, -0x78fb05b

    return v0
.end method
