.class public final Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x77a5a354
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I

.field private h:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099365
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099364
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1099362
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1099363
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099360
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->f:Ljava/lang/String;

    .line 1099361
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1099350
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099351
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1099352
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1099353
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1099354
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1099355
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1099356
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->g:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1099357
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->h:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1099358
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099359
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1099347
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099348
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099349
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099366
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1099343
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1099344
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->g:I

    .line 1099345
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->h:I

    .line 1099346
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1099340
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;-><init>()V

    .line 1099341
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1099342
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1099339
    const v0, 0x677bdda

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1099338
    const v0, 0x4ed245b

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099336
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1099337
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 1099334
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1099335
    iget v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->g:I

    return v0
.end method

.method public final l()I
    .locals 2

    .prologue
    .line 1099332
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1099333
    iget v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->h:I

    return v0
.end method
