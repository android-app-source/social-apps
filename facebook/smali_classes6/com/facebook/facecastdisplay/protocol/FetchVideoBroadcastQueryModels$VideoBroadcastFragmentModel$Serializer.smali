.class public final Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1098175
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;

    new-instance v1, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1098176
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1098177
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1098178
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1098179
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x2

    const/4 p0, 0x0

    .line 1098180
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1098181
    invoke-virtual {v1, v0, p0, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1098182
    if-eqz v2, :cond_0

    .line 1098183
    const-string v3, "atom_size"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098184
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1098185
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1098186
    if-eqz v2, :cond_1

    .line 1098187
    const-string v3, "bitrate"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098188
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1098189
    :cond_1
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result v2

    .line 1098190
    if-eqz v2, :cond_2

    .line 1098191
    const-string v2, "broadcast_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098192
    invoke-virtual {v1, v0, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1098193
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1098194
    if-eqz v2, :cond_3

    .line 1098195
    const-string v3, "hdAtomSize"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098196
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1098197
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1098198
    if-eqz v2, :cond_4

    .line 1098199
    const-string v3, "hdBitrate"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098200
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1098201
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1098202
    if-eqz v2, :cond_5

    .line 1098203
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098204
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1098205
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1098206
    if-eqz v2, :cond_6

    .line 1098207
    const-string v3, "is_live_streaming"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098208
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1098209
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1098210
    if-eqz v2, :cond_7

    .line 1098211
    const-string v3, "live_viewer_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098212
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1098213
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1098214
    if-eqz v2, :cond_8

    .line 1098215
    const-string v3, "live_viewer_count_read_only"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098216
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1098217
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1098218
    if-eqz v2, :cond_9

    .line 1098219
    const-string v3, "playableUrlHdString"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098220
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1098221
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1098222
    if-eqz v2, :cond_a

    .line 1098223
    const-string v3, "playable_duration_in_ms"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098224
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1098225
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1098226
    if-eqz v2, :cond_b

    .line 1098227
    const-string v3, "playable_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098228
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1098229
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1098230
    if-eqz v2, :cond_c

    .line 1098231
    const-string v3, "playlist"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098232
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1098233
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1098234
    if-eqz v2, :cond_d

    .line 1098235
    const-string v3, "preferredPlayableUrlString"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1098236
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1098237
    :cond_d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1098238
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1098239
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel$Serializer;->a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
