.class public final Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3e8a9869
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1100429
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1100430
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1100431
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1100432
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1100433
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1100434
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1100435
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1100436
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1100437
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1100438
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1100439
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1100440
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1100441
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    .line 1100442
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1100443
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;

    .line 1100444
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    .line 1100445
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1100446
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100447
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    .line 1100448
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1100449
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;-><init>()V

    .line 1100450
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1100451
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1100452
    const v0, 0x396a6f9c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1100453
    const v0, -0x10b68efe

    return v0
.end method
