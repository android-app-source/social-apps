.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1091141
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel;

    new-instance v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1091142
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1091143
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1091144
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1091145
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/6S3;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1091146
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1091147
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel$Serializer;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel;LX/0nX;LX/0my;)V

    return-void
.end method
