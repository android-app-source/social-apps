.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1090791
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;

    new-instance v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1090792
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1090811
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1090794
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1090795
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1090796
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1090797
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1090798
    if-eqz v2, :cond_0

    .line 1090799
    const-string p0, "feedback"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1090800
    invoke-static {v1, v2, p1, p2}, LX/6S4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1090801
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1090802
    if-eqz v2, :cond_1

    .line 1090803
    const-string p0, "reaction_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1090804
    invoke-static {v1, v2, p1}, LX/6Rz;->a(LX/15i;ILX/0nX;)V

    .line 1090805
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1090806
    if-eqz v2, :cond_2

    .line 1090807
    const-string p0, "reactor"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1090808
    invoke-static {v1, v2, p1}, LX/6S0;->a(LX/15i;ILX/0nX;)V

    .line 1090809
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1090810
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1090793
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$Serializer;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel;LX/0nX;LX/0my;)V

    return-void
.end method
