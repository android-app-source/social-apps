.class public final Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3ceeda2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1100379
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1100378
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1100376
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1100377
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100373
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1100374
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1100375
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100371
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->h:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$ProfilePictureModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->h:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$ProfilePictureModel;

    .line 1100372
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->h:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1100359
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1100360
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1100361
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1100362
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1100363
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->k()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$ProfilePictureModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1100364
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1100365
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1100366
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1100367
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1100368
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1100369
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1100370
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1100341
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1100342
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->k()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1100343
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->k()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$ProfilePictureModel;

    .line 1100344
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->k()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1100345
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    .line 1100346
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->h:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel$ProfilePictureModel;

    .line 1100347
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1100348
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100358
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1100355
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;-><init>()V

    .line 1100356
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1100357
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100353
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->f:Ljava/lang/String;

    .line 1100354
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100351
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->g:Ljava/lang/String;

    .line 1100352
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1100350
    const v0, -0x26a94413

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1100349
    const v0, 0x42d13b4

    return v0
.end method
