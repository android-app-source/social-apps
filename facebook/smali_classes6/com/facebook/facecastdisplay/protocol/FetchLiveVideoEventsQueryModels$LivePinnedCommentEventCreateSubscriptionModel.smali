.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x71bdd037
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1094947
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1094948
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1094949
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1094950
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094951
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1094952
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1094953
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094954
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->f:Ljava/lang/String;

    .line 1094955
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1094956
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1094957
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1094958
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1094959
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1094960
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1094961
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1094962
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1094963
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1094964
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1094965
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1094966
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1094967
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1094968
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;

    .line 1094969
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1094970
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;

    .line 1094971
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->g:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;

    .line 1094972
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1094973
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPinnedCommentEvent"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094974
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->g:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->g:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;

    .line 1094975
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->g:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1094976
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;-><init>()V

    .line 1094977
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1094978
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1094979
    const v0, 0x3c8a1c23

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1094980
    const v0, -0x1509a70e

    return v0
.end method
