.class public final Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1100136
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;

    new-instance v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1100137
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1100135
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1100117
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1100118
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 1100119
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1100120
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 1100121
    if-eqz p0, :cond_0

    .line 1100122
    const-string p0, "__type__"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1100123
    invoke-static {v1, v0, p2, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1100124
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1100125
    if-eqz p0, :cond_1

    .line 1100126
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1100127
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1100128
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1100129
    if-eqz p0, :cond_2

    .line 1100130
    const-string p2, "is_tip_jar_eligible"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1100131
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1100132
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1100133
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1100134
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel$Serializer;->a(Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
