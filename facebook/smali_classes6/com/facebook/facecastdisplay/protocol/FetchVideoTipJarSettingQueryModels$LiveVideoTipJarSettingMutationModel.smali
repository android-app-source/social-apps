.class public final Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x69d10cb1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099122
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099146
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1099144
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1099145
    return-void
.end method

.method private a()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099142
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;

    .line 1099143
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1099136
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099137
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1099138
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1099139
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1099140
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099141
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1099128
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099129
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1099130
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;

    .line 1099131
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1099132
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel;

    .line 1099133
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel;->e:Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel$VideoTipJarSettingModel;

    .line 1099134
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099135
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1099125
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarSettingMutationModel;-><init>()V

    .line 1099126
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1099127
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1099124
    const v0, -0x5690871b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1099123
    const v0, 0x3aaab36

    return v0
.end method
