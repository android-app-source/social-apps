.class public final Lcom/facebook/facecastdisplay/protocol/LiveVideoSendTipMutationModels$LiveVideoSendTipMutationModel$TipJarTransactionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoSendTipMutationModels$LiveVideoSendTipMutationModel$TipJarTransactionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoSendTipMutationModels$LiveVideoSendTipMutationModel$TipJarTransactionModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099643
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoSendTipMutationModels$LiveVideoSendTipMutationModel$TipJarTransactionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099642
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoSendTipMutationModels$LiveVideoSendTipMutationModel$TipJarTransactionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1099640
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1099641
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099638
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoSendTipMutationModels$LiveVideoSendTipMutationModel$TipJarTransactionModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoSendTipMutationModels$LiveVideoSendTipMutationModel$TipJarTransactionModel;->e:Ljava/lang/String;

    .line 1099639
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoSendTipMutationModels$LiveVideoSendTipMutationModel$TipJarTransactionModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1099632
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099633
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoSendTipMutationModels$LiveVideoSendTipMutationModel$TipJarTransactionModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1099634
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1099635
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1099636
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099637
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1099629
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099630
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099631
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099628
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoSendTipMutationModels$LiveVideoSendTipMutationModel$TipJarTransactionModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1099623
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoSendTipMutationModels$LiveVideoSendTipMutationModel$TipJarTransactionModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoSendTipMutationModels$LiveVideoSendTipMutationModel$TipJarTransactionModel;-><init>()V

    .line 1099624
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1099625
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1099627
    const v0, -0x57ffb8cf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1099626
    const v0, 0x4677331e

    return v0
.end method
