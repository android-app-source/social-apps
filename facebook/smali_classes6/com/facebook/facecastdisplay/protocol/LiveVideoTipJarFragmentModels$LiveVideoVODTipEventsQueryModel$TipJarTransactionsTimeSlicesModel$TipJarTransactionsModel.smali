.class public final Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x587043e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1100921
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1100920
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1100918
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1100919
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100916
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->f:Ljava/lang/String;

    .line 1100917
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100914
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->g:Ljava/lang/String;

    .line 1100915
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1100901
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1100902
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->j()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1100903
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1100904
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1100905
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->k()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1100906
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1100907
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1100908
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1100909
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1100910
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1100911
    const/4 v0, 0x4

    iget v1, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->i:I

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 1100912
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1100913
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1100888
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1100889
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->j()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1100890
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->j()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;

    .line 1100891
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->j()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1100892
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;

    .line 1100893
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;

    .line 1100894
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->k()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1100895
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->k()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;

    .line 1100896
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->k()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1100897
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;

    .line 1100898
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->h:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;

    .line 1100899
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1100900
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100873
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1100885
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1100886
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->i:I

    .line 1100887
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1100882
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;-><init>()V

    .line 1100883
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1100884
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1100881
    const v0, 0x1348788f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1100880
    const v0, 0x4677331e

    return v0
.end method

.method public final j()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100878
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;

    .line 1100879
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->e:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1100876
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->h:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->h:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;

    .line 1100877
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->h:Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;

    return-object v0
.end method

.method public final l()I
    .locals 2

    .prologue
    .line 1100874
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1100875
    iget v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->i:I

    return v0
.end method
