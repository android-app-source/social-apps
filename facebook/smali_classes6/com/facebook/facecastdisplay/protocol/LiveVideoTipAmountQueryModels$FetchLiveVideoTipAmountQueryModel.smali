.class public final Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x180ec35c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:D

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099750
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1099749
    const-class v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1099747
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1099748
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099724
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;->e:Ljava/lang/String;

    .line 1099725
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1099739
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099740
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1099741
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1099742
    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1099743
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;->f:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1099744
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;->g:I

    invoke-virtual {p1, v0, v1, v6}, LX/186;->a(III)V

    .line 1099745
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099746
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1099736
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1099737
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1099738
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1099735
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1099731
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1099732
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;->f:D

    .line 1099733
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;->g:I

    .line 1099734
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1099728
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipAmountQueryModels$FetchLiveVideoTipAmountQueryModel;-><init>()V

    .line 1099729
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1099730
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1099727
    const v0, -0x7a80f97

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1099726
    const v0, -0x23fad327

    return v0
.end method
