.class public final Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/3cn;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x12d9695d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1094201
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1094202
    const-class v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1094203
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1094204
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1094205
    iput p1, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->n:I

    .line 1094206
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1094207
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1094208
    if-eqz v0, :cond_0

    .line 1094209
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1094210
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1094211
    iput-object p1, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->l:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 1094212
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1094213
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1094214
    if-eqz v0, :cond_0

    .line 1094215
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1094216
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1094217
    iput-object p1, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->i:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1094218
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1094219
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1094220
    if-eqz v0, :cond_0

    .line 1094221
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1094222
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1094223
    iput-object p1, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->h:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    .line 1094224
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1094225
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1094226
    if-eqz v0, :cond_0

    .line 1094227
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1094228
    :cond_0
    return-void
.end method

.method private m()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094229
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    .line 1094230
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    return-object v0
.end method

.method private n()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094231
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->g:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->g:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 1094232
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->g:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    return-object v0
.end method

.method private o()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094233
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->h:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->h:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    .line 1094234
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->h:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    return-object v0
.end method

.method private p()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094235
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->i:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->i:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1094236
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->i:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    return-object v0
.end method

.method private q()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094237
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->j:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->j:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    .line 1094238
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->j:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    return-object v0
.end method

.method private r()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094239
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->l:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->l:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 1094240
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->l:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    return-object v0
.end method

.method private s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094241
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->m:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->m:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 1094242
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->m:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1094243
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1094244
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->m()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1094245
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->n()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1094246
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->o()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1094247
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->p()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1094248
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->q()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1094249
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->dt_()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1094250
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->r()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1094251
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1094252
    const/16 v8, 0xa

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1094253
    iget-boolean v8, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->e:Z

    invoke-virtual {p1, v9, v8}, LX/186;->a(IZ)V

    .line 1094254
    const/4 v8, 0x1

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 1094255
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1094256
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1094257
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1094258
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1094259
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1094260
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1094261
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1094262
    const/16 v0, 0x9

    iget v1, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->n:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 1094263
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1094264
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1094157
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1094158
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->m()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1094159
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->m()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    .line 1094160
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->m()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1094161
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;

    .line 1094162
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    .line 1094163
    :cond_0
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->n()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1094164
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->n()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 1094165
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->n()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1094166
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;

    .line 1094167
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->g:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 1094168
    :cond_1
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->o()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1094169
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->o()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    .line 1094170
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->o()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1094171
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;

    .line 1094172
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->h:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    .line 1094173
    :cond_2
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->p()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1094174
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->p()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1094175
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->p()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1094176
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;

    .line 1094177
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->i:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1094178
    :cond_3
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->q()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1094179
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->q()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    .line 1094180
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->q()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1094181
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;

    .line 1094182
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->j:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    .line 1094183
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->dt_()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1094184
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->dt_()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1094185
    if-eqz v2, :cond_5

    .line 1094186
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;

    .line 1094187
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->k:Ljava/util/List;

    move-object v1, v0

    .line 1094188
    :cond_5
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->r()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1094189
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->r()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 1094190
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->r()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1094191
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;

    .line 1094192
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->l:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 1094193
    :cond_6
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1094194
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 1094195
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1094196
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;

    .line 1094197
    iput-object v0, v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->m:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 1094198
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1094199
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    :cond_8
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1094200
    new-instance v0, LX/6SX;

    invoke-direct {v0, p1}, LX/6SX;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1094063
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1094064
    invoke-virtual {p1, p2, v1}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->e:Z

    .line 1094065
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->n:I

    .line 1094066
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1094068
    const-string v0, "comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1094069
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->m()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    move-result-object v0

    .line 1094070
    if-eqz v0, :cond_4

    .line 1094071
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1094072
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1094073
    iput v2, p2, LX/18L;->c:I

    .line 1094074
    :goto_0
    return-void

    .line 1094075
    :cond_0
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1094076
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->o()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    move-result-object v0

    .line 1094077
    if-eqz v0, :cond_4

    .line 1094078
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1094079
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1094080
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1094081
    :cond_1
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1094082
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->p()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 1094083
    if-eqz v0, :cond_4

    .line 1094084
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1094085
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1094086
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1094087
    :cond_2
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1094088
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->q()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    move-result-object v0

    .line 1094089
    if-eqz v0, :cond_4

    .line 1094090
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1094091
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1094092
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1094093
    :cond_3
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1094094
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1094095
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1094096
    const/16 v0, 0x9

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1094097
    :cond_4
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1094098
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1094099
    check-cast p2, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;)V

    .line 1094100
    :cond_0
    :goto_0
    return-void

    .line 1094101
    :cond_1
    const-string v0, "reactors"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1094102
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-direct {p0, p2}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)V

    goto :goto_0

    .line 1094103
    :cond_2
    const-string v0, "top_reactions"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1094104
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-direct {p0, p2}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1094105
    const-string v0, "comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1094106
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->m()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    move-result-object v0

    .line 1094107
    if-eqz v0, :cond_0

    .line 1094108
    if-eqz p3, :cond_1

    .line 1094109
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    .line 1094110
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;->a(I)V

    .line 1094111
    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->f:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    .line 1094112
    :cond_0
    :goto_0
    return-void

    .line 1094113
    :cond_1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;->a(I)V

    goto :goto_0

    .line 1094114
    :cond_2
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1094115
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->o()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    move-result-object v0

    .line 1094116
    if-eqz v0, :cond_0

    .line 1094117
    if-eqz p3, :cond_3

    .line 1094118
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    .line 1094119
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;->a(I)V

    .line 1094120
    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->h:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    goto :goto_0

    .line 1094121
    :cond_3
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;->a(I)V

    goto :goto_0

    .line 1094122
    :cond_4
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1094123
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->p()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 1094124
    if-eqz v0, :cond_0

    .line 1094125
    if-eqz p3, :cond_5

    .line 1094126
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1094127
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    .line 1094128
    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->i:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    goto :goto_0

    .line 1094129
    :cond_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    goto :goto_0

    .line 1094130
    :cond_6
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1094131
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->q()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    move-result-object v0

    .line 1094132
    if-eqz v0, :cond_0

    .line 1094133
    if-eqz p3, :cond_7

    .line 1094134
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    .line 1094135
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;->a(I)V

    .line 1094136
    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->j:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    goto/16 :goto_0

    .line 1094137
    :cond_7
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;->a(I)V

    goto/16 :goto_0

    .line 1094138
    :cond_8
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1094139
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->a(I)V

    goto/16 :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1094140
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1094141
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->e:Z

    return v0
.end method

.method public final synthetic b()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094142
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->m()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1094143
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;-><init>()V

    .line 1094144
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1094145
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094067
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->n()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094146
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->o()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1094147
    const v0, 0x2f70569c

    return v0
.end method

.method public final dt_()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1094148
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->k:Ljava/util/List;

    .line 1094149
    iget-object v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic du_()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094150
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->q()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094151
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->p()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1094152
    const v0, -0x78fb05b

    return v0
.end method

.method public final synthetic j()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094153
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->r()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1094154
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()I
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1094155
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1094156
    iget v0, p0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->n:I

    return v0
.end method
