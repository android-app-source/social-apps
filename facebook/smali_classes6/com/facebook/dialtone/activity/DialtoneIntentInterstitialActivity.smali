.class public Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final r:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public p:LX/121;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Landroid/content/Intent;

.field private t:Z

.field private u:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 905976
    const-class v0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;

    sput-object v0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->r:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 905977
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 905978
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->p:LX/121;

    sget-object v1, LX/0yY;->DIALTONE_FACEWEB:LX/0yY;

    const v2, 0x7f0805f7

    invoke-virtual {p0, v2}, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0805f8

    invoke-virtual {p0, v3}, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/5NA;

    invoke-direct {v4, p0}, LX/5NA;-><init>(Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;)V

    invoke-virtual {v0, v1, v2, v3, v4}, LX/121;->a(LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;)LX/121;

    .line 905979
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->p:LX/121;

    sget-object v1, LX/0yY;->DIALTONE_FACEWEB:LX/0yY;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/121;->a(LX/0yY;LX/0gc;Ljava/lang/Object;)Landroid/support/v4/app/DialogFragment;

    .line 905980
    return-void
.end method

.method private static a(Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;LX/121;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 905981
    iput-object p1, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->p:LX/121;

    iput-object p2, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->q:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;

    invoke-static {v1}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v0

    check-cast v0, LX/121;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1}, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->a(Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;LX/121;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 905982
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 905983
    invoke-static {p0, p0}, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 905984
    invoke-virtual {p0}, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 905985
    const-string v0, "destination_intent"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->s:Landroid/content/Intent;

    .line 905986
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->s:Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    .line 905987
    const-string v0, "start_for_result"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->t:Z

    .line 905988
    const-string v0, "request_code"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->u:I

    .line 905989
    invoke-direct {p0}, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->a()V

    .line 905990
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 905991
    iget-boolean v0, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->t:Z

    if-eqz v0, :cond_0

    .line 905992
    :try_start_0
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->q:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->s:Landroid/content/Intent;

    iget v2, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->u:I

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 905993
    :goto_0
    return-void

    .line 905994
    :catch_0
    move-exception v0

    .line 905995
    sget-object v1, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->r:Ljava/lang/Class;

    const-string v2, "Activity not found for intent: [%s]"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->s:Landroid/content/Intent;

    aput-object v4, v3, v5

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 905996
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->q:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->s:Landroid/content/Intent;

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 905997
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->finish()V

    goto :goto_0

    .line 905998
    :catch_1
    move-exception v0

    .line 905999
    sget-object v1, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->r:Ljava/lang/Class;

    const-string v2, "Activity not found for intent: [%s]"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/dialtone/activity/DialtoneIntentInterstitialActivity;->s:Landroid/content/Intent;

    aput-object v4, v3, v5

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
