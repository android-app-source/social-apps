.class public Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final A:Landroid/view/animation/Interpolator;

.field private static final B:Landroid/view/animation/Interpolator;


# instance fields
.field private C:Landroid/view/View;

.field public p:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private v:Landroid/view/View;

.field private w:Lcom/facebook/resources/ui/FbTextView;

.field public x:Landroid/view/View;

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 906121
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->A:Landroid/view/animation/Interpolator;

    .line 906122
    new-instance v0, Landroid/view/animation/OvershootInterpolator;

    const/high16 v1, 0x3e800000    # 0.25f

    invoke-direct {v0, v1}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    sput-object v0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->B:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 906120
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;Landroid/os/Handler;LX/0yc;LX/0Ot;LX/0Zb;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;",
            "Landroid/os/Handler;",
            "LX/0yc;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 906119
    iput-object p1, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->p:Landroid/os/Handler;

    iput-object p2, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->q:LX/0yc;

    iput-object p3, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->r:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->s:LX/0Zb;

    iput-object p5, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->t:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->u:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;

    invoke-static {v6}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    invoke-static {v6}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v2

    check-cast v2, LX/0yc;

    const/16 v3, 0x259

    invoke-static {v6, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v6}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    const/16 v5, 0x455

    invoke-static {v6, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v7, 0xf9a

    invoke-static {v6, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->a(Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;Landroid/os/Handler;LX/0yc;LX/0Ot;LX/0Zb;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;)V
    .locals 4

    .prologue
    .line 906114
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->v:Landroid/view/View;

    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 906115
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 906116
    new-instance v1, LX/5NE;

    invoke-direct {v1, p0}, LX/5NE;-><init>(Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 906117
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 906118
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static b(Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;)V
    .locals 6

    .prologue
    .line 906123
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->v:Landroid/view/View;

    const-string v1, "translationY"

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b02c4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 906124
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 906125
    sget-object v1, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->B:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 906126
    new-instance v1, LX/5NF;

    invoke-direct {v1, p0}, LX/5NF;-><init>(Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 906127
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 906128
    return-void
.end method

.method public static b(Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 906111
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 906112
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 906113
    :cond_0
    return-void
.end method

.method public static l(Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;)V
    .locals 2

    .prologue
    .line 906107
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->q:LX/0yc;

    const-string v1, "dialtone_mode_transition"

    invoke-virtual {v0, v1}, LX/0yc;->a(Ljava/lang/String;)Z

    move-result v0

    .line 906108
    if-eqz v0, :cond_0

    .line 906109
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->q:LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->a(Landroid/content/Context;)V

    .line 906110
    :cond_0
    return-void
.end method

.method public static m(Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;)V
    .locals 4

    .prologue
    .line 906100
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->w:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 906101
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 906102
    sget-object v1, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->A:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 906103
    new-instance v1, LX/5NG;

    invoke-direct {v1, p0}, LX/5NG;-><init>(Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 906104
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 906105
    return-void

    .line 906106
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 906065
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 906066
    invoke-static {p0, p0}, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 906067
    const v0, 0x7f030418

    invoke-virtual {p0, v0}, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->setContentView(I)V

    .line 906068
    const v0, 0x7f0d0c93

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->C:Landroid/view/View;

    .line 906069
    const v0, 0x7f0d07ac

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->v:Landroid/view/View;

    .line 906070
    const v0, 0x7f0d0c94

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->w:Lcom/facebook/resources/ui/FbTextView;

    .line 906071
    const v0, 0x7f0d0c95

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->x:Landroid/view/View;

    .line 906072
    invoke-virtual {p0}, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "follow_up_intent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->z:Ljava/lang/String;

    .line 906073
    invoke-virtual {p0}, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "action"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->y:Ljava/lang/String;

    .line 906074
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->y:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "upgrade"

    :goto_0
    iput-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->y:Ljava/lang/String;

    .line 906075
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->y:Ljava/lang/String;

    const-string v1, "upgrade"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 906076
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->v:Landroid/view/View;

    const v1, 0x7f020702

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 906077
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->w:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0805ed

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 906078
    :goto_1
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "dialtone_transition_interstitial_impression"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "dialtone"

    .line 906079
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 906080
    move-object v1, v0

    .line 906081
    const-string v0, "ref"

    invoke-virtual {p0}, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "ref"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 906082
    const-string v2, "carrier_id"

    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 906083
    const-string v0, "action"

    iget-object v2, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->y:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 906084
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->s:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 906085
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->C:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 906086
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->v:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 906087
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->w:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 906088
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 906089
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->C:Landroid/view/View;

    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 906090
    sget-object v1, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->A:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 906091
    new-instance v1, LX/5ND;

    invoke-direct {v1, p0}, LX/5ND;-><init>(Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 906092
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 906093
    return-void

    .line 906094
    :cond_0
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->y:Ljava/lang/String;

    goto/16 :goto_0

    .line 906095
    :cond_1
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->y:Ljava/lang/String;

    const-string v1, "downgrade"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 906096
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->v:Landroid/view/View;

    const v1, 0x7f020b58

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 906097
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->w:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0805ee

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_1

    .line 906098
    :cond_2
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "dialtone"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid transition mode for Dialtone."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 906099
    invoke-virtual {p0}, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;->finish()V

    goto/16 :goto_1

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
