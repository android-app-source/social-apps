.class public Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# instance fields
.field public p:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 906171
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yc;LX/0Zb;)V
    .locals 0

    .prologue
    .line 906170
    iput-object p1, p0, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->q:LX/0yc;

    iput-object p3, p0, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->r:LX/0Zb;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;

    invoke-static {v2}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v2}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v1

    check-cast v1, LX/0yc;

    invoke-static {v2}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->a(Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yc;LX/0Zb;)V

    return-void
.end method

.method public static b(Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 906164
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "dialtone"

    .line 906165
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 906166
    move-object v0, v0

    .line 906167
    const-string v1, "carrier_id"

    iget-object v2, p0, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 906168
    iget-object v1, p0, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->r:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 906169
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 906163
    const-string v0, "dialtone_ineligible_interstitial"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 906144
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 906145
    invoke-static {p0, p0}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 906146
    const v0, 0x7f030410

    invoke-virtual {p0, v0}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->setContentView(I)V

    .line 906147
    const v0, 0x7f0d0841

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 906148
    invoke-virtual {p0}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "dialtone_wrong_carrier_flag"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 906149
    iget-object v1, p0, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dQ;->h:LX/0Tn;

    const v3, 0x7f080619

    invoke-virtual {p0, v3}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 906150
    const v2, 0x7f080614

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 906151
    :goto_0
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 906152
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 906153
    const v0, 0x7f0d0842

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 906154
    const v1, 0x7f080615

    invoke-virtual {p0, v1}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 906155
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 906156
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 906157
    const v0, 0x7f0d0c71

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 906158
    new-instance v1, LX/5NH;

    invoke-direct {v1, p0}, LX/5NH;-><init>(Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 906159
    return-void

    .line 906160
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "dialtone_not_in_region_flag"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 906161
    const v1, 0x7f080617

    invoke-virtual {p0, v1}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 906162
    :cond_1
    const v1, 0x7f080616

    invoke-virtual {p0, v1}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 906134
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->q:LX/0yc;

    const-string v1, "dialtone_ineligible_interstitial_back_pressed"

    invoke-virtual {v0, v1}, LX/0yc;->b(Ljava/lang/String;)Z

    .line 906135
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 906136
    const-string v0, "dialtone_ineligible_interstitial_back_pressed"

    invoke-static {p0, v0}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->b(Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;Ljava/lang/String;)V

    .line 906137
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x658eedf6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 906141
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 906142
    const-string v1, "dialtone_ineligible_interstitial_become_invisible"

    invoke-static {p0, v1}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->b(Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;Ljava/lang/String;)V

    .line 906143
    const/16 v1, 0x23

    const v2, 0x6a457f4a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x215a48fd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 906138
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 906139
    const-string v1, "dialtone_ineligible_interstitial_impression"

    invoke-static {p0, v1}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->b(Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;Ljava/lang/String;)V

    .line 906140
    const/16 v1, 0x23

    const v2, 0x190318cb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
