.class public Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/0l6;


# instance fields
.field public p:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 906013
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;LX/0yc;Lcom/facebook/content/SecureContextHelper;LX/0Zb;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;",
            "LX/0yc;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 906014
    iput-object p1, p0, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->p:LX/0yc;

    iput-object p2, p0, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->q:Lcom/facebook/content/SecureContextHelper;

    iput-object p3, p0, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->r:LX/0Zb;

    iput-object p4, p0, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->s:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;

    invoke-static {v3}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v3}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    const/16 v4, 0xf9a

    invoke-static {v3, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->a(Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;LX/0yc;Lcom/facebook/content/SecureContextHelper;LX/0Zb;LX/0Ot;)V

    return-void
.end method

.method public static b(Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;)V
    .locals 3

    .prologue
    .line 906015
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->q:Lcom/facebook/content/SecureContextHelper;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 906016
    return-void
.end method

.method public static b(Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 906017
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "dialtone"

    .line 906018
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 906019
    move-object v1, v0

    .line 906020
    const-string v2, "carrier_id"

    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 906021
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->r:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 906022
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 906023
    const-string v0, "dialtone_mode_selection_interstitial"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 906024
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 906025
    invoke-static {p0, p0}, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 906026
    const v0, 0x7f03040f

    invoke-virtual {p0, v0}, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->setContentView(I)V

    .line 906027
    const v0, 0x7f0d0c67

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/5NB;

    invoke-direct {v1, p0}, LX/5NB;-><init>(Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 906028
    const v0, 0x7f0d0c6c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/5NC;

    invoke-direct {v1, p0}, LX/5NC;-><init>(Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 906029
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 906030
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 906031
    const-string v0, "dialtone_mode_selection_interstitial_dismissed_by_other_interstitial"

    invoke-static {p0, v0}, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->b(Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;Ljava/lang/String;)V

    .line 906032
    invoke-virtual {p0}, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->finish()V

    .line 906033
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 906034
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 906035
    const-string v0, "dialtone_mode_selection_interstitial_back_pressed"

    invoke-static {p0, v0}, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->b(Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;Ljava/lang/String;)V

    .line 906036
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x1b93064f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 906037
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 906038
    const-string v1, "dialtone_mode_selection_interstitial_become_invisible"

    invoke-static {p0, v1}, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->b(Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;Ljava/lang/String;)V

    .line 906039
    const/16 v1, 0x23

    const v2, -0x67c103dd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x774bfc04

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 906040
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 906041
    const-string v1, "dialtone_mode_selection_interstitial_impression"

    invoke-static {p0, v1}, Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;->b(Lcom/facebook/dialtone/activity/DialtoneModeSelectionActivity;Ljava/lang/String;)V

    .line 906042
    const/16 v1, 0x23

    const v2, 0x44134c1c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
