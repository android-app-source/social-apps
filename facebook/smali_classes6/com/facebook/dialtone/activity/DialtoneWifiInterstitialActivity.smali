.class public Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# instance fields
.field public p:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 906216
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yc;LX/0Zb;)V
    .locals 0

    .prologue
    .line 906217
    iput-object p1, p0, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->q:LX/0yc;

    iput-object p3, p0, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->r:LX/0Zb;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;

    invoke-static {v2}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v2}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v1

    check-cast v1, LX/0yc;

    invoke-static {v2}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->a(Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yc;LX/0Zb;)V

    return-void
.end method

.method public static b(Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 906210
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "dialtone"

    .line 906211
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 906212
    move-object v0, v0

    .line 906213
    const-string v1, "carrier_id"

    iget-object v2, p0, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 906214
    iget-object v1, p0, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->r:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 906215
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 906209
    const-string v0, "dialtone_wifi_interstitial"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 906194
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 906195
    invoke-static {p0, p0}, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 906196
    const v0, 0x7f030410

    invoke-virtual {p0, v0}, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->setContentView(I)V

    .line 906197
    const v0, 0x7f0d0841

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 906198
    const v1, 0x7f080612

    invoke-virtual {p0, v1}, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 906199
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 906200
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 906201
    const v0, 0x7f0d0842

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 906202
    iget-object v1, p0, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dQ;->h:LX/0Tn;

    const v3, 0x7f080619

    invoke-virtual {p0, v3}, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 906203
    const v2, 0x7f080613

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 906204
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 906205
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 906206
    const v0, 0x7f0d0c71

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 906207
    new-instance v1, LX/5NI;

    invoke-direct {v1, p0}, LX/5NI;-><init>(Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 906208
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 906190
    iget-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->q:LX/0yc;

    const-string v1, "dialtone_wifi_interstitial_back_pressed"

    invoke-virtual {v0, v1}, LX/0yc;->b(Ljava/lang/String;)Z

    .line 906191
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 906192
    const-string v0, "dialtone_wifi_interstitial_back_pressed"

    invoke-static {p0, v0}, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->b(Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;Ljava/lang/String;)V

    .line 906193
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x2ec7fc01

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 906187
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 906188
    const-string v1, "dialtone_wifi_interstitial_become_invisible"

    invoke-static {p0, v1}, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->b(Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;Ljava/lang/String;)V

    .line 906189
    const/16 v1, 0x23

    const v2, 0x8a3ad8b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x5af8d367

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 906184
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 906185
    const-string v1, "dialtone_wifi_interstitial_impression"

    invoke-static {p0, v1}, Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;->b(Lcom/facebook/dialtone/activity/DialtoneWifiInterstitialActivity;Ljava/lang/String;)V

    .line 906186
    const/16 v1, 0x23

    const v2, 0x157e1318

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
