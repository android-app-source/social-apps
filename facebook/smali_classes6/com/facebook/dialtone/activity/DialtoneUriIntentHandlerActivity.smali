.class public Lcom/facebook/dialtone/activity/DialtoneUriIntentHandlerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation runtime Lcom/facebook/base/activity/DeliverOnNewIntentWhenFinishing;
.end annotation


# instance fields
.field public p:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 906172
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/dialtone/activity/DialtoneUriIntentHandlerActivity;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v0

    check-cast v0, LX/0yc;

    iput-object v0, p0, Lcom/facebook/dialtone/activity/DialtoneUriIntentHandlerActivity;->p:LX/0yc;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 906173
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 906174
    invoke-static {p0, p0}, Lcom/facebook/dialtone/activity/DialtoneUriIntentHandlerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 906175
    invoke-virtual {p0}, Lcom/facebook/dialtone/activity/DialtoneUriIntentHandlerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 906176
    iget-object v1, p0, Lcom/facebook/dialtone/activity/DialtoneUriIntentHandlerActivity;->p:LX/0yc;

    invoke-virtual {v1, p0, v0}, LX/0yc;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 906177
    invoke-virtual {p0}, Lcom/facebook/dialtone/activity/DialtoneUriIntentHandlerActivity;->finish()V

    .line 906178
    return-void
.end method
