.class public Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field public a:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 906301
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 906302
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 906303
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 906304
    invoke-direct {p0, p1, p2}, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 906305
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 906306
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 906307
    invoke-direct {p0, p1, p2}, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 906308
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 906309
    const-class v0, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;

    invoke-static {v0, p0}, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 906310
    sget-object v0, LX/03r;->LightswitchPhoneImageWithText:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 906311
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 906312
    const/16 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->b:Z

    .line 906313
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 906314
    return-void

    .line 906315
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The mode attribute needs to be set via XML"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v0

    check-cast v0, LX/0yc;

    iput-object v0, p0, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->a:LX/0yc;

    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 906316
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 906317
    invoke-virtual {p0}, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 906318
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 906319
    const/4 v0, -0x1

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 906320
    sget-object v0, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 906321
    const v0, 0x7f0b004e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 906322
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-double v4, v0

    const-wide v6, 0x3fd0f5c28f5c28f6L    # 0.265

    mul-double/2addr v4, v6

    double-to-float v3, v4

    .line 906323
    iget-boolean v0, p0, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->b:Z

    if-eqz v0, :cond_1

    const v0, 0x7f080620

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 906324
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {p1, v0, v4, v3, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 906325
    iget-object v0, p0, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->a:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 906326
    const v0, 0x7f020a20

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 906327
    iget-boolean v0, p0, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->b:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0a00e6

    invoke-static {v0, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    .line 906328
    :goto_1
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    move-object v0, v1

    .line 906329
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    int-to-double v2, v1

    const-wide v4, 0x3fd999999999999aL    # 0.4

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    int-to-double v2, v2

    const-wide/high16 v4, 0x3fe8000000000000L    # 0.75

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    int-to-double v4, v3

    const-wide v6, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v4, v6

    double-to-int v3, v4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v4

    int-to-double v4, v4

    const-wide v6, 0x3fee666666666666L    # 0.95

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 906330
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 906331
    :cond_0
    return-void

    .line 906332
    :cond_1
    const v0, 0x7f080621

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 906333
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0a00d5

    invoke-static {v0, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    goto :goto_1

    .line 906334
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->b:Z

    if-eqz v0, :cond_0

    .line 906335
    const v0, 0x7f02095e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 906336
    invoke-virtual {p0}, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a00e6

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_2
.end method

.method public setIsFreeMode(Z)V
    .locals 0

    .prologue
    .line 906337
    iput-boolean p1, p0, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->b:Z

    .line 906338
    return-void
.end method
