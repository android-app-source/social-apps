.class public Lcom/facebook/componentscript/components/CSImageSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 899054
    const-class v0, Lcom/facebook/componentscript/components/CSImageSpec;

    const-string v1, "ComponentScript"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/componentscript/components/CSImageSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 899055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 899056
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/componentscript/components/CSImageSpec;
    .locals 4

    .prologue
    .line 899057
    const-class v1, Lcom/facebook/componentscript/components/CSImageSpec;

    monitor-enter v1

    .line 899058
    :try_start_0
    sget-object v0, Lcom/facebook/componentscript/components/CSImageSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 899059
    sput-object v2, Lcom/facebook/componentscript/components/CSImageSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 899060
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 899061
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 899062
    new-instance p0, Lcom/facebook/componentscript/components/CSImageSpec;

    invoke-direct {p0}, Lcom/facebook/componentscript/components/CSImageSpec;-><init>()V

    .line 899063
    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    .line 899064
    iput-object v3, p0, Lcom/facebook/componentscript/components/CSImageSpec;->b:LX/1Ad;

    .line 899065
    move-object v0, p0

    .line 899066
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 899067
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/componentscript/components/CSImageSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 899068
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 899069
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
