.class public final Lcom/facebook/componentscript/components/CSImage$CSImageImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/5Kj;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/java2js/JSValue;

.field public b:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic c:LX/5Kj;


# direct methods
.method public constructor <init>(LX/5Kj;)V
    .locals 1

    .prologue
    .line 899009
    iput-object p1, p0, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;->c:LX/5Kj;

    .line 899010
    move-object v0, p1

    .line 899011
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 899012
    sget-object v0, Lcom/facebook/componentscript/components/CSImageSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 899013
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 899014
    const-string v0, "CSImage"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 899015
    if-ne p0, p1, :cond_1

    .line 899016
    :cond_0
    :goto_0
    return v0

    .line 899017
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 899018
    goto :goto_0

    .line 899019
    :cond_3
    check-cast p1, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;

    .line 899020
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 899021
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 899022
    if-eq v2, v3, :cond_0

    .line 899023
    iget-object v2, p0, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;->a:Lcom/facebook/java2js/JSValue;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;->a:Lcom/facebook/java2js/JSValue;

    iget-object v3, p1, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;->a:Lcom/facebook/java2js/JSValue;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 899024
    goto :goto_0

    .line 899025
    :cond_5
    iget-object v2, p1, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;->a:Lcom/facebook/java2js/JSValue;

    if-nez v2, :cond_4

    .line 899026
    :cond_6
    iget-object v2, p0, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 899027
    goto :goto_0

    .line 899028
    :cond_7
    iget-object v2, p1, Lcom/facebook/componentscript/components/CSImage$CSImageImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
