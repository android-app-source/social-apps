.class public final Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6JU;

.field public final synthetic b:Landroid/os/Handler;

.field public final synthetic c:LX/6LE;


# direct methods
.method public constructor <init>(LX/6LE;LX/6JU;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1078087
    iput-object p1, p0, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$1;->c:LX/6LE;

    iput-object p2, p0, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$1;->a:LX/6JU;

    iput-object p3, p0, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$1;->b:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1078066
    iget-object v0, p0, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$1;->c:LX/6LE;

    iget-object v1, p0, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$1;->a:LX/6JU;

    iget-object v2, p0, Lcom/facebook/cameracore/mediapipeline/recorder/SurfaceVideoEncoderImpl$1;->b:Landroid/os/Handler;

    .line 1078067
    iget-object v3, v0, LX/6LE;->e:LX/6LB;

    sget-object v4, LX/6LB;->STOPPED:LX/6LB;

    if-eq v3, v4, :cond_0

    .line 1078068
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Must only call prepare() on a stopped SurfaceVideoEncoder. Current state is: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, LX/6LE;->e:LX/6LB;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2, v3}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    .line 1078069
    :goto_0
    return-void

    .line 1078070
    :cond_0
    :try_start_0
    iget-object v3, v0, LX/6LE;->d:LX/6LG;

    .line 1078071
    iget-object v4, v3, LX/6LG;->e:Ljava/lang/String;

    .line 1078072
    const-string v5, "high"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    move v4, v5

    .line 1078073
    if-eqz v4, :cond_1

    .line 1078074
    const/4 v4, 0x1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-static {v3, v4}, LX/6LE;->a(LX/6LG;Z)Landroid/media/MediaFormat;

    move-result-object v4

    .line 1078075
    const-string v5, "video/avc"

    invoke-static {v5, v4}, LX/6L6;->a(Ljava/lang/String;Landroid/media/MediaFormat;)Landroid/media/MediaCodec;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    move-result-object v4

    .line 1078076
    :goto_1
    move-object v3, v4

    .line 1078077
    iput-object v3, v0, LX/6LE;->g:Landroid/media/MediaCodec;

    .line 1078078
    iget-object v3, v0, LX/6LE;->g:Landroid/media/MediaCodec;

    invoke-virtual {v3}, Landroid/media/MediaCodec;->createInputSurface()Landroid/view/Surface;

    move-result-object v3

    iput-object v3, v0, LX/6LE;->f:Landroid/view/Surface;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1078079
    sget-object v3, LX/6LB;->PREPARED:LX/6LB;

    iput-object v3, v0, LX/6LE;->e:LX/6LB;

    .line 1078080
    invoke-static {v1, v2}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;)V

    goto :goto_0

    .line 1078081
    :catch_0
    move-exception v3

    .line 1078082
    invoke-static {v1, v2, v3}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1078083
    :catch_1
    move-exception v4

    .line 1078084
    sget-object v5, LX/6LE;->a:Ljava/lang/String;

    const-string p0, "Error getting video encoder for high profile. Fall back to baseline"

    invoke-static {v5, p0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1078085
    :cond_1
    const/4 v4, 0x0

    invoke-static {v3, v4}, LX/6LE;->a(LX/6LG;Z)Landroid/media/MediaFormat;

    move-result-object v4

    .line 1078086
    const-string v5, "video/avc"

    invoke-static {v5, v4}, LX/6L6;->a(Ljava/lang/String;Landroid/media/MediaFormat;)Landroid/media/MediaCodec;

    move-result-object v4

    goto :goto_1
.end method
