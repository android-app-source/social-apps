.class public final Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6JU;

.field public final synthetic b:Landroid/os/Handler;

.field public final synthetic c:LX/6L1;


# direct methods
.method public constructor <init>(LX/6L1;LX/6JU;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1077787
    iput-object p1, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$1;->c:LX/6L1;

    iput-object p2, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$1;->a:LX/6JU;

    iput-object p3, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$1;->b:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1077788
    iget-object v0, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$1;->c:LX/6L1;

    iget-object v1, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$1;->a:LX/6JU;

    iget-object v2, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$1;->b:Landroid/os/Handler;

    .line 1077789
    iget-object v3, v0, LX/6L1;->d:LX/6Kx;

    sget-object v4, LX/6Kx;->STOPPED:LX/6Kx;

    if-eq v3, v4, :cond_0

    .line 1077790
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Must only call prepare() on a stopped AudioEncoder. Current state is: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, LX/6L1;->d:LX/6Kx;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2, v3}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    .line 1077791
    :goto_0
    return-void

    .line 1077792
    :cond_0
    :try_start_0
    const-string v3, "audio/mp4a-latm"

    iget-object v4, v0, LX/6L1;->c:LX/6Kz;

    .line 1077793
    const-string v5, "audio/mp4a-latm"

    iget v6, v4, LX/6Kz;->b:I

    iget p0, v4, LX/6Kz;->c:I

    invoke-static {v5, v6, p0}, Landroid/media/MediaFormat;->createAudioFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v5

    .line 1077794
    const-string v6, "aac-profile"

    const/4 p0, 0x1

    invoke-virtual {v5, v6, p0}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1077795
    const-string v6, "bitrate"

    iget p0, v4, LX/6Kz;->a:I

    invoke-virtual {v5, v6, p0}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1077796
    iget v6, v4, LX/6Kz;->d:I

    if-lez v6, :cond_1

    .line 1077797
    const-string v6, "max-input-size"

    iget p0, v4, LX/6Kz;->d:I

    invoke-virtual {v5, v6, p0}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1077798
    :cond_1
    move-object v4, v5

    .line 1077799
    invoke-static {v3, v4}, LX/6L6;->a(Ljava/lang/String;Landroid/media/MediaFormat;)Landroid/media/MediaCodec;

    move-result-object v3

    iput-object v3, v0, LX/6L1;->e:Landroid/media/MediaCodec;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1077800
    sget-object v3, LX/6Kx;->PREPARED:LX/6Kx;

    iput-object v3, v0, LX/6L1;->d:LX/6Kx;

    .line 1077801
    invoke-static {v1, v2}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;)V

    goto :goto_0

    .line 1077802
    :catch_0
    move-exception v3

    .line 1077803
    invoke-static {v1, v2, v3}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
