.class public final Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6JU;

.field public final synthetic b:Landroid/os/Handler;

.field public final synthetic c:LX/6L1;


# direct methods
.method public constructor <init>(LX/6L1;LX/6JU;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1077813
    iput-object p1, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$2;->c:LX/6L1;

    iput-object p2, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$2;->a:LX/6JU;

    iput-object p3, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$2;->b:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1077804
    iget-object v0, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$2;->c:LX/6L1;

    iget-object v1, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$2;->a:LX/6JU;

    iget-object v2, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$2;->b:Landroid/os/Handler;

    .line 1077805
    iget-object v3, v0, LX/6L1;->d:LX/6Kx;

    sget-object v4, LX/6Kx;->PREPARED:LX/6Kx;

    if-eq v3, v4, :cond_0

    .line 1077806
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p0, "prepare() must be called before starting audio encoding. Current state is: "

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p0, v0, LX/6L1;->d:LX/6Kx;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2, v3}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    .line 1077807
    :goto_0
    return-void

    .line 1077808
    :cond_0
    :try_start_0
    iget-object v3, v0, LX/6L1;->e:Landroid/media/MediaCodec;

    invoke-virtual {v3}, Landroid/media/MediaCodec;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1077809
    sget-object v3, LX/6Kx;->STARTED:LX/6Kx;

    iput-object v3, v0, LX/6L1;->d:LX/6Kx;

    .line 1077810
    invoke-static {v1, v2}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;)V

    goto :goto_0

    .line 1077811
    :catch_0
    move-exception v3

    .line 1077812
    invoke-static {v1, v2, v3}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
