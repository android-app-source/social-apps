.class public final Lcom/facebook/cameracore/mediapipeline/recorder/AudioRecorder$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6L3;


# direct methods
.method public constructor <init>(LX/6L3;)V
    .locals 0

    .prologue
    .line 1077878
    iput-object p1, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioRecorder$1;->a:LX/6L3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 1077879
    iget-object v0, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioRecorder$1;->a:LX/6L3;

    iget v0, v0, LX/6L3;->d:I

    new-array v0, v0, [B

    .line 1077880
    :goto_0
    iget-object v1, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioRecorder$1;->a:LX/6L3;

    iget-object v1, v1, LX/6L3;->e:LX/6L2;

    sget-object v2, LX/6L2;->STARTED:LX/6L2;

    if-ne v1, v2, :cond_0

    .line 1077881
    iget-object v1, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioRecorder$1;->a:LX/6L3;

    iget-object v1, v1, LX/6L3;->f:Landroid/media/AudioRecord;

    const/4 v2, 0x0

    array-length v3, v0

    invoke-virtual {v1, v0, v2, v3}, Landroid/media/AudioRecord;->read([BII)I

    move-result v1

    .line 1077882
    iget-object v2, p0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioRecorder$1;->a:LX/6L3;

    iget-object v2, v2, LX/6L3;->a:LX/6K0;

    .line 1077883
    iget-object v4, v2, LX/6K0;->a:LX/6K6;

    .line 1077884
    iget-object v5, v4, LX/6K6;->m:LX/6Jc;

    if-nez v5, :cond_1

    .line 1077885
    :goto_1
    goto :goto_0

    .line 1077886
    :cond_0
    return-void

    .line 1077887
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v11

    const-wide/16 v13, 0x3e8

    div-long/2addr v11, v13

    move-wide v5, v11

    .line 1077888
    iget-wide v7, v4, LX/6K6;->n:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-nez v7, :cond_2

    .line 1077889
    iput-wide v5, v4, LX/6K6;->n:J

    .line 1077890
    :cond_2
    iget-object v7, v4, LX/6K6;->m:LX/6Jc;

    iget-wide v9, v4, LX/6K6;->n:J

    sub-long/2addr v5, v9

    .line 1077891
    iget-object v8, v7, LX/6Jc;->g:LX/6L1;

    if-nez v8, :cond_3

    .line 1077892
    :goto_2
    goto :goto_1

    .line 1077893
    :cond_3
    iget-object v8, v7, LX/6Jc;->g:LX/6L1;

    invoke-virtual {v8, v0, v1, v5, v6}, LX/6L1;->a([BIJ)V

    goto :goto_2
.end method
