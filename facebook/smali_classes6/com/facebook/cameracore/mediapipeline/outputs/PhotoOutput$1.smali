.class public final Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:Landroid/graphics/Bitmap;

.field public final synthetic c:LX/6Jm;

.field public final synthetic d:LX/6Kt;


# direct methods
.method public constructor <init>(LX/6Kt;Ljava/io/File;Landroid/graphics/Bitmap;LX/6Jm;)V
    .locals 0

    .prologue
    .line 1077570
    iput-object p1, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->d:LX/6Kt;

    iput-object p2, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->a:Ljava/io/File;

    iput-object p3, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->b:Landroid/graphics/Bitmap;

    iput-object p4, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->c:LX/6Jm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1077571
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->a:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1077572
    new-instance v1, Ljava/io/BufferedOutputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1077573
    iget-object v2, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->b:Landroid/graphics/Bitmap;

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x5a

    invoke-virtual {v2, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1077574
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V

    .line 1077575
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V

    .line 1077576
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 1077577
    iget-object v0, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->c:LX/6Jm;

    if-eqz v0, :cond_0

    .line 1077578
    iget-object v0, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->c:LX/6Jm;

    .line 1077579
    iget-object v1, v0, LX/6Jm;->c:LX/6Jt;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, LX/6Jt;->c(I)V

    .line 1077580
    iget-object v1, v0, LX/6Jm;->c:LX/6Jt;

    iget-object v1, v1, LX/6Jt;->b:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/cameracore/capturecoordinator/CaptureCoordinator$2$1;

    invoke-direct {v2, v0}, Lcom/facebook/cameracore/capturecoordinator/CaptureCoordinator$2$1;-><init>(LX/6Jm;)V

    const v3, -0x4af62f96

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1077581
    iget-object v1, v0, LX/6Jm;->c:LX/6Jt;

    iget-object v1, v1, LX/6Jt;->c:LX/6Km;

    iget-object v2, v0, LX/6Jm;->b:Ljava/util/List;

    invoke-virtual {v1, v2}, LX/6Km;->b(Ljava/util/List;)V

    .line 1077582
    iget-object v1, v0, LX/6Jm;->c:LX/6Jt;

    const/4 v2, 0x0

    .line 1077583
    iput-boolean v2, v1, LX/6Jt;->r:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1077584
    :cond_0
    iget-object v0, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 1077585
    iget-object v0, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1077586
    :cond_1
    :goto_0
    return-void

    .line 1077587
    :catch_0
    move-exception v0

    .line 1077588
    :try_start_1
    sget-object v1, LX/6Kt;->a:Ljava/lang/String;

    const-string v2, "Unable to create FileOutputStream"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1077589
    iget-object v1, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->c:LX/6Jm;

    if-eqz v1, :cond_2

    .line 1077590
    iget-object v1, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->c:LX/6Jm;

    invoke-virtual {v1, v0}, LX/6Jm;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1077591
    :cond_2
    iget-object v0, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 1077592
    iget-object v0, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 1077593
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->b:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    .line 1077594
    iget-object v1, p0, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    throw v0
.end method
