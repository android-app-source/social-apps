.class public final Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6JR;

.field public final synthetic b:LX/6JU;

.field public final synthetic c:I

.field public final synthetic d:LX/6K6;


# direct methods
.method public constructor <init>(LX/6K6;LX/6JR;LX/6JU;I)V
    .locals 0

    .prologue
    .line 1076130
    iput-object p1, p0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$2;->d:LX/6K6;

    iput-object p2, p0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$2;->a:LX/6JR;

    iput-object p3, p0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$2;->b:LX/6JU;

    iput p4, p0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$2;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 1076131
    iget-object v0, p0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$2;->d:LX/6K6;

    iget-object v1, p0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$2;->a:LX/6JR;

    iget-object v2, p0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$2;->b:LX/6JU;

    iget v3, p0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImpl$2;->c:I

    const/4 p0, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1076132
    iget-object v4, v0, LX/6K6;->q:LX/6K9;

    sget-object v5, LX/6K9;->STOPPED:LX/6K9;

    if-eq v4, v5, :cond_0

    .line 1076133
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "prepareRecordingVideo can only be called when stopped. Current state is: %s"

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, v0, LX/6K6;->q:LX/6K9;

    invoke-virtual {v7}, LX/6K9;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v4}, LX/6JU;->a(Ljava/lang/Throwable;)V

    .line 1076134
    invoke-static {v0, v8}, LX/6K6;->a(LX/6K6;Z)V

    .line 1076135
    :goto_0
    return-void

    .line 1076136
    :cond_0
    iget v4, v1, LX/6JR;->a:I

    rem-int/lit8 v4, v4, 0x10

    if-nez v4, :cond_1

    iget v4, v1, LX/6JR;->b:I

    rem-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_2

    .line 1076137
    :cond_1
    sget-object v4, LX/6K6;->a:Ljava/lang/String;

    const-string v5, "The input size {%dx%d} is not a multiple of 16"

    new-array v6, p0, [Ljava/lang/Object;

    iget v7, v1, LX/6JR;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, v1, LX/6JR;->b:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1076138
    :cond_2
    if-eqz v3, :cond_3

    if-ne v3, p0, :cond_4

    .line 1076139
    :cond_3
    new-instance v4, LX/6JR;

    iget v5, v1, LX/6JR;->b:I

    iget v6, v1, LX/6JR;->a:I

    invoke-direct {v4, v5, v6}, LX/6JR;-><init>(II)V

    iput-object v4, v0, LX/6K6;->o:LX/6JR;

    .line 1076140
    :goto_1
    new-instance v4, LX/6LF;

    invoke-direct {v4}, LX/6LF;-><init>()V

    move-object v4, v4

    .line 1076141
    iget-object v5, v0, LX/6K6;->o:LX/6JR;

    iget v5, v5, LX/6JR;->a:I

    .line 1076142
    iput v5, v4, LX/6LF;->a:I

    .line 1076143
    move-object v4, v4

    .line 1076144
    iget-object v5, v0, LX/6K6;->o:LX/6JR;

    iget v5, v5, LX/6JR;->b:I

    .line 1076145
    iput v5, v4, LX/6LF;->b:I

    .line 1076146
    move-object v4, v4

    .line 1076147
    iget-object v5, v0, LX/6K6;->d:LX/6KK;

    .line 1076148
    iget-object v6, v5, LX/6KK;->a:Ljava/lang/String;

    move-object v5, v6

    .line 1076149
    iput-object v5, v4, LX/6LF;->e:Ljava/lang/String;

    .line 1076150
    move-object v4, v4

    .line 1076151
    new-instance v5, LX/6LG;

    invoke-direct {v5, v4}, LX/6LG;-><init>(LX/6LF;)V

    move-object v4, v5

    .line 1076152
    const/4 v9, 0x0

    .line 1076153
    iget-object v5, v0, LX/6K6;->q:LX/6K9;

    sget-object v6, LX/6K9;->STOPPED:LX/6K9;

    if-eq v5, v6, :cond_5

    .line 1076154
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "prepareRecordingVideo can only be called when stopped. Current state is: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, v0, LX/6K6;->q:LX/6K9;

    invoke-virtual {v8}, LX/6K9;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v5}, LX/6JU;->a(Ljava/lang/Throwable;)V

    .line 1076155
    invoke-static {v0, v9}, LX/6K6;->a(LX/6K6;Z)V

    .line 1076156
    :goto_2
    goto :goto_0

    .line 1076157
    :cond_4
    iput-object v1, v0, LX/6K6;->o:LX/6JR;

    goto :goto_1

    .line 1076158
    :cond_5
    sget-object v5, LX/6K9;->PREPARE_STARTED:LX/6K9;

    iput-object v5, v0, LX/6K6;->q:LX/6K9;

    .line 1076159
    new-instance v5, Landroid/os/HandlerThread;

    const-string v6, "AudioRecordingThread"

    invoke-direct {v5, v6}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v5, v0, LX/6K6;->g:Landroid/os/HandlerThread;

    .line 1076160
    new-instance v5, Landroid/os/HandlerThread;

    const-string v6, "VideoRecordingThread"

    invoke-direct {v5, v6}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v5, v0, LX/6K6;->h:Landroid/os/HandlerThread;

    .line 1076161
    iget-object v5, v0, LX/6K6;->g:Landroid/os/HandlerThread;

    invoke-virtual {v5}, Landroid/os/HandlerThread;->start()V

    .line 1076162
    iget-object v5, v0, LX/6K6;->h:Landroid/os/HandlerThread;

    invoke-virtual {v5}, Landroid/os/HandlerThread;->start()V

    .line 1076163
    new-instance v5, Landroid/os/Handler;

    iget-object v6, v0, LX/6K6;->g:Landroid/os/HandlerThread;

    invoke-virtual {v6}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v5, v0, LX/6K6;->e:Landroid/os/Handler;

    .line 1076164
    new-instance v5, Landroid/os/Handler;

    iget-object v6, v0, LX/6K6;->h:Landroid/os/HandlerThread;

    invoke-virtual {v6}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v5, v0, LX/6K6;->f:Landroid/os/Handler;

    .line 1076165
    new-instance v5, LX/6L4;

    invoke-direct {v5}, LX/6L4;-><init>()V

    move-object v5, v5

    .line 1076166
    new-instance v6, LX/6L5;

    invoke-direct {v6, v5}, LX/6L5;-><init>(LX/6L4;)V

    move-object v6, v6

    .line 1076167
    iget-object v5, v0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/6Jt;

    const/16 v7, 0xe

    invoke-virtual {v5, v7}, LX/6Jt;->b(I)V

    .line 1076168
    iget-object v5, v0, LX/6K6;->o:LX/6JR;

    if-nez v5, :cond_6

    .line 1076169
    new-instance v5, LX/6JR;

    iget v7, v4, LX/6LG;->a:I

    iget v8, v4, LX/6LG;->b:I

    invoke-direct {v5, v7, v8}, LX/6JR;-><init>(II)V

    iput-object v5, v0, LX/6K6;->o:LX/6JR;

    .line 1076170
    :cond_6
    new-instance v5, LX/6L3;

    iget-object v7, v0, LX/6K6;->e:Landroid/os/Handler;

    iget-object v8, v0, LX/6K6;->t:LX/6K0;

    invoke-direct {v5, v6, v7, v8}, LX/6L3;-><init>(LX/6L5;Landroid/os/Handler;LX/6K0;)V

    iput-object v5, v0, LX/6K6;->i:LX/6L3;

    .line 1076171
    new-instance v5, LX/6Ky;

    invoke-direct {v5}, LX/6Ky;-><init>()V

    move-object v5, v5

    .line 1076172
    iget-object v6, v0, LX/6K6;->i:LX/6L3;

    .line 1076173
    iget v7, v6, LX/6L3;->d:I

    move v6, v7

    .line 1076174
    iput v6, v5, LX/6Ky;->d:I

    .line 1076175
    move-object v5, v5

    .line 1076176
    new-instance v6, LX/6Kz;

    invoke-direct {v6, v5}, LX/6Kz;-><init>(LX/6Ky;)V

    move-object v5, v6

    .line 1076177
    new-instance v6, LX/6Jc;

    iget-object v7, v0, LX/6K6;->e:Landroid/os/Handler;

    iget-object v8, v0, LX/6K6;->f:Landroid/os/Handler;

    invoke-direct {v6, v5, v4, v7, v8}, LX/6Jc;-><init>(LX/6Kz;LX/6LG;Landroid/os/Handler;Landroid/os/Handler;)V

    iput-object v6, v0, LX/6K6;->m:LX/6Jc;

    .line 1076178
    iget-object v5, v0, LX/6K6;->i:LX/6L3;

    new-instance v6, LX/6K1;

    invoke-direct {v6, v0, v2}, LX/6K1;-><init>(LX/6K6;LX/6JU;)V

    iget-object v7, v0, LX/6K6;->b:Landroid/os/Handler;

    .line 1076179
    invoke-static {v5, v7}, LX/6L3;->a(LX/6L3;Landroid/os/Handler;)V

    .line 1076180
    iget-object v8, v5, LX/6L3;->b:Landroid/os/Handler;

    new-instance v9, Lcom/facebook/cameracore/mediapipeline/recorder/AudioRecorder$2;

    invoke-direct {v9, v5, v6, v7}, Lcom/facebook/cameracore/mediapipeline/recorder/AudioRecorder$2;-><init>(LX/6L3;LX/6JU;Landroid/os/Handler;)V

    const v4, 0x6a5fc45d

    invoke-static {v8, v9, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1076181
    iget-object v5, v0, LX/6K6;->m:LX/6Jc;

    new-instance v6, LX/6K2;

    invoke-direct {v6, v0, v2}, LX/6K2;-><init>(LX/6K6;LX/6JU;)V

    iget-object v7, v0, LX/6K6;->b:Landroid/os/Handler;

    .line 1076182
    iget-object v8, v5, LX/6Jc;->g:LX/6L1;

    if-nez v8, :cond_7

    iget-object v8, v5, LX/6Jc;->h:LX/6LC;

    if-eqz v8, :cond_8

    .line 1076183
    :cond_7
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "Cannot call prepare() again until stopping"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v7, v8}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    .line 1076184
    :goto_3
    goto/16 :goto_2

    .line 1076185
    :cond_8
    new-instance v8, LX/6Jb;

    const/4 v9, 0x2

    invoke-direct {v8, v9}, LX/6Jb;-><init>(I)V

    .line 1076186
    iget-object v9, v5, LX/6Jc;->a:LX/6Kz;

    iget-object p0, v5, LX/6Jc;->m:LX/6JS;

    iget-object v1, v5, LX/6Jc;->c:Landroid/os/Handler;

    .line 1076187
    new-instance v0, LX/6L1;

    invoke-direct {v0, v9, p0, v1}, LX/6L1;-><init>(LX/6Kz;LX/6JS;Landroid/os/Handler;)V

    move-object v9, v0

    .line 1076188
    iput-object v9, v5, LX/6Jc;->g:LX/6L1;

    .line 1076189
    iget-object v9, v5, LX/6Jc;->g:LX/6L1;

    new-instance p0, LX/6JV;

    invoke-direct {p0, v5, v8, v6, v7}, LX/6JV;-><init>(LX/6Jc;LX/6Jb;LX/6JU;Landroid/os/Handler;)V

    iget-object v1, v5, LX/6Jc;->e:Landroid/os/Handler;

    .line 1076190
    new-instance v0, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v0}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v0, v9, LX/6L1;->g:Landroid/media/MediaCodec$BufferInfo;

    .line 1076191
    iget-object v0, v9, LX/6L1;->b:Landroid/os/Handler;

    new-instance v4, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$1;

    invoke-direct {v4, v9, p0, v1}, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$1;-><init>(LX/6L1;LX/6JU;Landroid/os/Handler;)V

    const v2, 0x27e45b1c

    invoke-static {v0, v4, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1076192
    iget-object v9, v5, LX/6Jc;->b:LX/6LG;

    iget-object p0, v5, LX/6Jc;->n:LX/6JT;

    iget-object v1, v5, LX/6Jc;->d:Landroid/os/Handler;

    .line 1076193
    new-instance v0, LX/6LE;

    invoke-direct {v0, v9, p0, v1}, LX/6LE;-><init>(LX/6LG;LX/6JT;Landroid/os/Handler;)V

    move-object v9, v0

    .line 1076194
    iput-object v9, v5, LX/6Jc;->h:LX/6LC;

    .line 1076195
    iget-object v9, v5, LX/6Jc;->h:LX/6LC;

    new-instance p0, LX/6JW;

    invoke-direct {p0, v5, v8, v6, v7}, LX/6JW;-><init>(LX/6Jc;LX/6Jb;LX/6JU;Landroid/os/Handler;)V

    iget-object v8, v5, LX/6Jc;->e:Landroid/os/Handler;

    invoke-interface {v9, p0, v8}, LX/6LC;->a(LX/6JU;Landroid/os/Handler;)V

    goto :goto_3
.end method
