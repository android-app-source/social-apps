.class public final Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImplNative$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6JU;

.field public final synthetic b:LX/6K8;


# direct methods
.method public constructor <init>(LX/6K8;LX/6JU;)V
    .locals 0

    .prologue
    .line 1076370
    iput-object p1, p0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImplNative$1;->b:LX/6K8;

    iput-object p2, p0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImplNative$1;->a:LX/6JU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1076371
    iget-object v0, p0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImplNative$1;->b:LX/6K8;

    iget-object v1, p0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImplNative$1;->a:LX/6JU;

    .line 1076372
    iget-object v2, v0, LX/6K8;->d:LX/6K9;

    sget-object v3, LX/6K9;->STOPPED:LX/6K9;

    if-eq v2, v3, :cond_0

    .line 1076373
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string p0, "prepareRecordingVideo can only be called when stopped. Current state is: "

    invoke-direct {v3, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p0, v0, LX/6K8;->d:LX/6K9;

    invoke-virtual {p0}, LX/6K9;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, LX/6JU;->a(Ljava/lang/Throwable;)V

    .line 1076374
    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/6K8;->a$redex0(LX/6K8;Z)V

    .line 1076375
    :goto_0
    return-void

    .line 1076376
    :cond_0
    sget-object v2, LX/6K9;->PREPARED:LX/6K9;

    iput-object v2, v0, LX/6K8;->d:LX/6K9;

    .line 1076377
    iget-object v2, v0, LX/6K8;->a:Landroid/os/Handler;

    invoke-static {v1, v2}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;)V

    .line 1076378
    const/4 v2, 0x1

    invoke-static {v0, v2}, LX/6K8;->a$redex0(LX/6K8;Z)V

    goto :goto_0
.end method
