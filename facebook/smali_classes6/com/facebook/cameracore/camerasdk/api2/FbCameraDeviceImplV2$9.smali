.class public final Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6Ik;

.field public final synthetic b:LX/6Iu;


# direct methods
.method public constructor <init>(LX/6Iu;LX/6Ik;)V
    .locals 0

    .prologue
    .line 1074643
    iput-object p1, p0, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$9;->b:LX/6Iu;

    iput-object p2, p0, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$9;->a:LX/6Ik;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1074644
    iget-object v0, p0, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$9;->b:LX/6Iu;

    iget-object v1, p0, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$9;->a:LX/6Ik;

    .line 1074645
    invoke-virtual {v0}, LX/6Iu;->c()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1074646
    :goto_0
    iget-object v0, p0, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$9;->b:LX/6Iu;

    const/4 v3, 0x0

    .line 1074647
    iput-object v3, v0, LX/6Iu;->x:LX/6JB;

    .line 1074648
    iput-object v3, v0, LX/6Iu;->y:LX/6JC;

    .line 1074649
    iget-object v1, v0, LX/6Iu;->n:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 1074650
    iget-object v1, v0, LX/6Iu;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Surface;

    .line 1074651
    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    goto :goto_1

    .line 1074652
    :cond_0
    iput-object v3, v0, LX/6Iu;->n:Ljava/util/List;

    .line 1074653
    :cond_1
    iput-object v3, v0, LX/6Iu;->M:LX/6JG;

    .line 1074654
    iput-object v3, v0, LX/6Iu;->m:Ljava/io/File;

    .line 1074655
    iget-object v1, v0, LX/6Iu;->l:Landroid/media/ImageReader;

    if-eqz v1, :cond_2

    .line 1074656
    iget-object v1, v0, LX/6Iu;->l:Landroid/media/ImageReader;

    invoke-virtual {v1}, Landroid/media/ImageReader;->close()V

    .line 1074657
    :cond_2
    iput-object v3, v0, LX/6Iu;->l:Landroid/media/ImageReader;

    .line 1074658
    invoke-static {v0}, LX/6Iu;->o(LX/6Iu;)V

    .line 1074659
    iget-object v1, v0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    if-eqz v1, :cond_3

    invoke-virtual {v0}, LX/6Iu;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1074660
    iget-object v1, v0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->stop()V

    .line 1074661
    :cond_3
    iput-object v3, v0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    .line 1074662
    iput-object v3, v0, LX/6Iu;->O:LX/6JG;

    .line 1074663
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/6Iu;->r:Z

    .line 1074664
    iput-object v3, v0, LX/6Iu;->z:LX/6JC;

    .line 1074665
    return-void

    .line 1074666
    :cond_4
    const/4 v2, 0x7

    invoke-static {v0, v2}, LX/6Iu;->b$redex0(LX/6Iu;I)V

    .line 1074667
    invoke-static {v0}, LX/6Iu;->D(LX/6Iu;)V

    .line 1074668
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/6Iu;->f:Z

    .line 1074669
    iget-object v2, v0, LX/6Iu;->i:Landroid/hardware/camera2/CameraDevice;

    invoke-static {v2}, LX/0J3;->a(Landroid/hardware/camera2/CameraDevice;)V

    .line 1074670
    const/4 v2, 0x0

    iput-object v2, v0, LX/6Iu;->i:Landroid/hardware/camera2/CameraDevice;

    .line 1074671
    iget-object v2, v0, LX/6Iu;->e:LX/6Iv;

    new-instance v3, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$13;

    invoke-direct {v3, v0, v1}, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$13;-><init>(LX/6Iu;LX/6Ik;)V

    invoke-virtual {v2, v3}, LX/6Iv;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
