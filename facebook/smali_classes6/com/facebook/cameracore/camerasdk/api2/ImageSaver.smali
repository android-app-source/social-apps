.class public Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field private final a:Landroid/media/Image;

.field private final b:Ljava/io/File;

.field private final c:LX/6Iv;

.field public final d:LX/6JG;


# direct methods
.method public constructor <init>(Landroid/media/Image;Ljava/io/File;LX/6Iv;LX/6JG;)V
    .locals 0

    .prologue
    .line 1075084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075085
    iput-object p1, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->a:Landroid/media/Image;

    .line 1075086
    iput-object p2, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->b:Ljava/io/File;

    .line 1075087
    iput-object p3, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->c:LX/6Iv;

    .line 1075088
    iput-object p4, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->d:LX/6JG;

    .line 1075089
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1075090
    iget-object v0, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->a:Landroid/media/Image;

    invoke-virtual {v0}, Landroid/media/Image;->getPlanes()[Landroid/media/Image$Plane;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/media/Image$Plane;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1075091
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    new-array v3, v1, [B

    .line 1075092
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 1075093
    const/4 v2, 0x0

    .line 1075094
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v0, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->b:Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1075095
    :try_start_1
    invoke-virtual {v1, v3}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1075096
    iget-object v0, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->a:Landroid/media/Image;

    invoke-virtual {v0}, Landroid/media/Image;->close()V

    .line 1075097
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 1075098
    iget-object v0, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->c:LX/6Iv;

    new-instance v1, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$2;

    invoke-direct {v1, p0}, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$2;-><init>(Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;)V

    invoke-virtual {v0, v1}, LX/6Iv;->a(Ljava/lang/Runnable;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1075099
    :cond_0
    :goto_0
    return-void

    .line 1075100
    :catch_0
    move-exception v0

    .line 1075101
    iget-object v1, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->c:LX/6Iv;

    new-instance v2, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$3;

    invoke-direct {v2, p0, v0}, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$3;-><init>(Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;Ljava/io/IOException;)V

    invoke-virtual {v1, v2}, LX/6Iv;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1075102
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 1075103
    :goto_1
    :try_start_3
    iget-object v2, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->c:LX/6Iv;

    new-instance v3, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$1;

    invoke-direct {v3, p0, v0}, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$1;-><init>(Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;Ljava/io/IOException;)V

    invoke-virtual {v2, v3}, LX/6Iv;->a(Ljava/lang/Runnable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1075104
    iget-object v0, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->a:Landroid/media/Image;

    invoke-virtual {v0}, Landroid/media/Image;->close()V

    .line 1075105
    if-eqz v1, :cond_0

    .line 1075106
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 1075107
    iget-object v0, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->c:LX/6Iv;

    new-instance v1, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$2;

    invoke-direct {v1, p0}, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$2;-><init>(Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;)V

    invoke-virtual {v0, v1}, LX/6Iv;->a(Ljava/lang/Runnable;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1075108
    :catch_2
    move-exception v0

    .line 1075109
    iget-object v1, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->c:LX/6Iv;

    new-instance v2, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$3;

    invoke-direct {v2, p0, v0}, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$3;-><init>(Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;Ljava/io/IOException;)V

    invoke-virtual {v1, v2}, LX/6Iv;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1075110
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    iget-object v2, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->a:Landroid/media/Image;

    invoke-virtual {v2}, Landroid/media/Image;->close()V

    .line 1075111
    if-eqz v1, :cond_1

    .line 1075112
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 1075113
    iget-object v1, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->c:LX/6Iv;

    new-instance v2, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$2;

    invoke-direct {v2, p0}, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$2;-><init>(Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;)V

    invoke-virtual {v1, v2}, LX/6Iv;->a(Ljava/lang/Runnable;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1075114
    :cond_1
    :goto_3
    throw v0

    .line 1075115
    :catch_3
    move-exception v1

    .line 1075116
    iget-object v2, p0, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;->c:LX/6Iv;

    new-instance v3, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$3;

    invoke-direct {v3, p0, v1}, Lcom/facebook/cameracore/camerasdk/api2/ImageSaver$3;-><init>(Lcom/facebook/cameracore/camerasdk/api2/ImageSaver;Ljava/io/IOException;)V

    invoke-virtual {v2, v3}, LX/6Iv;->a(Ljava/lang/Runnable;)V

    goto :goto_3

    .line 1075117
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 1075118
    :catch_4
    move-exception v0

    goto :goto_1
.end method
