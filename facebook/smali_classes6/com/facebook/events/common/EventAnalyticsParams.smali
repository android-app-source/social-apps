.class public Lcom/facebook/events/common/EventAnalyticsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/events/common/EventAnalyticsParams;


# instance fields
.field public final b:Lcom/facebook/events/common/EventActionContext;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 907939
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    new-instance v1, Lcom/facebook/events/common/EventActionContext;

    sget-object v2, Lcom/facebook/events/common/ActionSource;->DASHBOARD:Lcom/facebook/events/common/ActionSource;

    sget-object v3, Lcom/facebook/events/common/ActionSource;->UNKNOWN:Lcom/facebook/events/common/ActionSource;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/events/common/EventActionContext;-><init>(Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionSource;Z)V

    invoke-direct {v0, v1}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;)V

    sput-object v0, Lcom/facebook/events/common/EventAnalyticsParams;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 907940
    new-instance v0, LX/5O5;

    invoke-direct {v0}, LX/5O5;-><init>()V

    sput-object v0, Lcom/facebook/events/common/EventAnalyticsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 907933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 907934
    const-class v0, Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/common/EventActionContext;

    iput-object v0, p0, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 907935
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    .line 907936
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    .line 907937
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    .line 907938
    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventActionContext;)V
    .locals 1

    .prologue
    .line 907931
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;)V

    .line 907932
    return-void
.end method

.method private constructor <init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 907927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 907928
    if-nez p1, :cond_0

    sget-object p1, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    :cond_0
    iput-object p1, p0, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 907929
    iput-object p2, p0, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    .line 907930
    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 907941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 907942
    if-nez p1, :cond_0

    sget-object p1, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    :cond_0
    iput-object p1, p0, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 907943
    iput-object p2, p0, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    .line 907944
    iput-object p3, p0, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    .line 907945
    iput-object p4, p0, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    .line 907946
    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 907905
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 907906
    if-nez p1, :cond_0

    sget-object p1, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    :cond_0
    iput-object p1, p0, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 907907
    iput-object p2, p0, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    .line 907908
    iput-object p4, p0, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    .line 907909
    iput-object p3, p0, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    .line 907910
    iput-object p5, p0, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    .line 907911
    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionSource;)V
    .locals 1

    .prologue
    .line 907921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 907922
    iget-object v0, p1, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {v0, p2}, Lcom/facebook/events/common/EventActionContext;->a(Lcom/facebook/events/common/ActionSource;)Lcom/facebook/events/common/EventActionContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 907923
    iget-object v0, p1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    .line 907924
    iget-object v0, p1, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    .line 907925
    iget-object v0, p1, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    .line 907926
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/common/ActionSource;)Lcom/facebook/events/common/EventAnalyticsParams;
    .locals 11

    .prologue
    .line 907912
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, p0, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 907913
    new-instance v5, Lcom/facebook/events/common/EventActionContext;

    iget-object v7, v1, Lcom/facebook/events/common/EventActionContext;->g:Lcom/facebook/events/common/ActionMechanism;

    iget-object v8, v1, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    iget-object v9, v1, Lcom/facebook/events/common/EventActionContext;->h:Lcom/facebook/events/common/ActionMechanism;

    iget-boolean v10, v1, Lcom/facebook/events/common/EventActionContext;->i:Z

    move-object v6, p1

    invoke-direct/range {v5 .. v10}, Lcom/facebook/events/common/EventActionContext;-><init>(Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionMechanism;Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionMechanism;Z)V

    move-object v1, v5

    .line 907914
    iget-object v2, p0, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 907915
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 907916
    iget-object v0, p0, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 907917
    iget-object v0, p0, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 907918
    iget-object v0, p0, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 907919
    iget-object v0, p0, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 907920
    return-void
.end method
