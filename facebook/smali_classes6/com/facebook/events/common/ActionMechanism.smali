.class public final enum Lcom/facebook/events/common/ActionMechanism;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/events/common/ActionMechanism;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/events/common/ActionMechanism;

.field public static final enum BUY_TICKETS_CTA:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum BUY_TICKETS_FLOW:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum CALENDAR_TAB:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum CALENDAR_TAB_DOWN_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum CALENDAR_TAB_EVENT:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum CALENDAR_TAB_UP_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum CANCEL_EVENT_FLOW:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum CHECKIN_COMPOSER:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum COPY_EVENT:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum COPY_EVENT_INVITES:Lcom/facebook/events/common/ActionMechanism;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/common/ActionMechanism;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DASHBOARD_FAB:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum DASHBOARD_HEADER:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum DASHBOARD_ROW_GUEST_STATUS:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum DASHBOARD_ROW_INLINE_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum DASHBOARD_SUBSCRIPTIONS_CARD:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum DASHBOARD_SUGGESTIONS_CARD:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENTS_LIST:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENTS_SUGGESTION_UNIT:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_CHAINING:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_COLLECTION_EVENT_CARD:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_CREATE:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_DASHBOARD_CALENDAR_TAB_INVITATION:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_DASHBOARD_HOME_TAB_CATEGORIES:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_DASHBOARD_HOME_TAB_SEE_ALL_UPCOMING_EVENTS:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_DASHBOARD_HOME_TAB_TODAY_TIME_FILTER:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_DASHBOARD_HOME_TAB_TOMORROW_TIME_FILTER:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_DASHBOARD_HOME_TAB_WEEKEND_TIME_FILTER:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_DASHBOARD_HOSTING_TAB_DRAFT_ROW_EDIT:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_DASHBORD_BIRTHDAY_ROW_CREATE:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_DASHBORD_CALENDAR_TAB:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_DASHBORD_CATEGORY_ROW:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_DASHBORD_TIME_ROW:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_PROMPT_ACTION_BUTTON:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum EVENT_TIPS:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum FEED_ATTACHMENT:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum GROUP_PERMALINK_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum GUEST_LIST_EDIT_GUEST_RSVP:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum HEADER:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum HOME_TAB:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum HOME_TAB_NEXT_EVENT:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum HOME_TAB_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum HOME_TAB_UPCOMING_EVENT:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum HOSTING_TAB:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum HOSTING_TAB_DRAFT_EVENT:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum HOSTING_TAB_PAST_EVENT:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum HOSTING_TAB_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum HOSTING_TAB_UPCOMING_EVENT:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PAGES_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PAGES_MANAGER_FAB:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PAGES_SURFACE_EVENTS_TAB:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PAGE_CREATE_DIALOG:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PAGE_EVENT_CREATE_BUTTON:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PAGE_EVENT_LIST_SUBSCRIBE_BUTTON:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PAGE_UPCOMING_EVENTS_CARD:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PERMALINK:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PERMALINK_ACTIVITY_TAB:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PERMALINK_CONTEXT_ROW:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PERMALINK_DRAFT_BANNER:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PERMALINK_EDIT_PAGE:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PERMALINK_EVENTS_CAROUSEL:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PERMALINK_RELATIONSHIP_BAR:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum PLACE_TIPS:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum REACTION_ATTACHMENT:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum REACTION_EVENT_ROW_COMPONENT:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum SAY_THANKS_COMPOSER:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum SEARCH_RESULT_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum TICKETING_ONSITE_FLOW:Lcom/facebook/events/common/ActionMechanism;

.field public static final enum USER_CREATE_DIALOG:Lcom/facebook/events/common/ActionMechanism;


# instance fields
.field private mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 907743
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "DASHBOARD_HEADER"

    const-string v2, "dashboard_header"

    invoke-direct {v0, v1, v4, v2}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_HEADER:Lcom/facebook/events/common/ActionMechanism;

    .line 907744
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "DASHBOARD_FAB"

    const-string v2, "dashboard_fab"

    invoke-direct {v0, v1, v5, v2}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_FAB:Lcom/facebook/events/common/ActionMechanism;

    .line 907745
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "DASHBOARD_ROW_INLINE_ACTIONS"

    const-string v2, "inline_actions"

    invoke-direct {v0, v1, v6, v2}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_ROW_INLINE_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

    .line 907746
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "DASHBOARD_ROW_GUEST_STATUS"

    const-string v2, "guest_status"

    invoke-direct {v0, v1, v7, v2}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_ROW_GUEST_STATUS:Lcom/facebook/events/common/ActionMechanism;

    .line 907747
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PERMALINK_ACTION_BAR"

    const-string v2, "permalink_action_bar"

    invoke-direct {v0, v1, v8, v2}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    .line 907748
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PERMALINK_DRAFT_BANNER"

    const/4 v2, 0x5

    const-string v3, "draft_banner"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_DRAFT_BANNER:Lcom/facebook/events/common/ActionMechanism;

    .line 907749
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PERMALINK_RELATIONSHIP_BAR"

    const/4 v2, 0x6

    const-string v3, "permalink_relationship_bar"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_RELATIONSHIP_BAR:Lcom/facebook/events/common/ActionMechanism;

    .line 907750
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PAGE_UPCOMING_EVENTS_CARD"

    const/4 v2, 0x7

    const-string v3, "upcoming_card"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PAGE_UPCOMING_EVENTS_CARD:Lcom/facebook/events/common/ActionMechanism;

    .line 907751
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PAGE_EVENT_CREATE_BUTTON"

    const/16 v2, 0x8

    const-string v3, "page_event_create_button"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PAGE_EVENT_CREATE_BUTTON:Lcom/facebook/events/common/ActionMechanism;

    .line 907752
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PAGE_EVENT_LIST_SUBSCRIBE_BUTTON"

    const/16 v2, 0x9

    const-string v3, "page_events_list_subscribe_button"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PAGE_EVENT_LIST_SUBSCRIBE_BUTTON:Lcom/facebook/events/common/ActionMechanism;

    .line 907753
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PAGES_SURFACE_EVENTS_TAB"

    const/16 v2, 0xa

    const-string v3, "page_surface_events_tab"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PAGES_SURFACE_EVENTS_TAB:Lcom/facebook/events/common/ActionMechanism;

    .line 907754
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PAGES_ACTION_BAR"

    const/16 v2, 0xb

    const-string v3, "page_action_bar"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PAGES_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    .line 907755
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PAGES_MANAGER_FAB"

    const/16 v2, 0xc

    const-string v3, "pages_manager_fab"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PAGES_MANAGER_FAB:Lcom/facebook/events/common/ActionMechanism;

    .line 907756
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "DASHBOARD_SUGGESTIONS_CARD"

    const/16 v2, 0xd

    const-string v3, "suggestions_card"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_SUGGESTIONS_CARD:Lcom/facebook/events/common/ActionMechanism;

    .line 907757
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "DASHBOARD_SUBSCRIPTIONS_CARD"

    const/16 v2, 0xe

    const-string v3, "subscriptions_card"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_SUBSCRIPTIONS_CARD:Lcom/facebook/events/common/ActionMechanism;

    .line 907758
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PERMALINK_EVENTS_CAROUSEL"

    const/16 v2, 0xf

    const-string v3, "events_carousel"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_EVENTS_CAROUSEL:Lcom/facebook/events/common/ActionMechanism;

    .line 907759
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENTS_LIST"

    const/16 v2, 0x10

    const-string v3, "events_list"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENTS_LIST:Lcom/facebook/events/common/ActionMechanism;

    .line 907760
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_CREATE"

    const/16 v2, 0x11

    const-string v3, "event_create"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_CREATE:Lcom/facebook/events/common/ActionMechanism;

    .line 907761
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "GROUP_PERMALINK_ACTIONS"

    const/16 v2, 0x12

    const-string v3, "group_permalink_actions"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->GROUP_PERMALINK_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

    .line 907762
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "SEARCH_RESULT_ACTIONS"

    const/16 v2, 0x13

    const-string v3, "search_result_actions"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->SEARCH_RESULT_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

    .line 907763
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_CHAINING"

    const/16 v2, 0x14

    const-string v3, "event_chaining"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_CHAINING:Lcom/facebook/events/common/ActionMechanism;

    .line 907764
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "FEED_ATTACHMENT"

    const/16 v2, 0x15

    const-string v3, "feed_attachment"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->FEED_ATTACHMENT:Lcom/facebook/events/common/ActionMechanism;

    .line 907765
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PLACE_TIPS"

    const/16 v2, 0x16

    const-string v3, "place_tips"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PLACE_TIPS:Lcom/facebook/events/common/ActionMechanism;

    .line 907766
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "REACTION_ATTACHMENT"

    const/16 v2, 0x17

    const-string v3, "reaction_attachment"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->REACTION_ATTACHMENT:Lcom/facebook/events/common/ActionMechanism;

    .line 907767
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PERMALINK"

    const/16 v2, 0x18

    const-string v3, "permalink"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PERMALINK:Lcom/facebook/events/common/ActionMechanism;

    .line 907768
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "GUEST_LIST_EDIT_GUEST_RSVP"

    const/16 v2, 0x19

    const-string v3, "guest_list_edit_guest_rsvp"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->GUEST_LIST_EDIT_GUEST_RSVP:Lcom/facebook/events/common/ActionMechanism;

    .line 907769
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "HEADER"

    const/16 v2, 0x1a

    const-string v3, "header"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->HEADER:Lcom/facebook/events/common/ActionMechanism;

    .line 907770
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "CHECKIN_COMPOSER"

    const/16 v2, 0x1b

    const-string v3, "checkin_composer"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->CHECKIN_COMPOSER:Lcom/facebook/events/common/ActionMechanism;

    .line 907771
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "SAY_THANKS_COMPOSER"

    const/16 v2, 0x1c

    const-string v3, "say_thanks_composer"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->SAY_THANKS_COMPOSER:Lcom/facebook/events/common/ActionMechanism;

    .line 907772
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "BUY_TICKETS_CTA"

    const/16 v2, 0x1d

    const-string v3, "buy_tickets_cta"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->BUY_TICKETS_CTA:Lcom/facebook/events/common/ActionMechanism;

    .line 907773
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "TICKETING_ONSITE_FLOW"

    const/16 v2, 0x1e

    const-string v3, "onsite_flow"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->TICKETING_ONSITE_FLOW:Lcom/facebook/events/common/ActionMechanism;

    .line 907774
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PERMALINK_CONTEXT_ROW"

    const/16 v2, 0x1f

    const-string v3, "permalink_context_row"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_CONTEXT_ROW:Lcom/facebook/events/common/ActionMechanism;

    .line 907775
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_TIPS"

    const/16 v2, 0x20

    const-string v3, "event_tips"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_TIPS:Lcom/facebook/events/common/ActionMechanism;

    .line 907776
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "REACTION_EVENT_ROW_COMPONENT"

    const/16 v2, 0x21

    const-string v3, "reaction_event_row_component"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->REACTION_EVENT_ROW_COMPONENT:Lcom/facebook/events/common/ActionMechanism;

    .line 907777
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "BUY_TICKETS_FLOW"

    const/16 v2, 0x22

    const-string v3, "buy_tickets_flow"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->BUY_TICKETS_FLOW:Lcom/facebook/events/common/ActionMechanism;

    .line 907778
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_PROMPT_ACTION_BUTTON"

    const/16 v2, 0x23

    const-string v3, "event_prompt_action_button"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_PROMPT_ACTION_BUTTON:Lcom/facebook/events/common/ActionMechanism;

    .line 907779
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "CANCEL_EVENT_FLOW"

    const/16 v2, 0x24

    const-string v3, "cancel_event_flow"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->CANCEL_EVENT_FLOW:Lcom/facebook/events/common/ActionMechanism;

    .line 907780
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PAGE_CREATE_DIALOG"

    const/16 v2, 0x25

    const-string v3, "page_create_dialog"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PAGE_CREATE_DIALOG:Lcom/facebook/events/common/ActionMechanism;

    .line 907781
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "USER_CREATE_DIALOG"

    const/16 v2, 0x26

    const-string v3, "user_create_dialog"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->USER_CREATE_DIALOG:Lcom/facebook/events/common/ActionMechanism;

    .line 907782
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "COPY_EVENT"

    const/16 v2, 0x27

    const-string v3, "copy_event"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->COPY_EVENT:Lcom/facebook/events/common/ActionMechanism;

    .line 907783
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PERMALINK_EDIT_PAGE"

    const/16 v2, 0x28

    const-string v3, "permalink_edit_page"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_EDIT_PAGE:Lcom/facebook/events/common/ActionMechanism;

    .line 907784
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "PERMALINK_ACTIVITY_TAB"

    const/16 v2, 0x29

    const-string v3, "permalink_activity_tab"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTIVITY_TAB:Lcom/facebook/events/common/ActionMechanism;

    .line 907785
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_COLLECTION_EVENT_CARD"

    const/16 v2, 0x2a

    const-string v3, "event_collection_event_card"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_COLLECTION_EVENT_CARD:Lcom/facebook/events/common/ActionMechanism;

    .line 907786
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_DASHBORD_CATEGORY_ROW"

    const/16 v2, 0x2b

    const-string v3, "event_dashboard_category_row"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBORD_CATEGORY_ROW:Lcom/facebook/events/common/ActionMechanism;

    .line 907787
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_DASHBORD_TIME_ROW"

    const/16 v2, 0x2c

    const-string v3, "event_dashboard_time_row"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBORD_TIME_ROW:Lcom/facebook/events/common/ActionMechanism;

    .line 907788
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "COPY_EVENT_INVITES"

    const/16 v2, 0x2d

    const-string v3, "copy_event_invites"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->COPY_EVENT_INVITES:Lcom/facebook/events/common/ActionMechanism;

    .line 907789
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENTS_SUGGESTION_UNIT"

    const/16 v2, 0x2e

    const-string v3, "events_suggestion_unit"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENTS_SUGGESTION_UNIT:Lcom/facebook/events/common/ActionMechanism;

    .line 907790
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_DASHBORD_BIRTHDAY_ROW_CREATE"

    const/16 v2, 0x2f

    const-string v3, "hosting_tab_birthday_row_create"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBORD_BIRTHDAY_ROW_CREATE:Lcom/facebook/events/common/ActionMechanism;

    .line 907791
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_DASHBORD_CALENDAR_TAB"

    const/16 v2, 0x30

    const-string v3, "calendar_tab"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBORD_CALENDAR_TAB:Lcom/facebook/events/common/ActionMechanism;

    .line 907792
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_DASHBOARD_HOSTING_TAB_DRAFT_ROW_EDIT"

    const/16 v2, 0x31

    const-string v3, "hosting_tab_draft_row_edit"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOSTING_TAB_DRAFT_ROW_EDIT:Lcom/facebook/events/common/ActionMechanism;

    .line 907793
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_DASHBOARD_CALENDAR_TAB_INVITATION"

    const/16 v2, 0x32

    const-string v3, "calendar_tab_invitation"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_CALENDAR_TAB_INVITATION:Lcom/facebook/events/common/ActionMechanism;

    .line 907794
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "HOME_TAB_UPCOMING_EVENT"

    const/16 v2, 0x33

    const-string v3, "home_tab_upcoming_event"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->HOME_TAB_UPCOMING_EVENT:Lcom/facebook/events/common/ActionMechanism;

    .line 907795
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "HOME_TAB_NEXT_EVENT"

    const/16 v2, 0x34

    const-string v3, "home_tab_next_event"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->HOME_TAB_NEXT_EVENT:Lcom/facebook/events/common/ActionMechanism;

    .line 907796
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "CALENDAR_TAB_EVENT"

    const/16 v2, 0x35

    const-string v3, "calendar_tab_event"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->CALENDAR_TAB_EVENT:Lcom/facebook/events/common/ActionMechanism;

    .line 907797
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "HOSTING_TAB_UPCOMING_EVENT"

    const/16 v2, 0x36

    const-string v3, "hosting_tab_upcoming_event"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB_UPCOMING_EVENT:Lcom/facebook/events/common/ActionMechanism;

    .line 907798
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "HOSTING_TAB_DRAFT_EVENT"

    const/16 v2, 0x37

    const-string v3, "hosting_tab_draft_event"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB_DRAFT_EVENT:Lcom/facebook/events/common/ActionMechanism;

    .line 907799
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "HOSTING_TAB_PAST_EVENT"

    const/16 v2, 0x38

    const-string v3, "hosting_tab_past_event"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB_PAST_EVENT:Lcom/facebook/events/common/ActionMechanism;

    .line 907800
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "HOME_TAB_SCROLL_VIEW"

    const/16 v2, 0x39

    const-string v3, "home_tab_scroll_view"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->HOME_TAB_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

    .line 907801
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "CALENDAR_TAB_UP_SCROLL_VIEW"

    const/16 v2, 0x3a

    const-string v3, "calendar_tab_up_scroll_view"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->CALENDAR_TAB_UP_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

    .line 907802
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "CALENDAR_TAB_DOWN_SCROLL_VIEW"

    const/16 v2, 0x3b

    const-string v3, "calendar_tab_down_scroll_view"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->CALENDAR_TAB_DOWN_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

    .line 907803
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "HOSTING_TAB_SCROLL_VIEW"

    const/16 v2, 0x3c

    const-string v3, "hosting_tab_scroll_view"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

    .line 907804
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "HOME_TAB"

    const/16 v2, 0x3d

    const-string v3, "home_tab"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->HOME_TAB:Lcom/facebook/events/common/ActionMechanism;

    .line 907805
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "CALENDAR_TAB"

    const/16 v2, 0x3e

    const-string v3, "calendar_tab"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->CALENDAR_TAB:Lcom/facebook/events/common/ActionMechanism;

    .line 907806
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "HOSTING_TAB"

    const/16 v2, 0x3f

    const-string v3, "hosting_tab"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB:Lcom/facebook/events/common/ActionMechanism;

    .line 907807
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_DASHBOARD_HOME_TAB_SEE_ALL_UPCOMING_EVENTS"

    const/16 v2, 0x40

    const-string v3, "home_tab_see_all_upcoming_events"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_SEE_ALL_UPCOMING_EVENTS:Lcom/facebook/events/common/ActionMechanism;

    .line 907808
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_DASHBOARD_HOME_TAB_TODAY_TIME_FILTER"

    const/16 v2, 0x41

    const-string v3, "home_tab_see_suggested_events_today"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_TODAY_TIME_FILTER:Lcom/facebook/events/common/ActionMechanism;

    .line 907809
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_DASHBOARD_HOME_TAB_TOMORROW_TIME_FILTER"

    const/16 v2, 0x42

    const-string v3, "home_tab_see_suggested_events_tomorrow"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_TOMORROW_TIME_FILTER:Lcom/facebook/events/common/ActionMechanism;

    .line 907810
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_DASHBOARD_HOME_TAB_WEEKEND_TIME_FILTER"

    const/16 v2, 0x43

    const-string v3, "home_tab_see_suggested_events_future"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_WEEKEND_TIME_FILTER:Lcom/facebook/events/common/ActionMechanism;

    .line 907811
    new-instance v0, Lcom/facebook/events/common/ActionMechanism;

    const-string v1, "EVENT_DASHBOARD_HOME_TAB_CATEGORIES"

    const/16 v2, 0x44

    const-string v3, "home_tab_categories"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/common/ActionMechanism;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_CATEGORIES:Lcom/facebook/events/common/ActionMechanism;

    .line 907812
    const/16 v0, 0x45

    new-array v0, v0, [Lcom/facebook/events/common/ActionMechanism;

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_HEADER:Lcom/facebook/events/common/ActionMechanism;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_FAB:Lcom/facebook/events/common/ActionMechanism;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_ROW_INLINE_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_ROW_GUEST_STATUS:Lcom/facebook/events/common/ActionMechanism;

    aput-object v1, v0, v7

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_DRAFT_BANNER:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_RELATIONSHIP_BAR:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PAGE_UPCOMING_EVENTS_CARD:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PAGE_EVENT_CREATE_BUTTON:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PAGE_EVENT_LIST_SUBSCRIBE_BUTTON:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PAGES_SURFACE_EVENTS_TAB:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PAGES_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PAGES_MANAGER_FAB:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_SUGGESTIONS_CARD:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_SUBSCRIPTIONS_CARD:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_EVENTS_CAROUSEL:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENTS_LIST:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_CREATE:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->GROUP_PERMALINK_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->SEARCH_RESULT_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_CHAINING:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->FEED_ATTACHMENT:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PLACE_TIPS:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->REACTION_ATTACHMENT:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->GUEST_LIST_EDIT_GUEST_RSVP:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->HEADER:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->CHECKIN_COMPOSER:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->SAY_THANKS_COMPOSER:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->BUY_TICKETS_CTA:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->TICKETING_ONSITE_FLOW:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_CONTEXT_ROW:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_TIPS:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->REACTION_EVENT_ROW_COMPONENT:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->BUY_TICKETS_FLOW:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_PROMPT_ACTION_BUTTON:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->CANCEL_EVENT_FLOW:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PAGE_CREATE_DIALOG:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->USER_CREATE_DIALOG:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->COPY_EVENT:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_EDIT_PAGE:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTIVITY_TAB:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_COLLECTION_EVENT_CARD:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBORD_CATEGORY_ROW:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBORD_TIME_ROW:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->COPY_EVENT_INVITES:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENTS_SUGGESTION_UNIT:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBORD_BIRTHDAY_ROW_CREATE:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBORD_CALENDAR_TAB:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOSTING_TAB_DRAFT_ROW_EDIT:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_CALENDAR_TAB_INVITATION:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->HOME_TAB_UPCOMING_EVENT:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->HOME_TAB_NEXT_EVENT:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->CALENDAR_TAB_EVENT:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB_UPCOMING_EVENT:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB_DRAFT_EVENT:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB_PAST_EVENT:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->HOME_TAB_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->CALENDAR_TAB_UP_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->CALENDAR_TAB_DOWN_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->HOME_TAB:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->CALENDAR_TAB:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_SEE_ALL_UPCOMING_EVENTS:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_TODAY_TIME_FILTER:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_TOMORROW_TIME_FILTER:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_WEEKEND_TIME_FILTER:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_CATEGORIES:Lcom/facebook/events/common/ActionMechanism;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->$VALUES:[Lcom/facebook/events/common/ActionMechanism;

    .line 907813
    new-instance v0, LX/5O2;

    invoke-direct {v0}, LX/5O2;-><init>()V

    sput-object v0, Lcom/facebook/events/common/ActionMechanism;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 907814
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 907815
    iput-object p3, p0, Lcom/facebook/events/common/ActionMechanism;->mName:Ljava/lang/String;

    .line 907816
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/events/common/ActionMechanism;
    .locals 1

    .prologue
    .line 907817
    const-class v0, Lcom/facebook/events/common/ActionMechanism;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/common/ActionMechanism;

    return-object v0
.end method

.method public static values()[Lcom/facebook/events/common/ActionMechanism;
    .locals 1

    .prologue
    .line 907818
    sget-object v0, Lcom/facebook/events/common/ActionMechanism;->$VALUES:[Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/events/common/ActionMechanism;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 907819
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 907820
    iget-object v0, p0, Lcom/facebook/events/common/ActionMechanism;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 907821
    invoke-virtual {p0}, Lcom/facebook/events/common/ActionMechanism;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 907822
    return-void
.end method
