.class public final Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/5tj;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x33e5922d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I

.field private j:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1017475
    const-class v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1017476
    const-class v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1017477
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1017478
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1017479
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1017480
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1017481
    return-void
.end method

.method public static a(LX/5tj;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;
    .locals 4

    .prologue
    .line 1017482
    if-nez p0, :cond_0

    .line 1017483
    const/4 p0, 0x0

    .line 1017484
    :goto_0
    return-object p0

    .line 1017485
    :cond_0
    instance-of v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    if-eqz v0, :cond_1

    .line 1017486
    check-cast p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    goto :goto_0

    .line 1017487
    :cond_1
    new-instance v0, LX/5tu;

    invoke-direct {v0}, LX/5tu;-><init>()V

    .line 1017488
    invoke-interface {p0}, LX/5tj;->bg_()J

    move-result-wide v2

    iput-wide v2, v0, LX/5tu;->a:J

    .line 1017489
    invoke-interface {p0}, LX/5tj;->e()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;->a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v1

    iput-object v1, v0, LX/5tu;->b:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1017490
    invoke-interface {p0}, LX/5tj;->bh_()LX/1VU;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->a(LX/1VU;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5tu;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    .line 1017491
    invoke-interface {p0}, LX/5tj;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5tu;->d:Ljava/lang/String;

    .line 1017492
    invoke-interface {p0}, LX/5tj;->b()I

    move-result v1

    iput v1, v0, LX/5tu;->e:I

    .line 1017493
    invoke-interface {p0}, LX/5tj;->c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5tu;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1017494
    invoke-interface {p0}, LX/5tj;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;->a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v1

    iput-object v1, v0, LX/5tu;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    .line 1017495
    invoke-interface {p0}, LX/5tj;->l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;->a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v1

    iput-object v1, v0, LX/5tu;->h:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1017496
    invoke-interface {p0}, LX/5tj;->d()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;->a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v1

    iput-object v1, v0, LX/5tu;->i:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1017497
    invoke-virtual {v0}, LX/5tu;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1017498
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1017499
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1017500
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1017501
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1017502
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->o()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1017503
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->p()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1017504
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1017505
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->r()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1017506
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1017507
    iget-wide v2, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1017508
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1017509
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1017510
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1017511
    const/4 v0, 0x4

    iget v2, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->i:I

    invoke-virtual {p1, v0, v2, v1}, LX/186;->a(III)V

    .line 1017512
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1017513
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1017514
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1017515
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 1017516
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1017517
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1017518
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1017519
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1017520
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1017521
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1017522
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    .line 1017523
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1017524
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1017525
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    .line 1017526
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1017527
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    .line 1017528
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    .line 1017529
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->o()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1017530
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->o()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1017531
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->o()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1017532
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    .line 1017533
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->j:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1017534
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->p()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1017535
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->p()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    .line 1017536
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->p()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1017537
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    .line 1017538
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    .line 1017539
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1017540
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1017541
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1017542
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    .line 1017543
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->l:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1017544
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->r()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1017545
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->r()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1017546
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->r()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1017547
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    .line 1017548
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1017549
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1017550
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017453
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1017551
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1017552
    const-wide/16 v0, 0x0

    invoke-virtual {p1, p2, v2, v0, v1}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->e:J

    .line 1017553
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->i:I

    .line 1017554
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1017555
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1017556
    iget v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->i:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1017557
    new-instance v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    invoke-direct {v0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;-><init>()V

    .line 1017558
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1017559
    return-object v0
.end method

.method public final bg_()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1017560
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1017561
    iget-wide v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->e:J

    return-wide v0
.end method

.method public final synthetic bh_()LX/1VU;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017474
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017562
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->o()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017473
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->r()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1017472
    const v0, 0x4396cc8c

    return v0
.end method

.method public final synthetic e()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017471
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1017470
    const v0, -0x7d2175f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017468
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->h:Ljava/lang/String;

    .line 1017469
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017467
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->p()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017466
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    return-object v0
.end method

.method public final m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017464
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1017465
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017462
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    .line 1017463
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    return-object v0
.end method

.method public final o()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017460
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->j:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->j:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1017461
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->j:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    return-object v0
.end method

.method public final p()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017458
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    .line 1017459
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    return-object v0
.end method

.method public final q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017456
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->l:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->l:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1017457
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->l:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    return-object v0
.end method

.method public final r()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017454
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1017455
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    return-object v0
.end method
