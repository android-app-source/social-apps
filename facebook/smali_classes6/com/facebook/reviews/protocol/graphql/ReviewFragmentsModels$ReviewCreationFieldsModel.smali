.class public final Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/5th;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x632d1dde
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1017261
    const-class v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1017260
    const-class v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1017224
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1017225
    return-void
.end method

.method private a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017258
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1017259
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    return-object v0
.end method

.method private j()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017256
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1017257
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1017247
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1017248
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1017249
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->j()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1017250
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1017251
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1017252
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1017253
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1017254
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1017255
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1017234
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1017235
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1017236
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1017237
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1017238
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;

    .line 1017239
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1017240
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->j()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1017241
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->j()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1017242
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->j()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1017243
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;

    .line 1017244
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1017245
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1017246
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1017231
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1017232
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;->e:J

    .line 1017233
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1017228
    new-instance v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;-><init>()V

    .line 1017229
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1017230
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1017227
    const v0, 0x33834964

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1017226
    const v0, -0x7d2175f

    return v0
.end method
