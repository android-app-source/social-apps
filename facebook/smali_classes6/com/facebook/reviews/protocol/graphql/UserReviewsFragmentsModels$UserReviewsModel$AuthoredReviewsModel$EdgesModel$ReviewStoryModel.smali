.class public final Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5cd1758e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1018899
    const-class v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1018898
    const-class v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1018896
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1018897
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1018893
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1018894
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1018895
    return-void
.end method

.method public static a(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;)Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;
    .locals 2

    .prologue
    .line 1018885
    if-nez p0, :cond_0

    .line 1018886
    const/4 p0, 0x0

    .line 1018887
    :goto_0
    return-object p0

    .line 1018888
    :cond_0
    instance-of v0, p0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;

    if-eqz v0, :cond_1

    .line 1018889
    check-cast p0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;

    goto :goto_0

    .line 1018890
    :cond_1
    new-instance v0, LX/5uL;

    invoke-direct {v0}, LX/5uL;-><init>()V

    .line 1018891
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/5uL;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1018892
    invoke-virtual {v0}, LX/5uL;->a()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1018900
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1018901
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1018902
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1018903
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1018904
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1018905
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1018877
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1018878
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1018879
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1018880
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1018881
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;

    .line 1018882
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1018883
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1018884
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1018875
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1018876
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1018872
    new-instance v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;

    invoke-direct {v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;-><init>()V

    .line 1018873
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1018874
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1018871
    const v0, 0x104bfc31

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1018870
    const v0, -0x7d2175f

    return v0
.end method
