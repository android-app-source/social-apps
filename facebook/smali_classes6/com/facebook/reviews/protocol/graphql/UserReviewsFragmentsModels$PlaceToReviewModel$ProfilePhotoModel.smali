.class public final Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6129ea6c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1018390
    const-class v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1018391
    const-class v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1018392
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1018393
    return-void
.end method

.method private j()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1018394
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;->e:Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;->e:Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    .line 1018395
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;->e:Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1018396
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1018397
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;->j()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1018398
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1018399
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1018400
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1018401
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1018402
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1018403
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;->j()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1018404
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;->j()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    .line 1018405
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;->j()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1018406
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;

    .line 1018407
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;->e:Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    .line 1018408
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1018409
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1018410
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;->j()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1018411
    new-instance v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;

    invoke-direct {v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;-><init>()V

    .line 1018412
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1018413
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1018414
    const v0, -0x6b0f30cf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1018415
    const v0, 0x4984e12

    return v0
.end method
