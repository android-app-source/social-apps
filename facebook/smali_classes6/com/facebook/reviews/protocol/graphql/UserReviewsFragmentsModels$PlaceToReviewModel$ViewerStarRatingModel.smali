.class public final Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x39666b16
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1018466
    const-class v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1018465
    const-class v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1018443
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1018444
    return-void
.end method


# virtual methods
.method public final a()D
    .locals 2

    .prologue
    .line 1018463
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1018464
    iget-wide v0, p0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel;->f:D

    return-wide v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1018457
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1018458
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1018459
    iget v0, p0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1018460
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel;->f:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1018461
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1018462
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1018454
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1018455
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1018456
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1018450
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1018451
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel;->e:I

    .line 1018452
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel;->f:D

    .line 1018453
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1018447
    new-instance v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel;

    invoke-direct {v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel;-><init>()V

    .line 1018448
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1018449
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1018446
    const v0, -0x13bc2327

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1018445
    const v0, -0x6e856243

    return v0
.end method
