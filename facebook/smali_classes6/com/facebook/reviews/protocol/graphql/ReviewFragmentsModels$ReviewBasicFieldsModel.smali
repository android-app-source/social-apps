.class public final Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/55r;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x689da85
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1016767
    const-class v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1016766
    const-class v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1016764
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1016765
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1016761
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1016762
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1016763
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1016759
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->e:Ljava/lang/String;

    .line 1016760
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1016757
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1016758
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1016755
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->h:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->h:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1016756
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->h:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1016744
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1016745
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1016746
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1016747
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1016748
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1016749
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1016750
    const/4 v0, 0x1

    iget v3, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->f:I

    invoke-virtual {p1, v0, v3, v4}, LX/186;->a(III)V

    .line 1016751
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1016752
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1016753
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1016754
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1016768
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1016769
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1016770
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1016771
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1016772
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    .line 1016773
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1016774
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1016775
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1016776
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1016777
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    .line 1016778
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->h:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1016779
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1016780
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1016731
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1016732
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1016733
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->f:I

    .line 1016734
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1016735
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1016736
    iget v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1016737
    new-instance v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    invoke-direct {v0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;-><init>()V

    .line 1016738
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1016739
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1016740
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1016743
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;->l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1016741
    const v0, 0x786d851f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1016742
    const v0, -0x7d2175f

    return v0
.end method
