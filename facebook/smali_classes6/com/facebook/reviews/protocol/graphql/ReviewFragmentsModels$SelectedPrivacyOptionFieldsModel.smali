.class public final Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2932ad85
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1017780
    const-class v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1017777
    const-class v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1017778
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1017779
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1017781
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1017782
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1017783
    return-void
.end method

.method public static a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .locals 2

    .prologue
    .line 1017784
    if-nez p0, :cond_0

    .line 1017785
    const/4 p0, 0x0

    .line 1017786
    :goto_0
    return-object p0

    .line 1017787
    :cond_0
    instance-of v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    if-eqz v0, :cond_1

    .line 1017788
    check-cast p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    goto :goto_0

    .line 1017789
    :cond_1
    new-instance v0, LX/5tx;

    invoke-direct {v0}, LX/5tx;-><init>()V

    .line 1017790
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->a()LX/1Fd;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;->a(LX/1Fd;)Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5tx;->a:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    .line 1017791
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->b()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;->a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    move-result-object v1

    iput-object v1, v0, LX/5tx;->b:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    .line 1017792
    invoke-virtual {v0}, LX/5tx;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object p0

    goto :goto_0
.end method

.method private j()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017773
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->e:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->e:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    .line 1017774
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->e:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017775
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    iput-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    .line 1017776
    iget-object v0, p0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1017765
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1017766
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->j()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1017767
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1017768
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1017769
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1017770
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1017771
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1017772
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1017752
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1017753
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->j()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1017754
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->j()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    .line 1017755
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->j()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1017756
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1017757
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->e:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    .line 1017758
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1017759
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    .line 1017760
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1017761
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1017762
    iput-object v0, v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    .line 1017763
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1017764
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/1Fd;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017751
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->j()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1017745
    new-instance v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    invoke-direct {v0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;-><init>()V

    .line 1017746
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1017747
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1017750
    invoke-direct {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1017749
    const v0, 0x7c850548

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1017748
    const v0, -0x1c648c34

    return v0
.end method
