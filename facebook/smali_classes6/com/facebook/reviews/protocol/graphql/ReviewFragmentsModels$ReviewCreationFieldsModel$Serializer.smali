.class public final Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1017132
    const-class v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;

    new-instance v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1017133
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1017134
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1017135
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1017136
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 1017137
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1017138
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1017139
    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 1017140
    const-string v4, "creation_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1017141
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1017142
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1017143
    if-eqz v2, :cond_1

    .line 1017144
    const-string v3, "creator"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1017145
    invoke-static {v1, v2, p1, p2}, LX/5u6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1017146
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1017147
    if-eqz v2, :cond_2

    .line 1017148
    const-string v3, "story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1017149
    invoke-static {v1, v2, p1}, LX/5u7;->a(LX/15i;ILX/0nX;)V

    .line 1017150
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1017151
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1017152
    check-cast p1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$Serializer;->a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
