.class public Lcom/facebook/http/executors/liger/LigerTraceEventHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Xa;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:LX/0Zb;

.field private final d:LX/1iW;

.field private final e:LX/1hg;

.field private final f:LX/1iQ;

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/1hI;

.field private final j:LX/0YR;

.field private final k:LX/1MY;

.field private final l:LX/1Mf;


# direct methods
.method public constructor <init>(LX/0Zb;LX/1iW;Ljava/lang/String;Lorg/apache/http/protocol/HttpContext;LX/0p3;LX/1iQ;LX/0p7;LX/0oz;LX/0So;LX/1hI;LX/0YR;LX/1MY;LX/1Mf;)V
    .locals 8

    .prologue
    .line 1108511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108512
    iput-object p1, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->c:LX/0Zb;

    .line 1108513
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    .line 1108514
    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v3, "http_stack"

    invoke-virtual {p2}, LX/1iW;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108515
    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v3, "connection_type"

    invoke-virtual {p2}, LX/1iW;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108516
    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v3, "connection_subtype"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, LX/1iW;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, LX/1iW;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108517
    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v3, "request_queue_time_ms"

    invoke-virtual {p2}, LX/1iW;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108518
    invoke-static {p4}, LX/1iV;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iV;

    move-result-object v2

    .line 1108519
    iput-object p6, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->f:LX/1iQ;

    .line 1108520
    iput-object p3, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->a:Ljava/lang/String;

    .line 1108521
    invoke-virtual {v2}, LX/1iV;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->b:Ljava/lang/String;

    .line 1108522
    iget-object v3, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v4, "request_friendly_name"

    iget-object v5, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->b:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108523
    iget-object v3, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v4, "connqual"

    invoke-virtual {p5}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108524
    invoke-virtual {v2}, LX/1iV;->d()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 1108525
    if-eqz v2, :cond_0

    .line 1108526
    iget-object v3, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v4, "request_call_path"

    invoke-virtual {v2}, Lcom/facebook/common/callercontext/CallerContext;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108527
    iget-object v3, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v4, "request_analytics_tag"

    invoke-virtual {v2}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108528
    iget-object v3, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v4, "request_module_analytics_tag"

    invoke-virtual {v2}, Lcom/facebook/common/callercontext/CallerContext;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108529
    iget-object v3, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v4, "request_feature_tag"

    invoke-virtual {v2}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108530
    :cond_0
    if-eqz p8, :cond_1

    .line 1108531
    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v3, "conncls_bandwidth_qual"

    invoke-virtual/range {p8 .. p8}, LX/0oz;->b()LX/0p3;

    move-result-object v4

    invoke-virtual {v4}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108532
    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v3, "conncls_bandwidth_bps"

    invoke-virtual/range {p8 .. p8}, LX/0oz;->f()D

    move-result-wide v4

    const-wide v6, 0x405f400000000000L    # 125.0

    mul-double/2addr v4, v6

    double-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108533
    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v3, "conncls_latency_qual"

    invoke-virtual/range {p8 .. p8}, LX/0oz;->e()LX/0p3;

    move-result-object v4

    invoke-virtual {v4}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108534
    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v3, "conncls_latency_ms"

    invoke-virtual/range {p8 .. p8}, LX/0oz;->k()D

    move-result-wide v4

    double-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108535
    :cond_1
    const-string v2, "request_method"

    invoke-interface {p4, v2}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1108536
    iget-object v3, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v4, "request_method"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108537
    iput-object p2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->d:LX/1iW;

    .line 1108538
    if-eqz p7, :cond_5

    if-eqz p9, :cond_5

    .line 1108539
    new-instance v2, LX/1hg;

    move-object/from16 v0, p9

    invoke-direct {v2, p7, v0}, LX/1hg;-><init>(LX/0p7;LX/0So;)V

    iput-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->e:LX/1hg;

    .line 1108540
    :goto_0
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->i:LX/1hI;

    .line 1108541
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->j:LX/0YR;

    .line 1108542
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->k:LX/1MY;

    .line 1108543
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->l:LX/1Mf;

    .line 1108544
    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->f:LX/1iQ;

    invoke-virtual {v2}, LX/1iQ;->isCellTowerSampled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1108545
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->h:Ljava/util/Map;

    .line 1108546
    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->i:LX/1hI;

    iget-object v3, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->h:Ljava/util/Map;

    invoke-virtual {v2, v3}, LX/1hI;->a(Ljava/util/Map;)V

    .line 1108547
    :cond_2
    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->f:LX/1iQ;

    invoke-virtual {v2}, LX/1iQ;->isFlowTimeSampled()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->f:LX/1iQ;

    invoke-virtual {v2}, LX/1iQ;->isCellTowerSampled()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1108548
    :cond_3
    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->j:LX/0YR;

    invoke-interface {v2}, LX/0YR;->a()LX/1hM;

    move-result-object v2

    .line 1108549
    if-eqz v2, :cond_4

    .line 1108550
    iget-object v3, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    invoke-virtual {v2}, LX/1hM;->e()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1108551
    :cond_4
    return-void

    .line 1108552
    :cond_5
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->e:LX/1hg;

    goto :goto_0
.end method

.method private a(LX/4iP;)V
    .locals 8

    .prologue
    .line 1108553
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->e:LX/1hg;

    if-eqz v0, :cond_0

    .line 1108554
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->e:LX/1hg;

    .line 1108555
    iget v1, p1, LX/4iP;->mRspBodyCompBytes:I

    move v1, v1

    .line 1108556
    int-to-long v2, v1

    .line 1108557
    iget-wide v6, p1, LX/4iP;->mRspBodyBytesTime:J

    move-wide v4, v6

    .line 1108558
    invoke-virtual {v0, v2, v3, v4, v5}, LX/1hg;->a(JJ)V

    .line 1108559
    :cond_0
    return-void
.end method


# virtual methods
.method public final decorateStatistics(LX/4iZ;J)V
    .locals 14

    .prologue
    .line 1108560
    if-nez p1, :cond_1

    .line 1108561
    :cond_0
    :goto_0
    return-void

    .line 1108562
    :cond_1
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    const-string v1, "request_status"

    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->d:LX/1iW;

    invoke-virtual {v2}, LX/1iW;->k()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108563
    invoke-virtual {p1}, LX/4iZ;->getFlowStats()LX/4iP;

    move-result-object v12

    .line 1108564
    invoke-direct {p0, v12}, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->a(LX/4iP;)V

    .line 1108565
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->f:LX/1iQ;

    invoke-virtual {v0}, LX/1iQ;->isRequestsBatchLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1108566
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->l:LX/1Mf;

    iget-object v1, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->d:LX/1iW;

    invoke-virtual {v0, v1, v2, v12, v3}, LX/1Mf;->a(Ljava/lang/String;Ljava/lang/String;LX/4iP;LX/1iW;)V

    .line 1108567
    :cond_2
    invoke-virtual {v12}, LX/4iP;->getIsNewConnection()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v12}, LX/4iP;->getRequestHeaderCompressedBytes()I

    move-result v2

    invoke-virtual {v12}, LX/4iP;->getRequestBodyBytes()I

    move-result v3

    invoke-virtual {v12}, LX/4iP;->getResponseHeaderCompressedBytes()I

    move-result v4

    invoke-virtual {v12}, LX/4iP;->getResponseBodyCompressedBytes()I

    move-result v5

    invoke-virtual {v12}, LX/4iP;->getDnsLatency()J

    move-result-wide v6

    long-to-int v6, v6

    invoke-virtual {v12}, LX/4iP;->getTcpLatency()J

    move-result-wide v8

    long-to-int v7, v8

    invoke-virtual {v12}, LX/4iP;->getTlsLatency()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v12}, LX/4iP;->getRspBodyBytesTime()J

    move-result-wide v10

    long-to-int v9, v10

    move-wide/from16 v10, p2

    invoke-static/range {v1 .. v11}, Lcom/facebook/loom/logger/api/LoomLogger;->a(IIIIIIIIIJ)V

    .line 1108568
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->f:LX/1iQ;

    invoke-virtual {v0}, LX/1iQ;->shouldPrintTraceEvents()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1108569
    invoke-virtual {p1}, LX/4iZ;->getTraceEvents()[Lcom/facebook/proxygen/TraceEvent;

    move-result-object v1

    .line 1108570
    array-length v2, v1

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 1108571
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/proxygen/TraceEvent;->toPrettyJson()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1108572
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1108573
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 1108574
    :cond_4
    if-eqz v12, :cond_6

    .line 1108575
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->d:LX/1iW;

    iget-object v0, v0, LX/1iW;->requestHeaderBytes:LX/1iX;

    invoke-virtual {v12}, LX/4iP;->getRequestHeaderBytes()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/1iX;->a(J)V

    .line 1108576
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->d:LX/1iW;

    iget-object v0, v0, LX/1iW;->requestBodyBytes:LX/1iX;

    invoke-virtual {v12}, LX/4iP;->getRequestBodyBytes()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/1iX;->a(J)V

    .line 1108577
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->d:LX/1iW;

    iget-object v0, v0, LX/1iW;->responseHeaderBytes:LX/1iX;

    invoke-virtual {v12}, LX/4iP;->getResponseHeaderBytes()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/1iX;->a(J)V

    .line 1108578
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->d:LX/1iW;

    iget-object v0, v0, LX/1iW;->responseBodyBytes:LX/1iX;

    invoke-virtual {v12}, LX/4iP;->getResponseBodyCompressedBytes()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/1iX;->a(J)V

    .line 1108579
    invoke-virtual {v12}, LX/4iP;->getServerAddress()Ljava/net/InetAddress;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1108580
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->d:LX/1iW;

    invoke-virtual {v12}, LX/4iP;->getServerAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1iW;->a(Ljava/lang/String;)V

    .line 1108581
    :cond_5
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->d:LX/1iW;

    invoke-virtual {v12}, LX/4iP;->getIsNewConnection()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/1iW;->b(Z)V

    .line 1108582
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->d:LX/1iW;

    invoke-virtual {v12}, LX/4iP;->isSpdy()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/1iW;->a(Z)V

    .line 1108583
    :cond_6
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->f:LX/1iQ;

    invoke-virtual {v0}, LX/1iQ;->isFlowTimeSampled()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1108584
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "mobile_http_flow"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1108585
    const-string v1, "RequestStats"

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108586
    iget-object v1, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108587
    const-string v1, "weight"

    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->f:LX/1iQ;

    invoke-virtual {v2}, LX/1iQ;->getFlowTimeWeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108588
    invoke-virtual {p1}, LX/4iZ;->getFlowTimeData()Ljava/util/Map;

    move-result-object v1

    .line 1108589
    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108590
    iget-object v1, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->k:LX/1MY;

    invoke-interface {v1}, LX/1MY;->a()LX/763;

    move-result-object v1

    .line 1108591
    sget-object v2, LX/763;->UNAVAILABLE:LX/763;

    if-eq v1, v2, :cond_7

    .line 1108592
    const-string v2, "mqtt_status"

    invoke-virtual {v1}, LX/763;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108593
    :cond_7
    iget-object v1, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1108594
    :cond_8
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->f:LX/1iQ;

    invoke-virtual {v0}, LX/1iQ;->isCertSampled()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1108595
    invoke-virtual {p1}, LX/4iZ;->getCertificateVerificationData()Ljava/util/Map;

    move-result-object v0

    .line 1108596
    if-eqz v0, :cond_a

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    .line 1108597
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "cert_verification"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1108598
    const-string v2, "RequestStats"

    invoke-virtual {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108599
    const-string v2, "weight"

    const-wide/16 v4, 0x1388

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108600
    invoke-virtual {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108601
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->j:LX/0YR;

    invoke-interface {v0}, LX/0YR;->b()LX/1hP;

    move-result-object v0

    .line 1108602
    if-eqz v0, :cond_9

    .line 1108603
    invoke-virtual {v0}, LX/1hP;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108604
    :cond_9
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->c:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1108605
    :cond_a
    iget-object v0, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->f:LX/1iQ;

    invoke-virtual {v0}, LX/1iQ;->isCellTowerSampled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1108606
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "cell_tower_info"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1108607
    const-string v1, "RequestStats"

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108608
    iget-object v1, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->i:LX/1hI;

    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->h:Ljava/util/Map;

    invoke-virtual {v1, v2}, LX/1hI;->b(Ljava/util/Map;)V

    .line 1108609
    iget-object v1, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->i:LX/1hI;

    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->h:Ljava/util/Map;

    invoke-virtual {v1, v2}, LX/1hI;->c(Ljava/util/Map;)V

    .line 1108610
    iget-object v1, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->h:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108611
    iget-object v1, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->g:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108612
    const-string v1, "weight"

    iget-object v2, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->f:LX/1iQ;

    invoke-virtual {v2}, LX/1iQ;->getCellTowerWeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108613
    if-eqz v12, :cond_b

    .line 1108614
    const-string v1, "request_header_size"

    invoke-virtual {v12}, LX/4iP;->getRequestHeaderCompressedBytes()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108615
    const-string v1, "request_body_size"

    invoke-virtual {v12}, LX/4iP;->getRequestBodyBytes()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108616
    const-string v1, "response_header_size"

    invoke-virtual {v12}, LX/4iP;->getResponseHeaderCompressedBytes()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108617
    const-string v1, "response_body_size"

    invoke-virtual {v12}, LX/4iP;->getResponseBodyCompressedBytes()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108618
    const-string v1, "rtt"

    invoke-virtual {v12}, LX/4iP;->getRtt()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108619
    const-string v1, "ttfb"

    invoke-virtual {v12}, LX/4iP;->getTimeToFirstByte()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108620
    const-string v1, "ttlb"

    invoke-virtual {v12}, LX/4iP;->getTimeToLastByte()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108621
    const-string v1, "response_server_quality"

    invoke-virtual {v12}, LX/4iP;->getServerQuality()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1108622
    :cond_b
    iget-object v1, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->h:Ljava/util/Map;

    invoke-static {v1}, LX/1hI;->d(Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1108623
    iget-object v1, p0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0
.end method
