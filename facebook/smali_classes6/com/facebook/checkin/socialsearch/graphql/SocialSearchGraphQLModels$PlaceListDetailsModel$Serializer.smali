.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 894164
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel;

    new-instance v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 894165
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 894163
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel;LX/0nX;LX/0my;)V
    .locals 10

    .prologue
    .line 894166
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 894167
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 894168
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 894169
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 894170
    if-eqz v2, :cond_0

    .line 894171
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 894172
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 894173
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 894174
    if-eqz v2, :cond_1

    .line 894175
    const-string v3, "lightweight_recs"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 894176
    invoke-static {v1, v2, p1, p2}, LX/5G6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 894177
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 894178
    if-eqz v2, :cond_2

    .line 894179
    const-string v3, "list_items"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 894180
    invoke-static {v1, v2, p1, p2}, LX/5Is;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 894181
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 894182
    if-eqz v2, :cond_7

    .line 894183
    const-string v3, "world_view_bounding_box"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 894184
    const-wide/16 v8, 0x0

    .line 894185
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 894186
    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 894187
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_3

    .line 894188
    const-string v6, "east"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 894189
    invoke-virtual {p1, v4, v5}, LX/0nX;->a(D)V

    .line 894190
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {v1, v2, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 894191
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_4

    .line 894192
    const-string v6, "north"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 894193
    invoke-virtual {p1, v4, v5}, LX/0nX;->a(D)V

    .line 894194
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v1, v2, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 894195
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_5

    .line 894196
    const-string v6, "south"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 894197
    invoke-virtual {p1, v4, v5}, LX/0nX;->a(D)V

    .line 894198
    :cond_5
    const/4 v4, 0x3

    invoke-virtual {v1, v2, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 894199
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_6

    .line 894200
    const-string v6, "west"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 894201
    invoke-virtual {p1, v4, v5}, LX/0nX;->a(D)V

    .line 894202
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 894203
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 894204
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 894162
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$Serializer;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel;LX/0nX;LX/0my;)V

    return-void
.end method
