.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x41f49008
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 895327
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 895326
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 895324
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 895325
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 895321
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 895322
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 895323
    return-void
.end method

.method public static a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;
    .locals 8

    .prologue
    .line 895279
    if-nez p0, :cond_0

    .line 895280
    const/4 p0, 0x0

    .line 895281
    :goto_0
    return-object p0

    .line 895282
    :cond_0
    instance-of v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;

    if-eqz v0, :cond_1

    .line 895283
    check-cast p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;

    goto :goto_0

    .line 895284
    :cond_1
    new-instance v0, LX/5IV;

    invoke-direct {v0}, LX/5IV;-><init>()V

    .line 895285
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    move-result-object v1

    iput-object v1, v0, LX/5IV;->a:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    .line 895286
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 895287
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 895288
    iget-object v3, v0, LX/5IV;->a:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 895289
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 895290
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 895291
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 895292
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 895293
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 895294
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 895295
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 895296
    new-instance v3, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;

    invoke-direct {v3, v2}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;-><init>(LX/15i;)V

    .line 895297
    move-object p0, v3

    .line 895298
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 895315
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 895316
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 895317
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 895318
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 895319
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 895320
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 895307
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 895308
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 895309
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    .line 895310
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 895311
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;

    .line 895312
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    .line 895313
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 895314
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 895306
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 895303
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;-><init>()V

    .line 895304
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 895305
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 895302
    const v0, -0x3aafbfcc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 895301
    const v0, -0x4b900828

    return v0
.end method

.method public final j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 895299
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    .line 895300
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    return-object v0
.end method
