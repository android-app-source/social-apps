.class public final Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 890395
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel;

    new-instance v1, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 890396
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 890397
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 890398
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 890399
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 890400
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 890401
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 890402
    if-eqz v2, :cond_0

    .line 890403
    const-string p0, "comment"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 890404
    invoke-static {v1, v2, p1, p2}, LX/5HZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 890405
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 890406
    if-eqz v2, :cond_1

    .line 890407
    const-string p0, "removed_recommendation_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 890408
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 890409
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 890410
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 890411
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel$Serializer;->a(Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel;LX/0nX;LX/0my;)V

    return-void
.end method
