.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 894372
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;

    new-instance v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 894373
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 894388
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 894375
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 894376
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 894377
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 894378
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 894379
    if-eqz v2, :cond_0

    .line 894380
    const-string p0, "attachments"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 894381
    invoke-static {v1, v2, p1, p2}, LX/5Iv;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 894382
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 894383
    if-eqz v2, :cond_1

    .line 894384
    const-string p0, "place_recommendation_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 894385
    invoke-static {v1, v2, p1, p2}, LX/5GC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 894386
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 894387
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 894374
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel$Serializer;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;LX/0nX;LX/0my;)V

    return-void
.end method
