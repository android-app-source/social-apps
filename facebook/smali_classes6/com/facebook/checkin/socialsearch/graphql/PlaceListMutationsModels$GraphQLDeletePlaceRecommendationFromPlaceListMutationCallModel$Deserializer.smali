.class public final Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 891286
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    new-instance v1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 891287
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 891288
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 891289
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 891290
    const/4 v2, 0x0

    .line 891291
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 891292
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 891293
    :goto_0
    move v1, v2

    .line 891294
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 891295
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 891296
    new-instance v1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    invoke-direct {v1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;-><init>()V

    .line 891297
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 891298
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 891299
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 891300
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 891301
    :cond_0
    return-object v1

    .line 891302
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 891303
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_5

    .line 891304
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 891305
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 891306
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v4, :cond_2

    .line 891307
    const-string p0, "changed_pages"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 891308
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 891309
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, p0, :cond_3

    .line 891310
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, p0, :cond_3

    .line 891311
    invoke-static {p1, v0}, LX/5Hw;->b(LX/15w;LX/186;)I

    move-result v4

    .line 891312
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 891313
    :cond_3
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 891314
    goto :goto_1

    .line 891315
    :cond_4
    const-string p0, "story"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 891316
    invoke-static {p1, v0}, LX/5I1;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 891317
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 891318
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 891319
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 891320
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_1
.end method
