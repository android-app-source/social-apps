.class public final Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 891285
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 891284
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 891282
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 891283
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 891276
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 891277
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 891278
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 891279
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 891280
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 891281
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 891273
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 891274
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 891275
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 891272
    new-instance v0, LX/5Hn;

    invoke-direct {v0, p1}, LX/5Hn;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 891261
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 891270
    invoke-virtual {p2}, LX/18L;->a()V

    .line 891271
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 891269
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 891266
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel;-><init>()V

    .line 891267
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 891268
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 891265
    const v0, -0x6dc8bd0f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 891264
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 891262
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel;->e:Ljava/lang/String;

    .line 891263
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel;->e:Ljava/lang/String;

    return-object v0
.end method
