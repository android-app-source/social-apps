.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x78985ecf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 896423
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 896375
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 896421
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 896422
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 896418
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 896419
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 896420
    return-void
.end method

.method public static a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;
    .locals 8
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 896398
    if-nez p0, :cond_0

    .line 896399
    const/4 p0, 0x0

    .line 896400
    :goto_0
    return-object p0

    .line 896401
    :cond_0
    instance-of v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;

    if-eqz v0, :cond_1

    .line 896402
    check-cast p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;

    goto :goto_0

    .line 896403
    :cond_1
    new-instance v0, LX/5J1;

    invoke-direct {v0}, LX/5J1;-><init>()V

    .line 896404
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    move-result-object v1

    iput-object v1, v0, LX/5J1;->a:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    .line 896405
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 896406
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 896407
    iget-object v3, v0, LX/5J1;->a:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 896408
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 896409
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 896410
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 896411
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 896412
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 896413
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 896414
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 896415
    new-instance v3, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;

    invoke-direct {v3, v2}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;-><init>(LX/15i;)V

    .line 896416
    move-object p0, v3

    .line 896417
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 896392
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 896393
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 896394
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 896395
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 896396
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 896397
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 896384
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 896385
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 896386
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    .line 896387
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 896388
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;

    .line 896389
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    .line 896390
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 896391
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTarget"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 896383
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 896380
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;-><init>()V

    .line 896381
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 896382
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 896379
    const v0, 0x2afe62b3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 896378
    const v0, -0x4b900828

    return v0
.end method

.method public final j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTarget"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 896376
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    .line 896377
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    return-object v0
.end method
