.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2f1a17ae
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 893775
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 893774
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 893772
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 893773
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 893764
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 893765
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 893766
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 893767
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 893768
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 893769
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 893770
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 893771
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 893751
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 893752
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 893753
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    .line 893754
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 893755
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;

    .line 893756
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    .line 893757
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 893758
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    .line 893759
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 893760
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;

    .line 893761
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->f:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    .line 893762
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 893763
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 893742
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    .line 893743
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 893748
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;-><init>()V

    .line 893749
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 893750
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 893747
    const v0, 0x62ed5853

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 893746
    const v0, 0x346ba01b

    return v0
.end method

.method public final j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 893744
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->f:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->f:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    .line 893745
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->f:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    return-object v0
.end method
