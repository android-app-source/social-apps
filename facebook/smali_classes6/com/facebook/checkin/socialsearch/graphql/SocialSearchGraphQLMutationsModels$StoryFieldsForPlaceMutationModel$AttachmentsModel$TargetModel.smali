.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3d7939a9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 895277
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 895276
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 895274
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 895275
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 895271
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 895272
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 895273
    return-void
.end method

.method public static a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;
    .locals 9

    .prologue
    .line 895246
    if-nez p0, :cond_0

    .line 895247
    const/4 p0, 0x0

    .line 895248
    :goto_0
    return-object p0

    .line 895249
    :cond_0
    instance-of v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    if-eqz v0, :cond_1

    .line 895250
    check-cast p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    goto :goto_0

    .line 895251
    :cond_1
    new-instance v0, LX/5IW;

    invoke-direct {v0}, LX/5IW;-><init>()V

    .line 895252
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/5IW;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 895253
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->c()I

    move-result v1

    iput v1, v0, LX/5IW;->b:I

    .line 895254
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5IW;->c:Ljava/lang/String;

    .line 895255
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v8, 0x0

    .line 895256
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 895257
    iget-object v3, v0, LX/5IW;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 895258
    iget-object v5, v0, LX/5IW;->c:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 895259
    const/4 v7, 0x3

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 895260
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 895261
    iget v3, v0, LX/5IW;->b:I

    invoke-virtual {v2, v6, v3, v8}, LX/186;->a(III)V

    .line 895262
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 895263
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 895264
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 895265
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 895266
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 895267
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 895268
    new-instance v3, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    invoke-direct {v3, v2}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;-><init>(LX/15i;)V

    .line 895269
    move-object p0, v3

    .line 895270
    goto :goto_0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 895240
    iput p1, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->f:I

    .line 895241
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 895242
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 895243
    if-eqz v0, :cond_0

    .line 895244
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 895245
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 895231
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 895232
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 895233
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 895234
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 895235
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 895236
    const/4 v0, 0x1

    iget v2, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->f:I

    invoke-virtual {p1, v0, v2, v3}, LX/186;->a(III)V

    .line 895237
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 895238
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 895239
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 895228
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 895229
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 895230
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 895227
    new-instance v0, LX/5IX;

    invoke-direct {v0, p1}, LX/5IX;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 895278
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 895224
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 895225
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->f:I

    .line 895226
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 895218
    const-string v0, "distinct_recommenders_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 895219
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 895220
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 895221
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 895222
    :goto_0
    return-void

    .line 895223
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 895215
    const-string v0, "distinct_recommenders_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 895216
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->a(I)V

    .line 895217
    :cond_0
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 895203
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 895204
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 895205
    :cond_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 895212
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;-><init>()V

    .line 895213
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 895214
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 895210
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 895211
    iget v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->f:I

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 895208
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->g:Ljava/lang/String;

    .line 895209
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 895207
    const v0, 0x28d7b46a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 895206
    const v0, 0x252222

    return v0
.end method
