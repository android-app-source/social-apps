.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3787169d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 893968
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 893967
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 893965
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 893966
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 893931
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;->e:Ljava/util/List;

    .line 893932
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 893933
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 893934
    if-eqz v0, :cond_0

    .line 893935
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 893936
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 893959
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 893960
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 893961
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 893962
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 893963
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 893964
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 893957
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;->e:Ljava/util/List;

    .line 893958
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 893949
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 893950
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 893951
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 893952
    if-eqz v1, :cond_0

    .line 893953
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;

    .line 893954
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;->e:Ljava/util/List;

    .line 893955
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 893956
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 893948
    new-instance v0, LX/5IF;

    invoke-direct {v0, p1}, LX/5IF;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 893946
    invoke-virtual {p2}, LX/18L;->a()V

    .line 893947
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 893943
    const-string v0, "attachments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 893944
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;->a(Ljava/util/List;)V

    .line 893945
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 893942
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 893939
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;-><init>()V

    .line 893940
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 893941
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 893938
    const v0, -0x5c643da6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 893937
    const v0, 0x4c808d5

    return v0
.end method
