.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 895034
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;

    new-instance v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 895035
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 895036
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 895037
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 895038
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 895039
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 895040
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 895041
    if-eqz v2, :cond_0

    .line 895042
    const-string p0, "changed_pages"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895043
    invoke-static {v1, v2, p1, p2}, LX/5Ec;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 895044
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 895045
    if-eqz v2, :cond_1

    .line 895046
    const-string p0, "comment"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895047
    invoke-static {v1, v2, p1, p2}, LX/5Ie;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 895048
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 895049
    if-eqz v2, :cond_2

    .line 895050
    const-string p0, "story"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 895051
    invoke-static {v1, v2, p1, p2}, LX/5Ii;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 895052
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 895053
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 895054
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel$Serializer;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
