.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$PlaceListAskFriendsForRecommendationsMutationModel$InvitedFriendsInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4838bfe6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$PlaceListAskFriendsForRecommendationsMutationModel$InvitedFriendsInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$PlaceListAskFriendsForRecommendationsMutationModel$InvitedFriendsInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 895101
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$PlaceListAskFriendsForRecommendationsMutationModel$InvitedFriendsInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 895117
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$PlaceListAskFriendsForRecommendationsMutationModel$InvitedFriendsInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 895115
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 895116
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 895110
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 895111
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 895112
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$PlaceListAskFriendsForRecommendationsMutationModel$InvitedFriendsInfoModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 895113
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 895114
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 895118
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 895119
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 895120
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 895107
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 895108
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$PlaceListAskFriendsForRecommendationsMutationModel$InvitedFriendsInfoModel;->e:Z

    .line 895109
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 895104
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$PlaceListAskFriendsForRecommendationsMutationModel$InvitedFriendsInfoModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$PlaceListAskFriendsForRecommendationsMutationModel$InvitedFriendsInfoModel;-><init>()V

    .line 895105
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 895106
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 895103
    const v0, 0x28008ef6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 895102
    const v0, -0x6f94e693

    return v0
.end method
