.class public final Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x261b0ed9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 891744
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 891743
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 891731
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 891732
    return-void
.end method

.method private j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 891741
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    .line 891742
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 891733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 891734
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->c(Ljava/util/List;)I

    move-result v0

    .line 891735
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 891736
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 891737
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 891738
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 891739
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 891740
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 891745
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->e:Ljava/util/List;

    .line 891746
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 891721
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 891722
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 891723
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    .line 891724
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 891725
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;

    .line 891726
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    .line 891727
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 891728
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic b()LX/580;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 891717
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 891718
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;-><init>()V

    .line 891719
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 891720
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 891729
    const v0, -0x41efa8dc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 891730
    const v0, -0x4b900828

    return v0
.end method
