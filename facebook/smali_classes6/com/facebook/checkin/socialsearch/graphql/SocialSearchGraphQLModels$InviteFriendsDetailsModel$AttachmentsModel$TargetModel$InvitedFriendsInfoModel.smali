.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x74be7f0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 893808
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 893811
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 893809
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 893810
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 893799
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 893800
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 893801
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 893802
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;->e:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 893803
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 893804
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 893805
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 893806
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;->f:Ljava/util/List;

    .line 893807
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 893791
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 893792
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 893793
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 893794
    if-eqz v1, :cond_0

    .line 893795
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;

    .line 893796
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;->f:Ljava/util/List;

    .line 893797
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 893798
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 893788
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 893789
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;->e:Z

    .line 893790
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 893785
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;-><init>()V

    .line 893786
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 893787
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 893784
    const v0, 0x11d94f20

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 893783
    const v0, -0x6f94e693

    return v0
.end method
