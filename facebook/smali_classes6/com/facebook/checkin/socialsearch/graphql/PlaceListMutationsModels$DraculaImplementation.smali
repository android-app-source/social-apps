.class public final Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 891143
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 891144
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 891141
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 891142
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 891127
    if-nez p1, :cond_0

    .line 891128
    :goto_0
    return v0

    .line 891129
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 891130
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 891131
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 891132
    const v2, 0x30818145

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 891133
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 891134
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 891135
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 891136
    :sswitch_1
    const-class v1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel$TargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel$RecommendingCommentsNodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 891137
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 891138
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 891139
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 891140
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x12459cd0 -> :sswitch_0
        0x30818145 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 891118
    if-nez p0, :cond_0

    move v0, v1

    .line 891119
    :goto_0
    return v0

    .line 891120
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 891121
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 891122
    :goto_1
    if-ge v1, v2, :cond_2

    .line 891123
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 891124
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 891125
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 891126
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 891111
    const/4 v7, 0x0

    .line 891112
    const/4 v1, 0x0

    .line 891113
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 891114
    invoke-static {v2, v3, v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 891115
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 891116
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 891117
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 891110
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 891106
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 891107
    if-eqz v0, :cond_0

    .line 891108
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 891109
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 891099
    sparse-switch p2, :sswitch_data_0

    .line 891100
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 891101
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 891102
    const v1, 0x30818145

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 891103
    :goto_0
    return-void

    .line 891104
    :sswitch_1
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel$TargetModel$ListItemsModel$NodesModel$RecommendingCommentsModel$RecommendingCommentsNodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 891105
    invoke-static {v0, p3}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x12459cd0 -> :sswitch_0
        0x30818145 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 891093
    if-eqz p1, :cond_0

    .line 891094
    invoke-static {p0, p1, p2}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;

    move-result-object v1

    .line 891095
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;

    .line 891096
    if-eq v0, v1, :cond_0

    .line 891097
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 891098
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 891145
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 891060
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 891061
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 891065
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 891066
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 891067
    :cond_0
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;->a:LX/15i;

    .line 891068
    iput p2, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;->b:I

    .line 891069
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 891070
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 891071
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 891072
    iget v0, p0, LX/1vt;->c:I

    .line 891073
    move v0, v0

    .line 891074
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 891075
    iget v0, p0, LX/1vt;->c:I

    .line 891076
    move v0, v0

    .line 891077
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 891078
    iget v0, p0, LX/1vt;->b:I

    .line 891079
    move v0, v0

    .line 891080
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 891062
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 891063
    move-object v0, v0

    .line 891064
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 891081
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 891082
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 891083
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 891084
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 891085
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 891086
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 891087
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 891088
    invoke-static {v3, v9, v2}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 891089
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 891090
    iget v0, p0, LX/1vt;->c:I

    .line 891091
    move v0, v0

    .line 891092
    return v0
.end method
