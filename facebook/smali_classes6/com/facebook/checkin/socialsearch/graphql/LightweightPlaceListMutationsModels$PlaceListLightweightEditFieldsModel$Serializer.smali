.class public final Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightEditFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightEditFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 890348
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightEditFieldsModel;

    new-instance v1, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightEditFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightEditFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 890349
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 890350
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightEditFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 890351
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 890352
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 890353
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 890354
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 890355
    if-eqz v2, :cond_0

    .line 890356
    const-string p0, "comment"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 890357
    invoke-static {v1, v2, p1, p2}, LX/5HW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 890358
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 890359
    if-eqz v2, :cond_1

    .line 890360
    const-string p0, "edited_recommendation"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 890361
    invoke-static {v1, v2, p1, p2}, LX/5G6;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 890362
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 890363
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 890364
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightEditFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightEditFieldsModel$Serializer;->a(Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightEditFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
