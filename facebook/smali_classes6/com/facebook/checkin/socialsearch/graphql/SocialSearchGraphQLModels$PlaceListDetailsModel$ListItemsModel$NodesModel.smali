.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x62b71cf3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$RecommendingCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 894117
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 894116
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 894082
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 894083
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 894114
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->e:Ljava/lang/String;

    .line 894115
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 894112
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 894113
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$RecommendingCommentsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRecommendingComments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 894110
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->h:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->h:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    .line 894111
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->h:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 894118
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 894119
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 894120
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 894121
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 894122
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->m()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 894123
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 894124
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 894125
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 894126
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 894127
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 894128
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 894129
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 894092
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 894093
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 894094
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;

    .line 894095
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 894096
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;

    .line 894097
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;

    .line 894098
    :cond_0
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 894099
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 894100
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 894101
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;

    .line 894102
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 894103
    :cond_1
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->m()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 894104
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->m()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    .line 894105
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->m()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 894106
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;

    .line 894107
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->h:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel$RecommendingCommentsModel;

    .line 894108
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 894109
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 894091
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 894088
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;-><init>()V

    .line 894089
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 894090
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 894087
    const v0, 0x4f602f61

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 894086
    const v0, 0x7dfc96d8

    return v0
.end method

.method public final j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 894084
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;

    .line 894085
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;

    return-object v0
.end method
