.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 893587
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 893588
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 893585
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 893586
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    .line 893560
    if-nez p1, :cond_0

    .line 893561
    const/4 v0, 0x0

    .line 893562
    :goto_0
    return v0

    .line 893563
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 893564
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 893565
    :sswitch_0
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 893566
    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 893567
    const/4 v0, 0x2

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 893568
    const/4 v0, 0x3

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v10

    .line 893569
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 893570
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 893571
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 893572
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 893573
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v10

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 893574
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 893575
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 893576
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 893577
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 893578
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 893579
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 893580
    :sswitch_2
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->c(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 893581
    invoke-virtual {p3, v0}, LX/186;->c(Ljava/util/List;)I

    move-result v0

    .line 893582
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 893583
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 893584
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3c3bb180 -> :sswitch_1
        -0x22a641d5 -> :sswitch_0
        0x3e3a976b -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 893551
    if-nez p0, :cond_0

    move v0, v1

    .line 893552
    :goto_0
    return v0

    .line 893553
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 893554
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 893555
    :goto_1
    if-ge v1, v2, :cond_2

    .line 893556
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 893557
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 893558
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 893559
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 893544
    const/4 v7, 0x0

    .line 893545
    const/4 v1, 0x0

    .line 893546
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 893547
    invoke-static {v2, v3, v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 893548
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 893549
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 893550
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 893543
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 893540
    sparse-switch p0, :sswitch_data_0

    .line 893541
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 893542
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x3c3bb180 -> :sswitch_0
        -0x22a641d5 -> :sswitch_0
        0x3e3a976b -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 893539
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 893589
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;->b(I)V

    .line 893590
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 893534
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 893535
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 893536
    :cond_0
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 893537
    iput p2, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;->b:I

    .line 893538
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 893533
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 893532
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 893529
    iget v0, p0, LX/1vt;->c:I

    .line 893530
    move v0, v0

    .line 893531
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 893526
    iget v0, p0, LX/1vt;->c:I

    .line 893527
    move v0, v0

    .line 893528
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 893523
    iget v0, p0, LX/1vt;->b:I

    .line 893524
    move v0, v0

    .line 893525
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 893520
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 893521
    move-object v0, v0

    .line 893522
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 893511
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 893512
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 893513
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 893514
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 893515
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 893516
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 893517
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 893518
    invoke-static {v3, v9, v2}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 893519
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 893508
    iget v0, p0, LX/1vt;->c:I

    .line 893509
    move v0, v0

    .line 893510
    return v0
.end method
