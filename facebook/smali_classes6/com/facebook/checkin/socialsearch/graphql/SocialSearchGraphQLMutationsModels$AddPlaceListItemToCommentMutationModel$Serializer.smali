.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 894508
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;

    new-instance v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 894509
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 894510
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 894511
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 894512
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 894513
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 894514
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 894515
    if-eqz v2, :cond_0

    .line 894516
    const-string p0, "changed_pages"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 894517
    invoke-static {v1, v2, p1, p2}, LX/5Ec;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 894518
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 894519
    if-eqz v2, :cond_1

    .line 894520
    const-string p0, "comment"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 894521
    invoke-static {v1, v2, p1, p2}, LX/5Ie;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 894522
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 894523
    if-eqz v2, :cond_2

    .line 894524
    const-string p0, "story"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 894525
    invoke-static {v1, v2, p1, p2}, LX/5Ii;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 894526
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 894527
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 894528
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel$Serializer;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
