.class public final Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 891339
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    new-instance v1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 891340
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 891341
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 891322
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 891323
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 891324
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 891325
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 891326
    if-eqz v2, :cond_1

    .line 891327
    const-string v3, "changed_pages"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 891328
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 891329
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_0

    .line 891330
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/5Hw;->a(LX/15i;ILX/0nX;)V

    .line 891331
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 891332
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 891333
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 891334
    if-eqz v2, :cond_2

    .line 891335
    const-string v3, "story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 891336
    invoke-static {v1, v2, p1, p2}, LX/5I1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 891337
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 891338
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 891321
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$Serializer;->a(Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;LX/0nX;LX/0my;)V

    return-void
.end method
