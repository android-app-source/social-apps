.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x55590c1f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 894984
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 894987
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 894985
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 894986
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 894939
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 894940
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 894941
    return-void
.end method

.method public static a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;
    .locals 12
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 894954
    if-nez p0, :cond_0

    .line 894955
    const/4 p0, 0x0

    .line 894956
    :goto_0
    return-object p0

    .line 894957
    :cond_0
    instance-of v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    if-eqz v0, :cond_1

    .line 894958
    check-cast p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    goto :goto_0

    .line 894959
    :cond_1
    new-instance v2, LX/5IR;

    invoke-direct {v2}, LX/5IR;-><init>()V

    .line 894960
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 894961
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 894962
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 894963
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 894964
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/5IR;->a:LX/0Px;

    .line 894965
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/5IR;->b:Ljava/lang/String;

    .line 894966
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->c()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    move-result-object v0

    iput-object v0, v2, LX/5IR;->c:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    .line 894967
    const/4 v8, 0x1

    const/4 v11, 0x0

    const/4 v6, 0x0

    .line 894968
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 894969
    iget-object v5, v2, LX/5IR;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 894970
    iget-object v7, v2, LX/5IR;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 894971
    iget-object v9, v2, LX/5IR;->c:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    invoke-static {v4, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 894972
    const/4 v10, 0x3

    invoke-virtual {v4, v10}, LX/186;->c(I)V

    .line 894973
    invoke-virtual {v4, v11, v5}, LX/186;->b(II)V

    .line 894974
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 894975
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 894976
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 894977
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 894978
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 894979
    invoke-virtual {v5, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 894980
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 894981
    new-instance v5, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    invoke-direct {v5, v4}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;-><init>(LX/15i;)V

    .line 894982
    move-object p0, v5

    .line 894983
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 894952
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->g:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->g:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    .line 894953
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->g:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 894942
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 894943
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->d()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 894944
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 894945
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 894946
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 894947
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 894948
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 894949
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 894950
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 894951
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 894988
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 894989
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->d()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 894990
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->d()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 894991
    if-eqz v1, :cond_2

    .line 894992
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    .line 894993
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 894994
    :goto_0
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 894995
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    .line 894996
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 894997
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    .line 894998
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->g:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    .line 894999
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 895000
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 894928
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 894929
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;-><init>()V

    .line 894930
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 894931
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 894932
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->f:Ljava/lang/String;

    .line 894933
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 894934
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$ParentFeedbackModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttachments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 894935
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->e:Ljava/util/List;

    .line 894936
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 894937
    const v0, -0x72e1b70e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 894938
    const v0, -0x642179c1

    return v0
.end method
