.class public final Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 894322
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;

    new-instance v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 894323
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 894324
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 894325
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 894326
    const/4 v2, 0x0

    .line 894327
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 894328
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 894329
    :goto_0
    move v1, v2

    .line 894330
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 894331
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 894332
    new-instance v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;

    invoke-direct {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;-><init>()V

    .line 894333
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 894334
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 894335
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 894336
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 894337
    :cond_0
    return-object v1

    .line 894338
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 894339
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 894340
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 894341
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 894342
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 894343
    const-string v5, "attachments"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 894344
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 894345
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_3

    .line 894346
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_3

    .line 894347
    const/4 v5, 0x0

    .line 894348
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_a

    .line 894349
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 894350
    :goto_3
    move v4, v5

    .line 894351
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 894352
    :cond_3
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 894353
    goto :goto_1

    .line 894354
    :cond_4
    const-string v5, "place_recommendation_info"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 894355
    invoke-static {p1, v0}, LX/5GC;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 894356
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 894357
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 894358
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 894359
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 894360
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 894361
    :cond_8
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_9

    .line 894362
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 894363
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 894364
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_8

    if-eqz v6, :cond_8

    .line 894365
    const-string p0, "style_list"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 894366
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_4

    .line 894367
    :cond_9
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 894368
    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 894369
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_3

    :cond_a
    move v4, v5

    goto :goto_4
.end method
