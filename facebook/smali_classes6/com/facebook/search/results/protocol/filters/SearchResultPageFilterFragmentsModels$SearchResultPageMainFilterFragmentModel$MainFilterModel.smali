.class public final Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/5uu;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1c490917
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1020947
    const-class v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1020948
    const-class v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1020949
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1020950
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1020951
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1020952
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1020953
    return-void
.end method

.method public static a(LX/5uu;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;
    .locals 2

    .prologue
    .line 1020954
    if-nez p0, :cond_0

    .line 1020955
    const/4 p0, 0x0

    .line 1020956
    :goto_0
    return-object p0

    .line 1020957
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    if-eqz v0, :cond_1

    .line 1020958
    check-cast p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    goto :goto_0

    .line 1020959
    :cond_1
    new-instance v0, LX/5v2;

    invoke-direct {v0}, LX/5v2;-><init>()V

    .line 1020960
    invoke-interface {p0}, LX/5uu;->b()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;->a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->a:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    .line 1020961
    invoke-interface {p0}, LX/5uu;->c()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;->a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->b:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    .line 1020962
    invoke-interface {p0}, LX/5uu;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->c:Ljava/lang/String;

    .line 1020963
    invoke-interface {p0}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->d:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    .line 1020964
    invoke-interface {p0}, LX/5uu;->bk_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->e:Ljava/lang/String;

    .line 1020965
    invoke-interface {p0}, LX/5uu;->bl_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->f:Ljava/lang/String;

    .line 1020966
    invoke-interface {p0}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->g:Ljava/lang/String;

    .line 1020967
    invoke-interface {p0}, LX/5uu;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->h:Ljava/lang/String;

    .line 1020968
    invoke-interface {p0}, LX/5uu;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->i:Ljava/lang/String;

    .line 1020969
    invoke-virtual {v0}, LX/5v2;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 1020970
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1020971
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->m()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1020972
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->n()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1020973
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1020974
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->o()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1020975
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->bk_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1020976
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->bl_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1020977
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1020978
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->k()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1020979
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1020980
    const/16 v9, 0x9

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1020981
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 1020982
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1020983
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1020984
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1020985
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1020986
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1020987
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1020988
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1020989
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1020990
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1020991
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1020992
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1020993
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->m()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1020994
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->m()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    .line 1020995
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->m()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1020996
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    .line 1020997
    iput-object v0, v1, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->e:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    .line 1020998
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->n()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1020999
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->n()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    .line 1021000
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->n()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1021001
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    .line 1021002
    iput-object v0, v1, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->f:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    .line 1021003
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->o()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1021004
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->o()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    .line 1021005
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->o()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1021006
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    .line 1021007
    iput-object v0, v1, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->h:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    .line 1021008
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1021009
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1020939
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->bl_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1021010
    new-instance v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;-><init>()V

    .line 1021011
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1021012
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1021013
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->m()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    move-result-object v0

    return-object v0
.end method

.method public final bk_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1020945
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->i:Ljava/lang/String;

    .line 1020946
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final bl_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1021014
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->j:Ljava/lang/String;

    .line 1021015
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1020944
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->n()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1020942
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->g:Ljava/lang/String;

    .line 1020943
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1020941
    const v0, -0x15be0062

    return v0
.end method

.method public final synthetic e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1020940
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->o()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1020938
    const v0, -0x55db0bf6

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1020936
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->k:Ljava/lang/String;

    .line 1020937
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1020934
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->l:Ljava/lang/String;

    .line 1020935
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1020932
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->m:Ljava/lang/String;

    .line 1020933
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1020930
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->e:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->e:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    .line 1020931
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->e:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1020928
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->f:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->f:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    .line 1020929
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->f:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    return-object v0
.end method

.method public final o()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1020926
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->h:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->h:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    .line 1020927
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->h:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    return-object v0
.end method
