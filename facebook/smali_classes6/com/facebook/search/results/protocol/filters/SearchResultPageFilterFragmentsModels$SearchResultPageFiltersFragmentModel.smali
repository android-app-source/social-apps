.class public final Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2cfa8417
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1020846
    const-class v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1020845
    const-class v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1020843
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1020844
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1020806
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1020807
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1020808
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;
    .locals 10

    .prologue
    .line 1020819
    if-nez p0, :cond_0

    .line 1020820
    const/4 p0, 0x0

    .line 1020821
    :goto_0
    return-object p0

    .line 1020822
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    if-eqz v0, :cond_1

    .line 1020823
    check-cast p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    goto :goto_0

    .line 1020824
    :cond_1
    new-instance v2, LX/5v0;

    invoke-direct {v2}, LX/5v0;-><init>()V

    .line 1020825
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1020826
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1020827
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;

    invoke-static {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;->a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1020828
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1020829
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/5v0;->a:LX/0Px;

    .line 1020830
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1020831
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1020832
    iget-object v5, v2, LX/5v0;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1020833
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 1020834
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 1020835
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1020836
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1020837
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1020838
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1020839
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1020840
    new-instance v5, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    invoke-direct {v5, v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;-><init>(LX/15i;)V

    .line 1020841
    move-object p0, v5

    .line 1020842
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1020847
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1020848
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1020849
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1020850
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1020851
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1020852
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1020817
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->e:Ljava/util/List;

    .line 1020818
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1020809
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1020810
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1020811
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1020812
    if-eqz v1, :cond_0

    .line 1020813
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    .line 1020814
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->e:Ljava/util/List;

    .line 1020815
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1020816
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1020803
    new-instance v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;-><init>()V

    .line 1020804
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1020805
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1020802
    const v0, 0x634b35cf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1020801
    const v0, 0x434b975e

    return v0
.end method
