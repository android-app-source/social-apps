.class public Lcom/facebook/search/results/protocol/filters/FilterValue;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Ljava/lang/String;

.field public final f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1020350
    new-instance v0, LX/5uq;

    invoke-direct {v0}, LX/5uq;-><init>()V

    sput-object v0, Lcom/facebook/search/results/protocol/filters/FilterValue;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5ur;)V
    .locals 1

    .prologue
    .line 1020337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1020338
    iget-object v0, p1, LX/5ur;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1020339
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->a:Ljava/lang/String;

    .line 1020340
    iget-object v0, p1, LX/5ur;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1020341
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->b:Ljava/lang/String;

    .line 1020342
    iget-object v0, p1, LX/5ur;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1020343
    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->c:Ljava/lang/String;

    .line 1020344
    iget-boolean v0, p1, LX/5ur;->d:Z

    move v0, v0

    .line 1020345
    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->d:Z

    .line 1020346
    iget-object v0, p1, LX/5ur;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1020347
    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->e:Ljava/lang/String;

    .line 1020348
    iget-boolean v0, p1, LX/5ur;->f:Z

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->f:Z

    .line 1020349
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1020327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1020328
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->a:Ljava/lang/String;

    .line 1020329
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->b:Ljava/lang/String;

    .line 1020330
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->c:Ljava/lang/String;

    .line 1020331
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->d:Z

    .line 1020332
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->e:Ljava/lang/String;

    .line 1020333
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->f:Z

    .line 1020334
    return-void

    :cond_0
    move v0, v2

    .line 1020335
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1020336
    goto :goto_1
.end method

.method public static g()LX/5ur;
    .locals 2

    .prologue
    .line 1020298
    new-instance v0, LX/5ur;

    invoke-direct {v0}, LX/5ur;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1020326
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1020323
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/search/results/protocol/filters/FilterValue;

    if-nez v0, :cond_1

    .line 1020324
    :cond_0
    const/4 v0, 0x0

    .line 1020325
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->b:Ljava/lang/String;

    check-cast p1, Lcom/facebook/search/results/protocol/filters/FilterValue;

    iget-object v1, p1, Lcom/facebook/search/results/protocol/filters/FilterValue;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1020322
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1020308
    new-instance v0, LX/0lp;

    invoke-direct {v0}, LX/0lp;-><init>()V

    .line 1020309
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 1020310
    :try_start_0
    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v0

    .line 1020311
    invoke-virtual {v0}, LX/0nX;->f()V

    .line 1020312
    const-string v2, "value"

    iget-object v3, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020313
    const-string v2, "text"

    iget-object v3, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020314
    const-string v2, "name"

    iget-object v3, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020315
    const-string v2, "is_selected"

    iget-boolean v3, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->d:Z

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020316
    const-string v2, "profile_picture_uri"

    iget-object v3, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020317
    const-string v2, "is_fuzzy"

    iget-boolean v3, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->f:Z

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020318
    invoke-virtual {v0}, LX/0nX;->g()V

    .line 1020319
    invoke-virtual {v0}, LX/0nX;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1020320
    invoke-virtual {v1}, Ljava/io/StringWriter;->getBuffer()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1020321
    :catch_0
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1020299
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1020300
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1020301
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1020302
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1020303
    iget-object v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1020304
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/filters/FilterValue;->f:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1020305
    return-void

    :cond_0
    move v0, v2

    .line 1020306
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1020307
    goto :goto_1
.end method
