.class public Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/interstitial/api/InterstitialNUXFetchResultDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final extraData:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "extra_data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final steps:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "steps"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/NuxStep;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1108863
    const-class v0, Lcom/facebook/interstitial/api/InterstitialNUXFetchResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1108864
    new-instance v0, LX/6Xo;

    invoke-direct {v0}, LX/6Xo;-><init>()V

    sput-object v0, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1108865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108866
    iput-object v0, p0, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->steps:Ljava/util/List;

    .line 1108867
    iput-object v0, p0, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->extraData:Ljava/util/Map;

    .line 1108868
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1108869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108870
    const-class v0, Lcom/facebook/ipc/model/NuxStep;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->steps:Ljava/util/List;

    .line 1108871
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->extraData:Ljava/util/Map;

    .line 1108872
    iget-object v0, p0, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->extraData:Ljava/util/Map;

    const-class v1, Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1108873
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1108874
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1108875
    iget-object v0, p0, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->steps:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1108876
    iget-object v0, p0, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->extraData:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1108877
    return-void
.end method
