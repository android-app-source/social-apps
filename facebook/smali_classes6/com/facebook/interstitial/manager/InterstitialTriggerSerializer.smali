.class public Lcom/facebook/interstitial/manager/InterstitialTriggerSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1109071
    const-class v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTriggerSerializer;

    invoke-direct {v1}, Lcom/facebook/interstitial/manager/InterstitialTriggerSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1109072
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1109070
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/interstitial/manager/InterstitialTrigger;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1109073
    if-nez p0, :cond_0

    .line 1109074
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1109075
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1109076
    invoke-static {p0, p1, p2}, Lcom/facebook/interstitial/manager/InterstitialTriggerSerializer;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;LX/0nX;LX/0my;)V

    .line 1109077
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1109078
    return-void
.end method

.method private static b(Lcom/facebook/interstitial/manager/InterstitialTrigger;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1109067
    const-string v0, "action"

    iget-object v1, p0, Lcom/facebook/interstitial/manager/InterstitialTrigger;->action:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1109068
    const-string v0, "activity_class"

    iget-object v1, p0, Lcom/facebook/interstitial/manager/InterstitialTrigger;->activityClass:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1109069
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1109066
    check-cast p1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {p1, p2, p3}, Lcom/facebook/interstitial/manager/InterstitialTriggerSerializer;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;LX/0nX;LX/0my;)V

    return-void
.end method
