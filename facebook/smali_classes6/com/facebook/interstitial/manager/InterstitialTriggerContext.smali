.class public Lcom/facebook/interstitial/manager/InterstitialTriggerContext;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTriggerContext;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1109041
    new-instance v0, LX/6Xy;

    invoke-direct {v0}, LX/6Xy;-><init>()V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1109037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109038
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1109039
    iput-object v0, p0, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a:LX/0P1;

    .line 1109040
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1109034
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109035
    const-class v0, Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a:LX/0P1;

    .line 1109036
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1109031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1109032
    invoke-static {p1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a:LX/0P1;

    .line 1109033
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1109030
    iget-object v0, p0, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109019
    iget-object v0, p0, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1109029
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1109024
    instance-of v0, p1, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    if-eqz v0, :cond_0

    .line 1109025
    check-cast p1, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    .line 1109026
    iget-object v0, p0, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a:LX/0P1;

    iget-object v1, p1, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a:LX/0P1;

    invoke-virtual {v0, v1}, LX/0P1;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1109027
    const/4 v0, 0x1

    .line 1109028
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1109023
    iget-object v0, p0, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a:LX/0P1;

    invoke-static {v0}, LX/1bi;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109022
    iget-object v0, p0, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1109020
    iget-object v0, p0, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1109021
    return-void
.end method
