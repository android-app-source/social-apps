.class public Lcom/facebook/interstitial/manager/InterstitialLogger;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0iA;

.field private final c:LX/0Wd;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Xp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1108975
    const-class v0, LX/0iA;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialLogger;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0iA;LX/0Wd;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0iA;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Xp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1108976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108977
    iput-object p1, p0, Lcom/facebook/interstitial/manager/InterstitialLogger;->b:LX/0iA;

    .line 1108978
    iput-object p2, p0, Lcom/facebook/interstitial/manager/InterstitialLogger;->c:LX/0Wd;

    .line 1108979
    iput-object p3, p0, Lcom/facebook/interstitial/manager/InterstitialLogger;->d:LX/0Ot;

    .line 1108980
    iput-object p4, p0, Lcom/facebook/interstitial/manager/InterstitialLogger;->e:LX/0Ot;

    .line 1108981
    return-void
.end method

.method public static a(Lcom/facebook/interstitial/manager/InterstitialLogger;Ljava/lang/String;LX/6Xs;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/6Xs;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1108982
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Lcom/facebook/interstitial/manager/InterstitialLogger;Ljava/lang/String;LX/6Xs;LX/6Xr;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/interstitial/manager/InterstitialLogger;Ljava/lang/String;LX/6Xs;LX/6Xr;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p2    # LX/6Xs;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/6Xs;",
            "LX/6Xr;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1108983
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108984
    iget-object v0, p0, Lcom/facebook/interstitial/manager/InterstitialLogger;->b:LX/0iA;

    invoke-virtual {v0, p1}, LX/0iA;->a(Ljava/lang/String;)LX/0i1;

    move-result-object v0

    .line 1108985
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108986
    instance-of v1, v0, LX/13D;

    if-eqz v1, :cond_1

    .line 1108987
    check-cast v0, LX/13D;

    .line 1108988
    iget-object v1, v0, LX/13D;->a:LX/13K;

    .line 1108989
    iget-object v2, v1, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-eqz v2, :cond_2

    .line 1108990
    const-string v2, "promotion_id"

    iget-object v0, v1, LX/13K;->q:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-static {v2, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    .line 1108991
    :goto_0
    move-object v1, v2

    .line 1108992
    move-object v0, v1

    .line 1108993
    :goto_1
    if-eqz p3, :cond_0

    .line 1108994
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1108995
    invoke-virtual {v1, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 1108996
    const-string v0, "action_type"

    invoke-virtual {p3}, LX/6Xr;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1108997
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 1108998
    :cond_0
    new-instance v1, Lcom/facebook/interstitial/logging/LogInterstitialParams;

    invoke-direct {v1, p1, p2, v0}, Lcom/facebook/interstitial/logging/LogInterstitialParams;-><init>(Ljava/lang/String;LX/6Xs;LX/0P1;)V

    .line 1108999
    iget-object v0, p0, Lcom/facebook/interstitial/manager/InterstitialLogger;->c:LX/0Wd;

    new-instance v2, LX/6Xv;

    invoke-direct {v2, p0, v1}, LX/6Xv;-><init>(Lcom/facebook/interstitial/manager/InterstitialLogger;Lcom/facebook/interstitial/logging/LogInterstitialParams;)V

    invoke-interface {v0, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1109000
    :cond_1
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1109001
    goto :goto_1

    .line 1109002
    :cond_2
    sget-object v2, LX/0Rg;->a:LX/0Rg;

    move-object v2, v2

    .line 1109003
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1109004
    sget-object v0, LX/6Xs;->IMPRESSION:LX/6Xs;

    invoke-static {p0, p1, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Lcom/facebook/interstitial/manager/InterstitialLogger;Ljava/lang/String;LX/6Xs;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1109005
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1109006
    sget-object v0, LX/6Xs;->ACTION:LX/6Xs;

    sget-object v1, LX/6Xr;->PRIMARY:LX/6Xr;

    invoke-static {p0, p1, v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Lcom/facebook/interstitial/manager/InterstitialLogger;Ljava/lang/String;LX/6Xs;LX/6Xr;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1109007
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1109008
    sget-object v0, LX/6Xs;->DISMISSAL:LX/6Xs;

    invoke-static {p0, p1, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Lcom/facebook/interstitial/manager/InterstitialLogger;Ljava/lang/String;LX/6Xs;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1109009
    return-void
.end method
