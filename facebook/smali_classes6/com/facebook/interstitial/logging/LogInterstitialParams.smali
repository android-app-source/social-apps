.class public Lcom/facebook/interstitial/logging/LogInterstitialParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/interstitial/logging/LogInterstitialParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/6Xs;

.field public final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1108942
    new-instance v0, LX/6Xq;

    invoke-direct {v0}, LX/6Xq;-><init>()V

    sput-object v0, Lcom/facebook/interstitial/logging/LogInterstitialParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1108943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108944
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/interstitial/logging/LogInterstitialParams;->a:Ljava/lang/String;

    .line 1108945
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6Xs;->valueOf(Ljava/lang/String;)LX/6Xs;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/interstitial/logging/LogInterstitialParams;->b:LX/6Xs;

    .line 1108946
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/interstitial/logging/LogInterstitialParams;->c:LX/0P1;

    .line 1108947
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/6Xs;LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/6Xs;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1108948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108949
    iput-object p1, p0, Lcom/facebook/interstitial/logging/LogInterstitialParams;->a:Ljava/lang/String;

    .line 1108950
    iput-object p2, p0, Lcom/facebook/interstitial/logging/LogInterstitialParams;->b:LX/6Xs;

    .line 1108951
    iput-object p3, p0, Lcom/facebook/interstitial/logging/LogInterstitialParams;->c:LX/0P1;

    .line 1108952
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1108953
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1108954
    iget-object v0, p0, Lcom/facebook/interstitial/logging/LogInterstitialParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1108955
    iget-object v0, p0, Lcom/facebook/interstitial/logging/LogInterstitialParams;->b:LX/6Xs;

    invoke-virtual {v0}, LX/6Xs;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1108956
    iget-object v0, p0, Lcom/facebook/interstitial/logging/LogInterstitialParams;->c:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1108957
    return-void
.end method
