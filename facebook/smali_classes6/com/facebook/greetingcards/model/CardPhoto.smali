.class public Lcom/facebook/greetingcards/model/CardPhoto;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/greetingcards/model/CardPhoto;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:LX/5QD;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/ipc/media/MediaItem;

.field public final e:Landroid/graphics/PointF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 912396
    new-instance v0, LX/5QC;

    invoke-direct {v0}, LX/5QC;-><init>()V

    sput-object v0, Lcom/facebook/greetingcards/model/CardPhoto;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;LX/5QD;Ljava/lang/String;Lcom/facebook/ipc/media/MediaItem;Landroid/graphics/PointF;)V
    .locals 0

    .prologue
    .line 912397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 912398
    iput-object p1, p0, Lcom/facebook/greetingcards/model/CardPhoto;->a:Landroid/net/Uri;

    .line 912399
    iput-object p3, p0, Lcom/facebook/greetingcards/model/CardPhoto;->c:Ljava/lang/String;

    .line 912400
    iput-object p2, p0, Lcom/facebook/greetingcards/model/CardPhoto;->b:LX/5QD;

    .line 912401
    iput-object p4, p0, Lcom/facebook/greetingcards/model/CardPhoto;->d:Lcom/facebook/ipc/media/MediaItem;

    .line 912402
    iput-object p5, p0, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    .line 912403
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/high16 v3, -0x40800000    # -1.0f

    .line 912404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 912405
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/greetingcards/model/CardPhoto;->a:Landroid/net/Uri;

    .line 912406
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5QD;->valueOf(Ljava/lang/String;)LX/5QD;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/model/CardPhoto;->b:LX/5QD;

    .line 912407
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/model/CardPhoto;->c:Ljava/lang/String;

    .line 912408
    const-class v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    iput-object v0, p0, Lcom/facebook/greetingcards/model/CardPhoto;->d:Lcom/facebook/ipc/media/MediaItem;

    .line 912409
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    .line 912410
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    .line 912411
    cmpl-float v2, v0, v3

    if-eqz v2, :cond_0

    cmpl-float v2, v1, v3

    if-eqz v2, :cond_0

    .line 912412
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, p0, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    .line 912413
    :goto_0
    return-void

    .line 912414
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;Ljava/lang/String;Landroid/graphics/PointF;)Lcom/facebook/greetingcards/model/CardPhoto;
    .locals 6
    .param p2    # Landroid/graphics/PointF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 912415
    new-instance v0, Lcom/facebook/greetingcards/model/CardPhoto;

    sget-object v2, LX/5QD;->REMOTE:LX/5QD;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/greetingcards/model/CardPhoto;-><init>(Landroid/net/Uri;LX/5QD;Ljava/lang/String;Lcom/facebook/ipc/media/MediaItem;Landroid/graphics/PointF;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/ipc/media/MediaItem;)Lcom/facebook/greetingcards/model/CardPhoto;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 912416
    new-instance v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/5QD;->LOCAL:LX/5QD;

    move-object v4, p0

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/greetingcards/model/CardPhoto;-><init>(Landroid/net/Uri;LX/5QD;Ljava/lang/String;Lcom/facebook/ipc/media/MediaItem;Landroid/graphics/PointF;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 912417
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 912418
    iget-object v0, p0, Lcom/facebook/greetingcards/model/CardPhoto;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 912419
    iget-object v0, p0, Lcom/facebook/greetingcards/model/CardPhoto;->b:LX/5QD;

    invoke-virtual {v0}, LX/5QD;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 912420
    iget-object v0, p0, Lcom/facebook/greetingcards/model/CardPhoto;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 912421
    iget-object v0, p0, Lcom/facebook/greetingcards/model/CardPhoto;->d:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 912422
    iget-object v0, p0, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    .line 912423
    iget-object v0, p0, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 912424
    iget-object v0, p0, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 912425
    :goto_0
    return-void

    .line 912426
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeFloat(F)V

    .line 912427
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeFloat(F)V

    goto :goto_0
.end method
