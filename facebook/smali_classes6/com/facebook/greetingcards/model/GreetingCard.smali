.class public Lcom/facebook/greetingcards/model/GreetingCard;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/greetingcards/model/GreetingCardSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/greetingcards/model/GreetingCard;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/model/GreetingCard$Slide;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;


# direct methods
.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 912482
    const-class v0, Lcom/facebook/greetingcards/model/GreetingCardSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 912483
    new-instance v0, LX/5QE;

    invoke-direct {v0}, LX/5QE;-><init>()V

    sput-object v0, Lcom/facebook/greetingcards/model/GreetingCard;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 912484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 912485
    const-class v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    .line 912486
    const-class v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    .line 912487
    const-class v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    .line 912488
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->d:Ljava/lang/String;

    .line 912489
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->e:Ljava/lang/String;

    .line 912490
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->f:Ljava/lang/String;

    .line 912491
    return-void
.end method

.method public constructor <init>(Lcom/facebook/greetingcards/model/GreetingCard$Slide;LX/0Px;Lcom/facebook/greetingcards/model/GreetingCard$Slide;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/model/GreetingCard$Slide;",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/model/GreetingCard$Slide;",
            ">;",
            "Lcom/facebook/greetingcards/model/GreetingCard$Slide;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 912458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 912459
    iput-object p1, p0, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    .line 912460
    iput-object p2, p0, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    .line 912461
    iput-object p3, p0, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    .line 912462
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->d:Ljava/lang/String;

    .line 912463
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->e:Ljava/lang/String;

    .line 912464
    iput-object p6, p0, Lcom/facebook/greetingcards/model/GreetingCard;->f:Ljava/lang/String;

    .line 912465
    return-void
.end method

.method public static a(Lcom/facebook/greetingcards/model/GreetingCard;)Lcom/facebook/greetingcards/model/GreetingCard;
    .locals 7

    .prologue
    .line 912479
    new-instance v1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v0, v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    const-string v2, ""

    iget-object v3, p0, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v3, v3, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-direct {v1, v0, v2, v3}, Lcom/facebook/greetingcards/model/GreetingCard$Slide;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    .line 912480
    new-instance v3, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v0, v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    const-string v2, ""

    iget-object v4, p0, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v4, v4, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-direct {v3, v0, v2, v4}, Lcom/facebook/greetingcards/model/GreetingCard$Slide;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    .line 912481
    new-instance v0, Lcom/facebook/greetingcards/model/GreetingCard;

    iget-object v2, p0, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    iget-object v4, p0, Lcom/facebook/greetingcards/model/GreetingCard;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/greetingcards/model/GreetingCard;->e:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/greetingcards/model/GreetingCard;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/greetingcards/model/GreetingCard;-><init>(Lcom/facebook/greetingcards/model/GreetingCard$Slide;LX/0Px;Lcom/facebook/greetingcards/model/GreetingCard$Slide;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/greetingcards/model/GreetingCard;Ljava/util/Map;)Lcom/facebook/greetingcards/model/GreetingCard;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/model/GreetingCard;",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/greetingcards/model/GreetingCard;"
        }
    .end annotation

    .prologue
    .line 912474
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 912475
    iget-object v3, p0, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    .line 912476
    invoke-static {v0, p1}, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a(Lcom/facebook/greetingcards/model/GreetingCard$Slide;Ljava/util/Map;)Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 912477
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 912478
    :cond_0
    new-instance v0, Lcom/facebook/greetingcards/model/GreetingCard;

    iget-object v1, p0, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    invoke-static {v1, p1}, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a(Lcom/facebook/greetingcards/model/GreetingCard$Slide;Ljava/util/Map;)Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    move-result-object v1

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    invoke-static {v3, p1}, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a(Lcom/facebook/greetingcards/model/GreetingCard$Slide;Ljava/util/Map;)Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/greetingcards/model/GreetingCard;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/greetingcards/model/GreetingCard;->e:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/greetingcards/model/GreetingCard;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/greetingcards/model/GreetingCard;-><init>(Lcom/facebook/greetingcards/model/GreetingCard$Slide;LX/0Px;Lcom/facebook/greetingcards/model/GreetingCard$Slide;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 912473
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 912466
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 912467
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 912468
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 912469
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 912470
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 912471
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 912472
    return-void
.end method
