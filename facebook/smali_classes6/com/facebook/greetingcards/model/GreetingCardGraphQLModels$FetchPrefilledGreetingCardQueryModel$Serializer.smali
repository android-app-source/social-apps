.class public final Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$FetchPrefilledGreetingCardQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$FetchPrefilledGreetingCardQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 912676
    const-class v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$FetchPrefilledGreetingCardQueryModel;

    new-instance v1, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$FetchPrefilledGreetingCardQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$FetchPrefilledGreetingCardQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 912677
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 912679
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$FetchPrefilledGreetingCardQueryModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 912680
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 912681
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 912682
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 912683
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 912684
    if-eqz v2, :cond_d

    .line 912685
    const-string v3, "prefilled_greeting_card"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912686
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 912687
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 912688
    if-eqz v3, :cond_0

    .line 912689
    const-string v4, "greeting_card_template"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912690
    invoke-static {v1, v3, p1}, LX/5QJ;->a(LX/15i;ILX/0nX;)V

    .line 912691
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 912692
    if-eqz v3, :cond_b

    .line 912693
    const-string v4, "slides"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912694
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 912695
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 912696
    if-eqz v4, :cond_a

    .line 912697
    const-string v5, "nodes"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912698
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 912699
    const/4 v5, 0x0

    :goto_0
    invoke-virtual {v1, v4}, LX/15i;->c(I)I

    move-result v6

    if-ge v5, v6, :cond_9

    .line 912700
    invoke-virtual {v1, v4, v5}, LX/15i;->q(II)I

    move-result v6

    const/4 v0, 0x2

    .line 912701
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 912702
    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, LX/15i;->g(II)I

    move-result v7

    .line 912703
    if-eqz v7, :cond_2

    .line 912704
    const-string p0, "message"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912705
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 912706
    const/4 p0, 0x0

    invoke-virtual {v1, v7, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 912707
    if-eqz p0, :cond_1

    .line 912708
    const-string v3, "text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912709
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 912710
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 912711
    :cond_2
    const/4 v7, 0x1

    invoke-virtual {v1, v6, v7}, LX/15i;->g(II)I

    move-result v7

    .line 912712
    if-eqz v7, :cond_5

    .line 912713
    const-string p0, "photos"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912714
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 912715
    const/4 p0, 0x0

    invoke-virtual {v1, v7, p0}, LX/15i;->g(II)I

    move-result p0

    .line 912716
    if-eqz p0, :cond_4

    .line 912717
    const-string v3, "nodes"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912718
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 912719
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v7

    if-ge v3, v7, :cond_3

    .line 912720
    invoke-virtual {v1, p0, v3}, LX/15i;->q(II)I

    move-result v7

    invoke-static {v1, v7, p1, p2}, LX/5QK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 912721
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 912722
    :cond_3
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 912723
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 912724
    :cond_5
    invoke-virtual {v1, v6, v0}, LX/15i;->g(II)I

    move-result v7

    .line 912725
    if-eqz v7, :cond_6

    .line 912726
    const-string v7, "slide_type"

    invoke-virtual {p1, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912727
    invoke-virtual {v1, v6, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/0nX;->b(Ljava/lang/String;)V

    .line 912728
    :cond_6
    const/4 v7, 0x3

    invoke-virtual {v1, v6, v7}, LX/15i;->g(II)I

    move-result v7

    .line 912729
    if-eqz v7, :cond_8

    .line 912730
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912731
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 912732
    const/4 p0, 0x0

    invoke-virtual {v1, v7, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 912733
    if-eqz p0, :cond_7

    .line 912734
    const-string v0, "text"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912735
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 912736
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 912737
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 912738
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 912739
    :cond_9
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 912740
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 912741
    :cond_b
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 912742
    if-eqz v3, :cond_c

    .line 912743
    const-string v4, "theme"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912744
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 912745
    :cond_c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 912746
    :cond_d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 912747
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 912678
    check-cast p1, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$FetchPrefilledGreetingCardQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$FetchPrefilledGreetingCardQueryModel$Serializer;->a(Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$FetchPrefilledGreetingCardQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
