.class public final Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5046af4a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 912897
    const-class v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 912896
    const-class v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 912894
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 912895
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 912839
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->i:Ljava/lang/String;

    .line 912840
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 912880
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 912881
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 912882
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 912883
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x21ddfcb3

    invoke-static {v3, v2, v4}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 912884
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x3858a460    # -85687.25f

    invoke-static {v4, v3, v5}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 912885
    invoke-direct {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 912886
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 912887
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 912888
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 912889
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 912890
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 912891
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 912892
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 912893
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 912859
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 912860
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 912861
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 912862
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 912863
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;

    .line 912864
    iput-object v0, v1, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 912865
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 912866
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x21ddfcb3

    invoke-static {v2, v0, v3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 912867
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 912868
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;

    .line 912869
    iput v3, v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->g:I

    move-object v1, v0

    .line 912870
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 912871
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x3858a460    # -85687.25f

    invoke-static {v2, v0, v3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 912872
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 912873
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;

    .line 912874
    iput v3, v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->h:I

    move-object v1, v0

    .line 912875
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 912876
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 912877
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 912878
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_3
    move-object p0, v1

    .line 912879
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 912858
    invoke-virtual {p0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 912854
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 912855
    const/4 v0, 0x2

    const v1, 0x21ddfcb3

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->g:I

    .line 912856
    const/4 v0, 0x3

    const v1, -0x3858a460    # -85687.25f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->h:I

    .line 912857
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 912851
    new-instance v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;

    invoke-direct {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;-><init>()V

    .line 912852
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 912853
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 912850
    const v0, 0x2f0a30aa

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 912849
    const v0, 0x4984e12

    return v0
.end method

.method public final j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 912847
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 912848
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 912845
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->f:Ljava/lang/String;

    .line 912846
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLargeImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 912843
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 912844
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMediumImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 912841
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 912842
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
