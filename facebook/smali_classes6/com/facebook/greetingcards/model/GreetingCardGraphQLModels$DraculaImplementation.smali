.class public final Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 912648
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 912649
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 912646
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 912647
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 912577
    if-nez p1, :cond_0

    move v0, v1

    .line 912578
    :goto_0
    return v0

    .line 912579
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 912580
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 912581
    :sswitch_0
    const-class v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$GreetingCardTemplateModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$GreetingCardTemplateModel;

    .line 912582
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 912583
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v2

    .line 912584
    const v3, 0x756900df

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 912585
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 912586
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 912587
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 912588
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 912589
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 912590
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 912591
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 912592
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 912593
    const v2, -0x15caa70d

    const/4 v4, 0x0

    .line 912594
    if-nez v0, :cond_1

    move v3, v4

    .line 912595
    :goto_1
    move v0, v3

    .line 912596
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 912597
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 912598
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 912599
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 912600
    const v2, 0x6759d2b6

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 912601
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v2

    .line 912602
    const v3, -0x44eb3bdb    # -0.002269992f

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 912603
    invoke-virtual {p0, p1, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    move-result-object v3

    .line 912604
    invoke-virtual {p3, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 912605
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v4

    .line 912606
    const v5, -0x23242203

    invoke-static {p0, v4, v5, p3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v4

    .line 912607
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 912608
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 912609
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 912610
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 912611
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 912612
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 912613
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 912614
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 912615
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 912616
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 912617
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 912618
    :sswitch_4
    const-class v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 912619
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 912620
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 912621
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 912622
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 912623
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 912624
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 912625
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 912626
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 912627
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 912628
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 912629
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 912630
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 912631
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 912632
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 912633
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 912634
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 912635
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 912636
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 912637
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 912638
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v5

    .line 912639
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 912640
    :goto_2
    if-ge v4, v5, :cond_3

    .line 912641
    invoke-virtual {p0, v0, v4}, LX/15i;->q(II)I

    move-result v7

    .line 912642
    invoke-static {p0, v7, v2, p3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v7

    aput v7, v3, v4

    .line 912643
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 912644
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 912645
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x44eb3bdb -> :sswitch_4
        -0x3858a460 -> :sswitch_6
        -0x2a3aa016 -> :sswitch_0
        -0x23242203 -> :sswitch_7
        -0x15caa70d -> :sswitch_2
        0x21ddfcb3 -> :sswitch_5
        0x6759d2b6 -> :sswitch_3
        0x756900df -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 912576
    new-instance v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 912572
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 912573
    if-eqz v0, :cond_0

    .line 912574
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 912575
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 912567
    if-eqz p0, :cond_0

    .line 912568
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 912569
    if-eq v0, p0, :cond_0

    .line 912570
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 912571
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 912543
    sparse-switch p2, :sswitch_data_0

    .line 912544
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 912545
    :sswitch_0
    const-class v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$GreetingCardTemplateModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$GreetingCardTemplateModel;

    .line 912546
    invoke-static {v0, p3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 912547
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 912548
    const v1, 0x756900df

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 912549
    :goto_0
    :sswitch_1
    return-void

    .line 912550
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 912551
    const v1, -0x15caa70d

    .line 912552
    if-eqz v0, :cond_0

    .line 912553
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 912554
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 912555
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 912556
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 912557
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 912558
    :cond_0
    goto :goto_0

    .line 912559
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 912560
    const v1, 0x6759d2b6

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 912561
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 912562
    const v1, -0x44eb3bdb    # -0.002269992f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 912563
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 912564
    const v1, -0x23242203

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 912565
    :sswitch_4
    const-class v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 912566
    invoke-static {v0, p3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x44eb3bdb -> :sswitch_4
        -0x3858a460 -> :sswitch_1
        -0x2a3aa016 -> :sswitch_0
        -0x23242203 -> :sswitch_1
        -0x15caa70d -> :sswitch_3
        0x21ddfcb3 -> :sswitch_1
        0x6759d2b6 -> :sswitch_1
        0x756900df -> :sswitch_2
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 912537
    if-eqz p1, :cond_0

    .line 912538
    invoke-static {p0, p1, p2}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 912539
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;

    .line 912540
    if-eq v0, v1, :cond_0

    .line 912541
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 912542
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 912536
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 912534
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 912535
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 912503
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 912504
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 912505
    :cond_0
    iput-object p1, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 912506
    iput p2, p0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->b:I

    .line 912507
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 912533
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 912532
    new-instance v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 912529
    iget v0, p0, LX/1vt;->c:I

    .line 912530
    move v0, v0

    .line 912531
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 912526
    iget v0, p0, LX/1vt;->c:I

    .line 912527
    move v0, v0

    .line 912528
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 912523
    iget v0, p0, LX/1vt;->b:I

    .line 912524
    move v0, v0

    .line 912525
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 912520
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 912521
    move-object v0, v0

    .line 912522
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 912511
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 912512
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 912513
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 912514
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 912515
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 912516
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 912517
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 912518
    invoke-static {v3, v9, v2}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 912519
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 912508
    iget v0, p0, LX/1vt;->c:I

    .line 912509
    move v0, v0

    .line 912510
    return v0
.end method
