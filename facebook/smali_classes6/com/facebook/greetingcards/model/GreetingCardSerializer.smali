.class public Lcom/facebook/greetingcards/model/GreetingCardSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/greetingcards/model/GreetingCard;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 913140
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(LX/0nX;Lcom/facebook/greetingcards/model/GreetingCard$Slide;)V
    .locals 4

    .prologue
    .line 913141
    const-string v0, "title"

    iget-object v1, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 913142
    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 913143
    const-string v0, "message"

    iget-object v1, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 913144
    :cond_0
    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 913145
    const-string v0, "photos"

    invoke-virtual {p0, v0}, LX/0nX;->f(Ljava/lang/String;)V

    .line 913146
    iget-object v2, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    .line 913147
    iget-object v0, v0, Lcom/facebook/greetingcards/model/CardPhoto;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/0nX;->a(Ljava/lang/Object;)V

    .line 913148
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 913149
    :cond_1
    invoke-virtual {p0}, LX/0nX;->e()V

    .line 913150
    :cond_2
    return-void
.end method

.method private static a(Lcom/facebook/greetingcards/model/GreetingCard;LX/0nX;)V
    .locals 5

    .prologue
    .line 913151
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 913152
    const-string v0, "template_id"

    iget-object v1, p0, Lcom/facebook/greetingcards/model/GreetingCard;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 913153
    const-string v0, "theme"

    iget-object v1, p0, Lcom/facebook/greetingcards/model/GreetingCard;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 913154
    const-string v0, "slides"

    invoke-virtual {p1, v0}, LX/0nX;->f(Ljava/lang/String;)V

    .line 913155
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 913156
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    invoke-static {p1, v0}, Lcom/facebook/greetingcards/model/GreetingCardSerializer;->a(LX/0nX;Lcom/facebook/greetingcards/model/GreetingCard$Slide;)V

    .line 913157
    const-string v0, "slide_type"

    const-string v1, "COVER_SLIDE"

    invoke-virtual {p1, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 913158
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 913159
    iget-object v2, p0, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    .line 913160
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 913161
    invoke-static {p1, v0}, Lcom/facebook/greetingcards/model/GreetingCardSerializer;->a(LX/0nX;Lcom/facebook/greetingcards/model/GreetingCard$Slide;)V

    .line 913162
    const-string v0, "slide_type"

    const-string v4, "STORY_SLIDE"

    invoke-virtual {p1, v0, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 913163
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 913164
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 913165
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 913166
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    invoke-static {p1, v0}, Lcom/facebook/greetingcards/model/GreetingCardSerializer;->a(LX/0nX;Lcom/facebook/greetingcards/model/GreetingCard$Slide;)V

    .line 913167
    const-string v0, "slide_type"

    const-string v1, "CLOSING_SLIDE"

    invoke-virtual {p1, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 913168
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 913169
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 913170
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 913171
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 913172
    check-cast p1, Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-static {p1, p2}, Lcom/facebook/greetingcards/model/GreetingCardSerializer;->a(Lcom/facebook/greetingcards/model/GreetingCard;LX/0nX;)V

    return-void
.end method
