.class public final Lcom/facebook/greetingcards/model/GreetingCard$Slide;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/greetingcards/model/GreetingCard$Slide;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/model/CardPhoto;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 912434
    new-instance v0, LX/5QF;

    invoke-direct {v0}, LX/5QF;-><init>()V

    sput-object v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 912435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 912436
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    .line 912437
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->b:Ljava/lang/String;

    .line 912438
    const-class v0, Lcom/facebook/greetingcards/model/CardPhoto;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    .line 912439
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/model/CardPhoto;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 912440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 912441
    iput-object p1, p0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    .line 912442
    iput-object p2, p0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->b:Ljava/lang/String;

    .line 912443
    iput-object p3, p0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    .line 912444
    return-void
.end method

.method public static a(Lcom/facebook/greetingcards/model/GreetingCard$Slide;Ljava/util/Map;)Lcom/facebook/greetingcards/model/GreetingCard$Slide;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/model/GreetingCard$Slide;",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/greetingcards/model/GreetingCard$Slide;"
        }
    .end annotation

    .prologue
    .line 912445
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 912446
    iget-object v4, p0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    .line 912447
    iget-object v1, v0, Lcom/facebook/greetingcards/model/CardPhoto;->a:Landroid/net/Uri;

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 912448
    iget-object v1, v0, Lcom/facebook/greetingcards/model/CardPhoto;->a:Landroid/net/Uri;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 912449
    new-instance v6, Lcom/facebook/greetingcards/model/CardPhoto;

    iget-object v7, v0, Lcom/facebook/greetingcards/model/CardPhoto;->a:Landroid/net/Uri;

    sget-object v8, LX/5QD;->LOCAL_UPLOADED:LX/5QD;

    iget-object v10, v0, Lcom/facebook/greetingcards/model/CardPhoto;->d:Lcom/facebook/ipc/media/MediaItem;

    const/4 v11, 0x0

    move-object v9, v1

    invoke-direct/range {v6 .. v11}, Lcom/facebook/greetingcards/model/CardPhoto;-><init>(Landroid/net/Uri;LX/5QD;Ljava/lang/String;Lcom/facebook/ipc/media/MediaItem;Landroid/graphics/PointF;)V

    move-object v0, v6

    .line 912450
    :cond_0
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 912451
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 912452
    :cond_1
    new-instance v0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v1, p0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->b:Ljava/lang/String;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/greetingcards/model/GreetingCard$Slide;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 912453
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 912454
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 912455
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 912456
    iget-object v0, p0, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 912457
    return-void
.end method
