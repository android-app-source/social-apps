.class public Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/5SK;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:Z

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field public final o:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field public final p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 917964
    new-instance v0, LX/5SL;

    invoke-direct {v0}, LX/5SL;-><init>()V

    sput-object v0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5SM;)V
    .locals 1

    .prologue
    .line 917945
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917946
    iget-object v0, p1, LX/5SM;->a:LX/5SK;

    iput-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->a:LX/5SK;

    .line 917947
    iget-object v0, p1, LX/5SM;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->b:Ljava/lang/String;

    .line 917948
    iget-object v0, p1, LX/5SM;->c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 917949
    iget-boolean v0, p1, LX/5SM;->d:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->f:Z

    .line 917950
    iget-boolean v0, p1, LX/5SM;->e:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->g:Z

    .line 917951
    iget-boolean v0, p1, LX/5SM;->f:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->d:Z

    .line 917952
    iget-boolean v0, p1, LX/5SM;->g:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->e:Z

    .line 917953
    iget-boolean v0, p1, LX/5SM;->h:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->h:Z

    .line 917954
    iget-boolean v0, p1, LX/5SM;->i:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->i:Z

    .line 917955
    iget-boolean v0, p1, LX/5SM;->j:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->j:Z

    .line 917956
    iget v0, p1, LX/5SM;->k:I

    iput v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->k:I

    .line 917957
    iget v0, p1, LX/5SM;->l:I

    iput v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->l:I

    .line 917958
    iget v0, p1, LX/5SM;->m:I

    iput v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->n:I

    .line 917959
    iget v0, p1, LX/5SM;->n:I

    iput v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->o:I

    .line 917960
    iget v0, p1, LX/5SM;->o:I

    iput v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->m:I

    .line 917961
    iget-object v0, p1, LX/5SM;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->p:Ljava/lang/String;

    .line 917962
    iget-object v0, p1, LX/5SM;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->q:Ljava/lang/String;

    .line 917963
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 917926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917927
    const-class v0, LX/5SK;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5SK;

    iput-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->a:LX/5SK;

    .line 917928
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->b:Ljava/lang/String;

    .line 917929
    const-class v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 917930
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->f:Z

    .line 917931
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->g:Z

    .line 917932
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->d:Z

    .line 917933
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->e:Z

    .line 917934
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->h:Z

    .line 917935
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->i:Z

    .line 917936
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->j:Z

    .line 917937
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->k:I

    .line 917938
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->l:I

    .line 917939
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->n:I

    .line 917940
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->o:I

    .line 917941
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->m:I

    .line 917942
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->p:Ljava/lang/String;

    .line 917943
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->q:Ljava/lang/String;

    .line 917944
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 917925
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 917924
    iget-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->f:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 917922
    iget-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->g:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 917923
    iget-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->e:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 917921
    iget-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->h:Z

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 917899
    iget-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->i:Z

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 917920
    iget v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->k:I

    return v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 917919
    iget v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->l:I

    return v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 917918
    iget-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 917900
    iget-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->a:LX/5SK;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 917901
    iget-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917902
    iget-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 917903
    iget-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 917904
    iget-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 917905
    iget-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 917906
    iget-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 917907
    iget-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->h:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 917908
    iget-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->i:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 917909
    iget-boolean v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->j:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 917910
    iget v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917911
    iget v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917912
    iget v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->n:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917913
    iget v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->o:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917914
    iget v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->m:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917915
    iget-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917916
    iget-object v0, p0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917917
    return-void
.end method
