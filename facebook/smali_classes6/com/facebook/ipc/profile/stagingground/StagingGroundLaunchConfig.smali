.class public Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:Ljava/lang/String;

.field public final k:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:LX/5QV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

.field public final n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

.field public final o:Z

.field public final p:I

.field public final q:I

.field public final r:Z

.field public final s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 917752
    new-instance v0, LX/5SI;

    invoke-direct {v0}, LX/5SI;-><init>()V

    sput-object v0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5SJ;)V
    .locals 2

    .prologue
    .line 917753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917754
    iget-object v0, p1, LX/5SJ;->a:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->a:Landroid/net/Uri;

    .line 917755
    iget-object v0, p1, LX/5SJ;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->b:Ljava/lang/String;

    .line 917756
    iget-object v0, p1, LX/5SJ;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->c:Ljava/lang/String;

    .line 917757
    iget-object v0, p1, LX/5SJ;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->d:Ljava/lang/String;

    .line 917758
    iget-wide v0, p1, LX/5SJ;->e:J

    iput-wide v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->e:J

    .line 917759
    iget v0, p1, LX/5SJ;->f:I

    iput v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->f:I

    .line 917760
    iget v0, p1, LX/5SJ;->g:I

    iput v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->g:I

    .line 917761
    iget v0, p1, LX/5SJ;->h:I

    iput v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->h:I

    .line 917762
    iget v0, p1, LX/5SJ;->i:I

    iput v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->i:I

    .line 917763
    iget-object v0, p1, LX/5SJ;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->j:Ljava/lang/String;

    .line 917764
    iget-object v0, p1, LX/5SJ;->k:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->k:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 917765
    iget-object v0, p1, LX/5SJ;->l:LX/5QV;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->l:LX/5QV;

    .line 917766
    iget-object v0, p1, LX/5SJ;->m:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->m:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 917767
    iget-object v0, p1, LX/5SJ;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 917768
    iget-boolean v0, p1, LX/5SJ;->o:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->o:Z

    .line 917769
    iget v0, p1, LX/5SJ;->p:I

    iput v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->p:I

    .line 917770
    iget v0, p1, LX/5SJ;->q:I

    iput v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->q:I

    .line 917771
    iget-boolean v0, p1, LX/5SJ;->r:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->r:Z

    .line 917772
    iget-boolean v0, p1, LX/5SJ;->s:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->s:Z

    .line 917773
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 917774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917775
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->a:Landroid/net/Uri;

    .line 917776
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->b:Ljava/lang/String;

    .line 917777
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->c:Ljava/lang/String;

    .line 917778
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->d:Ljava/lang/String;

    .line 917779
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->e:J

    .line 917780
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->f:I

    .line 917781
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->g:I

    .line 917782
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->h:I

    .line 917783
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->i:I

    .line 917784
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->j:Ljava/lang/String;

    .line 917785
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->k:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 917786
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/5QV;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->l:LX/5QV;

    .line 917787
    const-class v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->m:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 917788
    const-class v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 917789
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->o:Z

    .line 917790
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->p:I

    .line 917791
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->q:I

    .line 917792
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->r:Z

    .line 917793
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->s:Z

    .line 917794
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 917795
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 917796
    iget-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 917797
    iget-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917798
    iget-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917799
    iget-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917800
    iget-wide v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 917801
    iget v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917802
    iget v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917803
    iget v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917804
    iget v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917805
    iget-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917806
    iget-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->k:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 917807
    iget-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->l:LX/5QV;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 917808
    iget-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->m:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 917809
    iget-object v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 917810
    iget-boolean v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->o:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 917811
    iget v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->p:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917812
    iget v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->q:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917813
    iget-boolean v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->r:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 917814
    iget-boolean v0, p0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->s:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 917815
    return-void
.end method
