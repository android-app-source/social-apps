.class public Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/5SD;

.field public final b:J

.field public final c:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 917658
    new-instance v0, LX/5SE;

    invoke-direct {v0}, LX/5SE;-><init>()V

    sput-object v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5SD;JLcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V
    .locals 0
    .param p4    # Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 917653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917654
    iput-object p1, p0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a:LX/5SD;

    .line 917655
    iput-wide p2, p0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->b:J

    .line 917656
    iput-object p4, p0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->c:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    .line 917657
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 917648
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917649
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5SD;

    iput-object v0, p0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a:LX/5SD;

    .line 917650
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->b:J

    .line 917651
    const-class v0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    iput-object v0, p0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->c:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    .line 917652
    return-void
.end method

.method public static a(LX/5SD;J)Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;
    .locals 3

    .prologue
    .line 917647
    new-instance v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;-><init>(LX/5SD;JLcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V

    return-object v0
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 917659
    iget-object v0, p0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a:LX/5SD;

    sget-object v1, LX/5SD;->VIEWING_MODE:LX/5SD;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 917646
    iget-object v0, p0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a:LX/5SD;

    sget-object v1, LX/5SD;->EDIT_PROFILE_PIC:LX/5SD;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 917645
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 917644
    iget-object v0, p0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a:LX/5SD;

    sget-object v1, LX/5SD;->EDIT_COVER_PHOTO:LX/5SD;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 917640
    iget-object v0, p0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a:LX/5SD;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 917641
    iget-wide v0, p0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 917642
    iget-object v0, p0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->c:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 917643
    return-void
.end method
