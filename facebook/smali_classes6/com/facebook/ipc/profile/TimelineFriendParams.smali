.class public Lcom/facebook/ipc/profile/TimelineFriendParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/profile/TimelineFriendParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Landroid/os/ParcelUuid;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public final e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 917604
    new-instance v0, LX/5SC;

    invoke-direct {v0}, LX/5SC;-><init>()V

    sput-object v0, Lcom/facebook/ipc/profile/TimelineFriendParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(JLandroid/os/ParcelUuid;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 1

    .prologue
    .line 917605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917606
    iput-wide p1, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->a:J

    .line 917607
    iput-object p3, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->b:Landroid/os/ParcelUuid;

    .line 917608
    iput-object p4, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->c:Ljava/lang/String;

    .line 917609
    iput-object p5, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 917610
    iput-object p6, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 917611
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 917612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917613
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->a:J

    .line 917614
    const-class v0, Landroid/os/ParcelUuid;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelUuid;

    iput-object v0, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->b:Landroid/os/ParcelUuid;

    .line 917615
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->c:Ljava/lang/String;

    .line 917616
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 917617
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 917618
    return-void
.end method

.method public static a(LX/5SB;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Lcom/facebook/ipc/profile/TimelineFriendParams;
    .locals 10

    .prologue
    .line 917619
    new-instance v1, Lcom/facebook/ipc/profile/TimelineFriendParams;

    .line 917620
    iget-wide v8, p0, LX/5SB;->b:J

    move-wide v2, v8

    .line 917621
    iget-object v0, p0, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v4, v0

    .line 917622
    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v1 .. v7}, Lcom/facebook/ipc/profile/TimelineFriendParams;-><init>(JLandroid/os/ParcelUuid;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    return-object v1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 917623
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 917624
    iget-wide v0, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 917625
    iget-object v0, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->b:Landroid/os/ParcelUuid;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 917626
    iget-object v0, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917627
    iget-object v0, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917628
    iget-object v0, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917629
    return-void
.end method
