.class public Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Ljava/lang/String;

.field public final c:LX/5Rr;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/5Rr;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/5Rq;

.field public final f:Z

.field public final g:Z

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Z

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

.field public final n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 916862
    new-instance v0, LX/5Rv;

    invoke-direct {v0}, LX/5Rv;-><init>()V

    sput-object v0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/net/Uri;Ljava/lang/String;LX/5Rr;LX/5Rq;Ljava/util/List;ZZLjava/lang/String;ZLjava/lang/String;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;Z)V
    .locals 1
    .param p11    # Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "LX/5Rr;",
            "LX/5Rq;",
            "Ljava/util/List",
            "<",
            "LX/5Rr;",
            ">;ZZ",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;",
            "Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 916881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916882
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->d:Ljava/util/List;

    .line 916883
    iput-object p1, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->a:Landroid/net/Uri;

    .line 916884
    iput-object p2, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->b:Ljava/lang/String;

    .line 916885
    iput-object p3, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->c:LX/5Rr;

    .line 916886
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->d:Ljava/util/List;

    invoke-interface {v0, p5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 916887
    iput-object p4, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->e:LX/5Rq;

    .line 916888
    iput-boolean p6, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->f:Z

    .line 916889
    iput-boolean p7, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->g:Z

    .line 916890
    iput-object p8, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->h:Ljava/lang/String;

    .line 916891
    iput-boolean p9, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->i:Z

    .line 916892
    iput-object p10, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->j:Ljava/lang/String;

    .line 916893
    iput-object p11, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 916894
    invoke-static {p12}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->l:LX/0Px;

    .line 916895
    iput-object p13, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 916896
    iput-boolean p14, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->n:Z

    .line 916897
    return-void
.end method

.method public synthetic constructor <init>(Landroid/net/Uri;Ljava/lang/String;LX/5Rr;LX/5Rq;Ljava/util/List;ZZLjava/lang/String;ZLjava/lang/String;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;ZB)V
    .locals 0

    .prologue
    .line 916880
    invoke-direct/range {p0 .. p14}, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;-><init>(Landroid/net/Uri;Ljava/lang/String;LX/5Rr;LX/5Rq;Ljava/util/List;ZZLjava/lang/String;ZLjava/lang/String;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 916898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916899
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->d:Ljava/util/List;

    .line 916900
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->a:Landroid/net/Uri;

    .line 916901
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->b:Ljava/lang/String;

    .line 916902
    const-class v0, LX/5Rr;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Rr;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->c:LX/5Rr;

    .line 916903
    const-class v0, LX/5Rq;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Rq;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->e:LX/5Rq;

    .line 916904
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->d:Ljava/util/List;

    const-class v1, LX/5Rr;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 916905
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->f:Z

    .line 916906
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->g:Z

    .line 916907
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->h:Ljava/lang/String;

    .line 916908
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->i:Z

    .line 916909
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->j:Ljava/lang/String;

    .line 916910
    const-class v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 916911
    sget-object v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->l:LX/0Px;

    .line 916912
    const-class v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 916913
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->n:Z

    .line 916914
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 916879
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/5Rr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 916878
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 916863
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 916864
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916865
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->c:LX/5Rr;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 916866
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->e:LX/5Rq;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 916867
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 916868
    iget-boolean v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 916869
    iget-boolean v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 916870
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916871
    iget-boolean v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->i:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 916872
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916873
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 916874
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->l:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 916875
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 916876
    iget-boolean v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->n:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 916877
    return-void
.end method
