.class public Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:LX/434;

.field public static final b:LX/434;


# instance fields
.field public final c:Landroid/graphics/RectF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Lcom/facebook/photos/creativeediting/model/StickerParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:F

.field public final f:F

.field public final g:LX/434;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xb4

    const/4 v1, -0x1

    .line 916961
    new-instance v0, LX/434;

    invoke-direct {v0, v2, v2}, LX/434;-><init>(II)V

    sput-object v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->a:LX/434;

    .line 916962
    new-instance v0, LX/434;

    invoke-direct {v0, v1, v1}, LX/434;-><init>(II)V

    sput-object v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->b:LX/434;

    .line 916963
    new-instance v0, LX/5Rx;

    invoke-direct {v0}, LX/5Rx;-><init>()V

    sput-object v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5Ry;)V
    .locals 1

    .prologue
    .line 916954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916955
    iget-object v0, p1, LX/5Ry;->a:Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->c:Landroid/graphics/RectF;

    .line 916956
    iget-object v0, p1, LX/5Ry;->b:Lcom/facebook/photos/creativeediting/model/StickerParams;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->d:Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 916957
    iget v0, p1, LX/5Ry;->c:F

    iput v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->e:F

    .line 916958
    iget v0, p1, LX/5Ry;->d:F

    iput v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->f:F

    .line 916959
    iget-object v0, p1, LX/5Ry;->e:LX/434;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->g:LX/434;

    .line 916960
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 916939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916940
    const-class v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->c:Landroid/graphics/RectF;

    .line 916941
    const-class v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->d:Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 916942
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->e:F

    .line 916943
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->f:F

    .line 916944
    new-instance v0, LX/434;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/434;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->g:LX/434;

    .line 916945
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 916953
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 916946
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->c:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 916947
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->d:Lcom/facebook/photos/creativeediting/model/StickerParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 916948
    iget v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->e:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 916949
    iget v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->f:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 916950
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->g:LX/434;

    iget v0, v0, LX/434;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 916951
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->g:LX/434;

    iget v0, v0, LX/434;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 916952
    return-void
.end method
