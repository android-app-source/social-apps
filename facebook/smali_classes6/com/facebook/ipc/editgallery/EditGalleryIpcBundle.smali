.class public Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/net/Uri;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

.field public final f:Ljava/lang/String;

.field public final g:Landroid/graphics/RectF;

.field public final h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 916774
    const-string v0, "edit_gallery_ipc_bundle_extra_key"

    sput-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    .line 916775
    new-instance v0, LX/5Rt;

    invoke-direct {v0}, LX/5Rt;-><init>()V

    sput-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/net/Uri;ILjava/lang/String;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Ljava/lang/String;Landroid/graphics/RectF;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 916765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916766
    iput-object p1, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    .line 916767
    iput p2, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->c:I

    .line 916768
    iput-object p3, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d:Ljava/lang/String;

    .line 916769
    iput-object p4, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 916770
    iput-object p5, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->f:Ljava/lang/String;

    .line 916771
    iput-object p6, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    .line 916772
    iput-object p7, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->h:Ljava/lang/String;

    .line 916773
    return-void
.end method

.method public synthetic constructor <init>(Landroid/net/Uri;ILjava/lang/String;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Ljava/lang/String;Landroid/graphics/RectF;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 916764
    invoke-direct/range {p0 .. p7}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;-><init>(Landroid/net/Uri;ILjava/lang/String;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Ljava/lang/String;Landroid/graphics/RectF;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 916755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916756
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    .line 916757
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->c:I

    .line 916758
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d:Ljava/lang/String;

    .line 916759
    const-class v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 916760
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->f:Ljava/lang/String;

    .line 916761
    const-class v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    .line 916762
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->h:Ljava/lang/String;

    .line 916763
    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 916741
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 916754
    iget v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->c:I

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 916753
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .locals 1

    .prologue
    .line 916752
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 916751
    const/4 v0, 0x0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 916750
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 916742
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 916743
    iget v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 916744
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916745
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 916746
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916747
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 916748
    iget-object v0, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916749
    return-void
.end method
