.class public Lcom/facebook/ipc/katana/model/GeoRegion;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/katana/model/GeoRegionDeserializer;
.end annotation


# instance fields
.field public final abbrName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "abbr_name"
    .end annotation
.end field

.field public final id:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_fbid"
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 917020
    const-class v0, Lcom/facebook/ipc/katana/model/GeoRegionDeserializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 917021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917022
    iput-object v0, p0, Lcom/facebook/ipc/katana/model/GeoRegion;->abbrName:Ljava/lang/String;

    .line 917023
    iput-object v0, p0, Lcom/facebook/ipc/katana/model/GeoRegion;->type:Ljava/lang/String;

    .line 917024
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/ipc/katana/model/GeoRegion;->id:J

    .line 917025
    return-void
.end method

.method public static a(LX/0Px;)Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/katana/model/GeoRegion;",
            ">;)",
            "Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 917026
    if-nez p0, :cond_0

    move-object v0, v2

    .line 917027
    :goto_0
    return-object v0

    .line 917028
    :cond_0
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v3, v0

    move-object v1, v2

    :goto_1
    if-ge v3, v4, :cond_1

    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/katana/model/GeoRegion;

    .line 917029
    iget-object v5, v0, Lcom/facebook/ipc/katana/model/GeoRegion;->type:Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/facebook/ipc/katana/model/GeoRegion;->type:Ljava/lang/String;

    const-string v6, "city"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 917030
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v0

    goto :goto_1

    .line 917031
    :cond_1
    if-eqz v1, :cond_2

    .line 917032
    iget-object v0, v1, Lcom/facebook/ipc/katana/model/GeoRegion;->abbrName:Ljava/lang/String;

    iget-wide v2, v1, Lcom/facebook/ipc/katana/model/GeoRegion;->id:J

    invoke-static {v0, v2, v3}, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->a(Ljava/lang/String;J)LX/5S2;

    move-result-object v0

    invoke-virtual {v0}, LX/5S2;->a()Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 917033
    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method
