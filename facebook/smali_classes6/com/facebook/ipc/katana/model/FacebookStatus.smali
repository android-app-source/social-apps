.class public Lcom/facebook/ipc/katana/model/FacebookStatus;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/katana/model/FacebookStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:J

.field private c:Lcom/facebook/ipc/model/FacebookUser;

.field private final d:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 916990
    new-instance v0, LX/5S0;

    invoke-direct {v0}, LX/5S0;-><init>()V

    sput-object v0, Lcom/facebook/ipc/katana/model/FacebookStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 916984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916985
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/katana/model/FacebookStatus;->a:Ljava/lang/String;

    .line 916986
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/katana/model/FacebookStatus;->b:J

    .line 916987
    const-class v0, Lcom/facebook/ipc/model/FacebookUser;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookUser;

    iput-object v0, p0, Lcom/facebook/ipc/katana/model/FacebookStatus;->c:Lcom/facebook/ipc/model/FacebookUser;

    .line 916988
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/katana/model/FacebookStatus;->d:J

    .line 916989
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 916991
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 916979
    iget-object v0, p0, Lcom/facebook/ipc/katana/model/FacebookStatus;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916980
    iget-wide v0, p0, Lcom/facebook/ipc/katana/model/FacebookStatus;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 916981
    iget-object v0, p0, Lcom/facebook/ipc/katana/model/FacebookStatus;->c:Lcom/facebook/ipc/model/FacebookUser;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 916982
    iget-wide v0, p0, Lcom/facebook/ipc/katana/model/FacebookStatus;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 916983
    return-void
.end method
