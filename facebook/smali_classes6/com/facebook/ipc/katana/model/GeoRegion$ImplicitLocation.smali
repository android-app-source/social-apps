.class public final Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/katana/model/GeoRegion_ImplicitLocationDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/katana/model/GeoRegion_ImplicitLocationSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "label"
    .end annotation
.end field

.field public final pageId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 917019
    const-class v0, Lcom/facebook/ipc/katana/model/GeoRegion_ImplicitLocationDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 917018
    const-class v0, Lcom/facebook/ipc/katana/model/GeoRegion_ImplicitLocationSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 917017
    new-instance v0, LX/5S1;

    invoke-direct {v0}, LX/5S1;-><init>()V

    sput-object v0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 917013
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917014
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->label:Ljava/lang/String;

    .line 917015
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->pageId:J

    .line 917016
    return-void
.end method

.method public constructor <init>(LX/5S2;)V
    .locals 2

    .prologue
    .line 917009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917010
    iget-object v0, p1, LX/5S2;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->label:Ljava/lang/String;

    .line 917011
    iget-wide v0, p1, LX/5S2;->b:J

    iput-wide v0, p0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->pageId:J

    .line 917012
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 917005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917006
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->label:Ljava/lang/String;

    .line 917007
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->pageId:J

    .line 917008
    return-void
.end method

.method public static a(Ljava/lang/String;J)LX/5S2;
    .locals 3

    .prologue
    .line 917004
    new-instance v0, LX/5S2;

    invoke-direct {v0, p0, p1, p2}, LX/5S2;-><init>(Ljava/lang/String;J)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 917003
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 917000
    iget-object v0, p0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->label:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917001
    iget-wide v0, p0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->pageId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 917002
    return-void
.end method
