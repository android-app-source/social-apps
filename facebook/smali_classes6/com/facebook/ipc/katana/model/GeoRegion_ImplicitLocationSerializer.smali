.class public Lcom/facebook/ipc/katana/model/GeoRegion_ImplicitLocationSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 917084
    const-class v0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    new-instance v1, Lcom/facebook/ipc/katana/model/GeoRegion_ImplicitLocationSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/katana/model/GeoRegion_ImplicitLocationSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 917085
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 917083
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 917086
    if-nez p0, :cond_0

    .line 917087
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 917088
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 917089
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/katana/model/GeoRegion_ImplicitLocationSerializer;->b(Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;LX/0nX;LX/0my;)V

    .line 917090
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 917091
    return-void
.end method

.method private static b(Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 917080
    const-string v0, "label"

    iget-object v1, p0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->label:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 917081
    const-string v0, "page_id"

    iget-wide v2, p0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->pageId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 917082
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 917079
    check-cast p1, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/katana/model/GeoRegion_ImplicitLocationSerializer;->a(Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;LX/0nX;LX/0my;)V

    return-void
.end method
