.class public final Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/151;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/151;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 913844
    iput-object p1, p0, Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;->a:LX/151;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 913845
    iput-object p2, p0, Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;->b:Ljava/lang/String;

    .line 913846
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 913847
    iget-object v0, p0, Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;->a:LX/151;

    iget-object v1, p0, Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/151;->b(LX/151;Ljava/lang/String;)LX/152;

    move-result-object v0

    .line 913848
    iget-object v1, p0, Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;->a:LX/151;

    iget-object v1, v1, LX/151;->f:Ljava/util/Map;

    monitor-enter v1

    .line 913849
    :try_start_0
    iget-object v2, p0, Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;->a:LX/151;

    iget-object v2, v2, LX/151;->f:Ljava/util/Map;

    iget-object v3, p0, Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;->b:Ljava/lang/String;

    new-instance v4, LX/5Qs;

    iget-object v5, p0, Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;->a:LX/151;

    iget-object v5, v5, LX/151;->e:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    invoke-direct {v4, v0, v6, v7}, LX/5Qs;-><init>(LX/152;J)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 913850
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 913851
    iget-object v0, p0, Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;->a:LX/151;

    iget-object v1, v0, LX/151;->g:Ljava/util/Map;

    monitor-enter v1

    .line 913852
    :try_start_1
    iget-object v0, p0, Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;->a:LX/151;

    iget-object v0, v0, LX/151;->g:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/ipc/appuserstatus/BaseAppUserStatusUtils$StatusFetchRunnable;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 913853
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 913854
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 913855
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
