.class public final Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

.field public static final b:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;


# instance fields
.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 916382
    new-instance v0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    const-string v1, "custom"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->a:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 916383
    new-instance v0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    const-string v1, "everyone"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->b:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 916384
    new-instance v0, LX/5Rh;

    invoke-direct {v0}, LX/5Rh;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 916389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916390
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->c:Ljava/lang/String;

    .line 916391
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->d:Ljava/lang/String;

    .line 916392
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 916393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916394
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->c:Ljava/lang/String;

    .line 916395
    invoke-static {p2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->d:Ljava/lang/String;

    .line 916396
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 916388
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 916385
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916386
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916387
    return-void
.end method
