.class public final Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerCallToAction_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 914495
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCallToAction_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 914496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914497
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->a:Ljava/lang/String;

    .line 914498
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->b:Ljava/lang/String;

    .line 914499
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->d:Ljava/lang/String;

    .line 914500
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->e:Ljava/lang/String;

    .line 914501
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/model/ComposerCallToAction;
    .locals 2

    .prologue
    .line 914486
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;-><init>(Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;)V

    return-object v0
.end method

.method public setCallToActionType(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "call_to_action_type"
    .end annotation

    .prologue
    .line 914493
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->a:Ljava/lang/String;

    .line 914494
    return-object p0
.end method

.method public setLabel(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "label"
    .end annotation

    .prologue
    .line 914502
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->b:Ljava/lang/String;

    .line 914503
    return-object p0
.end method

.method public setLink(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "link"
    .end annotation

    .prologue
    .line 914491
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->c:Ljava/lang/String;

    .line 914492
    return-object p0
.end method

.method public setLinkImage(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "link_image"
    .end annotation

    .prologue
    .line 914489
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->d:Ljava/lang/String;

    .line 914490
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation

    .prologue
    .line 914487
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->e:Ljava/lang/String;

    .line 914488
    return-object p0
.end method
