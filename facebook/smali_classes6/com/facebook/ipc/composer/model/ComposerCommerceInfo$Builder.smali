.class public final Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerCommerceInfo_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 914624
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 914618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914619
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 914620
    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->b:LX/0Px;

    .line 914621
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 914622
    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->c:LX/0Px;

    .line 914623
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;
    .locals 2

    .prologue
    .line 914617
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;-><init>(Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;)V

    return-object v0
.end method

.method public setCurrencyCode(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "currency_code"
    .end annotation

    .prologue
    .line 914615
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->a:Ljava/lang/String;

    .line 914616
    return-object p0
.end method

.method public setInterceptWords(LX/0Px;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "intercept_words"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;"
        }
    .end annotation

    .prologue
    .line 914625
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->b:LX/0Px;

    .line 914626
    return-object p0
.end method

.method public setInterceptWordsAfterNumber(LX/0Px;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "intercept_words_after_number"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;"
        }
    .end annotation

    .prologue
    .line 914613
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->c:LX/0Px;

    .line 914614
    return-object p0
.end method

.method public setIsCategoryOptional(Z)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_category_optional"
    .end annotation

    .prologue
    .line 914611
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->d:Z

    .line 914612
    return-object p0
.end method

.method public setMarketplaceCrossPostSettingModel(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "marketplace_cross_post_setting_model"
    .end annotation

    .prologue
    .line 914609
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->e:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    .line 914610
    return-object p0
.end method

.method public setPrefillCategoryId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prefill_category_id"
    .end annotation

    .prologue
    .line 914607
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->f:Ljava/lang/String;

    .line 914608
    return-object p0
.end method

.method public setProductItemLocationPickerSettings(Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "product_item_location_picker_settings"
    .end annotation

    .prologue
    .line 914605
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->g:Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    .line 914606
    return-object p0
.end method
