.class public final Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerStickerData_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915657
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerStickerData_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 915658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915659
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->f:Ljava/lang/String;

    .line 915660
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/model/ComposerStickerData;
    .locals 2

    .prologue
    .line 915661
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerStickerData;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;-><init>(Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;)V

    return-object v0
.end method

.method public setAnimatedDiskUri(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "animated_disk_uri"
    .end annotation

    .prologue
    .line 915662
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->a:Ljava/lang/String;

    .line 915663
    return-object p0
.end method

.method public setAnimatedWebUri(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "animated_web_uri"
    .end annotation

    .prologue
    .line 915664
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->b:Ljava/lang/String;

    .line 915665
    return-object p0
.end method

.method public setPackId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "pack_id"
    .end annotation

    .prologue
    .line 915666
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->c:Ljava/lang/String;

    .line 915667
    return-object p0
.end method

.method public setStaticDiskUri(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "static_disk_uri"
    .end annotation

    .prologue
    .line 915668
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->d:Ljava/lang/String;

    .line 915669
    return-object p0
.end method

.method public setStaticWebUri(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "static_web_uri"
    .end annotation

    .prologue
    .line 915670
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->e:Ljava/lang/String;

    .line 915671
    return-object p0
.end method

.method public setStickerId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sticker_id"
    .end annotation

    .prologue
    .line 915672
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->f:Ljava/lang/String;

    .line 915673
    return-object p0
.end method
