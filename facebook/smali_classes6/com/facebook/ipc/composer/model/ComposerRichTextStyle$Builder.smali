.class public final Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerRichTextStyle_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/Boolean;

.field private static final c:Ljava/lang/String;

.field private static final d:LX/5RY;

.field private static final e:Ljava/lang/String;

.field private static final f:LX/5RS;


# instance fields
.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/String;

.field public l:LX/5RY;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:LX/5RS;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915381
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 915382
    new-instance v0, LX/5RU;

    invoke-direct {v0}, LX/5RU;-><init>()V

    .line 915383
    const-string v0, "#FFFFFFFF"

    move-object v0, v0

    .line 915384
    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->a:Ljava/lang/String;

    .line 915385
    new-instance v0, LX/5RW;

    invoke-direct {v0}, LX/5RW;-><init>()V

    .line 915386
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    move-object v0, v0

    .line 915387
    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->b:Ljava/lang/Boolean;

    .line 915388
    new-instance v0, LX/5RV;

    invoke-direct {v0}, LX/5RV;-><init>()V

    .line 915389
    const-string v0, "#FF000000"

    move-object v0, v0

    .line 915390
    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->c:Ljava/lang/String;

    .line 915391
    new-instance v0, LX/2rp;

    invoke-direct {v0}, LX/2rp;-><init>()V

    .line 915392
    sget-object v0, LX/5RY;->NORMAL:LX/5RY;

    move-object v0, v0

    .line 915393
    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->d:LX/5RY;

    .line 915394
    new-instance v0, LX/5RX;

    invoke-direct {v0}, LX/5RX;-><init>()V

    .line 915395
    const-string v0, "default"

    move-object v0, v0

    .line 915396
    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->e:Ljava/lang/String;

    .line 915397
    new-instance v0, LX/5RT;

    invoke-direct {v0}, LX/5RT;-><init>()V

    .line 915398
    sget-object v0, LX/5RS;->LEFT:LX/5RS;

    move-object v0, v0

    .line 915399
    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->f:LX/5RS;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 915320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915321
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->g:Ljava/lang/String;

    .line 915322
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->h:Ljava/lang/String;

    .line 915323
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->i:Ljava/lang/String;

    .line 915324
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->j:Ljava/lang/Boolean;

    .line 915325
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->k:Ljava/lang/String;

    .line 915326
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->d:LX/5RY;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->l:LX/5RY;

    .line 915327
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->m:Ljava/lang/String;

    .line 915328
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->n:Ljava/lang/String;

    .line 915329
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->o:Ljava/lang/String;

    .line 915330
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->p:Ljava/lang/String;

    .line 915331
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->f:LX/5RS;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->q:LX/5RS;

    .line 915332
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)V
    .locals 1

    .prologue
    .line 915354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915355
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 915356
    instance-of v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    if-eqz v0, :cond_0

    .line 915357
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 915358
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->g:Ljava/lang/String;

    .line 915359
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->h:Ljava/lang/String;

    .line 915360
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->i:Ljava/lang/String;

    .line 915361
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->j:Ljava/lang/Boolean;

    .line 915362
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->k:Ljava/lang/String;

    .line 915363
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->f:LX/5RY;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->l:LX/5RY;

    .line 915364
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->m:Ljava/lang/String;

    .line 915365
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->n:Ljava/lang/String;

    .line 915366
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->o:Ljava/lang/String;

    .line 915367
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->p:Ljava/lang/String;

    .line 915368
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->k:LX/5RS;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->q:LX/5RS;

    .line 915369
    :goto_0
    return-void

    .line 915370
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->g:Ljava/lang/String;

    .line 915371
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->h:Ljava/lang/String;

    .line 915372
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->i:Ljava/lang/String;

    .line 915373
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getCanDefault()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->j:Ljava/lang/Boolean;

    .line 915374
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getColor()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->k:Ljava/lang/String;

    .line 915375
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->l:LX/5RY;

    .line 915376
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientDirection()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->m:Ljava/lang/String;

    .line 915377
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->n:Ljava/lang/String;

    .line 915378
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->o:Ljava/lang/String;

    .line 915379
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->p:Ljava/lang/String;

    .line 915380
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getTextAlign()LX/5RS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->q:LX/5RS;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .locals 2

    .prologue
    .line 915353
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;-><init>(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;)V

    return-object v0
.end method

.method public setBackgroundColor(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "background_color"
    .end annotation

    .prologue
    .line 915351
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->g:Ljava/lang/String;

    .line 915352
    return-object p0
.end method

.method public setBackgroundImageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "background_image_name"
    .end annotation

    .prologue
    .line 915349
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->h:Ljava/lang/String;

    .line 915350
    return-object p0
.end method

.method public setBackgroundImageUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "background_image_url"
    .end annotation

    .prologue
    .line 915347
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->i:Ljava/lang/String;

    .line 915348
    return-object p0
.end method

.method public setCanDefault(Ljava/lang/Boolean;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "can_default"
    .end annotation

    .prologue
    .line 915400
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->j:Ljava/lang/Boolean;

    .line 915401
    return-object p0
.end method

.method public setColor(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "color"
    .end annotation

    .prologue
    .line 915345
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->k:Ljava/lang/String;

    .line 915346
    return-object p0
.end method

.method public setFontWeight(LX/5RY;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "font_weight"
    .end annotation

    .prologue
    .line 915343
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->l:LX/5RY;

    .line 915344
    return-object p0
.end method

.method public setGradientDirection(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "gradient_direction"
    .end annotation

    .prologue
    .line 915341
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->m:Ljava/lang/String;

    .line 915342
    return-object p0
.end method

.method public setGradientEndColor(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "gradient_end_color"
    .end annotation

    .prologue
    .line 915339
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->n:Ljava/lang/String;

    .line 915340
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation

    .prologue
    .line 915337
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->o:Ljava/lang/String;

    .line 915338
    return-object p0
.end method

.method public setPresetId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "preset_id"
    .end annotation

    .prologue
    .line 915335
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->p:Ljava/lang/String;

    .line 915336
    return-object p0
.end method

.method public setTextAlign(LX/5RS;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_align"
    .end annotation

    .prologue
    .line 915333
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->q:LX/5RS;

    .line 915334
    return-object p0
.end method
