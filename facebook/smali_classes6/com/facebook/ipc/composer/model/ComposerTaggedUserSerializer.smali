.class public Lcom/facebook/ipc/composer/model/ComposerTaggedUserSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 915938
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerTaggedUserSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerTaggedUserSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 915939
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 915937
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ComposerTaggedUser;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915931
    if-nez p0, :cond_0

    .line 915932
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 915933
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 915934
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerTaggedUserSerializer;->b(Lcom/facebook/ipc/composer/model/ComposerTaggedUser;LX/0nX;LX/0my;)V

    .line 915935
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 915936
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ComposerTaggedUser;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 915926
    const-string v0, "id"

    iget-wide v2, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 915927
    const-string v0, "name"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915928
    const-string v0, "image_url"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mImageUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915929
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915930
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ComposerTaggedUserSerializer;->a(Lcom/facebook/ipc/composer/model/ComposerTaggedUser;LX/0nX;LX/0my;)V

    return-void
.end method
