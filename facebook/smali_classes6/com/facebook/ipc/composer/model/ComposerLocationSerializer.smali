.class public Lcom/facebook/ipc/composer/model/ComposerLocationSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerLocation;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 915196
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerLocation;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerLocationSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 915197
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 915198
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ComposerLocation;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915199
    if-nez p0, :cond_0

    .line 915200
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 915201
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 915202
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerLocationSerializer;->b(Lcom/facebook/ipc/composer/model/ComposerLocation;LX/0nX;LX/0my;)V

    .line 915203
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 915204
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ComposerLocation;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 915205
    const-string v0, "latitude"

    iget-wide v2, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->latitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Double;)V

    .line 915206
    const-string v0, "longitude"

    iget-wide v2, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->longitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Double;)V

    .line 915207
    const-string v0, "accuracy"

    iget v1, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->accuracy:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 915208
    const-string v0, "time"

    iget-wide v2, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->time:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 915209
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915210
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerLocation;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ComposerLocationSerializer;->a(Lcom/facebook/ipc/composer/model/ComposerLocation;LX/0nX;LX/0my;)V

    return-void
.end method
