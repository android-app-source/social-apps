.class public final Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerReshareContext_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915214
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerReshareContext_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 915215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915216
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->a:Ljava/lang/String;

    .line 915217
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->b:Ljava/lang/String;

    .line 915218
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerReshareContext;)V
    .locals 1

    .prologue
    .line 915219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915220
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 915221
    instance-of v0, p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    if-eqz v0, :cond_0

    .line 915222
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    .line 915223
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->a:Ljava/lang/String;

    .line 915224
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->b:Ljava/lang/String;

    .line 915225
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->c:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->c:Z

    .line 915226
    :goto_0
    return-void

    .line 915227
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->getOriginalShareActorName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->a:Ljava/lang/String;

    .line 915228
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->getReshareMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->b:Ljava/lang/String;

    .line 915229
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->shouldIncludeReshareContext()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->c:Z

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/model/ComposerReshareContext;
    .locals 2

    .prologue
    .line 915230
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;-><init>(Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;)V

    return-object v0
.end method

.method public setOriginalShareActorName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "original_share_actor_name"
    .end annotation

    .prologue
    .line 915231
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->a:Ljava/lang/String;

    .line 915232
    return-object p0
.end method

.method public setReshareMessage(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reshare_message"
    .end annotation

    .prologue
    .line 915233
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->b:Ljava/lang/String;

    .line 915234
    return-object p0
.end method

.method public setShouldIncludeReshareContext(Z)Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_include_reshare_context"
    .end annotation

    .prologue
    .line 915235
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->c:Z

    .line 915236
    return-object p0
.end method
