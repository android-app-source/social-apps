.class public Lcom/facebook/ipc/composer/model/ComposerSlideshowDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerSlideshowData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 915624
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerSlideshowDataSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerSlideshowDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 915625
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 915626
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915627
    if-nez p0, :cond_0

    .line 915628
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 915629
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 915630
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerSlideshowDataSerializer;->b(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;LX/0nX;LX/0my;)V

    .line 915631
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 915632
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 915633
    const-string v0, "mood_id"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;->getMoodId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915634
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915635
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ComposerSlideshowDataSerializer;->a(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;LX/0nX;LX/0my;)V

    return-void
.end method
