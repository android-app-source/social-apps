.class public Lcom/facebook/ipc/composer/model/ProductItemPlace;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ProductItemPlaceDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ProductItemPlaceSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ProductItemPlace;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final latitude:D
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "latitude"
    .end annotation
.end field

.field public final locationPageID:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "location_page_id"
    .end annotation
.end field

.field public final longitude:D
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "longitude"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 916608
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemPlaceDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 916630
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemPlaceSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 916629
    new-instance v0, LX/5Rl;

    invoke-direct {v0}, LX/5Rl;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 916627
    new-instance v0, LX/5Rm;

    invoke-direct {v0}, LX/5Rm;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/ipc/composer/model/ProductItemPlace;-><init>(LX/5Rm;)V

    .line 916628
    return-void
.end method

.method public constructor <init>(LX/5Rm;)V
    .locals 2

    .prologue
    .line 916621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916622
    iget-object v0, p1, LX/5Rm;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->name:Ljava/lang/String;

    .line 916623
    iget-object v0, p1, LX/5Rm;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->locationPageID:Ljava/lang/String;

    .line 916624
    iget-wide v0, p1, LX/5Rm;->c:D

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->latitude:D

    .line 916625
    iget-wide v0, p1, LX/5Rm;->d:D

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->longitude:D

    .line 916626
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 916615
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916616
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->name:Ljava/lang/String;

    .line 916617
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->locationPageID:Ljava/lang/String;

    .line 916618
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->latitude:D

    .line 916619
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->longitude:D

    .line 916620
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 916614
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 916609
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916610
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->locationPageID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916611
    iget-wide v0, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->latitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 916612
    iget-wide v0, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->longitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 916613
    return-void
.end method
