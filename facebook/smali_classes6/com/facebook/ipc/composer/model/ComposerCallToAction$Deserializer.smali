.class public final Lcom/facebook/ipc/composer/model/ComposerCallToAction$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerCallToAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/ipc/composer/model/ComposerCallToAction_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 914504
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerCallToAction_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Deserializer;->a:Lcom/facebook/ipc/composer/model/ComposerCallToAction_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 914505
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 914506
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/ipc/composer/model/ComposerCallToAction;
    .locals 1

    .prologue
    .line 914507
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Deserializer;->a:Lcom/facebook/ipc/composer/model/ComposerCallToAction_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;

    .line 914508
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 914509
    invoke-static {p1, p2}, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    move-result-object v0

    return-object v0
.end method
