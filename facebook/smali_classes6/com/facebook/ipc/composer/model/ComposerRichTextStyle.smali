.class public Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/Boolean;

.field public final e:Ljava/lang/String;

.field public final f:LX/5RY;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:LX/5RS;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915433
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915434
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 915435
    new-instance v0, LX/5RR;

    invoke-direct {v0}, LX/5RR;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 915436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915437
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->a:Ljava/lang/String;

    .line 915438
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->b:Ljava/lang/String;

    .line 915439
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->c:Ljava/lang/String;

    .line 915440
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->d:Ljava/lang/Boolean;

    .line 915441
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->e:Ljava/lang/String;

    .line 915442
    invoke-static {}, LX/5RY;->values()[LX/5RY;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->f:LX/5RY;

    .line 915443
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->g:Ljava/lang/String;

    .line 915444
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->h:Ljava/lang/String;

    .line 915445
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->i:Ljava/lang/String;

    .line 915446
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->j:Ljava/lang/String;

    .line 915447
    invoke-static {}, LX/5RS;->values()[LX/5RS;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->k:LX/5RS;

    .line 915448
    return-void

    .line 915449
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;)V
    .locals 1

    .prologue
    .line 915451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915452
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->a:Ljava/lang/String;

    .line 915453
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->b:Ljava/lang/String;

    .line 915454
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->i:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->c:Ljava/lang/String;

    .line 915455
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->j:Ljava/lang/Boolean;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->d:Ljava/lang/Boolean;

    .line 915456
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->e:Ljava/lang/String;

    .line 915457
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->l:LX/5RY;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5RY;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->f:LX/5RY;

    .line 915458
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->m:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->g:Ljava/lang/String;

    .line 915459
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->n:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->h:Ljava/lang/String;

    .line 915460
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->i:Ljava/lang/String;

    .line 915461
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->p:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->j:Ljava/lang/String;

    .line 915462
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->q:LX/5RS;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5RS;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->k:LX/5RS;

    .line 915463
    return-void
.end method

.method public static a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    .locals 2

    .prologue
    .line 915450
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;-><init>(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;
    .locals 2

    .prologue
    .line 915426
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 915465
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 915466
    if-ne p0, p1, :cond_1

    .line 915467
    :cond_0
    :goto_0
    return v0

    .line 915468
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    if-nez v2, :cond_2

    move v0, v1

    .line 915469
    goto :goto_0

    .line 915470
    :cond_2
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 915471
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 915472
    goto :goto_0

    .line 915473
    :cond_3
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 915474
    goto :goto_0

    .line 915475
    :cond_4
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 915476
    goto :goto_0

    .line 915477
    :cond_5
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->d:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->d:Ljava/lang/Boolean;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 915478
    goto :goto_0

    .line 915479
    :cond_6
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 915480
    goto :goto_0

    .line 915481
    :cond_7
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->f:LX/5RY;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->f:LX/5RY;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 915482
    goto :goto_0

    .line 915483
    :cond_8
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 915484
    goto :goto_0

    .line 915485
    :cond_9
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->h:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 915486
    goto :goto_0

    .line 915487
    :cond_a
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->i:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 915488
    goto :goto_0

    .line 915489
    :cond_b
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->j:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 915490
    goto :goto_0

    .line 915491
    :cond_c
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->k:LX/5RS;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->k:LX/5RS;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 915492
    goto/16 :goto_0
.end method

.method public getBackgroundColor()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "background_color"
    .end annotation

    .prologue
    .line 915464
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getBackgroundImageName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "background_image_name"
    .end annotation

    .prologue
    .line 915431
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getBackgroundImageUrl()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "background_image_url"
    .end annotation

    .prologue
    .line 915432
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getCanDefault()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "can_default"
    .end annotation

    .prologue
    .line 915430
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->d:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getColor()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "color"
    .end annotation

    .prologue
    .line 915429
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getFontWeight()LX/5RY;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "font_weight"
    .end annotation

    .prologue
    .line 915428
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->f:LX/5RY;

    return-object v0
.end method

.method public getGradientDirection()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "gradient_direction"
    .end annotation

    .prologue
    .line 915427
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getGradientEndColor()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "gradient_end_color"
    .end annotation

    .prologue
    .line 915425
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation

    .prologue
    .line 915424
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getPresetId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "preset_id"
    .end annotation

    .prologue
    .line 915423
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getTextAlign()LX/5RS;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_align"
    .end annotation

    .prologue
    .line 915422
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->k:LX/5RS;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 915421
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->d:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->f:LX/5RY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->k:LX/5RS;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 915408
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915409
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915410
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915411
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 915412
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915413
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->f:LX/5RY;

    invoke-virtual {v0}, LX/5RY;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 915414
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915415
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915416
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915417
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915418
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->k:LX/5RS;

    invoke-virtual {v0}, LX/5RS;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 915419
    return-void

    .line 915420
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
