.class public Lcom/facebook/ipc/composer/model/ComposerStorylineDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerStorylineData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 915842
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerStorylineDataSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerStorylineDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 915843
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 915853
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ComposerStorylineData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915847
    if-nez p0, :cond_0

    .line 915848
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 915849
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 915850
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerStorylineDataSerializer;->b(Lcom/facebook/ipc/composer/model/ComposerStorylineData;LX/0nX;LX/0my;)V

    .line 915851
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 915852
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ComposerStorylineData;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 915845
    const-string v0, "mood_id"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->getMoodId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915846
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915844
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ComposerStorylineDataSerializer;->a(Lcom/facebook/ipc/composer/model/ComposerStorylineData;LX/0nX;LX/0my;)V

    return-void
.end method
