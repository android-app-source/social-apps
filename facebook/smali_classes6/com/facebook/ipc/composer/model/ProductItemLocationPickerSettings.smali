.class public Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettingsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 916549
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 916550
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettingsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 916559
    new-instance v0, LX/5Rk;

    invoke-direct {v0}, LX/5Rk;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 916551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916552
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->a:Z

    .line 916553
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->b:Z

    .line 916554
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->c:Z

    .line 916555
    return-void

    :cond_0
    move v0, v2

    .line 916556
    goto :goto_0

    :cond_1
    move v0, v2

    .line 916557
    goto :goto_1

    :cond_2
    move v1, v2

    .line 916558
    goto :goto_2
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;)V
    .locals 1

    .prologue
    .line 916560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916561
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->a:Z

    .line 916562
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->b:Z

    .line 916563
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->c:Z

    .line 916564
    return-void
.end method

.method public static newBuilder()Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;
    .locals 2

    .prologue
    .line 916547
    new-instance v0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 916548
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 916536
    if-ne p0, p1, :cond_1

    .line 916537
    :cond_0
    :goto_0
    return v0

    .line 916538
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    if-nez v2, :cond_2

    move v0, v1

    .line 916539
    goto :goto_0

    .line 916540
    :cond_2
    check-cast p1, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    .line 916541
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->a:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 916542
    goto :goto_0

    .line 916543
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->b:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 916544
    goto :goto_0

    .line 916545
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->c:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 916546
    goto :goto_0
.end method

.method public getIsCompulsory()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_compulsory"
    .end annotation

    .prologue
    .line 916535
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->a:Z

    return v0
.end method

.method public getUseNeighborhoodDataSource()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "use_neighborhood_data_source"
    .end annotation

    .prologue
    .line 916534
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->b:Z

    return v0
.end method

.method public getUseZipCode()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "use_zip_code"
    .end annotation

    .prologue
    .line 916533
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->c:Z

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 916532
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 916525
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 916526
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 916527
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->c:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 916528
    return-void

    :cond_0
    move v0, v2

    .line 916529
    goto :goto_0

    :cond_1
    move v0, v2

    .line 916530
    goto :goto_1

    :cond_2
    move v1, v2

    .line 916531
    goto :goto_2
.end method
