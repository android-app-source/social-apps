.class public Lcom/facebook/ipc/composer/model/ComposerStickerData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerStickerData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ComposerStickerDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerStickerData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915739
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerStickerData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915738
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerStickerDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 915737
    new-instance v0, LX/2rq;

    invoke-direct {v0}, LX/2rq;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 915719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915720
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 915721
    iput-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->a:Ljava/lang/String;

    .line 915722
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 915723
    iput-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->b:Ljava/lang/String;

    .line 915724
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 915725
    iput-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->c:Ljava/lang/String;

    .line 915726
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 915727
    iput-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->d:Ljava/lang/String;

    .line 915728
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 915729
    iput-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->e:Ljava/lang/String;

    .line 915730
    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->f:Ljava/lang/String;

    .line 915731
    return-void

    .line 915732
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->a:Ljava/lang/String;

    goto :goto_0

    .line 915733
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->b:Ljava/lang/String;

    goto :goto_1

    .line 915734
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->c:Ljava/lang/String;

    goto :goto_2

    .line 915735
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->d:Ljava/lang/String;

    goto :goto_3

    .line 915736
    :cond_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->e:Ljava/lang/String;

    goto :goto_4
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;)V
    .locals 1

    .prologue
    .line 915711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915712
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->a:Ljava/lang/String;

    .line 915713
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->b:Ljava/lang/String;

    .line 915714
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->c:Ljava/lang/String;

    .line 915715
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->d:Ljava/lang/String;

    .line 915716
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->e:Ljava/lang/String;

    .line 915717
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->f:Ljava/lang/String;

    .line 915718
    return-void
.end method

.method public static newBuilder()Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;
    .locals 2

    .prologue
    .line 915710
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 915709
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 915740
    if-ne p0, p1, :cond_1

    .line 915741
    :cond_0
    :goto_0
    return v0

    .line 915742
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/composer/model/ComposerStickerData;

    if-nez v2, :cond_2

    move v0, v1

    .line 915743
    goto :goto_0

    .line 915744
    :cond_2
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 915745
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerStickerData;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 915746
    goto :goto_0

    .line 915747
    :cond_3
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerStickerData;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 915748
    goto :goto_0

    .line 915749
    :cond_4
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerStickerData;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 915750
    goto :goto_0

    .line 915751
    :cond_5
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerStickerData;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 915752
    goto :goto_0

    .line 915753
    :cond_6
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerStickerData;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 915754
    goto :goto_0

    .line 915755
    :cond_7
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerStickerData;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 915756
    goto :goto_0
.end method

.method public getAnimatedDiskUri()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "animated_disk_uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 915708
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getAnimatedWebUri()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "animated_web_uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 915707
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getPackId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "pack_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 915706
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getStaticDiskUri()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "static_disk_uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 915680
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getStaticWebUri()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "static_web_uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 915705
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getStickerId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sticker_id"
    .end annotation

    .prologue
    .line 915704
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 915703
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 915681
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 915682
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 915683
    :goto_0
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 915684
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 915685
    :goto_1
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 915686
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 915687
    :goto_2
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 915688
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 915689
    :goto_3
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 915690
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 915691
    :goto_4
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915692
    return-void

    .line 915693
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 915694
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 915695
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 915696
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 915697
    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 915698
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 915699
    :cond_3
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 915700
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3

    .line 915701
    :cond_4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 915702
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_4
.end method
