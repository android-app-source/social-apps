.class public final Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo_BuilderDeserializer;
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 916048
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 916049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916050
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 916051
    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->a:LX/0Px;

    .line 916052
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;)V
    .locals 2

    .prologue
    .line 916028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916029
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 916030
    instance-of v0, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    if-eqz v0, :cond_0

    .line 916031
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    .line 916032
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->a:LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->a:LX/0Px;

    .line 916033
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->b:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->b:Z

    .line 916034
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->c:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->c:Z

    .line 916035
    iget-wide v0, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->d:J

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->d:J

    .line 916036
    :goto_0
    return-void

    .line 916037
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getFrames()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->a:LX/0Px;

    .line 916038
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->hasFaceDetectionFinished()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->b:Z

    .line 916039
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->hasFaceboxes()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->c:Z

    .line 916040
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getTimeToFindFirstFaceMs()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->d:J

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;
    .locals 2

    .prologue
    .line 916047
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;-><init>(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;)V

    return-object v0
.end method

.method public setFrames(LX/0Px;)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "frames"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;",
            ">;)",
            "Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;"
        }
    .end annotation

    .prologue
    .line 916053
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->a:LX/0Px;

    .line 916054
    return-object p0
.end method

.method public setHasFaceDetectionFinished(Z)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_face_detection_finished"
    .end annotation

    .prologue
    .line 916045
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->b:Z

    .line 916046
    return-object p0
.end method

.method public setHasFaceboxes(Z)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_faceboxes"
    .end annotation

    .prologue
    .line 916043
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->c:Z

    .line 916044
    return-object p0
.end method

.method public setTimeToFindFirstFaceMs(J)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "time_to_find_first_face_ms"
    .end annotation

    .prologue
    .line 916041
    iput-wide p1, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->d:J

    .line 916042
    return-object p0
.end method
