.class public Lcom/facebook/ipc/composer/model/ComposerStickerDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerStickerData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 915757
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerStickerData;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerStickerDataSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 915758
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 915773
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ComposerStickerData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915767
    if-nez p0, :cond_0

    .line 915768
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 915769
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 915770
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerStickerDataSerializer;->b(Lcom/facebook/ipc/composer/model/ComposerStickerData;LX/0nX;LX/0my;)V

    .line 915771
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 915772
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ComposerStickerData;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 915760
    const-string v0, "animated_disk_uri"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getAnimatedDiskUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915761
    const-string v0, "animated_web_uri"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getAnimatedWebUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915762
    const-string v0, "pack_id"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getPackId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915763
    const-string v0, "static_disk_uri"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticDiskUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915764
    const-string v0, "static_web_uri"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticWebUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915765
    const-string v0, "sticker_id"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915766
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915759
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerStickerData;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ComposerStickerDataSerializer;->a(Lcom/facebook/ipc/composer/model/ComposerStickerData;LX/0nX;LX/0my;)V

    return-void
.end method
