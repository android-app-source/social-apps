.class public Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModelSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 916247
    const-class v0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    new-instance v1, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModelSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModelSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 916248
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 916249
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 916250
    if-nez p0, :cond_0

    .line 916251
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 916252
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 916253
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModelSerializer;->b(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;LX/0nX;LX/0my;)V

    .line 916254
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 916255
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 916256
    const-string v0, "is_enabled"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isEnabled:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 916257
    const-string v0, "is_marketplace_available"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isMarketplaceAvailable:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 916258
    const-string v0, "shold_show_intercept"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->shouldShowIntercept:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 916259
    const-string v0, "shold_show_nux"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->shouldShowNux:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 916260
    const-string v0, "is_compulsory"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isCompulsory:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 916261
    const-string v0, "nux_label"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->nuxLabel:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916262
    const-string v0, "check_box_label"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->checkBoxLabel:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916263
    const-string v0, "upsell_title_label"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellTitleLabel:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916264
    const-string v0, "upsell_subtitle_label"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellSubtitleLabel:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916265
    const-string v0, "intercept_accept_button_label"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->interceptAcceptButtonLabel:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916266
    const-string v0, "intercept_decline_button_label"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->interceptDeclineButtonLabel:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916267
    const-string v0, "upsell_accept_button_label"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellAcceptButtonLabel:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916268
    const-string v0, "upsell_decline_button_label"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellDeclineButtonLabel:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916269
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 916270
    check-cast p1, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModelSerializer;->a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;LX/0nX;LX/0my;)V

    return-void
.end method
