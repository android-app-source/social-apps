.class public Lcom/facebook/ipc/composer/model/ComposerReshareContextSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerReshareContext;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 915292
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerReshareContextSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerReshareContextSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 915293
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 915280
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ComposerReshareContext;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915286
    if-nez p0, :cond_0

    .line 915287
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 915288
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 915289
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerReshareContextSerializer;->b(Lcom/facebook/ipc/composer/model/ComposerReshareContext;LX/0nX;LX/0my;)V

    .line 915290
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 915291
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ComposerReshareContext;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 915282
    const-string v0, "original_share_actor_name"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->getOriginalShareActorName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915283
    const-string v0, "reshare_message"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->getReshareMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915284
    const-string v0, "should_include_reshare_context"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->shouldIncludeReshareContext()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 915285
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915281
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ComposerReshareContextSerializer;->a(Lcom/facebook/ipc/composer/model/ComposerReshareContext;LX/0nX;LX/0my;)V

    return-void
.end method
