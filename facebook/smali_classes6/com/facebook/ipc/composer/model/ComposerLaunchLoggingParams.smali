.class public Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParamsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/5RI;

.field public final b:Ljava/lang/String;

.field public final c:LX/21D;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 914942
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 914941
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParamsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 914940
    new-instance v0, LX/5RJ;

    invoke-direct {v0}, LX/5RJ;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 914935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914936
    invoke-static {}, LX/5RI;->values()[LX/5RI;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->a:LX/5RI;

    .line 914937
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->b:Ljava/lang/String;

    .line 914938
    invoke-static {}, LX/21D;->values()[LX/21D;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->c:LX/21D;

    .line 914939
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;)V
    .locals 1

    .prologue
    .line 914930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914931
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->c:LX/5RI;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5RI;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->a:LX/5RI;

    .line 914932
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->b:Ljava/lang/String;

    .line 914933
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->e:LX/21D;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/21D;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->c:LX/21D;

    .line 914934
    return-void
.end method

.method public static a(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;
    .locals 2

    .prologue
    .line 914908
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;-><init>(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;
    .locals 2

    .prologue
    .line 914929
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 914928
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 914917
    if-ne p0, p1, :cond_1

    .line 914918
    :cond_0
    :goto_0
    return v0

    .line 914919
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 914920
    goto :goto_0

    .line 914921
    :cond_2
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    .line 914922
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->a:LX/5RI;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->a:LX/5RI;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 914923
    goto :goto_0

    .line 914924
    :cond_3
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 914925
    goto :goto_0

    .line 914926
    :cond_4
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->c:LX/21D;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->c:LX/21D;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 914927
    goto :goto_0
.end method

.method public getEntryPicker()LX/5RI;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "entry_picker"
    .end annotation

    .prologue
    .line 914916
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->a:LX/5RI;

    return-object v0
.end method

.method public getEntryPointName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "entry_point_name"
    .end annotation

    .prologue
    .line 914915
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceSurface()LX/21D;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "source_surface"
    .end annotation

    .prologue
    .line 914914
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->c:LX/21D;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 914913
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->a:LX/5RI;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->c:LX/21D;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 914909
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->a:LX/5RI;

    invoke-virtual {v0}, LX/5RI;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 914910
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 914911
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->c:LX/21D;

    invoke-virtual {v0}, LX/21D;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 914912
    return-void
.end method
