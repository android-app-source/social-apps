.class public Lcom/facebook/ipc/composer/model/ComposerDateInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerDateInfoDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ComposerDateInfoSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerDateInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mEndDate:Lcom/facebook/uicontrib/datepicker/Date;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "end_date"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mIsCurrent:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_current"
    .end annotation
.end field

.field public final mStartDate:Lcom/facebook/uicontrib/datepicker/Date;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "start_date"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 914814
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerDateInfoDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 914815
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerDateInfoSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 914798
    new-instance v0, LX/5RG;

    invoke-direct {v0}, LX/5RG;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 914812
    new-instance v0, LX/5RH;

    invoke-direct {v0}, LX/5RH;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/ipc/composer/model/ComposerDateInfo;-><init>(LX/5RH;)V

    .line 914813
    return-void
.end method

.method public constructor <init>(LX/5RH;)V
    .locals 1

    .prologue
    .line 914807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914808
    iget-object v0, p1, LX/5RH;->a:Lcom/facebook/uicontrib/datepicker/Date;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->mStartDate:Lcom/facebook/uicontrib/datepicker/Date;

    .line 914809
    iget-object v0, p1, LX/5RH;->b:Lcom/facebook/uicontrib/datepicker/Date;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->mEndDate:Lcom/facebook/uicontrib/datepicker/Date;

    .line 914810
    iget-boolean v0, p1, LX/5RH;->c:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->mIsCurrent:Z

    .line 914811
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 914816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914817
    const-class v0, Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/Date;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->mStartDate:Lcom/facebook/uicontrib/datepicker/Date;

    .line 914818
    const-class v0, Lcom/facebook/uicontrib/datepicker/DatePicker;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/Date;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->mEndDate:Lcom/facebook/uicontrib/datepicker/Date;

    .line 914819
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->mIsCurrent:Z

    .line 914820
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/uicontrib/datepicker/Date;
    .locals 1

    .prologue
    .line 914806
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->mStartDate:Lcom/facebook/uicontrib/datepicker/Date;

    return-object v0
.end method

.method public final b()Lcom/facebook/uicontrib/datepicker/Date;
    .locals 1

    .prologue
    .line 914805
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->mEndDate:Lcom/facebook/uicontrib/datepicker/Date;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 914804
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->mIsCurrent:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 914803
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 914799
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->mStartDate:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 914800
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->mEndDate:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 914801
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->mIsCurrent:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 914802
    return-void
.end method
