.class public Lcom/facebook/ipc/composer/model/ComposerCommerceInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 914728
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 914729
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 914730
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 914731
    if-nez p0, :cond_0

    .line 914732
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 914733
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 914734
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfoSerializer;->b(Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;LX/0nX;LX/0my;)V

    .line 914735
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 914736
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 914737
    const-string v0, "currency_code"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 914738
    const-string v0, "intercept_words"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getInterceptWords()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 914739
    const-string v0, "intercept_words_after_number"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getInterceptWordsAfterNumber()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 914740
    const-string v0, "is_category_optional"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getIsCategoryOptional()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 914741
    const-string v0, "marketplace_cross_post_setting_model"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getMarketplaceCrossPostSettingModel()Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 914742
    const-string v0, "prefill_category_id"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getPrefillCategoryId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 914743
    const-string v0, "product_item_location_picker_settings"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getProductItemLocationPickerSettings()Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 914744
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 914745
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfoSerializer;->a(Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;LX/0nX;LX/0my;)V

    return-void
.end method
