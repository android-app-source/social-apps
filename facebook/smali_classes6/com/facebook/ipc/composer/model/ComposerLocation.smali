.class public Lcom/facebook/ipc/composer/model/ComposerLocation;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerLocationDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ComposerLocationSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerLocation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final accuracy:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "accuracy"
    .end annotation
.end field

.field public final latitude:D
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "latitude"
    .end annotation
.end field

.field public final longitude:D
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "longitude"
    .end annotation
.end field

.field public final time:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "time"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915016
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerLocationDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915015
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerLocationSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 914985
    new-instance v0, LX/5RM;

    invoke-direct {v0}, LX/5RM;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 9
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 915013
    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v1 .. v8}, Lcom/facebook/ipc/composer/model/ComposerLocation;-><init>(DDFJ)V

    .line 915014
    return-void
.end method

.method private constructor <init>(DDFJ)V
    .locals 1

    .prologue
    .line 915007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915008
    iput-wide p1, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->latitude:D

    .line 915009
    iput-wide p3, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->longitude:D

    .line 915010
    iput p5, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->accuracy:F

    .line 915011
    iput-wide p6, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->time:J

    .line 915012
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 915001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915002
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->latitude:D

    .line 915003
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->longitude:D

    .line 915004
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->accuracy:F

    .line 915005
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->time:J

    .line 915006
    return-void
.end method

.method public static a(Landroid/location/Location;)Lcom/facebook/ipc/composer/model/ComposerLocation;
    .locals 9
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 914998
    if-nez p0, :cond_0

    .line 914999
    const/4 v1, 0x0

    .line 915000
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerLocation;

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v6

    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v7

    invoke-direct/range {v1 .. v8}, Lcom/facebook/ipc/composer/model/ComposerLocation;-><init>(DDFJ)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/location/Location;
    .locals 4
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 914992
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 914993
    iget-wide v2, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->latitude:D

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 914994
    iget-wide v2, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->longitude:D

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 914995
    iget v1, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->accuracy:F

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    .line 914996
    iget-wide v2, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->time:J

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setTime(J)V

    .line 914997
    return-object v0
.end method

.method public final describeContents()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 914991
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 914986
    iget-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->latitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 914987
    iget-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->longitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 914988
    iget v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->accuracy:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 914989
    iget-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocation;->time:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 914990
    return-void
.end method
