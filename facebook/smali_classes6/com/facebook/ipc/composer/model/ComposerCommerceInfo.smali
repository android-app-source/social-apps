.class public Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ComposerCommerceInfoSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z

.field private final e:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 914725
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 914696
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfoSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 914697
    new-instance v0, LX/5RD;

    invoke-direct {v0}, LX/5RD;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 914698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914699
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 914700
    iput-object v5, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->a:Ljava/lang/String;

    .line 914701
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    move v0, v1

    .line 914702
    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 914703
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 914704
    aput-object v4, v3, v0

    .line 914705
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 914706
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->a:Ljava/lang/String;

    goto :goto_0

    .line 914707
    :cond_1
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->b:LX/0Px;

    .line 914708
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    move v0, v1

    .line 914709
    :goto_2
    array-length v4, v3

    if-ge v0, v4, :cond_2

    .line 914710
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 914711
    aput-object v4, v3, v0

    .line 914712
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 914713
    :cond_2
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->c:LX/0Px;

    .line 914714
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v2, :cond_3

    move v1, v2

    :cond_3
    iput-boolean v1, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->d:Z

    .line 914715
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 914716
    iput-object v5, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->e:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    .line 914717
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    .line 914718
    iput-object v5, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->f:Ljava/lang/String;

    .line 914719
    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    .line 914720
    iput-object v5, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->g:Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    .line 914721
    :goto_5
    return-void

    .line 914722
    :cond_4
    sget-object v0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->e:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    goto :goto_3

    .line 914723
    :cond_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->f:Ljava/lang/String;

    goto :goto_4

    .line 914724
    :cond_6
    sget-object v0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->g:Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    goto :goto_5
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;)V
    .locals 1

    .prologue
    .line 914667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914668
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->a:Ljava/lang/String;

    .line 914669
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->b:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->b:LX/0Px;

    .line 914670
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->c:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->c:LX/0Px;

    .line 914671
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->d:Z

    .line 914672
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->e:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->e:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    .line 914673
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->f:Ljava/lang/String;

    .line 914674
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->g:Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->g:Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    .line 914675
    return-void
.end method

.method public static newBuilder()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;
    .locals 2

    .prologue
    .line 914726
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 914727
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 914676
    if-ne p0, p1, :cond_1

    .line 914677
    :cond_0
    :goto_0
    return v0

    .line 914678
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    if-nez v2, :cond_2

    move v0, v1

    .line 914679
    goto :goto_0

    .line 914680
    :cond_2
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    .line 914681
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 914682
    goto :goto_0

    .line 914683
    :cond_3
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->b:LX/0Px;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->b:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 914684
    goto :goto_0

    .line 914685
    :cond_4
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->c:LX/0Px;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->c:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 914686
    goto :goto_0

    .line 914687
    :cond_5
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->d:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 914688
    goto :goto_0

    .line 914689
    :cond_6
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->e:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->e:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 914690
    goto :goto_0

    .line 914691
    :cond_7
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 914692
    goto :goto_0

    .line 914693
    :cond_8
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->g:Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->g:Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 914694
    goto :goto_0
.end method

.method public getCurrencyCode()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "currency_code"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 914695
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getInterceptWords()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "intercept_words"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 914633
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->b:LX/0Px;

    return-object v0
.end method

.method public getInterceptWordsAfterNumber()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "intercept_words_after_number"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 914634
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->c:LX/0Px;

    return-object v0
.end method

.method public getIsCategoryOptional()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_category_optional"
    .end annotation

    .prologue
    .line 914635
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->d:Z

    return v0
.end method

.method public getMarketplaceCrossPostSettingModel()Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "marketplace_cross_post_setting_model"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 914636
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->e:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    return-object v0
.end method

.method public getPrefillCategoryId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prefill_category_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 914637
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getProductItemLocationPickerSettings()Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "product_item_location_picker_settings"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 914638
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->g:Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 914639
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->b:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->c:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->e:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->g:Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 914640
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 914641
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 914642
    :goto_0
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 914643
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_1

    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->b:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 914644
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 914645
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 914646
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 914647
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 914648
    :cond_1
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 914649
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_2

    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->c:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 914650
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 914651
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 914652
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->d:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 914653
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->e:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    if-nez v0, :cond_4

    .line 914654
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 914655
    :goto_4
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->f:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 914656
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 914657
    :goto_5
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->g:Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    if-nez v0, :cond_6

    .line 914658
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 914659
    :goto_6
    return-void

    :cond_3
    move v0, v2

    .line 914660
    goto :goto_3

    .line 914661
    :cond_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 914662
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->e:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_4

    .line 914663
    :cond_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 914664
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_5

    .line 914665
    :cond_6
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 914666
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->g:Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_6
.end method
