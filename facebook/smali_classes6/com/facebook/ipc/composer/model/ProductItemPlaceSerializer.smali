.class public Lcom/facebook/ipc/composer/model/ProductItemPlaceSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ProductItemPlace;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 916662
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemPlace;

    new-instance v1, Lcom/facebook/ipc/composer/model/ProductItemPlaceSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ProductItemPlaceSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 916663
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 916656
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ProductItemPlace;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 916664
    if-nez p0, :cond_0

    .line 916665
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 916666
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 916667
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ProductItemPlaceSerializer;->b(Lcom/facebook/ipc/composer/model/ProductItemPlace;LX/0nX;LX/0my;)V

    .line 916668
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 916669
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ProductItemPlace;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 916657
    const-string v0, "name"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->name:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916658
    const-string v0, "location_page_id"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->locationPageID:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916659
    const-string v0, "latitude"

    iget-wide v2, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->latitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Double;)V

    .line 916660
    const-string v0, "longitude"

    iget-wide v2, p0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->longitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Double;)V

    .line 916661
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 916655
    check-cast p1, Lcom/facebook/ipc/composer/model/ProductItemPlace;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ProductItemPlaceSerializer;->a(Lcom/facebook/ipc/composer/model/ProductItemPlace;LX/0nX;LX/0my;)V

    return-void
.end method
