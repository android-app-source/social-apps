.class public Lcom/facebook/ipc/composer/model/ComposerLocationInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerLocationInfoDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ComposerLocationInfoSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerLocationInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mImplicitLoc:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "implicit_loc"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mIsCheckin:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_checkin"
    .end annotation
.end field

.field public final mLightweightPlacePickerPlaces:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "lightweight_place_picker_places"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;",
            ">;"
        }
    .end annotation
.end field

.field public final mLightweightPlacePickerSearchResultsId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "lightweight_place_picker_search_results_id"
    .end annotation
.end field

.field public final mLightweightPlacePickerSessionId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "lightweight_place_picker_session_id"
    .end annotation
.end field

.field public final mPlaceAttachmentRemoved:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "place_attachment_removed"
    .end annotation
.end field

.field public final mTaggedPlace:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tagged_place"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mTextOnlyPlace:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_only_place"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mXedLocation:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "xed_location"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915141
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerLocationInfoDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915145
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerLocationInfoSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 915144
    new-instance v0, LX/5RN;

    invoke-direct {v0}, LX/5RN;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 915142
    new-instance v0, LX/5RO;

    invoke-direct {v0}, LX/5RO;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;-><init>(LX/5RO;)V

    .line 915143
    return-void
.end method

.method public constructor <init>(LX/5RO;)V
    .locals 1

    .prologue
    .line 915130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915131
    iget-object v0, p1, LX/5RO;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mTaggedPlace:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 915132
    iget-boolean v0, p1, LX/5RO;->b:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mPlaceAttachmentRemoved:Z

    .line 915133
    iget-object v0, p1, LX/5RO;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mTextOnlyPlace:Ljava/lang/String;

    .line 915134
    iget-boolean v0, p1, LX/5RO;->d:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mIsCheckin:Z

    .line 915135
    iget-boolean v0, p1, LX/5RO;->e:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mXedLocation:Z

    .line 915136
    iget-object v0, p1, LX/5RO;->f:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mImplicitLoc:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 915137
    iget-object v0, p1, LX/5RO;->g:LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerPlaces:LX/0Px;

    .line 915138
    iget-object v0, p1, LX/5RO;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerSessionId:Ljava/lang/String;

    .line 915139
    iget-object v0, p1, LX/5RO;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerSearchResultsId:Ljava/lang/String;

    .line 915140
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 915119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915120
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mTaggedPlace:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 915121
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mTextOnlyPlace:Ljava/lang/String;

    .line 915122
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mIsCheckin:Z

    .line 915123
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mXedLocation:Z

    .line 915124
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mPlaceAttachmentRemoved:Z

    .line 915125
    const-class v0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mImplicitLoc:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 915126
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerPlaces:LX/0Px;

    .line 915127
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerSessionId:Ljava/lang/String;

    .line 915128
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerSearchResultsId:Ljava/lang/String;

    .line 915129
    return-void
.end method

.method public static a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)LX/5RO;
    .locals 2

    .prologue
    .line 915118
    new-instance v0, LX/5RO;

    invoke-direct {v0, p0}, LX/5RO;-><init>(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)V

    return-object v0
.end method

.method public static newBuilder()LX/5RO;
    .locals 2

    .prologue
    .line 915117
    new-instance v0, LX/5RO;

    invoke-direct {v0}, LX/5RO;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 915116
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mTaggedPlace:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 915115
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mPlaceAttachmentRemoved:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 915146
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mTextOnlyPlace:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 915114
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mIsCheckin:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 915113
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 915112
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mXedLocation:Z

    return v0
.end method

.method public final f()Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 915111
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mImplicitLoc:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    return-object v0
.end method

.method public final g()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;"
        }
    .end annotation

    .prologue
    .line 915110
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerPlaces:LX/0Px;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 915109
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 915091
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerSearchResultsId:Ljava/lang/String;

    return-object v0
.end method

.method public final j()J
    .locals 3

    .prologue
    .line 915103
    const-wide/16 v0, -0x1

    .line 915104
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 915105
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 915106
    :cond_0
    :goto_0
    return-wide v0

    .line 915107
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->f()Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 915108
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->f()Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    move-result-object v0

    iget-wide v0, v0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->pageId:J

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 915102
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 915092
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mTaggedPlace:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 915093
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mTextOnlyPlace:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915094
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mIsCheckin:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 915095
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mXedLocation:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 915096
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mPlaceAttachmentRemoved:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 915097
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mImplicitLoc:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 915098
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerPlaces:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 915099
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915100
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerSearchResultsId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915101
    return-void
.end method
