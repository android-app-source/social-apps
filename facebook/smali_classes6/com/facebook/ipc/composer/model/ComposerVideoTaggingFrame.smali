.class public Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrameSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/ipc/media/data/MediaData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915989
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915988
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrameSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 915987
    new-instance v0, LX/5Rd;

    invoke-direct {v0}, LX/5Rd;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 915981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915982
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 915983
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->a:Lcom/facebook/ipc/media/data/MediaData;

    .line 915984
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->b:J

    .line 915985
    return-void

    .line 915986
    :cond_0
    sget-object v0, Lcom/facebook/ipc/media/data/MediaData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/data/MediaData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->a:Lcom/facebook/ipc/media/data/MediaData;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;)V
    .locals 2

    .prologue
    .line 915977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915978
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;->a:Lcom/facebook/ipc/media/data/MediaData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->a:Lcom/facebook/ipc/media/data/MediaData;

    .line 915979
    iget-wide v0, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->b:J

    .line 915980
    return-void
.end method

.method public static newBuilder()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;
    .locals 2

    .prologue
    .line 915976
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 915957
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 915967
    if-ne p0, p1, :cond_1

    .line 915968
    :cond_0
    :goto_0
    return v0

    .line 915969
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    if-nez v2, :cond_2

    move v0, v1

    .line 915970
    goto :goto_0

    .line 915971
    :cond_2
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    .line 915972
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->a:Lcom/facebook/ipc/media/data/MediaData;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->a:Lcom/facebook/ipc/media/data/MediaData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 915973
    goto :goto_0

    .line 915974
    :cond_3
    iget-wide v2, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->b:J

    iget-wide v4, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 915975
    goto :goto_0
.end method

.method public getMediaData()Lcom/facebook/ipc/media/data/MediaData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 915966
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->a:Lcom/facebook/ipc/media/data/MediaData;

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "timestamp"
    .end annotation

    .prologue
    .line 915965
    iget-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->b:J

    return-wide v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 915964
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->a:Lcom/facebook/ipc/media/data/MediaData;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 915958
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->a:Lcom/facebook/ipc/media/data/MediaData;

    if-nez v0, :cond_0

    .line 915959
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 915960
    :goto_0
    iget-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 915961
    return-void

    .line 915962
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 915963
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->a:Lcom/facebook/ipc/media/data/MediaData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/media/data/MediaData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0
.end method
