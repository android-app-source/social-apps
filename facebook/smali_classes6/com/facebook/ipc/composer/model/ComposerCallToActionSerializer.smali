.class public Lcom/facebook/ipc/composer/model/ComposerCallToActionSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerCallToAction;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 914561
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerCallToActionSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerCallToActionSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 914562
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 914563
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ComposerCallToAction;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 914564
    if-nez p0, :cond_0

    .line 914565
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 914566
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 914567
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerCallToActionSerializer;->b(Lcom/facebook/ipc/composer/model/ComposerCallToAction;LX/0nX;LX/0my;)V

    .line 914568
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 914569
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ComposerCallToAction;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 914570
    const-string v0, "call_to_action_type"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getCallToActionType()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 914571
    const-string v0, "label"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 914572
    const-string v0, "link"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getLink()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 914573
    const-string v0, "link_image"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getLinkImage()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 914574
    const-string v0, "title"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 914575
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 914576
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ComposerCallToActionSerializer;->a(Lcom/facebook/ipc/composer/model/ComposerCallToAction;LX/0nX;LX/0my;)V

    return-void
.end method
