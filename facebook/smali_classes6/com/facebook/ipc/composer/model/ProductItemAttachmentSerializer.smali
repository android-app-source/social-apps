.class public Lcom/facebook/ipc/composer/model/ProductItemAttachmentSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ProductItemAttachment;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 916485
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    new-instance v1, Lcom/facebook/ipc/composer/model/ProductItemAttachmentSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ProductItemAttachmentSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 916486
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 916487
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ProductItemAttachment;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 916488
    if-nez p0, :cond_0

    .line 916489
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 916490
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 916491
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ProductItemAttachmentSerializer;->b(Lcom/facebook/ipc/composer/model/ProductItemAttachment;LX/0nX;LX/0my;)V

    .line 916492
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 916493
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ProductItemAttachment;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 916494
    const-string v0, "title"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->title:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916495
    const-string v0, "pickup_delivery_info"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->pickupDeliveryInfo:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916496
    const-string v0, "description"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->description:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916497
    const-string v0, "price"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->price:Ljava/lang/Long;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 916498
    const-string v0, "currency"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->currencyCode:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916499
    const-string v0, "category_id"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->categoryID:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916500
    const-string v0, "latitude"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->latitude:Ljava/lang/Double;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Double;)V

    .line 916501
    const-string v0, "longitude"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->longitude:Ljava/lang/Double;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Double;)V

    .line 916502
    const-string v0, "draft_type"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->draftType:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916503
    const-string v0, "location_page_id"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->getLocationPageID()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916504
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 916505
    check-cast p1, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ProductItemAttachmentSerializer;->a(Lcom/facebook/ipc/composer/model/ProductItemAttachment;LX/0nX;LX/0my;)V

    return-void
.end method
