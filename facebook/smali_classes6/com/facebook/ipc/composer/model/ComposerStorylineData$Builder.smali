.class public final Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerStorylineData_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915803
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerStorylineData_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 915806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915807
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/model/ComposerStorylineData;
    .locals 2

    .prologue
    .line 915808
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerStorylineData;-><init>(Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;)V

    return-object v0
.end method

.method public setMoodId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mood_id"
    .end annotation

    .prologue
    .line 915804
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;->a:Ljava/lang/String;

    .line 915805
    return-object p0
.end method
