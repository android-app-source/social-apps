.class public Lcom/facebook/ipc/composer/model/MinutiaeTagSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/MinutiaeTag;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 916362
    const-class v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;

    new-instance v1, Lcom/facebook/ipc/composer/model/MinutiaeTagSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/MinutiaeTagSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 916363
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 916364
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/MinutiaeTag;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 916365
    if-nez p0, :cond_0

    .line 916366
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 916367
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 916368
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/MinutiaeTagSerializer;->b(Lcom/facebook/ipc/composer/model/MinutiaeTag;LX/0nX;LX/0my;)V

    .line 916369
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 916370
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/MinutiaeTag;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 916371
    const-string v0, "og_action_type_id"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogActionTypeId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916372
    const-string v0, "og_object_id"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogObjectId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916373
    const-string v0, "og_phrase"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogPhrase:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916374
    const-string v0, "og_icon_id"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogIconId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916375
    const-string v0, "oh_hide_attachment"

    iget-boolean v1, p0, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogHideAttachment:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 916376
    const-string v0, "og_suggestion_mechanism"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogSuggestionMechanism:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 916377
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 916378
    check-cast p1, Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/MinutiaeTagSerializer;->a(Lcom/facebook/ipc/composer/model/MinutiaeTag;LX/0nX;LX/0my;)V

    return-void
.end method
