.class public Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettingsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 916565
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    new-instance v1, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettingsSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettingsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 916566
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 916567
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 916568
    if-nez p0, :cond_0

    .line 916569
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 916570
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 916571
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettingsSerializer;->b(Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;LX/0nX;LX/0my;)V

    .line 916572
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 916573
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 916574
    const-string v0, "is_compulsory"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->getIsCompulsory()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 916575
    const-string v0, "use_neighborhood_data_source"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->getUseNeighborhoodDataSource()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 916576
    const-string v0, "use_zip_code"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->getUseZipCode()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 916577
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 916578
    check-cast p1, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettingsSerializer;->a(Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;LX/0nX;LX/0my;)V

    return-void
.end method
