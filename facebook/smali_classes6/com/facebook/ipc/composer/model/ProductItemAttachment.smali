.class public Lcom/facebook/ipc/composer/model/ProductItemAttachment;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ProductItemAttachmentDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ProductItemAttachmentSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ProductItemAttachment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/ipc/composer/model/ProductItemPlace;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public final b:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public final c:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public final categoryID:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "category_id"
    .end annotation
.end field

.field public final currencyCode:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "currency"
    .end annotation
.end field

.field public final d:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "description"
    .end annotation
.end field

.field public final draftType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "draft_type"
    .end annotation
.end field

.field public final latitude:Ljava/lang/Double;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "latitude"
    .end annotation
.end field

.field public final longitude:Ljava/lang/Double;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "longitude"
    .end annotation
.end field

.field public final pickupDeliveryInfo:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "pickup_delivery_info"
    .end annotation
.end field

.field public final price:Ljava/lang/Long;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "price"
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 916455
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemAttachmentDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 916454
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemAttachmentSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 916453
    new-instance v0, LX/5Ri;

    invoke-direct {v0}, LX/5Ri;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 916451
    new-instance v0, LX/5Rj;

    invoke-direct {v0}, LX/5Rj;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;-><init>(LX/5Rj;)V

    .line 916452
    return-void
.end method

.method public constructor <init>(LX/5Rj;)V
    .locals 1

    .prologue
    .line 916403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916404
    iget-object v0, p1, LX/5Rj;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->title:Ljava/lang/String;

    .line 916405
    iget-object v0, p1, LX/5Rj;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->pickupDeliveryInfo:Ljava/lang/String;

    .line 916406
    iget-object v0, p1, LX/5Rj;->c:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->a:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    .line 916407
    iget-object v0, p1, LX/5Rj;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->description:Ljava/lang/String;

    .line 916408
    iget-object v0, p1, LX/5Rj;->e:Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->price:Ljava/lang/Long;

    .line 916409
    iget-object v0, p1, LX/5Rj;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->currencyCode:Ljava/lang/String;

    .line 916410
    iget-object v0, p1, LX/5Rj;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->categoryID:Ljava/lang/String;

    .line 916411
    iget-object v0, p1, LX/5Rj;->i:Ljava/lang/Double;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->latitude:Ljava/lang/Double;

    .line 916412
    iget-object v0, p1, LX/5Rj;->j:Ljava/lang/Double;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->longitude:Ljava/lang/Double;

    .line 916413
    iget-boolean v0, p1, LX/5Rj;->k:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->b:Z

    .line 916414
    iget-boolean v0, p1, LX/5Rj;->l:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->c:Z

    .line 916415
    iget-boolean v0, p1, LX/5Rj;->m:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->d:Z

    .line 916416
    iget-object v0, p1, LX/5Rj;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->draftType:Ljava/lang/String;

    .line 916417
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 916436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916437
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->title:Ljava/lang/String;

    .line 916438
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->pickupDeliveryInfo:Ljava/lang/String;

    .line 916439
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemPlace;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ProductItemPlace;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->a:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    .line 916440
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->description:Ljava/lang/String;

    .line 916441
    invoke-static {p1}, LX/46R;->d(Landroid/os/Parcel;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->price:Ljava/lang/Long;

    .line 916442
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->currencyCode:Ljava/lang/String;

    .line 916443
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->categoryID:Ljava/lang/String;

    .line 916444
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->latitude:Ljava/lang/Double;

    .line 916445
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->longitude:Ljava/lang/Double;

    .line 916446
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->draftType:Ljava/lang/String;

    .line 916447
    iput-boolean v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->b:Z

    .line 916448
    iput-boolean v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->c:Z

    .line 916449
    iput-boolean v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->d:Z

    .line 916450
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 916435
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->title:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 916434
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->price:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 916433
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->description:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 916432
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->categoryID:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 916431
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 916430
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->categoryID:Ljava/lang/String;

    if-eqz v1, :cond_3

    :cond_0
    iget-boolean v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->d:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->pickupDeliveryInfo:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->pickupDeliveryInfo:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    iget-boolean v1, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->c:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x5

    :goto_0
    if-lt v2, v1, :cond_3

    :cond_1
    :goto_1
    return v0

    :cond_2
    move v1, v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getLocationPageID()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "location_page_id"
    .end annotation

    .prologue
    .line 916429
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->a:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->a:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    iget-object v0, v0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->locationPageID:Ljava/lang/String;

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 916418
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916419
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->pickupDeliveryInfo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916420
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->a:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 916421
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916422
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->price:Ljava/lang/Long;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Long;)V

    .line 916423
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->currencyCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916424
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->categoryID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916425
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->latitude:Ljava/lang/Double;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 916426
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->longitude:Ljava/lang/Double;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 916427
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->draftType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916428
    return-void
.end method
