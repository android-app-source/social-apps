.class public Lcom/facebook/ipc/composer/model/ComposerCommerceInfo_BuilderDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/common/json/FbJsonField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 914746
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo_BuilderDeserializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo_BuilderDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 914747
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 914748
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 914749
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    invoke-virtual {p0, v0}, Lcom/facebook/common/json/FbJsonDeserializer;->init(Ljava/lang/Class;)V

    .line 914750
    return-void
.end method


# virtual methods
.method public final getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 914751
    const-class v3, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo_BuilderDeserializer;

    monitor-enter v3

    .line 914752
    :try_start_0
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo_BuilderDeserializer;->a:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 914753
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo_BuilderDeserializer;->a:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 914754
    :cond_0
    const/4 v0, -0x1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_1
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 914755
    invoke-super {p0, p1}, Lcom/facebook/common/json/FbJsonDeserializer;->getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v3

    .line 914756
    :goto_1
    return-object v0

    .line 914757
    :cond_2
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo_BuilderDeserializer;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/json/FbJsonField;

    .line 914758
    if-eqz v0, :cond_0

    .line 914759
    monitor-exit v3

    goto :goto_1

    .line 914760
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 914761
    :sswitch_0
    :try_start_3
    const-string v2, "currency_code"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v1, "intercept_words"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v1, "intercept_words_after_number"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "is_category_optional"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "marketplace_cross_post_setting_model"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v1, "prefill_category_id"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v1, "product_item_location_picker_settings"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x6

    goto :goto_0

    .line 914762
    :pswitch_0
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    const-string v1, "setCurrencyCode"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 914763
    :goto_2
    :try_start_4
    sget-object v1, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo_BuilderDeserializer;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 914764
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 914765
    :pswitch_1
    :try_start_5
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    const-string v1, "setInterceptWords"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, LX/0Px;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;Ljava/lang/Class;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 914766
    :pswitch_2
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    const-string v1, "setInterceptWordsAfterNumber"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, LX/0Px;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;Ljava/lang/Class;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 914767
    :pswitch_3
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    const-string v1, "setIsCategoryOptional"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 914768
    :pswitch_4
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    const-string v1, "setMarketplaceCrossPostSettingModel"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 914769
    :pswitch_5
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    const-string v1, "setPrefillCategoryId"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 914770
    :pswitch_6
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    const-string v1, "setProductItemLocationPickerSettings"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    goto/16 :goto_2

    .line 914771
    :catch_0
    move-exception v0

    .line 914772
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :sswitch_data_0
    .sparse-switch
        -0x42265b7a -> :sswitch_6
        -0x2afdd312 -> :sswitch_4
        -0x1913c254 -> :sswitch_3
        0x1e5e077f -> :sswitch_2
        0x310fdfe3 -> :sswitch_5
        0x4215d95b -> :sswitch_0
        0x5db284ec -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
