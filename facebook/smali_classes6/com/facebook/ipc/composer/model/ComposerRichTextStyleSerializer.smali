.class public Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 915493
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 915494
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 915495
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915496
    if-nez p0, :cond_0

    .line 915497
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 915498
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 915499
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSerializer;->b(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0nX;LX/0my;)V

    .line 915500
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 915501
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 915502
    const-string v0, "background_color"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915503
    const-string v0, "background_image_name"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915504
    const-string v0, "background_image_url"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915505
    const-string v0, "can_default"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getCanDefault()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 915506
    const-string v0, "color"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getColor()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915507
    const-string v0, "font_weight"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 915508
    const-string v0, "gradient_direction"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientDirection()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915509
    const-string v0, "gradient_end_color"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915510
    const-string v0, "name"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915511
    const-string v0, "preset_id"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915512
    const-string v0, "text_align"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getTextAlign()LX/5RS;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 915513
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915514
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSerializer;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0nX;LX/0my;)V

    return-void
.end method
