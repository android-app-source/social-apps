.class public Lcom/facebook/ipc/composer/model/ComposerReshareContext;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerReshareContext$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ComposerReshareContextSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerReshareContext;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915279
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915278
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerReshareContextSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 915277
    new-instance v0, LX/5RQ;

    invoke-direct {v0}, LX/5RQ;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 915271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915272
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->a:Ljava/lang/String;

    .line 915273
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->b:Ljava/lang/String;

    .line 915274
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->c:Z

    .line 915275
    return-void

    .line 915276
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;)V
    .locals 1

    .prologue
    .line 915266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915267
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->a:Ljava/lang/String;

    .line 915268
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->b:Ljava/lang/String;

    .line 915269
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->c:Z

    .line 915270
    return-void
.end method

.method public static a(Lcom/facebook/ipc/composer/model/ComposerReshareContext;)Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;
    .locals 2

    .prologue
    .line 915265
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;-><init>(Lcom/facebook/ipc/composer/model/ComposerReshareContext;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;
    .locals 2

    .prologue
    .line 915264
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 915243
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 915253
    if-ne p0, p1, :cond_1

    .line 915254
    :cond_0
    :goto_0
    return v0

    .line 915255
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    if-nez v2, :cond_2

    move v0, v1

    .line 915256
    goto :goto_0

    .line 915257
    :cond_2
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    .line 915258
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 915259
    goto :goto_0

    .line 915260
    :cond_3
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 915261
    goto :goto_0

    .line 915262
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->c:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 915263
    goto :goto_0
.end method

.method public getOriginalShareActorName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "original_share_actor_name"
    .end annotation

    .prologue
    .line 915252
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getReshareMessage()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reshare_message"
    .end annotation

    .prologue
    .line 915251
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 915250
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public shouldIncludeReshareContext()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_include_reshare_context"
    .end annotation

    .prologue
    .line 915249
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->c:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 915244
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915245
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915246
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 915247
    return-void

    .line 915248
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
