.class public final Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Lcom/facebook/ipc/media/data/MediaData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915943
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 915944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915945
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;
    .locals 2

    .prologue
    .line 915946
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;-><init>(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;)V

    return-object v0
.end method

.method public setMediaData(Lcom/facebook/ipc/media/data/MediaData;)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/media/data/MediaData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_data"
    .end annotation

    .prologue
    .line 915947
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;->a:Lcom/facebook/ipc/media/data/MediaData;

    .line 915948
    return-object p0
.end method

.method public setTimestamp(J)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "timestamp"
    .end annotation

    .prologue
    .line 915949
    iput-wide p1, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;->b:J

    .line 915950
    return-object p0
.end method
