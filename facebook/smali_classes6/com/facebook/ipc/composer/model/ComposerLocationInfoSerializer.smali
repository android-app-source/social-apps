.class public Lcom/facebook/ipc/composer/model/ComposerLocationInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerLocationInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 915188
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerLocationInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 915189
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 915187
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915190
    if-nez p0, :cond_0

    .line 915191
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 915192
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 915193
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfoSerializer;->b(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;LX/0nX;LX/0my;)V

    .line 915194
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 915195
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 915177
    const-string v0, "tagged_place"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mTaggedPlace:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 915178
    const-string v0, "place_attachment_removed"

    iget-boolean v1, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mPlaceAttachmentRemoved:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 915179
    const-string v0, "text_only_place"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mTextOnlyPlace:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915180
    const-string v0, "is_checkin"

    iget-boolean v1, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mIsCheckin:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 915181
    const-string v0, "xed_location"

    iget-boolean v1, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mXedLocation:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 915182
    const-string v0, "implicit_loc"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mImplicitLoc:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 915183
    const-string v0, "lightweight_place_picker_places"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerPlaces:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 915184
    const-string v0, "lightweight_place_picker_session_id"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerSessionId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915185
    const-string v0, "lightweight_place_picker_search_results_id"

    iget-object v1, p0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerSearchResultsId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 915186
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915176
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfoSerializer;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;LX/0nX;LX/0my;)V

    return-void
.end method
