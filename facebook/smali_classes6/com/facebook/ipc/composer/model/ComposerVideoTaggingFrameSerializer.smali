.class public Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrameSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 915990
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrameSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrameSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 915991
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 916002
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915996
    if-nez p0, :cond_0

    .line 915997
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 915998
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 915999
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrameSerializer;->b(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;LX/0nX;LX/0my;)V

    .line 916000
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 916001
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 915993
    const-string v0, "media_data"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getMediaData()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 915994
    const-string v0, "timestamp"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getTimestamp()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 915995
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 915992
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrameSerializer;->a(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;LX/0nX;LX/0my;)V

    return-void
.end method
