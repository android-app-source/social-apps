.class public Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParamsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 914949
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParamsSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParamsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 914950
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 914948
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 914951
    if-nez p0, :cond_0

    .line 914952
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 914953
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 914954
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParamsSerializer;->b(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;LX/0nX;LX/0my;)V

    .line 914955
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 914956
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 914944
    const-string v0, "entry_picker"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getEntryPicker()LX/5RI;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 914945
    const-string v0, "entry_point_name"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getEntryPointName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 914946
    const-string v0, "source_surface"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 914947
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 914943
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParamsSerializer;->a(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;LX/0nX;LX/0my;)V

    return-void
.end method
