.class public Lcom/facebook/ipc/composer/model/ComposerTaggedUser;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerTaggedUserDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ComposerTaggedUserSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final mImageUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "image_url"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915900
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUserDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915901
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUserSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 915879
    new-instance v0, LX/2rs;

    invoke-direct {v0}, LX/2rs;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 915898
    new-instance v0, LX/5Rc;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, LX/5Rc;-><init>(J)V

    invoke-direct {p0, v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;-><init>(LX/5Rc;)V

    .line 915899
    return-void
.end method

.method public constructor <init>(LX/5Rc;)V
    .locals 2

    .prologue
    .line 915893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915894
    iget-wide v0, p1, LX/5Rc;->a:J

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mId:J

    .line 915895
    iget-object v0, p1, LX/5Rc;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mName:Ljava/lang/String;

    .line 915896
    iget-object v0, p1, LX/5Rc;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mImageUrl:Ljava/lang/String;

    .line 915897
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 915888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915889
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mId:J

    .line 915890
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mName:Ljava/lang/String;

    .line 915891
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mImageUrl:Ljava/lang/String;

    .line 915892
    return-void
.end method

.method public static a(J)LX/5Rc;
    .locals 2

    .prologue
    .line 915902
    new-instance v0, LX/5Rc;

    invoke-direct {v0, p0, p1}, LX/5Rc;-><init>(J)V

    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 915887
    iget-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mId:J

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 915886
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 915885
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 915884
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 915880
    iget-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 915881
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915882
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->mImageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 915883
    return-void
.end method
