.class public final Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:LX/5RI;

.field private static final b:LX/21D;


# instance fields
.field public c:LX/5RI;

.field public d:Ljava/lang/String;

.field public e:LX/21D;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 914872
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 914891
    new-instance v0, LX/5RK;

    invoke-direct {v0}, LX/5RK;-><init>()V

    .line 914892
    sget-object v0, LX/5RI;->NONE:LX/5RI;

    move-object v0, v0

    .line 914893
    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a:LX/5RI;

    .line 914894
    new-instance v0, LX/5RL;

    invoke-direct {v0}, LX/5RL;-><init>()V

    .line 914895
    sget-object v0, LX/21D;->INVALID:LX/21D;

    move-object v0, v0

    .line 914896
    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->b:LX/21D;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 914897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914898
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a:LX/5RI;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->c:LX/5RI;

    .line 914899
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->d:Ljava/lang/String;

    .line 914900
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->b:LX/21D;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->e:LX/21D;

    .line 914901
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)V
    .locals 1

    .prologue
    .line 914879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914880
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 914881
    instance-of v0, p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    if-eqz v0, :cond_0

    .line 914882
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    .line 914883
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->a:LX/5RI;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->c:LX/5RI;

    .line 914884
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->d:Ljava/lang/String;

    .line 914885
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->c:LX/21D;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->e:LX/21D;

    .line 914886
    :goto_0
    return-void

    .line 914887
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getEntryPicker()LX/5RI;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->c:LX/5RI;

    .line 914888
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getEntryPointName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->d:Ljava/lang/String;

    .line 914889
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->e:LX/21D;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;
    .locals 2

    .prologue
    .line 914890
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;-><init>(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;)V

    return-object v0
.end method

.method public setEntryPicker(LX/5RI;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "entry_picker"
    .end annotation

    .prologue
    .line 914877
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->c:LX/5RI;

    .line 914878
    return-object p0
.end method

.method public setEntryPointName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "entry_point_name"
    .end annotation

    .prologue
    .line 914875
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->d:Ljava/lang/String;

    .line 914876
    return-object p0
.end method

.method public setSourceSurface(LX/21D;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "source_surface"
    .end annotation

    .prologue
    .line 914873
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->e:LX/21D;

    .line 914874
    return-object p0
.end method
