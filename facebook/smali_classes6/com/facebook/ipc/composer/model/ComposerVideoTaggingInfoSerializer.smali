.class public Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 916121
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    new-instance v1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 916122
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 916115
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 916123
    if-nez p0, :cond_0

    .line 916124
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 916125
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 916126
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfoSerializer;->b(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;LX/0nX;LX/0my;)V

    .line 916127
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 916128
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 916116
    const-string v0, "frames"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getFrames()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 916117
    const-string v0, "has_face_detection_finished"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->hasFaceDetectionFinished()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 916118
    const-string v0, "has_faceboxes"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->hasFaceboxes()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 916119
    const-string v0, "time_to_find_first_face_ms"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getTimeToFindFirstFaceMs()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 916120
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 916114
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfoSerializer;->a(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;LX/0nX;LX/0my;)V

    return-void
.end method
