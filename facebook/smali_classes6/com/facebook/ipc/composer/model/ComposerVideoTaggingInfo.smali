.class public Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfoSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Z

.field public final c:Z

.field public final d:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 916113
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 916112
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfoSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 916111
    new-instance v0, LX/5Re;

    invoke-direct {v0}, LX/5Re;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 916098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916099
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    move v1, v2

    .line 916100
    :goto_0
    array-length v0, v4

    if-ge v1, v0, :cond_0

    .line 916101
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    .line 916102
    aput-object v0, v4, v1

    .line 916103
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 916104
    :cond_0
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->a:LX/0Px;

    .line 916105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->b:Z

    .line 916106
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_2

    :goto_2
    iput-boolean v3, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->c:Z

    .line 916107
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->d:J

    .line 916108
    return-void

    :cond_1
    move v0, v2

    .line 916109
    goto :goto_1

    :cond_2
    move v3, v2

    .line 916110
    goto :goto_2
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;)V
    .locals 2

    .prologue
    .line 916092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916093
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->a:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->a:LX/0Px;

    .line 916094
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->b:Z

    .line 916095
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->c:Z

    .line 916096
    iget-wide v0, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->d:J

    .line 916097
    return-void
.end method

.method public static a(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;
    .locals 2

    .prologue
    .line 916061
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;-><init>(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;
    .locals 2

    .prologue
    .line 916091
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 916090
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 916077
    if-ne p0, p1, :cond_1

    .line 916078
    :cond_0
    :goto_0
    return v0

    .line 916079
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    if-nez v2, :cond_2

    move v0, v1

    .line 916080
    goto :goto_0

    .line 916081
    :cond_2
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    .line 916082
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->a:LX/0Px;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->a:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 916083
    goto :goto_0

    .line 916084
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->b:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 916085
    goto :goto_0

    .line 916086
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->c:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 916087
    goto :goto_0

    .line 916088
    :cond_5
    iget-wide v2, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->d:J

    iget-wide v4, p1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 916089
    goto :goto_0
.end method

.method public getFrames()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "frames"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;",
            ">;"
        }
    .end annotation

    .prologue
    .line 916076
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->a:LX/0Px;

    return-object v0
.end method

.method public getTimeToFindFirstFaceMs()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "time_to_find_first_face_ms"
    .end annotation

    .prologue
    .line 916075
    iget-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->d:J

    return-wide v0
.end method

.method public hasFaceDetectionFinished()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_face_detection_finished"
    .end annotation

    .prologue
    .line 916074
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->b:Z

    return v0
.end method

.method public hasFaceboxes()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_faceboxes"
    .end annotation

    .prologue
    .line 916073
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->c:Z

    return v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 916072
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->a:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 916062
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 916063
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->a:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    .line 916064
    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->writeToParcel(Landroid/os/Parcel;I)V

    .line 916065
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 916066
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 916067
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->c:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 916068
    iget-wide v0, p0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 916069
    return-void

    :cond_1
    move v0, v2

    .line 916070
    goto :goto_1

    :cond_2
    move v1, v2

    .line 916071
    goto :goto_2
.end method
