.class public Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModelDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModelSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final checkBoxLabel:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "check_box_label"
    .end annotation
.end field

.field public final interceptAcceptButtonLabel:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "intercept_accept_button_label"
    .end annotation
.end field

.field public final interceptDeclineButtonLabel:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "intercept_decline_button_label"
    .end annotation
.end field

.field public final isCompulsory:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_compulsory"
    .end annotation
.end field

.field public final isEnabled:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_enabled"
    .end annotation
.end field

.field public final isMarketplaceAvailable:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_marketplace_available"
    .end annotation
.end field

.field public final nuxLabel:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "nux_label"
    .end annotation
.end field

.field public final shouldShowIntercept:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shold_show_intercept"
    .end annotation
.end field

.field public final shouldShowNux:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shold_show_nux"
    .end annotation
.end field

.field public final upsellAcceptButtonLabel:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "upsell_accept_button_label"
    .end annotation
.end field

.field public final upsellDeclineButtonLabel:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "upsell_decline_button_label"
    .end annotation
.end field

.field public final upsellSubtitleLabel:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "upsell_subtitle_label"
    .end annotation
.end field

.field public final upsellTitleLabel:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "upsell_title_label"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 916213
    const-class v0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModelDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 916212
    const-class v0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModelSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 916211
    new-instance v0, LX/2ru;

    invoke-direct {v0}, LX/2ru;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 916209
    new-instance v0, LX/5Rf;

    invoke-direct {v0}, LX/5Rf;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;-><init>(LX/5Rf;)V

    .line 916210
    return-void
.end method

.method public constructor <init>(LX/5Rf;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 916189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916190
    iget-boolean v0, p1, LX/5Rf;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isEnabled:Ljava/lang/Integer;

    .line 916191
    iget-boolean v0, p1, LX/5Rf;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isMarketplaceAvailable:Ljava/lang/Integer;

    .line 916192
    iget-boolean v0, p1, LX/5Rf;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->shouldShowIntercept:Ljava/lang/Integer;

    .line 916193
    iget-boolean v0, p1, LX/5Rf;->d:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->shouldShowNux:Ljava/lang/Integer;

    .line 916194
    iget-boolean v0, p1, LX/5Rf;->e:Z

    if-eqz v0, :cond_4

    :goto_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isCompulsory:Ljava/lang/Integer;

    .line 916195
    iget-object v0, p1, LX/5Rf;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->nuxLabel:Ljava/lang/String;

    .line 916196
    iget-object v0, p1, LX/5Rf;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->checkBoxLabel:Ljava/lang/String;

    .line 916197
    iget-object v0, p1, LX/5Rf;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellTitleLabel:Ljava/lang/String;

    .line 916198
    iget-object v0, p1, LX/5Rf;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellSubtitleLabel:Ljava/lang/String;

    .line 916199
    iget-object v0, p1, LX/5Rf;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->interceptAcceptButtonLabel:Ljava/lang/String;

    .line 916200
    iget-object v0, p1, LX/5Rf;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->interceptDeclineButtonLabel:Ljava/lang/String;

    .line 916201
    iget-object v0, p1, LX/5Rf;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellAcceptButtonLabel:Ljava/lang/String;

    .line 916202
    iget-object v0, p1, LX/5Rf;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellDeclineButtonLabel:Ljava/lang/String;

    .line 916203
    return-void

    :cond_0
    move v0, v2

    .line 916204
    goto :goto_0

    :cond_1
    move v0, v2

    .line 916205
    goto :goto_1

    :cond_2
    move v0, v2

    .line 916206
    goto :goto_2

    :cond_3
    move v0, v2

    .line 916207
    goto :goto_3

    :cond_4
    move v1, v2

    .line 916208
    goto :goto_4
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 916174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916175
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isEnabled:Ljava/lang/Integer;

    .line 916176
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isMarketplaceAvailable:Ljava/lang/Integer;

    .line 916177
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->shouldShowIntercept:Ljava/lang/Integer;

    .line 916178
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->shouldShowNux:Ljava/lang/Integer;

    .line 916179
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isCompulsory:Ljava/lang/Integer;

    .line 916180
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->nuxLabel:Ljava/lang/String;

    .line 916181
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->checkBoxLabel:Ljava/lang/String;

    .line 916182
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellTitleLabel:Ljava/lang/String;

    .line 916183
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellSubtitleLabel:Ljava/lang/String;

    .line 916184
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->interceptAcceptButtonLabel:Ljava/lang/String;

    .line 916185
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->interceptDeclineButtonLabel:Ljava/lang/String;

    .line 916186
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellAcceptButtonLabel:Ljava/lang/String;

    .line 916187
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellDeclineButtonLabel:Ljava/lang/String;

    .line 916188
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 916173
    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isEnabled:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 916172
    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isMarketplaceAvailable:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 916171
    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->shouldShowIntercept:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 916155
    iget-object v1, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isCompulsory:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 916170
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 916156
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isEnabled:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 916157
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isMarketplaceAvailable:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 916158
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->shouldShowIntercept:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 916159
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->shouldShowNux:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 916160
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->isCompulsory:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 916161
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->nuxLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916162
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->checkBoxLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916163
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellTitleLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916164
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellSubtitleLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916165
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->interceptAcceptButtonLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916166
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->interceptDeclineButtonLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916167
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellAcceptButtonLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916168
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellDeclineButtonLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 916169
    return-void
.end method
