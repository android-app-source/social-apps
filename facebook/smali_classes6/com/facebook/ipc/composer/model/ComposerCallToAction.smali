.class public Lcom/facebook/ipc/composer/model/ComposerCallToAction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerCallToAction$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ComposerCallToActionSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerCallToAction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 914560
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 914559
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerCallToActionSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 914558
    new-instance v0, LX/5RC;

    invoke-direct {v0}, LX/5RC;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 914549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914550
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->a:Ljava/lang/String;

    .line 914551
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->b:Ljava/lang/String;

    .line 914552
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 914553
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->c:Ljava/lang/String;

    .line 914554
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->d:Ljava/lang/String;

    .line 914555
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->e:Ljava/lang/String;

    .line 914556
    return-void

    .line 914557
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;)V
    .locals 1

    .prologue
    .line 914542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914543
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->a:Ljava/lang/String;

    .line 914544
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->b:Ljava/lang/String;

    .line 914545
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->c:Ljava/lang/String;

    .line 914546
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->d:Ljava/lang/String;

    .line 914547
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->e:Ljava/lang/String;

    .line 914548
    return-void
.end method

.method public static newBuilder()Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;
    .locals 2

    .prologue
    .line 914541
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 914540
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 914510
    if-ne p0, p1, :cond_1

    .line 914511
    :cond_0
    :goto_0
    return v0

    .line 914512
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    if-nez v2, :cond_2

    move v0, v1

    .line 914513
    goto :goto_0

    .line 914514
    :cond_2
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    .line 914515
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 914516
    goto :goto_0

    .line 914517
    :cond_3
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 914518
    goto :goto_0

    .line 914519
    :cond_4
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 914520
    goto :goto_0

    .line 914521
    :cond_5
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 914522
    goto :goto_0

    .line 914523
    :cond_6
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 914524
    goto :goto_0
.end method

.method public getCallToActionType()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "call_to_action_type"
    .end annotation

    .prologue
    .line 914539
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "label"
    .end annotation

    .prologue
    .line 914538
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getLink()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "link"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 914537
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkImage()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "link_image"
    .end annotation

    .prologue
    .line 914536
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation

    .prologue
    .line 914535
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 914534
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 914525
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 914526
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 914527
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 914528
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 914529
    :goto_0
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 914530
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 914531
    return-void

    .line 914532
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 914533
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
