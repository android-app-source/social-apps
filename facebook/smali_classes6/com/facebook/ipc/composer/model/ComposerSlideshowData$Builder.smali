.class public final Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerSlideshowData_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915589
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 915587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915588
    return-void
.end method

.method public constructor <init>(LX/5Ra;)V
    .locals 1

    .prologue
    .line 915580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915581
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 915582
    instance-of v0, p1, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    if-eqz v0, :cond_0

    .line 915583
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 915584
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;->a:Ljava/lang/String;

    .line 915585
    :goto_0
    return-void

    .line 915586
    :cond_0
    invoke-interface {p1}, LX/5Ra;->getMoodId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;->a:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .locals 2

    .prologue
    .line 915577
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;-><init>(Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;)V

    return-object v0
.end method

.method public setMoodId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mood_id"
    .end annotation

    .prologue
    .line 915578
    iput-object p1, p0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;->a:Ljava/lang/String;

    .line 915579
    return-object p0
.end method
