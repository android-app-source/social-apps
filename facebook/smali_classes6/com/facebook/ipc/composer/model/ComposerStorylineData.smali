.class public Lcom/facebook/ipc/composer/model/ComposerStorylineData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ComposerStorylineData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/model/ComposerStorylineDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerStorylineData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915841
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerStorylineData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 915840
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerStorylineDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 915839
    new-instance v0, LX/2rr;

    invoke-direct {v0}, LX/2rr;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 915834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915835
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 915836
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->a:Ljava/lang/String;

    .line 915837
    :goto_0
    return-void

    .line 915838
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;)V
    .locals 1

    .prologue
    .line 915831
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915832
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->a:Ljava/lang/String;

    .line 915833
    return-void
.end method

.method public static newBuilder()Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;
    .locals 2

    .prologue
    .line 915815
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/model/ComposerStorylineData$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 915830
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 915823
    if-ne p0, p1, :cond_1

    .line 915824
    :cond_0
    :goto_0
    return v0

    .line 915825
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    if-nez v2, :cond_2

    move v0, v1

    .line 915826
    goto :goto_0

    .line 915827
    :cond_2
    check-cast p1, Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    .line 915828
    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 915829
    goto :goto_0
.end method

.method public getMoodId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mood_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 915822
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 915821
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 915816
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 915817
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 915818
    :goto_0
    return-void

    .line 915819
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 915820
    iget-object v0, p0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
