.class public final Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 916518
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 916516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916517
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;
    .locals 2

    .prologue
    .line 916515
    new-instance v0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;-><init>(Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;)V

    return-object v0
.end method

.method public setIsCompulsory(Z)Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_compulsory"
    .end annotation

    .prologue
    .line 916509
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;->a:Z

    .line 916510
    return-object p0
.end method

.method public setUseNeighborhoodDataSource(Z)Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "use_neighborhood_data_source"
    .end annotation

    .prologue
    .line 916513
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;->b:Z

    .line 916514
    return-object p0
.end method

.method public setUseZipCode(Z)Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "use_zip_code"
    .end annotation

    .prologue
    .line 916511
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;->c:Z

    .line 916512
    return-object p0
.end method
