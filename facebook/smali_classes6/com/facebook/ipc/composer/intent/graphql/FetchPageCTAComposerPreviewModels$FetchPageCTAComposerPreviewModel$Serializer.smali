.class public final Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 914295
    const-class v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;

    new-instance v1, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 914296
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 914297
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 914298
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 914299
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 914300
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 914301
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 914302
    if-eqz v2, :cond_1

    .line 914303
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914304
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 914305
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 914306
    if-eqz v3, :cond_0

    .line 914307
    const-string v4, "photo"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914308
    invoke-static {v1, v3, p1, p2}, LX/5R8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 914309
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 914310
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 914311
    if-eqz v2, :cond_2

    .line 914312
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914313
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 914314
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 914315
    if-eqz v2, :cond_8

    .line 914316
    const-string v3, "page_call_to_action"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914317
    const/4 v4, 0x0

    .line 914318
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 914319
    invoke-virtual {v1, v2, v4}, LX/15i;->g(II)I

    move-result v3

    .line 914320
    if-eqz v3, :cond_3

    .line 914321
    const-string v3, "ads_cta_type"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914322
    invoke-virtual {v1, v2, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 914323
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 914324
    if-eqz v3, :cond_4

    .line 914325
    const-string v4, "desktop_uri"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914326
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 914327
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 914328
    if-eqz v3, :cond_6

    .line 914329
    const-string v4, "icon_info"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914330
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 914331
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 914332
    if-eqz v4, :cond_5

    .line 914333
    const-string p0, "icon_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914334
    invoke-static {v1, v4, p1}, LX/5R9;->a(LX/15i;ILX/0nX;)V

    .line 914335
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 914336
    :cond_6
    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 914337
    if-eqz v3, :cond_7

    .line 914338
    const-string v4, "label"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914339
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 914340
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 914341
    :cond_8
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 914342
    if-eqz v2, :cond_b

    .line 914343
    const-string v3, "profile_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914344
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 914345
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 914346
    if-eqz v3, :cond_a

    .line 914347
    const-string v4, "image"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914348
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 914349
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 914350
    if-eqz v4, :cond_9

    .line 914351
    const-string v2, "uri"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 914352
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 914353
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 914354
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 914355
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 914356
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 914357
    check-cast p1, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$Serializer;->a(Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;LX/0nX;LX/0my;)V

    return-void
.end method
