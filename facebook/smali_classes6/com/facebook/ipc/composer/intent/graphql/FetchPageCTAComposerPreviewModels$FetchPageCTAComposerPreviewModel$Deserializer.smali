.class public final Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 914139
    const-class v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;

    new-instance v1, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 914140
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 914141
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 914142
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 914143
    const/4 v2, 0x0

    .line 914144
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 914145
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 914146
    :goto_0
    move v1, v2

    .line 914147
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 914148
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 914149
    new-instance v1, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;-><init>()V

    .line 914150
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 914151
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 914152
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 914153
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 914154
    :cond_0
    return-object v1

    .line 914155
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 914156
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 914157
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 914158
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 914159
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_2

    if-eqz v6, :cond_2

    .line 914160
    const-string v7, "cover_photo"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 914161
    const/4 v6, 0x0

    .line 914162
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_b

    .line 914163
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 914164
    :goto_2
    move v5, v6

    .line 914165
    goto :goto_1

    .line 914166
    :cond_3
    const-string v7, "name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 914167
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 914168
    :cond_4
    const-string v7, "page_call_to_action"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 914169
    const/4 v6, 0x0

    .line 914170
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v7, :cond_12

    .line 914171
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 914172
    :goto_3
    move v3, v6

    .line 914173
    goto :goto_1

    .line 914174
    :cond_5
    const-string v7, "profile_photo"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 914175
    const/4 v6, 0x0

    .line 914176
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v7, :cond_1a

    .line 914177
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 914178
    :goto_4
    move v1, v6

    .line 914179
    goto :goto_1

    .line 914180
    :cond_6
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 914181
    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 914182
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 914183
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 914184
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 914185
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    goto/16 :goto_1

    .line 914186
    :cond_8
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 914187
    :cond_9
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 914188
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 914189
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 914190
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_9

    if-eqz v7, :cond_9

    .line 914191
    const-string v8, "photo"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 914192
    invoke-static {p1, v0}, LX/5R8;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_5

    .line 914193
    :cond_a
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 914194
    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 914195
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto/16 :goto_2

    :cond_b
    move v5, v6

    goto :goto_5

    .line 914196
    :cond_c
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 914197
    :cond_d
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_11

    .line 914198
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 914199
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 914200
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, p0, :cond_d

    if-eqz v10, :cond_d

    .line 914201
    const-string v11, "ads_cta_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 914202
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_6

    .line 914203
    :cond_e
    const-string v11, "desktop_uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 914204
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_6

    .line 914205
    :cond_f
    const-string v11, "icon_info"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 914206
    const/4 v10, 0x0

    .line 914207
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v11, :cond_16

    .line 914208
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 914209
    :goto_7
    move v7, v10

    .line 914210
    goto :goto_6

    .line 914211
    :cond_10
    const-string v11, "label"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 914212
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_6

    .line 914213
    :cond_11
    const/4 v10, 0x4

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 914214
    invoke-virtual {v0, v6, v9}, LX/186;->b(II)V

    .line 914215
    const/4 v6, 0x1

    invoke-virtual {v0, v6, v8}, LX/186;->b(II)V

    .line 914216
    const/4 v6, 0x2

    invoke-virtual {v0, v6, v7}, LX/186;->b(II)V

    .line 914217
    const/4 v6, 0x3

    invoke-virtual {v0, v6, v3}, LX/186;->b(II)V

    .line 914218
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_12
    move v3, v6

    move v7, v6

    move v8, v6

    move v9, v6

    goto/16 :goto_6

    .line 914219
    :cond_13
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 914220
    :cond_14
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_15

    .line 914221
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 914222
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 914223
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_14

    if-eqz v11, :cond_14

    .line 914224
    const-string p0, "icon_image"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_13

    .line 914225
    invoke-static {p1, v0}, LX/5R9;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_8

    .line 914226
    :cond_15
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 914227
    invoke-virtual {v0, v10, v7}, LX/186;->b(II)V

    .line 914228
    invoke-virtual {v0}, LX/186;->d()I

    move-result v10

    goto :goto_7

    :cond_16
    move v7, v10

    goto :goto_8

    .line 914229
    :cond_17
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 914230
    :cond_18
    :goto_9
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_19

    .line 914231
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 914232
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 914233
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_18

    if-eqz v7, :cond_18

    .line 914234
    const-string v8, "image"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_17

    .line 914235
    const/4 v7, 0x0

    .line 914236
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v8, :cond_1e

    .line 914237
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 914238
    :goto_a
    move v1, v7

    .line 914239
    goto :goto_9

    .line 914240
    :cond_19
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 914241
    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 914242
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto/16 :goto_4

    :cond_1a
    move v1, v6

    goto :goto_9

    .line 914243
    :cond_1b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 914244
    :cond_1c
    :goto_b
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_1d

    .line 914245
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 914246
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 914247
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1c

    if-eqz v8, :cond_1c

    .line 914248
    const-string v9, "uri"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1b

    .line 914249
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_b

    .line 914250
    :cond_1d
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 914251
    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 914252
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto :goto_a

    :cond_1e
    move v1, v7

    goto :goto_b
.end method
