.class public final Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5c4f9b4c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 914415
    const-class v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 914358
    const-class v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 914413
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 914414
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 914401
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 914402
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x21d4ecde

    invoke-static {v1, v0, v2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 914403
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 914404
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x29d03c71

    invoke-static {v3, v2, v4}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 914405
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x1acb3bcb

    invoke-static {v4, v3, v5}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 914406
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 914407
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 914408
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 914409
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 914410
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 914411
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 914412
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 914379
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 914380
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 914381
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x21d4ecde

    invoke-static {v2, v0, v3}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 914382
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 914383
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;

    .line 914384
    iput v3, v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->e:I

    move-object v1, v0

    .line 914385
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 914386
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x29d03c71

    invoke-static {v2, v0, v3}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 914387
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 914388
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;

    .line 914389
    iput v3, v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->g:I

    move-object v1, v0

    .line 914390
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 914391
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x1acb3bcb

    invoke-static {v2, v0, v3}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 914392
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 914393
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;

    .line 914394
    iput v3, v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->h:I

    move-object v1, v0

    .line 914395
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 914396
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 914397
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 914398
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 914399
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_3
    move-object p0, v1

    .line 914400
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 914377
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 914378
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 914376
    new-instance v0, LX/5R6;

    invoke-direct {v0, p1}, LX/5R6;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 914371
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 914372
    const/4 v0, 0x0

    const v1, -0x21d4ecde

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->e:I

    .line 914373
    const/4 v0, 0x2

    const v1, -0x29d03c71

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->g:I

    .line 914374
    const/4 v0, 0x3

    const v1, 0x1acb3bcb

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->h:I

    .line 914375
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 914416
    invoke-virtual {p2}, LX/18L;->a()V

    .line 914417
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 914370
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 914367
    new-instance v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;-><init>()V

    .line 914368
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 914369
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 914366
    const v0, -0x6ba2bf0e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 914365
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 914363
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->f:Ljava/lang/String;

    .line 914364
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageCallToAction"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 914361
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 914362
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 914359
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 914360
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
