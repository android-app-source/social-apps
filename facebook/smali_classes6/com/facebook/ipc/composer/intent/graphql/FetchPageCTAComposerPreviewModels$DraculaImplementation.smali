.class public final Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 914082
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 914083
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 913988
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 913989
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 914039
    if-nez p1, :cond_0

    move v0, v1

    .line 914040
    :goto_0
    return v0

    .line 914041
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 914042
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 914043
    :sswitch_0
    const-class v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$CoverPhotoModel$PhotoModel;

    .line 914044
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 914045
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 914046
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 914047
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 914048
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 914049
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 914050
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 914051
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 914052
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 914053
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    .line 914054
    invoke-virtual {p3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 914055
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 914056
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 914057
    invoke-virtual {p0, p1, v7}, LX/15i;->p(II)I

    move-result v3

    .line 914058
    const v4, 0x64cf6222

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 914059
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 914060
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 914061
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 914062
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 914063
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 914064
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 914065
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 914066
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 914067
    :sswitch_3
    const-class v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$PageCallToActionModel$IconInfoModel$IconImageModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$PageCallToActionModel$IconInfoModel$IconImageModel;

    .line 914068
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 914069
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 914070
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 914071
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 914072
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 914073
    const v2, 0x3077765b

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 914074
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 914075
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 914076
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 914077
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 914078
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 914079
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 914080
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 914081
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x29d03c71 -> :sswitch_2
        -0x21d4ecde -> :sswitch_0
        0x1acb3bcb -> :sswitch_4
        0x3077765b -> :sswitch_5
        0x64cf6222 -> :sswitch_3
        0x6c747c27 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 914038
    new-instance v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 914033
    if-eqz p0, :cond_0

    .line 914034
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 914035
    if-eq v0, p0, :cond_0

    .line 914036
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 914037
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 914022
    sparse-switch p2, :sswitch_data_0

    .line 914023
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 914024
    :sswitch_0
    const-class v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$CoverPhotoModel$PhotoModel;

    .line 914025
    invoke-static {v0, p3}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 914026
    :goto_0
    :sswitch_1
    return-void

    .line 914027
    :sswitch_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 914028
    const v1, 0x64cf6222

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 914029
    :sswitch_3
    const-class v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$PageCallToActionModel$IconInfoModel$IconImageModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$PageCallToActionModel$IconInfoModel$IconImageModel;

    .line 914030
    invoke-static {v0, p3}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 914031
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 914032
    const v1, 0x3077765b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x29d03c71 -> :sswitch_2
        -0x21d4ecde -> :sswitch_0
        0x1acb3bcb -> :sswitch_4
        0x3077765b -> :sswitch_1
        0x64cf6222 -> :sswitch_3
        0x6c747c27 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 914016
    if-eqz p1, :cond_0

    .line 914017
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;

    move-result-object v1

    .line 914018
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;

    .line 914019
    if-eq v0, v1, :cond_0

    .line 914020
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 914021
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 914015
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 914013
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 914014
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 914084
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 914085
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 914086
    :cond_0
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a:LX/15i;

    .line 914087
    iput p2, p0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->b:I

    .line 914088
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 914012
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 914011
    new-instance v0, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 914008
    iget v0, p0, LX/1vt;->c:I

    .line 914009
    move v0, v0

    .line 914010
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 914005
    iget v0, p0, LX/1vt;->c:I

    .line 914006
    move v0, v0

    .line 914007
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 914002
    iget v0, p0, LX/1vt;->b:I

    .line 914003
    move v0, v0

    .line 914004
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 913999
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 914000
    move-object v0, v0

    .line 914001
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 913990
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 913991
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 913992
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 913993
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 913994
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 913995
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 913996
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 913997
    invoke-static {v3, v9, v2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 913998
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 913985
    iget v0, p0, LX/1vt;->c:I

    .line 913986
    move v0, v0

    .line 913987
    return v0
.end method
