.class public Lcom/facebook/ipc/model/PageTopic;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/model/PageTopicDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/model/PageTopic;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final displayName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final id:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final pageCount:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "count"
    .end annotation
.end field

.field public final parentIds:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "parent_ids"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final pic:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "pic_square"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 917548
    const-class v0, Lcom/facebook/ipc/model/PageTopicDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 917547
    new-instance v0, LX/5S8;

    invoke-direct {v0}, LX/5S8;-><init>()V

    sput-object v0, Lcom/facebook/ipc/model/PageTopic;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 917540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917541
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/ipc/model/PageTopic;->id:J

    .line 917542
    iput-object v2, p0, Lcom/facebook/ipc/model/PageTopic;->displayName:Ljava/lang/String;

    .line 917543
    iput-object v2, p0, Lcom/facebook/ipc/model/PageTopic;->pic:Ljava/lang/String;

    .line 917544
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/PageTopic;->parentIds:Ljava/util/List;

    .line 917545
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/ipc/model/PageTopic;->pageCount:I

    .line 917546
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 917515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917516
    iput-wide p1, p0, Lcom/facebook/ipc/model/PageTopic;->id:J

    .line 917517
    iput-object p3, p0, Lcom/facebook/ipc/model/PageTopic;->displayName:Ljava/lang/String;

    .line 917518
    iput-object p4, p0, Lcom/facebook/ipc/model/PageTopic;->pic:Ljava/lang/String;

    .line 917519
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/facebook/ipc/model/PageTopic;->parentIds:Ljava/util/List;

    .line 917520
    iput p6, p0, Lcom/facebook/ipc/model/PageTopic;->pageCount:I

    .line 917521
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 917532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917533
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/model/PageTopic;->id:J

    .line 917534
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/PageTopic;->displayName:Ljava/lang/String;

    .line 917535
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/PageTopic;->pic:Ljava/lang/String;

    .line 917536
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/ipc/model/PageTopic;->parentIds:Ljava/util/List;

    .line 917537
    iget-object v0, p0, Lcom/facebook/ipc/model/PageTopic;->parentIds:Ljava/util/List;

    const-class v1, Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 917538
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/model/PageTopic;->pageCount:I

    .line 917539
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 917531
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 917529
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/ipc/model/PageTopic;

    if-nez v1, :cond_1

    .line 917530
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p0, Lcom/facebook/ipc/model/PageTopic;->id:J

    check-cast p1, Lcom/facebook/ipc/model/PageTopic;

    iget-wide v4, p1, Lcom/facebook/ipc/model/PageTopic;->id:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 917528
    iget-wide v0, p0, Lcom/facebook/ipc/model/PageTopic;->id:J

    long-to-int v0, v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 917522
    iget-wide v0, p0, Lcom/facebook/ipc/model/PageTopic;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 917523
    iget-object v0, p0, Lcom/facebook/ipc/model/PageTopic;->displayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917524
    iget-object v0, p0, Lcom/facebook/ipc/model/PageTopic;->pic:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917525
    iget-object v0, p0, Lcom/facebook/ipc/model/PageTopic;->parentIds:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 917526
    iget v0, p0, Lcom/facebook/ipc/model/PageTopic;->pageCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917527
    return-void
.end method
