.class public Lcom/facebook/ipc/model/FacebookEvent;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/model/FacebookEventDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/model/FacebookEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public mEndTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "end_time"
    .end annotation
.end field

.field public mEventId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "eid"
    .end annotation
.end field

.field public mName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public mPicSquare:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "pic_square"
    .end annotation
.end field

.field public mStartTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "start_time"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 917119
    const-class v0, Lcom/facebook/ipc/model/FacebookEventDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 917117
    const-class v0, Lcom/facebook/ipc/model/FacebookEvent;

    sput-object v0, Lcom/facebook/ipc/model/FacebookEvent;->a:Ljava/lang/Class;

    .line 917118
    new-instance v0, LX/5S3;

    invoke-direct {v0}, LX/5S3;-><init>()V

    sput-object v0, Lcom/facebook/ipc/model/FacebookEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 917112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917113
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookEvent;->mEventId:J

    .line 917114
    iput-object v2, p0, Lcom/facebook/ipc/model/FacebookEvent;->mName:Ljava/lang/String;

    .line 917115
    iput-object v2, p0, Lcom/facebook/ipc/model/FacebookEvent;->mPicSquare:Ljava/lang/String;

    .line 917116
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 917105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917106
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookEvent;->mEventId:J

    .line 917107
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookEvent;->mName:Ljava/lang/String;

    .line 917108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookEvent;->mPicSquare:Ljava/lang/String;

    .line 917109
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookEvent;->mStartTime:J

    .line 917110
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookEvent;->mEndTime:J

    .line 917111
    return-void
.end method

.method private a(Lcom/facebook/ipc/model/FacebookEvent;)Z
    .locals 4

    .prologue
    .line 917120
    iget-wide v0, p0, Lcom/facebook/ipc/model/FacebookEvent;->mEventId:J

    iget-wide v2, p1, Lcom/facebook/ipc/model/FacebookEvent;->mEventId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 917104
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 917103
    instance-of v0, p1, Lcom/facebook/ipc/model/FacebookEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/facebook/ipc/model/FacebookEvent;

    invoke-direct {p0, p1}, Lcom/facebook/ipc/model/FacebookEvent;->a(Lcom/facebook/ipc/model/FacebookEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 917102
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookEvent;->mEventId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 917095
    const-class v0, Lcom/facebook/ipc/model/FacebookEvent;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "id"

    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookEvent;->mEventId:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "name"

    iget-object v2, p0, Lcom/facebook/ipc/model/FacebookEvent;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "pic"

    iget-object v2, p0, Lcom/facebook/ipc/model/FacebookEvent;->mPicSquare:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "start"

    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookEvent;->mStartTime:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "end"

    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookEvent;->mEndTime:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 917096
    iget-wide v0, p0, Lcom/facebook/ipc/model/FacebookEvent;->mEventId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 917097
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookEvent;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917098
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookEvent;->mPicSquare:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917099
    iget-wide v0, p0, Lcom/facebook/ipc/model/FacebookEvent;->mStartTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 917100
    iget-wide v0, p0, Lcom/facebook/ipc/model/FacebookEvent;->mEndTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 917101
    return-void
.end method
