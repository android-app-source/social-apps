.class public Lcom/facebook/ipc/model/FacebookProfileSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/model/FacebookProfile;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 917385
    const-class v0, Lcom/facebook/ipc/model/FacebookProfile;

    new-instance v1, Lcom/facebook/ipc/model/FacebookProfileSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/model/FacebookProfileSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 917386
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 917384
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/model/FacebookProfile;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 917378
    if-nez p0, :cond_0

    .line 917379
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 917380
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 917381
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/model/FacebookProfileSerializer;->b(Lcom/facebook/ipc/model/FacebookProfile;LX/0nX;LX/0my;)V

    .line 917382
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 917383
    return-void
.end method

.method private static b(Lcom/facebook/ipc/model/FacebookProfile;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 917373
    const-string v0, "id"

    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 917374
    const-string v0, "name"

    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 917375
    const-string v0, "pic_square"

    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 917376
    const-string v0, "type"

    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookProfile;->mTypeString:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 917377
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 917372
    check-cast p1, Lcom/facebook/ipc/model/FacebookProfile;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/model/FacebookProfileSerializer;->a(Lcom/facebook/ipc/model/FacebookProfile;LX/0nX;LX/0my;)V

    return-void
.end method
