.class public Lcom/facebook/ipc/model/FacebookPhonebookContactMap;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/model/FacebookPhonebookContactMap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 917297
    new-instance v0, LX/5S5;

    invoke-direct {v0}, LX/5S5;-><init>()V

    sput-object v0, Lcom/facebook/ipc/model/FacebookPhonebookContactMap;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 917294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917295
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContactMap;->a:Ljava/util/Map;

    .line 917296
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 917291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917292
    const-class v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContactMap;->a:Ljava/util/Map;

    .line 917293
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 917285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917286
    iput-object p1, p0, Lcom/facebook/ipc/model/FacebookPhonebookContactMap;->a:Ljava/util/Map;

    .line 917287
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 917290
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 917288
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContactMap;->a:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 917289
    return-void
.end method
