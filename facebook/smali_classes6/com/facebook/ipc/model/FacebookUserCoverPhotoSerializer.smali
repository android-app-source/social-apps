.class public Lcom/facebook/ipc/model/FacebookUserCoverPhotoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/model/FacebookUserCoverPhoto;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 917411
    const-class v0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;

    new-instance v1, Lcom/facebook/ipc/model/FacebookUserCoverPhotoSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/model/FacebookUserCoverPhotoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 917412
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 917413
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/model/FacebookUserCoverPhoto;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 917414
    if-nez p0, :cond_0

    .line 917415
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 917416
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 917417
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/model/FacebookUserCoverPhotoSerializer;->b(Lcom/facebook/ipc/model/FacebookUserCoverPhoto;LX/0nX;LX/0my;)V

    .line 917418
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 917419
    return-void
.end method

.method private static b(Lcom/facebook/ipc/model/FacebookUserCoverPhoto;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 917420
    const-string v0, "cover_id"

    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->coverID:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 917421
    const-string v0, "source"

    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->source:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 917422
    const-string v0, "offset_x"

    iget v1, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->offsetX:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 917423
    const-string v0, "offset_y"

    iget v1, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->offsetY:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 917424
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 917425
    check-cast p1, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/model/FacebookUserCoverPhotoSerializer;->a(Lcom/facebook/ipc/model/FacebookUserCoverPhoto;LX/0nX;LX/0my;)V

    return-void
.end method
