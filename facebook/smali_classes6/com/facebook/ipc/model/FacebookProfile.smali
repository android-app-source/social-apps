.class public Lcom/facebook/ipc/model/FacebookProfile;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/0Pm;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/model/FacebookProfileDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/model/FacebookProfileSerializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/ipc/model/FacebookProfile;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field public final mDisplayName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final mId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final mImageUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "pic_square"
    .end annotation
.end field

.field public mTypeString:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 917341
    const-class v0, Lcom/facebook/ipc/model/FacebookProfileDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 917340
    const-class v0, Lcom/facebook/ipc/model/FacebookProfileSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 917339
    new-instance v0, LX/5S6;

    invoke-direct {v0}, LX/5S6;-><init>()V

    sput-object v0, Lcom/facebook/ipc/model/FacebookProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 917333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917334
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    .line 917335
    iput-object v2, p0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 917336
    iput-object v2, p0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 917337
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->a:I

    .line 917338
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 917327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917328
    iput-wide p1, p0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    .line 917329
    iput-object p3, p0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 917330
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 917331
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->a:I

    .line 917332
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 917321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917322
    iput-wide p1, p0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    .line 917323
    iput-object p3, p0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 917324
    iput-object p4, p0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 917325
    iput p5, p0, Lcom/facebook/ipc/model/FacebookProfile;->a:I

    .line 917326
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 917342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917343
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    .line 917344
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 917345
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 917346
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->a:I

    .line 917347
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 917312
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mTypeString:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 917313
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mTypeString:Ljava/lang/String;

    const-string v1, "page"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 917314
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->a:I

    .line 917315
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mTypeString:Ljava/lang/String;

    .line 917316
    return-object p0

    .line 917317
    :cond_1
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mTypeString:Ljava/lang/String;

    const-string v1, "group"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 917318
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->a:I

    goto :goto_0

    .line 917319
    :cond_2
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mTypeString:Ljava/lang/String;

    const-string v1, "event"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 917320
    const/4 v0, 0x4

    iput v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->a:I

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 917311
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 917310
    instance-of v0, p1, Lcom/facebook/ipc/model/FacebookProfile;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    check-cast p1, Lcom/facebook/ipc/model/FacebookProfile;

    iget-wide v2, p1, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 917309
    iget-wide v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    long-to-int v0, v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 917306
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 917307
    const-string v1, "FacebookProfile("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "))"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 917308
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 917301
    iget-wide v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 917302
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917303
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917304
    iget v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917305
    return-void
.end method
