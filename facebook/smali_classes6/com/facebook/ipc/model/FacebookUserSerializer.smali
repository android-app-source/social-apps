.class public Lcom/facebook/ipc/model/FacebookUserSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/model/FacebookUser;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 917461
    const-class v0, Lcom/facebook/ipc/model/FacebookUser;

    new-instance v1, Lcom/facebook/ipc/model/FacebookUserSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/model/FacebookUserSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 917462
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 917460
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/model/FacebookUser;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 917463
    if-nez p0, :cond_0

    .line 917464
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 917465
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 917466
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/model/FacebookUserSerializer;->b(Lcom/facebook/ipc/model/FacebookUser;LX/0nX;LX/0my;)V

    .line 917467
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 917468
    return-void
.end method

.method private static b(Lcom/facebook/ipc/model/FacebookUser;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 917453
    const-string v0, "uid"

    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookUser;->mUserId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 917454
    const-string v0, "first_name"

    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookUser;->mFirstName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 917455
    const-string v0, "last_name"

    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookUser;->mLastName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 917456
    const-string v0, "name"

    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookUser;->mDisplayName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 917457
    const-string v0, "pic_square"

    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookUser;->mImageUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 917458
    const-string v0, "pic_cover"

    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookUser;->mCoverPhoto:Lcom/facebook/ipc/model/FacebookUserCoverPhoto;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 917459
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 917452
    check-cast p1, Lcom/facebook/ipc/model/FacebookUser;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/model/FacebookUserSerializer;->a(Lcom/facebook/ipc/model/FacebookUser;LX/0nX;LX/0my;)V

    return-void
.end method
