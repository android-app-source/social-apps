.class public Lcom/facebook/ipc/model/FacebookPhonebookContact;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/model/FacebookPhonebookContactDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final email:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "email"
    .end annotation
.end field

.field public final isFriend:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_friend"
    .end annotation
.end field

.field public name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final nativeName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "native_name"
    .end annotation
.end field

.field public final ordinal:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "ordinal"
    .end annotation
.end field

.field public final phone:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cell"
    .end annotation
.end field

.field public final recordId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "record_id"
    .end annotation
.end field

.field public final userId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uid"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 917239
    const-class v0, Lcom/facebook/ipc/model/FacebookPhonebookContactDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 917252
    const-class v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    sput-object v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->c:Ljava/lang/Class;

    .line 917253
    new-instance v0, LX/5S4;

    invoke-direct {v0}, LX/5S4;-><init>()V

    sput-object v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    .line 917240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917241
    iput-object v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    .line 917242
    iput-wide v4, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->recordId:J

    .line 917243
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->isFriend:Z

    .line 917244
    iput-wide v4, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->userId:J

    .line 917245
    iput-object v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->phone:Ljava/lang/String;

    .line 917246
    iput-object v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    .line 917247
    iput-object v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->email:Ljava/lang/String;

    .line 917248
    iput-object v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    .line 917249
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->ordinal:J

    .line 917250
    iput-object v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->nativeName:Ljava/lang/String;

    .line 917251
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 917210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917211
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    .line 917212
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->recordId:J

    .line 917213
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->isFriend:Z

    .line 917214
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->userId:J

    .line 917215
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->phone:Ljava/lang/String;

    .line 917216
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    .line 917217
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->email:Ljava/lang/String;

    .line 917218
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    .line 917219
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->ordinal:J

    .line 917220
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->nativeName:Ljava/lang/String;

    .line 917221
    return-void

    .line 917222
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 917223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917224
    iput-object p1, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    .line 917225
    iput-wide p2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->recordId:J

    .line 917226
    iput-object p4, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    .line 917227
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 917228
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->email:Ljava/lang/String;

    .line 917229
    :goto_0
    iput-object p5, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    .line 917230
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 917231
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->phone:Ljava/lang/String;

    .line 917232
    :goto_1
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->userId:J

    .line 917233
    iput-boolean v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->isFriend:Z

    .line 917234
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->ordinal:J

    .line 917235
    iput-object v3, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->nativeName:Ljava/lang/String;

    .line 917236
    return-void

    .line 917237
    :cond_0
    iput-object v3, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->email:Ljava/lang/String;

    goto :goto_0

    .line 917238
    :cond_1
    iput-object v3, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->phone:Ljava/lang/String;

    goto :goto_1
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 917184
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 917185
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    .line 917186
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 917187
    iget-object v1, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 917188
    const-string v1, "name"

    iget-object v5, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    invoke-virtual {v4, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 917189
    :cond_0
    iget-object v1, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    .line 917190
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 917191
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 917192
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 917193
    invoke-virtual {v5, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 917194
    :catch_0
    move-exception v0

    .line 917195
    sget-object v1, Lcom/facebook/ipc/model/FacebookPhonebookContact;->c:Ljava/lang/Class;

    const-string v2, "How do we get a JSONException when *encoding* data? %s"

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 917196
    const-string v0, ""

    :goto_2
    return-object v0

    .line 917197
    :cond_1
    :try_start_1
    const-string v1, "emails"

    invoke-virtual {v4, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 917198
    :cond_2
    iget-object v1, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    .line 917199
    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 917200
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 917201
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 917202
    invoke-virtual {v5, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_3

    .line 917203
    :cond_3
    const-string v1, "phones"

    invoke-virtual {v4, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 917204
    :cond_4
    iget-wide v6, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->recordId:J

    const-wide/16 v8, -0x1

    cmp-long v1, v6, v8

    if-eqz v1, :cond_5

    .line 917205
    const-string v1, "record_id"

    iget-wide v6, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->recordId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 917206
    :cond_5
    iget-object v1, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->nativeName:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 917207
    const-string v1, "native_name"

    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->nativeName:Ljava/lang/String;

    invoke-virtual {v4, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 917208
    :cond_6
    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto/16 :goto_0

    .line 917209
    :cond_7
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 917174
    const-string v0, ""

    .line 917175
    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->email:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 917176
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->email:Ljava/lang/String;

    .line 917177
    :cond_0
    :goto_0
    return-object v0

    .line 917178
    :cond_1
    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->phone:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 917179
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->phone:Ljava/lang/String;

    goto :goto_0

    .line 917180
    :cond_2
    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 917181
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 917182
    :cond_3
    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 917183
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 917173
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 917162
    if-ne p0, p1, :cond_1

    .line 917163
    :cond_0
    :goto_0
    return v0

    .line 917164
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 917165
    :cond_3
    check-cast p1, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    .line 917166
    iget-boolean v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->isFriend:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/model/FacebookPhonebookContact;->isFriend:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 917167
    :cond_4
    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->ordinal:J

    iget-wide v4, p1, Lcom/facebook/ipc/model/FacebookPhonebookContact;->ordinal:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    .line 917168
    :cond_5
    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->recordId:J

    iget-wide v4, p1, Lcom/facebook/ipc/model/FacebookPhonebookContact;->recordId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    .line 917169
    :cond_6
    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->userId:J

    iget-wide v4, p1, Lcom/facebook/ipc/model/FacebookPhonebookContact;->userId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    .line 917170
    :cond_7
    iget-object v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    .line 917171
    :cond_8
    iget-object v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    .line 917172
    :cond_9
    iget-object v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    iget-object v3, p1, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 917161
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->recordId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->userId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->isFriend:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->ordinal:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 917149
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917150
    iget-wide v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->recordId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 917151
    iget-boolean v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->isFriend:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917152
    iget-wide v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->userId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 917153
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->phone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917154
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 917155
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->email:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917156
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 917157
    iget-wide v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->ordinal:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 917158
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->nativeName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 917159
    return-void

    .line 917160
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
