.class public final Lcom/facebook/fig/mediagrid/FigMediaGrid;
.super LX/6WN;
.source ""


# instance fields
.field public a:LX/6WP;

.field private b:Z

.field private c:I

.field private d:I

.field public e:LX/6WM;

.field private f:LX/3rW;

.field private g:LX/6WV;

.field private final h:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1105762
    invoke-direct {p0, p1}, LX/6WN;-><init>(Landroid/content/Context;)V

    .line 1105763
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->h:Landroid/graphics/Paint;

    .line 1105764
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1105765
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1105890
    invoke-direct {p0, p1, p2}, LX/6WN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1105891
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->h:Landroid/graphics/Paint;

    .line 1105892
    invoke-direct {p0, p1, p2}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1105893
    return-void
.end method

.method public static synthetic a(Lcom/facebook/fig/mediagrid/FigMediaGrid;I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1105889
    invoke-virtual {p0, p1}, LX/6WN;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1105883
    iget-object v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1105884
    new-instance v0, LX/3rW;

    new-instance v1, LX/6WL;

    invoke-direct {v1, p0}, LX/6WL;-><init>(Lcom/facebook/fig/mediagrid/FigMediaGrid;)V

    invoke-direct {v0, p1, v1}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->f:LX/3rW;

    .line 1105885
    new-instance v0, LX/6WV;

    invoke-direct {v0, p1}, LX/6WV;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->g:LX/6WV;

    .line 1105886
    sget-object v0, LX/03r;->FigMediaGridAttrs:[I

    const v1, 0x7f0e0393

    invoke-direct {p0, v0, p2, v1}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a([ILandroid/util/AttributeSet;I)V

    .line 1105887
    new-instance v0, LX/6WT;

    invoke-direct {v0, p0}, LX/6WT;-><init>(LX/6WN;)V

    invoke-virtual {p0, v0}, LX/6WN;->setAccessibilityHelper(LX/6WT;)V

    .line 1105888
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 1105871
    invoke-virtual {p0}, LX/6WN;->getNumDraweeControllers()I

    move-result v7

    .line 1105872
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_0

    .line 1105873
    invoke-virtual {p0, v6}, LX/6WN;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1105874
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1105875
    iget-object v1, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->h:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 1105876
    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v1

    .line 1105877
    iget v3, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v1

    .line 1105878
    iget v4, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v1

    .line 1105879
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    .line 1105880
    int-to-float v1, v2

    int-to-float v2, v3

    int-to-float v3, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->h:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1105881
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 1105882
    :cond_0
    return-void
.end method

.method private a([ILandroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # [I
        .annotation build Landroid/support/annotation/StyleableRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1105846
    invoke-virtual {p0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p3, p1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1105847
    invoke-virtual {p0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p2, p1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 1105848
    const/16 v0, 0x0

    const/16 v3, 0x0

    invoke-virtual {v1, v3, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 1105849
    iget-object v3, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->h:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1105850
    const/16 v0, 0x3

    const/16 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    .line 1105851
    iget-object v3, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->h:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1105852
    const/16 v0, 0x1

    const/16 v3, 0x1

    invoke-virtual {v1, v3, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 1105853
    iget-object v3, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->g:LX/6WV;

    .line 1105854
    iget-object v4, v3, LX/6WV;->c:Landroid/graphics/Paint;

    if-nez v4, :cond_0

    .line 1105855
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, v3, LX/6WV;->c:Landroid/graphics/Paint;

    .line 1105856
    iget-object v4, v3, LX/6WV;->c:Landroid/graphics/Paint;

    sget-object p1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, p1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1105857
    :cond_0
    iget-object v4, v3, LX/6WV;->c:Landroid/graphics/Paint;

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1105858
    const/16 v0, 0x2

    const/16 v3, 0x2

    invoke-virtual {v1, v3, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 1105859
    iget-object v3, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->g:LX/6WV;

    invoke-virtual {v3, v0}, LX/6WU;->e(I)V

    .line 1105860
    const/16 v0, 0x4

    const/16 v3, 0x4

    invoke-virtual {v1, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 1105861
    iget-object v3, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->g:LX/6WV;

    invoke-virtual {v3, v0}, LX/6WU;->d(I)V

    .line 1105862
    const/16 v0, 0x5

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1105863
    if-nez v0, :cond_1

    .line 1105864
    const/16 v0, 0x5

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1105865
    :cond_1
    iput-object v0, p0, LX/6WN;->e:Landroid/graphics/drawable/Drawable;

    .line 1105866
    const/16 v0, 0x7

    const/16 v3, 0x7

    invoke-virtual {v1, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->d:I

    .line 1105867
    const/16 v0, 0x6

    const/16 v3, 0x6

    invoke-virtual {v1, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->c:I

    .line 1105868
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1105869
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 1105870
    return-void
.end method

.method public static synthetic b(Lcom/facebook/fig/mediagrid/FigMediaGrid;I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1105845
    invoke-virtual {p0, p1}, LX/6WN;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic c(Lcom/facebook/fig/mediagrid/FigMediaGrid;I)LX/1aZ;
    .locals 1

    .prologue
    .line 1105844
    invoke-virtual {p0, p1}, LX/6WN;->b(I)LX/1aZ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/0Px;LX/6WP;)Lcom/facebook/fig/mediagrid/FigMediaGrid;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1aZ;",
            ">;",
            "LX/6WP;",
            ")",
            "Lcom/facebook/fig/mediagrid/FigMediaGrid;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1105834
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1105835
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1105836
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p2}, LX/6WP;->e()I

    move-result v2

    if-ne v0, v2, :cond_0

    invoke-virtual {p2}, LX/6WP;->e()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1105837
    invoke-virtual {p0, p1}, LX/6WN;->setDraweeControllers(LX/0Px;)V

    .line 1105838
    iput-object p2, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    .line 1105839
    invoke-virtual {p0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->invalidate()V

    .line 1105840
    invoke-virtual {p0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->requestLayout()V

    .line 1105841
    iput-boolean v1, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->b:Z

    .line 1105842
    return-object p0

    .line 1105843
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/6WM;)Lcom/facebook/fig/mediagrid/FigMediaGrid;
    .locals 2

    .prologue
    .line 1105829
    iget-object v0, p0, LX/6WN;->g:LX/6WT;

    move-object v0, v0

    .line 1105830
    const/4 v1, 0x1

    .line 1105831
    iput-boolean v1, v0, LX/6WT;->d:Z

    .line 1105832
    iput-object p1, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->e:LX/6WM;

    .line 1105833
    return-object p0
.end method

.method public final c(I)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1105813
    invoke-virtual {p0}, LX/6WN;->getNumDraweeControllers()I

    move-result v0

    .line 1105814
    add-int/lit8 v4, p1, 0x1

    .line 1105815
    iget-object v1, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->g:LX/6WV;

    .line 1105816
    iget v5, v1, LX/6WV;->b:I

    move v1, v5

    .line 1105817
    if-lez v1, :cond_0

    move v1, v2

    .line 1105818
    :goto_0
    if-eqz v1, :cond_1

    if-ne v4, v0, :cond_1

    .line 1105819
    invoke-virtual {p0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c72

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->g:LX/6WV;

    .line 1105820
    iget v5, v4, LX/6WV;->b:I

    move v4, v5

    .line 1105821
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1105822
    :goto_1
    return-object v0

    :cond_0
    move v1, v3

    .line 1105823
    goto :goto_0

    .line 1105824
    :cond_1
    invoke-virtual {p0, p1}, LX/6WN;->b(I)LX/1aZ;

    move-result-object v5

    invoke-interface {v5}, LX/1aZ;->g()Ljava/lang/String;

    move-result-object v5

    .line 1105825
    if-eqz v1, :cond_2

    add-int/lit8 v0, v0, -0x1

    .line 1105826
    :cond_2
    if-nez v5, :cond_3

    .line 1105827
    invoke-virtual {p0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f080c73

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1105828
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f080c74

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v2

    aput-object v5, v7, v8

    invoke-virtual {v1, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final d(I)Lcom/facebook/fig/mediagrid/FigMediaGrid;
    .locals 1

    .prologue
    .line 1105810
    iget-object v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->g:LX/6WV;

    invoke-virtual {v0, p1}, LX/6WV;->a(I)V

    .line 1105811
    invoke-virtual {p0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->invalidate()V

    .line 1105812
    return-object p0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1105806
    invoke-super {p0, p1}, LX/6WN;->onDraw(Landroid/graphics/Canvas;)V

    .line 1105807
    iget-object v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->g:LX/6WV;

    invoke-virtual {v0, p1}, LX/6WU;->a(Landroid/graphics/Canvas;)V

    .line 1105808
    invoke-direct {p0, p1}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a(Landroid/graphics/Canvas;)V

    .line 1105809
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 11

    .prologue
    .line 1105792
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->b:Z

    if-nez v0, :cond_0

    .line 1105793
    :goto_0
    return-void

    .line 1105794
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->b:Z

    .line 1105795
    iget-object v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    invoke-virtual {v0}, LX/6WP;->e()I

    move-result v1

    .line 1105796
    sub-int v0, p4, p2

    iget v2, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->d:I

    add-int/2addr v0, v2

    int-to-float v0, v0

    iget-object v2, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    invoke-virtual {v2}, LX/6WP;->a()I

    move-result v2

    int-to-float v2, v2

    div-float v2, v0, v2

    .line 1105797
    sub-int v0, p5, p3

    iget v3, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->c:I

    add-int/2addr v0, v3

    int-to-float v0, v0

    iget-object v3, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    invoke-virtual {v3}, LX/6WP;->b()I

    move-result v3

    int-to-float v3, v3

    div-float v3, v0, v3

    .line 1105798
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    .line 1105799
    iget-object v4, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    invoke-virtual {v4, v0}, LX/6WP;->a(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v2

    .line 1105800
    iget-object v5, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    invoke-virtual {v5, v0}, LX/6WP;->b(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v3

    .line 1105801
    iget-object v6, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    invoke-virtual {v6, v0}, LX/6WP;->c(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    iget v7, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->d:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    .line 1105802
    iget-object v7, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    invoke-virtual {v7, v0}, LX/6WP;->d(I)I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v3

    iget v8, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->c:I

    int-to-float v8, v8

    sub-float/2addr v7, v8

    .line 1105803
    invoke-virtual {p0, v0}, LX/6WN;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    const/high16 v9, 0x3f000000    # 0.5f

    add-float/2addr v9, v4

    float-to-int v9, v9

    const/high16 v10, 0x3f000000    # 0.5f

    add-float/2addr v10, v5

    float-to-int v10, v10

    add-float/2addr v4, v6

    const/high16 v6, 0x3f000000    # 0.5f

    add-float/2addr v4, v6

    float-to-int v4, v4

    add-float/2addr v5, v7

    const/high16 v6, 0x3f000000    # 0.5f

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v8, v9, v10, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1105804
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1105805
    :cond_1
    iget-object v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->g:LX/6WV;

    invoke-virtual {p0}, LX/6WN;->getNumDraweeControllers()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, LX/6WN;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6WU;->a(Landroid/graphics/Rect;)V

    goto/16 :goto_0
.end method

.method public final onMeasure(II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1105768
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 1105769
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1105770
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 1105771
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1105772
    if-nez v2, :cond_0

    .line 1105773
    invoke-virtual {p0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1105774
    :cond_0
    const/high16 v2, 0x40000000    # 2.0f

    if-eq v3, v2, :cond_1

    .line 1105775
    iget-object v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    if-nez v0, :cond_2

    move v0, v1

    .line 1105776
    :cond_1
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->setMeasuredDimension(II)V

    .line 1105777
    return-void

    .line 1105778
    :cond_2
    iget-object v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    .line 1105779
    iget v2, v0, LX/6WP;->e:F

    move v0, v2

    .line 1105780
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-nez v2, :cond_3

    .line 1105781
    iget-object v0, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    .line 1105782
    iget v2, v0, LX/6WP;->c:I

    move v0, v2

    .line 1105783
    int-to-float v0, v0

    iget-object v2, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    invoke-virtual {v2}, LX/6WP;->c()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    int-to-float v2, v1

    mul-float/2addr v0, v2

    float-to-int v0, v0

    goto :goto_0

    .line 1105784
    :cond_3
    iget-object v2, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    invoke-virtual {v2, v4}, LX/6WP;->c(I)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    .line 1105785
    iget p1, v3, LX/6WP;->b:I

    move v3, p1

    .line 1105786
    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 1105787
    div-float v0, v2, v0

    .line 1105788
    iget-object v2, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    .line 1105789
    iget v3, v2, LX/6WP;->c:I

    move v2, v3

    .line 1105790
    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a:LX/6WP;

    invoke-virtual {v3, v4}, LX/6WP;->d(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 1105791
    int-to-float v3, v1

    mul-float/2addr v2, v3

    mul-float/2addr v0, v2

    float-to-int v0, v0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x3aede812

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1105766
    iget-object v2, p0, Lcom/facebook/fig/mediagrid/FigMediaGrid;->f:LX/3rW;

    invoke-virtual {v2, p1}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 1105767
    invoke-super {p0, p1}, LX/6WN;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    const v2, -0x564b1a81

    invoke-static {v2, v1}, LX/02F;->a(II)V

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
