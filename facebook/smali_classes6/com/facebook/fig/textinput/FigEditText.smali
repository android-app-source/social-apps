.class public Lcom/facebook/fig/textinput/FigEditText;
.super Lcom/facebook/resources/ui/FbEditText;
.source ""


# instance fields
.field private A:Landroid/text/TextWatcher;

.field private B:I

.field private C:Z

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/content/res/ColorStateList;

.field private d:Landroid/content/res/ColorStateList;

.field private e:Landroid/content/res/ColorStateList;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Landroid/content/res/ColorStateList;

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:Ljava/lang/CharSequence;

.field private q:Ljava/lang/CharSequence;

.field private r:I

.field private final s:Landroid/text/TextPaint;

.field private final t:Landroid/graphics/Rect;

.field private u:Landroid/content/res/ColorStateList;

.field private v:Landroid/content/res/ColorStateList;

.field private w:Landroid/content/res/ColorStateList;

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1106237
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    .line 1106238
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->s:Landroid/text/TextPaint;

    .line 1106239
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->t:Landroid/graphics/Rect;

    .line 1106240
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/fig/textinput/FigEditText;->a(Landroid/util/AttributeSet;)V

    .line 1106241
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1106180
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fig/textinput/FigEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1106181
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1106183
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1106184
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->s:Landroid/text/TextPaint;

    .line 1106185
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->t:Landroid/graphics/Rect;

    .line 1106186
    invoke-direct {p0, p2}, Lcom/facebook/fig/textinput/FigEditText;->a(Landroid/util/AttributeSet;)V

    .line 1106187
    return-void
.end method

.method private a()I
    .locals 5

    .prologue
    .line 1106188
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1106189
    invoke-static {}, LX/0zj;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 1106190
    iget-object v1, p0, Lcom/facebook/fig/textinput/FigEditText;->s:Landroid/text/TextPaint;

    const-string v2, "1"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1106191
    iget v1, p0, Lcom/facebook/fig/textinput/FigEditText;->x:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/fig/textinput/FigEditText;->y:I

    add-int/2addr v0, v1

    .line 1106192
    :goto_0
    return v0

    .line 1106193
    :cond_0
    iget v0, p0, Lcom/facebook/fig/textinput/FigEditText;->o:I

    goto :goto_0
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1106194
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/facebook/fig/textinput/FigEditText;->C:Z

    .line 1106195
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->s:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1106196
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->s:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1106197
    if-eqz p1, :cond_1

    .line 1106198
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->FigEditText:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1106199
    :try_start_0
    const/16 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/textinput/FigEditText;->setType(I)V

    .line 1106200
    const/16 v0, 0x3

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/textinput/FigEditText;->setCharLimit(I)V

    .line 1106201
    const/16 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/textinput/FigEditText;->setGlyph(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1106202
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1106203
    :cond_1
    return-void

    .line 1106204
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 5

    .prologue
    .line 1106205
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1106206
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->p:Ljava/lang/CharSequence;

    .line 1106207
    :goto_0
    return-void

    .line 1106208
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getWidth()I

    move-result v0

    if-lez v0, :cond_1

    .line 1106209
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getWidth()I

    move-result v0

    invoke-static {p0}, LX/0vv;->n(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {p0}, LX/0vv;->o(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 1106210
    iget-object v1, p0, Lcom/facebook/fig/textinput/FigEditText;->s:Landroid/text/TextPaint;

    int-to-float v0, v0

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p1, v1, v0, v2}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 1106211
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v0

    .line 1106212
    iget-object v1, p0, Lcom/facebook/fig/textinput/FigEditText;->s:Landroid/text/TextPaint;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/fig/textinput/FigEditText;->t:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1106213
    iput-object p1, p0, Lcom/facebook/fig/textinput/FigEditText;->p:Ljava/lang/CharSequence;

    .line 1106214
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->f()V

    goto :goto_0
.end method

.method private b()I
    .locals 2

    .prologue
    .line 1106215
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->f:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/fig/textinput/FigEditText;->l:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/facebook/fig/textinput/FigEditText;->i:I

    iget v1, p0, Lcom/facebook/fig/textinput/FigEditText;->h:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/fig/textinput/FigEditText;->k:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private c()V
    .locals 9

    .prologue
    .line 1106216
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->b()I

    move-result v6

    .line 1106217
    iget v8, p0, Lcom/facebook/fig/textinput/FigEditText;->n:I

    .line 1106218
    iget v7, p0, Lcom/facebook/fig/textinput/FigEditText;->m:I

    .line 1106219
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->a()I

    move-result v5

    .line 1106220
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    .line 1106221
    iget-boolean v0, p0, Lcom/facebook/fig/textinput/FigEditText;->C:Z

    if-eqz v0, :cond_0

    move v2, v6

    .line 1106222
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/fig/textinput/FigEditText;->C:Z

    if-eqz v0, :cond_1

    iget v4, p0, Lcom/facebook/fig/textinput/FigEditText;->m:I

    .line 1106223
    :goto_1
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    iget-object v1, p0, Lcom/facebook/fig/textinput/FigEditText;->b:Landroid/graphics/drawable/Drawable;

    iget v3, p0, Lcom/facebook/fig/textinput/FigEditText;->n:I

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 1106224
    invoke-static {p0, v0}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1106225
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 1106226
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1106227
    iget-boolean v0, p0, Lcom/facebook/fig/textinput/FigEditText;->C:Z

    if-eqz v0, :cond_2

    iget v0, v2, Landroid/graphics/Rect;->left:I

    :goto_2
    add-int/2addr v6, v0

    .line 1106228
    iget-boolean v0, p0, Lcom/facebook/fig/textinput/FigEditText;->C:Z

    if-eqz v0, :cond_3

    iget v0, v2, Landroid/graphics/Rect;->right:I

    :goto_3
    add-int/2addr v0, v7

    .line 1106229
    iget v1, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v8

    .line 1106230
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v2

    .line 1106231
    :goto_4
    invoke-static {p0, v6, v1, v0, v5}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 1106232
    return-void

    .line 1106233
    :cond_0
    iget v2, p0, Lcom/facebook/fig/textinput/FigEditText;->m:I

    goto :goto_0

    :cond_1
    move v4, v6

    .line 1106234
    goto :goto_1

    .line 1106235
    :cond_2
    iget v0, v2, Landroid/graphics/Rect;->right:I

    goto :goto_2

    .line 1106236
    :cond_3
    iget v0, v2, Landroid/graphics/Rect;->left:I

    goto :goto_3

    :cond_4
    move v0, v7

    move v1, v8

    goto :goto_4
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1106254
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->i()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1106255
    :cond_0
    :goto_0
    return-void

    .line 1106256
    :cond_1
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->getCharCountText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/fig/textinput/FigEditText;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1106257
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 1106258
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->i()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->getTextLength()I

    move-result v0

    iget v1, p0, Lcom/facebook/fig/textinput/FigEditText;->r:I

    if-le v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->e:Landroid/content/res/ColorStateList;

    .line 1106259
    :goto_0
    iget-object v1, p0, Lcom/facebook/fig/textinput/FigEditText;->c:Landroid/content/res/ColorStateList;

    if-eq v1, v0, :cond_1

    .line 1106260
    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->c:Landroid/content/res/ColorStateList;

    .line 1106261
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->b:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/fig/textinput/FigEditText;->c:Landroid/content/res/ColorStateList;

    invoke-static {v0, v1}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 1106262
    :cond_1
    return-void

    .line 1106263
    :cond_2
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->d:Landroid/content/res/ColorStateList;

    goto :goto_0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 1106246
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1106247
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->w:Landroid/content/res/ColorStateList;

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->u:Landroid/content/res/ColorStateList;

    .line 1106248
    :goto_0
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->u:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getDrawableState()[I

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 1106249
    iget-object v1, p0, Lcom/facebook/fig/textinput/FigEditText;->s:Landroid/text/TextPaint;

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1106250
    return-void

    .line 1106251
    :cond_0
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1106252
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->getTextLength()I

    move-result v0

    iget v1, p0, Lcom/facebook/fig/textinput/FigEditText;->r:I

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->w:Landroid/content/res/ColorStateList;

    :goto_1
    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->u:Landroid/content/res/ColorStateList;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->v:Landroid/content/res/ColorStateList;

    goto :goto_1

    .line 1106253
    :cond_2
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->v:Landroid/content/res/ColorStateList;

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->u:Landroid/content/res/ColorStateList;

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1106264
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1106265
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->f:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/fig/textinput/FigEditText;->g:Landroid/content/res/ColorStateList;

    invoke-static {v0, v1}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 1106266
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1106267
    :cond_0
    return-void
.end method

.method private getCharCountText()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1106245
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->getTextLength()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/fig/textinput/FigEditText;->r:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTextLength()I
    .locals 1

    .prologue
    .line 1106243
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1106244
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 1106242
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->i()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 1106182
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->q:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Z
    .locals 1

    .prologue
    .line 1106095
    iget v0, p0, Lcom/facebook/fig/textinput/FigEditText;->r:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 1106090
    invoke-super {p0}, Lcom/facebook/resources/ui/FbEditText;->drawableStateChanged()V

    .line 1106091
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->f()V

    .line 1106092
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1106093
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1106094
    :cond_0
    return-void
.end method

.method public getCharLimit()I
    .locals 1

    .prologue
    .line 1106089
    iget v0, p0, Lcom/facebook/fig/textinput/FigEditText;->r:I

    return v0
.end method

.method public getErrorMessage()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1106088
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->q:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getGlyph()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1106087
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->f:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 1106086
    iget v0, p0, Lcom/facebook/fig/textinput/FigEditText;->B:I

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1106071
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbEditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 1106072
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1106073
    iget-boolean v0, p0, Lcom/facebook/fig/textinput/FigEditText;->C:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/facebook/fig/textinput/FigEditText;->i:I

    .line 1106074
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getScrollX()I

    move-result v1

    add-int/2addr v0, v1

    .line 1106075
    iget-object v1, p0, Lcom/facebook/fig/textinput/FigEditText;->f:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lcom/facebook/fig/textinput/FigEditText;->j:I

    iget v3, p0, Lcom/facebook/fig/textinput/FigEditText;->h:I

    add-int/2addr v3, v0

    iget v4, p0, Lcom/facebook/fig/textinput/FigEditText;->j:I

    iget v5, p0, Lcom/facebook/fig/textinput/FigEditText;->h:I

    add-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1106076
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1106077
    :cond_0
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1106078
    iget-boolean v0, p0, Lcom/facebook/fig/textinput/FigEditText;->C:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getWidth()I

    move-result v0

    invoke-static {p0}, LX/0vv;->o(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/fig/textinput/FigEditText;->t:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1106079
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getScrollX()I

    move-result v1

    add-int/2addr v0, v1

    .line 1106080
    iget-object v1, p0, Lcom/facebook/fig/textinput/FigEditText;->p:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    int-to-float v0, v0

    iget v2, p0, Lcom/facebook/fig/textinput/FigEditText;->z:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/fig/textinput/FigEditText;->s:Landroid/text/TextPaint;

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1106081
    :cond_1
    return-void

    .line 1106082
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/fig/textinput/FigEditText;->i:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/fig/textinput/FigEditText;->h:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 1106083
    :cond_3
    invoke-static {p0}, LX/0vv;->o(Landroid/view/View;)I

    move-result v0

    goto :goto_1
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1106172
    invoke-super/range {p0 .. p5}, Lcom/facebook/resources/ui/FbEditText;->onLayout(ZIIII)V

    .line 1106173
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1106174
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->q:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/facebook/fig/textinput/FigEditText;->a(Ljava/lang/CharSequence;)V

    .line 1106175
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1106176
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lcom/facebook/fig/textinput/FigEditText;->y:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/fig/textinput/FigEditText;->z:I

    .line 1106177
    :cond_1
    return-void

    .line 1106178
    :cond_2
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1106179
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->getCharCountText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/fig/textinput/FigEditText;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1106096
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbEditText;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 1106097
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1106098
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->d()V

    .line 1106099
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->e()V

    .line 1106100
    :cond_0
    return-void
.end method

.method public setCharLimit(I)V
    .locals 1

    .prologue
    .line 1106101
    iget v0, p0, Lcom/facebook/fig/textinput/FigEditText;->r:I

    if-ne v0, p1, :cond_1

    .line 1106102
    :cond_0
    :goto_0
    return-void

    .line 1106103
    :cond_1
    iput p1, p0, Lcom/facebook/fig/textinput/FigEditText;->r:I

    .line 1106104
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1106105
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->d()V

    .line 1106106
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->e()V

    .line 1106107
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->c()V

    .line 1106108
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->requestLayout()V

    .line 1106109
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->invalidate()V

    goto :goto_0
.end method

.method public setErrorMessage(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1106110
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->q:Ljava/lang/CharSequence;

    if-ne v0, p1, :cond_0

    .line 1106111
    :goto_0
    return-void

    .line 1106112
    :cond_0
    iput-object p1, p0, Lcom/facebook/fig/textinput/FigEditText;->q:Ljava/lang/CharSequence;

    .line 1106113
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1106114
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->q:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/facebook/fig/textinput/FigEditText;->a(Ljava/lang/CharSequence;)V

    .line 1106115
    :goto_1
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->e()V

    .line 1106116
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->c()V

    .line 1106117
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->requestLayout()V

    .line 1106118
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->invalidate()V

    goto :goto_0

    .line 1106119
    :cond_1
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->d()V

    goto :goto_1
.end method

.method public setGlyph(I)V
    .locals 1

    .prologue
    .line 1106120
    if-lez p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fig/textinput/FigEditText;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 1106121
    return-void

    .line 1106122
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setGlyph(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1106123
    if-eqz p1, :cond_0

    .line 1106124
    invoke-static {p1}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->f:Landroid/graphics/drawable/Drawable;

    .line 1106125
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->g()V

    .line 1106126
    :goto_0
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->c()V

    .line 1106127
    return-void

    .line 1106128
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->f:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setSingleLine(Z)V
    .locals 0

    .prologue
    .line 1106084
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbEditText;->setSingleLine(Z)V

    .line 1106085
    return-void
.end method

.method public setTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 1106129
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->A:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/facebook/fig/textinput/FigEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1106130
    iput-object p1, p0, Lcom/facebook/fig/textinput/FigEditText;->A:Landroid/text/TextWatcher;

    invoke-virtual {p0, p1}, Lcom/facebook/fig/textinput/FigEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1106131
    return-void
.end method

.method public setType(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1106132
    iput p1, p0, Lcom/facebook/fig/textinput/FigEditText;->B:I

    .line 1106133
    if-ne p1, v3, :cond_0

    const v0, 0x7f0e070d

    .line 1106134
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/03r;->FigEditTextInternal:[I

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1106135
    :try_start_0
    const/16 v0, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    .line 1106136
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/fig/textinput/FigEditText;->setTextSize(IF)V

    .line 1106137
    const/16 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 1106138
    invoke-virtual {p0, v0}, Lcom/facebook/fig/textinput/FigEditText;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1106139
    const/16 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 1106140
    invoke-virtual {p0, v0}, Lcom/facebook/fig/textinput/FigEditText;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 1106141
    const/16 v0, 0x12

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->g:Landroid/content/res/ColorStateList;

    .line 1106142
    const/16 v0, 0x7

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/textinput/FigEditText;->h:I

    .line 1106143
    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/textinput/FigEditText;->i:I

    .line 1106144
    const/16 v0, 0xa

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/textinput/FigEditText;->j:I

    .line 1106145
    const/16 v0, 0x9

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/textinput/FigEditText;->k:I

    .line 1106146
    const/16 v0, 0xe

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/textinput/FigEditText;->x:I

    .line 1106147
    const/16 v0, 0xf

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/textinput/FigEditText;->y:I

    .line 1106148
    const/16 v0, 0xd

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    .line 1106149
    iget-object v2, p0, Lcom/facebook/fig/textinput/FigEditText;->s:Landroid/text/TextPaint;

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1106150
    const/16 v0, 0x10

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->v:Landroid/content/res/ColorStateList;

    .line 1106151
    const/16 v0, 0x11

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->w:Landroid/content/res/ColorStateList;

    .line 1106152
    const/16 v0, 0x5

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/textinput/FigEditText;->l:I

    .line 1106153
    const/16 v0, 0x3

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/textinput/FigEditText;->n:I

    .line 1106154
    const/16 v0, 0x6

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/textinput/FigEditText;->m:I

    .line 1106155
    const/16 v0, 0x4

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/textinput/FigEditText;->o:I

    .line 1106156
    const/16 v0, 0xb

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->d:Landroid/content/res/ColorStateList;

    .line 1106157
    const/16 v0, 0xc

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->e:Landroid/content/res/ColorStateList;

    .line 1106158
    if-ne p1, v3, :cond_1

    .line 1106159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->b:Landroid/graphics/drawable/Drawable;

    .line 1106160
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1106161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->c:Landroid/content/res/ColorStateList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1106162
    :goto_1
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1106163
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->d()V

    .line 1106164
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->e()V

    .line 1106165
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->g()V

    .line 1106166
    invoke-direct {p0}, Lcom/facebook/fig/textinput/FigEditText;->c()V

    .line 1106167
    return-void

    .line 1106168
    :cond_0
    const v0, 0x7f0e070c

    goto/16 :goto_0

    .line 1106169
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/facebook/fig/textinput/FigEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f020847

    invoke-static {v0, v2}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->b:Landroid/graphics/drawable/Drawable;

    .line 1106170
    iget-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fig/textinput/FigEditText;->b:Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1106171
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method
