.class public Lcom/facebook/fig/listitem/FigListItem;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:I
    .annotation build Lcom/facebook/fig/listitem/Const$ThumbnailSize;
    .end annotation
.end field

.field private k:I

.field private l:LX/6WW;

.field private m:LX/6WW;

.field private n:LX/6WW;

.field private o:I
    .annotation build Lcom/facebook/fig/listitem/annotations/TitleTextAppearenceType;
    .end annotation
.end field

.field private p:I
    .annotation build Lcom/facebook/fig/listitem/annotations/BodyTextAppearenceType;
    .end annotation
.end field

.field private q:I
    .annotation build Lcom/facebook/fig/listitem/annotations/MetaTextAppearenceType;
    .end annotation
.end field

.field private r:Z

.field private s:Z

.field private t:I

.field private u:I

.field private v:I

.field private w:I
    .annotation build Lcom/facebook/fig/listitem/annotations/ActionType;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1105615
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1105616
    iput-boolean v1, p0, Lcom/facebook/fig/listitem/FigListItem;->r:Z

    .line 1105617
    iput v1, p0, Lcom/facebook/fig/listitem/FigListItem;->w:I

    .line 1105618
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->a(Landroid/util/AttributeSet;I)V

    .line 1105619
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p2    # I
        .annotation build Lcom/facebook/fig/listitem/annotations/ActionType;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1105479
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1105480
    iput-boolean v1, p0, Lcom/facebook/fig/listitem/FigListItem;->r:Z

    .line 1105481
    iput v1, p0, Lcom/facebook/fig/listitem/FigListItem;->w:I

    .line 1105482
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->a(Landroid/util/AttributeSet;I)V

    .line 1105483
    invoke-direct {p0, p2}, Lcom/facebook/fig/listitem/FigListItem;->setActionType(I)V

    .line 1105484
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1105485
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1105486
    iput-boolean v0, p0, Lcom/facebook/fig/listitem/FigListItem;->r:Z

    .line 1105487
    iput v0, p0, Lcom/facebook/fig/listitem/FigListItem;->w:I

    .line 1105488
    const v0, 0x7f0101fb

    invoke-direct {p0, p2, v0}, Lcom/facebook/fig/listitem/FigListItem;->a(Landroid/util/AttributeSet;I)V

    .line 1105489
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1105490
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1105491
    iput-boolean v0, p0, Lcom/facebook/fig/listitem/FigListItem;->r:Z

    .line 1105492
    iput v0, p0, Lcom/facebook/fig/listitem/FigListItem;->w:I

    .line 1105493
    invoke-direct {p0, p2, p3}, Lcom/facebook/fig/listitem/FigListItem;->a(Landroid/util/AttributeSet;I)V

    .line 1105494
    return-void
.end method

.method private a(Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 1105495
    new-instance v0, LX/6WW;

    invoke-direct {v0}, LX/6WW;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    .line 1105496
    new-instance v0, LX/6WW;

    invoke-direct {v0}, LX/6WW;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    .line 1105497
    new-instance v0, LX/6WW;

    invoke-direct {v0}, LX/6WW;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    .line 1105498
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f8d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1105499
    invoke-super {p0, v0, v0, v0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setPadding(IIII)V

    .line 1105500
    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1105501
    const v1, 0x7f0e0131

    invoke-direct {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleTextAppearance(I)V

    .line 1105502
    const v1, 0x7f0e0132

    invoke-direct {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyTextAppearance(I)V

    .line 1105503
    const v1, 0x7f0e0133

    invoke-direct {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setMetaTextAppearance(I)V

    .line 1105504
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setAuxViewPadding(I)V

    .line 1105505
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setAuxViewPadding(I)V

    .line 1105506
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0097

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setBackgroundColor(I)V

    .line 1105507
    if-eqz p1, :cond_1

    .line 1105508
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->FigListItem:[I

    invoke-virtual {v0, p1, v1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1105509
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1105510
    if-lez v1, :cond_2

    .line 1105511
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(I)V

    .line 1105512
    :goto_0
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1105513
    if-lez v1, :cond_3

    .line 1105514
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(I)V

    .line 1105515
    :goto_1
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1105516
    if-lez v1, :cond_4

    .line 1105517
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(I)V

    .line 1105518
    :goto_2
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 1105519
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setThumbnailSizeType(I)V

    .line 1105520
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 1105521
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleTextAppearenceType(I)V

    .line 1105522
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 1105523
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyTextAppearenceType(I)V

    .line 1105524
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 1105525
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setMetaTextAppearenceType(I)V

    .line 1105526
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/fig/listitem/FigListItem;->r:Z

    .line 1105527
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fig/listitem/FigListItem;->t:I

    .line 1105528
    const/16 v1, 0x6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fig/listitem/FigListItem;->u:I

    .line 1105529
    const/16 v1, 0x6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fig/listitem/FigListItem;->v:I

    .line 1105530
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 1105531
    invoke-direct {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionType(I)V

    .line 1105532
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1105533
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionText(Ljava/lang/CharSequence;)V

    .line 1105534
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1105535
    if-eqz v1, :cond_0

    .line 1105536
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1105537
    :cond_0
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 1105538
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionState(Z)V

    .line 1105539
    invoke-direct {p0}, Lcom/facebook/fig/listitem/FigListItem;->e()V

    .line 1105540
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1105541
    :cond_1
    invoke-direct {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContent()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1105542
    return-void

    .line 1105543
    :cond_2
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1105544
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1105545
    :cond_3
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1105546
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1105547
    :cond_4
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1105548
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method private a(I)Z
    .locals 2

    .prologue
    .line 1105549
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->c()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v1, p1, 0x2

    add-int/2addr v0, v1

    .line 1105550
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getThumbnailDimensionSize()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)I
    .locals 3

    .prologue
    .line 1105551
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->c()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v1, p1, 0x2

    add-int/2addr v0, v1

    .line 1105552
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getThumbnailDimensionSize()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1105553
    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1105554
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->t:I

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->u:I

    if-ltz v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "titleMaxLines and bodyMaxLines must be non-negative"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1105555
    iget-boolean v0, p0, Lcom/facebook/fig/listitem/FigListItem;->r:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/fig/listitem/FigListItem;->s:Z

    if-nez v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    const-string v0, "maxLinesFromThumbnailSize must be false if titleMaxLines and bodyMaxLines are specified"

    invoke-static {v2, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1105556
    return-void

    :cond_2
    move v0, v2

    .line 1105557
    goto :goto_0
.end method

.method private getContent()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1105558
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getBodyText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getMetaText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setActionType(I)V
    .locals 5
    .param p1    # I
        .annotation build Lcom/facebook/fig/listitem/annotations/ActionType;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x2

    const/4 v4, 0x0

    .line 1105559
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->w:I

    if-eq p1, v0, :cond_1

    .line 1105560
    iput p1, p0, Lcom/facebook/fig/listitem/FigListItem;->w:I

    .line 1105561
    new-instance v1, LX/1au;

    invoke-direct {v1, v2, v2}, LX/1au;-><init>(II)V

    .line 1105562
    iput-boolean v3, v1, LX/1au;->b:Z

    .line 1105563
    const/16 v0, 0x11

    iput v0, v1, LX/1au;->d:I

    .line 1105564
    iput v4, v1, LX/1au;->leftMargin:I

    .line 1105565
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1105566
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->removeView(Landroid/view/View;)V

    .line 1105567
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1105568
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105569
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105570
    :cond_1
    return-void

    .line 1105571
    :pswitch_1
    new-instance v2, Lcom/facebook/fig/button/FigButton;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/facebook/fig/button/FigButton;-><init>(Landroid/content/Context;)V

    .line 1105572
    packed-switch p1, :pswitch_data_1

    .line 1105573
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported button type:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1105574
    :pswitch_2
    const/16 v0, 0x24

    .line 1105575
    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 1105576
    invoke-super {p0, v2, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1105577
    :pswitch_3
    const/16 v0, 0x12

    .line 1105578
    goto :goto_1

    .line 1105579
    :pswitch_4
    const/16 v0, 0x82

    .line 1105580
    goto :goto_1

    .line 1105581
    :pswitch_5
    const/16 v0, 0x102

    .line 1105582
    goto :goto_1

    .line 1105583
    :pswitch_6
    new-instance v0, Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/resources/ui/FbCheckBox;-><init>(Landroid/content/Context;)V

    .line 1105584
    invoke-super {p0, v0, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1105585
    :pswitch_7
    new-instance v0, Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/widget/SwitchCompat;-><init>(Landroid/content/Context;)V

    .line 1105586
    invoke-super {p0, v0, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1105587
    :pswitch_8
    new-instance v0, Lcom/facebook/resources/ui/FbRadioButton;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/resources/ui/FbRadioButton;-><init>(Landroid/content/Context;)V

    .line 1105588
    invoke-super {p0, v0, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1105589
    :pswitch_9
    if-ne p1, v3, :cond_2

    const v0, 0x7f0e0125

    .line 1105590
    :goto_2
    new-instance v2, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1105591
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1105592
    invoke-super {p0, v2, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1105593
    :cond_2
    const v0, 0x7f0e0123

    goto :goto_2

    .line 1105594
    :pswitch_a
    new-instance v0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;)V

    .line 1105595
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a00a6

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1105596
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f020722

    invoke-static {v2, v3}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1105597
    invoke-super {p0, v0, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 1105598
    :pswitch_b
    new-instance v0, Lcom/facebook/fig/button/FigToggleButton;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/fig/button/FigToggleButton;-><init>(Landroid/content/Context;)V

    .line 1105599
    const/16 v2, 0x14

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigToggleButton;->setType(I)V

    .line 1105600
    invoke-super {p0, v0, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 1105601
    :pswitch_c
    new-instance v0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;)V

    .line 1105602
    invoke-super {p0, v0, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_9
        :pswitch_9
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_b
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_c
        :pswitch_a
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private setBodyTextAppearance(I)V
    .locals 2

    .prologue
    .line 1105603
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1105604
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105605
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105606
    return-void
.end method

.method private setMetaTextAppearance(I)V
    .locals 2

    .prologue
    .line 1105607
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1105608
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105609
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105610
    return-void
.end method

.method private setTitleTextAppearance(I)V
    .locals 2

    .prologue
    .line 1105611
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1105612
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105613
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105614
    return-void
.end method


# virtual methods
.method public final a(Landroid/util/AttributeSet;)LX/1au;
    .locals 2

    .prologue
    .line 1105477
    new-instance v0, LX/1au;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/1au;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup$LayoutParams;)LX/1au;
    .locals 1

    .prologue
    .line 1105620
    instance-of v0, p1, LX/1au;

    if-eqz v0, :cond_0

    .line 1105621
    check-cast p1, LX/1au;

    .line 1105622
    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->b()LX/1au;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 1105635
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->c(I)V

    .line 1105636
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->b()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1105637
    iget-object v1, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 1105638
    iget-object v2, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {v2, p1}, LX/6WW;->c(I)V

    .line 1105639
    iget-object v2, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {v2}, LX/6WW;->b()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1105640
    iget-object v2, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {v2}, LX/6WW;->c()I

    move-result v2

    add-int/2addr v1, v2

    .line 1105641
    iget-object v2, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {v2, p1}, LX/6WW;->c(I)V

    .line 1105642
    iget-object v2, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {v2}, LX/6WW;->b()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1105643
    iget-object v2, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {v2}, LX/6WW;->c()I

    move-result v2

    add-int/2addr v1, v2

    .line 1105644
    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(II)V

    .line 1105645
    return-void
.end method

.method public final a(IIII)V
    .locals 3

    .prologue
    .line 1105664
    invoke-direct {p0, p2}, Lcom/facebook/fig/listitem/FigListItem;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1105665
    invoke-direct {p0, p2}, Lcom/facebook/fig/listitem/FigListItem;->b(I)I

    move-result v0

    add-int/2addr p2, v0

    .line 1105666
    :cond_0
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v1

    invoke-virtual {v0, v1, p1, p2, p3}, LX/6WW;->a(ZIII)V

    .line 1105667
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->c()I

    move-result v0

    add-int/2addr v0, p2

    .line 1105668
    iget-object v1, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v2

    invoke-virtual {v1, v2, p1, v0, p3}, LX/6WW;->a(ZIII)V

    .line 1105669
    iget-object v1, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1105670
    iget-object v1, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v2

    invoke-virtual {v1, v2, p1, v0, p3}, LX/6WW;->a(ZIII)V

    .line 1105671
    return-void
.end method

.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 1105663
    return-void
.end method

.method public final b()LX/1au;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 1105662
    new-instance v0, LX/1au;

    invoke-direct {v0, v1, v1}, LX/1au;-><init>(II)V

    return-object v0
.end method

.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 1105661
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/1au;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1105656
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/fig/listitem/FigListItem;->u:I

    iput v0, p0, Lcom/facebook/fig/listitem/FigListItem;->t:I

    .line 1105657
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fig/listitem/FigListItem;->s:Z

    .line 1105658
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105659
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105660
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1105651
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1105652
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/graphics/Canvas;)V

    .line 1105653
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/graphics/Canvas;)V

    .line 1105654
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/graphics/Canvas;)V

    .line 1105655
    return-void
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 1105647
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1105648
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1105649
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1105650
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1105646
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->b()LX/1au;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1105623
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a(Landroid/util/AttributeSet;)LX/1au;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1105634
    invoke-virtual {p0, p1}, Lcom/facebook/fig/listitem/FigListItem;->a(Landroid/view/ViewGroup$LayoutParams;)LX/1au;

    move-result-object v0

    return-object v0
.end method

.method public getActionState()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1105628
    iget v1, p0, Lcom/facebook/fig/listitem/FigListItem;->w:I

    packed-switch v1, :pswitch_data_0

    .line 1105629
    :goto_0
    :pswitch_0
    return v0

    .line 1105630
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbCheckBox;->isChecked()Z

    move-result v0

    goto :goto_0

    .line 1105631
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    goto :goto_0

    .line 1105632
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/resources/ui/FbRadioButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbRadioButton;->isChecked()Z

    move-result v0

    goto :goto_0

    .line 1105633
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/button/FigToggleButton;

    invoke-virtual {v0}, Lcom/facebook/fig/button/FigToggleButton;->isChecked()Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getBodyMaxLines()I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1105627
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->u:I

    return v0
.end method

.method public getBodyText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1105626
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getMetaMaxLines()I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1105625
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->v:I

    return v0
.end method

.method public getMetaText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1105624
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getThumbnailDimensionSize()I
    .locals 2

    .prologue
    .line 1105478
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/facebook/fig/listitem/FigListItem;->j:I

    invoke-static {v1}, LX/6WK;->d(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public getThumbnailSize()I
    .locals 1
    .annotation build Lcom/facebook/fig/listitem/Const$ThumbnailSize;
    .end annotation

    .prologue
    .line 1105340
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->j:I

    return v0
.end method

.method public getTitleMaxLines()I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1105403
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->t:I

    return v0
.end method

.method public getTitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1105402
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final onMeasure(II)V
    .locals 9

    .prologue
    const/16 v5, 0x8

    const/4 v0, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1105377
    iget-boolean v3, p0, Lcom/facebook/fig/listitem/FigListItem;->r:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/facebook/fig/listitem/FigListItem;->s:Z

    if-eqz v3, :cond_2

    .line 1105378
    :cond_0
    iget-object v3, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {v3}, LX/6WW;->d()Z

    move-result v6

    .line 1105379
    iget-object v3, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {v3}, LX/6WW;->d()Z

    move-result v4

    .line 1105380
    iget-object v3, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {v3}, LX/6WW;->d()Z

    move-result v3

    .line 1105381
    iget-boolean v7, p0, Lcom/facebook/fig/listitem/FigListItem;->r:Z

    if-eqz v7, :cond_5

    .line 1105382
    iget v7, p0, Lcom/facebook/fig/listitem/FigListItem;->j:I

    const/4 v8, 0x3

    if-ne v7, v8, :cond_3

    .line 1105383
    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setTitleMaxLines(I)V

    .line 1105384
    if-eqz v3, :cond_1

    move v0, v1

    :cond_1
    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setBodyMaxLines(I)V

    move v0, v3

    move v3, v4

    .line 1105385
    :goto_0
    iget-object v4, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {v4, v1}, LX/6WW;->b(I)V

    .line 1105386
    iget-object v4, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    if-eqz v6, :cond_6

    move v1, v2

    .line 1105387
    :goto_1
    iput v1, v4, LX/6WW;->c:I

    .line 1105388
    iget-object v4, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    if-eqz v3, :cond_7

    move v1, v2

    .line 1105389
    :goto_2
    iput v1, v4, LX/6WW;->c:I

    .line 1105390
    iget-object v1, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    if-eqz v0, :cond_8

    .line 1105391
    :goto_3
    iput v2, v1, LX/6WW;->c:I

    .line 1105392
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onMeasure(II)V

    .line 1105393
    return-void

    .line 1105394
    :cond_3
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleMaxLines(I)V

    .line 1105395
    invoke-virtual {p0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyMaxLines(I)V

    .line 1105396
    iget v3, p0, Lcom/facebook/fig/listitem/FigListItem;->j:I

    if-ne v3, v0, :cond_4

    move v0, v1

    :goto_4
    move v3, v0

    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_4

    .line 1105397
    :cond_5
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->t:I

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setTitleMaxLines(I)V

    .line 1105398
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->u:I

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setBodyMaxLines(I)V

    move v0, v3

    move v3, v4

    goto :goto_0

    :cond_6
    move v1, v5

    .line 1105399
    goto :goto_1

    :cond_7
    move v1, v5

    .line 1105400
    goto :goto_2

    :cond_8
    move v2, v5

    .line 1105401
    goto :goto_3
.end method

.method public final removeView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1105376
    return-void
.end method

.method public final removeViewInLayout(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1105375
    return-void
.end method

.method public setActionContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1105372
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1105373
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1105374
    :cond_0
    return-void
.end method

.method public setActionDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1105366
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->w:I

    packed-switch v0, :pswitch_data_0

    .line 1105367
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105368
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105369
    return-void

    .line 1105370
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/button/FigToggleButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigToggleButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1105371
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public setActionIconColor(I)V
    .locals 1

    .prologue
    .line 1105363
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->w:I

    packed-switch v0, :pswitch_data_0

    .line 1105364
    :goto_0
    return-void

    .line 1105365
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
    .end packed-switch
.end method

.method public setActionOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1105360
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1105361
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1105362
    :cond_0
    return-void
.end method

.method public setActionOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 1105357
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1105358
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1105359
    :cond_0
    return-void
.end method

.method public setActionState(Z)V
    .locals 1

    .prologue
    .line 1105349
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->w:I

    packed-switch v0, :pswitch_data_0

    .line 1105350
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105351
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105352
    return-void

    .line 1105353
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    goto :goto_0

    .line 1105354
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    goto :goto_0

    .line 1105355
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/resources/ui/FbRadioButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    goto :goto_0

    .line 1105356
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/button/FigToggleButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigToggleButton;->setChecked(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setActionText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1105343
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->w:I

    packed-switch v0, :pswitch_data_0

    .line 1105344
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105345
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105346
    return-void

    .line 1105347
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1105348
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public setAuxView(Landroid/view/View;)V
    .locals 0
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1105341
    iput-object p1, p0, Lcom/facebook/fig/listitem/FigListItem;->a:Landroid/view/View;

    .line 1105342
    return-void
.end method

.method public setBodyMaxLines(I)V
    .locals 1

    .prologue
    .line 1105337
    iput p1, p0, Lcom/facebook/fig/listitem/FigListItem;->u:I

    .line 1105338
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->b(I)V

    .line 1105339
    return-void
.end method

.method public setBodyText(I)V
    .locals 2

    .prologue
    .line 1105410
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1105411
    invoke-direct {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContent()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1105412
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105413
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105414
    return-void
.end method

.method public setBodyText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1105415
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1105416
    invoke-direct {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContent()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1105417
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105418
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105419
    return-void
.end method

.method public setBodyTextAppearenceType(I)V
    .locals 3
    .param p1    # I
        .annotation build Lcom/facebook/fig/listitem/annotations/BodyTextAppearenceType;
        .end annotation
    .end param

    .prologue
    .line 1105420
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->p:I

    if-eq p1, v0, :cond_0

    .line 1105421
    iput p1, p0, Lcom/facebook/fig/listitem/FigListItem;->p:I

    .line 1105422
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->m:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1}, LX/6WK;->b(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1105423
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105424
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105425
    :cond_0
    return-void
.end method

.method public setIsActionVisible(Z)V
    .locals 0

    .prologue
    .line 1105426
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 1105427
    return-void
.end method

.method public setMaxLinesFromThumbnailSize(Z)V
    .locals 1

    .prologue
    .line 1105404
    iget-boolean v0, p0, Lcom/facebook/fig/listitem/FigListItem;->r:Z

    if-eq v0, p1, :cond_0

    .line 1105405
    iput-boolean p1, p0, Lcom/facebook/fig/listitem/FigListItem;->r:Z

    .line 1105406
    invoke-direct {p0}, Lcom/facebook/fig/listitem/FigListItem;->e()V

    .line 1105407
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105408
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105409
    :cond_0
    return-void
.end method

.method public setMetaMaxLines(I)V
    .locals 1

    .prologue
    .line 1105428
    iput p1, p0, Lcom/facebook/fig/listitem/FigListItem;->v:I

    .line 1105429
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->b(I)V

    .line 1105430
    return-void
.end method

.method public setMetaText(I)V
    .locals 2

    .prologue
    .line 1105431
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1105432
    invoke-direct {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContent()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1105433
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105434
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105435
    return-void
.end method

.method public setMetaText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1105436
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1105437
    invoke-direct {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContent()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1105438
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105439
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105440
    return-void
.end method

.method public setMetaTextAppearenceType(I)V
    .locals 3
    .param p1    # I
        .annotation build Lcom/facebook/fig/listitem/annotations/MetaTextAppearenceType;
        .end annotation
    .end param

    .prologue
    .line 1105441
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->q:I

    if-eq p1, v0, :cond_0

    .line 1105442
    iput p1, p0, Lcom/facebook/fig/listitem/FigListItem;->q:I

    .line 1105443
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->n:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1}, LX/6WK;->c(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1105444
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105445
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105446
    :cond_0
    return-void
.end method

.method public final setPadding(IIII)V
    .locals 0

    .prologue
    .line 1105447
    return-void
.end method

.method public setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1105448
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->k:I

    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 1105449
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1105450
    return-void
.end method

.method public setThumbnailPadding(I)V
    .locals 0

    .prologue
    .line 1105451
    return-void
.end method

.method public setThumbnailSizeType(I)V
    .locals 1
    .param p1    # I
        .annotation build Lcom/facebook/fig/listitem/Const$ThumbnailSize;
        .end annotation
    .end param

    .prologue
    .line 1105452
    iput p1, p0, Lcom/facebook/fig/listitem/FigListItem;->j:I

    .line 1105453
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getThumbnailDimensionSize()I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/listitem/FigListItem;->k:I

    .line 1105454
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->k:I

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 1105455
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105456
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105457
    return-void
.end method

.method public setTitleMaxLines(I)V
    .locals 1

    .prologue
    .line 1105458
    iput p1, p0, Lcom/facebook/fig/listitem/FigListItem;->t:I

    .line 1105459
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->b(I)V

    .line 1105460
    return-void
.end method

.method public setTitleText(I)V
    .locals 2

    .prologue
    .line 1105461
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1105462
    invoke-direct {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContent()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1105463
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105464
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105465
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1105466
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1105467
    invoke-direct {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContent()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1105468
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105469
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105470
    return-void
.end method

.method public setTitleTextAppearenceType(I)V
    .locals 3
    .param p1    # I
        .annotation build Lcom/facebook/fig/listitem/annotations/TitleTextAppearenceType;
        .end annotation
    .end param

    .prologue
    .line 1105471
    iget v0, p0, Lcom/facebook/fig/listitem/FigListItem;->o:I

    if-eq p1, v0, :cond_0

    .line 1105472
    iput p1, p0, Lcom/facebook/fig/listitem/FigListItem;->o:I

    .line 1105473
    iget-object v0, p0, Lcom/facebook/fig/listitem/FigListItem;->l:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1}, LX/6WK;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1105474
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->requestLayout()V

    .line 1105475
    invoke-virtual {p0}, Lcom/facebook/fig/listitem/FigListItem;->invalidate()V

    .line 1105476
    :cond_0
    return-void
.end method
