.class public Lcom/facebook/fig/button/FigToggleButton;
.super LX/6WG;
.source ""


# static fields
.field private static a:Landroid/util/SparseIntArray;


# instance fields
.field private b:I

.field private c:I

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:I

.field private f:Landroid/content/res/ColorStateList;

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1105270
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 1105271
    sput-object v0, Lcom/facebook/fig/button/FigToggleButton;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x1

    const v2, 0x7f0e02e9

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1105272
    sget-object v0, Lcom/facebook/fig/button/FigToggleButton;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x2

    const v2, 0x7f0e02ea

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1105273
    sget-object v0, Lcom/facebook/fig/button/FigToggleButton;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x4

    const v2, 0x7f0e02eb

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1105274
    sget-object v0, Lcom/facebook/fig/button/FigToggleButton;->a:Landroid/util/SparseIntArray;

    const/16 v1, 0x10

    const v2, 0x7f0e02ed

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1105275
    sget-object v0, Lcom/facebook/fig/button/FigToggleButton;->a:Landroid/util/SparseIntArray;

    const/16 v1, 0x20

    const v2, 0x7f0e02ee

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1105276
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1105194
    invoke-direct {p0, p1}, LX/6WG;-><init>(Landroid/content/Context;)V

    .line 1105195
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/fig/button/FigToggleButton;->a(Landroid/util/AttributeSet;)V

    .line 1105196
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1105268
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fig/button/FigToggleButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1105269
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1105263
    invoke-direct {p0, p1, p2}, LX/6WG;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1105264
    invoke-direct {p0, p2}, Lcom/facebook/fig/button/FigToggleButton;->a(Landroid/util/AttributeSet;)V

    .line 1105265
    if-eqz p2, :cond_0

    .line 1105266
    invoke-direct {p0, p1, p2}, Lcom/facebook/fig/button/FigToggleButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1105267
    :cond_0
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 1105258
    iget-object v0, p0, Lcom/facebook/fig/button/FigToggleButton;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1105259
    iget-object v0, p0, Lcom/facebook/fig/button/FigToggleButton;->d:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/facebook/fig/button/FigToggleButton;->g:I

    iget v2, p0, Lcom/facebook/fig/button/FigToggleButton;->g:I

    iget v3, p0, Lcom/facebook/fig/button/FigToggleButton;->g:I

    iget v4, p0, Lcom/facebook/fig/button/FigToggleButton;->e:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/facebook/fig/button/FigToggleButton;->g:I

    iget v5, p0, Lcom/facebook/fig/button/FigToggleButton;->e:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1105260
    iget-object v0, p0, Lcom/facebook/fig/button/FigToggleButton;->d:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/fig/button/FigToggleButton;->f:Landroid/content/res/ColorStateList;

    invoke-static {v0, v1}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 1105261
    iget-object v0, p0, Lcom/facebook/fig/button/FigToggleButton;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/fig/button/FigToggleButton;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1105262
    :cond_0
    return-void
.end method

.method private a(I[I)V
    .locals 6
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param
    .param p2    # [I
        .annotation build Landroid/support/annotation/StyleableRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v5, -0x1

    .line 1105239
    iput-boolean v0, p0, LX/6WG;->a:Z

    .line 1105240
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigToggleButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1105241
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v2

    .line 1105242
    :goto_0
    if-ge v0, v2, :cond_4

    .line 1105243
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v3

    .line 1105244
    const/16 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 1105245
    invoke-virtual {v1, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/facebook/fig/button/FigToggleButton;->b:I

    .line 1105246
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1105247
    :cond_1
    const/16 v4, 0x0

    if-ne v3, v4, :cond_2

    .line 1105248
    invoke-virtual {v1, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-static {p0, v3}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1105249
    :cond_2
    const/16 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 1105250
    invoke-virtual {v1, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/facebook/fig/button/FigToggleButton;->e:I

    goto :goto_1

    .line 1105251
    :cond_3
    const/16 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 1105252
    invoke-virtual {v1, v3}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/fig/button/FigToggleButton;->f:Landroid/content/res/ColorStateList;

    goto :goto_1

    .line 1105253
    :cond_4
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1105254
    iget v0, p0, Lcom/facebook/fig/button/FigToggleButton;->b:I

    iget v1, p0, Lcom/facebook/fig/button/FigToggleButton;->e:I

    sub-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/fig/button/FigToggleButton;->g:I

    .line 1105255
    const/4 v0, 0x1

    .line 1105256
    iput-boolean v0, p0, LX/6WG;->a:Z

    .line 1105257
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1105231
    if-eqz p2, :cond_0

    .line 1105232
    sget-object v0, LX/03r;->FigToggleButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1105233
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1105234
    const/16 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/facebook/fig/button/FigToggleButton;->setType(I)V

    .line 1105235
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/fig/button/FigToggleButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 1105236
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1105237
    :cond_0
    return-void

    .line 1105238
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The toggleButtonType attribute needs to be set via XML"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1105223
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/button/FigToggleButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1105224
    if-eqz p1, :cond_1

    .line 1105225
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigToggleButton;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->FigToggleButton:[I

    invoke-virtual {v0, p1, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1105226
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1105227
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1105228
    invoke-virtual {p0, v1}, Lcom/facebook/fig/button/FigToggleButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 1105229
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1105230
    :cond_1
    return-void
.end method

.method public static a(I)[I
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 1105222
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/fig/button/FigToggleButton;->a:Landroid/util/SparseIntArray;

    and-int/lit8 v3, p0, 0xf

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/facebook/fig/button/FigToggleButton;->a:Landroid/util/SparseIntArray;

    and-int/lit16 v3, p0, 0xff0

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v2

    aput v2, v0, v1

    return-object v0
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 1105277
    sparse-switch p0, :sswitch_data_0

    .line 1105278
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1105279
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x12 -> :sswitch_0
        0x14 -> :sswitch_0
        0x21 -> :sswitch_0
    .end sparse-switch
.end method

.method private c(I)V
    .locals 5

    .prologue
    .line 1105219
    invoke-static {p1}, Lcom/facebook/fig/button/FigToggleButton;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1105220
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "%s doesn\'t support the supplied type: 0x%X"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1105221
    :cond_0
    return-void
.end method


# virtual methods
.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 1105215
    invoke-super {p0}, LX/6WG;->drawableStateChanged()V

    .line 1105216
    iget-object v0, p0, Lcom/facebook/fig/button/FigToggleButton;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1105217
    iget-object v0, p0, Lcom/facebook/fig/button/FigToggleButton;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/fig/button/FigToggleButton;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1105218
    :cond_0
    return-void
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 1105214
    iget v0, p0, Lcom/facebook/fig/button/FigToggleButton;->c:I

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1105210
    invoke-super {p0, p1}, LX/6WG;->onDraw(Landroid/graphics/Canvas;)V

    .line 1105211
    iget-object v0, p0, Lcom/facebook/fig/button/FigToggleButton;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1105212
    iget-object v0, p0, Lcom/facebook/fig/button/FigToggleButton;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1105213
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x79472c5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1105207
    iget v1, p0, Lcom/facebook/fig/button/FigToggleButton;->b:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1105208
    invoke-super {p0, v1, v1}, LX/6WG;->onMeasure(II)V

    .line 1105209
    const/16 v1, 0x2d

    const v2, -0x6f10ade3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setGlyph(I)V
    .locals 1

    .prologue
    .line 1105204
    if-lez p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/fig/button/FigToggleButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fig/button/FigToggleButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 1105205
    return-void

    .line 1105206
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setGlyph(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1105197
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fig/button/FigToggleButton;->d:Landroid/graphics/drawable/Drawable;

    .line 1105198
    if-eqz p1, :cond_0

    .line 1105199
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fig/button/FigToggleButton;->d:Landroid/graphics/drawable/Drawable;

    .line 1105200
    invoke-direct {p0}, Lcom/facebook/fig/button/FigToggleButton;->a()V

    .line 1105201
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigToggleButton;->requestLayout()V

    .line 1105202
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigToggleButton;->invalidate()V

    .line 1105203
    return-void
.end method

.method public setType(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1105186
    invoke-direct {p0, p1}, Lcom/facebook/fig/button/FigToggleButton;->c(I)V

    .line 1105187
    iput p1, p0, Lcom/facebook/fig/button/FigToggleButton;->c:I

    .line 1105188
    sget-object v0, Lcom/facebook/fig/button/FigToggleButton;->a:Landroid/util/SparseIntArray;

    and-int/lit8 v1, p1, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    sget-object v1, LX/03r;->FigToggleButtonAttrs:[I

    invoke-direct {p0, v0, v1}, Lcom/facebook/fig/button/FigToggleButton;->a(I[I)V

    .line 1105189
    sget-object v0, Lcom/facebook/fig/button/FigToggleButton;->a:Landroid/util/SparseIntArray;

    and-int/lit16 v1, p1, 0xff0

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    sget-object v1, LX/03r;->FigToggleButtonAttrs:[I

    invoke-direct {p0, v0, v1}, Lcom/facebook/fig/button/FigToggleButton;->a(I[I)V

    .line 1105190
    invoke-direct {p0}, Lcom/facebook/fig/button/FigToggleButton;->a()V

    .line 1105191
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigToggleButton;->invalidate()V

    .line 1105192
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigToggleButton;->requestLayout()V

    .line 1105193
    return-void
.end method
