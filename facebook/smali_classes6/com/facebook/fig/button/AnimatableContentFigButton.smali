.class public Lcom/facebook/fig/button/AnimatableContentFigButton;
.super Lcom/facebook/fig/button/FigButton;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1104944
    invoke-direct {p0, p1}, Lcom/facebook/fig/button/FigButton;-><init>(Landroid/content/Context;)V

    .line 1104945
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1104942
    invoke-direct {p0, p1, p2}, Lcom/facebook/fig/button/FigButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1104943
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1104940
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fig/button/FigButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1104941
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 1104936
    iget-object v0, p0, Lcom/facebook/fig/button/FigButton;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1104937
    iget-object v0, p0, Lcom/facebook/fig/button/FigButton;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1104938
    invoke-virtual {p0}, Lcom/facebook/fig/button/AnimatableContentFigButton;->invalidate()V

    .line 1104939
    :cond_0
    return-void
.end method

.method public setTextColor(I)V
    .locals 2

    .prologue
    .line 1104930
    iget-boolean v0, p0, LX/6WF;->a:Z

    move v0, v0

    .line 1104931
    const/4 v1, 0x0

    .line 1104932
    iput-boolean v1, p0, LX/6WF;->a:Z

    .line 1104933
    invoke-super {p0, p1}, Lcom/facebook/fig/button/FigButton;->setTextColor(I)V

    .line 1104934
    iput-boolean v0, p0, LX/6WF;->a:Z

    .line 1104935
    return-void
.end method
