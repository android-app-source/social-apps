.class public Lcom/facebook/fig/button/FigButton;
.super LX/6WF;
.source ""


# static fields
.field private static j:Landroid/util/SparseIntArray;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:Landroid/content/res/ColorStateList;

.field public h:Landroid/graphics/drawable/Drawable;

.field public i:Landroid/graphics/Rect;

.field public k:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:Ljava/lang/CharSequence;

.field private m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1104845
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 1104846
    sput-object v0, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    const/4 v1, 0x1

    const v2, 0x7f0e02d6

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1104847
    sget-object v0, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    const/4 v1, 0x2

    const v2, 0x7f0e02d7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1104848
    sget-object v0, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    const/4 v1, 0x4

    const v2, 0x7f0e02d8

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1104849
    sget-object v0, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x8

    const v2, 0x7f0e02d9

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1104850
    sget-object v0, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x10

    const v2, 0x7f0e02de

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1104851
    sget-object v0, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x20

    const v2, 0x7f0e02dc

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1104852
    sget-object v0, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x40

    const v2, 0x7f0e02dd

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1104853
    sget-object v0, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x80

    const v2, 0x7f0e02e0

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1104854
    sget-object v0, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x100

    const v2, 0x7f0e02e1

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1104855
    sget-object v0, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x200

    const v2, 0x7f0e02e2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1104856
    sget-object v0, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x400

    const v2, 0x7f0e02e3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 1104857
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1104858
    invoke-direct {p0, p1}, LX/6WF;-><init>(Landroid/content/Context;)V

    .line 1104859
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/button/FigButton;->i:Landroid/graphics/Rect;

    .line 1104860
    invoke-direct {p0}, Lcom/facebook/fig/button/FigButton;->b()V

    .line 1104861
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1104862
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fig/button/FigButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1104863
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1104864
    invoke-direct {p0, p1, p2}, LX/6WF;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1104865
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/button/FigButton;->i:Landroid/graphics/Rect;

    .line 1104866
    invoke-direct {p0}, Lcom/facebook/fig/button/FigButton;->b()V

    .line 1104867
    if-eqz p2, :cond_0

    .line 1104868
    invoke-direct {p0, p1, p2}, Lcom/facebook/fig/button/FigButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1104869
    :cond_0
    return-void
.end method

.method private a(I[I)V
    .locals 6
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param
    .param p2    # [I
        .annotation build Landroid/support/annotation/StyleableRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1104870
    iput-boolean v1, p0, LX/6WF;->a:Z

    .line 1104871
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 1104872
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    move v0, v1

    .line 1104873
    :goto_0
    if-ge v0, v3, :cond_8

    .line 1104874
    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    .line 1104875
    const/16 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 1104876
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/facebook/fig/button/FigButton;->e:I

    .line 1104877
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1104878
    :cond_1
    const/16 v5, 0x3

    if-ne v4, v5, :cond_2

    .line 1104879
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/facebook/fig/button/FigButton;->f:I

    goto :goto_1

    .line 1104880
    :cond_2
    const/16 v5, 0x5

    if-ne v4, v5, :cond_3

    .line 1104881
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p0, v1, v4}, Lcom/facebook/fig/button/FigButton;->setTextSize(IF)V

    goto :goto_1

    .line 1104882
    :cond_3
    const/16 v5, 0x8

    if-ne v4, v5, :cond_4

    .line 1104883
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/facebook/fig/button/FigButton;->d:I

    goto :goto_1

    .line 1104884
    :cond_4
    const/16 v5, 0x0

    if-ne v4, v5, :cond_5

    .line 1104885
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/facebook/fig/button/FigButton;->b:I

    goto :goto_1

    .line 1104886
    :cond_5
    const/16 v5, 0x1

    if-ne v4, v5, :cond_6

    .line 1104887
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-static {p0, v4}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1104888
    :cond_6
    const/16 v5, 0x4

    if-ne v4, v5, :cond_7

    .line 1104889
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/fig/button/FigButton;->g:Landroid/content/res/ColorStateList;

    .line 1104890
    iget-object v4, p0, Lcom/facebook/fig/button/FigButton;->g:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v4}, Lcom/facebook/fig/button/FigButton;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_1

    .line 1104891
    :cond_7
    const/16 v5, 0x6

    if-ne v4, v5, :cond_0

    .line 1104892
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/facebook/fig/button/FigButton;->c:I

    goto :goto_1

    .line 1104893
    :cond_8
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 1104894
    const/4 v0, 0x1

    .line 1104895
    iput-boolean v0, p0, LX/6WF;->a:Z

    .line 1104896
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1104897
    sget-object v0, LX/03r;->FigButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1104898
    const/16 v1, 0x2

    const/16 v2, 0x408

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 1104899
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/fig/button/FigButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 1104900
    const/16 v1, 0x0

    invoke-static {p1, v0, v1}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/fig/button/FigButton;->l:Ljava/lang/CharSequence;

    .line 1104901
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1104902
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fig/button/FigButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/fig/button/FigButton;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    iput-object v0, p0, Lcom/facebook/fig/button/FigButton;->k:LX/23P;

    return-void
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 1104927
    sparse-switch p0, :sswitch_data_0

    .line 1104928
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1104929
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x12 -> :sswitch_0
        0x18 -> :sswitch_0
        0x24 -> :sswitch_0
        0x28 -> :sswitch_0
        0x44 -> :sswitch_0
        0x48 -> :sswitch_0
        0x81 -> :sswitch_0
        0x82 -> :sswitch_0
        0x88 -> :sswitch_0
        0x101 -> :sswitch_0
        0x102 -> :sswitch_0
        0x108 -> :sswitch_0
        0x201 -> :sswitch_0
        0x202 -> :sswitch_0
        0x208 -> :sswitch_0
        0x401 -> :sswitch_0
        0x402 -> :sswitch_0
        0x408 -> :sswitch_0
    .end sparse-switch
.end method

.method private b()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1104903
    const-class v0, Lcom/facebook/fig/button/FigButton;

    invoke-static {v0, p0}, Lcom/facebook/fig/button/FigButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1104904
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/fig/button/FigButton;->m:Z

    .line 1104905
    const/16 v0, 0x11

    invoke-super {p0, v0}, LX/6WF;->setGravity(I)V

    .line 1104906
    invoke-super {p0, v1}, LX/6WF;->setLines(I)V

    .line 1104907
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-super {p0, v0}, LX/6WF;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1104908
    iget-object v0, p0, Lcom/facebook/fig/button/FigButton;->k:LX/23P;

    invoke-super {p0, v0}, LX/6WF;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1104909
    invoke-super {p0, v2, v2, v2, v2}, LX/6WF;->setPadding(IIII)V

    .line 1104910
    invoke-virtual {p0, v2}, Lcom/facebook/fig/button/FigButton;->setIncludeFontPadding(Z)V

    .line 1104911
    iput-boolean v1, p0, LX/6WF;->a:Z

    .line 1104912
    return-void

    :cond_0
    move v0, v2

    .line 1104913
    goto :goto_0
.end method

.method public static b(I)[I
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 1104914
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    and-int/lit8 v3, p0, 0xf

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    and-int/lit16 v3, p0, 0xff0

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v2

    aput v2, v0, v1

    return-object v0
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 1104915
    invoke-static {p1}, Lcom/facebook/fig/button/FigButton;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1104916
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " doesn\'t support the supplied type: 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1104917
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, LX/0zj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1104918
    :cond_0
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 1104919
    iget-object v0, p0, Lcom/facebook/fig/button/FigButton;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1104920
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 1104921
    if-nez v1, :cond_1

    .line 1104922
    :cond_0
    :goto_0
    return v0

    .line 1104923
    :cond_1
    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    .line 1104924
    if-lez v2, :cond_0

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d(I)Z
    .locals 1

    .prologue
    .line 1104925
    iget v0, p0, Lcom/facebook/fig/button/FigButton;->a:I

    and-int/2addr v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 1104926
    const/16 v0, 0x60

    invoke-direct {p0, v0}, Lcom/facebook/fig/button/FigButton;->d(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(I)Z
    .locals 1

    .prologue
    .line 1104843
    iget v0, p0, Lcom/facebook/fig/button/FigButton;->a:I

    and-int/2addr v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 1104844
    const/16 v0, 0xf0

    invoke-direct {p0, v0}, Lcom/facebook/fig/button/FigButton;->d(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/fig/button/FigButton;->e(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1104756
    iget-object v0, p0, Lcom/facebook/fig/button/FigButton;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1104757
    iget-object v0, p0, Lcom/facebook/fig/button/FigButton;->h:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/fig/button/FigButton;->g:Landroid/content/res/ColorStateList;

    invoke-static {v0, v1}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 1104758
    iget-object v0, p0, Lcom/facebook/fig/button/FigButton;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1104759
    :cond_0
    return-void
.end method

.method private getGlyph()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1104760
    iget-object v0, p0, Lcom/facebook/fig/button/FigButton;->h:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1104761
    invoke-virtual {p0, p1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 1104762
    iput-object p2, p0, Lcom/facebook/fig/button/FigButton;->l:Ljava/lang/CharSequence;

    .line 1104763
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->requestLayout()V

    .line 1104764
    return-void
.end method

.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 1104765
    invoke-super {p0}, LX/6WF;->drawableStateChanged()V

    .line 1104766
    invoke-direct {p0}, Lcom/facebook/fig/button/FigButton;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1104767
    iget-object v0, p0, Lcom/facebook/fig/button/FigButton;->h:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 1104768
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1104769
    :cond_0
    return-void
.end method

.method public getCompoundPaddingEnd()I
    .locals 1

    .prologue
    .line 1104770
    iget v0, p0, Lcom/facebook/fig/button/FigButton;->f:I

    return v0
.end method

.method public getCompoundPaddingLeft()I
    .locals 1

    .prologue
    .line 1104771
    iget-boolean v0, p0, Lcom/facebook/fig/button/FigButton;->m:Z

    if-eqz v0, :cond_0

    .line 1104772
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getCompoundPaddingStart()I

    move-result v0

    .line 1104773
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getCompoundPaddingEnd()I

    move-result v0

    goto :goto_0
.end method

.method public getCompoundPaddingRight()I
    .locals 1

    .prologue
    .line 1104774
    iget-boolean v0, p0, Lcom/facebook/fig/button/FigButton;->m:Z

    if-eqz v0, :cond_0

    .line 1104775
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getCompoundPaddingEnd()I

    move-result v0

    .line 1104776
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getCompoundPaddingStart()I

    move-result v0

    goto :goto_0
.end method

.method public getCompoundPaddingStart()I
    .locals 2

    .prologue
    .line 1104777
    invoke-direct {p0}, Lcom/facebook/fig/button/FigButton;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/fig/button/FigButton;->e:I

    iget v1, p0, Lcom/facebook/fig/button/FigButton;->c:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/fig/button/FigButton;->d:I

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/facebook/fig/button/FigButton;->e:I

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 1104778
    iget v0, p0, Lcom/facebook/fig/button/FigButton;->a:I

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1104779
    invoke-super {p0, p1}, LX/6WF;->onDraw(Landroid/graphics/Canvas;)V

    .line 1104780
    invoke-direct {p0}, Lcom/facebook/fig/button/FigButton;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1104781
    iget-object v0, p0, Lcom/facebook/fig/button/FigButton;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1104782
    :cond_0
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1104783
    invoke-super/range {p0 .. p5}, LX/6WF;->onLayout(ZIIII)V

    .line 1104784
    invoke-direct {p0}, Lcom/facebook/fig/button/FigButton;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1104785
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 1104786
    invoke-virtual {v0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1104787
    invoke-static {}, LX/0zj;->a()Landroid/graphics/Rect;

    move-result-object v3

    .line 1104788
    invoke-static {v1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v4

    .line 1104789
    invoke-virtual {v0}, Landroid/text/Layout;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1104790
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 1104791
    iget v1, p0, Lcom/facebook/fig/button/FigButton;->c:I

    iget v4, p0, Lcom/facebook/fig/button/FigButton;->d:I

    add-int/2addr v1, v4

    add-int/2addr v1, v0

    .line 1104792
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getHeight()I

    move-result v4

    invoke-virtual {v3, v2, v2, v0, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1104793
    const/16 v0, 0x11

    iget-object v6, p0, Lcom/facebook/fig/button/FigButton;->i:Landroid/graphics/Rect;

    move v4, v2

    move v5, v2

    invoke-static/range {v0 .. v6}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;IILandroid/graphics/Rect;)V

    .line 1104794
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/facebook/fig/button/FigButton;->c:I

    sub-int/2addr v0, v1

    shr-int/lit8 v1, v0, 0x1

    .line 1104795
    iget-boolean v0, p0, Lcom/facebook/fig/button/FigButton;->m:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/fig/button/FigButton;->i:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 1104796
    :goto_0
    iget-object v2, p0, Lcom/facebook/fig/button/FigButton;->h:Landroid/graphics/drawable/Drawable;

    iget v3, p0, Lcom/facebook/fig/button/FigButton;->c:I

    add-int/2addr v3, v0

    iget v4, p0, Lcom/facebook/fig/button/FigButton;->c:I

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1104797
    :cond_0
    return-void

    .line 1104798
    :cond_1
    iget-object v0, p0, Lcom/facebook/fig/button/FigButton;->i:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/facebook/fig/button/FigButton;->c:I

    sub-int/2addr v0, v2

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, -0x23e45c7c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1104799
    iget v1, p0, Lcom/facebook/fig/button/FigButton;->b:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1104800
    invoke-super {p0, p1, v1}, LX/6WF;->onMeasure(II)V

    .line 1104801
    iget-object v2, p0, Lcom/facebook/fig/button/FigButton;->l:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/facebook/fig/button/FigButton;->l:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1104802
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x4624e987

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1104803
    :goto_0
    return-void

    .line 1104804
    :cond_1
    invoke-direct {p0}, Lcom/facebook/fig/button/FigButton;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1104805
    iget-object v2, p0, Lcom/facebook/fig/button/FigButton;->l:Ljava/lang/CharSequence;

    invoke-virtual {p0, v2}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 1104806
    invoke-virtual {p0, p1, v1}, Lcom/facebook/fig/button/FigButton;->measure(II)V

    .line 1104807
    :cond_2
    const v1, -0x6912e23

    invoke-static {v1, v0}, LX/02F;->g(II)V

    goto :goto_0
.end method

.method public setGlyph(I)V
    .locals 1

    .prologue
    .line 1104808
    if-lez p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fig/button/FigButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 1104809
    return-void

    .line 1104810
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setGlyph(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1104811
    iget-object v0, p0, Lcom/facebook/fig/button/FigButton;->h:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    .line 1104812
    :goto_0
    return-void

    .line 1104813
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fig/button/FigButton;->h:Landroid/graphics/drawable/Drawable;

    .line 1104814
    invoke-direct {p0}, Lcom/facebook/fig/button/FigButton;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1104815
    const/4 v0, 0x0

    .line 1104816
    iput-boolean v0, p0, LX/6WF;->a:Z

    .line 1104817
    if-eqz p1, :cond_1

    .line 1104818
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fig/button/FigButton;->h:Landroid/graphics/drawable/Drawable;

    .line 1104819
    iget v0, p0, Lcom/facebook/fig/button/FigButton;->d:I

    invoke-virtual {p0, v0}, Lcom/facebook/fig/button/FigButton;->setCompoundDrawablePadding(I)V

    .line 1104820
    invoke-direct {p0}, Lcom/facebook/fig/button/FigButton;->g()V

    .line 1104821
    :cond_1
    const/4 v0, 0x1

    .line 1104822
    iput-boolean v0, p0, LX/6WF;->a:Z

    .line 1104823
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->requestLayout()V

    .line 1104824
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->invalidate()V

    goto :goto_0
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 1104825
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/facebook/fig/button/FigButton;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1104826
    const/4 v0, -0x2

    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1104827
    :cond_0
    invoke-super {p0, p1}, LX/6WF;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1104828
    return-void
.end method

.method public setShorterText(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1104829
    iput-object p1, p0, Lcom/facebook/fig/button/FigButton;->l:Ljava/lang/CharSequence;

    .line 1104830
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->requestLayout()V

    .line 1104831
    return-void
.end method

.method public final setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1

    .prologue
    .line 1104832
    invoke-super {p0, p1, p2}, LX/6WF;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 1104833
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fig/button/FigButton;->l:Ljava/lang/CharSequence;

    .line 1104834
    return-void
.end method

.method public setType(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1104835
    invoke-direct {p0, p1}, Lcom/facebook/fig/button/FigButton;->c(I)V

    .line 1104836
    iput p1, p0, Lcom/facebook/fig/button/FigButton;->a:I

    .line 1104837
    sget-object v0, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    and-int/lit8 v1, p1, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    sget-object v1, LX/03r;->FigButtonAttrs:[I

    invoke-direct {p0, v0, v1}, Lcom/facebook/fig/button/FigButton;->a(I[I)V

    .line 1104838
    sget-object v0, Lcom/facebook/fig/button/FigButton;->j:Landroid/util/SparseIntArray;

    and-int/lit16 v1, p1, 0xff0

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    sget-object v1, LX/03r;->FigButtonAttrs:[I

    invoke-direct {p0, v0, v1}, Lcom/facebook/fig/button/FigButton;->a(I[I)V

    .line 1104839
    invoke-direct {p0}, Lcom/facebook/fig/button/FigButton;->g()V

    .line 1104840
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->requestLayout()V

    .line 1104841
    invoke-virtual {p0}, Lcom/facebook/fig/button/FigButton;->invalidate()V

    .line 1104842
    return-void
.end method
