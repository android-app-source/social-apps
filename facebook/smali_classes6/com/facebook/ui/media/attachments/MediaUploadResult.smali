.class public Lcom/facebook/ui/media/attachments/MediaUploadResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:[B
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1035934
    new-instance v0, LX/5zu;

    invoke-direct {v0}, LX/5zu;-><init>()V

    sput-object v0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1035935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1035936
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->a:Ljava/lang/String;

    .line 1035937
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->b:[B

    .line 1035938
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->c:Ljava/lang/String;

    .line 1035939
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->d:Ljava/lang/Long;

    .line 1035940
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1035941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1035942
    iput-object p1, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->a:Ljava/lang/String;

    .line 1035943
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->b:[B

    .line 1035944
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->c:Ljava/lang/String;

    .line 1035945
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->d:Ljava/lang/Long;

    .line 1035946
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BLjava/lang/String;J)V
    .locals 2

    .prologue
    .line 1035947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1035948
    array-length v0, p2

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1035949
    iput-object p1, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->a:Ljava/lang/String;

    .line 1035950
    iput-object p2, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->b:[B

    .line 1035951
    iput-object p3, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->c:Ljava/lang/String;

    .line 1035952
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->d:Ljava/lang/Long;

    .line 1035953
    return-void

    .line 1035954
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1035955
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1035956
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1035957
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1035958
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->b:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1035959
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1035960
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->d:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1035961
    return-void
.end method
