.class public Lcom/facebook/ui/media/attachments/MediaResource;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Landroid/graphics/RectF;


# instance fields
.field public final A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final B:Landroid/net/Uri;

.field public final C:Z

.field public final D:J

.field public final E:Z

.field public final F:Z

.field public final G:Ljava/lang/String;

.field public final H:Ljava/lang/String;

.field public final c:Landroid/net/Uri;

.field public final d:LX/2MK;

.field public final e:LX/5zj;

.field public final f:LX/5zi;

.field public final g:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:J

.field public final i:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final j:J

.field public final k:I

.field public final l:I

.field public final m:LX/47d;

.field public final n:Z

.field public final o:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final s:J

.field public final t:Landroid/graphics/RectF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final u:Z

.field public final v:I

.field public final w:I

.field public final x:Lcom/facebook/ui/media/attachments/MediaUploadResult;

.field public final y:Z

.field public final z:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1035660
    new-instance v0, LX/5zg;

    invoke-direct {v0}, LX/5zg;-><init>()V

    sput-object v0, Lcom/facebook/ui/media/attachments/MediaResource;->a:Ljava/util/Comparator;

    .line 1035661
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, Lcom/facebook/ui/media/attachments/MediaResource;->b:Landroid/graphics/RectF;

    .line 1035662
    new-instance v0, LX/5zh;

    invoke-direct {v0}, LX/5zh;-><init>()V

    sput-object v0, Lcom/facebook/ui/media/attachments/MediaResource;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5zn;)V
    .locals 4

    .prologue
    .line 1035594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1035595
    iget-object v0, p1, LX/5zn;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 1035596
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 1035597
    iget-object v0, p1, LX/5zn;->c:LX/2MK;

    move-object v0, v0

    .line 1035598
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2MK;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    .line 1035599
    iget-object v0, p1, LX/5zn;->d:LX/5zj;

    move-object v0, v0

    .line 1035600
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5zj;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    .line 1035601
    iget-object v0, p1, LX/5zn;->e:LX/5zi;

    move-object v0, v0

    .line 1035602
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->f:LX/5zi;

    .line 1035603
    iget-object v0, p1, LX/5zn;->f:Landroid/net/Uri;

    move-object v0, v0

    .line 1035604
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->g:Landroid/net/Uri;

    .line 1035605
    iget-wide v2, p1, LX/5zn;->h:J

    move-wide v0, v2

    .line 1035606
    iput-wide v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    .line 1035607
    iget-object v0, p1, LX/5zn;->g:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v0, v0

    .line 1035608
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1035609
    iget-wide v2, p1, LX/5zn;->i:J

    move-wide v0, v2

    .line 1035610
    iput-wide v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    .line 1035611
    iget v0, p1, LX/5zn;->j:I

    move v0, v0

    .line 1035612
    iput v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    .line 1035613
    iget v0, p1, LX/5zn;->k:I

    move v0, v0

    .line 1035614
    iput v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    .line 1035615
    iget-object v0, p1, LX/5zn;->l:LX/47d;

    move-object v0, v0

    .line 1035616
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->m:LX/47d;

    .line 1035617
    iget-boolean v0, p1, LX/5zn;->m:Z

    move v0, v0

    .line 1035618
    iput-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->n:Z

    .line 1035619
    iget-object v0, p1, LX/5zn;->n:Landroid/net/Uri;

    move-object v0, v0

    .line 1035620
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    .line 1035621
    iget-object v0, p1, LX/5zn;->o:Ljava/lang/String;

    move-object v0, v0

    .line 1035622
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    .line 1035623
    iget-object v0, p1, LX/5zn;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 1035624
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1035625
    iget-object v0, p1, LX/5zn;->q:Ljava/lang/String;

    move-object v0, v0

    .line 1035626
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    .line 1035627
    iget-wide v2, p1, LX/5zn;->r:J

    move-wide v0, v2

    .line 1035628
    iput-wide v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    .line 1035629
    iget-object v0, p1, LX/5zn;->s:Landroid/graphics/RectF;

    move-object v0, v0

    .line 1035630
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    .line 1035631
    iget-boolean v0, p1, LX/5zn;->t:Z

    move v0, v0

    .line 1035632
    iput-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->u:Z

    .line 1035633
    iget v0, p1, LX/5zn;->u:I

    move v0, v0

    .line 1035634
    iput v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    .line 1035635
    iget v0, p1, LX/5zn;->v:I

    move v0, v0

    .line 1035636
    iput v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    .line 1035637
    iget-object v0, p1, LX/5zn;->w:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    move-object v0, v0

    .line 1035638
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->x:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 1035639
    iget-boolean v0, p1, LX/5zn;->x:Z

    move v0, v0

    .line 1035640
    iput-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->y:Z

    .line 1035641
    iget-object v0, p1, LX/5zn;->a:Ljava/util/Map;

    move-object v0, v0

    .line 1035642
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->z:LX/0P1;

    .line 1035643
    iget-object v0, p1, LX/5zn;->y:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-object v0, v0

    .line 1035644
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1035645
    iget-object v0, p1, LX/5zn;->z:Landroid/net/Uri;

    move-object v0, v0

    .line 1035646
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->B:Landroid/net/Uri;

    .line 1035647
    iget-boolean v0, p1, LX/5zn;->A:Z

    move v0, v0

    .line 1035648
    iput-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->C:Z

    .line 1035649
    iget-wide v2, p1, LX/5zn;->B:J

    move-wide v0, v2

    .line 1035650
    iput-wide v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->D:J

    .line 1035651
    iget-boolean v0, p1, LX/5zn;->C:Z

    move v0, v0

    .line 1035652
    iput-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->E:Z

    .line 1035653
    iget-boolean v0, p1, LX/5zn;->D:Z

    move v0, v0

    .line 1035654
    iput-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->F:Z

    .line 1035655
    iget-object v0, p1, LX/5zn;->E:Ljava/lang/String;

    move-object v0, v0

    .line 1035656
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->G:Ljava/lang/String;

    .line 1035657
    iget-object v0, p1, LX/5zn;->F:Ljava/lang/String;

    move-object v0, v0

    .line 1035658
    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->H:Ljava/lang/String;

    .line 1035659
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 1035558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1035559
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 1035560
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2MK;->valueOf(Ljava/lang/String;)LX/2MK;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    .line 1035561
    invoke-static {}, LX/5zj;->values()[LX/5zj;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    .line 1035562
    invoke-static {}, LX/5zi;->values()[LX/5zi;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->f:LX/5zi;

    .line 1035563
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->g:Landroid/net/Uri;

    .line 1035564
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    .line 1035565
    const-class v0, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1035566
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    .line 1035567
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    .line 1035568
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    .line 1035569
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/47d;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->m:LX/47d;

    .line 1035570
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->n:Z

    .line 1035571
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    .line 1035572
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    .line 1035573
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1035574
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    .line 1035575
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    .line 1035576
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    .line 1035577
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->u:Z

    .line 1035578
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    .line 1035579
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    .line 1035580
    const-class v0, Lcom/facebook/ui/media/attachments/MediaUploadResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaUploadResult;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->x:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 1035581
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->y:Z

    .line 1035582
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->z:LX/0P1;

    .line 1035583
    const-class v0, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1035584
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->B:Landroid/net/Uri;

    .line 1035585
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->C:Z

    .line 1035586
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->D:J

    .line 1035587
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->E:Z

    .line 1035588
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->F:Z

    .line 1035589
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->G:Ljava/lang/String;

    .line 1035590
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->H:Ljava/lang/String;

    .line 1035591
    return-void

    :cond_0
    move v0, v2

    .line 1035592
    goto/16 :goto_0

    :cond_1
    move v1, v2

    .line 1035593
    goto :goto_1
.end method

.method public static a()LX/5zn;
    .locals 1

    .prologue
    .line 1035557
    new-instance v0, LX/5zn;

    invoke-direct {v0}, LX/5zn;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1035553
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->x:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    if-eqz v0, :cond_0

    .line 1035554
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->x:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 1035555
    iget-object p0, v0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1035556
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 1035504
    iget-wide v4, p0, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    .line 1035505
    iget v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    if-ltz v0, :cond_1

    .line 1035506
    iget v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    int-to-long v0, v0

    .line 1035507
    :goto_0
    iget v6, p0, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    if-ltz v6, :cond_0

    .line 1035508
    iget v4, p0, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    int-to-long v4, v4

    .line 1035509
    :cond_0
    sub-long v0, v4, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1035510
    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1035552
    const-string v0, "image/gif"

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "image/webp"

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1035551
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1035547
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;

    if-nez v1, :cond_1

    .line 1035548
    :cond_0
    :goto_0
    return v0

    .line 1035549
    :cond_1
    check-cast p1, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1035550
    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->f:LX/5zi;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->f:LX/5zi;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->g:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->g:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->m:LX/47d;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->m:LX/47d;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->n:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->u:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->u:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->x:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->x:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->y:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->y:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->z:LX/0P1;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->z:LX/0P1;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->B:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->B:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->C:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->C:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->D:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->D:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->E:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->E:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->F:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->F:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->G:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->G:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->H:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->H:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1035546
    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->f:LX/5zi;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->g:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->m:LX/47d;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-boolean v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->u:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->x:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-boolean v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->y:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x17

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->z:LX/0P1;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->B:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    iget-boolean v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->C:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->D:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    iget-boolean v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->E:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    iget-boolean v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->F:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->G:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->H:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1035511
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1035512
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v0}, LX/2MK;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1035513
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v0}, LX/5zj;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1035514
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->f:LX/5zi;

    invoke-virtual {v0}, LX/5zi;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1035515
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->g:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1035516
    iget-wide v4, p0, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1035517
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1035518
    iget-wide v4, p0, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1035519
    iget v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1035520
    iget v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1035521
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->m:LX/47d;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1035522
    iget-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->n:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1035523
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1035524
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1035525
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1035526
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1035527
    iget-wide v4, p0, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1035528
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1035529
    iget-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->u:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1035530
    iget v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1035531
    iget v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1035532
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->x:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1035533
    iget-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->y:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1035534
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->z:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1035535
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1035536
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->B:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1035537
    iget-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->C:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1035538
    iget-wide v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->D:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1035539
    iget-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->E:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1035540
    iget-boolean v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->F:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1035541
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->G:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1035542
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->H:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1035543
    return-void

    :cond_0
    move v0, v2

    .line 1035544
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1035545
    goto :goto_1
.end method
