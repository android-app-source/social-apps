.class public interface abstract annotation Lcom/facebook/annotationprocessors/modelgen/iface/ModelField;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/facebook/annotationprocessors/modelgen/iface/ModelField;
        defaultValueProvider = LX/559;
        fieldName = ""
        preprocessor = LX/55A;
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->CLASS:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->METHOD:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract defaultValueProvider()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "LX/559;",
            ">;"
        }
    .end annotation
.end method

.method public abstract fieldName()Ljava/lang/String;
.end method

.method public abstract preprocessor()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "LX/55A;",
            ">;"
        }
    .end annotation
.end method
