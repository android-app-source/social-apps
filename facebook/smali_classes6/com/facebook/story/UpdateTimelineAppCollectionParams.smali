.class public Lcom/facebook/story/UpdateTimelineAppCollectionParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:LX/5vL;

.field public final d:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation
.end field

.field public final f:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public final g:Ljava/lang/String;

.field public final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/lang/String;

.field public final j:Z

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1021819
    new-instance v0, LX/5vK;

    invoke-direct {v0}, LX/5vK;-><init>()V

    sput-object v0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5vM;)V
    .locals 1

    .prologue
    .line 1021820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1021821
    iget-object v0, p1, LX/5vM;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->a:Ljava/lang/String;

    .line 1021822
    iget-object v0, p1, LX/5vM;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->b:Ljava/lang/String;

    .line 1021823
    iget-object v0, p1, LX/5vM;->c:LX/5vL;

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->c:LX/5vL;

    .line 1021824
    iget-object v0, p1, LX/5vM;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->d:Ljava/lang/String;

    .line 1021825
    iget-object v0, p1, LX/5vM;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->e:Ljava/lang/String;

    .line 1021826
    iget-object v0, p1, LX/5vM;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1021827
    iget-object v0, p1, LX/5vM;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->g:Ljava/lang/String;

    .line 1021828
    iget-object v0, p1, LX/5vM;->h:LX/0Px;

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    .line 1021829
    iget-object v0, p1, LX/5vM;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->i:Ljava/lang/String;

    .line 1021830
    iget-boolean v0, p1, LX/5vM;->j:Z

    iput-boolean v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->j:Z

    .line 1021831
    iget-object v0, p1, LX/5vM;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->k:Ljava/lang/String;

    .line 1021832
    iget-object v0, p1, LX/5vM;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->l:Ljava/lang/String;

    .line 1021833
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1021834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1021835
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->a:Ljava/lang/String;

    .line 1021836
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->b:Ljava/lang/String;

    .line 1021837
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5vL;

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->c:LX/5vL;

    .line 1021838
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->d:Ljava/lang/String;

    .line 1021839
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->e:Ljava/lang/String;

    .line 1021840
    const-class v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1021841
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->g:Ljava/lang/String;

    .line 1021842
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    .line 1021843
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->i:Ljava/lang/String;

    .line 1021844
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->j:Z

    .line 1021845
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->k:Ljava/lang/String;

    .line 1021846
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->l:Ljava/lang/String;

    .line 1021847
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1021848
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1021849
    const-class v0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "collectionId"

    iget-object v2, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "itemId"

    iget-object v2, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "action"

    iget-object v2, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->c:LX/5vL;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "curationSurface"

    iget-object v2, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "curationMechanism"

    iget-object v2, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v1

    const-string v2, "parentUnitType"

    iget-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "privacy"

    iget-object v2, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "storyCacheIds"

    iget-object v2, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "attachmentDedupKey"

    iget-object v2, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "isSaveCollection"

    iget-boolean v2, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->j:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "tracking"

    iget-object v2, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "sourceStoryId"

    iget-object v2, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1021850
    iget-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021851
    iget-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021852
    iget-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->c:LX/5vL;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1021853
    iget-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021854
    iget-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021855
    iget-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1021856
    iget-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021857
    iget-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1021858
    iget-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021859
    iget-boolean v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->j:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1021860
    iget-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021861
    iget-object v0, p0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1021862
    return-void
.end method
