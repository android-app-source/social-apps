.class public interface abstract annotation Lcom/facebook/crudolib/urimap/UriPattern;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/facebook/crudolib/urimap/UriPattern;
        componentType = .enum LX/447;->NONE:LX/447;
        scheme = .enum LX/6Pn;->NONE:LX/6Pn;
        template = ""
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->CLASS:Ljava/lang/annotation/RetentionPolicy;
.end annotation


# virtual methods
.method public abstract componentType()LX/447;
.end method

.method public abstract pattern()Ljava/lang/String;
.end method

.method public abstract scheme()LX/6Pn;
.end method

.method public abstract template()Ljava/lang/String;
.end method
