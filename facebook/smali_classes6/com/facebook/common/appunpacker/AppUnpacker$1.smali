.class public final Lcom/facebook/common/appunpacker/AppUnpacker$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/5JH;


# direct methods
.method public constructor <init>(LX/5JH;)V
    .locals 0

    .prologue
    .line 896768
    iput-object p1, p0, Lcom/facebook/common/appunpacker/AppUnpacker$1;->a:LX/5JH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const-wide v2, 0x80000000L

    .line 896769
    const-string v0, "AppUnpacker.finishUnpackingOnBackgroundThread()"

    invoke-static {v2, v3, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 896770
    :try_start_0
    iget-object v0, p0, Lcom/facebook/common/appunpacker/AppUnpacker$1;->a:LX/5JH;

    invoke-static {v0}, LX/5JH;->c(LX/5JH;)V

    .line 896771
    iget-object v0, p0, Lcom/facebook/common/appunpacker/AppUnpacker$1;->a:LX/5JH;

    invoke-static {v0}, LX/5JH;->f(LX/5JH;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 896772
    invoke-static {v2, v3}, LX/018;->a(J)V

    .line 896773
    return-void

    .line 896774
    :catch_0
    move-exception v0

    .line 896775
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 896776
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, LX/018;->a(J)V

    throw v0
.end method
