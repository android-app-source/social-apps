.class public Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field private a:LX/0w3;

.field private b:Landroid/content/res/Resources;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field public h:LX/6Lk;

.field private i:Z

.field private j:I

.field public k:LX/6Lj;

.field private l:Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1078796
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1078797
    invoke-direct {p0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->b()V

    .line 1078798
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1078794
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1078795
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1078788
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1078789
    sget-object v0, LX/03r;->CustomKeyboardLayout:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1078790
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->g:I

    .line 1078791
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1078792
    invoke-direct {p0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->b()V

    .line 1078793
    return-void
.end method

.method private a(LX/0w3;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1078785
    iput-object p1, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->a:LX/0w3;

    .line 1078786
    iput-object p2, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->b:Landroid/content/res/Resources;

    .line 1078787
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    invoke-static {v1}, LX/0w3;->a(LX/0QB;)LX/0w3;

    move-result-object v0

    check-cast v0, LX/0w3;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-direct {p0, v0, v1}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->a(LX/0w3;Landroid/content/res/Resources;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1078780
    const-class v0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    invoke-static {v0, p0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1078781
    invoke-direct {p0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->d()V

    .line 1078782
    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->b:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->c:I

    .line 1078783
    new-instance v0, LX/6Li;

    invoke-direct {v0, p0}, LX/6Li;-><init>(Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;)V

    iput-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->l:Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;

    .line 1078784
    return-void
.end method

.method public static c(Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;)Z
    .locals 1

    .prologue
    .line 1078777
    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->a:LX/0w3;

    .line 1078778
    iget-boolean p0, v0, LX/0w3;->f:Z

    move v0, p0

    .line 1078779
    return v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1078799
    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b01ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->d:I

    .line 1078800
    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b01ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->e:I

    .line 1078801
    return-void
.end method

.method private e()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1078772
    invoke-virtual {p0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1078773
    invoke-virtual {p0, v1}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    .line 1078774
    const/4 v0, 0x1

    .line 1078775
    :cond_0
    return v0

    .line 1078776
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static setIsCovered(Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;Z)V
    .locals 2

    .prologue
    .line 1078765
    iget-boolean v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->i:Z

    if-ne p1, v0, :cond_1

    .line 1078766
    :cond_0
    :goto_0
    return-void

    .line 1078767
    :cond_1
    iput-boolean p1, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->i:Z

    .line 1078768
    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    if-eqz v0, :cond_2

    .line 1078769
    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, LX/6Lj;->removeMessages(I)V

    .line 1078770
    :cond_2
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->h:LX/6Lk;

    if-eqz v0, :cond_0

    .line 1078771
    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->h:LX/6Lk;

    invoke-interface {v0}, LX/6Lk;->a()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/16 v1, 0x3e9

    .line 1078759
    invoke-static {p0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->c(Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1078760
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->setIsCovered(Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;Z)V

    .line 1078761
    :cond_0
    :goto_0
    return-void

    .line 1078762
    :cond_1
    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    invoke-virtual {v0, v1}, LX/6Lj;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1078763
    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1078764
    iget-object v1, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, LX/6Lj;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1078756
    invoke-static {p0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->c(Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1078757
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1078758
    :cond_0
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6e80ffd5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1078752
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 1078753
    new-instance v1, LX/6Lj;

    invoke-direct {v1, p0}, LX/6Lj;-><init>(Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;)V

    iput-object v1, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    .line 1078754
    invoke-virtual {p0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->l:Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalFocusChangeListener(Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;)V

    .line 1078755
    const/16 v1, 0x2d

    const v2, -0x1daa9fb3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0xb97394c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1078746
    iget-object v1, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    if-eqz v1, :cond_0

    .line 1078747
    iget-object v1, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, LX/6Lj;->removeMessages(I)V

    .line 1078748
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->k:LX/6Lj;

    .line 1078749
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->l:Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnGlobalFocusChangeListener(Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;)V

    .line 1078750
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 1078751
    const/16 v1, 0x2d

    const v2, 0x3825b3f4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1078743
    invoke-static {p0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->c(Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1078744
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 1078745
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 1078717
    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->b:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 1078718
    iget v1, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->c:I

    if-eq v0, v1, :cond_0

    .line 1078719
    invoke-direct {p0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->d()V

    .line 1078720
    iput v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->c:I

    .line 1078721
    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1078722
    invoke-static {p0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->c(Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;)Z

    move-result v1

    .line 1078723
    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->e()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1078724
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 1078725
    if-ne v2, v5, :cond_1

    .line 1078726
    :goto_0
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1078727
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 1078728
    invoke-static {p0, v4}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->setIsCovered(Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;Z)V

    .line 1078729
    :goto_1
    return-void

    .line 1078730
    :cond_1
    iget v1, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->d:I

    iget v3, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->f:I

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1078731
    iget v3, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->e:I

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1078732
    iget v3, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->j:I

    add-int/2addr v1, v3

    .line 1078733
    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_4

    .line 1078734
    iget v2, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->g:I

    sub-int/2addr v0, v2

    .line 1078735
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 1078736
    :cond_2
    if-eqz v1, :cond_3

    .line 1078737
    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->a:LX/0w3;

    .line 1078738
    iget v1, v0, LX/0w3;->e:I

    move v0, v1

    .line 1078739
    iput v0, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->f:I

    .line 1078740
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->setIsCovered(Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;Z)V

    .line 1078741
    :cond_3
    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 1078742
    invoke-virtual {p0, v4, v4}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->setMeasuredDimension(II)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public setAdditionalHeight(I)V
    .locals 0

    .prologue
    .line 1078715
    iput p1, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->j:I

    .line 1078716
    return-void
.end method

.method public setOnCoverListener(LX/6Lk;)V
    .locals 0
    .param p1    # LX/6Lk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1078713
    iput-object p1, p0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->h:LX/6Lk;

    .line 1078714
    return-void
.end method
