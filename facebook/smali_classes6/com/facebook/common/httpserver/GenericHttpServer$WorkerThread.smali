.class public final Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lorg/apache/http/protocol/HttpService;

.field private final b:Lorg/apache/http/HttpServerConnection;

.field private final c:LX/0Sj;

.field private final d:LX/03V;


# direct methods
.method public constructor <init>(Lorg/apache/http/protocol/HttpService;Lorg/apache/http/HttpServerConnection;LX/0Sj;LX/03V;)V
    .locals 0

    .prologue
    .line 1078495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1078496
    iput-object p1, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->a:Lorg/apache/http/protocol/HttpService;

    .line 1078497
    iput-object p2, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->b:Lorg/apache/http/HttpServerConnection;

    .line 1078498
    iput-object p3, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->c:LX/0Sj;

    .line 1078499
    iput-object p4, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->d:LX/03V;

    .line 1078500
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "LogMethodNoExceptionInCatch"
        }
    .end annotation

    .prologue
    .line 1078501
    iget-object v0, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->c:LX/0Sj;

    const-string v1, "GenericHttpServer"

    const-string v2, "RequestListenerThread"

    invoke-interface {v0, v1, v2}, LX/0Sj;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0cV;

    move-result-object v1

    .line 1078502
    if-eqz v1, :cond_0

    .line 1078503
    invoke-interface {v1}, LX/0cV;->a()V

    .line 1078504
    :cond_0
    :try_start_0
    new-instance v0, Lorg/apache/http/protocol/BasicHttpContext;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lorg/apache/http/protocol/BasicHttpContext;-><init>(Lorg/apache/http/protocol/HttpContext;)V

    .line 1078505
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->b:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v2}, Lorg/apache/http/HttpServerConnection;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1078506
    iget-object v2, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->a:Lorg/apache/http/protocol/HttpService;

    iget-object v3, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->b:Lorg/apache/http/HttpServerConnection;

    invoke-virtual {v2, v3, v0}, Lorg/apache/http/protocol/HttpService;->handleRequest(Lorg/apache/http/HttpServerConnection;Lorg/apache/http/protocol/HttpContext;)V
    :try_end_0
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/apache/http/HttpException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1078507
    :cond_1
    if-eqz v1, :cond_2

    .line 1078508
    invoke-interface {v1}, LX/0cV;->b()V

    .line 1078509
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->b:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v0}, Lorg/apache/http/HttpServerConnection;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9

    .line 1078510
    :goto_0
    return-void

    .line 1078511
    :catch_0
    move-exception v0

    .line 1078512
    :try_start_2
    const-string v2, "GenericHttpServer"

    const-string v3, "Client closed connection: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lorg/apache/http/ConnectionClosedException;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1078513
    if-eqz v1, :cond_3

    .line 1078514
    invoke-interface {v1}, LX/0cV;->b()V

    .line 1078515
    :cond_3
    :try_start_3
    iget-object v0, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->b:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v0}, Lorg/apache/http/HttpServerConnection;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 1078516
    :catch_1
    goto :goto_0

    .line 1078517
    :catch_2
    move-exception v0

    .line 1078518
    :try_start_4
    const-string v2, "GenericHttpServer"

    const-string v3, "I/O error"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1078519
    if-eqz v1, :cond_4

    .line 1078520
    invoke-interface {v1}, LX/0cV;->b()V

    .line 1078521
    :cond_4
    :try_start_5
    iget-object v0, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->b:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v0}, Lorg/apache/http/HttpServerConnection;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_0

    .line 1078522
    :catch_3
    goto :goto_0

    .line 1078523
    :catch_4
    move-exception v0

    .line 1078524
    :try_start_6
    const-string v2, "GenericHttpServer"

    const-string v3, "Unrecoverable HTTP protocol violation"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1078525
    if-eqz v1, :cond_5

    .line 1078526
    invoke-interface {v1}, LX/0cV;->b()V

    .line 1078527
    :cond_5
    :try_start_7
    iget-object v0, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->b:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v0}, Lorg/apache/http/HttpServerConnection;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_0

    .line 1078528
    :catch_5
    goto :goto_0

    .line 1078529
    :catch_6
    move-exception v0

    .line 1078530
    :try_start_8
    iget-object v2, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->d:LX/03V;

    const-string v3, "GenericHttpServer"

    const-string v4, "Unexpected exception when handling request"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1078531
    if-eqz v1, :cond_6

    .line 1078532
    invoke-interface {v1}, LX/0cV;->b()V

    .line 1078533
    :cond_6
    :try_start_9
    iget-object v0, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->b:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v0}, Lorg/apache/http/HttpServerConnection;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    goto :goto_0

    .line 1078534
    :catch_7
    goto :goto_0

    .line 1078535
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_7

    .line 1078536
    invoke-interface {v1}, LX/0cV;->b()V

    .line 1078537
    :cond_7
    :try_start_a
    iget-object v1, p0, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;->b:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v1}, Lorg/apache/http/HttpServerConnection;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 1078538
    :goto_1
    throw v0

    :catch_8
    goto :goto_1

    .line 1078539
    :catch_9
    goto :goto_0
.end method
