.class public Lcom/facebook/common/banner/BasicBannerNotificationView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/6LQ;

.field private final b:Landroid/widget/TextView;

.field private c:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/widget/ProgressBar;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1078461
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/common/banner/BasicBannerNotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1078462
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1078459
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/common/banner/BasicBannerNotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1078460
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1078451
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1078452
    const v0, 0x7f030c1d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1078453
    const v0, 0x7f0d1dd7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->b:Landroid/widget/TextView;

    .line 1078454
    const v0, 0x7f0d051d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->b(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->e:LX/0am;

    .line 1078455
    const v0, 0x7f0d1dd6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->b(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->f:LX/0am;

    .line 1078456
    const v0, 0x7f0d1dd8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->c:LX/4ob;

    .line 1078457
    const v0, 0x7f0d1dd9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->d:LX/4ob;

    .line 1078458
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1078446
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->c:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 1078447
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->d:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 1078448
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1078449
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1078450
    :cond_0
    return-void
.end method

.method private a(LX/6LR;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1078391
    iget-object v2, p1, LX/6LR;->e:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1078392
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->c:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 1078393
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->c:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1078394
    invoke-direct {p0, v0, p1, v1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->a(Lcom/facebook/widget/text/BetterTextView;LX/6LR;I)V

    .line 1078395
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->b:Landroid/widget/TextView;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 1078396
    return-void

    :cond_0
    move v0, v1

    .line 1078397
    goto :goto_0
.end method

.method private a(Lcom/facebook/widget/text/BetterTextView;LX/6LR;I)V
    .locals 1

    .prologue
    .line 1078437
    iget-object v0, p2, LX/6LR;->e:LX/0Px;

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1078438
    iget-object v0, p2, LX/6LR;->f:LX/0Px;

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTag(Ljava/lang/Object;)V

    .line 1078439
    iget v0, p2, LX/6LR;->g:I

    if-eqz v0, :cond_0

    .line 1078440
    iget v0, p2, LX/6LR;->g:I

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1078441
    :cond_0
    iget-object v0, p2, LX/6LR;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 1078442
    iget-object v0, p2, LX/6LR;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1078443
    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1078444
    :cond_1
    new-instance v0, LX/6LP;

    invoke-direct {v0, p0, p1}, LX/6LP;-><init>(Lcom/facebook/common/banner/BasicBannerNotificationView;Lcom/facebook/widget/text/BetterTextView;)V

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1078445
    return-void
.end method

.method private b(LX/6LR;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1078421
    iget-object v1, p1, LX/6LR;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1078422
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->d:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 1078423
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->d:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1078424
    invoke-virtual {p0}, Lcom/facebook/common/banner/BasicBannerNotificationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    move v3, v2

    .line 1078425
    :goto_1
    iget-object v1, p1, LX/6LR;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_1

    .line 1078426
    const v1, 0x7f030c20

    invoke-virtual {v4, v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 1078427
    invoke-direct {p0, v1, p1, v3}, Lcom/facebook/common/banner/BasicBannerNotificationView;->a(Lcom/facebook/widget/text/BetterTextView;LX/6LR;I)V

    .line 1078428
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1078429
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 1078430
    goto :goto_0

    .line 1078431
    :cond_1
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1078432
    invoke-virtual {p0}, Lcom/facebook/common/banner/BasicBannerNotificationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b13a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1078433
    invoke-virtual {p0}, Lcom/facebook/common/banner/BasicBannerNotificationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b13a0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1078434
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2, v1, v2, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1078435
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1078436
    :cond_2
    return-void
.end method


# virtual methods
.method public setOnBannerButtonClickListener(LX/6LQ;)V
    .locals 0

    .prologue
    .line 1078419
    iput-object p1, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->a:LX/6LQ;

    .line 1078420
    return-void
.end method

.method public setParams(LX/6LR;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1078398
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->b:Landroid/widget/TextView;

    iget-object v3, p1, LX/6LR;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1078399
    iget v0, p1, LX/6LR;->c:I

    if-eqz v0, :cond_0

    .line 1078400
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->b:Landroid/widget/TextView;

    iget v3, p1, LX/6LR;->c:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1078401
    :cond_0
    iget-object v0, p1, LX/6LR;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1078402
    iget-object v0, p1, LX/6LR;->e:LX/0Px;

    if-eqz v0, :cond_5

    iget-object v0, p1, LX/6LR;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1078403
    iget-object v0, p1, LX/6LR;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 1078404
    invoke-direct {p0, p1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->a(LX/6LR;)V

    .line 1078405
    :goto_0
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1078406
    iget-object v0, p0, Lcom/facebook/common/banner/BasicBannerNotificationView;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iget-boolean v3, p1, LX/6LR;->b:Z

    if-eqz v3, :cond_6

    move v3, v2

    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1078407
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/common/banner/BasicBannerNotificationView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1078408
    instance-of v3, v0, LX/4ng;

    if-eqz v3, :cond_2

    .line 1078409
    check-cast v0, LX/4ng;

    .line 1078410
    iget-object v3, p1, LX/6LR;->i:LX/2M7;

    sget-object v4, LX/2M7;->ONLY_WHEN_SPACE_AVAILABLE:LX/2M7;

    if-ne v3, v4, :cond_7

    :goto_2
    iput-boolean v1, v0, LX/4ng;->a:Z

    .line 1078411
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/common/banner/BasicBannerNotificationView;->requestLayout()V

    .line 1078412
    return-void

    .line 1078413
    :cond_3
    iget-object v0, p1, LX/6LR;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v3, 0x3

    if-gt v0, v3, :cond_4

    move v0, v1

    :goto_3
    const-string v3, "No current support for more than 3 buttons in banner view."

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1078414
    invoke-direct {p0, p1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->b(LX/6LR;)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1078415
    goto :goto_3

    .line 1078416
    :cond_5
    invoke-direct {p0}, Lcom/facebook/common/banner/BasicBannerNotificationView;->a()V

    goto :goto_0

    .line 1078417
    :cond_6
    const/16 v3, 0x8

    goto :goto_1

    :cond_7
    move v1, v2

    .line 1078418
    goto :goto_2
.end method
