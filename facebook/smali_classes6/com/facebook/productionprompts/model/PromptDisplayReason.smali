.class public final Lcom/facebook/productionprompts/model/PromptDisplayReason;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/productionprompts/model/PromptDisplayReason;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reason_with_entities"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1007368
    const-class v0, Lcom/facebook/productionprompts/model/PromptDisplayReasonDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1007367
    const-class v0, Lcom/facebook/productionprompts/model/PromptDisplayReasonSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1007366
    new-instance v0, LX/5oa;

    invoke-direct {v0}, LX/5oa;-><init>()V

    sput-object v0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1007363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007364
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1007365
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1007317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007318
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1007319
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;)V
    .locals 6

    .prologue
    .line 1007336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007337
    if-nez p1, :cond_0

    .line 1007338
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1007339
    :goto_0
    return-void

    .line 1007340
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->b()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;

    .line 1007341
    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;->c()I

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;->b()I

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesEntityFieldsModel;

    move-result-object v0

    .line 1007342
    new-instance v4, LX/1nG;

    invoke-direct {v4}, LX/1nG;-><init>()V

    .line 1007343
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesEntityFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesEntityFieldsModel;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, v5, p1}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    .line 1007344
    if-nez v1, :cond_1

    const/4 v4, 0x0

    :goto_1
    move-object v0, v4

    .line 1007345
    iput-object v0, p0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    goto :goto_0

    :cond_1
    new-instance v4, LX/4af;

    invoke-direct {v4}, LX/4af;-><init>()V

    .line 1007346
    iput-object v1, v4, LX/4af;->b:Ljava/lang/String;

    .line 1007347
    move-object v4, v4

    .line 1007348
    new-instance v5, LX/4ag;

    invoke-direct {v5}, LX/4ag;-><init>()V

    .line 1007349
    iput v2, v5, LX/4ag;->c:I

    .line 1007350
    move-object v5, v5

    .line 1007351
    iput v3, v5, LX/4ag;->b:I

    .line 1007352
    move-object v5, v5

    .line 1007353
    new-instance p1, LX/4ad;

    invoke-direct {p1}, LX/4ad;-><init>()V

    .line 1007354
    iput-object v0, p1, LX/4ad;->g:Ljava/lang/String;

    .line 1007355
    move-object p1, p1

    .line 1007356
    invoke-virtual {p1}, LX/4ad;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesEntityFieldsModel;

    move-result-object p1

    .line 1007357
    iput-object p1, v5, LX/4ag;->a:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesEntityFieldsModel;

    .line 1007358
    move-object v5, v5

    .line 1007359
    invoke-virtual {v5}, LX/4ag;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;

    move-result-object v5

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    .line 1007360
    iput-object v5, v4, LX/4af;->a:LX/0Px;

    .line 1007361
    move-object v4, v4

    .line 1007362
    invoke-virtual {v4}, LX/4af;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v4

    goto :goto_1
.end method

.method private c()I
    .locals 1

    .prologue
    .line 1007335
    iget-object v0, p0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/facebook/productionprompts/model/PromptDisplayReason;->f()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;->c()I

    move-result v0

    goto :goto_0
.end method

.method private d()I
    .locals 1

    .prologue
    .line 1007334
    iget-object v0, p0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/facebook/productionprompts/model/PromptDisplayReason;->f()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;->b()I

    move-result v0

    goto :goto_0
.end method

.method private e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1007333
    iget-object v0, p0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/facebook/productionprompts/model/PromptDisplayReason;->f()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesEntityFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesEntityFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private f()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;
    .locals 2

    .prologue
    .line 1007332
    iget-object v0, p0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->b()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;

    return-object v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1007331
    iget-object v0, p0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1007330
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1007326
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/productionprompts/model/PromptDisplayReason;

    if-nez v1, :cond_1

    .line 1007327
    :cond_0
    :goto_0
    return v0

    .line 1007328
    :cond_1
    check-cast p1, Lcom/facebook/productionprompts/model/PromptDisplayReason;

    .line 1007329
    invoke-virtual {p0}, Lcom/facebook/productionprompts/model/PromptDisplayReason;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/PromptDisplayReason;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/productionprompts/model/PromptDisplayReason;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p1}, Lcom/facebook/productionprompts/model/PromptDisplayReason;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/productionprompts/model/PromptDisplayReason;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p1}, Lcom/facebook/productionprompts/model/PromptDisplayReason;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/productionprompts/model/PromptDisplayReason;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1}, Lcom/facebook/productionprompts/model/PromptDisplayReason;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1007323
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 1007324
    iget-object v2, p0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object v2, v2

    .line 1007325
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1007320
    iget-object v0, p0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object v0, v0

    .line 1007321
    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1007322
    return-void
.end method
