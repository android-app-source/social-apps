.class public Lcom/facebook/productionprompts/model/ProductionPromptSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/productionprompts/model/ProductionPrompt;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1007199
    const-class v0, Lcom/facebook/productionprompts/model/ProductionPrompt;

    new-instance v1, Lcom/facebook/productionprompts/model/ProductionPromptSerializer;

    invoke-direct {v1}, Lcom/facebook/productionprompts/model/ProductionPromptSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1007200
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1007201
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/productionprompts/model/ProductionPrompt;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1007202
    if-nez p0, :cond_0

    .line 1007203
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1007204
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1007205
    invoke-static {p0, p1, p2}, Lcom/facebook/productionprompts/model/ProductionPromptSerializer;->b(Lcom/facebook/productionprompts/model/ProductionPrompt;LX/0nX;LX/0my;)V

    .line 1007206
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1007207
    return-void
.end method

.method private static b(Lcom/facebook/productionprompts/model/ProductionPrompt;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1007208
    const-string v0, "id"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007209
    const-string v0, "title"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptTitle:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007210
    const-string v0, "banner_text"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerText:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007211
    const-string v0, "banner_subheader"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerSubheader:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007212
    const-string v0, "image_uri"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptImageUri:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007213
    const-string v0, "start_time"

    iget-wide v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStartTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1007214
    const-string v0, "end_time"

    iget-wide v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mEndTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1007215
    const-string v0, "composer_prompt_text"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mComposerPromptText:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007216
    const-string v0, "minutiae_object"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1007217
    const-string v0, "link_attachment_url"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mLinkAttachmentUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007218
    const-string v0, "dismiss_survey_id"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mDismissSurveyId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007219
    const-string v0, "ignore_survey_id"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mIgnoreSurveyId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007220
    const-string v0, "post_with_minutiae_survey_id"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPostWithMinutiaeSurveyId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007221
    const-string v0, "checkin_location_id"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007222
    const-string v0, "checkin_location_name"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007223
    const-string v0, "prompt_type"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptType:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007224
    const-string v0, "profile_picture_overlay"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mProfilePictureOverlay:Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1007225
    const-string v0, "thumbnail_uri"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mThumbnailUri:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007226
    const-string v0, "frame_pack_model"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mFramePackModel:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1007227
    const-string v0, "mask_model"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1007228
    const-string v0, "particle_effect_model"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1007229
    const-string v0, "shader_filter_model"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1007230
    const-string v0, "style_transfer_model"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1007231
    const-string v0, "server_ranking_score"

    iget-wide v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerRankingScore:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Double;)V

    .line 1007232
    const-string v0, "server_tracking_string"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerTrackingString:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007233
    const-string v0, "prompt_display_reason"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptDisplayReason:Lcom/facebook/productionprompts/model/PromptDisplayReason;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1007234
    const-string v0, "prompt_confidence"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptConfidence:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007235
    const-string v0, "cta_text"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCallToActionText:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007236
    const-string v0, "prefill_text"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPrefillText:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007237
    const-string v0, "open_action"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mOpenAction:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007238
    const-string v0, "meme_category_fields_model"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMemeCategoryFieldsModel:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1007239
    const-string v0, "suggested_cover_photos"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mSuggestedCoverPhotos:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1007240
    const-string v0, "prompt_feed_type"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptFeedType:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007241
    const-string v0, "tagged_Users"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mTaggedUsers:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1007242
    const-string v0, "inspiration_default_index"

    iget v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mInspirationDefaultIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1007243
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1007244
    check-cast p1, Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-static {p1, p2, p3}, Lcom/facebook/productionprompts/model/ProductionPromptSerializer;->a(Lcom/facebook/productionprompts/model/ProductionPrompt;LX/0nX;LX/0my;)V

    return-void
.end method
