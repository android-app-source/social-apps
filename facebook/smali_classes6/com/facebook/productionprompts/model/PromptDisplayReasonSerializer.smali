.class public Lcom/facebook/productionprompts/model/PromptDisplayReasonSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/productionprompts/model/PromptDisplayReason;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1007402
    const-class v0, Lcom/facebook/productionprompts/model/PromptDisplayReason;

    new-instance v1, Lcom/facebook/productionprompts/model/PromptDisplayReasonSerializer;

    invoke-direct {v1}, Lcom/facebook/productionprompts/model/PromptDisplayReasonSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1007403
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1007390
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/productionprompts/model/PromptDisplayReason;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1007391
    if-nez p0, :cond_0

    .line 1007392
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1007393
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1007394
    invoke-static {p0, p1, p2}, Lcom/facebook/productionprompts/model/PromptDisplayReasonSerializer;->b(Lcom/facebook/productionprompts/model/PromptDisplayReason;LX/0nX;LX/0my;)V

    .line 1007395
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1007396
    return-void
.end method

.method private static b(Lcom/facebook/productionprompts/model/PromptDisplayReason;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1007397
    const-string v0, "reason_with_entities"

    .line 1007398
    iget-object v1, p0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object v1, v1

    .line 1007399
    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1007400
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1007401
    check-cast p1, Lcom/facebook/productionprompts/model/PromptDisplayReason;

    invoke-static {p1, p2, p3}, Lcom/facebook/productionprompts/model/PromptDisplayReasonSerializer;->a(Lcom/facebook/productionprompts/model/PromptDisplayReason;LX/0nX;LX/0my;)V

    return-void
.end method
