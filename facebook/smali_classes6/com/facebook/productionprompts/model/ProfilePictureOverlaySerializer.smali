.class public Lcom/facebook/productionprompts/model/ProfilePictureOverlaySerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/productionprompts/model/ProfilePictureOverlay;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1007312
    const-class v0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    new-instance v1, Lcom/facebook/productionprompts/model/ProfilePictureOverlaySerializer;

    invoke-direct {v1}, Lcom/facebook/productionprompts/model/ProfilePictureOverlaySerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1007313
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1007301
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/productionprompts/model/ProfilePictureOverlay;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1007302
    if-nez p0, :cond_0

    .line 1007303
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1007304
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1007305
    invoke-static {p0, p1, p2}, Lcom/facebook/productionprompts/model/ProfilePictureOverlaySerializer;->b(Lcom/facebook/productionprompts/model/ProfilePictureOverlay;LX/0nX;LX/0my;)V

    .line 1007306
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1007307
    return-void
.end method

.method private static b(Lcom/facebook/productionprompts/model/ProfilePictureOverlay;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1007308
    const-string v0, "image_overlay_id"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007309
    const-string v0, "image_overlay_url"

    iget-object v1, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007310
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1007311
    check-cast p1, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    invoke-static {p1, p2, p3}, Lcom/facebook/productionprompts/model/ProfilePictureOverlaySerializer;->a(Lcom/facebook/productionprompts/model/ProfilePictureOverlay;LX/0nX;LX/0my;)V

    return-void
.end method
