.class public Lcom/facebook/productionprompts/model/ProfilePictureOverlay;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/productionprompts/model/ProfilePictureOverlayDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/productionprompts/model/ProfilePictureOverlaySerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/productionprompts/model/ProfilePictureOverlay;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mImageOverlayId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "image_overlay_id"
    .end annotation
.end field

.field public final mImageOverlayUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "image_overlay_url"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1007248
    const-class v0, Lcom/facebook/productionprompts/model/ProfilePictureOverlayDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1007278
    const-class v0, Lcom/facebook/productionprompts/model/ProfilePictureOverlaySerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1007277
    new-instance v0, LX/5oZ;

    invoke-direct {v0}, LX/5oZ;-><init>()V

    sput-object v0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1007273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007274
    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayId:Ljava/lang/String;

    .line 1007275
    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayUrl:Ljava/lang/String;

    .line 1007276
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1007269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007270
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayId:Ljava/lang/String;

    .line 1007271
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayUrl:Ljava/lang/String;

    .line 1007272
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1007265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007266
    iput-object p1, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayId:Ljava/lang/String;

    .line 1007267
    iput-object p2, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayUrl:Ljava/lang/String;

    .line 1007268
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/model/ProfilePictureOverlay;
    .locals 1

    .prologue
    .line 1007262
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1007263
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1007264
    new-instance v0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    invoke-direct {v0, p0, p1}, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1007261
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayId:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1007260
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1007259
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1007253
    if-ne p0, p1, :cond_1

    .line 1007254
    :cond_0
    :goto_0
    return v0

    .line 1007255
    :cond_1
    instance-of v2, p1, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    if-nez v2, :cond_2

    move v0, v1

    .line 1007256
    goto :goto_0

    .line 1007257
    :cond_2
    check-cast p1, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    .line 1007258
    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayId:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayId:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayUrl:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayUrl:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1007252
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1007249
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1007250
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->mImageOverlayUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1007251
    return-void
.end method
