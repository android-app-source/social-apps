.class public final Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3bd65f81
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1005037
    const-class v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1005036
    const-class v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1005034
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1005035
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1005024
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1005025
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1005026
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1005027
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1005028
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1005029
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1005030
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1005031
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1005032
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1005033
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1005021
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1005022
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1005023
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005038
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->e:Ljava/lang/String;

    .line 1005039
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1005018
    new-instance v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;

    invoke-direct {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;-><init>()V

    .line 1005019
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1005020
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1005017
    const v0, -0x75d6459f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1005016
    const v0, -0x4dfd0aa9

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005014
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->f:Ljava/lang/String;

    .line 1005015
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005012
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->g:Ljava/lang/String;

    .line 1005013
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->g:Ljava/lang/String;

    return-object v0
.end method
