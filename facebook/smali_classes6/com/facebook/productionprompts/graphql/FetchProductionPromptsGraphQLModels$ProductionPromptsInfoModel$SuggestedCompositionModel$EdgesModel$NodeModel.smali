.class public final Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7096264d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1005770
    const-class v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1005771
    const-class v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1005772
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1005773
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 16

    .prologue
    .line 1005774
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1005775
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1005776
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->j()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1005777
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->k()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1005778
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->l()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1005779
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->m()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1005780
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->n()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1005781
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->o()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1005782
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->p()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1005783
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1005784
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->r()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1005785
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->s()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1005786
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->t()LX/0Px;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/util/List;)I

    move-result v12

    .line 1005787
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->u()LX/0Px;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 1005788
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->v()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1005789
    const/16 v15, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->c(I)V

    .line 1005790
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v1}, LX/186;->b(II)V

    .line 1005791
    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1005792
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1005793
    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1005794
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1005795
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1005796
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1005797
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1005798
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1005799
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1005800
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1005801
    const/16 v1, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 1005802
    const/16 v1, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 1005803
    const/16 v1, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v14}, LX/186;->b(II)V

    .line 1005804
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1005805
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    return v1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1005808
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1005809
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1005810
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;

    .line 1005811
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1005812
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    .line 1005813
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;

    .line 1005814
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->j()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1005815
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->j()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1005816
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->j()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1005817
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    .line 1005818
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->f:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1005819
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->k()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1005820
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->k()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 1005821
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->k()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1005822
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    .line 1005823
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->g:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 1005824
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->l()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1005825
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->l()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;

    .line 1005826
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->l()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1005827
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    .line 1005828
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->h:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;

    .line 1005829
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->m()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1005830
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->m()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    .line 1005831
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->m()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1005832
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    .line 1005833
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->i:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    .line 1005834
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->n()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1005835
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->n()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    .line 1005836
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->n()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1005837
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    .line 1005838
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->j:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    .line 1005839
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->o()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1005840
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->o()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;

    .line 1005841
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->o()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1005842
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    .line 1005843
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->k:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;

    .line 1005844
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->p()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1005845
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->p()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    .line 1005846
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->p()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1005847
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    .line 1005848
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->l:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    .line 1005849
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1005850
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1005851
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1005852
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    .line 1005853
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->m:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1005854
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->r()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1005855
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->r()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    .line 1005856
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->r()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1005857
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    .line 1005858
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->n:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    .line 1005859
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->s()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1005860
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->s()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    .line 1005861
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->s()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1005862
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    .line 1005863
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->o:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    .line 1005864
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->u()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1005865
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->u()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1005866
    if-eqz v2, :cond_b

    .line 1005867
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    .line 1005868
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q:Ljava/util/List;

    move-object v1, v0

    .line 1005869
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->v()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1005870
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->v()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    .line 1005871
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->v()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 1005872
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    .line 1005873
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->r:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    .line 1005874
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1005875
    if-nez v1, :cond_d

    :goto_0
    return-object p0

    :cond_d
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005806
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;

    .line 1005807
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1005878
    new-instance v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;-><init>()V

    .line 1005879
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1005880
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1005759
    const v0, 0x2cf213f4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1005881
    const v0, -0x5b830959

    return v0
.end method

.method public final j()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005876
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->f:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->f:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1005877
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->f:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005766
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->g:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->g:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 1005767
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->g:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005768
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->h:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->h:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;

    .line 1005769
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->h:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005764
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->i:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->i:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    .line 1005765
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->i:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005762
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->j:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->j:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    .line 1005763
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->j:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    return-object v0
.end method

.method public final o()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005760
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->k:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->k:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;

    .line 1005761
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->k:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;

    return-object v0
.end method

.method public final p()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005757
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->l:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->l:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    .line 1005758
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->l:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    return-object v0
.end method

.method public final q()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005755
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->m:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->m:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1005756
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->m:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    return-object v0
.end method

.method public final r()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005753
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->n:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->n:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    .line 1005754
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->n:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    return-object v0
.end method

.method public final s()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005751
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->o:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->o:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    .line 1005752
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->o:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    return-object v0
.end method

.method public final t()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1005749
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->p:Ljava/util/List;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->p:Ljava/util/List;

    .line 1005750
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->p:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final u()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1005747
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q:Ljava/util/List;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q:Ljava/util/List;

    .line 1005748
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final v()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005745
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->r:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->r:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    .line 1005746
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->r:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    return-object v0
.end method
