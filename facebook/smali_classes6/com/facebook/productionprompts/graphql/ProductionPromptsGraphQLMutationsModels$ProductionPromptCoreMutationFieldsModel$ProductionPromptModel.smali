.class public final Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1006924
    const-class v0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1006923
    const-class v0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1006888
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1006889
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1006917
    iput-object p1, p0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;->e:Ljava/lang/String;

    .line 1006918
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1006919
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1006920
    if-eqz v0, :cond_0

    .line 1006921
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1006922
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1006915
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;->e:Ljava/lang/String;

    .line 1006916
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1006909
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1006910
    invoke-direct {p0}, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1006911
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1006912
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1006913
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1006914
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1006906
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1006907
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1006908
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1006905
    new-instance v0, LX/5oQ;

    invoke-direct {v0, p1}, LX/5oQ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1006904
    invoke-direct {p0}, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1006898
    const-string v0, "id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1006899
    invoke-direct {p0}, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1006900
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1006901
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1006902
    :goto_0
    return-void

    .line 1006903
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1006895
    const-string v0, "id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1006896
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;->a(Ljava/lang/String;)V

    .line 1006897
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1006892
    new-instance v0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;

    invoke-direct {v0}, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;-><init>()V

    .line 1006893
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1006894
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1006891
    const v0, 0x130aea3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1006890
    const v0, -0x75d3b463

    return v0
.end method
