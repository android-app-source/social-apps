.class public final Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2a6c3bfd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1006938
    const-class v0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1006939
    const-class v0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1006940
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1006941
    return-void
.end method

.method private a()Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1006942
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel;->e:Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel;->e:Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;

    .line 1006943
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel;->e:Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1006944
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1006945
    invoke-direct {p0}, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel;->a()Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1006946
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1006947
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1006948
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1006949
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1006950
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1006951
    invoke-direct {p0}, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel;->a()Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1006952
    invoke-direct {p0}, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel;->a()Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;

    .line 1006953
    invoke-direct {p0}, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel;->a()Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1006954
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel;

    .line 1006955
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel;->e:Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel$ProductionPromptModel;

    .line 1006956
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1006957
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1006958
    new-instance v0, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/productionprompts/graphql/ProductionPromptsGraphQLMutationsModels$ProductionPromptCoreMutationFieldsModel;-><init>()V

    .line 1006959
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1006960
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1006961
    const v0, -0x60d67c62

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1006962
    const v0, -0x2758a1e0

    return v0
.end method
