.class public final Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7707427f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1005642
    const-class v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1005657
    const-class v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1005658
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1005659
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1005643
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1005644
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1005645
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1005646
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1005647
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1005648
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1005649
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1005650
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1005651
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    .line 1005652
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1005653
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;

    .line 1005654
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;->e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    .line 1005655
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1005656
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1005635
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;->e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;->e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    .line 1005636
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;->e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1005637
    new-instance v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;

    invoke-direct {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;-><init>()V

    .line 1005638
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1005639
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1005640
    const v0, -0x50b09836

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1005641
    const v0, 0x4984e12

    return v0
.end method
