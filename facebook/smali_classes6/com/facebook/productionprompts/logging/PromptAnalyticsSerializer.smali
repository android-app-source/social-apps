.class public Lcom/facebook/productionprompts/logging/PromptAnalyticsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1007105
    const-class v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    new-instance v1, Lcom/facebook/productionprompts/logging/PromptAnalyticsSerializer;

    invoke-direct {v1}, Lcom/facebook/productionprompts/logging/PromptAnalyticsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1007106
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1007107
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1007108
    if-nez p0, :cond_0

    .line 1007109
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1007110
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1007111
    invoke-static {p0, p1, p2}, Lcom/facebook/productionprompts/logging/PromptAnalyticsSerializer;->b(Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0nX;LX/0my;)V

    .line 1007112
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1007113
    return-void
.end method

.method private static b(Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1007114
    const-string v0, "prompt_id"

    iget-object v1, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007115
    const-string v0, "prompt_type"

    iget-object v1, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007116
    const-string v0, "prompt_session_id"

    iget-object v1, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptSessionId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007117
    const-string v0, "composer_session_id"

    iget-object v1, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->composerSessionId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007118
    const-string v0, "tracking_string"

    iget-object v1, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->trackingString:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007119
    const-string v0, "prompt_view_state"

    iget-object v1, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptViewState:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007120
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1007121
    check-cast p1, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-static {p1, p2, p3}, Lcom/facebook/productionprompts/logging/PromptAnalyticsSerializer;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0nX;LX/0my;)V

    return-void
.end method
