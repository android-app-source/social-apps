.class public Lcom/facebook/productionprompts/logging/PromptAnalytics;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/productionprompts/logging/PromptAnalyticsDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/productionprompts/logging/PromptAnalyticsSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final composerSessionId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_session_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final promptId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_id"
    .end annotation
.end field

.field public final promptSessionId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_session_id"
    .end annotation
.end field

.field public final promptType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_type"
    .end annotation
.end field

.field public final promptViewState:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_view_state"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final trackingString:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tracking_string"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1007078
    const-class v0, Lcom/facebook/productionprompts/logging/PromptAnalyticsDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1007077
    const-class v0, Lcom/facebook/productionprompts/logging/PromptAnalyticsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1007076
    new-instance v0, LX/5oX;

    invoke-direct {v0}, LX/5oX;-><init>()V

    sput-object v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1007074
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/productionprompts/logging/PromptAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1007075
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1007066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007067
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    .line 1007068
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    .line 1007069
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptSessionId:Ljava/lang/String;

    .line 1007070
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->composerSessionId:Ljava/lang/String;

    .line 1007071
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->trackingString:Ljava/lang/String;

    .line 1007072
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptViewState:Ljava/lang/String;

    .line 1007073
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1007058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007059
    iput-object p1, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    .line 1007060
    iput-object p2, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    .line 1007061
    iput-object p3, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptSessionId:Ljava/lang/String;

    .line 1007062
    iput-object p4, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->composerSessionId:Ljava/lang/String;

    .line 1007063
    iput-object p5, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->trackingString:Ljava/lang/String;

    .line 1007064
    iput-object p6, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptViewState:Ljava/lang/String;

    .line 1007065
    return-void
.end method

.method public static a(LX/1RN;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .locals 6

    .prologue
    .line 1007057
    iget-object v0, p0, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    iget-object v2, p0, LX/1RN;->b:LX/1lP;

    iget-object v4, v2, LX/1lP;->b:Ljava/lang/String;

    iget-object v2, p0, LX/1RN;->c:LX/32e;

    iget-object v2, v2, LX/32e;->a:LX/24P;

    invoke-virtual {v2}, LX/24P;->getName()Ljava/lang/String;

    move-result-object v5

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1007056
    new-instance v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/productionprompts/logging/PromptAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1007048
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1007049
    iget-object v0, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1007050
    iget-object v0, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1007051
    iget-object v0, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1007052
    iget-object v0, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->composerSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1007053
    iget-object v0, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->trackingString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1007054
    iget-object v0, p0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptViewState:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1007055
    return-void
.end method
