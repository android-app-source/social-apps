.class public Lcom/facebook/libyuv/YUVColorConverter;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:Lcom/facebook/libyuv/YUVColorConverter;


# instance fields
.field private final a:LX/6Z4;


# direct methods
.method public constructor <init>(LX/6Z4;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1110856
    iput-object p1, p0, Lcom/facebook/libyuv/YUVColorConverter;->a:LX/6Z4;

    .line 1110857
    iget-object v0, p0, Lcom/facebook/libyuv/YUVColorConverter;->a:LX/6Z4;

    invoke-virtual {v0}, LX/03m;->b()V

    .line 1110858
    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;III)I
    .locals 1

    .prologue
    .line 1110876
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1110877
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1110878
    invoke-virtual {p4}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1110879
    invoke-static/range {p0 .. p7}, Lcom/facebook/libyuv/YUVColorConverter;->nativeConvertARGBToNV21(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;III)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;III)I
    .locals 1

    .prologue
    .line 1110871
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1110872
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1110873
    invoke-virtual {p4}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1110874
    invoke-virtual {p6}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1110875
    invoke-static/range {p0 .. p9}, Lcom/facebook/libyuv/YUVColorConverter;->nativeConvertARGBToI420(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;III)I

    move-result v0

    return v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/libyuv/YUVColorConverter;
    .locals 3

    .prologue
    .line 1110861
    sget-object v0, Lcom/facebook/libyuv/YUVColorConverter;->b:Lcom/facebook/libyuv/YUVColorConverter;

    if-nez v0, :cond_1

    .line 1110862
    const-class v1, Lcom/facebook/libyuv/YUVColorConverter;

    monitor-enter v1

    .line 1110863
    :try_start_0
    sget-object v0, Lcom/facebook/libyuv/YUVColorConverter;->b:Lcom/facebook/libyuv/YUVColorConverter;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1110864
    if-eqz v2, :cond_0

    .line 1110865
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/libyuv/YUVColorConverter;->b(LX/0QB;)Lcom/facebook/libyuv/YUVColorConverter;

    move-result-object v0

    sput-object v0, Lcom/facebook/libyuv/YUVColorConverter;->b:Lcom/facebook/libyuv/YUVColorConverter;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1110866
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1110867
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1110868
    :cond_1
    sget-object v0, Lcom/facebook/libyuv/YUVColorConverter;->b:Lcom/facebook/libyuv/YUVColorConverter;

    return-object v0

    .line 1110869
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1110870
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/libyuv/YUVColorConverter;
    .locals 2

    .prologue
    .line 1110859
    new-instance v1, Lcom/facebook/libyuv/YUVColorConverter;

    invoke-static {p0}, LX/6Z4;->a(LX/0QB;)LX/6Z4;

    move-result-object v0

    check-cast v0, LX/6Z4;

    invoke-direct {v1, v0}, Lcom/facebook/libyuv/YUVColorConverter;-><init>(LX/6Z4;)V

    .line 1110860
    return-object v1
.end method

.method private static native nativeConvertARGBToI420(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;III)I
.end method

.method private static native nativeConvertARGBToNV21(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;III)I
.end method

.method private static native nativeConvertI420ToNV21(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;III)I
.end method

.method private static native nativeConvertNV12ToARGB(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;III)I
.end method

.method private static native nativeConvertNV21ToARGB(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;III)I
.end method

.method private static native nativeConvertNV21ToI420(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;III)I
.end method

.method private static native nativeScaleI420(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;IIILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;ILjava/nio/ByteBuffer;IIII)I
.end method
