.class public Lcom/facebook/picassolike/PicassoLikeViewStub;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:Lcom/facebook/picassolike/fresco/FrescoImpl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 999587
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/picassolike/PicassoLikeViewStub;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 999588
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 999569
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 999570
    const-class v0, Lcom/facebook/picassolike/PicassoLikeViewStub;

    invoke-static {v0, p0}, Lcom/facebook/picassolike/PicassoLikeViewStub;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 999571
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/picassolike/PicassoLikeViewStub;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/picassolike/PicassoLikeViewStub;

    invoke-static {v0}, Lcom/facebook/picassolike/fresco/FrescoImpl;->a(LX/0QB;)Lcom/facebook/picassolike/fresco/FrescoImpl;

    move-result-object v0

    check-cast v0, Lcom/facebook/picassolike/fresco/FrescoImpl;

    iput-object v0, p0, Lcom/facebook/picassolike/PicassoLikeViewStub;->a:Lcom/facebook/picassolike/fresco/FrescoImpl;

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 4

    .prologue
    .line 999572
    invoke-virtual {p0}, Lcom/facebook/picassolike/PicassoLikeViewStub;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 999573
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 999574
    check-cast v0, Landroid/view/ViewGroup;

    .line 999575
    invoke-virtual {p0}, Lcom/facebook/picassolike/PicassoLikeViewStub;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 999576
    const v3, 0x7f03043f

    move v2, v3

    .line 999577
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 999578
    invoke-virtual {p0}, Lcom/facebook/picassolike/PicassoLikeViewStub;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 999579
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 999580
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 999581
    invoke-virtual {p0}, Lcom/facebook/picassolike/PicassoLikeViewStub;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 999582
    if-eqz v3, :cond_0

    .line 999583
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 999584
    :goto_0
    return-object v1

    .line 999585
    :cond_0
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_0

    .line 999586
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PicassoLikeViewStub must have a non-null ViewGroup viewParent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
