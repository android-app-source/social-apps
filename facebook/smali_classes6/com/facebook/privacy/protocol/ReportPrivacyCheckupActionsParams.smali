.class public Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1004645
    new-instance v0, LX/5nh;

    invoke-direct {v0}, LX/5nh;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;Ljava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;",
            ">;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 1004646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004647
    iput-object p1, p0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->a:LX/0Px;

    .line 1004648
    iput-object p2, p0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->b:Ljava/lang/String;

    .line 1004649
    iput-wide p3, p0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->c:J

    .line 1004650
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 1004651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004652
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1004653
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1004654
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1004655
    const-class v0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;

    .line 1004656
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1004657
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1004658
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->a:LX/0Px;

    .line 1004659
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->b:Ljava/lang/String;

    .line 1004660
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->c:J

    .line 1004661
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1004662
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1004663
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1004664
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;

    .line 1004665
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1004666
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1004667
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004668
    iget-wide v0, p0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1004669
    return-void
.end method
