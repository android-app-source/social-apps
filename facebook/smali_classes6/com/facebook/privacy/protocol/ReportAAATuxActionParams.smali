.class public final Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/Long;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1004410
    new-instance v0, LX/5nY;

    invoke-direct {v0}, LX/5nY;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5nZ;JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 1004399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004400
    invoke-virtual {p1}, LX/5nZ;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->a:Ljava/lang/String;

    .line 1004401
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->b:Ljava/lang/Long;

    .line 1004402
    iput-object p4, p0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->c:Ljava/lang/String;

    .line 1004403
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1004405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004406
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->a:Ljava/lang/String;

    .line 1004407
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->b:Ljava/lang/Long;

    .line 1004408
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->c:Ljava/lang/String;

    .line 1004409
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1004411
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1004404
    const-class v0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "event"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "eventTime"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "source"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1004395
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004396
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1004397
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004398
    return-void
.end method
