.class public final Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/Long;

.field public final c:Ljava/lang/Long;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1004702
    new-instance v0, LX/5nn;

    invoke-direct {v0}, LX/5nn;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5no;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1004717
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004718
    invoke-virtual {p1}, LX/5no;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->a:Ljava/lang/String;

    .line 1004719
    iput-object p2, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->b:Ljava/lang/Long;

    .line 1004720
    iput-object p3, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->c:Ljava/lang/Long;

    .line 1004721
    iput-object p4, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->d:Ljava/lang/String;

    .line 1004722
    iput-object p5, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->e:Ljava/lang/String;

    .line 1004723
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1004710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004711
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->a:Ljava/lang/String;

    .line 1004712
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->b:Ljava/lang/Long;

    .line 1004713
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->c:Ljava/lang/Long;

    .line 1004714
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->d:Ljava/lang/String;

    .line 1004715
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->e:Ljava/lang/String;

    .line 1004716
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1004724
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1004709
    const-class v0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "event"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "eventTime"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "suggestionTime"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->c:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "currentPrivacyJson"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "suggestedPrivacyJson"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1004703
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004704
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1004705
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1004706
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004707
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004708
    return-void
.end method
