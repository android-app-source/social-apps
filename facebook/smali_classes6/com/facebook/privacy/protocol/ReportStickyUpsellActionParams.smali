.class public final Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/Long;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1004766
    new-instance v0, LX/5nq;

    invoke-direct {v0}, LX/5nq;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5nr;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;LX/5ns;)V
    .locals 1

    .prologue
    .line 1004767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004768
    invoke-virtual {p1}, LX/5nr;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->a:Ljava/lang/String;

    .line 1004769
    iput-object p2, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->b:Ljava/lang/Long;

    .line 1004770
    iput-object p3, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->c:Ljava/lang/String;

    .line 1004771
    iput-object p4, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->d:Ljava/lang/String;

    .line 1004772
    invoke-virtual {p5}, LX/5ns;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->e:Ljava/lang/String;

    .line 1004773
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1004774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004775
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->a:Ljava/lang/String;

    .line 1004776
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->b:Ljava/lang/Long;

    .line 1004777
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->c:Ljava/lang/String;

    .line 1004778
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->d:Ljava/lang/String;

    .line 1004779
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->e:Ljava/lang/String;

    .line 1004780
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1004781
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1004782
    const-class v0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "event"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "eventTime"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "currentPrivacyJson"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "suggestedPrivacyJson"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "surface"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1004783
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004784
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1004785
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004786
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004787
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportStickyUpsellActionParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004788
    return-void
.end method
