.class public Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1002953
    new-instance v0, LX/5mw;

    invoke-direct {v0}, LX/5mw;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1002954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1002955
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->a:Ljava/lang/String;

    .line 1002956
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->b:Ljava/lang/String;

    .line 1002957
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->c:Ljava/lang/String;

    .line 1002958
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1002959
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 1

    .prologue
    .line 1002960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1002961
    iput-object p1, p0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->a:Ljava/lang/String;

    .line 1002962
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->b:Ljava/lang/String;

    .line 1002963
    iput-object p3, p0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->c:Ljava/lang/String;

    .line 1002964
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1002965
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1002966
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1002967
    iget-object v0, p0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1002968
    iget-object v0, p0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1002969
    iget-object v0, p0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1002970
    iget-object v0, p0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1002971
    return-void
.end method
