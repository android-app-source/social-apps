.class public final Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1003312
    const-class v0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel;

    new-instance v1, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1003313
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1003314
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1003315
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1003316
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1003317
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1003318
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1003319
    if-eqz v2, :cond_2

    .line 1003320
    const-string p0, "audience_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1003321
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1003322
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1003323
    if-eqz p0, :cond_0

    .line 1003324
    const-string v0, "eligible_for_newcomer_audience_selector"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1003325
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1003326
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1003327
    if-eqz p0, :cond_1

    .line 1003328
    const-string v0, "has_default_privacy"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1003329
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1003330
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1003331
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1003332
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1003333
    check-cast p1, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel$Serializer;->a(Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel;LX/0nX;LX/0my;)V

    return-void
.end method
