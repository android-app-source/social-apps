.class public final Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel$ComposerPrivacyOptionsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel$ComposerPrivacyOptionsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1003839
    const-class v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel$ComposerPrivacyOptionsModel;

    new-instance v1, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel$ComposerPrivacyOptionsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel$ComposerPrivacyOptionsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1003840
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1003841
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel$ComposerPrivacyOptionsModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1003842
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1003843
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/2b5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1003844
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1003845
    check-cast p1, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel$ComposerPrivacyOptionsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel$ComposerPrivacyOptionsModel$Serializer;->a(Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel$ComposerPrivacyOptionsModel;LX/0nX;LX/0my;)V

    return-void
.end method
