.class public Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1003031
    new-instance v0, LX/5my;

    invoke-direct {v0}, LX/5my;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 1003043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1003044
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams;->a:Ljava/lang/String;

    .line 1003045
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1003046
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1003047
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1003048
    const-class v0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;

    .line 1003049
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1003050
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1003051
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams;->b:LX/0Px;

    .line 1003052
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1003039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1003040
    iput-object p1, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams;->a:Ljava/lang/String;

    .line 1003041
    iput-object p2, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams;->b:LX/0Px;

    .line 1003042
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1003038
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1003032
    iget-object v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1003033
    iget-object v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1003034
    iget-object v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;

    .line 1003035
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1003036
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1003037
    :cond_0
    return-void
.end method
