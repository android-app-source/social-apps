.class public final Lcom/facebook/privacy/protocol/ReportNASActionParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/ReportNASActionParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/Long;

.field public final c:Z

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1004524
    new-instance v0, LX/5ne;

    invoke-direct {v0}, LX/5ne;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5nf;JZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1004502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004503
    invoke-virtual {p1}, LX/5nf;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->a:Ljava/lang/String;

    .line 1004504
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->b:Ljava/lang/Long;

    .line 1004505
    iput-boolean p4, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->c:Z

    .line 1004506
    iput-object p5, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->d:Ljava/lang/String;

    .line 1004507
    iput-object p6, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->e:Ljava/lang/String;

    .line 1004508
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1004509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004510
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->a:Ljava/lang/String;

    .line 1004511
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->b:Ljava/lang/Long;

    .line 1004512
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->c:Z

    .line 1004513
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->d:Ljava/lang/String;

    .line 1004514
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->e:Ljava/lang/String;

    .line 1004515
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1004516
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1004517
    const-class v0, Lcom/facebook/privacy/protocol/ReportNASActionParams;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "event"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "eventTime"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "isDefault"

    iget-boolean v2, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->c:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "privacyJson"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "source"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1004518
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004519
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1004520
    iget-boolean v0, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1004521
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004522
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportNASActionParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004523
    return-void
.end method
