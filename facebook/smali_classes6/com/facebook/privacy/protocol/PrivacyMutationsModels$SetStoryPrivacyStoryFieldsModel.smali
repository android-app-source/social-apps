.class public final Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x21dfb379
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1004202
    const-class v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1004149
    const-class v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1004200
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1004201
    return-void
.end method

.method private a(Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;)V
    .locals 3
    .param p1    # Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1004194
    iput-object p1, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->f:Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    .line 1004195
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1004196
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1004197
    if-eqz v0, :cond_0

    .line 1004198
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1004199
    :cond_0
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1004192
    iget-object v0, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->e:Ljava/lang/String;

    .line 1004193
    iget-object v0, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1004184
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1004185
    invoke-direct {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1004186
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->j()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1004187
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1004188
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1004189
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1004190
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1004191
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1004176
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1004177
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->j()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1004178
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->j()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    .line 1004179
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->j()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1004180
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    .line 1004181
    iput-object v0, v1, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->f:Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    .line 1004182
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1004183
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1004175
    new-instance v0, LX/5nO;

    invoke-direct {v0, p1}, LX/5nO;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1004203
    invoke-direct {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1004167
    const-string v0, "privacy_scope.type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1004168
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->j()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    move-result-object v0

    .line 1004169
    if-eqz v0, :cond_0

    .line 1004170
    invoke-virtual {v0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1004171
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1004172
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1004173
    :goto_0
    return-void

    .line 1004174
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1004164
    const-string v0, "privacy_scope"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1004165
    check-cast p2, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    invoke-direct {p0, p2}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->a(Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;)V

    .line 1004166
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1004155
    const-string v0, "privacy_scope.type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1004156
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->j()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    move-result-object v0

    .line 1004157
    if-eqz v0, :cond_0

    .line 1004158
    if-eqz p3, :cond_1

    .line 1004159
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    .line 1004160
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;->a(Ljava/lang/String;)V

    .line 1004161
    iput-object v0, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->f:Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    .line 1004162
    :cond_0
    :goto_0
    return-void

    .line 1004163
    :cond_1
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1004152
    new-instance v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    invoke-direct {v0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;-><init>()V

    .line 1004153
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1004154
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1004151
    const v0, 0x1ff374e4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1004150
    const v0, 0x4c808d5

    return v0
.end method

.method public final j()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1004147
    iget-object v0, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->f:Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->f:Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    .line 1004148
    iget-object v0, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->f:Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    return-object v0
.end method
