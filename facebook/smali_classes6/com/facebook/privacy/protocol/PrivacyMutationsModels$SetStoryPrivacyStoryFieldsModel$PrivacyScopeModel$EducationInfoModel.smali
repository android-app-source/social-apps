.class public final Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x261a6a67
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1004074
    const-class v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1004073
    const-class v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1004094
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1004095
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1004088
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1004089
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1004090
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1004091
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1004092
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1004093
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1004080
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1004081
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1004082
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1004083
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1004084
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;

    .line 1004085
    iput-object v0, v1, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1004086
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1004087
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1004096
    iget-object v0, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1004097
    iget-object v0, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1004077
    new-instance v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;

    invoke-direct {v0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;-><init>()V

    .line 1004078
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1004079
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1004076
    const v0, 0x551a2d8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1004075
    const v0, 0x462969ee

    return v0
.end method
