.class public final Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

.field public d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1003011
    new-instance v0, LX/5mz;

    invoke-direct {v0}, LX/5mz;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1003012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1003013
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;->a:Ljava/lang/String;

    .line 1003014
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;->b:J

    .line 1003015
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1003016
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;->c:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 1003017
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;->d:Ljava/lang/String;

    .line 1003018
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1003019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1003020
    iput-object p1, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;->a:Ljava/lang/String;

    .line 1003021
    iput-wide p2, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;->b:J

    .line 1003022
    iput-object p4, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;->c:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 1003023
    iput-object p5, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;->d:Ljava/lang/String;

    .line 1003024
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1003025
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1003026
    iget-object v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1003027
    iget-wide v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1003028
    iget-object v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;->c:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1003029
    iget-object v0, p0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1003030
    return-void
.end method
