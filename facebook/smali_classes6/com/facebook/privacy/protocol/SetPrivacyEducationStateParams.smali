.class public final Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1004856
    new-instance v0, LX/5nw;

    invoke-direct {v0}, LX/5nw;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1004844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004845
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->a:Ljava/lang/String;

    .line 1004846
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->b:Ljava/lang/String;

    .line 1004847
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->c:Ljava/lang/String;

    .line 1004848
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->d:Ljava/lang/Long;

    .line 1004849
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/5nx;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 1004850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004851
    iput-object p1, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->a:Ljava/lang/String;

    .line 1004852
    invoke-virtual {p2}, LX/5nx;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->b:Ljava/lang/String;

    .line 1004853
    const-string v0, "fb4a_permalink"

    iput-object v0, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->c:Ljava/lang/String;

    .line 1004854
    iput-object p3, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->d:Ljava/lang/Long;

    .line 1004855
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1004843
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1004842
    const-class v0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "educationType"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "operation"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "surface"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "eventTime"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->d:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1004837
    iget-object v0, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004838
    iget-object v0, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004839
    iget-object v0, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004840
    iget-object v0, p0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1004841
    return-void
.end method
