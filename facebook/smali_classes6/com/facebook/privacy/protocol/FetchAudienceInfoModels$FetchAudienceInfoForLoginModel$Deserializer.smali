.class public final Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1003267
    const-class v0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel;

    new-instance v1, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1003268
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1003269
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1003270
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1003271
    const/4 v2, 0x0

    .line 1003272
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1003273
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1003274
    :goto_0
    move v1, v2

    .line 1003275
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1003276
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1003277
    new-instance v1, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel;

    invoke-direct {v1}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoForLoginModel;-><init>()V

    .line 1003278
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1003279
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1003280
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1003281
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1003282
    :cond_0
    return-object v1

    .line 1003283
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1003284
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1003285
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1003286
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1003287
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1003288
    const-string v4, "audience_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1003289
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1003290
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_b

    .line 1003291
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1003292
    :goto_2
    move v1, v3

    .line 1003293
    goto :goto_1

    .line 1003294
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1003295
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1003296
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1003297
    :cond_5
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_8

    .line 1003298
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1003299
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1003300
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_5

    if-eqz v8, :cond_5

    .line 1003301
    const-string p0, "eligible_for_newcomer_audience_selector"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1003302
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v5

    move v7, v5

    move v5, v4

    goto :goto_3

    .line 1003303
    :cond_6
    const-string p0, "has_default_privacy"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1003304
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v6, v1

    move v1, v4

    goto :goto_3

    .line 1003305
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1003306
    :cond_8
    const/4 v8, 0x2

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1003307
    if-eqz v5, :cond_9

    .line 1003308
    invoke-virtual {v0, v3, v7}, LX/186;->a(IZ)V

    .line 1003309
    :cond_9
    if-eqz v1, :cond_a

    .line 1003310
    invoke-virtual {v0, v4, v6}, LX/186;->a(IZ)V

    .line 1003311
    :cond_a
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_b
    move v1, v3

    move v5, v3

    move v6, v3

    move v7, v3

    goto :goto_3
.end method
