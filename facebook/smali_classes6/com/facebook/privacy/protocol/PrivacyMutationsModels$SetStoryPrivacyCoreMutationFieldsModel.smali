.class public final Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x599dd5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1004016
    const-class v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1004015
    const-class v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1004013
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1004014
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1004007
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1004008
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;->a()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1004009
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1004010
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1004011
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1004012
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1004017
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1004018
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;->a()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1004019
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;->a()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    .line 1004020
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;->a()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1004021
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;

    .line 1004022
    iput-object v0, v1, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;->e:Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    .line 1004023
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1004024
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1004005
    iget-object v0, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;->e:Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;->e:Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    .line 1004006
    iget-object v0, p0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;->e:Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1004000
    new-instance v0, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;-><init>()V

    .line 1004001
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1004002
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1004004
    const v0, -0x1dfc1ad0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1004003
    const v0, -0x728c38c9

    return v0
.end method
