.class public final Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/Long;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1004342
    new-instance v0, LX/5nV;

    invoke-direct {v0}, LX/5nV;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5nW;JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 1004337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004338
    invoke-virtual {p1}, LX/5nW;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->a:Ljava/lang/String;

    .line 1004339
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->b:Ljava/lang/Long;

    .line 1004340
    iput-object p4, p0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->c:Ljava/lang/String;

    .line 1004341
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1004343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004344
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->a:Ljava/lang/String;

    .line 1004345
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->b:Ljava/lang/Long;

    .line 1004346
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->c:Ljava/lang/String;

    .line 1004347
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1004336
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1004335
    const-class v0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "event"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "eventTime"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "eventSource"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1004331
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004332
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1004333
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004334
    return-void
.end method
