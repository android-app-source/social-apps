.class public final Lcom/facebook/privacy/protocol/SetComposerStickyPrivacyParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/SetComposerStickyPrivacyParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1004802
    new-instance v0, LX/5nu;

    invoke-direct {v0}, LX/5nu;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/SetComposerStickyPrivacyParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1004803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004804
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/SetComposerStickyPrivacyParams;->a:Ljava/lang/String;

    .line 1004805
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1004806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004807
    iput-object p1, p0, Lcom/facebook/privacy/protocol/SetComposerStickyPrivacyParams;->a:Ljava/lang/String;

    .line 1004808
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1004809
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1004810
    const-class v0, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "privacyJson"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/SetComposerStickyPrivacyParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1004811
    iget-object v0, p0, Lcom/facebook/privacy/protocol/SetComposerStickyPrivacyParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004812
    return-void
.end method
