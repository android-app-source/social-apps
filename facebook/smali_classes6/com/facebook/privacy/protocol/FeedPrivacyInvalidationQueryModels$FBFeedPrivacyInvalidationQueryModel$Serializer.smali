.class public final Lcom/facebook/privacy/protocol/FeedPrivacyInvalidationQueryModels$FBFeedPrivacyInvalidationQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/privacy/protocol/FeedPrivacyInvalidationQueryModels$FBFeedPrivacyInvalidationQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1003139
    const-class v0, Lcom/facebook/privacy/protocol/FeedPrivacyInvalidationQueryModels$FBFeedPrivacyInvalidationQueryModel;

    new-instance v1, Lcom/facebook/privacy/protocol/FeedPrivacyInvalidationQueryModels$FBFeedPrivacyInvalidationQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/privacy/protocol/FeedPrivacyInvalidationQueryModels$FBFeedPrivacyInvalidationQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1003140
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1003124
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/privacy/protocol/FeedPrivacyInvalidationQueryModels$FBFeedPrivacyInvalidationQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1003126
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1003127
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 1003128
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1003129
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 1003130
    if-eqz p0, :cond_0

    .line 1003131
    const-string p0, "__type__"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1003132
    invoke-static {v1, v0, p2, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1003133
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1003134
    if-eqz p0, :cond_1

    .line 1003135
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1003136
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1003137
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1003138
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1003125
    check-cast p1, Lcom/facebook/privacy/protocol/FeedPrivacyInvalidationQueryModels$FBFeedPrivacyInvalidationQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/privacy/protocol/FeedPrivacyInvalidationQueryModels$FBFeedPrivacyInvalidationQueryModel$Serializer;->a(Lcom/facebook/privacy/protocol/FeedPrivacyInvalidationQueryModels$FBFeedPrivacyInvalidationQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
