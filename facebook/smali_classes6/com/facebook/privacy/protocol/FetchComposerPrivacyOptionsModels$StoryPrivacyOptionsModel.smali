.class public final Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7b13d9d4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1003811
    const-class v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1003808
    const-class v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1003809
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1003810
    return-void
.end method

.method private a(Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;)V
    .locals 3
    .param p1    # Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1003820
    iput-object p1, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;->e:Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    .line 1003821
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1003822
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1003823
    if-eqz v0, :cond_0

    .line 1003824
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1003825
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1003812
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;->e:Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;->e:Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    .line 1003813
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;->e:Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1003814
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1003815
    invoke-direct {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;->j()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1003816
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1003817
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1003818
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1003819
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1003799
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1003800
    invoke-direct {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;->j()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1003801
    invoke-direct {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;->j()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    .line 1003802
    invoke-direct {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;->j()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1003803
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;

    .line 1003804
    iput-object v0, v1, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;->e:Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    .line 1003805
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1003806
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1003807
    new-instance v0, LX/5nF;

    invoke-direct {v0, p1}, LX/5nF;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1003798
    invoke-direct {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;->j()Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1003796
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1003797
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1003793
    const-string v0, "privacy_scope"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1003794
    check-cast p2, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;

    invoke-direct {p0, p2}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;->a(Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel$PrivacyScopeModel;)V

    .line 1003795
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1003792
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1003787
    new-instance v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;

    invoke-direct {v0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$StoryPrivacyOptionsModel;-><init>()V

    .line 1003788
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1003789
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1003791
    const v0, -0x1c46defd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1003790
    const v0, 0x4c808d5

    return v0
.end method
