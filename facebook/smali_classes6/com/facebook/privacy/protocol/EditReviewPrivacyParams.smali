.class public Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1003070
    new-instance v0, LX/5n1;

    invoke-direct {v0}, LX/5n1;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1003075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1003076
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;->a:J

    .line 1003077
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;->b:Ljava/lang/String;

    .line 1003078
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 1003079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1003080
    iput-wide p2, p0, Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;->a:J

    .line 1003081
    iput-object p1, p0, Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;->b:Ljava/lang/String;

    .line 1003082
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1003074
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1003071
    iget-wide v0, p0, Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1003072
    iget-object v0, p0, Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1003073
    return-void
.end method
