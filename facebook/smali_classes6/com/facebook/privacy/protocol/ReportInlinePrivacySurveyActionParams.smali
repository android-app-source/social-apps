.class public Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1004444
    new-instance v0, LX/5nb;

    invoke-direct {v0}, LX/5nb;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5nc;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1004445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004446
    invoke-virtual {p1}, LX/5nc;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->a:Ljava/lang/String;

    .line 1004447
    iput-wide p2, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->b:J

    .line 1004448
    iput-object p4, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->c:Ljava/lang/String;

    .line 1004449
    iput-object p5, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->d:Ljava/lang/String;

    .line 1004450
    iput-object p6, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->e:Ljava/lang/String;

    .line 1004451
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1004452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004453
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->a:Ljava/lang/String;

    .line 1004454
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->b:J

    .line 1004455
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->c:Ljava/lang/String;

    .line 1004456
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->d:Ljava/lang/String;

    .line 1004457
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->e:Ljava/lang/String;

    .line 1004458
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1004459
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1004460
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004461
    iget-wide v0, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1004462
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004463
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004464
    iget-object v0, p0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1004465
    return-void
.end method
