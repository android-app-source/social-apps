.class public Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/5mv;

.field public c:LX/5mu;

.field public d:J

.field public e:Ljava/lang/String;

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1002949
    new-instance v0, LX/5mt;

    invoke-direct {v0}, LX/5mt;-><init>()V

    sput-object v0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1002924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1002925
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->a:Ljava/lang/String;

    .line 1002926
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5mv;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->b:LX/5mv;

    .line 1002927
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5mu;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->c:LX/5mu;

    .line 1002928
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->d:J

    .line 1002929
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->e:Ljava/lang/String;

    .line 1002930
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->f:Z

    .line 1002931
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/5mv;LX/5mu;JLjava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1002941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1002942
    iput-object p1, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->a:Ljava/lang/String;

    .line 1002943
    iput-object p2, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->b:LX/5mv;

    .line 1002944
    iput-object p3, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->c:LX/5mu;

    .line 1002945
    iput-wide p4, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->d:J

    .line 1002946
    iput-object p6, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->e:Ljava/lang/String;

    .line 1002947
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->f:Z

    .line 1002948
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1002940
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1002939
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "fbid"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "caller"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->b:LX/5mv;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "albumType"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->c:LX/5mu;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "clientTime"

    iget-wide v2, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->d:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    const-string v1, "privacyJson"

    iget-object v2, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "capsPrivacy"

    iget-boolean v2, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->f:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1002932
    iget-object v0, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1002933
    iget-object v0, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->b:LX/5mv;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1002934
    iget-object v0, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->c:LX/5mu;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1002935
    iget-wide v0, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1002936
    iget-object v0, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1002937
    iget-boolean v0, p0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1002938
    return-void
.end method
