.class public final Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 911332
    const-class v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;

    new-instance v1, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 911333
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 911334
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 911335
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 911336
    const/4 v2, 0x0

    .line 911337
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_c

    .line 911338
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 911339
    :goto_0
    move v1, v2

    .line 911340
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 911341
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 911342
    new-instance v1, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;

    invoke-direct {v1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;-><init>()V

    .line 911343
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 911344
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 911345
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 911346
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 911347
    :cond_0
    return-object v1

    .line 911348
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 911349
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_b

    .line 911350
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 911351
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 911352
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v10, :cond_2

    .line 911353
    const-string p0, "__type__"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 911354
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_1

    .line 911355
    :cond_4
    const-string p0, "employer"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 911356
    invoke-static {p1, v0}, LX/5Pz;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 911357
    :cond_5
    const-string p0, "id"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 911358
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 911359
    :cond_6
    const-string p0, "redirection_info"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 911360
    invoke-static {p1, v0}, LX/5Q3;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 911361
    :cond_7
    const-string p0, "school"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 911362
    invoke-static {p1, v0}, LX/5Q1;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 911363
    :cond_8
    const-string p0, "school_class"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 911364
    invoke-static {p1, v0}, LX/5Q0;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 911365
    :cond_9
    const-string p0, "url"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 911366
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 911367
    :cond_a
    const-string p0, "work_project"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 911368
    invoke-static {p1, v0}, LX/5Q2;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 911369
    :cond_b
    const/16 v10, 0x8

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 911370
    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 911371
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 911372
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 911373
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 911374
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 911375
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 911376
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 911377
    const/4 v2, 0x7

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 911378
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_1
.end method
