.class public final Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 911522
    const-class v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;

    new-instance v1, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 911523
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 911524
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 911525
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 911526
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 911527
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 911528
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 911529
    if-eqz v2, :cond_0

    .line 911530
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911531
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 911532
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 911533
    if-eqz v2, :cond_1

    .line 911534
    const-string p0, "employer"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911535
    invoke-static {v1, v2, p1}, LX/5Pz;->a(LX/15i;ILX/0nX;)V

    .line 911536
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 911537
    if-eqz v2, :cond_2

    .line 911538
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911539
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 911540
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 911541
    if-eqz v2, :cond_3

    .line 911542
    const-string p0, "redirection_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911543
    invoke-static {v1, v2, p1, p2}, LX/5Q3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 911544
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 911545
    if-eqz v2, :cond_4

    .line 911546
    const-string p0, "school"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911547
    invoke-static {v1, v2, p1}, LX/5Q1;->a(LX/15i;ILX/0nX;)V

    .line 911548
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 911549
    if-eqz v2, :cond_5

    .line 911550
    const-string p0, "school_class"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911551
    invoke-static {v1, v2, p1}, LX/5Q0;->a(LX/15i;ILX/0nX;)V

    .line 911552
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 911553
    if-eqz v2, :cond_6

    .line 911554
    const-string p0, "url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911555
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 911556
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 911557
    if-eqz v2, :cond_7

    .line 911558
    const-string p0, "work_project"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911559
    invoke-static {v1, v2, p1}, LX/5Q2;->a(LX/15i;ILX/0nX;)V

    .line 911560
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 911561
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 911562
    check-cast p1, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel$Serializer;->a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
