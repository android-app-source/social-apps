.class public final Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 911901
    const-class v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel;

    new-instance v1, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 911902
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 911903
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 911869
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 911870
    const/4 v2, 0x0

    .line 911871
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 911872
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 911873
    :goto_0
    move v1, v2

    .line 911874
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 911875
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 911876
    new-instance v1, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel;

    invoke-direct {v1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel;-><init>()V

    .line 911877
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 911878
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 911879
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 911880
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 911881
    :cond_0
    return-object v1

    .line 911882
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 911883
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_6

    .line 911884
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 911885
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 911886
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v6, :cond_2

    .line 911887
    const-string p0, "image_ranges"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 911888
    invoke-static {p1, v0}, LX/5Q5;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 911889
    :cond_3
    const-string p0, "inline_style_ranges"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 911890
    invoke-static {p1, v0}, LX/5Q8;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 911891
    :cond_4
    const-string p0, "ranges"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 911892
    invoke-static {p1, v0}, LX/5Q7;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 911893
    :cond_5
    const-string p0, "text"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 911894
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 911895
    :cond_6
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 911896
    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 911897
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 911898
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 911899
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 911900
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    goto :goto_1
.end method
