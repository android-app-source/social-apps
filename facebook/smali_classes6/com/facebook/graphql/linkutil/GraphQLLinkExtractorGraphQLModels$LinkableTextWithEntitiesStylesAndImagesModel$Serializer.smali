.class public final Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 911904
    const-class v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel;

    new-instance v1, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 911905
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 911906
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 911907
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 911908
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 911909
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 911910
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 911911
    if-eqz v2, :cond_4

    .line 911912
    const-string v3, "image_ranges"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911913
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 911914
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 911915
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    const/4 p0, 0x0

    .line 911916
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 911917
    invoke-virtual {v1, v4, p0}, LX/15i;->g(II)I

    move-result v5

    .line 911918
    if-eqz v5, :cond_0

    .line 911919
    const-string v6, "entity_with_image"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911920
    invoke-static {v1, v5, p1, p2}, LX/5Q4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 911921
    :cond_0
    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5, p0}, LX/15i;->a(III)I

    move-result v5

    .line 911922
    if-eqz v5, :cond_1

    .line 911923
    const-string v6, "length"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911924
    invoke-virtual {p1, v5}, LX/0nX;->b(I)V

    .line 911925
    :cond_1
    const/4 v5, 0x2

    invoke-virtual {v1, v4, v5, p0}, LX/15i;->a(III)I

    move-result v5

    .line 911926
    if-eqz v5, :cond_2

    .line 911927
    const-string v6, "offset"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911928
    invoke-virtual {p1, v5}, LX/0nX;->b(I)V

    .line 911929
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 911930
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 911931
    :cond_3
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 911932
    :cond_4
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 911933
    if-eqz v2, :cond_5

    .line 911934
    const-string v3, "inline_style_ranges"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911935
    invoke-static {v1, v2, p1, p2}, LX/5Q8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 911936
    :cond_5
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 911937
    if-eqz v2, :cond_6

    .line 911938
    const-string v3, "ranges"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911939
    invoke-static {v1, v2, p1, p2}, LX/5Q7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 911940
    :cond_6
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 911941
    if-eqz v2, :cond_7

    .line 911942
    const-string v3, "text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 911943
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 911944
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 911945
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 911946
    check-cast p1, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel$Serializer;->a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesStylesAndImagesModel;LX/0nX;LX/0my;)V

    return-void
.end method
