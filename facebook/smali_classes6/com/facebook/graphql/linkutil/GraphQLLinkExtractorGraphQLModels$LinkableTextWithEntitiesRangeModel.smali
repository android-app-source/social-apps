.class public final Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/5Ph;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6e668a5f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 911868
    const-class v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 911867
    const-class v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 911865
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 911866
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 911862
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 911863
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 911864
    return-void
.end method

.method public static a(LX/5Ph;)Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;
    .locals 8

    .prologue
    .line 911806
    if-nez p0, :cond_0

    .line 911807
    const/4 p0, 0x0

    .line 911808
    :goto_0
    return-object p0

    .line 911809
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;

    if-eqz v0, :cond_1

    .line 911810
    check-cast p0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;

    goto :goto_0

    .line 911811
    :cond_1
    new-instance v0, LX/5Pv;

    invoke-direct {v0}, LX/5Pv;-><init>()V

    .line 911812
    invoke-interface {p0}, LX/5Ph;->a()LX/1yA;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;->a(LX/1yA;)Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    move-result-object v1

    iput-object v1, v0, LX/5Pv;->a:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    .line 911813
    invoke-interface {p0}, LX/5Ph;->b()I

    move-result v1

    iput v1, v0, LX/5Pv;->b:I

    .line 911814
    invoke-interface {p0}, LX/5Ph;->c()I

    move-result v1

    iput v1, v0, LX/5Pv;->c:I

    .line 911815
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 911816
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 911817
    iget-object v3, v0, LX/5Pv;->a:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 911818
    const/4 v5, 0x3

    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 911819
    invoke-virtual {v2, v7, v3}, LX/186;->b(II)V

    .line 911820
    iget v3, v0, LX/5Pv;->b:I

    invoke-virtual {v2, v6, v3, v7}, LX/186;->a(III)V

    .line 911821
    const/4 v3, 0x2

    iget v5, v0, LX/5Pv;->c:I

    invoke-virtual {v2, v3, v5, v7}, LX/186;->a(III)V

    .line 911822
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 911823
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 911824
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 911825
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 911826
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 911827
    new-instance v3, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;

    invoke-direct {v3, v2}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;-><init>(LX/15i;)V

    .line 911828
    move-object p0, v3

    .line 911829
    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 911860
    iget-object v0, p0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    iput-object v0, p0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    .line 911861
    iget-object v0, p0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 911852
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 911853
    invoke-direct {p0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 911854
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 911855
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 911856
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 911857
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 911858
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 911859
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 911844
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 911845
    invoke-direct {p0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 911846
    invoke-direct {p0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    .line 911847
    invoke-direct {p0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 911848
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;

    .line 911849
    iput-object v0, v1, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    .line 911850
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 911851
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/1yA;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 911843
    invoke-direct {p0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 911839
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 911840
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->f:I

    .line 911841
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->g:I

    .line 911842
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 911837
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 911838
    iget v0, p0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 911834
    new-instance v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;

    invoke-direct {v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;-><init>()V

    .line 911835
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 911836
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 911832
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 911833
    iget v0, p0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesRangeModel;->g:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 911831
    const v0, -0x168bb064

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 911830
    const v0, -0x3d10ccb9

    return v0
.end method
