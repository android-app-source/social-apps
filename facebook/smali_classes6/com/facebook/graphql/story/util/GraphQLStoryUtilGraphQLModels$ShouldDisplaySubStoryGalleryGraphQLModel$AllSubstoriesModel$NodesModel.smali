.class public final Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/6XG;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5bd3fd98
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1108155
    const-class v0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1108192
    const-class v0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1108190
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1108191
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1108184
    iput-object p1, p0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel;->e:Ljava/util/List;

    .line 1108185
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1108186
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1108187
    if-eqz v0, :cond_0

    .line 1108188
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 1108189
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1108178
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1108179
    invoke-virtual {p0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1108180
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1108181
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1108182
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1108183
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1108170
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1108171
    invoke-virtual {p0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1108172
    invoke-virtual {p0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1108173
    if-eqz v1, :cond_0

    .line 1108174
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel;

    .line 1108175
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel;->e:Ljava/util/List;

    .line 1108176
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1108177
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1108169
    new-instance v0, LX/6XL;

    invoke-direct {v0, p1}, LX/6XL;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1108167
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1108168
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1108164
    const-string v0, "attachments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1108165
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel;->a(Ljava/util/List;)V

    .line 1108166
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1108163
    return-void
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1108161
    iget-object v0, p0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel;->e:Ljava/util/List;

    .line 1108162
    iget-object v0, p0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1108158
    new-instance v0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel;-><init>()V

    .line 1108159
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1108160
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1108157
    const v0, 0x371dc1d7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1108156
    const v0, 0x4c808d5

    return v0
.end method
