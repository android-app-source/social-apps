.class public final Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1108266
    const-class v0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel;

    new-instance v1, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1108267
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1108268
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1108269
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1108270
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1108271
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1108272
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1108273
    if-eqz v2, :cond_1

    .line 1108274
    const-string v3, "actors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1108275
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1108276
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_0

    .line 1108277
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/6XP;->a(LX/15i;ILX/0nX;)V

    .line 1108278
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1108279
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1108280
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1108281
    if-eqz v2, :cond_2

    .line 1108282
    const-string v3, "all_substories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1108283
    invoke-static {v1, v2, p1, p2}, LX/6XS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1108284
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1108285
    if-eqz v2, :cond_3

    .line 1108286
    const-string v3, "has_comprehensive_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1108287
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1108288
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1108289
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1108290
    check-cast p1, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$Serializer;->a(Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
