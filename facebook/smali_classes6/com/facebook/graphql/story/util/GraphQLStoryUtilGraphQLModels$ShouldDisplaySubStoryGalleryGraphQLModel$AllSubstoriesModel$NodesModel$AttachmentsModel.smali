.class public final Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/6XF;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5e1acf75
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1108124
    const-class v0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1108123
    const-class v0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1108121
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1108122
    return-void
.end method

.method private j()Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1108119
    iget-object v0, p0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->f:Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    iput-object v0, p0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->f:Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    .line 1108120
    iget-object v0, p0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->f:Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1108111
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1108112
    invoke-virtual {p0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->c(Ljava/util/List;)I

    move-result v0

    .line 1108113
    invoke-direct {p0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->j()Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1108114
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1108115
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1108116
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1108117
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1108118
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1108125
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1108126
    invoke-direct {p0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->j()Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1108127
    invoke-direct {p0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->j()Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    .line 1108128
    invoke-direct {p0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->j()Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1108129
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;

    .line 1108130
    iput-object v0, v1, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->f:Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    .line 1108131
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1108132
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1108110
    invoke-direct {p0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->j()Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1108108
    iget-object v0, p0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->e:Ljava/util/List;

    .line 1108109
    iget-object v0, p0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1108103
    new-instance v0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;

    invoke-direct {v0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;-><init>()V

    .line 1108104
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1108105
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1108107
    const v0, 0x63c1e6ab

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1108106
    const v0, -0x4b900828

    return v0
.end method
