.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xdcdc46e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 999894
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 999893
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 999891
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 999892
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 999888
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 999889
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 999890
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 999886
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;

    .line 999887
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 999878
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 999879
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 999880
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 999881
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 999882
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 999883
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 999884
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 999885
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 999895
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 999896
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 999897
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;

    .line 999898
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 999899
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;

    .line 999900
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;

    .line 999901
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 999902
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 999877
    new-instance v0, LX/5m7;

    invoke-direct {v0, p1}, LX/5m7;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 999876
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 999874
    invoke-virtual {p2}, LX/18L;->a()V

    .line 999875
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 999873
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 999870
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;-><init>()V

    .line 999871
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 999872
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 999869
    const v0, 0x65ed3bfa

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 999868
    const v0, 0x3c2b9d5

    return v0
.end method
