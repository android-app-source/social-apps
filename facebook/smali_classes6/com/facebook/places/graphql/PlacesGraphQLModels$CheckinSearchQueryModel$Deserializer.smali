.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1000681
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;

    new-instance v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1000682
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1000683
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1000684
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1000685
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1000686
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1000687
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1000688
    :goto_0
    move v1, v2

    .line 1000689
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1000690
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1000691
    new-instance v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;

    invoke-direct {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;-><init>()V

    .line 1000692
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1000693
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1000694
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1000695
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1000696
    :cond_0
    return-object v1

    .line 1000697
    :cond_1
    const-string p0, "suggest_place_when_people_tagging"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1000698
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v5, v1

    move v1, v3

    .line 1000699
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_6

    .line 1000700
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1000701
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1000702
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v8, :cond_2

    .line 1000703
    const-string p0, "closest_city"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1000704
    invoke-static {p1, v0}, LX/5mb;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1000705
    :cond_3
    const-string p0, "place_results"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1000706
    invoke-static {p1, v0}, LX/5md;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1000707
    :cond_4
    const-string p0, "tracking"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1000708
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1000709
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1000710
    :cond_6
    const/4 v8, 0x4

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1000711
    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1000712
    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1000713
    if-eqz v1, :cond_7

    .line 1000714
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->a(IZ)V

    .line 1000715
    :cond_7
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1000716
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_1
.end method
