.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1001545
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;

    new-instance v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1001546
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1001547
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1001548
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1001549
    const/4 v11, 0x0

    .line 1001550
    const/4 v10, 0x0

    .line 1001551
    const/4 v9, 0x0

    .line 1001552
    const/4 v8, 0x0

    .line 1001553
    const/4 v7, 0x0

    .line 1001554
    const/4 v6, 0x0

    .line 1001555
    const/4 v5, 0x0

    .line 1001556
    const/4 v4, 0x0

    .line 1001557
    const/4 v3, 0x0

    .line 1001558
    const/4 v2, 0x0

    .line 1001559
    const/4 v1, 0x0

    .line 1001560
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, p0, :cond_2

    .line 1001561
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1001562
    const/4 v1, 0x0

    .line 1001563
    :goto_0
    move v1, v1

    .line 1001564
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1001565
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1001566
    new-instance v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;

    invoke-direct {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;-><init>()V

    .line 1001567
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1001568
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1001569
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1001570
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1001571
    :cond_0
    return-object v1

    .line 1001572
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1001573
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_e

    .line 1001574
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1001575
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1001576
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 1001577
    const-string p0, "__type__"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1001578
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v11

    goto :goto_1

    .line 1001579
    :cond_4
    const-string p0, "address"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1001580
    invoke-static {p1, v0}, LX/5ml;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1001581
    :cond_5
    const-string p0, "all_phones"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1001582
    invoke-static {p1, v0}, LX/5mm;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1001583
    :cond_6
    const-string p0, "category_names"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1001584
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1001585
    :cond_7
    const-string p0, "friends_who_visited"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1001586
    invoke-static {p1, v0}, LX/5mo;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1001587
    :cond_8
    const-string p0, "full_name"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1001588
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1001589
    :cond_9
    const-string p0, "id"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1001590
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 1001591
    :cond_a
    const-string p0, "location"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    .line 1001592
    invoke-static {p1, v0}, LX/5mp;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1001593
    :cond_b
    const-string p0, "name"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 1001594
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1001595
    :cond_c
    const-string p0, "profile_picture"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_d

    .line 1001596
    invoke-static {p1, v0}, LX/5mq;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1001597
    :cond_d
    const-string p0, "websites"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1001598
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 1001599
    :cond_e
    const/16 v12, 0xb

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1001600
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1001601
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1001602
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1001603
    const/4 v9, 0x3

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1001604
    const/4 v8, 0x4

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1001605
    const/4 v7, 0x5

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1001606
    const/4 v6, 0x6

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1001607
    const/4 v5, 0x7

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1001608
    const/16 v4, 0x8

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1001609
    const/16 v3, 0x9

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 1001610
    const/16 v2, 0xa

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1001611
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
