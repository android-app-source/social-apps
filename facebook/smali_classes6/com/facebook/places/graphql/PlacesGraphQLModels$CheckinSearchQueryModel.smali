.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1d411aa2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1000881
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1000880
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1000878
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1000879
    return-void
.end method

.method private j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000876
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    .line 1000877
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    return-object v0
.end method

.method private k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000874
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    .line 1000875
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1000863
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1000864
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1000865
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1000866
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1000867
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1000868
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1000869
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1000870
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1000871
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1000872
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1000873
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1000850
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1000851
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1000852
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    .line 1000853
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1000854
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;

    .line 1000855
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    .line 1000856
    :cond_0
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1000857
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    .line 1000858
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1000859
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;

    .line 1000860
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    .line 1000861
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1000862
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000882
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1000837
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1000838
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->g:Z

    .line 1000839
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1000847
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;-><init>()V

    .line 1000848
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1000849
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000846
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1000844
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1000845
    iget-boolean v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->g:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000842
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->h:Ljava/lang/String;

    .line 1000843
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1000841
    const v0, 0x215ad63d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1000840
    const v0, -0x3e224e4d

    return v0
.end method
