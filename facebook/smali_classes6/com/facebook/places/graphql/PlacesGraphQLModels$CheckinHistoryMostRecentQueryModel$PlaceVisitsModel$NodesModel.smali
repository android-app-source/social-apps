.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1df3bede
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 999793
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 999818
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 999816
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 999817
    return-void
.end method

.method private j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 999814
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 999815
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 999808
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 999809
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 999810
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 999811
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 999812
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 999813
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 999800
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 999801
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 999802
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 999803
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 999804
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;

    .line 999805
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 999806
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 999807
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 999799
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 999796
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;-><init>()V

    .line 999797
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 999798
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 999795
    const v0, -0x5856fb3e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 999794
    const v0, 0x6673eca4

    return v0
.end method
