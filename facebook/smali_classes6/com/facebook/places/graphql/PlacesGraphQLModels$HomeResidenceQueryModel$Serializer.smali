.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1001462
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;

    new-instance v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1001463
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1001413
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1001415
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1001416
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 1001417
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1001418
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1001419
    if-eqz v2, :cond_0

    .line 1001420
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001421
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1001422
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1001423
    if-eqz v2, :cond_2

    .line 1001424
    const-string v3, "address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001425
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1001426
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1001427
    if-eqz v3, :cond_1

    .line 1001428
    const-string p0, "street"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001429
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1001430
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1001431
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1001432
    if-eqz v2, :cond_3

    .line 1001433
    const-string v3, "city"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001434
    invoke-static {v1, v2, p1, p2}, LX/5mj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1001435
    :cond_3
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1001436
    if-eqz v2, :cond_4

    .line 1001437
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001438
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1001439
    :cond_4
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1001440
    if-eqz v2, :cond_5

    .line 1001441
    const-string v3, "neighborhood_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001442
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1001443
    :cond_5
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1001444
    if-eqz v2, :cond_6

    .line 1001445
    const-string v3, "privacy_option"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001446
    invoke-static {v1, v2, p1, p2}, LX/39L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1001447
    :cond_6
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1001448
    if-eqz v2, :cond_8

    .line 1001449
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001450
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1001451
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1001452
    if-eqz v3, :cond_7

    .line 1001453
    const-string p0, "uri"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001454
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1001455
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1001456
    :cond_8
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1001457
    if-eqz v2, :cond_9

    .line 1001458
    const-string v3, "profile_picture_is_silhouette"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001459
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1001460
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1001461
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1001414
    check-cast p1, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$Serializer;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
