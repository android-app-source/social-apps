.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x311555fc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1001116
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1001117
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1001118
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1001119
    return-void
.end method

.method private j()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001120
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    .line 1001121
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1001122
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1001123
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1001124
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1001125
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1001126
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1001127
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1001128
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1001129
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1001130
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    .line 1001131
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1001132
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;

    .line 1001133
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    .line 1001134
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1001135
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001136
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1001137
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;-><init>()V

    .line 1001138
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1001139
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1001140
    const v0, -0x1471b4fd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1001141
    const v0, -0x3e224e4d

    return v0
.end method
