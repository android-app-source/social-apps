.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1f4f4863
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1001491
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1001490
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1001464
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1001465
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001492
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1001493
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1001494
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCity"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001495
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    .line 1001496
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 1001497
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1001498
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1001499
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->e()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x64f58721

    invoke-static {v2, v1, v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1001500
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1001501
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1001502
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1001503
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->c()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1001504
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->by_()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, 0x209d15b8

    invoke-static {v7, v6, v8}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1001505
    const/16 v7, 0x8

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1001506
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1001507
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1001508
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1001509
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1001510
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1001511
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1001512
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1001513
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1001514
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1001515
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1001516
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1001517
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->e()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1001518
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->e()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x64f58721

    invoke-static {v2, v0, v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1001519
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->e()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1001520
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;

    .line 1001521
    iput v3, v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->f:I

    move-object v1, v0

    .line 1001522
    :cond_0
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1001523
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    .line 1001524
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1001525
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;

    .line 1001526
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    .line 1001527
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->c()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1001528
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->c()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1001529
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->c()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1001530
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;

    .line 1001531
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1001532
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->by_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 1001533
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->by_()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x209d15b8

    invoke-static {v2, v0, v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1001534
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->by_()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1001535
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;

    .line 1001536
    iput v3, v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->k:I

    move-object v1, v0

    .line 1001537
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1001538
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    .line 1001539
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1001540
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_4
    move-object p0, v1

    .line 1001541
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1001542
    new-instance v0, LX/5mK;

    invoke-direct {v0, p1}, LX/5mK;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001543
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->h:Ljava/lang/String;

    .line 1001544
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1001483
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1001484
    const/4 v0, 0x1

    const v1, -0x64f58721

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->f:I

    .line 1001485
    const/4 v0, 0x6

    const v1, 0x209d15b8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->k:I

    .line 1001486
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->l:Z

    .line 1001487
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1001488
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1001489
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1001482
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1001479
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;-><init>()V

    .line 1001480
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1001481
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001477
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->i:Ljava/lang/String;

    .line 1001478
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final by_()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001475
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1001476
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->k:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bz_()Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCity"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001474
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001472
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1001473
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1001470
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1001471
    iget-boolean v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->l:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1001469
    const v0, -0x1f0853b7

    return v0
.end method

.method public final e()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddress"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001467
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1001468
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1001466
    const v0, 0x252222

    return v0
.end method
