.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1001724
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;

    new-instance v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1001725
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1001726
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;LX/0nX;LX/0my;)V
    .locals 12

    .prologue
    .line 1001727
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1001728
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 v5, 0xa

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 1001729
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1001730
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1001731
    if-eqz v2, :cond_0

    .line 1001732
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001733
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1001734
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1001735
    if-eqz v2, :cond_3

    .line 1001736
    const-string v3, "address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001737
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1001738
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1001739
    if-eqz v3, :cond_1

    .line 1001740
    const-string v6, "city"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001741
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1001742
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1001743
    if-eqz v3, :cond_2

    .line 1001744
    const-string v6, "street"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001745
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1001746
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1001747
    :cond_3
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1001748
    if-eqz v2, :cond_7

    .line 1001749
    const-string v3, "all_phones"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001750
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1001751
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v6

    if-ge v3, v6, :cond_6

    .line 1001752
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v6

    .line 1001753
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1001754
    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, LX/15i;->g(II)I

    move-result v7

    .line 1001755
    if-eqz v7, :cond_5

    .line 1001756
    const-string v8, "phone_number"

    invoke-virtual {p1, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001757
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1001758
    const/4 v8, 0x0

    invoke-virtual {v1, v7, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 1001759
    if-eqz v8, :cond_4

    .line 1001760
    const-string v6, "display_number"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001761
    invoke-virtual {p1, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1001762
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1001763
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1001764
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1001765
    :cond_6
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1001766
    :cond_7
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1001767
    if-eqz v2, :cond_8

    .line 1001768
    const-string v2, "category_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001769
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1001770
    :cond_8
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1001771
    if-eqz v2, :cond_9

    .line 1001772
    const-string v3, "friends_who_visited"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001773
    invoke-static {v1, v2, p1, p2}, LX/5mo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1001774
    :cond_9
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1001775
    if-eqz v2, :cond_a

    .line 1001776
    const-string v3, "full_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001777
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1001778
    :cond_a
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1001779
    if-eqz v2, :cond_b

    .line 1001780
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001781
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1001782
    :cond_b
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1001783
    if-eqz v2, :cond_e

    .line 1001784
    const-string v3, "location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001785
    const-wide/16 v10, 0x0

    .line 1001786
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1001787
    const/4 v6, 0x0

    invoke-virtual {v1, v2, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1001788
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_c

    .line 1001789
    const-string v8, "latitude"

    invoke-virtual {p1, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001790
    invoke-virtual {p1, v6, v7}, LX/0nX;->a(D)V

    .line 1001791
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {v1, v2, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1001792
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_d

    .line 1001793
    const-string v8, "longitude"

    invoke-virtual {p1, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001794
    invoke-virtual {p1, v6, v7}, LX/0nX;->a(D)V

    .line 1001795
    :cond_d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1001796
    :cond_e
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1001797
    if-eqz v2, :cond_f

    .line 1001798
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001799
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1001800
    :cond_f
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1001801
    if-eqz v2, :cond_11

    .line 1001802
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001803
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1001804
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1001805
    if-eqz v3, :cond_10

    .line 1001806
    const-string v4, "uri"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001807
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1001808
    :cond_10
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1001809
    :cond_11
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1001810
    if-eqz v2, :cond_12

    .line 1001811
    const-string v2, "websites"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1001812
    invoke-virtual {v1, v0, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1001813
    :cond_12
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1001814
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1001815
    check-cast p1, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$Serializer;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;LX/0nX;LX/0my;)V

    return-void
.end method
