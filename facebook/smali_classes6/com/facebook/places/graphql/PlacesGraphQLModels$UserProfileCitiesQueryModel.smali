.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xebd7908
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1002017
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1002018
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1002024
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1002025
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1002019
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1002020
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1002021
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCurrentCity"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1002022
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    .line 1002023
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    return-object v0
.end method

.method private k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getHometown"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1002005
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    .line 1002006
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1002007
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1002008
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1002009
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1002010
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1002011
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1002012
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1002013
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1002014
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1002015
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1002016
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1001992
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1001993
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1001994
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    .line 1001995
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1001996
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;

    .line 1001997
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    .line 1001998
    :cond_0
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1001999
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    .line 1002000
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1002001
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;

    .line 1002002
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinCityModel;

    .line 1002003
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1002004
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1001991
    new-instance v0, LX/5mN;

    invoke-direct {v0, p1}, LX/5mN;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1001989
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1001990
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1001988
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1001985
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$UserProfileCitiesQueryModel;-><init>()V

    .line 1001986
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1001987
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1001984
    const v0, 0x2b654770

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1001983
    const v0, 0x3c2b9d5

    return v0
.end method
