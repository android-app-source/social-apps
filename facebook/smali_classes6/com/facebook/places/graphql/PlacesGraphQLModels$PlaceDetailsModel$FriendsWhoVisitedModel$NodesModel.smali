.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7423598b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1001647
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1001688
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1001686
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1001687
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001684
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->e:Ljava/lang/String;

    .line 1001685
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001682
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->f:Ljava/lang/String;

    .line 1001683
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001680
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1001681
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1001670
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1001671
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1001672
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1001673
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x5961a818

    invoke-static {v3, v2, v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1001674
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1001675
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1001676
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1001677
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1001678
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1001679
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1001660
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1001661
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1001662
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x5961a818

    invoke-static {v2, v0, v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1001663
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1001664
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;

    .line 1001665
    iput v3, v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->g:I

    .line 1001666
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1001667
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1001668
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1001669
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1001689
    new-instance v0, LX/5mL;

    invoke-direct {v0, p1}, LX/5mL;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001659
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1001656
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1001657
    const/4 v0, 0x2

    const v1, 0x5961a818

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;->g:I

    .line 1001658
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1001654
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1001655
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1001653
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1001650
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel$NodesModel;-><init>()V

    .line 1001651
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1001652
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1001649
    const v0, -0x60512baf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1001648
    const v0, 0x285feb

    return v0
.end method
