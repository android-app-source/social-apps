.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1001363
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;

    new-instance v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1001364
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1001365
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1001366
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1001367
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1001368
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_d

    .line 1001369
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1001370
    :goto_0
    move v1, v2

    .line 1001371
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1001372
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1001373
    new-instance v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;

    invoke-direct {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;-><init>()V

    .line 1001374
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1001375
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1001376
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1001377
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1001378
    :cond_0
    return-object v1

    .line 1001379
    :cond_1
    const-string p0, "profile_picture_is_silhouette"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1001380
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v4, v1

    move v1, v3

    .line 1001381
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_b

    .line 1001382
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1001383
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1001384
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 1001385
    const-string p0, "__type__"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1001386
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v11

    goto :goto_1

    .line 1001387
    :cond_4
    const-string p0, "address"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1001388
    invoke-static {p1, v0}, LX/5mh;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1001389
    :cond_5
    const-string p0, "city"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1001390
    invoke-static {p1, v0}, LX/5mj;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1001391
    :cond_6
    const-string p0, "name"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1001392
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1001393
    :cond_7
    const-string p0, "neighborhood_name"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1001394
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1001395
    :cond_8
    const-string p0, "privacy_option"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1001396
    invoke-static {p1, v0}, LX/39L;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1001397
    :cond_9
    const-string p0, "profile_picture"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1001398
    invoke-static {p1, v0}, LX/5mk;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1001399
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1001400
    :cond_b
    const/16 v12, 0x8

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1001401
    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1001402
    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1001403
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1001404
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1001405
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1001406
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1001407
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1001408
    if-eqz v1, :cond_c

    .line 1001409
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 1001410
    :cond_c
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_d
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    goto/16 :goto_1
.end method
