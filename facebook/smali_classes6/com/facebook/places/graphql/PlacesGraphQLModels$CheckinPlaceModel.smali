.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x76c9723b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1000528
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1000529
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1000530
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1000531
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1000532
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1000533
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1000534
    return-void
.end method

.method public static a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .locals 2

    .prologue
    .line 1000508
    if-nez p0, :cond_0

    .line 1000509
    const/4 p0, 0x0

    .line 1000510
    :goto_0
    return-object p0

    .line 1000511
    :cond_0
    instance-of v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-eqz v0, :cond_1

    .line 1000512
    check-cast p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    goto :goto_0

    .line 1000513
    :cond_1
    new-instance v0, LX/5m9;

    invoke-direct {v0}, LX/5m9;-><init>()V

    .line 1000514
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/5m9;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1000515
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v1

    iput-object v1, v0, LX/5m9;->b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    .line 1000516
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->d()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v1

    iput-object v1, v0, LX/5m9;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    .line 1000517
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5m9;->d:Ljava/lang/String;

    .line 1000518
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bw_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v1

    iput-object v1, v0, LX/5m9;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    .line 1000519
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5m9;->f:Ljava/lang/String;

    .line 1000520
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v1

    iput-object v1, v0, LX/5m9;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    .line 1000521
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5m9;->h:Ljava/lang/String;

    .line 1000522
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->l()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object v1

    iput-object v1, v0, LX/5m9;->i:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    .line 1000523
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5m9;->j:Ljava/lang/String;

    .line 1000524
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->n()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v1

    iput-object v1, v0, LX/5m9;->k:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 1000525
    invoke-virtual {v0}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object p0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1000535
    iput-object p1, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->l:Ljava/lang/String;

    .line 1000536
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1000537
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1000538
    if-eqz v0, :cond_0

    .line 1000539
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1000540
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 1000541
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1000542
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1000543
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->o()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1000544
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->p()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1000545
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1000546
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->q()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1000547
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1000548
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->r()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1000549
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1000550
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->s()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1000551
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->m()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1000552
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->n()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 1000553
    const/16 v11, 0xb

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1000554
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 1000555
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1000556
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1000557
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1000558
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1000559
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1000560
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1000561
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1000562
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1000563
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1000564
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1000565
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1000566
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1000567
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1000568
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->o()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1000569
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->o()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    .line 1000570
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->o()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1000571
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1000572
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    .line 1000573
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->p()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1000574
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->p()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    .line 1000575
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->p()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1000576
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1000577
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    .line 1000578
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->q()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1000579
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->q()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    .line 1000580
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->q()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1000581
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1000582
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->i:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    .line 1000583
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->r()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1000584
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->r()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    .line 1000585
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->r()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1000586
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1000587
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    .line 1000588
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->s()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1000589
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->s()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    .line 1000590
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->s()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1000591
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1000592
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->m:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    .line 1000593
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1000594
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1000595
    new-instance v0, LX/5mF;

    invoke-direct {v0, p1}, LX/5mF;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000596
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1000597
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1000598
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1000599
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1000600
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    .line 1000601
    :goto_0
    return-void

    .line 1000602
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1000603
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1000604
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Ljava/lang/String;)V

    .line 1000605
    :cond_0
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000606
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1000607
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1000608
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1000609
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;-><init>()V

    .line 1000610
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1000611
    return-object v0
.end method

.method public final bv_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000526
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j:Ljava/lang/String;

    .line 1000527
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic bw_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000612
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->q()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000484
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->o()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000485
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->p()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1000486
    const v0, -0x513fac16

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000487
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->h:Ljava/lang/String;

    .line 1000488
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1000489
    const v0, 0x499e8e7

    return v0
.end method

.method public final synthetic j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000490
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->r()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000491
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->l:Ljava/lang/String;

    .line 1000492
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic l()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000493
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->s()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000494
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->n:Ljava/lang/String;

    .line 1000495
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000496
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->o:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->o:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 1000497
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->o:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-object v0
.end method

.method public final o()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000498
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    .line 1000499
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    return-object v0
.end method

.method public final p()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000500
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    .line 1000501
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    return-object v0
.end method

.method public final q()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000502
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->i:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->i:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    .line 1000503
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->i:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    return-object v0
.end method

.method public final r()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000504
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    .line 1000505
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    return-object v0
.end method

.method public final s()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000506
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->m:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->m:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    .line 1000507
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->m:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    return-object v0
.end method
