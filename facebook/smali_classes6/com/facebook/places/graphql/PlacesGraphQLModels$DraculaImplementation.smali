.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1000927
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1000928
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1001005
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1001006
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x2

    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1000945
    if-nez p1, :cond_0

    .line 1000946
    :goto_0
    return v1

    .line 1000947
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1000948
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1000949
    :sswitch_0
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1000950
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1000951
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 1000952
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 1000953
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1000954
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1000955
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1000956
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1000957
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1000958
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1000959
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1000960
    :sswitch_2
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1000961
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1000962
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 1000963
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 1000964
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1000965
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1000966
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1000967
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1000968
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1000969
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1000970
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1000971
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1000972
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1000973
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1000974
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1000975
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 1000976
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1000977
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 1000978
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 1000979
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1000980
    const v2, 0x5b1c7d1c

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1000981
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1000982
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1000983
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 1000984
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1000985
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1000986
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1000987
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1000988
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 1000989
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1000990
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1000991
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1000992
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1000993
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 1000994
    :sswitch_8
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1000995
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1000996
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 1000997
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 1000998
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1000999
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 1001000
    :sswitch_9
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1001001
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1001002
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1001003
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1001004
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x64f58721 -> :sswitch_1
        -0x2d8422a3 -> :sswitch_8
        0xd8230ba -> :sswitch_5
        0x209d15b8 -> :sswitch_3
        0x21ee05bc -> :sswitch_9
        0x328c2607 -> :sswitch_0
        0x5961a818 -> :sswitch_7
        0x5b1c7d1c -> :sswitch_6
        0x5cfc4016 -> :sswitch_2
        0x5e71ae1f -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1000936
    if-nez p0, :cond_0

    move v0, v1

    .line 1000937
    :goto_0
    return v0

    .line 1000938
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1000939
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1000940
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1000941
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1000942
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1000943
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1000944
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1000929
    const/4 v7, 0x0

    .line 1000930
    const/4 v1, 0x0

    .line 1000931
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1000932
    invoke-static {v2, v3, v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1000933
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1000934
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1000935
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1000914
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1000922
    sparse-switch p2, :sswitch_data_0

    .line 1000923
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1000924
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1000925
    const v1, 0x5b1c7d1c

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1000926
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x64f58721 -> :sswitch_1
        -0x2d8422a3 -> :sswitch_1
        0xd8230ba -> :sswitch_0
        0x209d15b8 -> :sswitch_1
        0x21ee05bc -> :sswitch_1
        0x328c2607 -> :sswitch_1
        0x5961a818 -> :sswitch_1
        0x5b1c7d1c -> :sswitch_1
        0x5cfc4016 -> :sswitch_1
        0x5e71ae1f -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1000916
    if-eqz p1, :cond_0

    .line 1000917
    invoke-static {p0, p1, p2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1000918
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    .line 1000919
    if-eq v0, v1, :cond_0

    .line 1000920
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1000921
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1000915
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1001007
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1001008
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1000886
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1000887
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1000888
    :cond_0
    iput-object p1, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1000889
    iput p2, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->b:I

    .line 1000890
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1000891
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1000892
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1000893
    iget v0, p0, LX/1vt;->c:I

    .line 1000894
    move v0, v0

    .line 1000895
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1000896
    iget v0, p0, LX/1vt;->c:I

    .line 1000897
    move v0, v0

    .line 1000898
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1000899
    iget v0, p0, LX/1vt;->b:I

    .line 1000900
    move v0, v0

    .line 1000901
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000902
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1000903
    move-object v0, v0

    .line 1000904
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1000905
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1000906
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1000907
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1000908
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1000909
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1000910
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1000911
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1000912
    invoke-static {v3, v9, v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1000913
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1000883
    iget v0, p0, LX/1vt;->c:I

    .line 1000884
    move v0, v0

    .line 1000885
    return v0
.end method
