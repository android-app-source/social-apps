.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1000476
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1000473
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1000471
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1000472
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1000468
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1000469
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1000470
    return-void
.end method

.method public static a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;
    .locals 2

    .prologue
    .line 1000460
    if-nez p0, :cond_0

    .line 1000461
    const/4 p0, 0x0

    .line 1000462
    :goto_0
    return-object p0

    .line 1000463
    :cond_0
    instance-of v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    if-eqz v0, :cond_1

    .line 1000464
    check-cast p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    goto :goto_0

    .line 1000465
    :cond_1
    new-instance v0, LX/5mG;

    invoke-direct {v0}, LX/5mG;-><init>()V

    .line 1000466
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;->a()I

    move-result v1

    iput v1, v0, LX/5mG;->a:I

    .line 1000467
    invoke-virtual {v0}, LX/5mG;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1000474
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1000475
    iget v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1000455
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1000456
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1000457
    iget v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1000458
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1000459
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1000452
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1000453
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1000454
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1000449
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1000450
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;->e:I

    .line 1000451
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1000446
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;-><init>()V

    .line 1000447
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1000448
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1000445
    const v0, 0x49007cfd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1000444
    const v0, -0x31f775ab

    return v0
.end method
