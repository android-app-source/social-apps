.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x14987218
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1000265
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1000262
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1000266
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1000267
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1000268
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1000269
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1000270
    return-void
.end method

.method public static a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;
    .locals 14

    .prologue
    .line 1000271
    if-nez p0, :cond_0

    .line 1000272
    const/4 p0, 0x0

    .line 1000273
    :goto_0
    return-object p0

    .line 1000274
    :cond_0
    instance-of v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    if-eqz v0, :cond_1

    .line 1000275
    check-cast p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    goto :goto_0

    .line 1000276
    :cond_1
    new-instance v1, LX/5mB;

    invoke-direct {v1}, LX/5mB;-><init>()V

    .line 1000277
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/5mB;->a:Ljava/lang/String;

    .line 1000278
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1000279
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 1000280
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1000281
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1000282
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/5mB;->b:LX/0Px;

    .line 1000283
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    move-result-object v0

    iput-object v0, v1, LX/5mB;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    .line 1000284
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->d()Z

    move-result v0

    iput-boolean v0, v1, LX/5mB;->d:Z

    .line 1000285
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/5mB;->e:Ljava/lang/String;

    .line 1000286
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v0

    iput-object v0, v1, LX/5mB;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    .line 1000287
    const/4 v8, 0x1

    const/4 v13, 0x0

    const/4 v6, 0x0

    .line 1000288
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1000289
    iget-object v5, v1, LX/5mB;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1000290
    iget-object v7, v1, LX/5mB;->b:LX/0Px;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/util/List;)I

    move-result v7

    .line 1000291
    iget-object v9, v1, LX/5mB;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    invoke-static {v4, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1000292
    iget-object v10, v1, LX/5mB;->e:Ljava/lang/String;

    invoke-virtual {v4, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1000293
    iget-object v11, v1, LX/5mB;->f:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    invoke-static {v4, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1000294
    const/4 v12, 0x6

    invoke-virtual {v4, v12}, LX/186;->c(I)V

    .line 1000295
    invoke-virtual {v4, v13, v5}, LX/186;->b(II)V

    .line 1000296
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 1000297
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 1000298
    const/4 v5, 0x3

    iget-boolean v7, v1, LX/5mB;->d:Z

    invoke-virtual {v4, v5, v7}, LX/186;->a(IZ)V

    .line 1000299
    const/4 v5, 0x4

    invoke-virtual {v4, v5, v10}, LX/186;->b(II)V

    .line 1000300
    const/4 v5, 0x5

    invoke-virtual {v4, v5, v11}, LX/186;->b(II)V

    .line 1000301
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1000302
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1000303
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1000304
    invoke-virtual {v5, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1000305
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1000306
    new-instance v5, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    invoke-direct {v5, v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;-><init>(LX/15i;)V

    .line 1000307
    move-object p0, v5

    .line 1000308
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000309
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    .line 1000310
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    return-object v0
.end method

.method private k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000311
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->j:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->j:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    .line 1000312
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->j:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1000313
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1000314
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1000315
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1000316
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1000317
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1000318
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1000319
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1000320
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1000321
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1000322
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1000323
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1000324
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1000325
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1000326
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1000327
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1000328
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1000329
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1000330
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    .line 1000331
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1000332
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    .line 1000333
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    .line 1000334
    :cond_0
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1000335
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    .line 1000336
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1000337
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    .line 1000338
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->j:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    .line 1000339
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1000340
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000263
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->e:Ljava/lang/String;

    .line 1000264
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1000259
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1000260
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->h:Z

    .line 1000261
    return-void
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1000257
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->f:Ljava/util/List;

    .line 1000258
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1000254
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;-><init>()V

    .line 1000255
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1000256
    return-object v0
.end method

.method public final synthetic bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000253
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->k()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000252
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1000250
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1000251
    iget-boolean v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->h:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1000249
    const v0, -0x51e06bb9

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000247
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->i:Ljava/lang/String;

    .line 1000248
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1000246
    const v0, -0x5b82dd22

    return v0
.end method
