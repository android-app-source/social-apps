.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x432b477f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1001903
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1001925
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1001923
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1001924
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1001917
    iput-object p1, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->m:Ljava/lang/String;

    .line 1001918
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1001919
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1001920
    if-eqz v0, :cond_0

    .line 1001921
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1001922
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001914
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1001915
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1001916
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddress"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001912
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1001913
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private l()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllPhones"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1001910
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->g:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x2

    const v4, 0xd8230ba

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->g:LX/3Sb;

    .line 1001911
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->g:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private m()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1001908
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->h:Ljava/util/List;

    .line 1001909
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private n()Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFriendsWhoVisited"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001906
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->i:Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->i:Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel;

    .line 1001907
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->i:Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001904
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->j:Ljava/lang/String;

    .line 1001905
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001901
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->k:Ljava/lang/String;

    .line 1001902
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method private q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001899
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1001900
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->l:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001926
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->m:Ljava/lang/String;

    .line 1001927
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method private s()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1001816
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1001817
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->n:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private t()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1001844
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->o:Ljava/util/List;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->o:Ljava/util/List;

    .line 1001845
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->o:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 1001818
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1001819
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1001820
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x5e71ae1f

    invoke-static {v2, v1, v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1001821
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->l()LX/2uF;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v2

    .line 1001822
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->m()LX/0Px;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v3

    .line 1001823
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->n()Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1001824
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1001825
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1001826
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->q()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, -0x2d8422a3

    invoke-static {v8, v7, v9}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1001827
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->r()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1001828
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->s()LX/1vs;

    move-result-object v9

    iget-object v10, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    const v11, 0x21ee05bc

    invoke-static {v10, v9, v11}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1001829
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->t()LX/0Px;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/util/List;)I

    move-result v10

    .line 1001830
    const/16 v11, 0xb

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1001831
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 1001832
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1001833
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1001834
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1001835
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1001836
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1001837
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1001838
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1001839
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1001840
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1001841
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1001842
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1001843
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1001846
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1001847
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 1001848
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x5e71ae1f

    invoke-static {v2, v0, v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1001849
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1001850
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;

    .line 1001851
    iput v3, v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->f:I

    .line 1001852
    :goto_0
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->l()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1001853
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->l()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1001854
    if-eqz v1, :cond_0

    .line 1001855
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;

    .line 1001856
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->g:LX/3Sb;

    :cond_0
    move-object v1, v0

    .line 1001857
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->n()Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1001858
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->n()Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel;

    .line 1001859
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->n()Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1001860
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;

    .line 1001861
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->i:Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel$FriendsWhoVisitedModel;

    .line 1001862
    :cond_1
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1001863
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2d8422a3

    invoke-static {v2, v0, v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1001864
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1001865
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;

    .line 1001866
    iput v3, v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->l:I

    move-object v1, v0

    .line 1001867
    :cond_2
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->s()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 1001868
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->s()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x21ee05bc

    invoke-static {v2, v0, v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/graphql/PlacesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1001869
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->s()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1001870
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;

    .line 1001871
    iput v3, v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->n:I

    move-object v1, v0

    .line 1001872
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1001873
    if-nez v1, :cond_4

    :goto_1
    return-object p0

    .line 1001874
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1001875
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 1001876
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_4
    move-object p0, v1

    .line 1001877
    goto :goto_1

    :cond_5
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1001878
    new-instance v0, LX/5mM;

    invoke-direct {v0, p1}, LX/5mM;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001879
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1001880
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1001881
    const/4 v0, 0x1

    const v1, 0x5e71ae1f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->f:I

    .line 1001882
    const/4 v0, 0x7

    const v1, -0x2d8422a3

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->l:I

    .line 1001883
    const/16 v0, 0x9

    const v1, 0x21ee05bc

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->n:I

    .line 1001884
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1001885
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1001886
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1001887
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1001888
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    .line 1001889
    :goto_0
    return-void

    .line 1001890
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1001891
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1001892
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;->a(Ljava/lang/String;)V

    .line 1001893
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1001894
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$PlaceDetailsModel;-><init>()V

    .line 1001895
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1001896
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1001897
    const v0, -0xe7863de

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1001898
    const v0, 0x499e8e7

    return v0
.end method
