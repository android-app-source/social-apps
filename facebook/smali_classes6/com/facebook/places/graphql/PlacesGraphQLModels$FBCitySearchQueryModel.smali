.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x704f9ba0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1001297
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1001296
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1001294
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1001295
    return-void
.end method

.method private j()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001292
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;

    .line 1001293
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1001286
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1001287
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1001288
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1001289
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1001290
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1001291
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1001278
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1001279
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1001280
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;

    .line 1001281
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1001282
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;

    .line 1001283
    iput-object v0, v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;

    .line 1001284
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1001285
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1001272
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1001275
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;-><init>()V

    .line 1001276
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1001277
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1001274
    const v0, -0x73909194

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1001273
    const v0, -0x3e224e4d

    return v0
.end method
