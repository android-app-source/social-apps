.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4ad7ddcf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1000680
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1000679
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1000677
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1000678
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1000671
    iput-object p1, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->g:Ljava/lang/String;

    .line 1000672
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1000673
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1000674
    if-eqz v0, :cond_0

    .line 1000675
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1000676
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000668
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1000669
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1000670
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1000658
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1000659
    invoke-direct {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1000660
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1000661
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1000662
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1000663
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1000664
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1000665
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1000666
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1000667
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1000655
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1000656
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1000657
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1000654
    new-instance v0, LX/5mH;

    invoke-direct {v0, p1}, LX/5mH;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000635
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1000648
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1000649
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1000650
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1000651
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 1000652
    :goto_0
    return-void

    .line 1000653
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1000645
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1000646
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->a(Ljava/lang/String;)V

    .line 1000647
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1000642
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;-><init>()V

    .line 1000643
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1000644
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000640
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->f:Ljava/lang/String;

    .line 1000641
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000638
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->g:Ljava/lang/String;

    .line 1000639
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1000637
    const v0, 0x7ebb855e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1000636
    const v0, 0x499e8e7

    return v0
.end method
