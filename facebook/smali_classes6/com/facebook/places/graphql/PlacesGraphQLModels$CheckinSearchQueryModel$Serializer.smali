.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1000812
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;

    new-instance v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1000813
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1000814
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1000815
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1000816
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1000817
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1000818
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1000819
    if-eqz v2, :cond_0

    .line 1000820
    const-string p0, "closest_city"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1000821
    invoke-static {v1, v2, p1}, LX/5mb;->a(LX/15i;ILX/0nX;)V

    .line 1000822
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1000823
    if-eqz v2, :cond_1

    .line 1000824
    const-string p0, "place_results"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1000825
    invoke-static {v1, v2, p1, p2}, LX/5md;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1000826
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1000827
    if-eqz v2, :cond_2

    .line 1000828
    const-string p0, "suggest_place_when_people_tagging"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1000829
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1000830
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1000831
    if-eqz v2, :cond_3

    .line 1000832
    const-string p0, "tracking"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1000833
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1000834
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1000835
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1000836
    check-cast p1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$Serializer;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
