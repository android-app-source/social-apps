.class public final Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x792a84d8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1000803
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1000802
    const-class v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1000800
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1000801
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1000788
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1000789
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1000790
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->b()Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1000791
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1000792
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1000793
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1000794
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1000795
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1000798
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->e:Ljava/util/List;

    .line 1000799
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1000804
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1000805
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1000806
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1000807
    if-eqz v1, :cond_0

    .line 1000808
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    .line 1000809
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->e:Ljava/util/List;

    .line 1000810
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1000811
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1000796
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->f:Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;

    iput-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->f:Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;

    .line 1000797
    iget-object v0, p0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;->f:Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1000785
    new-instance v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;

    invoke-direct {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$PlaceResultsModel;-><init>()V

    .line 1000786
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1000787
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1000784
    const v0, 0x5479988f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1000783
    const v0, -0x67b7b45e

    return v0
.end method
