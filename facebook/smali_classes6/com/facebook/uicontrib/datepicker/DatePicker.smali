.class public Lcom/facebook/uicontrib/datepicker/DatePicker;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field private static final a:Lcom/facebook/uicontrib/datepicker/Date;


# instance fields
.field public b:LX/5zz;

.field private c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

.field private d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

.field private e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

.field private f:Lcom/facebook/uicontrib/datepicker/Period;

.field private g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1036113
    new-instance v0, LX/5zw;

    invoke-direct {v0}, LX/5zw;-><init>()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 1036114
    iput v1, v0, LX/5zw;->a:I

    .line 1036115
    move-object v0, v0

    .line 1036116
    iput-object v3, v0, LX/5zw;->b:Ljava/lang/Integer;

    .line 1036117
    move-object v0, v0

    .line 1036118
    iput-object v3, v0, LX/5zw;->c:Ljava/lang/Integer;

    .line 1036119
    move-object v0, v0

    .line 1036120
    invoke-virtual {v0}, LX/5zw;->a()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v0

    sput-object v0, Lcom/facebook/uicontrib/datepicker/DatePicker;->a:Lcom/facebook/uicontrib/datepicker/Date;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1036121
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1036122
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePicker;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1036123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1036124
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1036125
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1036126
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1036099
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->getSpinnerAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, LX/606;

    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->f:Lcom/facebook/uicontrib/datepicker/Period;

    .line 1036100
    iput-object v1, v0, LX/606;->b:Lcom/facebook/uicontrib/datepicker/Period;

    .line 1036101
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->getSpinnerAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, LX/604;

    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->f:Lcom/facebook/uicontrib/datepicker/Period;

    .line 1036102
    iput-object v1, v0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    .line 1036103
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->getSpinnerAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, LX/603;

    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->f:Lcom/facebook/uicontrib/datepicker/Period;

    .line 1036104
    iput-object v1, v0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    .line 1036105
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->b()V

    .line 1036106
    return-void
.end method

.method private a(LX/5zx;IZ)V
    .locals 4

    .prologue
    .line 1036127
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081873

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setLabelText(Ljava/lang/String;)V

    .line 1036128
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081876

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setClearButtonAccessibilityText(Ljava/lang/String;)V

    .line 1036129
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081870

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setAddViewText(Ljava/lang/String;)V

    .line 1036130
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0, p3}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setIsOptional(Z)V

    .line 1036131
    new-instance v1, LX/606;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0303d2

    iget-object v3, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->f:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-direct {v1, v0, v2, v3}, LX/606;-><init>(Landroid/content/Context;ILcom/facebook/uicontrib/datepicker/Period;)V

    .line 1036132
    const v0, 0x1090009

    invoke-virtual {v1, v0}, LX/606;->setDropDownViewResource(I)V

    .line 1036133
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setSpinnerAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1036134
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036135
    iput-object p1, v0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->e:LX/5zx;

    .line 1036136
    iget-object v2, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setActive(Z)V

    .line 1036137
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036138
    iget-boolean v2, v0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v0, v2

    .line 1036139
    if-eqz v0, :cond_0

    .line 1036140
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v1, p2}, LX/606;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setSpinnerSelection(I)V

    .line 1036141
    :cond_0
    return-void

    .line 1036142
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/5zx;Ljava/lang/Integer;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1036143
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081874

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setLabelText(Ljava/lang/String;)V

    .line 1036144
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081877

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setClearButtonAccessibilityText(Ljava/lang/String;)V

    .line 1036145
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081871

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setAddViewText(Ljava/lang/String;)V

    .line 1036146
    new-instance v2, LX/604;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f0303d2

    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedYear()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->f:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-direct {v2, v0, v3, v4, v5}, LX/604;-><init>(Landroid/content/Context;IILcom/facebook/uicontrib/datepicker/Period;)V

    .line 1036147
    const v0, 0x1090009

    invoke-virtual {v2, v0}, LX/604;->setDropDownViewResource(I)V

    .line 1036148
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setSpinnerAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1036149
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036150
    iput-object p1, v0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->e:LX/5zx;

    .line 1036151
    iget-object v3, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036152
    iget-boolean v4, v0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v0, v4

    .line 1036153
    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setActive(Z)V

    .line 1036154
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036155
    iget-boolean v3, v0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v0, v3

    .line 1036156
    if-eqz v0, :cond_0

    .line 1036157
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v2, p2}, LX/604;->a(Ljava/lang/Integer;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setSpinnerSelection(I)V

    .line 1036158
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    iget-object v2, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036159
    iget-boolean v3, v2, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v2, v3

    .line 1036160
    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setVisibility(I)V

    .line 1036161
    return-void

    :cond_1
    move v0, v1

    .line 1036162
    goto :goto_0

    .line 1036163
    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1036164
    sget-object v0, LX/03r;->DatePicker:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1036165
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1036166
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->g:Z

    .line 1036167
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1036168
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setOrientation(I)V

    .line 1036169
    const v0, 0x7f0303d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1036170
    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f72

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0f72

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v3, v1, v3}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setPadding(IIII)V

    .line 1036171
    new-instance v0, Lcom/facebook/uicontrib/datepicker/Period;

    invoke-direct {v0}, Lcom/facebook/uicontrib/datepicker/Period;-><init>()V

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->f:Lcom/facebook/uicontrib/datepicker/Period;

    .line 1036172
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->f:Lcom/facebook/uicontrib/datepicker/Period;

    sget-object v1, Lcom/facebook/uicontrib/datepicker/Date;->a:Lcom/facebook/uicontrib/datepicker/Date;

    .line 1036173
    iput-object v1, v0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    .line 1036174
    iget-boolean v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->g:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/uicontrib/datepicker/Date;->b:Lcom/facebook/uicontrib/datepicker/Date;

    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setupSpinners(Lcom/facebook/uicontrib/datepicker/Date;)V

    .line 1036175
    return-void

    .line 1036176
    :cond_1
    sget-object v0, Lcom/facebook/uicontrib/datepicker/DatePicker;->a:Lcom/facebook/uicontrib/datepicker/Date;

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 1036177
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedYear()I

    move-result v2

    .line 1036178
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->getSpinnerAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, LX/606;

    .line 1036179
    invoke-virtual {v0}, LX/606;->clear()V

    .line 1036180
    invoke-static {v0}, LX/606;->b(LX/606;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, LX/606;->a(LX/606;Ljava/util/List;)V

    .line 1036181
    const v1, -0x102d8711

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1036182
    iget-object v3, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    if-eqz v2, :cond_1

    .line 1036183
    iget-object v1, v0, LX/606;->b:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/datepicker/Period;->a()I

    move-result v1

    if-gt v1, v2, :cond_2

    iget-object v1, v0, LX/606;->b:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/datepicker/Period;->b()I

    move-result v1

    if-gt v2, v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1036184
    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v3, v1}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setActive(Z)V

    .line 1036185
    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036186
    iget-boolean v3, v1, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v1, v3

    .line 1036187
    if-eqz v1, :cond_0

    .line 1036188
    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0, v2}, LX/606;->a(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setSpinnerSelection(I)V

    .line 1036189
    :cond_0
    invoke-static {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->c(Lcom/facebook/uicontrib/datepicker/DatePicker;)V

    .line 1036190
    return-void

    .line 1036191
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private b(LX/5zx;Ljava/lang/Integer;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1036192
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081875

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setLabelText(Ljava/lang/String;)V

    .line 1036193
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081878

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setClearButtonAccessibilityText(Ljava/lang/String;)V

    .line 1036194
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081872

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setAddViewText(Ljava/lang/String;)V

    .line 1036195
    new-instance v0, LX/603;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0303d2

    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedYear()I

    move-result v3

    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedMonth()Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->f:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-direct/range {v0 .. v5}, LX/603;-><init>(Landroid/content/Context;IILjava/lang/Integer;Lcom/facebook/uicontrib/datepicker/Period;)V

    .line 1036196
    const v1, 0x1090009

    invoke-virtual {v0, v1}, LX/603;->setDropDownViewResource(I)V

    .line 1036197
    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v1, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setSpinnerAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1036198
    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036199
    iput-object p1, v1, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->e:LX/5zx;

    .line 1036200
    iget-object v2, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036201
    iget-boolean v3, v1, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v1, v3

    .line 1036202
    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setActive(Z)V

    .line 1036203
    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036204
    iget-boolean v2, v1, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v1, v2

    .line 1036205
    if-eqz v1, :cond_0

    .line 1036206
    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0, p2}, LX/603;->a(Ljava/lang/Integer;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setSpinnerSelection(I)V

    .line 1036207
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036208
    iget-boolean v2, v1, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v1, v2

    .line 1036209
    if-eqz v1, :cond_2

    :goto_1
    invoke-virtual {v0, v6}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setVisibility(I)V

    .line 1036210
    return-void

    :cond_1
    move v1, v6

    .line 1036211
    goto :goto_0

    .line 1036212
    :cond_2
    const/16 v6, 0x8

    goto :goto_1
.end method

.method public static c(Lcom/facebook/uicontrib/datepicker/DatePicker;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1036213
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedMonth()Ljava/lang/Integer;

    move-result-object v3

    .line 1036214
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedYear()I

    move-result v1

    .line 1036215
    if-nez v1, :cond_1

    .line 1036216
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setActive(Z)V

    .line 1036217
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036218
    iget-boolean v3, v1, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v1, v3

    .line 1036219
    if-eqz v1, :cond_4

    :goto_1
    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setVisibility(I)V

    .line 1036220
    invoke-static {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->d(Lcom/facebook/uicontrib/datepicker/DatePicker;)V

    .line 1036221
    return-void

    .line 1036222
    :cond_1
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->getSpinnerAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, LX/604;

    .line 1036223
    iput v1, v0, LX/604;->b:I

    .line 1036224
    invoke-virtual {v0}, LX/604;->clear()V

    .line 1036225
    invoke-static {v0}, LX/604;->b(LX/604;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, LX/604;->a(LX/604;Ljava/util/List;)V

    .line 1036226
    const v1, -0x5b7c6b54

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1036227
    iget-object v4, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    if-eqz v3, :cond_3

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 1036228
    if-nez v3, :cond_5

    .line 1036229
    :cond_2
    :goto_2
    move v1, v1

    .line 1036230
    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_3
    invoke-virtual {v4, v1}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setActive(Z)V

    .line 1036231
    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036232
    iget-boolean v4, v1, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v1, v4

    .line 1036233
    if-eqz v1, :cond_0

    .line 1036234
    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0, v3}, LX/604;->a(Ljava/lang/Integer;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setSpinnerSelection(I)V

    goto :goto_0

    :cond_3
    move v1, v2

    .line 1036235
    goto :goto_3

    .line 1036236
    :cond_4
    const/16 v2, 0x8

    goto :goto_1

    .line 1036237
    :cond_5
    iget v6, v0, LX/604;->b:I

    if-nez v6, :cond_6

    move v1, v5

    .line 1036238
    goto :goto_2

    .line 1036239
    :cond_6
    iget v6, v0, LX/604;->b:I

    iget-object v7, v0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v7}, Lcom/facebook/uicontrib/datepicker/Period;->a()I

    move-result v7

    if-lt v6, v7, :cond_7

    iget v6, v0, LX/604;->b:I

    iget-object v7, v0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v7}, Lcom/facebook/uicontrib/datepicker/Period;->b()I

    move-result v7

    if-le v6, v7, :cond_8

    :cond_7
    move v1, v5

    .line 1036240
    goto :goto_2

    .line 1036241
    :cond_8
    iget v6, v0, LX/604;->b:I

    iget-object v7, v0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v7}, Lcom/facebook/uicontrib/datepicker/Period;->b()I

    move-result v7

    if-ne v6, v7, :cond_9

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, v0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v7}, Lcom/facebook/uicontrib/datepicker/Period;->d()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-le v6, v7, :cond_9

    move v1, v5

    .line 1036242
    goto :goto_2

    .line 1036243
    :cond_9
    iget v6, v0, LX/604;->b:I

    iget-object v7, v0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v7}, Lcom/facebook/uicontrib/datepicker/Period;->a()I

    move-result v7

    if-ne v6, v7, :cond_2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, v0, LX/604;->c:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v7}, Lcom/facebook/uicontrib/datepicker/Period;->c()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ge v6, v7, :cond_2

    move v1, v5

    .line 1036244
    goto :goto_2
.end method

.method public static d(Lcom/facebook/uicontrib/datepicker/DatePicker;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1036245
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedDay()Ljava/lang/Integer;

    move-result-object v3

    .line 1036246
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedMonth()Ljava/lang/Integer;

    move-result-object v1

    .line 1036247
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedYear()I

    move-result v4

    .line 1036248
    if-nez v1, :cond_1

    .line 1036249
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setActive(Z)V

    .line 1036250
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036251
    iget-boolean v3, v1, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v1, v3

    .line 1036252
    if-eqz v1, :cond_4

    :goto_1
    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setVisibility(I)V

    .line 1036253
    return-void

    .line 1036254
    :cond_1
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->getSpinnerAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, LX/603;

    .line 1036255
    iput v4, v0, LX/603;->b:I

    .line 1036256
    iput-object v1, v0, LX/603;->c:Ljava/lang/Integer;

    .line 1036257
    invoke-virtual {v0}, LX/603;->clear()V

    .line 1036258
    invoke-static {v0}, LX/603;->b(LX/603;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, LX/603;->a(LX/603;Ljava/util/List;)V

    .line 1036259
    const v1, -0x36add7b4    # -860804.75f

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1036260
    iget-object v4, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    if-eqz v3, :cond_3

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 1036261
    if-nez v3, :cond_5

    .line 1036262
    :cond_2
    :goto_2
    move v1, v1

    .line 1036263
    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_3
    invoke-virtual {v4, v1}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setActive(Z)V

    .line 1036264
    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036265
    iget-boolean v4, v1, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v1, v4

    .line 1036266
    if-eqz v1, :cond_0

    .line 1036267
    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0, v3}, LX/603;->a(Ljava/lang/Integer;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setSpinnerSelection(I)V

    goto :goto_0

    :cond_3
    move v1, v2

    .line 1036268
    goto :goto_3

    .line 1036269
    :cond_4
    const/16 v2, 0x8

    goto :goto_1

    .line 1036270
    :cond_5
    iget v6, v0, LX/603;->b:I

    if-eqz v6, :cond_6

    iget-object v6, v0, LX/603;->c:Ljava/lang/Integer;

    if-nez v6, :cond_7

    :cond_6
    move v1, v5

    .line 1036271
    goto :goto_2

    .line 1036272
    :cond_7
    invoke-static {v0}, LX/603;->c(LX/603;)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-static {v0}, LX/603;->d(LX/603;)Z

    move-result v6

    if-eqz v6, :cond_9

    :cond_8
    move v1, v5

    .line 1036273
    goto :goto_2

    .line 1036274
    :cond_9
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget v7, v0, LX/603;->b:I

    iget-object v8, v0, LX/603;->c:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v7, v8}, LX/603;->a(II)I

    move-result v7

    if-le v6, v7, :cond_a

    move v1, v5

    .line 1036275
    goto :goto_2

    .line 1036276
    :cond_a
    invoke-static {v0}, LX/603;->e(LX/603;)Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, v0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v7}, Lcom/facebook/uicontrib/datepicker/Period;->e()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ge v6, v7, :cond_b

    move v1, v5

    .line 1036277
    goto :goto_2

    .line 1036278
    :cond_b
    invoke-static {v0}, LX/603;->f(LX/603;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, v0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v7}, Lcom/facebook/uicontrib/datepicker/Period;->f()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-le v6, v7, :cond_2

    move v1, v5

    .line 1036279
    goto :goto_2
.end method

.method private getOnSelectionChangedListener()LX/5zx;
    .locals 1

    .prologue
    .line 1036280
    new-instance v0, LX/5zy;

    invoke-direct {v0, p0}, LX/5zy;-><init>(Lcom/facebook/uicontrib/datepicker/DatePicker;)V

    return-object v0
.end method

.method private getSelectedDay()Ljava/lang/Integer;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1036107
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036108
    iget-boolean v1, v0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v0, v1

    .line 1036109
    if-eqz v0, :cond_0

    .line 1036110
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->getSpinnerSelection()Ljava/lang/String;

    move-result-object v0

    .line 1036111
    sget-object v1, LX/603;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v0, v1

    .line 1036112
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getSelectedMonth()Ljava/lang/Integer;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1036093
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036094
    iget-boolean v1, v0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v0, v1

    .line 1036095
    if-eqz v0, :cond_0

    .line 1036096
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->getSpinnerSelection()Ljava/lang/String;

    move-result-object v0

    .line 1036097
    sget-object v1, LX/604;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v0, v1

    .line 1036098
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getSelectedYear()I
    .locals 2

    .prologue
    .line 1036086
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036087
    iget-boolean v1, v0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    move v0, v1

    .line 1036088
    if-eqz v0, :cond_0

    .line 1036089
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->getSpinnerSelection()Ljava/lang/String;

    move-result-object v0

    .line 1036090
    :try_start_0
    sget-object v1, LX/606;->a:Ljava/text/NumberFormat;

    invoke-virtual {v1, v0}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1036091
    :goto_0
    move v0, v1

    .line 1036092
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setupSpinners(Lcom/facebook/uicontrib/datepicker/Date;)V
    .locals 3

    .prologue
    .line 1036078
    const v0, 0x7f0d0be8

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->c:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036079
    const v0, 0x7f0d0be9

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->d:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036080
    const v0, 0x7f0d0bea

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->e:Lcom/facebook/uicontrib/datepicker/DatePickerRow;

    .line 1036081
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getOnSelectionChangedListener()LX/5zx;

    move-result-object v0

    .line 1036082
    invoke-virtual {p1}, Lcom/facebook/uicontrib/datepicker/Date;->a()I

    move-result v1

    iget-boolean v2, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->g:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/uicontrib/datepicker/DatePicker;->a(LX/5zx;IZ)V

    .line 1036083
    invoke-virtual {p1}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePicker;->a(LX/5zx;Ljava/lang/Integer;)V

    .line 1036084
    invoke-virtual {p1}, Lcom/facebook/uicontrib/datepicker/Date;->c()Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePicker;->b(LX/5zx;Ljava/lang/Integer;)V

    .line 1036085
    return-void
.end method


# virtual methods
.method public final dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1036076
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    .line 1036077
    return-void
.end method

.method public final dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1036074
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    .line 1036075
    return-void
.end method

.method public getSelectedDate()Lcom/facebook/uicontrib/datepicker/Date;
    .locals 2

    .prologue
    .line 1036062
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedYear()I

    move-result v0

    if-nez v0, :cond_0

    .line 1036063
    const/4 v0, 0x0

    .line 1036064
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/5zw;

    invoke-direct {v0}, LX/5zw;-><init>()V

    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedYear()I

    move-result v1

    .line 1036065
    iput v1, v0, LX/5zw;->a:I

    .line 1036066
    move-object v0, v0

    .line 1036067
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedMonth()Ljava/lang/Integer;

    move-result-object v1

    .line 1036068
    iput-object v1, v0, LX/5zw;->b:Ljava/lang/Integer;

    .line 1036069
    move-object v0, v0

    .line 1036070
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedDay()Ljava/lang/Integer;

    move-result-object v1

    .line 1036071
    iput-object v1, v0, LX/5zw;->c:Ljava/lang/Integer;

    .line 1036072
    move-object v0, v0

    .line 1036073
    invoke-virtual {v0}, LX/5zw;->a()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v0

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1036056
    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 1036057
    check-cast p1, Landroid/os/Bundle;

    .line 1036058
    const-string v0, "superSavedState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1036059
    const-string v0, "period"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/Period;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->f:Lcom/facebook/uicontrib/datepicker/Period;

    .line 1036060
    const-string v0, "selectedDate"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/Date;

    invoke-direct {p0, v0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setupSpinners(Lcom/facebook/uicontrib/datepicker/Date;)V

    .line 1036061
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 1036050
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1036051
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1036052
    const-string v2, "superSavedState"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1036053
    const-string v0, "selectedDate"

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedDate()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1036054
    const-string v0, "period"

    iget-object v2, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->f:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1036055
    return-object v1
.end method

.method public setMaximumDate(Lcom/facebook/uicontrib/datepicker/Date;)V
    .locals 1

    .prologue
    .line 1036046
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->f:Lcom/facebook/uicontrib/datepicker/Period;

    .line 1036047
    iput-object p1, v0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    .line 1036048
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->a()V

    .line 1036049
    return-void
.end method

.method public setMinimumDate(Lcom/facebook/uicontrib/datepicker/Date;)V
    .locals 1

    .prologue
    .line 1036042
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->f:Lcom/facebook/uicontrib/datepicker/Period;

    .line 1036043
    iput-object p1, v0, Lcom/facebook/uicontrib/datepicker/Period;->c:Lcom/facebook/uicontrib/datepicker/Date;

    .line 1036044
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->a()V

    .line 1036045
    return-void
.end method

.method public setOnSelectedDateChangedListener(LX/5zz;)V
    .locals 0

    .prologue
    .line 1036040
    iput-object p1, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->b:LX/5zz;

    .line 1036041
    return-void
.end method

.method public setSelectedDate(Lcom/facebook/uicontrib/datepicker/Date;)V
    .locals 1

    .prologue
    .line 1036035
    if-nez p1, :cond_1

    .line 1036036
    iget-boolean v0, p0, Lcom/facebook/uicontrib/datepicker/DatePicker;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/uicontrib/datepicker/Date;->b:Lcom/facebook/uicontrib/datepicker/Date;

    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setupSpinners(Lcom/facebook/uicontrib/datepicker/Date;)V

    .line 1036037
    :goto_1
    return-void

    .line 1036038
    :cond_0
    sget-object v0, Lcom/facebook/uicontrib/datepicker/DatePicker;->a:Lcom/facebook/uicontrib/datepicker/Date;

    goto :goto_0

    .line 1036039
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setupSpinners(Lcom/facebook/uicontrib/datepicker/Date;)V

    goto :goto_1
.end method
