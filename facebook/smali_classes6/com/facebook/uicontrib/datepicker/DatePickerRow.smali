.class public Lcom/facebook/uicontrib/datepicker/DatePickerRow;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbTextView;

.field public b:Landroid/widget/Spinner;

.field private c:Landroid/widget/ImageView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field public e:LX/5zx;

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1036297
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1036298
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1036344
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1036345
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1036341
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1036342
    invoke-direct {p0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->b()V

    .line 1036343
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1036331
    const v0, 0x7f0303d1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1036332
    const v0, 0x7f0d0beb

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1036333
    const v0, 0x7f0d0bec

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->b:Landroid/widget/Spinner;

    .line 1036334
    const v0, 0x7f0d0bed

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->c:Landroid/widget/ImageView;

    .line 1036335
    const v0, 0x7f0d0bee

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1036336
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->b:Landroid/widget/Spinner;

    new-instance v1, LX/600;

    invoke-direct {v1, p0}, LX/600;-><init>(Lcom/facebook/uicontrib/datepicker/DatePickerRow;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1036337
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->c:Landroid/widget/ImageView;

    new-instance v1, LX/601;

    invoke-direct {v1, p0}, LX/601;-><init>(Lcom/facebook/uicontrib/datepicker/DatePickerRow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1036338
    new-instance v0, LX/602;

    invoke-direct {v0, p0}, LX/602;-><init>(Lcom/facebook/uicontrib/datepicker/DatePickerRow;)V

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1036339
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->setActive(Z)V

    .line 1036340
    return-void
.end method


# virtual methods
.method public getSpinnerAdapter()Landroid/widget/SpinnerAdapter;
    .locals 1

    .prologue
    .line 1036330
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->b:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getSpinnerSelection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1036329
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->b:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setActive(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1036317
    iput-boolean p1, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    .line 1036318
    iget-boolean v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->f:Z

    if-eqz v0, :cond_0

    .line 1036319
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1036320
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1036321
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->b:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 1036322
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1036323
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->b:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1036324
    :goto_0
    return-void

    .line 1036325
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1036326
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->b:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 1036327
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1036328
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setAddViewText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1036315
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1036316
    return-void
.end method

.method public setClearButtonAccessibilityText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1036313
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1036314
    return-void
.end method

.method public setIsOptional(Z)V
    .locals 2

    .prologue
    .line 1036307
    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->c:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    const v0, 0x7f020262

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1036308
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1036309
    iget-object v1, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->c:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImportantForAccessibility(I)V

    .line 1036310
    :cond_0
    return-void

    .line 1036311
    :cond_1
    const v0, 0x106000d

    goto :goto_0

    .line 1036312
    :cond_2
    const/4 v0, 0x2

    goto :goto_1
.end method

.method public setLabelText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1036305
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1036306
    return-void
.end method

.method public setOnSelectionChangedListener(LX/5zx;)V
    .locals 0

    .prologue
    .line 1036303
    iput-object p1, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->e:LX/5zx;

    .line 1036304
    return-void
.end method

.method public setSpinnerAdapter(Landroid/widget/SpinnerAdapter;)V
    .locals 1

    .prologue
    .line 1036301
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->b:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1036302
    return-void
.end method

.method public setSpinnerSelection(I)V
    .locals 1

    .prologue
    .line 1036299
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/DatePickerRow;->b:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1036300
    return-void
.end method
