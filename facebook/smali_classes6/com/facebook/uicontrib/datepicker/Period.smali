.class public Lcom/facebook/uicontrib/datepicker/Period;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/uicontrib/datepicker/Period;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Lcom/facebook/uicontrib/datepicker/Date;

.field private static final b:Lcom/facebook/uicontrib/datepicker/Date;


# instance fields
.field public c:Lcom/facebook/uicontrib/datepicker/Date;

.field public d:Lcom/facebook/uicontrib/datepicker/Date;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1036421
    new-instance v0, LX/5zw;

    invoke-direct {v0}, LX/5zw;-><init>()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    add-int/lit8 v1, v1, -0x63

    .line 1036422
    iput v1, v0, LX/5zw;->a:I

    .line 1036423
    move-object v0, v0

    .line 1036424
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1036425
    iput-object v1, v0, LX/5zw;->b:Ljava/lang/Integer;

    .line 1036426
    move-object v0, v0

    .line 1036427
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1036428
    iput-object v1, v0, LX/5zw;->c:Ljava/lang/Integer;

    .line 1036429
    move-object v0, v0

    .line 1036430
    invoke-virtual {v0}, LX/5zw;->a()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v0

    sput-object v0, Lcom/facebook/uicontrib/datepicker/Period;->a:Lcom/facebook/uicontrib/datepicker/Date;

    .line 1036431
    new-instance v0, LX/5zw;

    invoke-direct {v0}, LX/5zw;-><init>()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 1036432
    iput v1, v0, LX/5zw;->a:I

    .line 1036433
    move-object v0, v0

    .line 1036434
    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1036435
    iput-object v1, v0, LX/5zw;->b:Ljava/lang/Integer;

    .line 1036436
    move-object v0, v0

    .line 1036437
    const/16 v1, 0x1f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1036438
    iput-object v1, v0, LX/5zw;->c:Ljava/lang/Integer;

    .line 1036439
    move-object v0, v0

    .line 1036440
    invoke-virtual {v0}, LX/5zw;->a()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v0

    sput-object v0, Lcom/facebook/uicontrib/datepicker/Period;->b:Lcom/facebook/uicontrib/datepicker/Date;

    .line 1036441
    new-instance v0, LX/605;

    invoke-direct {v0}, LX/605;-><init>()V

    sput-object v0, Lcom/facebook/uicontrib/datepicker/Period;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1036442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1036443
    sget-object v0, Lcom/facebook/uicontrib/datepicker/Period;->a:Lcom/facebook/uicontrib/datepicker/Date;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->c:Lcom/facebook/uicontrib/datepicker/Date;

    .line 1036444
    sget-object v0, Lcom/facebook/uicontrib/datepicker/Period;->b:Lcom/facebook/uicontrib/datepicker/Date;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    .line 1036445
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1036446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1036447
    const-class v0, Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/Date;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->c:Lcom/facebook/uicontrib/datepicker/Date;

    .line 1036448
    const-class v0, Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/Date;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    .line 1036449
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1036450
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->c:Lcom/facebook/uicontrib/datepicker/Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->c:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 1036451
    :cond_0
    sget-object v0, Lcom/facebook/uicontrib/datepicker/Period;->a:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->a()I

    move-result v0

    .line 1036452
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->c:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1036453
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 1036454
    :cond_0
    sget-object v0, Lcom/facebook/uicontrib/datepicker/Period;->b:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->a()I

    move-result v0

    .line 1036455
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1036456
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->c:Lcom/facebook/uicontrib/datepicker/Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->c:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1036457
    :cond_0
    sget-object v0, Lcom/facebook/uicontrib/datepicker/Period;->a:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v0

    .line 1036458
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->c:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1036459
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1036460
    :cond_0
    sget-object v0, Lcom/facebook/uicontrib/datepicker/Period;->b:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v0

    .line 1036461
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1036462
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1036463
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->c:Lcom/facebook/uicontrib/datepicker/Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->c:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->c()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1036464
    :cond_0
    sget-object v0, Lcom/facebook/uicontrib/datepicker/Period;->a:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->c()Ljava/lang/Integer;

    move-result-object v0

    .line 1036465
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->c:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->c()Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1036466
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->c()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1036467
    :cond_0
    sget-object v0, Lcom/facebook/uicontrib/datepicker/Period;->b:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->c()Ljava/lang/Integer;

    move-result-object v0

    .line 1036468
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->c()Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1036469
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->c:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1036470
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Period;->d:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1036471
    return-void
.end method
