.class public Lcom/facebook/uicontrib/datepicker/Date;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/uicontrib/datepicker/DateDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/uicontrib/datepicker/DateSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/uicontrib/datepicker/Date;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/uicontrib/datepicker/Date;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public static final b:Lcom/facebook/uicontrib/datepicker/Date;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field


# instance fields
.field private final dayOfMonth:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "day"
    .end annotation
.end field

.field private final month:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "month"
    .end annotation
.end field

.field private final year:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "year"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1035994
    const-class v0, Lcom/facebook/uicontrib/datepicker/DateDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1035998
    const-class v0, Lcom/facebook/uicontrib/datepicker/DateSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1035995
    new-instance v0, Lcom/facebook/uicontrib/datepicker/Date;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/uicontrib/datepicker/Date;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v0, Lcom/facebook/uicontrib/datepicker/Date;->a:Lcom/facebook/uicontrib/datepicker/Date;

    .line 1035996
    new-instance v0, Lcom/facebook/uicontrib/datepicker/Date;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v5, v5}, Lcom/facebook/uicontrib/datepicker/Date;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v0, Lcom/facebook/uicontrib/datepicker/Date;->b:Lcom/facebook/uicontrib/datepicker/Date;

    .line 1035997
    new-instance v0, LX/5zv;

    invoke-direct {v0}, LX/5zv;-><init>()V

    sput-object v0, Lcom/facebook/uicontrib/datepicker/Date;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1035992
    new-instance v0, LX/5zw;

    invoke-direct {v0}, LX/5zw;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/uicontrib/datepicker/Date;-><init>(LX/5zw;)V

    .line 1035993
    return-void
.end method

.method public constructor <init>(ILjava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1035987
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1035988
    iput p1, p0, Lcom/facebook/uicontrib/datepicker/Date;->year:I

    .line 1035989
    iput-object p2, p0, Lcom/facebook/uicontrib/datepicker/Date;->month:Ljava/lang/Integer;

    .line 1035990
    iput-object p3, p0, Lcom/facebook/uicontrib/datepicker/Date;->dayOfMonth:Ljava/lang/Integer;

    .line 1035991
    return-void
.end method

.method private constructor <init>(LX/5zw;)V
    .locals 1

    .prologue
    .line 1035999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1036000
    iget v0, p1, LX/5zw;->a:I

    iput v0, p0, Lcom/facebook/uicontrib/datepicker/Date;->year:I

    .line 1036001
    iget-object v0, p1, LX/5zw;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/Date;->month:Ljava/lang/Integer;

    .line 1036002
    iget-object v0, p1, LX/5zw;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/Date;->dayOfMonth:Ljava/lang/Integer;

    .line 1036003
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1035978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1035979
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/uicontrib/datepicker/Date;->year:I

    .line 1035980
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1035981
    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/facebook/uicontrib/datepicker/Date;->month:Ljava/lang/Integer;

    .line 1035982
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1035983
    if-nez v0, :cond_1

    :goto_1
    iput-object v1, p0, Lcom/facebook/uicontrib/datepicker/Date;->dayOfMonth:Ljava/lang/Integer;

    .line 1035984
    return-void

    .line 1035985
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 1035986
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1035977
    iget v0, p0, Lcom/facebook/uicontrib/datepicker/Date;->year:I

    return v0
.end method

.method public final b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1035976
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Date;->month:Ljava/lang/Integer;

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1035968
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Date;->dayOfMonth:Ljava/lang/Integer;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1035975
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1035969
    iget v0, p0, Lcom/facebook/uicontrib/datepicker/Date;->year:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1035970
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Date;->month:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1035971
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Date;->dayOfMonth:Ljava/lang/Integer;

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1035972
    return-void

    .line 1035973
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Date;->month:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 1035974
    :cond_1
    iget-object v0, p0, Lcom/facebook/uicontrib/datepicker/Date;->dayOfMonth:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1
.end method
