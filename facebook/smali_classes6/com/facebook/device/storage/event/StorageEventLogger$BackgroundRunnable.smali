.class public final Lcom/facebook/device/storage/event/StorageEventLogger$BackgroundRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:Ljava/lang/String;

.field public final synthetic b:LX/6Pv;


# direct methods
.method public constructor <init>(LX/6Pv;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1086545
    iput-object p1, p0, Lcom/facebook/device/storage/event/StorageEventLogger$BackgroundRunnable;->b:LX/6Pv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086546
    iput-object p2, p0, Lcom/facebook/device/storage/event/StorageEventLogger$BackgroundRunnable;->a:Ljava/lang/String;

    .line 1086547
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1086548
    iget-object v0, p0, Lcom/facebook/device/storage/event/StorageEventLogger$BackgroundRunnable;->b:LX/6Pv;

    iget-object v0, v0, LX/6Pv;->c:LX/2F7;

    invoke-virtual {v0}, LX/2F7;->a()Ljava/util/Map;

    move-result-object v1

    .line 1086549
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v0, p0, Lcom/facebook/device/storage/event/StorageEventLogger$BackgroundRunnable;->a:Ljava/lang/String;

    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1086550
    sget-object v0, LX/6Pv;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1086551
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1086552
    :cond_0
    iget-object v0, p0, Lcom/facebook/device/storage/event/StorageEventLogger$BackgroundRunnable;->b:LX/6Pv;

    iget-object v0, v0, LX/6Pv;->d:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1086553
    return-void
.end method
