.class public Lcom/facebook/device/storage/event/StorageEventBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public a:LX/6Pv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1086544
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/device/storage/event/StorageEventBroadcastReceiver;

    new-instance p1, LX/6Pv;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-static {v0}, LX/2F7;->b(LX/0QB;)LX/2F7;

    move-result-object v2

    check-cast v2, LX/2F7;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p1, v1, v2, v3}, LX/6Pv;-><init>(LX/0TD;LX/2F7;LX/0Zb;)V

    move-object v0, p1

    check-cast v0, LX/6Pv;

    iput-object v0, p0, Lcom/facebook/device/storage/event/StorageEventBroadcastReceiver;->a:LX/6Pv;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x20eb4aaa

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1086526
    iget-object v0, p0, Lcom/facebook/device/storage/event/StorageEventBroadcastReceiver;->a:LX/6Pv;

    if-nez v0, :cond_0

    .line 1086527
    invoke-static {p0, p1}, Lcom/facebook/device/storage/event/StorageEventBroadcastReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1086528
    :cond_0
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086529
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086530
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 1086531
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1086532
    const-string v2, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1086533
    const-string v0, "device_storage_low_warning"

    .line 1086534
    :goto_0
    iget-object v2, p0, Lcom/facebook/device/storage/event/StorageEventBroadcastReceiver;->a:LX/6Pv;

    .line 1086535
    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086536
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x1

    :goto_1
    const-string v4, "eventName must not be empty"

    invoke-static {v3, v4}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 1086537
    iget-object v3, v2, LX/6Pv;->b:LX/0TD;

    new-instance v4, Lcom/facebook/device/storage/event/StorageEventLogger$BackgroundRunnable;

    invoke-direct {v4, v2, v0}, Lcom/facebook/device/storage/event/StorageEventLogger$BackgroundRunnable;-><init>(LX/6Pv;Ljava/lang/String;)V

    const v5, -0x1ed445ef

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1086538
    const v0, 0x6e907cca

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    :goto_2
    return-void

    .line 1086539
    :cond_1
    const-string v2, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1086540
    const-string v0, "device_storage_low_resolved"

    goto :goto_0

    .line 1086541
    :cond_2
    const-class v2, Lcom/facebook/device/storage/event/StorageEventBroadcastReceiver;

    const-string v3, "Unexpected intent received \"%s\""

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1086542
    const v0, 0x777d18d8

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_2

    .line 1086543
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method
