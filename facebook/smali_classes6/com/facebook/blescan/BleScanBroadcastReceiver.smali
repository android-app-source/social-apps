.class public Lcom/facebook/blescan/BleScanBroadcastReceiver;
.super LX/25h;
.source ""


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/3C7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1063330
    const-class v0, Lcom/facebook/blescan/BleScanBroadcastReceiver;

    sput-object v0, Lcom/facebook/blescan/BleScanBroadcastReceiver;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 1063331
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "BLE_SCAN_ACTION_BLE_UPDATE"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, LX/25h;-><init>([Ljava/lang/String;)V

    .line 1063332
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1063333
    sget-object v0, LX/7cv;->f:LX/7e0;

    new-instance v1, LX/6Bz;

    invoke-direct {v1, p0}, LX/6Bz;-><init>(Lcom/facebook/blescan/BleScanBroadcastReceiver;)V

    invoke-interface {v0, p1, v1}, LX/7e0;->a(Landroid/content/Intent;LX/6By;)V

    .line 1063334
    return-void
.end method

.method private static a(Lcom/facebook/blescan/BleScanBroadcastReceiver;LX/3C7;LX/0SG;)V
    .locals 0

    .prologue
    .line 1063335
    iput-object p1, p0, Lcom/facebook/blescan/BleScanBroadcastReceiver;->a:LX/3C7;

    iput-object p2, p0, Lcom/facebook/blescan/BleScanBroadcastReceiver;->b:LX/0SG;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/blescan/BleScanBroadcastReceiver;

    invoke-static {v1}, LX/3C7;->a(LX/0QB;)LX/3C7;

    move-result-object v0

    check-cast v0, LX/3C7;

    invoke-static {v1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0, v0, v1}, Lcom/facebook/blescan/BleScanBroadcastReceiver;->a(Lcom/facebook/blescan/BleScanBroadcastReceiver;LX/3C7;LX/0SG;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1063336
    invoke-static {p0, p1}, Lcom/facebook/blescan/BleScanBroadcastReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1063337
    const-string v0, "BLE_SCAN_ACTION_BLE_UPDATE"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1063338
    invoke-direct {p0, p2}, Lcom/facebook/blescan/BleScanBroadcastReceiver;->a(Landroid/content/Intent;)V

    return-void

    .line 1063339
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
