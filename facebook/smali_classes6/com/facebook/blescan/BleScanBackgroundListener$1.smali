.class public final Lcom/facebook/blescan/BleScanBackgroundListener$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Landroid/app/PendingIntent;

.field public final synthetic c:LX/3C1;


# direct methods
.method public constructor <init>(LX/3C1;Ljava/util/List;Landroid/app/PendingIntent;)V
    .locals 0

    .prologue
    .line 1063275
    iput-object p1, p0, Lcom/facebook/blescan/BleScanBackgroundListener$1;->c:LX/3C1;

    iput-object p2, p0, Lcom/facebook/blescan/BleScanBackgroundListener$1;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/facebook/blescan/BleScanBackgroundListener$1;->b:Landroid/app/PendingIntent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1063276
    const/4 v1, 0x0

    .line 1063277
    :try_start_0
    new-instance v0, LX/2vz;

    iget-object v2, p0, Lcom/facebook/blescan/BleScanBackgroundListener$1;->c:LX/3C1;

    iget-object v2, v2, LX/3C1;->b:Landroid/content/Context;

    invoke-direct {v0, v2}, LX/2vz;-><init>(Landroid/content/Context;)V

    sget-object v2, LX/7cv;->e:LX/2vs;

    new-instance v3, LX/7e1;

    invoke-direct {v3}, LX/7e1;-><init>()V

    const/4 v4, 0x2

    iput v4, v3, LX/7e1;->a:I

    move-object v3, v3

    .line 1063278
    new-instance v4, LX/7e2;

    invoke-direct {v4, v3}, LX/7e2;-><init>(LX/7e1;)V

    move-object v3, v4

    .line 1063279
    invoke-virtual {v0, v2, v3}, LX/2vz;->a(LX/2vs;LX/2w6;)LX/2vz;

    move-result-object v0

    invoke-virtual {v0}, LX/2vz;->b()LX/2wX;

    move-result-object v1

    .line 1063280
    const-wide/16 v2, 0xa

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v0}, LX/2wX;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v0

    .line 1063281
    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1063282
    :try_start_1
    invoke-virtual {v1}, LX/2wX;->g()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1063283
    :goto_0
    return-void

    .line 1063284
    :catch_0
    move-exception v0

    .line 1063285
    iget-object v1, p0, Lcom/facebook/blescan/BleScanBackgroundListener$1;->c:LX/3C1;

    iget-object v1, v1, LX/3C1;->c:LX/03V;

    const-string v2, "fb_ble_scan_listener_google_play"

    const-string v3, "Google exception on disconnect"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1063286
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/facebook/blescan/BleScanBackgroundListener$1;->c:LX/3C1;

    iget-object v2, p0, Lcom/facebook/blescan/BleScanBackgroundListener$1;->a:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/blescan/BleScanBackgroundListener$1;->b:Landroid/app/PendingIntent;

    .line 1063287
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1063288
    :try_start_3
    const/4 v7, 0x0

    .line 1063289
    new-instance v5, LX/7dz;

    invoke-direct {v5}, LX/7dz;-><init>()V

    .line 1063290
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/UUID;

    .line 1063291
    invoke-virtual {v5, v4, v7, v7}, LX/7dz;->a(Ljava/util/UUID;Ljava/lang/Short;Ljava/lang/Short;)LX/7dz;

    goto :goto_1

    .line 1063292
    :cond_1
    invoke-virtual {v5}, LX/7dz;->b()Lcom/google/android/gms/nearby/messages/MessageFilter;

    move-result-object v4

    move-object v4, v4

    .line 1063293
    new-instance v5, LX/7e5;

    invoke-direct {v5}, LX/7e5;-><init>()V

    sget-object v6, Lcom/google/android/gms/nearby/messages/Strategy;->b:Lcom/google/android/gms/nearby/messages/Strategy;

    iput-object v6, v5, LX/7e5;->a:Lcom/google/android/gms/nearby/messages/Strategy;

    move-object v5, v5

    .line 1063294
    iput-object v4, v5, LX/7e5;->b:Lcom/google/android/gms/nearby/messages/MessageFilter;

    move-object v4, v5

    .line 1063295
    invoke-virtual {v4}, LX/7e5;->a()LX/7e6;

    move-result-object v4

    .line 1063296
    new-instance v5, LX/2rS;

    invoke-direct {v5, v0}, LX/2rS;-><init>(LX/3C1;)V

    .line 1063297
    sget-object v5, LX/7cv;->f:LX/7e0;

    invoke-interface {v5, v1, v3, v4}, LX/7e0;->a(LX/2wX;Landroid/app/PendingIntent;LX/7e6;)LX/2wg;
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1063298
    :goto_2
    :try_start_4
    invoke-virtual {v1}, LX/2wX;->g()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 1063299
    :catch_1
    move-exception v0

    .line 1063300
    iget-object v1, p0, Lcom/facebook/blescan/BleScanBackgroundListener$1;->c:LX/3C1;

    iget-object v1, v1, LX/3C1;->c:LX/03V;

    const-string v2, "fb_ble_scan_listener_google_play"

    const-string v3, "Google exception on disconnect"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1063301
    :catchall_0
    move-exception v0

    .line 1063302
    :try_start_5
    invoke-virtual {v1}, LX/2wX;->g()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_2

    .line 1063303
    :goto_3
    throw v0

    .line 1063304
    :catch_2
    move-exception v1

    .line 1063305
    iget-object v2, p0, Lcom/facebook/blescan/BleScanBackgroundListener$1;->c:LX/3C1;

    iget-object v2, v2, LX/3C1;->c:LX/03V;

    const-string v3, "fb_ble_scan_listener_google_play"

    const-string v4, "Google exception on disconnect"

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 1063306
    :catch_3
    move-exception v4

    .line 1063307
    iget-object v5, v0, LX/3C1;->c:LX/03V;

    const-string v6, "fb_ble_scan_listener_google_play"

    const-string v7, "missing permissions"

    invoke-virtual {v5, v6, v7, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1063308
    :catch_4
    move-exception v4

    .line 1063309
    invoke-static {v4}, LX/6C2;->a(Ljava/lang/RuntimeException;)V

    .line 1063310
    iget-object v5, v0, LX/3C1;->c:LX/03V;

    const-string v6, "fb_ble_scan_listener_google_play"

    const-string v7, "Google exception on request"

    invoke-virtual {v5, v6, v7, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method
