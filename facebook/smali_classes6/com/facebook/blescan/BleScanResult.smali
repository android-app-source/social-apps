.class public Lcom/facebook/blescan/BleScanResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/blescan/BleScanResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1063380
    new-instance v0, LX/6C0;

    invoke-direct {v0}, LX/6C0;-><init>()V

    sput-object v0, Lcom/facebook/blescan/BleScanResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 1063381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063382
    iput-wide p1, p0, Lcom/facebook/blescan/BleScanResult;->a:J

    .line 1063383
    iput-object p3, p0, Lcom/facebook/blescan/BleScanResult;->b:Ljava/lang/String;

    .line 1063384
    iput p4, p0, Lcom/facebook/blescan/BleScanResult;->c:I

    .line 1063385
    iput-object p5, p0, Lcom/facebook/blescan/BleScanResult;->d:Ljava/lang/String;

    .line 1063386
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1063343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063344
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/blescan/BleScanResult;->a:J

    .line 1063345
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/blescan/BleScanResult;->b:Ljava/lang/String;

    .line 1063346
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/blescan/BleScanResult;->c:I

    .line 1063347
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/blescan/BleScanResult;->d:Ljava/lang/String;

    .line 1063348
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1063362
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1063363
    if-ne p0, p1, :cond_1

    .line 1063364
    :cond_0
    :goto_0
    return v0

    .line 1063365
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1063366
    goto :goto_0

    .line 1063367
    :cond_3
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1063368
    goto :goto_0

    .line 1063369
    :cond_4
    check-cast p1, Lcom/facebook/blescan/BleScanResult;

    .line 1063370
    iget-wide v2, p0, Lcom/facebook/blescan/BleScanResult;->a:J

    iget-wide v4, p1, Lcom/facebook/blescan/BleScanResult;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 1063371
    goto :goto_0

    .line 1063372
    :cond_5
    iget v2, p0, Lcom/facebook/blescan/BleScanResult;->c:I

    iget v3, p1, Lcom/facebook/blescan/BleScanResult;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1063373
    goto :goto_0

    .line 1063374
    :cond_6
    iget-object v2, p0, Lcom/facebook/blescan/BleScanResult;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/blescan/BleScanResult;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/blescan/BleScanResult;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1063375
    goto :goto_0

    .line 1063376
    :cond_8
    iget-object v2, p1, Lcom/facebook/blescan/BleScanResult;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1063377
    :cond_9
    iget-object v2, p0, Lcom/facebook/blescan/BleScanResult;->d:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/facebook/blescan/BleScanResult;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/blescan/BleScanResult;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1063378
    goto :goto_0

    .line 1063379
    :cond_a
    iget-object v2, p1, Lcom/facebook/blescan/BleScanResult;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1063355
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 1063356
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/facebook/blescan/BleScanResult;->a:J

    iget-wide v4, p0, Lcom/facebook/blescan/BleScanResult;->a:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 1063357
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/blescan/BleScanResult;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/blescan/BleScanResult;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 1063358
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/facebook/blescan/BleScanResult;->c:I

    add-int/2addr v0, v2

    .line 1063359
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/blescan/BleScanResult;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/blescan/BleScanResult;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 1063360
    return v0

    :cond_1
    move v0, v1

    .line 1063361
    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x27

    .line 1063354
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BleScanResult{timestampMs="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/facebook/blescan/BleScanResult;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hardwareAddress=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/blescan/BleScanResult;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rssiDbm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/blescan/BleScanResult;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", payload=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/blescan/BleScanResult;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1063349
    iget-wide v0, p0, Lcom/facebook/blescan/BleScanResult;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1063350
    iget-object v0, p0, Lcom/facebook/blescan/BleScanResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1063351
    iget v0, p0, Lcom/facebook/blescan/BleScanResult;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1063352
    iget-object v0, p0, Lcom/facebook/blescan/BleScanResult;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1063353
    return-void
.end method
