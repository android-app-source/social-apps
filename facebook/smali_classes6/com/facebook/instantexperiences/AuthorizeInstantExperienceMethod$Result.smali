.class public final Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1108660
    new-instance v0, LX/6Xc;

    invoke-direct {v0}, LX/6Xc;-><init>()V

    sput-object v0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1108661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108662
    iput-object p1, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;->a:Ljava/lang/String;

    .line 1108663
    iput-object p2, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;->b:Ljava/lang/String;

    .line 1108664
    iput-object p3, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;->c:Ljava/util/List;

    .line 1108665
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1108666
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1108667
    iget-object v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1108668
    iget-object v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1108669
    iget-object v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Result;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1108670
    return-void
.end method
