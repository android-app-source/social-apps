.class public final Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1108632
    new-instance v0, LX/6Xb;

    invoke-direct {v0}, LX/6Xb;-><init>()V

    sput-object v0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1108641
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108642
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->a:Ljava/lang/String;

    .line 1108643
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->b:Ljava/util/List;

    .line 1108644
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->c:Ljava/lang/String;

    .line 1108645
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->d:Z

    .line 1108646
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->e:Ljava/lang/String;

    .line 1108647
    return-void

    .line 1108648
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1108649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108650
    iput-object p1, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->a:Ljava/lang/String;

    .line 1108651
    iput-object p2, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->b:Ljava/util/List;

    .line 1108652
    iput-object p3, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->c:Ljava/lang/String;

    .line 1108653
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->d:Z

    .line 1108654
    iput-object p5, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->e:Ljava/lang/String;

    .line 1108655
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1108640
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1108633
    iget-object v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1108634
    iget-object v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1108635
    iget-object v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1108636
    iget-boolean v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1108637
    iget-object v0, p0, Lcom/facebook/instantexperiences/AuthorizeInstantExperienceMethod$Params;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1108638
    return-void

    .line 1108639
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
