.class public Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphView;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/widget/SwitchCompat;

.field private d:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 972259
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 972260
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->b()V

    .line 972261
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 972262
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 972263
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->b()V

    .line 972264
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 972265
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 972266
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->b()V

    .line 972267
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 972268
    const v0, 0x7f030eba

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 972269
    const v0, 0x7f0d23fb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 972270
    const v0, 0x7f0d23fc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 972271
    const v0, 0x7f0d23fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->c:Lcom/facebook/widget/SwitchCompat;

    .line 972272
    const v0, 0x7f0d23fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 972273
    return-void
.end method

.method public static setVisibilitySwitchText(Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;Z)V
    .locals 2

    .prologue
    .line 972274
    iget-object v1, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->d:Lcom/facebook/resources/ui/FbTextView;

    if-eqz p1, :cond_0

    const v0, 0x7f08177e

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 972275
    return-void

    .line 972276
    :cond_0
    const v0, 0x7f08177f

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 972277
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->setVisibility(I)V

    .line 972278
    return-void
.end method

.method public final a(LX/5fq;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 972279
    if-nez p1, :cond_0

    .line 972280
    invoke-virtual {p0, v3}, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->setVisibility(I)V

    .line 972281
    :goto_0
    return-void

    .line 972282
    :cond_0
    iget-boolean v0, p1, LX/5fq;->b:Z

    if-eqz v0, :cond_1

    .line 972283
    iget-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 972284
    iget-boolean v0, p1, LX/5fq;->a:Z

    invoke-static {p0, v0}, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->setVisibilitySwitchText(Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;Z)V

    .line 972285
    iget-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->c:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/SwitchCompat;->setVisibility(I)V

    .line 972286
    iget-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->c:Lcom/facebook/widget/SwitchCompat;

    iget-boolean v1, p1, LX/5fq;->a:Z

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 972287
    iget-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->c:Lcom/facebook/widget/SwitchCompat;

    new-instance v1, LX/34K;

    invoke-direct {v1, p0, p1}, LX/34K;-><init>(Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;LX/5fq;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 972288
    :goto_1
    iget-object v0, p1, LX/5fq;->d:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_2

    .line 972289
    iget-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 972290
    iget-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p1, LX/5fq;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 972291
    iget-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p1, LX/5fq;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 972292
    :goto_2
    invoke-virtual {p0, v2}, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->setVisibility(I)V

    goto :goto_0

    .line 972293
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 972294
    iget-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->c:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/SwitchCompat;->setVisibility(I)V

    .line 972295
    iget-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->c:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_1

    .line 972296
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 972297
    iget-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 972298
    iget-object v0, p0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method
