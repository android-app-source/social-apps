.class public Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private k:Lcom/facebook/widget/text/BetterTextView;

.field private l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private m:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 972200
    const-class v0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 972201
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 972202
    invoke-direct {p0}, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->e()V

    .line 972203
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 972204
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 972205
    invoke-direct {p0}, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->e()V

    .line 972206
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 972207
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 972208
    invoke-direct {p0}, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->e()V

    .line 972209
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 972210
    const v0, 0x7f031337

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 972211
    const v0, 0x7f0d2c88

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 972212
    const v0, 0x7f0d2c87

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 972213
    const v0, 0x7f0d2c89

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->m:Lcom/facebook/fbui/glyph/GlyphView;

    .line 972214
    return-void
.end method


# virtual methods
.method public final a(LX/5fp;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 972215
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->k:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/5fp;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 972216
    iget-object v0, p1, LX/5fp;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 972217
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p1, LX/5fp;->b:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 972218
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 972219
    :goto_0
    iget-boolean v0, p1, LX/5fp;->c:Z

    if-eqz v0, :cond_1

    .line 972220
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->m:Lcom/facebook/fbui/glyph/GlyphView;

    iget v1, p1, LX/5fp;->d:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 972221
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->m:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p1, LX/5fp;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 972222
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->m:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 972223
    :goto_1
    return-void

    .line 972224
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 972225
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    goto :goto_0

    .line 972226
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->m:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final b(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 972227
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 972228
    if-eqz p1, :cond_0

    .line 972229
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 972230
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 972231
    :goto_0
    return-void

    .line 972232
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 972233
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 972234
    iget-object v0, p0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->m:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 972235
    return-void
.end method
