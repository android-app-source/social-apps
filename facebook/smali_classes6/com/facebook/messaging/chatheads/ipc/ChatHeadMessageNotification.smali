.class public Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/messaging/model/messages/Message;

.field private final b:Z

.field private final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1114113
    new-instance v0, LX/6c1;

    invoke-direct {v0}, LX/6c1;-><init>()V

    sput-object v0, Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1114114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1114115
    const-class v0, Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    iput-object v0, p0, Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 1114116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    .line 1114117
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;->b:Z

    .line 1114118
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;->c:Z

    .line 1114119
    return-void

    :cond_0
    move v0, v2

    .line 1114120
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1114121
    goto :goto_1
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/messages/Message;ZZ)V
    .locals 0

    .prologue
    .line 1114122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1114123
    iput-object p1, p0, Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 1114124
    iput-boolean p2, p0, Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;->b:Z

    .line 1114125
    iput-boolean p3, p0, Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;->c:Z

    .line 1114126
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1114127
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1114128
    iget-object v0, p0, Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1114129
    iget-object v0, p0, Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1114130
    iget-boolean v0, p0, Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1114131
    iget-boolean v0, p0, Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;->c:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1114132
    return-void

    :cond_0
    move v0, v2

    .line 1114133
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1114134
    goto :goto_1
.end method
