.class public final Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 963760
    const-class v0, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel;

    new-instance v1, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 963761
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 963762
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 963763
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 963764
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 963765
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 963766
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 963767
    if-eqz v2, :cond_2

    .line 963768
    const-string p0, "thread"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963769
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 963770
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 963771
    if-eqz p0, :cond_1

    .line 963772
    const-string v0, "thread_key"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963773
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 963774
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 963775
    if-eqz v0, :cond_0

    .line 963776
    const-string v2, "thread_fbid"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963777
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963778
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 963779
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 963780
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 963781
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 963782
    check-cast p1, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel$Serializer;->a(Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
