.class public final Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 965308
    const-class v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 965309
    const-class v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 965310
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 965311
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 965312
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 965313
    iget v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 965314
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 965315
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 965316
    iget v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 965317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 965318
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 965319
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 965320
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 965321
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 965322
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 965323
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;->e:I

    .line 965324
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 965325
    new-instance v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    invoke-direct {v0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;-><init>()V

    .line 965326
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 965327
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 965328
    const v0, 0x67176002

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 965329
    const v0, 0x72ac7576

    return v0
.end method
