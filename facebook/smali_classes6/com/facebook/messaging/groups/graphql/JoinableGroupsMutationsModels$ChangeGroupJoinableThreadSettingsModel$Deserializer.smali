.class public final Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupJoinableThreadSettingsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 964702
    const-class v0, Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupJoinableThreadSettingsModel;

    new-instance v1, Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupJoinableThreadSettingsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupJoinableThreadSettingsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 964703
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 964704
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 964705
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 964706
    const/4 v2, 0x0

    .line 964707
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 964708
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 964709
    :goto_0
    move v1, v2

    .line 964710
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 964711
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 964712
    new-instance v1, Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupJoinableThreadSettingsModel;

    invoke-direct {v1}, Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupJoinableThreadSettingsModel;-><init>()V

    .line 964713
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 964714
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 964715
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 964716
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 964717
    :cond_0
    return-object v1

    .line 964718
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 964719
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 964720
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 964721
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 964722
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 964723
    const-string v4, "thread"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 964724
    const/4 v3, 0x0

    .line 964725
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 964726
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 964727
    :goto_2
    move v1, v3

    .line 964728
    goto :goto_1

    .line 964729
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 964730
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 964731
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 964732
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 964733
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 964734
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 964735
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 964736
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, p0, :cond_6

    if-eqz v4, :cond_6

    .line 964737
    const-string v5, "joinable_mode"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 964738
    const/4 v4, 0x0

    .line 964739
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_c

    .line 964740
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 964741
    :goto_4
    move v1, v4

    .line 964742
    goto :goto_3

    .line 964743
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 964744
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 964745
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3

    .line 964746
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 964747
    :cond_a
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_b

    .line 964748
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 964749
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 964750
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_a

    if-eqz v5, :cond_a

    .line 964751
    const-string p0, "link"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 964752
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_5

    .line 964753
    :cond_b
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 964754
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 964755
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_c
    move v1, v4

    goto :goto_5
.end method
