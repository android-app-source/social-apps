.class public final Lcom/facebook/messaging/groups/graphql/GroupHashQueryModels$GroupThreadInfoQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/groups/graphql/GroupHashQueryModels$GroupThreadInfoQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 964132
    const-class v0, Lcom/facebook/messaging/groups/graphql/GroupHashQueryModels$GroupThreadInfoQueryModel;

    new-instance v1, Lcom/facebook/messaging/groups/graphql/GroupHashQueryModels$GroupThreadInfoQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/groups/graphql/GroupHashQueryModels$GroupThreadInfoQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 964133
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 964134
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/groups/graphql/GroupHashQueryModels$GroupThreadInfoQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 964135
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 964136
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v2, 0x0

    .line 964137
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 964138
    invoke-virtual {v1, v0, v2, v2}, LX/15i;->a(III)I

    move-result v2

    .line 964139
    if-eqz v2, :cond_0

    .line 964140
    const-string v3, "approval_mode"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964141
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 964142
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 964143
    if-eqz v2, :cond_2

    .line 964144
    const-string v3, "approval_requests"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964145
    const/4 v3, 0x0

    .line 964146
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 964147
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 964148
    if-eqz v3, :cond_1

    .line 964149
    const-string p0, "count"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964150
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 964151
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 964152
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 964153
    if-eqz v2, :cond_3

    .line 964154
    const-string v3, "customization_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964155
    invoke-static {v1, v2, p1}, LX/5dD;->a(LX/15i;ILX/0nX;)V

    .line 964156
    :cond_3
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 964157
    if-eqz v2, :cond_4

    .line 964158
    const-string v3, "description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964159
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 964160
    :cond_4
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 964161
    if-eqz v2, :cond_5

    .line 964162
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964163
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 964164
    :cond_5
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 964165
    if-eqz v2, :cond_7

    .line 964166
    const-string v3, "image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964167
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 964168
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 964169
    if-eqz v3, :cond_6

    .line 964170
    const-string p0, "uri"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964171
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 964172
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 964173
    :cond_7
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 964174
    if-eqz v2, :cond_8

    .line 964175
    const-string v3, "is_viewer_subscribed"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964176
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 964177
    :cond_8
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 964178
    if-eqz v2, :cond_9

    .line 964179
    const-string v3, "participant_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964180
    invoke-static {v1, v2, p1}, LX/5dI;->a(LX/15i;ILX/0nX;)V

    .line 964181
    :cond_9
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 964182
    if-eqz v2, :cond_b

    .line 964183
    const-string v3, "thread_admins"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964184
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 964185
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_a

    .line 964186
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/5cx;->a(LX/15i;ILX/0nX;)V

    .line 964187
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 964188
    :cond_a
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 964189
    :cond_b
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 964190
    if-eqz v2, :cond_c

    .line 964191
    const-string v3, "thread_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964192
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 964193
    :cond_c
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 964194
    if-eqz v2, :cond_d

    .line 964195
    const-string v3, "thread_queue_participants"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964196
    invoke-static {v1, v2, p1, p2}, LX/5dH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 964197
    :cond_d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 964198
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 964199
    check-cast p1, Lcom/facebook/messaging/groups/graphql/GroupHashQueryModels$GroupThreadInfoQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/groups/graphql/GroupHashQueryModels$GroupThreadInfoQueryModel$Serializer;->a(Lcom/facebook/messaging/groups/graphql/GroupHashQueryModels$GroupThreadInfoQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
