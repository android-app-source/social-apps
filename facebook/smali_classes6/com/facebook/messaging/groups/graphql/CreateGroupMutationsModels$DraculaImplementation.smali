.class public final Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 963870
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 963871
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 963868
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 963869
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 963854
    if-nez p1, :cond_0

    .line 963855
    :goto_0
    return v0

    .line 963856
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 963857
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 963858
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 963859
    const v2, -0x5a5cbd89

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 963860
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 963861
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 963862
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 963863
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 963864
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 963865
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 963866
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 963867
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x628104ec -> :sswitch_0
        -0x5a5cbd89 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 963853
    new-instance v0, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 963848
    sparse-switch p2, :sswitch_data_0

    .line 963849
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 963850
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 963851
    const v1, -0x5a5cbd89

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 963852
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x628104ec -> :sswitch_0
        -0x5a5cbd89 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 963842
    if-eqz p1, :cond_0

    .line 963843
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;

    move-result-object v1

    .line 963844
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;

    .line 963845
    if-eq v0, v1, :cond_0

    .line 963846
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 963847
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 963841
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 963839
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 963840
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 963872
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 963873
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 963874
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;->a:LX/15i;

    .line 963875
    iput p2, p0, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;->b:I

    .line 963876
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 963838
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 963837
    new-instance v0, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 963834
    iget v0, p0, LX/1vt;->c:I

    .line 963835
    move v0, v0

    .line 963836
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 963813
    iget v0, p0, LX/1vt;->c:I

    .line 963814
    move v0, v0

    .line 963815
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 963831
    iget v0, p0, LX/1vt;->b:I

    .line 963832
    move v0, v0

    .line 963833
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 963828
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 963829
    move-object v0, v0

    .line 963830
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 963819
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 963820
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 963821
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 963822
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 963823
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 963824
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 963825
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 963826
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 963827
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 963816
    iget v0, p0, LX/1vt;->c:I

    .line 963817
    move v0, v0

    .line 963818
    return v0
.end method
