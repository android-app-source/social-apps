.class public final Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5de35f69
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 965405
    const-class v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 965423
    const-class v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 965421
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 965422
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 965410
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 965411
    invoke-virtual {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 965412
    invoke-virtual {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 965413
    invoke-virtual {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 965414
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 965415
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 965416
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 965417
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 965418
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 965419
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 965420
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 965407
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 965408
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 965409
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 965406
    new-instance v0, LX/5dF;

    invoke-direct {v0, p1}, LX/5dF;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 965404
    invoke-virtual {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 965424
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 965425
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->g:Z

    .line 965426
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 965402
    invoke-virtual {p2}, LX/18L;->a()V

    .line 965403
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 965401
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 965390
    new-instance v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;

    invoke-direct {v0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;-><init>()V

    .line 965391
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 965392
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 965400
    const v0, 0x1cf74495

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 965399
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 965397
    iget-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->e:Ljava/lang/String;

    .line 965398
    iget-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 965395
    iget-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->f:Ljava/lang/String;

    .line 965396
    iget-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 965393
    iget-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->h:Ljava/lang/String;

    .line 965394
    iget-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method
