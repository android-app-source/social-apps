.class public final Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/5Wg;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1971f2b5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 965124
    const-class v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 965100
    const-class v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 965122
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 965123
    return-void
.end method

.method private a()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 965120
    iget-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    .line 965121
    iget-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 965114
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 965115
    invoke-direct {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel;->a()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 965116
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 965117
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 965118
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 965119
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 965106
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 965107
    invoke-direct {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel;->a()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 965108
    invoke-direct {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel;->a()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    .line 965109
    invoke-direct {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel;->a()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 965110
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel;

    .line 965111
    iput-object v0, v1, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    .line 965112
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 965113
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 965103
    new-instance v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel;

    invoke-direct {v0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel;-><init>()V

    .line 965104
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 965105
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 965102
    const v0, 0x719884c4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 965101
    const v0, -0x20950cd6

    return v0
.end method
