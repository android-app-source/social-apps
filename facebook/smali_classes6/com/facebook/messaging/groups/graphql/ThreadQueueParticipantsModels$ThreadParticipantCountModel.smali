.class public final Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x254802e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 965343
    const-class v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 965344
    const-class v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 965345
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 965346
    return-void
.end method

.method private a()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 965347
    iget-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    iput-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    .line 965348
    iget-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 965349
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 965350
    invoke-direct {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel;->a()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 965351
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 965352
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 965353
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 965354
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 965355
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 965356
    invoke-direct {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel;->a()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 965357
    invoke-direct {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel;->a()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    .line 965358
    invoke-direct {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel;->a()Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 965359
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel;

    .line 965360
    iput-object v0, v1, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel$ParticipantCountModel;

    .line 965361
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 965362
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 965363
    new-instance v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel;

    invoke-direct {v0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadParticipantCountModel;-><init>()V

    .line 965364
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 965365
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 965366
    const v0, 0x3094920f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 965367
    const v0, -0x20950cd6

    return v0
.end method
