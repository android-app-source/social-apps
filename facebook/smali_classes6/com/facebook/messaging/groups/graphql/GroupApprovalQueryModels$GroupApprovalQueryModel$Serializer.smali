.class public final Lcom/facebook/messaging/groups/graphql/GroupApprovalQueryModels$GroupApprovalQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/groups/graphql/GroupApprovalQueryModels$GroupApprovalQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 963948
    const-class v0, Lcom/facebook/messaging/groups/graphql/GroupApprovalQueryModels$GroupApprovalQueryModel;

    new-instance v1, Lcom/facebook/messaging/groups/graphql/GroupApprovalQueryModels$GroupApprovalQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/groups/graphql/GroupApprovalQueryModels$GroupApprovalQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 963949
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 963950
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/groups/graphql/GroupApprovalQueryModels$GroupApprovalQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 963951
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 963952
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 963953
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 963954
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 963955
    if-eqz v2, :cond_0

    .line 963956
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963957
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 963958
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 963959
    if-eqz v2, :cond_1

    .line 963960
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963961
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963962
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 963963
    if-eqz v2, :cond_3

    .line 963964
    const-string p0, "mutual_friends"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963965
    const/4 p0, 0x0

    .line 963966
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 963967
    invoke-virtual {v1, v2, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 963968
    if-eqz p0, :cond_2

    .line 963969
    const-string p2, "count"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963970
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 963971
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 963972
    :cond_3
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 963973
    if-eqz v2, :cond_4

    .line 963974
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963975
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963976
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 963977
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 963978
    check-cast p1, Lcom/facebook/messaging/groups/graphql/GroupApprovalQueryModels$GroupApprovalQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/groups/graphql/GroupApprovalQueryModels$GroupApprovalQueryModel$Serializer;->a(Lcom/facebook/messaging/groups/graphql/GroupApprovalQueryModels$GroupApprovalQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
