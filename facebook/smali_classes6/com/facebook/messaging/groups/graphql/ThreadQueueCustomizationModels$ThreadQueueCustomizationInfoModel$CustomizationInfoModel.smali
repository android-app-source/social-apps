.class public final Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5167f2a7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 965060
    const-class v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 965059
    const-class v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 965057
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 965058
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 965054
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 965055
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 965056
    return-void
.end method

.method public static a(Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;)Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;
    .locals 8

    .prologue
    .line 965034
    if-nez p0, :cond_0

    .line 965035
    const/4 p0, 0x0

    .line 965036
    :goto_0
    return-object p0

    .line 965037
    :cond_0
    instance-of v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    if-eqz v0, :cond_1

    .line 965038
    check-cast p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    goto :goto_0

    .line 965039
    :cond_1
    new-instance v0, LX/5dB;

    invoke-direct {v0}, LX/5dB;-><init>()V

    .line 965040
    invoke-virtual {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5dB;->a:Ljava/lang/String;

    .line 965041
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 965042
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 965043
    iget-object v3, v0, LX/5dB;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 965044
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 965045
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 965046
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 965047
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 965048
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 965049
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 965050
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 965051
    new-instance v3, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    invoke-direct {v3, v2}, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;-><init>(LX/15i;)V

    .line 965052
    move-object p0, v3

    .line 965053
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 965018
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 965019
    invoke-virtual {p0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 965020
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 965021
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 965022
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 965023
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 965031
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 965032
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 965033
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 965029
    iget-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;->e:Ljava/lang/String;

    .line 965030
    iget-object v0, p0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 965026
    new-instance v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    invoke-direct {v0}, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;-><init>()V

    .line 965027
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 965028
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 965025
    const v0, 0x282915e4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 965024
    const v0, -0x3b2f557e

    return v0
.end method
