.class public final Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 965379
    const-class v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;

    new-instance v1, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 965380
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 965378
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 965368
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 965369
    invoke-static {p1, v0}, LX/5dJ;->b(LX/15w;LX/186;)I

    move-result v1

    .line 965370
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 965371
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 965372
    new-instance v1, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;

    invoke-direct {v1}, Lcom/facebook/messaging/groups/graphql/ThreadQueueParticipantsModels$ThreadQueueParticipantFragmentModel;-><init>()V

    .line 965373
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 965374
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 965375
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 965376
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 965377
    :cond_0
    return-object v1
.end method
