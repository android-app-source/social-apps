.class public final Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 963706
    const-class v0, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel;

    new-instance v1, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 963707
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 963708
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 963709
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 963710
    const/4 v2, 0x0

    .line 963711
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 963712
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 963713
    :goto_0
    move v1, v2

    .line 963714
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 963715
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 963716
    new-instance v1, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel;

    invoke-direct {v1}, Lcom/facebook/messaging/groups/graphql/CreateGroupMutationsModels$CreateGroupQueryModel;-><init>()V

    .line 963717
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 963718
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 963719
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 963720
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 963721
    :cond_0
    return-object v1

    .line 963722
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 963723
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 963724
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 963725
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 963726
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 963727
    const-string v4, "thread"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 963728
    const/4 v3, 0x0

    .line 963729
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 963730
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 963731
    :goto_2
    move v1, v3

    .line 963732
    goto :goto_1

    .line 963733
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 963734
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 963735
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 963736
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 963737
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 963738
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 963739
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 963740
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, p0, :cond_6

    if-eqz v4, :cond_6

    .line 963741
    const-string v5, "thread_key"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 963742
    const/4 v4, 0x0

    .line 963743
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_c

    .line 963744
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 963745
    :goto_4
    move v1, v4

    .line 963746
    goto :goto_3

    .line 963747
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 963748
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 963749
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3

    .line 963750
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 963751
    :cond_a
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_b

    .line 963752
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 963753
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 963754
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_a

    if-eqz v5, :cond_a

    .line 963755
    const-string p0, "thread_fbid"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 963756
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_5

    .line 963757
    :cond_b
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 963758
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 963759
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_c
    move v1, v4

    goto :goto_5
.end method
