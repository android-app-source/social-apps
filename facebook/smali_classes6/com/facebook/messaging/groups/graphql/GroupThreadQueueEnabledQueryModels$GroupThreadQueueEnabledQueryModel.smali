.class public final Lcom/facebook/messaging/groups/graphql/GroupThreadQueueEnabledQueryModels$GroupThreadQueueEnabledQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2899592d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/groups/graphql/GroupThreadQueueEnabledQueryModels$GroupThreadQueueEnabledQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/groups/graphql/GroupThreadQueueEnabledQueryModels$GroupThreadQueueEnabledQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 964532
    const-class v0, Lcom/facebook/messaging/groups/graphql/GroupThreadQueueEnabledQueryModels$GroupThreadQueueEnabledQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 964553
    const-class v0, Lcom/facebook/messaging/groups/graphql/GroupThreadQueueEnabledQueryModels$GroupThreadQueueEnabledQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 964551
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 964552
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 964546
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 964547
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 964548
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/messaging/groups/graphql/GroupThreadQueueEnabledQueryModels$GroupThreadQueueEnabledQueryModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 964549
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 964550
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 964543
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 964544
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 964545
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 964540
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 964541
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/groups/graphql/GroupThreadQueueEnabledQueryModels$GroupThreadQueueEnabledQueryModel;->e:Z

    .line 964542
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 964538
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 964539
    iget-boolean v0, p0, Lcom/facebook/messaging/groups/graphql/GroupThreadQueueEnabledQueryModels$GroupThreadQueueEnabledQueryModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 964535
    new-instance v0, Lcom/facebook/messaging/groups/graphql/GroupThreadQueueEnabledQueryModels$GroupThreadQueueEnabledQueryModel;

    invoke-direct {v0}, Lcom/facebook/messaging/groups/graphql/GroupThreadQueueEnabledQueryModels$GroupThreadQueueEnabledQueryModel;-><init>()V

    .line 964536
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 964537
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 964534
    const v0, -0x4b492b87

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 964533
    const v0, -0x2c24372f

    return v0
.end method
