.class public final Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupApprovalModeSettingModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupApprovalModeSettingModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 964669
    const-class v0, Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupApprovalModeSettingModel;

    new-instance v1, Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupApprovalModeSettingModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupApprovalModeSettingModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 964670
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 964671
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupApprovalModeSettingModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 964672
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 964673
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 964674
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 964675
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 964676
    if-eqz p0, :cond_0

    .line 964677
    const-string p2, "client_mutation_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 964678
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 964679
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 964680
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 964681
    check-cast p1, Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupApprovalModeSettingModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupApprovalModeSettingModel$Serializer;->a(Lcom/facebook/messaging/groups/graphql/JoinableGroupsMutationsModels$ChangeGroupApprovalModeSettingModel;LX/0nX;LX/0my;)V

    return-void
.end method
