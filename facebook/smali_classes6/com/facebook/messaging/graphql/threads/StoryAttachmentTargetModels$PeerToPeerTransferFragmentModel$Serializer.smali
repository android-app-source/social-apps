.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 938119
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 938120
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 938122
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 938123
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 938124
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 938125
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 938126
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 938127
    if-eqz v2, :cond_0

    .line 938128
    const-string p0, "amount"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 938129
    invoke-static {v1, v2, p1}, LX/5Yo;->a(LX/15i;ILX/0nX;)V

    .line 938130
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 938131
    if-eqz v2, :cond_1

    .line 938132
    const-string p0, "payment_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 938133
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 938134
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 938135
    if-eqz v2, :cond_2

    .line 938136
    const-string p0, "receiver"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 938137
    invoke-static {v1, v2, p1}, LX/5Yp;->a(LX/15i;ILX/0nX;)V

    .line 938138
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 938139
    if-eqz v2, :cond_3

    .line 938140
    const-string p0, "sender"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 938141
    invoke-static {v1, v2, p1}, LX/5Yq;->a(LX/15i;ILX/0nX;)V

    .line 938142
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 938143
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 938121
    check-cast p1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
