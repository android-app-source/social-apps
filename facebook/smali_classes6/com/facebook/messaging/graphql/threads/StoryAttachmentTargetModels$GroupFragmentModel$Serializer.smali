.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 933907
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 933908
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 933909
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 933910
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 933911
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x6

    const/4 v4, 0x5

    .line 933912
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 933913
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 933914
    if-eqz v2, :cond_0

    .line 933915
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 933916
    invoke-static {v1, v2, p1, p2}, LX/5Y6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 933917
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 933918
    if-eqz v2, :cond_1

    .line 933919
    const-string v3, "group_friend_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 933920
    invoke-static {v1, v2, p1}, LX/5Y7;->a(LX/15i;ILX/0nX;)V

    .line 933921
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 933922
    if-eqz v2, :cond_2

    .line 933923
    const-string v3, "group_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 933924
    invoke-static {v1, v2, p1}, LX/5Y8;->a(LX/15i;ILX/0nX;)V

    .line 933925
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 933926
    if-eqz v2, :cond_3

    .line 933927
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 933928
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 933929
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 933930
    if-eqz v2, :cond_4

    .line 933931
    const-string v3, "viewer_invite_to_group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 933932
    invoke-static {v1, v2, p1, p2}, LX/5YA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 933933
    :cond_4
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 933934
    if-eqz v2, :cond_5

    .line 933935
    const-string v2, "viewer_join_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 933936
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 933937
    :cond_5
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 933938
    if-eqz v2, :cond_6

    .line 933939
    const-string v2, "visibility"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 933940
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 933941
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 933942
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 933943
    check-cast p1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
