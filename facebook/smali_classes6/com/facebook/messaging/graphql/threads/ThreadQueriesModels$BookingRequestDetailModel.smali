.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x56d6106c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:J

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 944523
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 944522
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 944520
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 944521
    return-void
.end method

.method private k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 944464
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->g:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->g:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    .line 944465
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->g:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    return-object v0
.end method

.method private l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProductItem"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 944518
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->h:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->h:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    .line 944519
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->h:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    return-object v0
.end method

.method private m()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 944516
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->k:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->k:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    .line 944517
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->k:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 944499
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 944500
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->b()Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 944501
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 944502
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 944503
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 944504
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->cW_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 944505
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->m()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 944506
    const/4 v4, 0x7

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 944507
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 944508
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 944509
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 944510
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 944511
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->i:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 944512
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 944513
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 944514
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 944515
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 944481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 944482
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 944483
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    .line 944484
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 944485
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;

    .line 944486
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->g:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    .line 944487
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 944488
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    .line 944489
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 944490
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;

    .line 944491
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->h:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    .line 944492
    :cond_1
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->m()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 944493
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->m()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    .line 944494
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->m()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 944495
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;

    .line 944496
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->k:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    .line 944497
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 944498
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 944480
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 944477
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 944478
    const/4 v0, 0x4

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->i:J

    .line 944479
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 944524
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 944525
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 944461
    new-instance v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;-><init>()V

    .line 944462
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 944463
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 944466
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->f:Ljava/lang/String;

    .line 944467
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final cW_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 944468
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->j:Ljava/lang/String;

    .line 944469
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic cX_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 944470
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->m()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 944471
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 944472
    const v0, 0x57b346e

    return v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 944473
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 944474
    iget-wide v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->i:J

    return-wide v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 944475
    const v0, -0x55958a16

    return v0
.end method

.method public final synthetic j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProductItem"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 944476
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    move-result-object v0

    return-object v0
.end method
