.class public final Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 923009
    const-class v0, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 923010
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 923008
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 923011
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 923012
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 923013
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 923014
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 923015
    if-eqz v2, :cond_0

    .line 923016
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923017
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 923018
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 923019
    if-eqz v2, :cond_1

    .line 923020
    const-string p0, "attribution_app"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923021
    invoke-static {v1, v2, p1, p2}, LX/5Tw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 923022
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 923023
    if-eqz v2, :cond_2

    .line 923024
    const-string p0, "attribution_app_scoped_ids"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923025
    invoke-static {v1, v2, p1, p2}, LX/5Tx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 923026
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 923027
    if-eqz v2, :cond_3

    .line 923028
    const-string p0, "attribution_metadata"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923029
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 923030
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 923031
    if-eqz v2, :cond_4

    .line 923032
    const-string p0, "messaging_attribution"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923033
    invoke-static {v1, v2, p1}, LX/5Ty;->a(LX/15i;ILX/0nX;)V

    .line 923034
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 923035
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 923007
    check-cast p1, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel;LX/0nX;LX/0my;)V

    return-void
.end method
