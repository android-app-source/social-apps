.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5f26ef09
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I

.field private k:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 951789
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 951794
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 951792
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 951793
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 951790
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 951791
    iget v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->f:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 951756
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 951757
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 951758
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 951759
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 951760
    const/4 v3, 0x7

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 951761
    iget v3, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->e:I

    invoke-virtual {p1, v5, v3, v5}, LX/186;->a(III)V

    .line 951762
    const/4 v3, 0x1

    iget v4, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->f:I

    invoke-virtual {p1, v3, v4, v5}, LX/186;->a(III)V

    .line 951763
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 951764
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 951765
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 951766
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->j:I

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 951767
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->k:I

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 951768
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 951769
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 951776
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 951777
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 951778
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 951779
    if-eqz v1, :cond_2

    .line 951780
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    .line 951781
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->g:Ljava/util/List;

    move-object v1, v0

    .line 951782
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 951783
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;

    .line 951784
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 951785
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    .line 951786
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->h:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;

    .line 951787
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 951788
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 951770
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 951771
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->e:I

    .line 951772
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->f:I

    .line 951773
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->j:I

    .line 951774
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->k:I

    .line 951775
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 951795
    new-instance v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;-><init>()V

    .line 951796
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 951797
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 951744
    const v0, 0x7a752763

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 951745
    const v0, 0x6cff0a72

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNodes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 951746
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->g:Ljava/util/List;

    .line 951747
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 951748
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->h:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->h:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;

    .line 951749
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->h:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 951750
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->i:Ljava/lang/String;

    .line 951751
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final m()I
    .locals 2

    .prologue
    .line 951752
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 951753
    iget v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->j:I

    return v0
.end method

.method public final n()I
    .locals 2

    .prologue
    .line 951754
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 951755
    iget v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->k:I

    return v0
.end method
