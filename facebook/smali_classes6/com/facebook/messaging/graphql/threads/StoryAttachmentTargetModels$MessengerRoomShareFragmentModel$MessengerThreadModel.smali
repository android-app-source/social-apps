.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/5Wg;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3e947eb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 936105
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 936106
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 936133
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 936134
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 936107
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 936108
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 936109
    return-void
.end method

.method public static a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;)Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;
    .locals 9

    .prologue
    .line 936110
    if-nez p0, :cond_0

    .line 936111
    const/4 p0, 0x0

    .line 936112
    :goto_0
    return-object p0

    .line 936113
    :cond_0
    instance-of v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;

    if-eqz v0, :cond_1

    .line 936114
    check-cast p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;

    goto :goto_0

    .line 936115
    :cond_1
    new-instance v0, LX/5XR;

    invoke-direct {v0}, LX/5XR;-><init>()V

    .line 936116
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->a()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;->a(Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;)Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v1

    iput-object v1, v0, LX/5XR;->a:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    .line 936117
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->b()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;->a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;)Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    move-result-object v1

    iput-object v1, v0, LX/5XR;->b:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    .line 936118
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 936119
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 936120
    iget-object v3, v0, LX/5XR;->a:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 936121
    iget-object v5, v0, LX/5XR;->b:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 936122
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 936123
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 936124
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 936125
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 936126
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 936127
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 936128
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 936129
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 936130
    new-instance v3, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;

    invoke-direct {v3, v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;-><init>(LX/15i;)V

    .line 936131
    move-object p0, v3

    .line 936132
    goto :goto_0
.end method

.method private j()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936101
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    .line 936102
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    return-object v0
.end method

.method private k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936103
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    .line 936104
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 936086
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 936087
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->j()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 936088
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 936089
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 936090
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 936091
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 936092
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 936093
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 936073
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 936074
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->j()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 936075
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->j()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    .line 936076
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->j()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 936077
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;

    .line 936078
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->e:Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    .line 936079
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 936080
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    .line 936081
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 936082
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;

    .line 936083
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    .line 936084
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 936085
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936100
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->j()Lcom/facebook/messaging/groups/graphql/ThreadQueueCustomizationModels$ThreadQueueCustomizationInfoModel$CustomizationInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 936094
    new-instance v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;-><init>()V

    .line 936095
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 936096
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936097
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;->k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel$ThreadQueueParticipantsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 936098
    const v0, 0x6b725207

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 936099
    const v0, -0x20950cd6

    return v0
.end method
