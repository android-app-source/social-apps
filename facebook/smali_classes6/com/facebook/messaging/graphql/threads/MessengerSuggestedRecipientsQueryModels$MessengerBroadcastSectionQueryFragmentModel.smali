.class public final Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x48ef504e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 930799
    const-class v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 930800
    const-class v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 930805
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 930806
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttachments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 930754
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->e:Ljava/util/List;

    .line 930755
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930801
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    .line 930802
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSeeMoreButton"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930803
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930804
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930795
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930796
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930797
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->i:Ljava/lang/String;

    .line 930798
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 930781
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 930782
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 930783
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 930784
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x56f5e1c2

    invoke-static {v3, v2, v4}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 930785
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x4c427b33    # 5.0982092E7f

    invoke-static {v4, v3, v5}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 930786
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 930787
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 930788
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 930789
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 930790
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 930791
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 930792
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 930793
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 930794
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 930760
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 930761
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 930762
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 930763
    if-eqz v1, :cond_3

    .line 930764
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;

    .line 930765
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 930766
    :goto_0
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 930767
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x56f5e1c2

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 930768
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 930769
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;

    .line 930770
    iput v3, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->g:I

    move-object v1, v0

    .line 930771
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 930772
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4c427b33    # 5.0982092E7f

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 930773
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 930774
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;

    .line 930775
    iput v3, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->h:I

    move-object v1, v0

    .line 930776
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 930777
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    .line 930778
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 930779
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 930780
    goto :goto_1

    :cond_3
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 930756
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 930757
    const/4 v0, 0x2

    const v1, 0x56f5e1c2

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->g:I

    .line 930758
    const/4 v0, 0x3

    const v1, 0x4c427b33    # 5.0982092E7f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;->h:I

    .line 930759
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 930751
    new-instance v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel;-><init>()V

    .line 930752
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 930753
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 930750
    const v0, 0x27870fd1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 930749
    const v0, -0x30a52409

    return v0
.end method
