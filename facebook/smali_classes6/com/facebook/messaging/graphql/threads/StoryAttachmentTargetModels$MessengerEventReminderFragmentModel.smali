.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/5We;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x338d4f49
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I

.field private k:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 935665
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 935664
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 935662
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 935663
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 935660
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->e:Ljava/lang/String;

    .line 935661
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 935618
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;

    .line 935619
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 935658
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->g:Ljava/lang/String;

    .line 935659
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 935656
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->h:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->h:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    .line 935657
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->h:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLLightweightEventType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 935654
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->i:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->i:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 935655
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->i:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 935638
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 935639
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 935640
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 935641
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 935642
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->m()Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 935643
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 935644
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 935645
    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 935646
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 935647
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 935648
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 935649
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 935650
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->j:I

    invoke-virtual {p1, v0, v1, v6}, LX/186;->a(III)V

    .line 935651
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->k:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 935652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 935653
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 935630
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 935631
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 935632
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;

    .line 935633
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 935634
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;

    .line 935635
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;

    .line 935636
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 935637
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 935629
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 935625
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 935626
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->j:I

    .line 935627
    const/4 v0, 0x6

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;->k:J

    .line 935628
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 935622
    new-instance v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;-><init>()V

    .line 935623
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 935624
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 935621
    const v0, 0x834a5dd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 935620
    const v0, 0x7f4f54f9

    return v0
.end method
