.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 935616
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 935617
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 935615
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;LX/0nX;LX/0my;)V
    .locals 10

    .prologue
    .line 935582
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 935583
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v8, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 935584
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 935585
    invoke-virtual {v1, v0, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 935586
    if-eqz v2, :cond_0

    .line 935587
    const-string v3, "event_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935588
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 935589
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 935590
    if-eqz v2, :cond_1

    .line 935591
    const-string v3, "firstThreeMembers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935592
    invoke-static {v1, v2, p1, p2}, LX/5YP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 935593
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 935594
    if-eqz v2, :cond_2

    .line 935595
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935596
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 935597
    :cond_2
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 935598
    if-eqz v2, :cond_3

    .line 935599
    const-string v2, "lightweight_event_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935600
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 935601
    :cond_3
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v2

    .line 935602
    if-eqz v2, :cond_4

    .line 935603
    const-string v2, "lightweight_event_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935604
    invoke-virtual {v1, v0, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 935605
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 935606
    if-eqz v2, :cond_5

    .line 935607
    const-string v3, "seconds_to_notify_before"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935608
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 935609
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, v8, v9}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 935610
    cmp-long v4, v2, v8

    if-eqz v4, :cond_6

    .line 935611
    const-string v4, "time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935612
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 935613
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 935614
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 935581
    check-cast p1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
