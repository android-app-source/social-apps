.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 946244
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 946245
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 946236
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 946237
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 946246
    if-nez p1, :cond_0

    move v0, v1

    .line 946247
    :goto_0
    return v0

    .line 946248
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 946249
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 946250
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946251
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946252
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946253
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946254
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 946255
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946256
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946257
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946258
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946259
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 946260
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946261
    const v2, 0x49b7fc49

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 946262
    invoke-virtual {p0, p1, v7}, LX/15i;->p(II)I

    move-result v2

    .line 946263
    const v3, 0x3d3f1b8

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 946264
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 946265
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946266
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 946267
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 946268
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946269
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946270
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946271
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946272
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 946273
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946274
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946275
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946276
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946277
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 946278
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLTimespanCategory;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimespanCategory;

    move-result-object v0

    .line 946279
    invoke-virtual {p3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 946280
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946281
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946282
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946283
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946284
    const v2, 0x4daa6308    # 3.57327104E8f

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 946285
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946286
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946287
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946288
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946289
    const v2, 0x5e4fec79

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 946290
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946291
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946292
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946293
    :sswitch_8
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946294
    const v2, 0x4be6cdd3    # 3.0251942E7f

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 946295
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946296
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946297
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946298
    :sswitch_9
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessThreadQueryModel$MessageThreadsModel$NodesModel$OtherParticipantsModel$OtherParticipantsNodesModel$MessagingActorModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessThreadQueryModel$MessageThreadsModel$NodesModel$OtherParticipantsModel$OtherParticipantsNodesModel$MessagingActorModel;

    .line 946299
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 946300
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946301
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946302
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946303
    :sswitch_a
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946304
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946305
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 946306
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 946307
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v3

    .line 946308
    const v4, -0x638c7e05

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 946309
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 946310
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 946311
    invoke-virtual {p3, v10}, LX/186;->c(I)V

    .line 946312
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946313
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 946314
    invoke-virtual {p3, v8, v3}, LX/186;->b(II)V

    .line 946315
    invoke-virtual {p3, v9, v4}, LX/186;->b(II)V

    .line 946316
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946317
    :sswitch_b
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946318
    const v2, -0x725c1b30

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 946319
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946320
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946321
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946322
    :sswitch_c
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946323
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946324
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946325
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946326
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946327
    :sswitch_d
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946328
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946329
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946330
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946331
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946332
    :sswitch_e
    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 946333
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 946334
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 946335
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 946336
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 946337
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946338
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 946339
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946340
    :sswitch_f
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946341
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946342
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946343
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946344
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946345
    :sswitch_10
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946346
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946347
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946348
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946349
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946350
    :sswitch_11
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946351
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946352
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946353
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946354
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946355
    :sswitch_12
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946356
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946357
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 946358
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 946359
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 946360
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946361
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 946362
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946363
    :sswitch_13
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946364
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946365
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 946366
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 946367
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 946368
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946369
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 946370
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946371
    :sswitch_14
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946372
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946373
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946374
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946375
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946376
    :sswitch_15
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946377
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946378
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946379
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946380
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946381
    :sswitch_16
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946382
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 946383
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageReactionsFieldModel$MessageReactionsModel$UserModel;

    invoke-virtual {p0, p1, v7, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageReactionsFieldModel$MessageReactionsModel$UserModel;

    .line 946384
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 946385
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 946386
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 946387
    invoke-virtual {p3, v7, v0}, LX/186;->b(II)V

    .line 946388
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946389
    :sswitch_17
    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v0

    .line 946390
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946391
    invoke-virtual {p3, v1, v0}, LX/186;->a(IZ)V

    .line 946392
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946393
    :sswitch_18
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946394
    const v2, 0x1f5d54e8

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 946395
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946396
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946397
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946398
    :sswitch_19
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946399
    const v2, -0x58e53f56

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 946400
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946401
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946402
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946403
    :sswitch_1a
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946404
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946405
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 946406
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 946407
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 946408
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 946409
    invoke-virtual {p0, p1, v9}, LX/15i;->p(II)I

    move-result v4

    .line 946410
    const v5, 0x2e06d92b

    invoke-static {p0, v4, v5, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v4

    .line 946411
    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 946412
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 946413
    const/4 v6, 0x5

    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 946414
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946415
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 946416
    invoke-virtual {p3, v8, v3}, LX/186;->b(II)V

    .line 946417
    invoke-virtual {p3, v9, v4}, LX/186;->b(II)V

    .line 946418
    invoke-virtual {p3, v10, v5}, LX/186;->b(II)V

    .line 946419
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946420
    :sswitch_1b
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946421
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946422
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 946423
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 946424
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 946425
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946426
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 946427
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946428
    :sswitch_1c
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946429
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946430
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 946431
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946432
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946433
    :sswitch_1d
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946434
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946435
    invoke-virtual {p0, p1, v7}, LX/15i;->p(II)I

    move-result v2

    .line 946436
    const v3, -0x32550f72

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 946437
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 946438
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946439
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 946440
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 946441
    :sswitch_1e
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 946442
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 946443
    invoke-virtual {p0, p1, v7, v1}, LX/15i;->a(III)I

    move-result v2

    .line 946444
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 946445
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 946446
    invoke-virtual {p3, v7, v2, v1}, LX/186;->a(III)V

    .line 946447
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x79150f85 -> :sswitch_f
        -0x725c1b30 -> :sswitch_c
        -0x65019081 -> :sswitch_6
        -0x638c7e05 -> :sswitch_b
        -0x600f001f -> :sswitch_a
        -0x5f1a9bf8 -> :sswitch_1a
        -0x5e362361 -> :sswitch_12
        -0x5736eee6 -> :sswitch_15
        -0x5212889e -> :sswitch_14
        -0x45b151e2 -> :sswitch_19
        -0x4516f065 -> :sswitch_5
        -0x3d319c14 -> :sswitch_11
        -0x35661b19 -> :sswitch_2
        -0x32550f72 -> :sswitch_1e
        -0x138e708a -> :sswitch_16
        0x3d3f1b8 -> :sswitch_4
        0x4cb4f08 -> :sswitch_d
        0x6a780cf -> :sswitch_e
        0x9f645e5 -> :sswitch_0
        0x11a99cc7 -> :sswitch_1c
        0x202033f5 -> :sswitch_1
        0x20d0d098 -> :sswitch_17
        0x2e06d92b -> :sswitch_1b
        0x336d9ff8 -> :sswitch_18
        0x340d0ac2 -> :sswitch_13
        0x49b7fc49 -> :sswitch_3
        0x4be6cdd3 -> :sswitch_9
        0x4daa6308 -> :sswitch_7
        0x5e4fec79 -> :sswitch_8
        0x6de44fb8 -> :sswitch_10
        0x78deb308 -> :sswitch_1d
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 946448
    if-nez p0, :cond_0

    move v0, v1

    .line 946449
    :goto_0
    return v0

    .line 946450
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 946451
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 946452
    :goto_1
    if-ge v1, v2, :cond_2

    .line 946453
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 946454
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 946455
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 946456
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 946457
    const/4 v7, 0x0

    .line 946458
    const/4 v1, 0x0

    .line 946459
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 946460
    invoke-static {v2, v3, v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 946461
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 946462
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 946463
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 946464
    new-instance v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 946465
    if-eqz p0, :cond_0

    .line 946466
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 946467
    if-eq v0, p0, :cond_0

    .line 946468
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 946469
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 946470
    sparse-switch p2, :sswitch_data_0

    .line 946471
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 946472
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946473
    const v1, 0x49b7fc49

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 946474
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 946475
    const v1, 0x3d3f1b8

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 946476
    :goto_0
    :sswitch_1
    return-void

    .line 946477
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946478
    const v1, 0x4daa6308    # 3.57327104E8f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 946479
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946480
    const v1, 0x5e4fec79

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 946481
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946482
    const v1, 0x4be6cdd3    # 3.0251942E7f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 946483
    :sswitch_5
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessThreadQueryModel$MessageThreadsModel$NodesModel$OtherParticipantsModel$OtherParticipantsNodesModel$MessagingActorModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessThreadQueryModel$MessageThreadsModel$NodesModel$OtherParticipantsModel$OtherParticipantsNodesModel$MessagingActorModel;

    .line 946484
    invoke-static {v0, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 946485
    :sswitch_6
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 946486
    const v1, -0x638c7e05

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 946487
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946488
    const v1, -0x725c1b30

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 946489
    :sswitch_8
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageReactionsFieldModel$MessageReactionsModel$UserModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageReactionsFieldModel$MessageReactionsModel$UserModel;

    .line 946490
    invoke-static {v0, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 946491
    :sswitch_9
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946492
    const v1, 0x1f5d54e8

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 946493
    :sswitch_a
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 946494
    const v1, -0x58e53f56

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 946495
    :sswitch_b
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 946496
    const v1, 0x2e06d92b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 946497
    :sswitch_c
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 946498
    const v1, -0x32550f72

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x79150f85 -> :sswitch_1
        -0x725c1b30 -> :sswitch_1
        -0x65019081 -> :sswitch_2
        -0x638c7e05 -> :sswitch_7
        -0x600f001f -> :sswitch_6
        -0x5f1a9bf8 -> :sswitch_b
        -0x5e362361 -> :sswitch_1
        -0x5736eee6 -> :sswitch_1
        -0x5212889e -> :sswitch_1
        -0x45b151e2 -> :sswitch_a
        -0x4516f065 -> :sswitch_1
        -0x3d319c14 -> :sswitch_1
        -0x35661b19 -> :sswitch_0
        -0x32550f72 -> :sswitch_1
        -0x138e708a -> :sswitch_8
        0x3d3f1b8 -> :sswitch_1
        0x4cb4f08 -> :sswitch_1
        0x6a780cf -> :sswitch_1
        0x9f645e5 -> :sswitch_1
        0x11a99cc7 -> :sswitch_1
        0x202033f5 -> :sswitch_1
        0x20d0d098 -> :sswitch_1
        0x2e06d92b -> :sswitch_1
        0x336d9ff8 -> :sswitch_9
        0x340d0ac2 -> :sswitch_1
        0x49b7fc49 -> :sswitch_1
        0x4be6cdd3 -> :sswitch_5
        0x4daa6308 -> :sswitch_3
        0x5e4fec79 -> :sswitch_4
        0x6de44fb8 -> :sswitch_1
        0x78deb308 -> :sswitch_c
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 946499
    if-nez p1, :cond_0

    move v0, v1

    .line 946500
    :goto_0
    return v0

    .line 946501
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 946502
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 946503
    :goto_1
    if-ge v1, v2, :cond_2

    .line 946504
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 946505
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 946506
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 946507
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 946508
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 946509
    if-eqz p1, :cond_0

    .line 946510
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 946511
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 946512
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 946513
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 946514
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 946515
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 946238
    if-eqz p1, :cond_0

    .line 946239
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    move-result-object v1

    .line 946240
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    .line 946241
    if-eq v0, v1, :cond_0

    .line 946242
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 946243
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 946202
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 946206
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 946207
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 946208
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 946209
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 946210
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a:LX/15i;

    .line 946211
    iput p2, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->b:I

    .line 946212
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 946213
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 946214
    new-instance v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 946215
    iget v0, p0, LX/1vt;->c:I

    .line 946216
    move v0, v0

    .line 946217
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 946218
    iget v0, p0, LX/1vt;->c:I

    .line 946219
    move v0, v0

    .line 946220
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 946221
    iget v0, p0, LX/1vt;->b:I

    .line 946222
    move v0, v0

    .line 946223
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 946224
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 946225
    move-object v0, v0

    .line 946226
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 946227
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 946228
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 946229
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 946230
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 946231
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 946232
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 946233
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 946234
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 946235
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 946203
    iget v0, p0, LX/1vt;->c:I

    .line 946204
    move v0, v0

    .line 946205
    return v0
.end method
