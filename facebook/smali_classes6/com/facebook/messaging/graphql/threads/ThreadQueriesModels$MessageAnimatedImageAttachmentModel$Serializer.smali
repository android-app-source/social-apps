.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 948505
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 948506
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 948504
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 948458
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 948459
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 948460
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 948461
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 948462
    if-eqz v2, :cond_0

    .line 948463
    const-string p0, "animated_image_full_screen"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 948464
    invoke-static {v1, v2, p1}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 948465
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 948466
    if-eqz v2, :cond_1

    .line 948467
    const-string p0, "animated_image_large_preview"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 948468
    invoke-static {v1, v2, p1}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 948469
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 948470
    if-eqz v2, :cond_2

    .line 948471
    const-string p0, "animated_image_medium_preview"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 948472
    invoke-static {v1, v2, p1}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 948473
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 948474
    if-eqz v2, :cond_3

    .line 948475
    const-string p0, "animated_image_original_dimensions"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 948476
    invoke-static {v1, v2, p1}, LX/5ac;->a(LX/15i;ILX/0nX;)V

    .line 948477
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 948478
    if-eqz v2, :cond_4

    .line 948479
    const-string p0, "animated_image_render_as_sticker"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 948480
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 948481
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 948482
    if-eqz v2, :cond_5

    .line 948483
    const-string p0, "animated_image_small_preview"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 948484
    invoke-static {v1, v2, p1}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 948485
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 948486
    if-eqz v2, :cond_6

    .line 948487
    const-string p0, "animated_static_image_full_screen"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 948488
    invoke-static {v1, v2, p1}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 948489
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 948490
    if-eqz v2, :cond_7

    .line 948491
    const-string p0, "animated_static_image_large_preview"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 948492
    invoke-static {v1, v2, p1}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 948493
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 948494
    if-eqz v2, :cond_8

    .line 948495
    const-string p0, "animated_static_image_medium_preview"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 948496
    invoke-static {v1, v2, p1}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 948497
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 948498
    if-eqz v2, :cond_9

    .line 948499
    const-string p0, "animated_static_image_small_preview"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 948500
    invoke-static {v1, v2, p1}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 948501
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 948502
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 948503
    check-cast p1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
