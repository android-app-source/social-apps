.class public final Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 924896
    const-class v0, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 924897
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 924895
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 924899
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 924900
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 924901
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 924902
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 924903
    if-eqz p0, :cond_0

    .line 924904
    const-string p2, "client_mutation_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 924905
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 924906
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 924907
    if-eqz p0, :cond_1

    .line 924908
    const-string p2, "client_subscription_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 924909
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 924910
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 924911
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 924898
    check-cast p1, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
