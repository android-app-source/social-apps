.class public final Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x58ef5b78
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 959527
    const-class v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 959526
    const-class v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 959524
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 959525
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 959504
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 959505
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;->a()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 959506
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 959507
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 959508
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 959509
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 959521
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 959522
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 959523
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 959520
    new-instance v0, LX/5bf;

    invoke-direct {v0, p1}, LX/5bf;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959518
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;->e:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;->e:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 959519
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;->e:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 959516
    invoke-virtual {p2}, LX/18L;->a()V

    .line 959517
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 959515
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 959512
    new-instance v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;-><init>()V

    .line 959513
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 959514
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 959511
    const v0, 0x77ca4223

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 959510
    const v0, -0x64104400

    return v0
.end method
