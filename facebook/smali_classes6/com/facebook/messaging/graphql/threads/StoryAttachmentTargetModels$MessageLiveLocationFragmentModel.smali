.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/5Wc;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2b4fde23
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:J

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$SenderModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 935095
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 935094
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 935045
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 935046
    return-void
.end method

.method private j()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 935092
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    .line 935093
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 935090
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->h:Ljava/lang/String;

    .line 935091
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 935088
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->i:Ljava/lang/String;

    .line 935089
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 935086
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->j:Ljava/lang/String;

    .line 935087
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$SenderModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 935096
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$SenderModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$SenderModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$SenderModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$SenderModel;

    .line 935097
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$SenderModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 935069
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 935070
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->j()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 935071
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 935072
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 935073
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 935074
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$SenderModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 935075
    const/16 v1, 0x8

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 935076
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->e:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 935077
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 935078
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->g:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 935079
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 935080
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 935081
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 935082
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 935083
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 935084
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 935085
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 935056
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 935057
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->j()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 935058
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->j()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    .line 935059
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->j()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 935060
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;

    .line 935061
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    .line 935062
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$SenderModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 935063
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$SenderModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$SenderModel;

    .line 935064
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$SenderModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 935065
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;

    .line 935066
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$SenderModel;

    .line 935067
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 935068
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 935055
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 935050
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 935051
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->e:Z

    .line 935052
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->g:J

    .line 935053
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;->l:Z

    .line 935054
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 935047
    new-instance v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;-><init>()V

    .line 935048
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 935049
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 935044
    const v0, 0x62b335c3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 935043
    const v0, -0x16bdadb8

    return v0
.end method
