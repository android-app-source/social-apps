.class public final Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xaee9268
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 924912
    const-class v0, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 924927
    const-class v0, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 924925
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 924926
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 924923
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;->e:Ljava/lang/String;

    .line 924924
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 924921
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;->f:Ljava/lang/String;

    .line 924922
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 924928
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 924929
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 924930
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 924931
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 924932
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 924933
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 924934
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 924935
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 924918
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 924919
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 924920
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 924915
    new-instance v0, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/CommerceAgentMutationsModels$AgentThreadStartMutationModel;-><init>()V

    .line 924916
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 924917
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 924914
    const v0, 0x24926b2b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 924913
    const v0, -0x5a0e0ec6

    return v0
.end method
