.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 936555
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 936556
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 936557
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 936558
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 936559
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 936560
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 936561
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 936562
    if-eqz v2, :cond_0

    .line 936563
    const-string p0, "account_number"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936564
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 936565
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 936566
    if-eqz v2, :cond_1

    .line 936567
    const-string p0, "agent_fee"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936568
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 936569
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 936570
    if-eqz v2, :cond_2

    .line 936571
    const-string p0, "agent_name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936572
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 936573
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 936574
    if-eqz v2, :cond_3

    .line 936575
    const-string p0, "amount_due"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936576
    invoke-static {v1, v2, p1}, LX/5YZ;->a(LX/15i;ILX/0nX;)V

    .line 936577
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 936578
    if-eqz v2, :cond_4

    .line 936579
    const-string p0, "biller_name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936580
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 936581
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 936582
    if-eqz v2, :cond_5

    .line 936583
    const-string p0, "convenience_fee"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936584
    invoke-static {v1, v2, p1}, LX/5Ya;->a(LX/15i;ILX/0nX;)V

    .line 936585
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 936586
    if-eqz v2, :cond_6

    .line 936587
    const-string p0, "due_date"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936588
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 936589
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 936590
    if-eqz v2, :cond_7

    .line 936591
    const-string p0, "pay_by_date"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936592
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 936593
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 936594
    if-eqz v2, :cond_8

    .line 936595
    const-string p0, "total_due"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936596
    invoke-static {v1, v2, p1}, LX/5Yb;->a(LX/15i;ILX/0nX;)V

    .line 936597
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 936598
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 936599
    check-cast p1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;LX/0nX;LX/0my;)V

    return-void
.end method
