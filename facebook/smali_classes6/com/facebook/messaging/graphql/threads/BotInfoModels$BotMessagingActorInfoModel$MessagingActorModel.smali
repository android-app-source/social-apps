.class public final Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1066f513
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 923770
    const-class v0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 923771
    const-class v0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 923772
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 923773
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 923792
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 923793
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 923794
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 923795
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 923796
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 923797
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->n()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 923798
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->o()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 923799
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 923800
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 923801
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 923802
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 923803
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 923804
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 923805
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 923806
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 923807
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 923774
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 923775
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 923776
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 923777
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 923778
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    .line 923779
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->h:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 923780
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->n()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 923781
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->n()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 923782
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->n()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 923783
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    .line 923784
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->i:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 923785
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->o()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 923786
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->o()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 923787
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->o()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 923788
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    .line 923789
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->j:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 923790
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 923791
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 923808
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 923766
    new-instance v0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;-><init>()V

    .line 923767
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 923768
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 923769
    const v0, -0x6609d710

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 923765
    const v0, -0x5de3ee8f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 923762
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 923763
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 923764
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 923760
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->f:Ljava/lang/String;

    .line 923761
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 923758
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->g:Ljava/lang/String;

    .line 923759
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 923756
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->h:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->h:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 923757
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->h:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 923754
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->i:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->i:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 923755
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->i:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    return-object v0
.end method

.method public final o()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 923752
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->j:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->j:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 923753
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->j:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    return-object v0
.end method
