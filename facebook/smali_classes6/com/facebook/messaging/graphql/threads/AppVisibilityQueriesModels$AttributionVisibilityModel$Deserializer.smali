.class public final Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 923476
    const-class v0, Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 923477
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 923478
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 923479
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 923480
    const/4 v2, 0x0

    .line 923481
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 923482
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 923483
    :goto_0
    move v1, v2

    .line 923484
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 923485
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 923486
    new-instance v1, Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel;-><init>()V

    .line 923487
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 923488
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 923489
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 923490
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 923491
    :cond_0
    return-object v1

    .line 923492
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 923493
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 923494
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 923495
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 923496
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 923497
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 923498
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 923499
    :cond_4
    const-string v5, "messenger_app_attribution_visibility"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 923500
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 923501
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_f

    .line 923502
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 923503
    :goto_2
    move v1, v4

    .line 923504
    goto :goto_1

    .line 923505
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 923506
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 923507
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 923508
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 923509
    :cond_7
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_b

    .line 923510
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 923511
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 923512
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_7

    if-eqz v11, :cond_7

    .line 923513
    const-string p0, "hide_attribution"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 923514
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v7

    move v10, v7

    move v7, v5

    goto :goto_3

    .line 923515
    :cond_8
    const-string p0, "hide_install_button"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 923516
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v6

    move v9, v6

    move v6, v5

    goto :goto_3

    .line 923517
    :cond_9
    const-string p0, "hide_reply_button"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 923518
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v8, v1

    move v1, v5

    goto :goto_3

    .line 923519
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 923520
    :cond_b
    const/4 v11, 0x3

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 923521
    if-eqz v7, :cond_c

    .line 923522
    invoke-virtual {v0, v4, v10}, LX/186;->a(IZ)V

    .line 923523
    :cond_c
    if-eqz v6, :cond_d

    .line 923524
    invoke-virtual {v0, v5, v9}, LX/186;->a(IZ)V

    .line 923525
    :cond_d
    if-eqz v1, :cond_e

    .line 923526
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v8}, LX/186;->a(IZ)V

    .line 923527
    :cond_e
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_f
    move v1, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    move v10, v4

    goto :goto_3
.end method
