.class public final Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4d40a9de
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 932483
    const-class v0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 932482
    const-class v0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 932480
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 932481
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 932477
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 932478
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 932479
    return-void
.end method

.method public static a(Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;)Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;
    .locals 10

    .prologue
    .line 932447
    if-nez p0, :cond_0

    .line 932448
    const/4 p0, 0x0

    .line 932449
    :goto_0
    return-object p0

    .line 932450
    :cond_0
    instance-of v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;

    if-eqz v0, :cond_1

    .line 932451
    check-cast p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;

    goto :goto_0

    .line 932452
    :cond_1
    new-instance v0, LX/5WB;

    invoke-direct {v0}, LX/5WB;-><init>()V

    .line 932453
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5WB;->a:Ljava/lang/String;

    .line 932454
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->b()Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    move-result-object v1

    iput-object v1, v0, LX/5WB;->b:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    .line 932455
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/5WB;->c:Z

    .line 932456
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->d()Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;->a(Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;)Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    move-result-object v1

    iput-object v1, v0, LX/5WB;->d:Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    .line 932457
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->e()Z

    move-result v1

    iput-boolean v1, v0, LX/5WB;->e:Z

    .line 932458
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 932459
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 932460
    iget-object v3, v0, LX/5WB;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 932461
    iget-object v5, v0, LX/5WB;->b:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    invoke-virtual {v2, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 932462
    iget-object v7, v0, LX/5WB;->d:Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 932463
    const/4 v8, 0x5

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 932464
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 932465
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 932466
    const/4 v3, 0x2

    iget-boolean v5, v0, LX/5WB;->c:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 932467
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 932468
    const/4 v3, 0x4

    iget-boolean v5, v0, LX/5WB;->e:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 932469
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 932470
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 932471
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 932472
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 932473
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 932474
    new-instance v3, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;

    invoke-direct {v3, v2}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;-><init>(LX/15i;)V

    .line 932475
    move-object p0, v3

    .line 932476
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 932445
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->h:Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->h:Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    .line 932446
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->h:Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 932407
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 932408
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 932409
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->b()Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 932410
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->j()Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 932411
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 932412
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 932413
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 932414
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 932415
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 932416
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 932417
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 932418
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 932437
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 932438
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->j()Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 932439
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->j()Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    .line 932440
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->j()Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 932441
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;

    .line 932442
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->h:Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    .line 932443
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 932444
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 932435
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->e:Ljava/lang/String;

    .line 932436
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 932431
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 932432
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->g:Z

    .line 932433
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->i:Z

    .line 932434
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 932429
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->f:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->f:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    .line 932430
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->f:Lcom/facebook/graphql/enums/GraphQLPaymentPlatformAttachmentCallToActionType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 932426
    new-instance v0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;-><init>()V

    .line 932427
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 932428
    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 932424
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 932425
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->g:Z

    return v0
.end method

.method public final synthetic d()Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 932423
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->j()Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel$LabelModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 932422
    const v0, -0xb638e4b

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 932420
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 932421
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentCallToActionsModel;->i:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 932419
    const v0, -0x22237255

    return v0
.end method
