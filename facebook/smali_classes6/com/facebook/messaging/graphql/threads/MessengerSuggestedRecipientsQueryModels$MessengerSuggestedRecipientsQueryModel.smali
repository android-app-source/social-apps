.class public final Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1fe2f847
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$MessengerSuggestedRecipientsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 930891
    const-class v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 930892
    const-class v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 930893
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 930894
    return-void
.end method

.method private a()Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$MessengerSuggestedRecipientsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessengerSuggestedRecipients"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930895
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel;->e:Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$MessengerSuggestedRecipientsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$MessengerSuggestedRecipientsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$MessengerSuggestedRecipientsModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel;->e:Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$MessengerSuggestedRecipientsModel;

    .line 930896
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel;->e:Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$MessengerSuggestedRecipientsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 930897
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 930898
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$MessengerSuggestedRecipientsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 930899
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 930900
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 930901
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 930902
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 930903
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 930904
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$MessengerSuggestedRecipientsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 930905
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$MessengerSuggestedRecipientsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$MessengerSuggestedRecipientsModel;

    .line 930906
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$MessengerSuggestedRecipientsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 930907
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel;

    .line 930908
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel;->e:Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel$MessengerSuggestedRecipientsModel;

    .line 930909
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 930910
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 930911
    new-instance v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerSuggestedRecipientsQueryModel;-><init>()V

    .line 930912
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 930913
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 930914
    const v0, -0x23f96ac4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 930915
    const v0, -0x6747e1ce

    return v0
.end method
