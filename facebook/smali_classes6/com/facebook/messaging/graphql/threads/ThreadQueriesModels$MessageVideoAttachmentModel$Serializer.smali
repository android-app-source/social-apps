.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 949524
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 949525
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 949485
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 949487
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 949488
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x7

    const/4 v4, 0x0

    .line 949489
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 949490
    invoke-virtual {v1, v0, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 949491
    if-eqz v2, :cond_0

    .line 949492
    const-string v3, "attachment_video_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 949493
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 949494
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 949495
    if-eqz v2, :cond_1

    .line 949496
    const-string v3, "filename"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 949497
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 949498
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 949499
    if-eqz v2, :cond_2

    .line 949500
    const-string v3, "original_dimensions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 949501
    invoke-static {v1, v2, p1}, LX/5an;->a(LX/15i;ILX/0nX;)V

    .line 949502
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 949503
    if-eqz v2, :cond_3

    .line 949504
    const-string v3, "playable_duration_in_ms"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 949505
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 949506
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 949507
    if-eqz v2, :cond_4

    .line 949508
    const-string v3, "rotation"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 949509
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 949510
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 949511
    if-eqz v2, :cond_5

    .line 949512
    const-string v3, "streamingImageThumbnail"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 949513
    invoke-static {v1, v2, p1}, LX/5ao;->a(LX/15i;ILX/0nX;)V

    .line 949514
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 949515
    if-eqz v2, :cond_6

    .line 949516
    const-string v3, "video_filesize"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 949517
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 949518
    :cond_6
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 949519
    if-eqz v2, :cond_7

    .line 949520
    const-string v2, "video_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 949521
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 949522
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 949523
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 949486
    check-cast p1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
