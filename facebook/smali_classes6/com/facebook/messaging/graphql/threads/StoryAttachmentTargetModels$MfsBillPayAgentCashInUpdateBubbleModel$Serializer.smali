.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 936302
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 936303
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 936305
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 936306
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 936307
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 936308
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 936309
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 936310
    if-eqz v2, :cond_0

    .line 936311
    const-string v3, "agent_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936312
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 936313
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 936314
    if-eqz v2, :cond_1

    .line 936315
    const-string v3, "agent_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936316
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 936317
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 936318
    if-eqz v2, :cond_2

    .line 936319
    const-string v3, "bill_account_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936320
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 936321
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 936322
    if-eqz v2, :cond_3

    .line 936323
    const-string v3, "bill_amount"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936324
    invoke-static {v1, v2, p1}, LX/5YW;->a(LX/15i;ILX/0nX;)V

    .line 936325
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 936326
    if-eqz v2, :cond_4

    .line 936327
    const-string v3, "biller_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936328
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 936329
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 936330
    cmp-long v4, v2, v4

    if-eqz v4, :cond_5

    .line 936331
    const-string v4, "completion_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936332
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 936333
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 936334
    if-eqz v2, :cond_6

    .line 936335
    const-string v3, "convenience_fee"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936336
    invoke-static {v1, v2, p1}, LX/5YX;->a(LX/15i;ILX/0nX;)V

    .line 936337
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 936338
    if-eqz v2, :cond_7

    .line 936339
    const-string v3, "displayed_payment_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 936340
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 936341
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 936342
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 936304
    check-cast p1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;LX/0nX;LX/0my;)V

    return-void
.end method
