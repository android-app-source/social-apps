.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 933731
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 933732
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 933733
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 933734
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 933735
    const/4 v2, 0x0

    .line 933736
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_a

    .line 933737
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 933738
    :goto_0
    move v1, v2

    .line 933739
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 933740
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 933741
    new-instance v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel;-><init>()V

    .line 933742
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 933743
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 933744
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 933745
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 933746
    :cond_0
    return-object v1

    .line 933747
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 933748
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_9

    .line 933749
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 933750
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 933751
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v9, :cond_2

    .line 933752
    const-string p0, "cover_photo"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 933753
    invoke-static {p1, v0}, LX/5Y6;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 933754
    :cond_3
    const-string p0, "group_friend_members"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 933755
    invoke-static {p1, v0}, LX/5Y7;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 933756
    :cond_4
    const-string p0, "group_members"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 933757
    invoke-static {p1, v0}, LX/5Y8;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 933758
    :cond_5
    const-string p0, "id"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 933759
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 933760
    :cond_6
    const-string p0, "viewer_invite_to_group"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 933761
    invoke-static {p1, v0}, LX/5YA;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 933762
    :cond_7
    const-string p0, "viewer_join_state"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 933763
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 933764
    :cond_8
    const-string p0, "visibility"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 933765
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    goto/16 :goto_1

    .line 933766
    :cond_9
    const/4 v9, 0x7

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 933767
    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 933768
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 933769
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 933770
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 933771
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 933772
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 933773
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 933774
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_1
.end method
