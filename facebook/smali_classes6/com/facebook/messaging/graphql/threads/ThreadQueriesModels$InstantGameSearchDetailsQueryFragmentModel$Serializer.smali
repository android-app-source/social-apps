.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 948239
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 948240
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 948241
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 948242
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 948243
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 948244
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 948245
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 948246
    if-eqz v2, :cond_0

    .line 948247
    const-string p0, "instant_game_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 948248
    invoke-static {v1, v2, p1, p2}, LX/5aa;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 948249
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 948250
    if-eqz v2, :cond_1

    .line 948251
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 948252
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 948253
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 948254
    if-eqz v2, :cond_2

    .line 948255
    const-string p0, "square_logo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 948256
    invoke-static {v1, v2, p1}, LX/5ab;->a(LX/15i;ILX/0nX;)V

    .line 948257
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 948258
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 948259
    check-cast p1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
