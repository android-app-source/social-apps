.class public final Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/5bb;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6b7e9ee5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 959204
    const-class v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 959203
    const-class v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 959201
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 959202
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 959193
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 959194
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;->b()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 959195
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 959196
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 959197
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 959198
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 959199
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 959200
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 959176
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 959177
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;->b()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 959178
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;->b()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 959179
    if-eqz v1, :cond_0

    .line 959180
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;

    .line 959181
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;->e:LX/3Sb;

    .line 959182
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 959183
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959191
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;->f:Ljava/lang/String;

    .line 959192
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final b()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getParts"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 959189
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, 0x16e9cb3

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;->e:LX/3Sb;

    .line 959190
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 959186
    new-instance v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;-><init>()V

    .line 959187
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 959188
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 959185
    const v0, -0x30522187

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 959184
    const v0, 0x24eeab

    return v0
.end method
