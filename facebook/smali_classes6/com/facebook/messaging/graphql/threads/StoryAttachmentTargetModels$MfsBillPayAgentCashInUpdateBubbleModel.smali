.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/5Wi;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x783e2b45
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:J

.field private k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$ConvenienceFeeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 936400
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 936399
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 936397
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 936398
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936395
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->e:Ljava/lang/String;

    .line 936396
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936393
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->f:Ljava/lang/String;

    .line 936394
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936391
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->g:Ljava/lang/String;

    .line 936392
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936389
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;

    .line 936390
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936387
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->i:Ljava/lang/String;

    .line 936388
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$ConvenienceFeeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936385
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$ConvenienceFeeModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$ConvenienceFeeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$ConvenienceFeeModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$ConvenienceFeeModel;

    .line 936386
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$ConvenienceFeeModel;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936383
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->l:Ljava/lang/String;

    .line 936384
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->l:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 936343
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 936344
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 936345
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 936346
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 936347
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->l()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 936348
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 936349
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$ConvenienceFeeModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 936350
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 936351
    const/16 v5, 0x8

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 936352
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 936353
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 936354
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 936355
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 936356
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 936357
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->j:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 936358
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 936359
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 936360
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 936361
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 936370
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 936371
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->l()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 936372
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->l()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;

    .line 936373
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->l()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 936374
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;

    .line 936375
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;

    .line 936376
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$ConvenienceFeeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 936377
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$ConvenienceFeeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$ConvenienceFeeModel;

    .line 936378
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$ConvenienceFeeModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 936379
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;

    .line 936380
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$ConvenienceFeeModel;

    .line 936381
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 936382
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 936367
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 936368
    const/4 v0, 0x5

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;->j:J

    .line 936369
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 936364
    new-instance v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel;-><init>()V

    .line 936365
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 936366
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 936363
    const v0, -0x6a2f091

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 936362
    const v0, -0x1de9d85f

    return v0
.end method
