.class public final Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x79236560
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 959073
    const-class v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 959072
    const-class v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 959070
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 959071
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 959052
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 959053
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;->j()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 959054
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 959055
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 959056
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 959057
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 959062
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 959063
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;->j()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 959064
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;->j()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959065
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;->j()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 959066
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;

    .line 959067
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;->e:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959068
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 959069
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/5Vu;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessagingActor"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959061
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;->j()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 959058
    new-instance v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;-><init>()V

    .line 959059
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 959060
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 959051
    const v0, 0x12abb0d8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 959050
    const v0, -0x3cd03651

    return v0
.end method

.method public final j()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessagingActor"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959048
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;->e:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;->e:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959049
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;->e:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    return-object v0
.end method
