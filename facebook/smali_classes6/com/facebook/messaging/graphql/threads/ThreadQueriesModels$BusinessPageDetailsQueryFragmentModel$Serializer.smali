.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageDetailsQueryFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageDetailsQueryFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 945295
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageDetailsQueryFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageDetailsQueryFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageDetailsQueryFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 945296
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 945240
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageDetailsQueryFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 945242
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 945243
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x2

    const/4 v3, 0x0

    .line 945244
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 945245
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 945246
    if-eqz v2, :cond_0

    .line 945247
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945248
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 945249
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 945250
    if-eqz v2, :cond_1

    .line 945251
    const-string v3, "best_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945252
    invoke-static {v1, v2, p1}, LX/5a0;->a(LX/15i;ILX/0nX;)V

    .line 945253
    :cond_1
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 945254
    if-eqz v2, :cond_2

    .line 945255
    const-string v2, "commerce_page_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945256
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 945257
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 945258
    if-eqz v2, :cond_3

    .line 945259
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945260
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 945261
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 945262
    if-eqz v2, :cond_4

    .line 945263
    const-string v3, "is_messenger_user"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945264
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 945265
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 945266
    if-eqz v2, :cond_5

    .line 945267
    const-string v3, "messenger_welcome_page_context_banner"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945268
    invoke-static {v1, v2, p1, p2}, LX/5a1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 945269
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 945270
    if-eqz v2, :cond_6

    .line 945271
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945272
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 945273
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 945274
    if-eqz v2, :cond_7

    .line 945275
    const-string v3, "profile_pic_large"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945276
    invoke-static {v1, v2, p1}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 945277
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 945278
    if-eqz v2, :cond_8

    .line 945279
    const-string v3, "profile_pic_medium"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945280
    invoke-static {v1, v2, p1}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 945281
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 945282
    if-eqz v2, :cond_9

    .line 945283
    const-string v3, "profile_pic_small"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945284
    invoke-static {v1, v2, p1}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 945285
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 945286
    if-eqz v2, :cond_a

    .line 945287
    const-string v3, "responsiveness_context"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945288
    invoke-static {v1, v2, p1}, LX/5a2;->a(LX/15i;ILX/0nX;)V

    .line 945289
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 945290
    if-eqz v2, :cond_b

    .line 945291
    const-string v3, "username"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945292
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 945293
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 945294
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 945241
    check-cast p1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageDetailsQueryFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageDetailsQueryFragmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageDetailsQueryFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
