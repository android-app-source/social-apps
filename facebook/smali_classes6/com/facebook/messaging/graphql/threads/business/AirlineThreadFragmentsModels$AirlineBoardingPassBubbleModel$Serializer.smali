.class public final Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineBoardingPassBubbleModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineBoardingPassBubbleModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 961214
    const-class v0, Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineBoardingPassBubbleModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineBoardingPassBubbleModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineBoardingPassBubbleModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 961215
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 961216
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineBoardingPassBubbleModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 961217
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 961218
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 961219
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 961220
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 961221
    if-eqz v2, :cond_0

    .line 961222
    const-string p0, "boarding_passes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961223
    invoke-static {v1, v2, p1, p2}, LX/5cO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 961224
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961225
    if-eqz v2, :cond_1

    .line 961226
    const-string p0, "boarding_time_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961227
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961228
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961229
    if-eqz v2, :cond_2

    .line 961230
    const-string p0, "boarding_zone_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961231
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961232
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961233
    if-eqz v2, :cond_3

    .line 961234
    const-string p0, "departure_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961235
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961236
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961237
    if-eqz v2, :cond_4

    .line 961238
    const-string p0, "flight_gate_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961239
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961240
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961241
    if-eqz v2, :cond_5

    .line 961242
    const-string p0, "flight_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961243
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961244
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961245
    if-eqz v2, :cond_6

    .line 961246
    const-string p0, "flight_terminal_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961247
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961248
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961249
    if-eqz v2, :cond_7

    .line 961250
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961251
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961252
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 961253
    if-eqz v2, :cond_8

    .line 961254
    const-string p0, "logo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961255
    invoke-static {v1, v2, p1}, LX/5VF;->a(LX/15i;ILX/0nX;)V

    .line 961256
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961257
    if-eqz v2, :cond_9

    .line 961258
    const-string p0, "message_cta_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961259
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961260
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 961261
    if-eqz v2, :cond_a

    .line 961262
    const-string p0, "page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961263
    invoke-static {v1, v2, p1}, LX/5cP;->a(LX/15i;ILX/0nX;)V

    .line 961264
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961265
    if-eqz v2, :cond_b

    .line 961266
    const-string p0, "passenger_name_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961267
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961268
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961269
    if-eqz v2, :cond_c

    .line 961270
    const-string p0, "passenger_names_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961271
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961272
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961273
    if-eqz v2, :cond_d

    .line 961274
    const-string p0, "passenger_seat_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961275
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961276
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961277
    if-eqz v2, :cond_e

    .line 961278
    const-string p0, "share_cta_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961279
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961280
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961281
    if-eqz v2, :cond_f

    .line 961282
    const-string p0, "tint_color"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961283
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961284
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 961285
    if-eqz v2, :cond_10

    .line 961286
    const-string p0, "view_boarding_pass_cta_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 961287
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 961288
    :cond_10
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 961289
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 961290
    check-cast p1, Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineBoardingPassBubbleModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineBoardingPassBubbleModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineBoardingPassBubbleModel;LX/0nX;LX/0my;)V

    return-void
.end method
