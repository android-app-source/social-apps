.class public final Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineConfirmationBubbleModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineConfirmationBubbleModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 962009
    const-class v0, Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineConfirmationBubbleModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineConfirmationBubbleModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineConfirmationBubbleModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 962010
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 962011
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineConfirmationBubbleModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 962012
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 962013
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 962014
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 962015
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 962016
    if-eqz v2, :cond_0

    .line 962017
    const-string p0, "booking_number_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962018
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 962019
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 962020
    if-eqz v2, :cond_1

    .line 962021
    const-string p0, "formatted_total"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962022
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 962023
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 962024
    if-eqz v2, :cond_2

    .line 962025
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962026
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 962027
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 962028
    if-eqz v2, :cond_3

    .line 962029
    const-string p0, "itinerary_legs"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962030
    invoke-static {v1, v2, p1, p2}, LX/5cW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 962031
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 962032
    if-eqz v2, :cond_4

    .line 962033
    const-string p0, "logo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962034
    invoke-static {v1, v2, p1}, LX/5VF;->a(LX/15i;ILX/0nX;)V

    .line 962035
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 962036
    if-eqz v2, :cond_5

    .line 962037
    const-string p0, "passenger_infos"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962038
    invoke-static {v1, v2, p1, p2}, LX/5cZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 962039
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 962040
    if-eqz v2, :cond_6

    .line 962041
    const-string p0, "passenger_names_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962042
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 962043
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 962044
    if-eqz v2, :cond_7

    .line 962045
    const-string p0, "pnr_number"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962046
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 962047
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 962048
    if-eqz v2, :cond_8

    .line 962049
    const-string p0, "tint_color"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962050
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 962051
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 962052
    if-eqz v2, :cond_9

    .line 962053
    const-string p0, "total_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962054
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 962055
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 962056
    if-eqz v2, :cond_a

    .line 962057
    const-string p0, "view_details_cta_label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962058
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 962059
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 962060
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 962061
    check-cast p1, Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineConfirmationBubbleModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineConfirmationBubbleModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineConfirmationBubbleModel;LX/0nX;LX/0my;)V

    return-void
.end method
