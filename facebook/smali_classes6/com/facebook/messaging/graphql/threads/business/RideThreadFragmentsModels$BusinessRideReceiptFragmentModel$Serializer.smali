.class public final Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideReceiptFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideReceiptFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 963449
    const-class v0, Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideReceiptFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideReceiptFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideReceiptFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 963450
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 963451
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideReceiptFragmentModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 963452
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 963453
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 963454
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 963455
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 963456
    if-eqz v2, :cond_0

    .line 963457
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963458
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 963459
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 963460
    if-eqz v2, :cond_1

    .line 963461
    const-string v3, "destination_address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963462
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963463
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 963464
    if-eqz v2, :cond_2

    .line 963465
    const-string v3, "destination_location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963466
    invoke-static {v1, v2, p1}, LX/5cj;->a(LX/15i;ILX/0nX;)V

    .line 963467
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 963468
    if-eqz v2, :cond_3

    .line 963469
    const-string v3, "display_duration"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963470
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963471
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 963472
    if-eqz v2, :cond_4

    .line 963473
    const-string v3, "display_total"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963474
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963475
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 963476
    cmpl-double v4, v2, v4

    if-eqz v4, :cond_5

    .line 963477
    const-string v4, "distance"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963478
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 963479
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 963480
    if-eqz v2, :cond_6

    .line 963481
    const-string v3, "distance_unit"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963482
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963483
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 963484
    if-eqz v2, :cond_7

    .line 963485
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963486
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963487
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 963488
    if-eqz v2, :cond_8

    .line 963489
    const-string v3, "ride_display_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963490
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963491
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 963492
    if-eqz v2, :cond_9

    .line 963493
    const-string v3, "ride_provider"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963494
    invoke-static {v1, v2, p1, p2}, LX/5ck;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 963495
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 963496
    if-eqz v2, :cond_a

    .line 963497
    const-string v3, "source_address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963498
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 963499
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 963500
    if-eqz v2, :cond_b

    .line 963501
    const-string v3, "source_location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 963502
    invoke-static {v1, v2, p1}, LX/5cj;->a(LX/15i;ILX/0nX;)V

    .line 963503
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 963504
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 963505
    check-cast p1, Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideReceiptFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideReceiptFragmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideReceiptFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
