.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 948392
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 948393
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 948394
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 948395
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 948396
    const/4 v11, 0x0

    .line 948397
    const/4 v10, 0x0

    .line 948398
    const/4 v9, 0x0

    .line 948399
    const/4 v8, 0x0

    .line 948400
    const/4 v7, 0x0

    .line 948401
    const/4 v6, 0x0

    .line 948402
    const/4 v5, 0x0

    .line 948403
    const/4 v4, 0x0

    .line 948404
    const/4 v3, 0x0

    .line 948405
    const/4 v2, 0x0

    .line 948406
    const/4 v1, 0x0

    .line 948407
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, p0, :cond_2

    .line 948408
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 948409
    const/4 v1, 0x0

    .line 948410
    :goto_0
    move v1, v1

    .line 948411
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 948412
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 948413
    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageAnimatedImageAttachmentModel;-><init>()V

    .line 948414
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 948415
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 948416
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 948417
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 948418
    :cond_0
    return-object v1

    .line 948419
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 948420
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_c

    .line 948421
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 948422
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 948423
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 948424
    const-string p0, "animated_image_full_screen"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 948425
    invoke-static {p1, v0}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 948426
    :cond_3
    const-string p0, "animated_image_large_preview"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 948427
    invoke-static {p1, v0}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 948428
    :cond_4
    const-string p0, "animated_image_medium_preview"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 948429
    invoke-static {p1, v0}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 948430
    :cond_5
    const-string p0, "animated_image_original_dimensions"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 948431
    invoke-static {p1, v0}, LX/5ac;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 948432
    :cond_6
    const-string p0, "animated_image_render_as_sticker"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 948433
    const/4 v1, 0x1

    .line 948434
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 948435
    :cond_7
    const-string p0, "animated_image_small_preview"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 948436
    invoke-static {p1, v0}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 948437
    :cond_8
    const-string p0, "animated_static_image_full_screen"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 948438
    invoke-static {p1, v0}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 948439
    :cond_9
    const-string p0, "animated_static_image_large_preview"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 948440
    invoke-static {p1, v0}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 948441
    :cond_a
    const-string p0, "animated_static_image_medium_preview"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    .line 948442
    invoke-static {p1, v0}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 948443
    :cond_b
    const-string p0, "animated_static_image_small_preview"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 948444
    invoke-static {p1, v0}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 948445
    :cond_c
    const/16 v12, 0xa

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 948446
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 948447
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 948448
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 948449
    const/4 v9, 0x3

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 948450
    if-eqz v1, :cond_d

    .line 948451
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 948452
    :cond_d
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 948453
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 948454
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 948455
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 948456
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 948457
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
