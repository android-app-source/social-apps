.class public final Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x8aee903
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 933210
    const-class v0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 933209
    const-class v0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 933207
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 933208
    return-void
.end method

.method private j()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 933205
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->f:Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->f:Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    .line 933206
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->f:Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 933195
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 933196
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 933197
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->j()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 933198
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 933199
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 933200
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 933201
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 933202
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 933203
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 933204
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 933211
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 933212
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->j()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 933213
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->j()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    .line 933214
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->j()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 933215
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    .line 933216
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->f:Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    .line 933217
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 933218
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 933193
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->e:Ljava/lang/String;

    .line 933194
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 933190
    new-instance v0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;-><init>()V

    .line 933191
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 933192
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 933189
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->j()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel$InitiatorModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 933187
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->g:Ljava/lang/String;

    .line 933188
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 933186
    const v0, -0x1a3d1017

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 933185
    const v0, -0x4d77ab77

    return v0
.end method
