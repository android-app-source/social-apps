.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessThreadQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessThreadQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 945765
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessThreadQueryModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessThreadQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessThreadQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 945766
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 945767
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessThreadQueryModel;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 945768
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 945769
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 945770
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 945771
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 945772
    if-eqz v2, :cond_6

    .line 945773
    const-string v3, "message_threads"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945774
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 945775
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 945776
    if-eqz v3, :cond_5

    .line 945777
    const-string v4, "nodes"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945778
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 945779
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 945780
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 945781
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 945782
    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 945783
    if-eqz v6, :cond_3

    .line 945784
    const-string p0, "other_participants"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945785
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 945786
    const/4 p0, 0x0

    invoke-virtual {v1, v6, p0}, LX/15i;->g(II)I

    move-result p0

    .line 945787
    if-eqz p0, :cond_2

    .line 945788
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945789
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 945790
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 945791
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v2

    .line 945792
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 945793
    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, LX/15i;->g(II)I

    move-result v5

    .line 945794
    if-eqz v5, :cond_0

    .line 945795
    const-string v6, "messaging_actor"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945796
    invoke-static {v1, v5, p1, p2}, LX/5a4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 945797
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 945798
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 945799
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 945800
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 945801
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 945802
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 945803
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 945804
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 945805
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 945806
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 945807
    check-cast p1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessThreadQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessThreadQueryModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessThreadQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
