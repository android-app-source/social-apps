.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 946860
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 946861
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 946862
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 946863
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 946864
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 946865
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 946866
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 946867
    if-eqz v2, :cond_0

    .line 946868
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 946869
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 946870
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 946871
    if-eqz v2, :cond_2

    .line 946872
    const-string p0, "blob_attachments"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 946873
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 946874
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_1

    .line 946875
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1}, LX/5aE;->a(LX/15i;ILX/0nX;)V

    .line 946876
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 946877
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 946878
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 946879
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 946880
    check-cast p1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$FileAttachmentUrlQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
