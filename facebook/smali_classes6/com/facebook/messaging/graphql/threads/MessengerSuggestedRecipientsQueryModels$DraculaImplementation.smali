.class public final Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 930162
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 930163
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 930247
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 930248
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 930212
    if-nez p1, :cond_0

    .line 930213
    :goto_0
    return v0

    .line 930214
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 930215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 930216
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 930217
    const v2, 0x4afa422e    # 8200471.0f

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 930218
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 930219
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 930220
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 930221
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 930222
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 930223
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 930224
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastListType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastListType;

    move-result-object v1

    .line 930225
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 930226
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v2

    .line 930227
    const v3, 0x7dcd80b3

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 930228
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 930229
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 930230
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 930231
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 930232
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 930233
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 930234
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 930235
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 930236
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 930237
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 930238
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 930239
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 930240
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 930241
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 930242
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 930243
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 930244
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 930245
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 930246
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4afa422e -> :sswitch_1
        0x4c427b33 -> :sswitch_3
        0x56f5e1c2 -> :sswitch_0
        0x7dcd80b3 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 930211
    new-instance v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 930204
    sparse-switch p2, :sswitch_data_0

    .line 930205
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 930206
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 930207
    const v1, 0x4afa422e    # 8200471.0f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 930208
    :goto_0
    :sswitch_1
    return-void

    .line 930209
    :sswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 930210
    const v1, 0x7dcd80b3

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4afa422e -> :sswitch_2
        0x4c427b33 -> :sswitch_1
        0x56f5e1c2 -> :sswitch_0
        0x7dcd80b3 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 930198
    if-eqz p1, :cond_0

    .line 930199
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;

    move-result-object v1

    .line 930200
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;

    .line 930201
    if-eq v0, v1, :cond_0

    .line 930202
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 930203
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 930197
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 930195
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 930196
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 930190
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 930191
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 930192
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->a:LX/15i;

    .line 930193
    iput p2, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->b:I

    .line 930194
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 930189
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 930188
    new-instance v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 930185
    iget v0, p0, LX/1vt;->c:I

    .line 930186
    move v0, v0

    .line 930187
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 930182
    iget v0, p0, LX/1vt;->c:I

    .line 930183
    move v0, v0

    .line 930184
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 930179
    iget v0, p0, LX/1vt;->b:I

    .line 930180
    move v0, v0

    .line 930181
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930176
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 930177
    move-object v0, v0

    .line 930178
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 930167
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 930168
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 930169
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 930170
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 930171
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 930172
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 930173
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 930174
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 930175
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 930164
    iget v0, p0, LX/1vt;->c:I

    .line 930165
    move v0, v0

    .line 930166
    return v0
.end method
