.class public final Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/5Vu;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xc0e9498
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$Serializer;
.end annotation


# instance fields
.field private A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLCommercePageType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:D

.field private l:Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 959713
    const-class v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 959714
    const-class v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 959715
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 959716
    return-void
.end method

.method private l()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959717
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->l:Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->l:Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    .line 959718
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->l:Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    return-object v0
.end method

.method private m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959719
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->x:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->x:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    .line 959720
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->x:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    return-object v0
.end method

.method private p()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessengerExtension"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959709
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->z:Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->z:Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    .line 959710
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->z:Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    return-object v0
.end method

.method private q()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959721
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->E:Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->E:Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    .line 959722
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->E:Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    return-object v0
.end method

.method private u()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959723
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->F:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->F:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 959724
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->F:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    return-object v0
.end method

.method private v()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959725
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->G:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->G:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 959726
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->G:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    return-object v0
.end method

.method private x()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959727
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->H:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->H:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 959728
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->H:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    return-object v0
.end method

.method private y()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStructuredName"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959729
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->I:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->I:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    .line 959730
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->I:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    return-object v0
.end method


# virtual methods
.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959731
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->n:Ljava/lang/String;

    .line 959732
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final D()Z
    .locals 2

    .prologue
    .line 959733
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959734
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->o:Z

    return v0
.end method

.method public final E()Z
    .locals 2

    .prologue
    .line 959812
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959813
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->p:Z

    return v0
.end method

.method public final G()Z
    .locals 2

    .prologue
    .line 959735
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959736
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->q:Z

    return v0
.end method

.method public final H()Z
    .locals 2

    .prologue
    .line 959810
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959811
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->r:Z

    return v0
.end method

.method public final I()Z
    .locals 2

    .prologue
    .line 959808
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959809
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->s:Z

    return v0
.end method

.method public final J()Z
    .locals 2

    .prologue
    .line 959806
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959807
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->t:Z

    return v0
.end method

.method public final K()Z
    .locals 2

    .prologue
    .line 959804
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959805
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->u:Z

    return v0
.end method

.method public final L()Z
    .locals 2

    .prologue
    .line 959802
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959803
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->v:Z

    return v0
.end method

.method public final M()Z
    .locals 1

    .prologue
    const/4 v0, 0x2

    .line 959800
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959801
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->w:Z

    return v0
.end method

.method public final Q()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 959798
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->y:Ljava/util/List;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->y:Ljava/util/List;

    .line 959799
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->y:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final R()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 959796
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->A:Ljava/util/List;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->A:Ljava/util/List;

    .line 959797
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->A:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final S()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959794
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->B:Ljava/lang/String;

    const/16 v1, 0x17

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->B:Ljava/lang/String;

    .line 959795
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final U()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959792
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->C:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->C:Ljava/lang/String;

    .line 959793
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final V()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageMessengerBot"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959711
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959712
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->D:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 24

    .prologue
    .line 959737
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 959738
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 959739
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->r()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(Ljava/util/List;)I

    move-result v3

    .line 959740
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->s()Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 959741
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->l()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 959742
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->w()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/util/List;)I

    move-result v9

    .line 959743
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->B()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 959744
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 959745
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->Q()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v12

    .line 959746
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->p()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 959747
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->R()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v14

    .line 959748
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->S()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 959749
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->U()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 959750
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->V()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, 0x7f229591

    invoke-static {v6, v5, v7}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 959751
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->q()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 959752
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->u()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 959753
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->v()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 959754
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->x()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 959755
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->y()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 959756
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->ab()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 959757
    const/16 v5, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 959758
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v2}, LX/186;->b(II)V

    .line 959759
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->f:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->a(IZ)V

    .line 959760
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->g:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->a(IZ)V

    .line 959761
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->h:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->a(IZ)V

    .line 959762
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 959763
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 959764
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->k:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 959765
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 959766
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 959767
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 959768
    const/16 v2, 0xa

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->o:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 959769
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 959770
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 959771
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 959772
    const/16 v2, 0xe

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->s:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 959773
    const/16 v2, 0xf

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->t:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 959774
    const/16 v2, 0x10

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->u:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 959775
    const/16 v2, 0x11

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->v:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 959776
    const/16 v2, 0x12

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->w:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 959777
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 959778
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 959779
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 959780
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 959781
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 959782
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 959783
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 959784
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 959785
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 959786
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 959787
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 959788
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 959789
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 959790
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 959791
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 959627
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 959628
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->l()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 959629
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->l()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    .line 959630
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->l()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 959631
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959632
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->l:Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    .line 959633
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 959634
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    .line 959635
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 959636
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959637
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->x:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    .line 959638
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->Q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 959639
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->Q()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 959640
    if-eqz v2, :cond_2

    .line 959641
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959642
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->y:Ljava/util/List;

    move-object v1, v0

    .line 959643
    :cond_2
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->p()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 959644
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->p()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    .line 959645
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->p()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 959646
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959647
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->z:Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    .line 959648
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->R()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 959649
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->R()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 959650
    if-eqz v2, :cond_4

    .line 959651
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959652
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->A:Ljava/util/List;

    move-object v1, v0

    .line 959653
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->V()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 959654
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->V()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7f229591

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 959655
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->V()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 959656
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959657
    iput v3, v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->D:I

    move-object v1, v0

    .line 959658
    :cond_5
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->q()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 959659
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->q()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    .line 959660
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->q()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 959661
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959662
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->E:Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    .line 959663
    :cond_6
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->u()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 959664
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->u()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 959665
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->u()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 959666
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959667
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->F:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 959668
    :cond_7
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->v()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 959669
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->v()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 959670
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->v()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 959671
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959672
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->G:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 959673
    :cond_8
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->x()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 959674
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->x()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 959675
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->x()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 959676
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959677
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->H:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 959678
    :cond_9
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->y()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 959679
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->y()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    .line 959680
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->y()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 959681
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 959682
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->I:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    .line 959683
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 959684
    if-nez v1, :cond_b

    :goto_0
    return-object p0

    .line 959685
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_b
    move-object p0, v1

    .line 959686
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 959596
    new-instance v0, LX/5bg;

    invoke-direct {v0, p1}, LX/5bg;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959624
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 959608
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 959609
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->f:Z

    .line 959610
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->g:Z

    .line 959611
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->h:Z

    .line 959612
    const/4 v0, 0x6

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->k:D

    .line 959613
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->o:Z

    .line 959614
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->p:Z

    .line 959615
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->q:Z

    .line 959616
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->r:Z

    .line 959617
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->s:Z

    .line 959618
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->t:Z

    .line 959619
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->u:Z

    .line 959620
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->v:Z

    .line 959621
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->w:Z

    .line 959622
    const/16 v0, 0x19

    const v1, 0x7f229591

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->D:I

    .line 959623
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 959606
    invoke-virtual {p2}, LX/18L;->a()V

    .line 959607
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 959605
    return-void
.end method

.method public final ab()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959603
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->J:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->J:Ljava/lang/String;

    .line 959604
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic ad()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStructuredName"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959601
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->y()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ag()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessengerExtension"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959600
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->p()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic am()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959599
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->x()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic an()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959598
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->v()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ao()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959597
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->u()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ap()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959602
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->q()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aq()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959687
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic au()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959688
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->l()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 959689
    new-instance v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;-><init>()V

    .line 959690
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 959691
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 959692
    const v0, 0x21d23644

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 959693
    const v0, -0x5de3ee8f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959694
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 959695
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 959696
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 959697
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959698
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->f:Z

    return v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 959625
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959626
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->g:Z

    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 959699
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959700
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->h:Z

    return v0
.end method

.method public final r()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 959701
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->i:Ljava/util/List;

    .line 959702
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLCommercePageType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 959703
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->j:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->j:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 959704
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->j:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    return-object v0
.end method

.method public final t()D
    .locals 2

    .prologue
    .line 959705
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 959706
    iget-wide v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->k:D

    return-wide v0
.end method

.method public final w()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 959707
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->m:Ljava/util/List;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->m:Ljava/util/List;

    .line 959708
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
