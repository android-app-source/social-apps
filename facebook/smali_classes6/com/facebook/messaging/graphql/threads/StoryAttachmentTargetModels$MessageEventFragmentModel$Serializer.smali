.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 934756
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 934757
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 934788
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 934759
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 934760
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v6, 0x0

    .line 934761
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 934762
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 934763
    cmp-long v4, v2, v6

    if-eqz v4, :cond_0

    .line 934764
    const-string v4, "end_timestamp"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 934765
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 934766
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 934767
    if-eqz v2, :cond_1

    .line 934768
    const-string v3, "event_coordinates"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 934769
    invoke-static {v1, v2, p1}, LX/5YF;->a(LX/15i;ILX/0nX;)V

    .line 934770
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 934771
    if-eqz v2, :cond_2

    .line 934772
    const-string v3, "event_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 934773
    invoke-static {v1, v2, p1}, LX/5YG;->a(LX/15i;ILX/0nX;)V

    .line 934774
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 934775
    if-eqz v2, :cond_3

    .line 934776
    const-string v3, "event_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 934777
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 934778
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 934779
    if-eqz v2, :cond_4

    .line 934780
    const-string v3, "is_all_day"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 934781
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 934782
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 934783
    cmp-long v4, v2, v6

    if-eqz v4, :cond_5

    .line 934784
    const-string v4, "start_timestamp"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 934785
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 934786
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 934787
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 934758
    check-cast p1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
