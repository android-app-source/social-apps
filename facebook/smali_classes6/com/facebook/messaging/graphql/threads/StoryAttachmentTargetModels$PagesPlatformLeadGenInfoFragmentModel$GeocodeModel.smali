.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x41d8a50c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 937485
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 937486
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 937440
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 937441
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 937482
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 937483
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 937484
    return-void
.end method

.method public static a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;)Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;
    .locals 9

    .prologue
    .line 937459
    if-nez p0, :cond_0

    .line 937460
    const/4 p0, 0x0

    .line 937461
    :goto_0
    return-object p0

    .line 937462
    :cond_0
    instance-of v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    if-eqz v0, :cond_1

    .line 937463
    check-cast p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    goto :goto_0

    .line 937464
    :cond_1
    new-instance v0, LX/5Xg;

    invoke-direct {v0}, LX/5Xg;-><init>()V

    .line 937465
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5Xg;->a:Ljava/lang/String;

    .line 937466
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5Xg;->b:Ljava/lang/String;

    .line 937467
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 937468
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 937469
    iget-object v3, v0, LX/5Xg;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 937470
    iget-object v5, v0, LX/5Xg;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 937471
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 937472
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 937473
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 937474
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 937475
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 937476
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 937477
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 937478
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 937479
    new-instance v3, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    invoke-direct {v3, v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;-><init>(LX/15i;)V

    .line 937480
    move-object p0, v3

    .line 937481
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 937451
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 937452
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 937453
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 937454
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 937455
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 937456
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 937457
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 937458
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 937487
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 937488
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 937489
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 937449
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;->e:Ljava/lang/String;

    .line 937450
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 937446
    new-instance v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;-><init>()V

    .line 937447
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 937448
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 937444
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;->f:Ljava/lang/String;

    .line 937445
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 937443
    const v0, -0x70f50191

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 937442
    const v0, 0x1a4d5a40

    return v0
.end method
