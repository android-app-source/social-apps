.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 945409
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 945410
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 945411
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 945412
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 945413
    const/4 v10, 0x0

    .line 945414
    const/4 v9, 0x0

    .line 945415
    const/4 v8, 0x0

    .line 945416
    const/4 v7, 0x0

    .line 945417
    const/4 v6, 0x0

    .line 945418
    const/4 v5, 0x0

    .line 945419
    const/4 v4, 0x0

    .line 945420
    const/4 v3, 0x0

    .line 945421
    const/4 v2, 0x0

    .line 945422
    const/4 v1, 0x0

    .line 945423
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, p0, :cond_2

    .line 945424
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 945425
    const/4 v1, 0x0

    .line 945426
    :goto_0
    move v1, v1

    .line 945427
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 945428
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 945429
    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel;-><init>()V

    .line 945430
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 945431
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 945432
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 945433
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 945434
    :cond_0
    return-object v1

    .line 945435
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 945436
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_c

    .line 945437
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 945438
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 945439
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 945440
    const-string p0, "__type__"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 945441
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 945442
    :cond_4
    const-string p0, "commerce_page_type"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 945443
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 945444
    :cond_5
    const-string p0, "id"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 945445
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 945446
    :cond_6
    const-string p0, "is_messenger_user"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 945447
    const/4 v1, 0x1

    .line 945448
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 945449
    :cond_7
    const-string p0, "name"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 945450
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 945451
    :cond_8
    const-string p0, "profile_pic_large"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 945452
    invoke-static {p1, v0}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 945453
    :cond_9
    const-string p0, "profile_pic_medium"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 945454
    invoke-static {p1, v0}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 945455
    :cond_a
    const-string p0, "profile_pic_small"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    .line 945456
    invoke-static {p1, v0}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 945457
    :cond_b
    const-string p0, "username"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 945458
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 945459
    :cond_c
    const/16 v11, 0x9

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 945460
    const/4 v11, 0x0

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 945461
    const/4 v10, 0x1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 945462
    const/4 v9, 0x2

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 945463
    if-eqz v1, :cond_d

    .line 945464
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 945465
    :cond_d
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 945466
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 945467
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 945468
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 945469
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 945470
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
