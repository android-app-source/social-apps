.class public final Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 923175
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 923176
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 923173
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 923174
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 923140
    if-nez p1, :cond_0

    .line 923141
    :goto_0
    return v0

    .line 923142
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 923143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 923144
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 923145
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 923146
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 923147
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 923148
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 923149
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 923150
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 923151
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 923152
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 923153
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 923154
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v3

    .line 923155
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 923156
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 923157
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 923158
    invoke-virtual {p3, v6, v3}, LX/186;->a(IZ)V

    .line 923159
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 923160
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 923161
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 923162
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 923163
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 923164
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 923165
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 923166
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 923167
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 923168
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 923169
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 923170
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 923171
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 923172
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x58e4c637 -> :sswitch_3
        -0x17250ad5 -> :sswitch_1
        0x43c448ce -> :sswitch_0
        0x6b8e6abf -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 923131
    if-nez p0, :cond_0

    move v0, v1

    .line 923132
    :goto_0
    return v0

    .line 923133
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 923134
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 923135
    :goto_1
    if-ge v1, v2, :cond_2

    .line 923136
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 923137
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 923138
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 923139
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 923124
    const/4 v7, 0x0

    .line 923125
    const/4 v1, 0x0

    .line 923126
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 923127
    invoke-static {v2, v3, v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 923128
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 923129
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 923130
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 923123
    new-instance v0, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 923120
    sparse-switch p0, :sswitch_data_0

    .line 923121
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 923122
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x58e4c637 -> :sswitch_0
        -0x17250ad5 -> :sswitch_0
        0x43c448ce -> :sswitch_0
        0x6b8e6abf -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 923119
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 923177
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;->b(I)V

    .line 923178
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 923114
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 923115
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 923116
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;->a:LX/15i;

    .line 923117
    iput p2, p0, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;->b:I

    .line 923118
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 923113
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 923112
    new-instance v0, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 923109
    iget v0, p0, LX/1vt;->c:I

    .line 923110
    move v0, v0

    .line 923111
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 923106
    iget v0, p0, LX/1vt;->c:I

    .line 923107
    move v0, v0

    .line 923108
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 923103
    iget v0, p0, LX/1vt;->b:I

    .line 923104
    move v0, v0

    .line 923105
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 923100
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 923101
    move-object v0, v0

    .line 923102
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 923091
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 923092
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 923093
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 923094
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 923095
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 923096
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 923097
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 923098
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 923099
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 923088
    iget v0, p0, LX/1vt;->c:I

    .line 923089
    move v0, v0

    .line 923090
    return v0
.end method
