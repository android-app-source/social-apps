.class public final Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 925829
    const-class v0, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 925830
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 925831
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 925832
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 925833
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 925834
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 925835
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 925836
    if-eqz v2, :cond_0

    .line 925837
    const-string v2, "bubble_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925838
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 925839
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 925840
    if-eqz v2, :cond_1

    .line 925841
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925842
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 925843
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 925844
    if-eqz v2, :cond_2

    .line 925845
    const-string p0, "merchant_name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925846
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 925847
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 925848
    if-eqz v2, :cond_3

    .line 925849
    const-string p0, "order_payment_method"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925850
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 925851
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 925852
    if-eqz v2, :cond_4

    .line 925853
    const-string p0, "partner_logo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925854
    invoke-static {v1, v2, p1}, LX/5VF;->a(LX/15i;ILX/0nX;)V

    .line 925855
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 925856
    if-eqz v2, :cond_5

    .line 925857
    const-string p0, "receipt_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925858
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 925859
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 925860
    if-eqz v2, :cond_6

    .line 925861
    const-string p0, "receipt_url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925862
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 925863
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 925864
    if-eqz v2, :cond_7

    .line 925865
    const-string p0, "retail_items"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925866
    invoke-static {v1, v2, p1, p2}, LX/5V2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 925867
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 925868
    if-eqz v2, :cond_8

    .line 925869
    const-string p0, "status"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925870
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 925871
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 925872
    if-eqz v2, :cond_9

    .line 925873
    const-string p0, "structured_address"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925874
    invoke-static {v1, v2, p1}, LX/5V0;->a(LX/15i;ILX/0nX;)V

    .line 925875
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 925876
    if-eqz v2, :cond_a

    .line 925877
    const-string p0, "total"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925878
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 925879
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 925880
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 925881
    check-cast p1, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel;LX/0nX;LX/0my;)V

    return-void
.end method
