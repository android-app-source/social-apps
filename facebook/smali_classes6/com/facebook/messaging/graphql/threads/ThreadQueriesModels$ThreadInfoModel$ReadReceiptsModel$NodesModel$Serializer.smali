.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 951265
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 951266
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 951267
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 951268
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 951269
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/5bC;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 951270
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 951271
    check-cast p1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel$NodesModel;LX/0nX;LX/0my;)V

    return-void
.end method
