.class public final Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$BusinessMessageModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$BusinessMessageModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 925053
    const-class v0, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$BusinessMessageModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$BusinessMessageModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$BusinessMessageModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 925054
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 925055
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$BusinessMessageModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 925056
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 925057
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 925058
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 925059
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 925060
    if-eqz v2, :cond_0

    .line 925061
    const-string p0, "business_items"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925062
    invoke-static {v1, v2, p1, p2}, LX/5Ux;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 925063
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 925064
    if-eqz v2, :cond_1

    .line 925065
    const-string p0, "call_to_actions"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925066
    invoke-static {v1, v2, p1, p2}, LX/5To;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 925067
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 925068
    if-eqz v2, :cond_2

    .line 925069
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925070
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 925071
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 925072
    if-eqz v2, :cond_3

    .line 925073
    const-string p0, "messaging_attribution"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925074
    invoke-static {v1, v2, p1}, LX/5Ty;->a(LX/15i;ILX/0nX;)V

    .line 925075
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 925076
    if-eqz v2, :cond_4

    .line 925077
    const-string p0, "snippet"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 925078
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 925079
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 925080
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 925081
    check-cast p1, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$BusinessMessageModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$BusinessMessageModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$BusinessMessageModel;LX/0nX;LX/0my;)V

    return-void
.end method
