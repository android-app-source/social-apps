.class public final Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentTrackingBubbleModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentTrackingBubbleModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 927399
    const-class v0, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentTrackingBubbleModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentTrackingBubbleModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentTrackingBubbleModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 927400
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 927401
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentTrackingBubbleModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 927402
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 927403
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x4

    const/4 v3, 0x0

    .line 927404
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 927405
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 927406
    if-eqz v2, :cond_0

    .line 927407
    const-string v2, "bubble_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927408
    invoke-virtual {v1, v0, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927409
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 927410
    if-eqz v2, :cond_1

    .line 927411
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927412
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927413
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 927414
    if-eqz v2, :cond_2

    .line 927415
    const-string v3, "messenger_commerce_location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927416
    invoke-static {v1, v2, p1}, LX/5V0;->a(LX/15i;ILX/0nX;)V

    .line 927417
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 927418
    if-eqz v2, :cond_3

    .line 927419
    const-string v3, "shipment"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927420
    invoke-static {v1, v2, p1, p2}, LX/5VE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 927421
    :cond_3
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 927422
    if-eqz v2, :cond_4

    .line 927423
    const-string v2, "shipment_tracking_event_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927424
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927425
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 927426
    if-eqz v2, :cond_5

    .line 927427
    const-string v3, "tracking_event_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927428
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927429
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 927430
    if-eqz v2, :cond_6

    .line 927431
    const-string v3, "tracking_event_time_for_display"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927432
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927433
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 927434
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 927435
    check-cast p1, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentTrackingBubbleModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentTrackingBubbleModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentTrackingBubbleModel;LX/0nX;LX/0my;)V

    return-void
.end method
