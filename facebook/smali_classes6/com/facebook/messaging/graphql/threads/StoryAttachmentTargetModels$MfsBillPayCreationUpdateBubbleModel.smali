.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/5Wj;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46a9f134
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$ConvenienceFeeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$TotalDueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 936706
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 936705
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 936703
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 936704
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936701
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->e:Ljava/lang/String;

    .line 936702
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936699
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->f:Ljava/lang/String;

    .line 936700
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936697
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->g:Ljava/lang/String;

    .line 936698
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936695
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    .line 936696
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936693
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->i:Ljava/lang/String;

    .line 936694
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$ConvenienceFeeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936691
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->j:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$ConvenienceFeeModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$ConvenienceFeeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$ConvenienceFeeModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->j:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$ConvenienceFeeModel;

    .line 936692
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->j:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$ConvenienceFeeModel;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936640
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->k:Ljava/lang/String;

    .line 936641
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936689
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->l:Ljava/lang/String;

    .line 936690
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method private q()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$TotalDueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936687
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->m:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$TotalDueModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$TotalDueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$TotalDueModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->m:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$TotalDueModel;

    .line 936688
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->m:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$TotalDueModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 936665
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 936666
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 936667
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 936668
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 936669
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->l()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 936670
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 936671
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$ConvenienceFeeModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 936672
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 936673
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->p()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 936674
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->q()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$TotalDueModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 936675
    const/16 v9, 0x9

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 936676
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 936677
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 936678
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 936679
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 936680
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 936681
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 936682
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 936683
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 936684
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 936685
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 936686
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 936647
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 936648
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->l()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 936649
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->l()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    .line 936650
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->l()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 936651
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;

    .line 936652
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    .line 936653
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$ConvenienceFeeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 936654
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$ConvenienceFeeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$ConvenienceFeeModel;

    .line 936655
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->n()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$ConvenienceFeeModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 936656
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;

    .line 936657
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->j:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$ConvenienceFeeModel;

    .line 936658
    :cond_1
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->q()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$TotalDueModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 936659
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->q()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$TotalDueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$TotalDueModel;

    .line 936660
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->q()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$TotalDueModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 936661
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;

    .line 936662
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;->m:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$TotalDueModel;

    .line 936663
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 936664
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 936644
    new-instance v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;-><init>()V

    .line 936645
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 936646
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 936643
    const v0, 0x58e49f53

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 936642
    const v0, -0x77f51fb1

    return v0
.end method
