.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 949379
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 949380
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 949381
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 949382
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 949383
    const/4 v11, 0x0

    .line 949384
    const/4 v10, 0x0

    .line 949385
    const/4 v9, 0x0

    .line 949386
    const/4 v8, 0x0

    .line 949387
    const/4 v7, 0x0

    .line 949388
    const/4 v6, 0x0

    .line 949389
    const/4 v5, 0x0

    .line 949390
    const/4 v4, 0x0

    .line 949391
    const/4 v3, 0x0

    .line 949392
    const/4 v2, 0x0

    .line 949393
    const/4 v1, 0x0

    .line 949394
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, p0, :cond_2

    .line 949395
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 949396
    const/4 v1, 0x0

    .line 949397
    :goto_0
    move v1, v1

    .line 949398
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 949399
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 949400
    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel;-><init>()V

    .line 949401
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 949402
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 949403
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 949404
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 949405
    :cond_0
    return-object v1

    .line 949406
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 949407
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_a

    .line 949408
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 949409
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 949410
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 949411
    const-string p0, "attachment_video_url"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 949412
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 949413
    :cond_3
    const-string p0, "filename"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 949414
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 949415
    :cond_4
    const-string p0, "original_dimensions"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 949416
    invoke-static {p1, v0}, LX/5an;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 949417
    :cond_5
    const-string p0, "playable_duration_in_ms"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 949418
    const/4 v3, 0x1

    .line 949419
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v8

    goto :goto_1

    .line 949420
    :cond_6
    const-string p0, "rotation"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 949421
    const/4 v2, 0x1

    .line 949422
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 949423
    :cond_7
    const-string p0, "streamingImageThumbnail"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 949424
    invoke-static {p1, v0}, LX/5ao;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 949425
    :cond_8
    const-string p0, "video_filesize"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 949426
    const/4 v1, 0x1

    .line 949427
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v5

    goto :goto_1

    .line 949428
    :cond_9
    const-string p0, "video_type"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 949429
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLMessageVideoType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessageVideoType;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto/16 :goto_1

    .line 949430
    :cond_a
    const/16 v12, 0x8

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 949431
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 949432
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 949433
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 949434
    if-eqz v3, :cond_b

    .line 949435
    const/4 v3, 0x3

    const/4 v9, 0x0

    invoke-virtual {v0, v3, v8, v9}, LX/186;->a(III)V

    .line 949436
    :cond_b
    if-eqz v2, :cond_c

    .line 949437
    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v7, v3}, LX/186;->a(III)V

    .line 949438
    :cond_c
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 949439
    if-eqz v1, :cond_d

    .line 949440
    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v5, v2}, LX/186;->a(III)V

    .line 949441
    :cond_d
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 949442
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
