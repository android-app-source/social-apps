.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2311512f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 936458
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 936457
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 936455
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 936456
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 936452
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 936453
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 936454
    return-void
.end method

.method public static a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;)Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;
    .locals 8

    .prologue
    .line 936432
    if-nez p0, :cond_0

    .line 936433
    const/4 p0, 0x0

    .line 936434
    :goto_0
    return-object p0

    .line 936435
    :cond_0
    instance-of v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    if-eqz v0, :cond_1

    .line 936436
    check-cast p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    goto :goto_0

    .line 936437
    :cond_1
    new-instance v0, LX/5XW;

    invoke-direct {v0}, LX/5XW;-><init>()V

    .line 936438
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5XW;->a:Ljava/lang/String;

    .line 936439
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 936440
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 936441
    iget-object v3, v0, LX/5XW;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 936442
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 936443
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 936444
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 936445
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 936446
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 936447
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 936448
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 936449
    new-instance v3, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    invoke-direct {v3, v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;-><init>(LX/15i;)V

    .line 936450
    move-object p0, v3

    .line 936451
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 936459
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 936460
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 936461
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 936462
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 936463
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 936464
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 936429
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 936430
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 936431
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936427
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;->e:Ljava/lang/String;

    .line 936428
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 936424
    new-instance v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;-><init>()V

    .line 936425
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 936426
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 936423
    const v0, 0x31e82352

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 936422
    const v0, -0x6db81817

    return v0
.end method
