.class public final Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotInfoModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 923657
    const-class v0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotInfoModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotInfoModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotInfoModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 923658
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 923659
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 923660
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 923661
    const/4 v2, 0x0

    .line 923662
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 923663
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 923664
    :goto_0
    move v1, v2

    .line 923665
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 923666
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 923667
    new-instance v1, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotInfoModel;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotInfoModel;-><init>()V

    .line 923668
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 923669
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 923670
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 923671
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 923672
    :cond_0
    return-object v1

    .line 923673
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 923674
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, p0, :cond_3

    .line 923675
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 923676
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 923677
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v3, :cond_2

    .line 923678
    const-string p0, "page_messenger_bot"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 923679
    invoke-static {p1, v0}, LX/5U5;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 923680
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 923681
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 923682
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1
.end method
