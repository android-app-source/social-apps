.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/5ZJ;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x8dd1e4c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 947992
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 947993
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 947994
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 947995
    return-void
.end method

.method private a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 947996
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel;->e:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel;->e:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    .line 947997
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel;->e:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 947998
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 947999
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 948000
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 948001
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 948002
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 948003
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 948004
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 948005
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 948006
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    .line 948007
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 948008
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel;

    .line 948009
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel;->e:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel$GenieSenderModel;

    .line 948010
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 948011
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 948012
    new-instance v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieUserMessageFragmentModel;-><init>()V

    .line 948013
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 948014
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 948015
    const v0, -0x31a84a24

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 948016
    const v0, -0x75a97664

    return v0
.end method
