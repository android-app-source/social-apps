.class public final Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 932016
    const-class v0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 932017
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 932018
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 932019
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 932020
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 932021
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_c

    .line 932022
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 932023
    :goto_0
    move v1, v2

    .line 932024
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 932025
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 932026
    new-instance v1, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel;-><init>()V

    .line 932027
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 932028
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 932029
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 932030
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 932031
    :cond_0
    return-object v1

    .line 932032
    :cond_1
    const-string p0, "is_viewer_seller"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 932033
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v9, v1

    move v1, v3

    .line 932034
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_a

    .line 932035
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 932036
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 932037
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 932038
    const-string p0, "click_action"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 932039
    invoke-static {p1, v0}, LX/5WG;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 932040
    :cond_3
    const-string p0, "components"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 932041
    invoke-static {p1, v0}, LX/5WJ;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 932042
    :cond_4
    const-string p0, "item_list"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 932043
    invoke-static {p1, v0}, LX/5WM;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 932044
    :cond_5
    const-string p0, "payment_call_to_actions"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 932045
    invoke-static {p1, v0}, LX/5WO;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 932046
    :cond_6
    const-string p0, "payment_modules_client"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 932047
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_1

    .line 932048
    :cond_7
    const-string p0, "payment_snippet"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 932049
    invoke-static {p1, v0}, LX/5WP;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 932050
    :cond_8
    const-string p0, "payment_total"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 932051
    invoke-static {p1, v0}, LX/5WQ;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 932052
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 932053
    :cond_a
    const/16 v12, 0x8

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 932054
    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 932055
    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 932056
    if-eqz v1, :cond_b

    .line 932057
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v9}, LX/186;->a(IZ)V

    .line 932058
    :cond_b
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 932059
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 932060
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 932061
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 932062
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 932063
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    goto/16 :goto_1
.end method
