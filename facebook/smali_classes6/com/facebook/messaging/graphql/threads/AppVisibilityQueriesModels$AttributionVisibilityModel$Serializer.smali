.class public final Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 923530
    const-class v0, Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 923531
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 923532
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 923533
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 923534
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 923535
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 923536
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 923537
    if-eqz v2, :cond_0

    .line 923538
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923539
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 923540
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 923541
    if-eqz v2, :cond_4

    .line 923542
    const-string p0, "messenger_app_attribution_visibility"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923543
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 923544
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 923545
    if-eqz p0, :cond_1

    .line 923546
    const-string v0, "hide_attribution"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923547
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 923548
    :cond_1
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 923549
    if-eqz p0, :cond_2

    .line 923550
    const-string v0, "hide_install_button"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923551
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 923552
    :cond_2
    const/4 p0, 0x2

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 923553
    if-eqz p0, :cond_3

    .line 923554
    const-string v0, "hide_reply_button"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923555
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 923556
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 923557
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 923558
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 923559
    check-cast p1, Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/AppVisibilityQueriesModels$AttributionVisibilityModel;LX/0nX;LX/0my;)V

    return-void
.end method
