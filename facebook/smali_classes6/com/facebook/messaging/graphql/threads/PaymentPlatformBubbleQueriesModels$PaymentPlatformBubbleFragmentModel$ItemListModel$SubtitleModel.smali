.class public final Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x50a312db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 932148
    const-class v0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 932147
    const-class v0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 932145
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 932146
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 932106
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 932107
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 932108
    return-void
.end method

.method public static a(Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;)Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;
    .locals 8

    .prologue
    .line 932125
    if-nez p0, :cond_0

    .line 932126
    const/4 p0, 0x0

    .line 932127
    :goto_0
    return-object p0

    .line 932128
    :cond_0
    instance-of v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;

    if-eqz v0, :cond_1

    .line 932129
    check-cast p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;

    goto :goto_0

    .line 932130
    :cond_1
    new-instance v0, LX/5W9;

    invoke-direct {v0}, LX/5W9;-><init>()V

    .line 932131
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5W9;->a:Ljava/lang/String;

    .line 932132
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 932133
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 932134
    iget-object v3, v0, LX/5W9;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 932135
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 932136
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 932137
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 932138
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 932139
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 932140
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 932141
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 932142
    new-instance v3, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;

    invoke-direct {v3, v2}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;-><init>(LX/15i;)V

    .line 932143
    move-object p0, v3

    .line 932144
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 932119
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 932120
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 932121
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 932122
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 932123
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 932124
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 932116
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 932117
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 932118
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 932114
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;->e:Ljava/lang/String;

    .line 932115
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 932111
    new-instance v0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ItemListModel$SubtitleModel;-><init>()V

    .line 932112
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 932113
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 932110
    const v0, -0x770e8328

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 932109
    const v0, -0x726d476c

    return v0
.end method
