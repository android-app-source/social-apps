.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/5Wm;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6f317bd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$FieldDataListModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 937613
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 937612
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 937610
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 937611
    return-void
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$FieldDataListModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 937608
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$FieldDataListModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->e:Ljava/util/List;

    .line 937609
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 937606
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    .line 937607
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 937566
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->g:Ljava/lang/String;

    .line 937567
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 937604
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$PageModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$PageModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$PageModel;

    .line 937605
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$PageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 937592
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 937593
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 937594
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 937595
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 937596
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$PageModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 937597
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 937598
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 937599
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 937600
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 937601
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 937602
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 937603
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 937574
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 937575
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 937576
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 937577
    if-eqz v1, :cond_3

    .line 937578
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;

    .line 937579
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 937580
    :goto_0
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 937581
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    .line 937582
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->k()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 937583
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;

    .line 937584
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    .line 937585
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 937586
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$PageModel;

    .line 937587
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 937588
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;

    .line 937589
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->h:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$PageModel;

    .line 937590
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 937591
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 937573
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 937570
    new-instance v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;-><init>()V

    .line 937571
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 937572
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 937569
    const v0, -0x6c51c797    # -4.39794E-27f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 937568
    const v0, -0x14e43f95

    return v0
.end method
