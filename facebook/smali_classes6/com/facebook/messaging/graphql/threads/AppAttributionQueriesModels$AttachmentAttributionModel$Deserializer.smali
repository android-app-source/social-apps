.class public final Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 922969
    const-class v0, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 922970
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 922971
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 922972
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 922973
    const/4 v2, 0x0

    .line 922974
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_9

    .line 922975
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 922976
    :goto_0
    move v1, v2

    .line 922977
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 922978
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 922979
    new-instance v1, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AttachmentAttributionModel;-><init>()V

    .line 922980
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 922981
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 922982
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 922983
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 922984
    :cond_0
    return-object v1

    .line 922985
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 922986
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_8

    .line 922987
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 922988
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 922989
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v7, :cond_2

    .line 922990
    const-string p0, "__type__"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 922991
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    goto :goto_1

    .line 922992
    :cond_4
    const-string p0, "attribution_app"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 922993
    invoke-static {p1, v0}, LX/5Tw;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 922994
    :cond_5
    const-string p0, "attribution_app_scoped_ids"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 922995
    invoke-static {p1, v0}, LX/5Tx;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 922996
    :cond_6
    const-string p0, "attribution_metadata"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 922997
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 922998
    :cond_7
    const-string p0, "messaging_attribution"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 922999
    invoke-static {p1, v0}, LX/5Ty;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 923000
    :cond_8
    const/4 v7, 0x5

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 923001
    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 923002
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 923003
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 923004
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 923005
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 923006
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto/16 :goto_1
.end method
