.class public final Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/5Vt;
.implements LX/5Vp;
.implements LX/5Vu;
.implements LX/5Vr;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x107ee194
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel$Serializer;
.end annotation


# instance fields
.field private A:Z

.field private B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private P:Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Q:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:I

.field private S:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private T:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private U:Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private V:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private X:I

.field private Y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Z:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aa:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ab:Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ac:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ad:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ae:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private af:Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ag:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ah:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ai:Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aj:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ak:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private al:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private am:Z

.field private an:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ao:I

.field private ap:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aq:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ar:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/enums/GraphQLCommercePageType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:D

.field private q:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:I

.field private x:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 930696
    const-class v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 930679
    const-class v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 930677
    const/16 v0, 0x42

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 930678
    return-void
.end method

.method private aA()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930675
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->O:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->O:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    .line 930676
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->O:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    return-object v0
.end method

.method private aB()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930673
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->Q:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->Q:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;

    .line 930674
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->Q:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;

    return-object v0
.end method

.method private aC()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930671
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->S:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->S:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    .line 930672
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->S:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    return-object v0
.end method

.method private aD()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessengerExtension"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930669
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->U:Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->U:Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    .line 930670
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->U:Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    return-object v0
.end method

.method private aE()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPendingBookingRequest"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930667
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aa:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;

    const/16 v1, 0x30

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aa:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;

    .line 930668
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aa:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;

    return-object v0
.end method

.method private aF()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930665
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ab:Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ab:Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    .line 930666
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ab:Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    return-object v0
.end method

.method private aG()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930663
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ac:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    const/16 v1, 0x32

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ac:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 930664
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ac:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    return-object v0
.end method

.method private aH()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930661
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ad:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ad:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 930662
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ad:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    return-object v0
.end method

.method private aI()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930633
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ae:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ae:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 930634
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ae:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    return-object v0
.end method

.method private aJ()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930657
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ag:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    const/16 v1, 0x36

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ag:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    .line 930658
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ag:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    return-object v0
.end method

.method private aK()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRequestedBookingRequest"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930655
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ah:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;

    const/16 v1, 0x37

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ah:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;

    .line 930656
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ah:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;

    return-object v0
.end method

.method private aL()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930653
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ai:Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    const/16 v1, 0x38

    const-class v2, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ai:Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    .line 930654
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ai:Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    return-object v0
.end method

.method private aM()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStructuredName"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930651
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aj:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    const/16 v1, 0x39

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aj:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    .line 930652
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aj:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    return-object v0
.end method

.method private aN()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930649
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->al:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    const/16 v1, 0x3b

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->al:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    .line 930650
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->al:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    return-object v0
.end method

.method private aO()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThreadQueueMetadata"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930647
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->an:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;

    const/16 v1, 0x3d

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->an:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;

    .line 930648
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->an:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;

    return-object v0
.end method

.method private aP()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930645
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aq:Ljava/lang/String;

    const/16 v1, 0x40

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aq:Ljava/lang/String;

    .line 930646
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aq:Ljava/lang/String;

    return-object v0
.end method

.method private av()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllParticipants"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930643
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->i:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->i:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    .line 930644
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->i:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    return-object v0
.end method

.method private aw()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getConfirmedBookingRequest"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930641
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->q:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->q:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;

    .line 930642
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->q:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;

    return-object v0
.end method

.method private ax()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930639
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->r:Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->r:Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    .line 930640
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->r:Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    return-object v0
.end method

.method private ay()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930637
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->t:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->t:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    .line 930638
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->t:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    return-object v0
.end method

.method private az()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930635
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->x:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->x:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;

    .line 930636
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->x:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;

    return-object v0
.end method


# virtual methods
.method public final A()Z
    .locals 2

    .prologue
    .line 930725
    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930726
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->A:Z

    return v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930682
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->B:Ljava/lang/String;

    const/16 v1, 0x17

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->B:Ljava/lang/String;

    .line 930683
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final C()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930723
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930724
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->C:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final D()Z
    .locals 2

    .prologue
    .line 930721
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930722
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->D:Z

    return v0
.end method

.method public final E()Z
    .locals 2

    .prologue
    .line 930719
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930720
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->E:Z

    return v0
.end method

.method public final F()Z
    .locals 1

    .prologue
    const/4 v0, 0x3

    .line 930717
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930718
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->F:Z

    return v0
.end method

.method public final G()Z
    .locals 2

    .prologue
    .line 930715
    const/4 v0, 0x3

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930716
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->G:Z

    return v0
.end method

.method public final H()Z
    .locals 2

    .prologue
    .line 930713
    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930714
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->H:Z

    return v0
.end method

.method public final I()Z
    .locals 2

    .prologue
    .line 930711
    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930712
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->I:Z

    return v0
.end method

.method public final J()Z
    .locals 2

    .prologue
    .line 930709
    const/4 v0, 0x3

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930710
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->J:Z

    return v0
.end method

.method public final K()Z
    .locals 2

    .prologue
    .line 930707
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930708
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->K:Z

    return v0
.end method

.method public final L()Z
    .locals 2

    .prologue
    .line 930705
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930706
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->L:Z

    return v0
.end method

.method public final M()Z
    .locals 2

    .prologue
    .line 930727
    const/4 v0, 0x4

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930728
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->M:Z

    return v0
.end method

.method public final N()Z
    .locals 2

    .prologue
    .line 930703
    const/4 v0, 0x4

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930704
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->N:Z

    return v0
.end method

.method public final O()Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930701
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->P:Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->P:Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;

    .line 930702
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->P:Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;

    return-object v0
.end method

.method public final P()I
    .locals 2

    .prologue
    .line 930699
    const/4 v0, 0x4

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930700
    iget v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->R:I

    return v0
.end method

.method public final Q()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 930697
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->T:Ljava/util/List;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->T:Ljava/util/List;

    .line 930698
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->T:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final R()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 930694
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->V:Ljava/util/List;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->V:Ljava/util/List;

    .line 930695
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->V:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final S()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930692
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->W:Ljava/lang/String;

    const/16 v1, 0x2c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->W:Ljava/lang/String;

    .line 930693
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->W:Ljava/lang/String;

    return-object v0
.end method

.method public final T()I
    .locals 1

    .prologue
    const/4 v0, 0x5

    .line 930690
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930691
    iget v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->X:I

    return v0
.end method

.method public final U()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930688
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->Y:Ljava/lang/String;

    const/16 v1, 0x2e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->Y:Ljava/lang/String;

    .line 930689
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->Y:Ljava/lang/String;

    return-object v0
.end method

.method public final V()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageMessengerBot"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930686
    const/4 v0, 0x5

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930687
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->Z:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final W()Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930684
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->af:Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;

    const/16 v1, 0x35

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->af:Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;

    .line 930685
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->af:Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;

    return-object v0
.end method

.method public final X()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThreadGames"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 930659
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ak:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0x3a

    const v4, 0x78deb308

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ak:LX/3Sb;

    .line 930660
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ak:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final Y()Z
    .locals 2

    .prologue
    .line 930680
    const/4 v0, 0x7

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930681
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->am:Z

    return v0
.end method

.method public final Z()I
    .locals 2

    .prologue
    .line 930272
    const/4 v0, 0x7

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930273
    iget v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ao:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 48

    .prologue
    .line 930474
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 930475
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 930476
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->l()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, 0x336d9ff8

    invoke-static {v6, v5, v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 930477
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->m()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, -0x45b151e2

    invoke-static {v7, v6, v8}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 930478
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->av()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 930479
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->q()Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 930480
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->r()LX/0Px;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->c(Ljava/util/List;)I

    move-result v9

    .line 930481
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->s()Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 930482
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aw()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 930483
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ax()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 930484
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->u()LX/1vs;

    move-result-object v13

    iget-object v14, v13, LX/1vs;->a:LX/15i;

    iget v13, v13, LX/1vs;->b:I

    const v15, -0x5f1a9bf8

    invoke-static {v14, v13, v15}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 930485
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ay()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 930486
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->v()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 930487
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->w()LX/0Px;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v16

    .line 930488
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->az()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 930489
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->y()Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    .line 930490
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->B()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 930491
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->C()LX/1vs;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    iget v0, v0, LX/1vs;->b:I

    move/from16 v20, v0

    const v22, 0x11a99cc7

    move-object/from16 v0, v21

    move/from16 v1, v20

    move/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 930492
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aA()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 930493
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->O()Lcom/facebook/graphql/enums/GraphQLMessengerThreadMentionsMuteSettingsMode;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v22

    .line 930494
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aB()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 930495
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aC()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 930496
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->Q()LX/0Px;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v25

    .line 930497
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aD()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 930498
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->R()LX/0Px;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v27

    .line 930499
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->S()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 930500
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->U()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 930501
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->V()LX/1vs;

    move-result-object v30

    move-object/from16 v0, v30

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v31, v0

    move-object/from16 v0, v30

    iget v0, v0, LX/1vs;->b:I

    move/from16 v30, v0

    const v32, 0x7f229591

    move-object/from16 v0, v31

    move/from16 v1, v30

    move/from16 v2, v32

    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 930502
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aE()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 930503
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aF()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 930504
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aG()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 930505
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aH()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 930506
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aI()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 930507
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->W()Lcom/facebook/graphql/enums/GraphQLMessengerThreadReactionsMuteSettingsMode;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v36

    .line 930508
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aJ()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 930509
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aK()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 930510
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aL()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 930511
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aM()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 930512
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->X()LX/2uF;

    move-result-object v41

    move-object/from16 v0, v41

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v41

    .line 930513
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aN()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 930514
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aO()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 930515
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aa()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v44

    .line 930516
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aP()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v45

    .line 930517
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ab()Ljava/lang/String;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v46

    .line 930518
    const/16 v47, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 930519
    const/16 v47, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 930520
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->f:Z

    move/from16 v47, v0

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 930521
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 930522
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 930523
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 930524
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->j:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930525
    const/4 v4, 0x6

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930526
    const/4 v4, 0x7

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->l:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930527
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 930528
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 930529
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 930530
    const/16 v5, 0xb

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->p:D

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 930531
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 930532
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 930533
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 930534
    const/16 v4, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 930535
    const/16 v4, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 930536
    const/16 v4, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930537
    const/16 v4, 0x12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->w:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 930538
    const/16 v4, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930539
    const/16 v4, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930540
    const/16 v5, 0x15

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->z:D

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 930541
    const/16 v4, 0x16

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->A:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930542
    const/16 v4, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930543
    const/16 v4, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930544
    const/16 v4, 0x19

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->D:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930545
    const/16 v4, 0x1a

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->E:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930546
    const/16 v4, 0x1b

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->F:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930547
    const/16 v4, 0x1c

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->G:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930548
    const/16 v4, 0x1d

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->H:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930549
    const/16 v4, 0x1e

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->I:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930550
    const/16 v4, 0x1f

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->J:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930551
    const/16 v4, 0x20

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->K:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930552
    const/16 v4, 0x21

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->L:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930553
    const/16 v4, 0x22

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->M:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930554
    const/16 v4, 0x23

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->N:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930555
    const/16 v4, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930556
    const/16 v4, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930557
    const/16 v4, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930558
    const/16 v4, 0x27

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->R:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 930559
    const/16 v4, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930560
    const/16 v4, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930561
    const/16 v4, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930562
    const/16 v4, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930563
    const/16 v4, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930564
    const/16 v4, 0x2d

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->X:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 930565
    const/16 v4, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930566
    const/16 v4, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930567
    const/16 v4, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930568
    const/16 v4, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930569
    const/16 v4, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930570
    const/16 v4, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930571
    const/16 v4, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930572
    const/16 v4, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930573
    const/16 v4, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930574
    const/16 v4, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930575
    const/16 v4, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930576
    const/16 v4, 0x39

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930577
    const/16 v4, 0x3a

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930578
    const/16 v4, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930579
    const/16 v4, 0x3c

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->am:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 930580
    const/16 v4, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930581
    const/16 v4, 0x3e

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ao:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 930582
    const/16 v4, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930583
    const/16 v4, 0x40

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930584
    const/16 v4, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 930585
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 930586
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    return v4
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 930325
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 930326
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 930327
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x336d9ff8

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 930328
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 930329
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930330
    iput v3, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->g:I

    move-object v1, v0

    .line 930331
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 930332
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x45b151e2

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 930333
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 930334
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930335
    iput v3, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->h:I

    move-object v1, v0

    .line 930336
    :cond_1
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->av()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 930337
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->av()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    .line 930338
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->av()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 930339
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930340
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->i:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    .line 930341
    :cond_2
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aw()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 930342
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aw()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;

    .line 930343
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aw()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 930344
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930345
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->q:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;

    .line 930346
    :cond_3
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ax()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 930347
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ax()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    .line 930348
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ax()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 930349
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930350
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->r:Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    .line 930351
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->u()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 930352
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->u()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x5f1a9bf8

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 930353
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->u()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 930354
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930355
    iput v3, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->s:I

    move-object v1, v0

    .line 930356
    :cond_5
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ay()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 930357
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ay()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    .line 930358
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ay()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 930359
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930360
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->t:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    .line 930361
    :cond_6
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->az()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 930362
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->az()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;

    .line 930363
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->az()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 930364
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930365
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->x:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;

    .line 930366
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->C()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    .line 930367
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->C()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x11a99cc7

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 930368
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->C()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 930369
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930370
    iput v3, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->C:I

    move-object v1, v0

    .line 930371
    :cond_8
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aA()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 930372
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aA()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    .line 930373
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aA()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 930374
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930375
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->O:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    .line 930376
    :cond_9
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aB()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 930377
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aB()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;

    .line 930378
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aB()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 930379
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930380
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->Q:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessagesModel;

    .line 930381
    :cond_a
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aC()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 930382
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aC()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    .line 930383
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aC()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 930384
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930385
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->S:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    .line 930386
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->Q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 930387
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->Q()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 930388
    if-eqz v2, :cond_c

    .line 930389
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930390
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->T:Ljava/util/List;

    move-object v1, v0

    .line 930391
    :cond_c
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aD()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 930392
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aD()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    .line 930393
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aD()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 930394
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930395
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->U:Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    .line 930396
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->R()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 930397
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->R()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 930398
    if-eqz v2, :cond_e

    .line 930399
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930400
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->V:Ljava/util/List;

    move-object v1, v0

    .line 930401
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->V()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_f

    .line 930402
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->V()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7f229591

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 930403
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->V()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_f

    .line 930404
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930405
    iput v3, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->Z:I

    move-object v1, v0

    .line 930406
    :cond_f
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aE()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 930407
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aE()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;

    .line 930408
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aE()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 930409
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930410
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aa:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;

    .line 930411
    :cond_10
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aF()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 930412
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aF()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    .line 930413
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aF()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 930414
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930415
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ab:Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    .line 930416
    :cond_11
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aG()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 930417
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aG()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 930418
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aG()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 930419
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930420
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ac:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 930421
    :cond_12
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aH()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 930422
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aH()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 930423
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aH()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 930424
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930425
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ad:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 930426
    :cond_13
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aI()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 930427
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aI()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 930428
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aI()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 930429
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930430
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ae:Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    .line 930431
    :cond_14
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aJ()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 930432
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aJ()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    .line 930433
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aJ()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 930434
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930435
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ag:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    .line 930436
    :cond_15
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aK()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 930437
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aK()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;

    .line 930438
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aK()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 930439
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930440
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ah:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;

    .line 930441
    :cond_16
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aL()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 930442
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aL()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    .line 930443
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aL()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 930444
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930445
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ai:Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    .line 930446
    :cond_17
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aM()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 930447
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aM()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    .line 930448
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aM()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 930449
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930450
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aj:Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    .line 930451
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->X()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 930452
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->X()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 930453
    if-eqz v2, :cond_19

    .line 930454
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930455
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ak:LX/3Sb;

    move-object v1, v0

    .line 930456
    :cond_19
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aN()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 930457
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aN()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    .line 930458
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aN()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 930459
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930460
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->al:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    .line 930461
    :cond_1a
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aO()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 930462
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aO()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;

    .line 930463
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aO()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 930464
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    .line 930465
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->an:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;

    .line 930466
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 930467
    if-nez v1, :cond_1c

    :goto_0
    return-object p0

    .line 930468
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    .line 930469
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 930470
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0

    .line 930471
    :catchall_3
    move-exception v0

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v0

    .line 930472
    :catchall_4
    move-exception v0

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v0

    :cond_1c
    move-object p0, v1

    .line 930473
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 930324
    new-instance v0, LX/5Vv;

    invoke-direct {v0, p1}, LX/5Vv;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930323
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 930293
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 930294
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->f:Z

    .line 930295
    const/4 v0, 0x2

    const v1, 0x336d9ff8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->g:I

    .line 930296
    const/4 v0, 0x3

    const v1, -0x45b151e2

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->h:I

    .line 930297
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->j:Z

    .line 930298
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->k:Z

    .line 930299
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->l:Z

    .line 930300
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->p:D

    .line 930301
    const/16 v0, 0xe

    const v1, -0x5f1a9bf8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->s:I

    .line 930302
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->w:I

    .line 930303
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->z:D

    .line 930304
    const/16 v0, 0x16

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->A:Z

    .line 930305
    const/16 v0, 0x18

    const v1, 0x11a99cc7

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->C:I

    .line 930306
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->D:Z

    .line 930307
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->E:Z

    .line 930308
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->F:Z

    .line 930309
    const/16 v0, 0x1c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->G:Z

    .line 930310
    const/16 v0, 0x1d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->H:Z

    .line 930311
    const/16 v0, 0x1e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->I:Z

    .line 930312
    const/16 v0, 0x1f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->J:Z

    .line 930313
    const/16 v0, 0x20

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->K:Z

    .line 930314
    const/16 v0, 0x21

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->L:Z

    .line 930315
    const/16 v0, 0x22

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->M:Z

    .line 930316
    const/16 v0, 0x23

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->N:Z

    .line 930317
    const/16 v0, 0x27

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->R:I

    .line 930318
    const/16 v0, 0x2d

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->X:I

    .line 930319
    const/16 v0, 0x2f

    const v1, 0x7f229591

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->Z:I

    .line 930320
    const/16 v0, 0x3c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->am:Z

    .line 930321
    const/16 v0, 0x3e

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ao:I

    .line 930322
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 930291
    invoke-virtual {p2}, LX/18L;->a()V

    .line 930292
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 930290
    return-void
.end method

.method public final aa()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930587
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ap:Ljava/lang/String;

    const/16 v1, 0x3f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ap:Ljava/lang/String;

    .line 930588
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ap:Ljava/lang/String;

    return-object v0
.end method

.method public final ab()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930288
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ar:Ljava/lang/String;

    const/16 v1, 0x41

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ar:Ljava/lang/String;

    .line 930289
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ar:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic ac()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThreadQueueMetadata"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930287
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aO()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ad()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStructuredName"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930286
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aM()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ae()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRequestedBookingRequest"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930285
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aK()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic af()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPendingBookingRequest"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930284
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aE()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ag()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessengerExtension"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930283
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aD()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ah()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getConfirmedBookingRequest"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930282
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aw()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ai()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllParticipants"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930281
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->av()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aj()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930280
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aN()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ak()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930279
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aL()Lcom/facebook/messaging/graphql/threads/RtcCallModels$RtcCallDataInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic al()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930278
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aJ()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ReadReceiptsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic am()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930277
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aI()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic an()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930276
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aH()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ao()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930275
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aG()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ap()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930274
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aF()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aq()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930271
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aC()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ar()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930613
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->aA()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic as()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930632
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->az()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$EventRemindersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic at()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930631
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ay()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$DeliveryReceiptsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic au()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930630
    invoke-direct {p0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->ax()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 930627
    new-instance v0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;-><init>()V

    .line 930628
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 930629
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 930626
    const v0, -0x1c142468

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 930625
    const v0, 0x300255e0

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930622
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 930623
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 930624
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 930620
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930621
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->f:Z

    return v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActiveBots"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930618
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930619
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllParticipantIds"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930616
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930617
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 930614
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930615
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->j:Z

    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 930589
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930590
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->k:Z

    return v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 930611
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930612
    iget-boolean v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->l:Z

    return v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930609
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->m:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->m:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 930610
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->m:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    return-object v0
.end method

.method public final r()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 930607
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->n:Ljava/util/List;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->n:Ljava/util/List;

    .line 930608
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLCommercePageType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930605
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->o:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->o:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 930606
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->o:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    return-object v0
.end method

.method public final t()D
    .locals 2

    .prologue
    .line 930603
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930604
    iget-wide v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->p:D

    return-wide v0
.end method

.method public final u()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCustomizationInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930601
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930602
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->s:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930599
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->u:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->u:Ljava/lang/String;

    .line 930600
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final w()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 930597
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->v:Ljava/util/List;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->v:Ljava/util/List;

    .line 930598
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->v:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final x()I
    .locals 1

    .prologue
    const/4 v0, 0x2

    .line 930595
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930596
    iget v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->w:I

    return v0
.end method

.method public final y()Lcom/facebook/graphql/enums/GraphQLMailboxFolder;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 930593
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->y:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->y:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    .line 930594
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->y:Lcom/facebook/graphql/enums/GraphQLMailboxFolder;

    return-object v0
.end method

.method public final z()D
    .locals 2

    .prologue
    .line 930591
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 930592
    iget-wide v0, p0, Lcom/facebook/messaging/graphql/threads/MessengerSuggestedRecipientsQueryModels$MessengerBroadcastSectionQueryFragmentModel$AttachmentsModel;->z:D

    return-wide v0
.end method
