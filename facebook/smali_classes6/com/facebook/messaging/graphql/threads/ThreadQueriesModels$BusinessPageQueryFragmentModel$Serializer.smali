.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 945516
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 945517
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 945473
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 945474
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 945475
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    const/4 v3, 0x0

    .line 945476
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 945477
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 945478
    if-eqz v2, :cond_0

    .line 945479
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945480
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 945481
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 945482
    if-eqz v2, :cond_1

    .line 945483
    const-string v2, "commerce_page_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945484
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 945485
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 945486
    if-eqz v2, :cond_2

    .line 945487
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945488
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 945489
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 945490
    if-eqz v2, :cond_3

    .line 945491
    const-string v3, "is_messenger_user"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945492
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 945493
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 945494
    if-eqz v2, :cond_4

    .line 945495
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945496
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 945497
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 945498
    if-eqz v2, :cond_5

    .line 945499
    const-string v3, "profile_pic_large"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945500
    invoke-static {v1, v2, p1}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 945501
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 945502
    if-eqz v2, :cond_6

    .line 945503
    const-string v3, "profile_pic_medium"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945504
    invoke-static {v1, v2, p1}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 945505
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 945506
    if-eqz v2, :cond_7

    .line 945507
    const-string v3, "profile_pic_small"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945508
    invoke-static {v1, v2, p1}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 945509
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 945510
    if-eqz v2, :cond_8

    .line 945511
    const-string v3, "username"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 945512
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 945513
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 945514
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 945515
    check-cast p1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
