.class public final Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 932658
    const-class v0, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 932659
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 932619
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 932620
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 932621
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x5

    .line 932622
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 932623
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 932624
    if-eqz v2, :cond_0

    .line 932625
    const-string v3, "click_action"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 932626
    invoke-static {v1, v2, p1}, LX/5WG;->a(LX/15i;ILX/0nX;)V

    .line 932627
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 932628
    if-eqz v2, :cond_1

    .line 932629
    const-string v3, "components"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 932630
    invoke-static {v1, v2, p1, p2}, LX/5WJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 932631
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 932632
    if-eqz v2, :cond_2

    .line 932633
    const-string v3, "is_viewer_seller"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 932634
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 932635
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 932636
    if-eqz v2, :cond_3

    .line 932637
    const-string v3, "item_list"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 932638
    invoke-static {v1, v2, p1, p2}, LX/5WM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 932639
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 932640
    if-eqz v2, :cond_4

    .line 932641
    const-string v3, "payment_call_to_actions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 932642
    invoke-static {v1, v2, p1, p2}, LX/5WO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 932643
    :cond_4
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 932644
    if-eqz v2, :cond_5

    .line 932645
    const-string v2, "payment_modules_client"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 932646
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 932647
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 932648
    if-eqz v2, :cond_6

    .line 932649
    const-string v3, "payment_snippet"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 932650
    invoke-static {v1, v2, p1}, LX/5WP;->a(LX/15i;ILX/0nX;)V

    .line 932651
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 932652
    if-eqz v2, :cond_7

    .line 932653
    const-string v3, "payment_total"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 932654
    invoke-static {v1, v2, p1}, LX/5WQ;->a(LX/15i;ILX/0nX;)V

    .line 932655
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 932656
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 932657
    check-cast p1, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
