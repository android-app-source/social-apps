.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 937541
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 937542
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 937543
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 937544
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 937545
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 937546
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 937547
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 937548
    if-eqz v2, :cond_0

    .line 937549
    const-string p0, "field_data_list"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 937550
    invoke-static {v1, v2, p1, p2}, LX/5Yi;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 937551
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 937552
    if-eqz v2, :cond_1

    .line 937553
    const-string p0, "geocode"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 937554
    invoke-static {v1, v2, p1}, LX/5Yj;->a(LX/15i;ILX/0nX;)V

    .line 937555
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 937556
    if-eqz v2, :cond_2

    .line 937557
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 937558
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 937559
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 937560
    if-eqz v2, :cond_3

    .line 937561
    const-string p0, "page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 937562
    invoke-static {v1, v2, p1}, LX/5Yk;->a(LX/15i;ILX/0nX;)V

    .line 937563
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 937564
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 937565
    check-cast p1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
