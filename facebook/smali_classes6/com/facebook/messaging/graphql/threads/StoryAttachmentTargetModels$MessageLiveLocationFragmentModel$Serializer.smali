.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 935041
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 935042
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 935040
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 935002
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 935003
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 935004
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 935005
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 935006
    if-eqz v2, :cond_0

    .line 935007
    const-string v3, "can_stop_sending_location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935008
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 935009
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 935010
    if-eqz v2, :cond_1

    .line 935011
    const-string v3, "coordinate"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935012
    invoke-static {v1, v2, p1}, LX/5YI;->a(LX/15i;ILX/0nX;)V

    .line 935013
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 935014
    cmp-long v4, v2, v4

    if-eqz v4, :cond_2

    .line 935015
    const-string v4, "expiration_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935016
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 935017
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 935018
    if-eqz v2, :cond_3

    .line 935019
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935020
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 935021
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 935022
    if-eqz v2, :cond_4

    .line 935023
    const-string v3, "location_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935024
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 935025
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 935026
    if-eqz v2, :cond_5

    .line 935027
    const-string v3, "offline_threading_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935028
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 935029
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 935030
    if-eqz v2, :cond_6

    .line 935031
    const-string v3, "sender"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935032
    invoke-static {v1, v2, p1}, LX/5YJ;->a(LX/15i;ILX/0nX;)V

    .line 935033
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 935034
    if-eqz v2, :cond_7

    .line 935035
    const-string v3, "should_show_eta"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 935036
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 935037
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 935038
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 935039
    check-cast p1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
