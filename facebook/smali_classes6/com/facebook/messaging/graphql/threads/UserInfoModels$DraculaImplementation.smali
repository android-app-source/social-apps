.class public final Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 958913
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 958914
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 958978
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 958979
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 958942
    if-nez p1, :cond_0

    move v0, v1

    .line 958943
    :goto_0
    return v0

    .line 958944
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 958945
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 958946
    :sswitch_0
    const-class v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorIdModel$MessagingActorIdOnlyModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorIdModel$MessagingActorIdOnlyModel;

    .line 958947
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 958948
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 958949
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 958950
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 958951
    :sswitch_1
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 958952
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 958953
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 958954
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 958955
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 958956
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 958957
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 958958
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 958959
    invoke-virtual {p3, v5, v3}, LX/186;->b(II)V

    .line 958960
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 958961
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 958962
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 958963
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 958964
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 958965
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 958966
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 958967
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 958968
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 958969
    :sswitch_3
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 958970
    invoke-virtual {p0, p1, v4, v1}, LX/15i;->a(III)I

    move-result v2

    .line 958971
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v3

    .line 958972
    invoke-virtual {p3, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 958973
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 958974
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 958975
    invoke-virtual {p3, v4, v2, v1}, LX/186;->a(III)V

    .line 958976
    invoke-virtual {p3, v5, v3}, LX/186;->b(II)V

    .line 958977
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x58e53f56 -> :sswitch_0
        -0x57708b72 -> :sswitch_1
        0x16e9cb3 -> :sswitch_3
        0x1f8972b0 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 958933
    if-nez p0, :cond_0

    move v0, v1

    .line 958934
    :goto_0
    return v0

    .line 958935
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 958936
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 958937
    :goto_1
    if-ge v1, v2, :cond_2

    .line 958938
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 958939
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 958940
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 958941
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 958926
    const/4 v7, 0x0

    .line 958927
    const/4 v1, 0x0

    .line 958928
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 958929
    invoke-static {v2, v3, v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 958930
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 958931
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 958932
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 958925
    new-instance v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 958920
    if-eqz p0, :cond_0

    .line 958921
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 958922
    if-eq v0, p0, :cond_0

    .line 958923
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 958924
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 958915
    sparse-switch p2, :sswitch_data_0

    .line 958916
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 958917
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorIdModel$MessagingActorIdOnlyModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorIdModel$MessagingActorIdOnlyModel;

    .line 958918
    invoke-static {v0, p3}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 958919
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x58e53f56 -> :sswitch_0
        -0x57708b72 -> :sswitch_1
        0x16e9cb3 -> :sswitch_1
        0x1f8972b0 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 958912
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 958980
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 958981
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 958907
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 958908
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 958909
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;->a:LX/15i;

    .line 958910
    iput p2, p0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;->b:I

    .line 958911
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 958881
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 958906
    new-instance v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 958903
    iget v0, p0, LX/1vt;->c:I

    .line 958904
    move v0, v0

    .line 958905
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 958900
    iget v0, p0, LX/1vt;->c:I

    .line 958901
    move v0, v0

    .line 958902
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 958897
    iget v0, p0, LX/1vt;->b:I

    .line 958898
    move v0, v0

    .line 958899
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 958894
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 958895
    move-object v0, v0

    .line 958896
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 958885
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 958886
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 958887
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 958888
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 958889
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 958890
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 958891
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 958892
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/UserInfoModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 958893
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 958882
    iget v0, p0, LX/1vt;->c:I

    .line 958883
    move v0, v0

    .line 958884
    return v0
.end method
