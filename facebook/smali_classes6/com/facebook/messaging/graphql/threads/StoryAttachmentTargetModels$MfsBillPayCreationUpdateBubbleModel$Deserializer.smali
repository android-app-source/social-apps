.class public final Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 936505
    const-class v0, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 936506
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 936507
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 936508
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 936509
    const/4 v2, 0x0

    .line 936510
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_c

    .line 936511
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 936512
    :goto_0
    move v1, v2

    .line 936513
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 936514
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 936515
    new-instance v1, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel;-><init>()V

    .line 936516
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 936517
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 936518
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 936519
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 936520
    :cond_0
    return-object v1

    .line 936521
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 936522
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_b

    .line 936523
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 936524
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 936525
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 936526
    const-string p0, "account_number"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 936527
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 936528
    :cond_3
    const-string p0, "agent_fee"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 936529
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 936530
    :cond_4
    const-string p0, "agent_name"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 936531
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 936532
    :cond_5
    const-string p0, "amount_due"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 936533
    invoke-static {p1, v0}, LX/5YZ;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 936534
    :cond_6
    const-string p0, "biller_name"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 936535
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 936536
    :cond_7
    const-string p0, "convenience_fee"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 936537
    invoke-static {p1, v0}, LX/5Ya;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 936538
    :cond_8
    const-string p0, "due_date"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 936539
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 936540
    :cond_9
    const-string p0, "pay_by_date"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 936541
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 936542
    :cond_a
    const-string p0, "total_due"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 936543
    invoke-static {p1, v0}, LX/5Yb;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 936544
    :cond_b
    const/16 v11, 0x9

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 936545
    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 936546
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 936547
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 936548
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 936549
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 936550
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 936551
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 936552
    const/4 v2, 0x7

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 936553
    const/16 v2, 0x8

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 936554
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    goto/16 :goto_1
.end method
