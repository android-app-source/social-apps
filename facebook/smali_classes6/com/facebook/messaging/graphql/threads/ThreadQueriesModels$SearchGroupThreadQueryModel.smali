.class public final Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xd8d0d7b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 950267
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 950268
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 950269
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 950270
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 950271
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 950272
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 950273
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 950274
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 950275
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 950276
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 950277
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 950278
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 950279
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;

    .line 950280
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 950281
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;

    .line 950282
    iput-object v0, v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;->e:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;

    .line 950283
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 950284
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSearchResults"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 950285
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;->e:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;

    iput-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;->e:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;

    .line 950286
    iget-object v0, p0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;->e:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel$SearchResultsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 950287
    new-instance v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;

    invoke-direct {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;-><init>()V

    .line 950288
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 950289
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 950290
    const v0, 0x25581c50

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 950291
    const v0, 0x13cda585

    return v0
.end method
