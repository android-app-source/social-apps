.class public final Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 929201
    const-class v0, Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel;

    new-instance v1, Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 929202
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 929142
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 929144
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 929145
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x9

    const/4 v4, 0x0

    .line 929146
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 929147
    invoke-virtual {v1, v0, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 929148
    if-eqz v2, :cond_0

    .line 929149
    const-string v3, "currency"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 929150
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 929151
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 929152
    if-eqz v2, :cond_1

    .line 929153
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 929154
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 929155
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 929156
    if-eqz v2, :cond_2

    .line 929157
    const-string v3, "invoice_notes"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 929158
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 929159
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 929160
    if-eqz v2, :cond_3

    .line 929161
    const-string v3, "page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 929162
    invoke-static {v1, v2, p1}, LX/5Vc;->a(LX/15i;ILX/0nX;)V

    .line 929163
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 929164
    if-eqz v2, :cond_4

    .line 929165
    const-string v3, "platform_context"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 929166
    invoke-static {v1, v2, p1}, LX/5Vd;->a(LX/15i;ILX/0nX;)V

    .line 929167
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 929168
    if-eqz v2, :cond_5

    .line 929169
    const-string v3, "selected_transaction_payment_option"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 929170
    invoke-static {v1, v2, p1}, LX/5Ve;->a(LX/15i;ILX/0nX;)V

    .line 929171
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 929172
    if-eqz v2, :cond_6

    .line 929173
    const-string v3, "transaction_discount"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 929174
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 929175
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 929176
    if-eqz v2, :cond_7

    .line 929177
    const-string v3, "transaction_payment"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 929178
    invoke-static {v1, v2, p1, p2}, LX/5Vg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 929179
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 929180
    if-eqz v2, :cond_8

    .line 929181
    const-string v3, "transaction_products"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 929182
    invoke-static {v1, v2, p1, p2}, LX/5Vl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 929183
    :cond_8
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 929184
    if-eqz v2, :cond_9

    .line 929185
    const-string v2, "transaction_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 929186
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 929187
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 929188
    if-eqz v2, :cond_a

    .line 929189
    const-string v3, "transaction_status_display"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 929190
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 929191
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 929192
    if-eqz v2, :cond_b

    .line 929193
    const-string v3, "transaction_subtotal_cost"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 929194
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 929195
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 929196
    if-eqz v2, :cond_c

    .line 929197
    const-string v3, "transaction_total_cost"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 929198
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 929199
    :cond_c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 929200
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 929143
    check-cast p1, Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel$Serializer;->a(Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
