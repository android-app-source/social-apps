.class public final Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 923809
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 923810
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 923865
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 923866
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 923851
    if-nez p1, :cond_0

    move v0, v1

    .line 923852
    :goto_0
    return v0

    .line 923853
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 923854
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 923855
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 923856
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 923857
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 923858
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 923859
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 923860
    :sswitch_1
    const-class v0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    .line 923861
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 923862
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 923863
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 923864
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1f5d54e8 -> :sswitch_1
        0x7f229591 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 923850
    new-instance v0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 923845
    if-eqz p0, :cond_0

    .line 923846
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 923847
    if-eq v0, p0, :cond_0

    .line 923848
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 923849
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 923840
    sparse-switch p2, :sswitch_data_0

    .line 923841
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 923842
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    .line 923843
    invoke-static {v0, p3}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 923844
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        0x1f5d54e8 -> :sswitch_0
        0x7f229591 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 923839
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 923837
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 923838
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 923867
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 923868
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 923869
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;->a:LX/15i;

    .line 923870
    iput p2, p0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;->b:I

    .line 923871
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 923836
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 923835
    new-instance v0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 923832
    iget v0, p0, LX/1vt;->c:I

    .line 923833
    move v0, v0

    .line 923834
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 923829
    iget v0, p0, LX/1vt;->c:I

    .line 923830
    move v0, v0

    .line 923831
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 923826
    iget v0, p0, LX/1vt;->b:I

    .line 923827
    move v0, v0

    .line 923828
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 923823
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 923824
    move-object v0, v0

    .line 923825
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 923814
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 923815
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 923816
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 923817
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 923818
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 923819
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 923820
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 923821
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/graphql/threads/BotInfoModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 923822
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 923811
    iget v0, p0, LX/1vt;->c:I

    .line 923812
    move v0, v0

    .line 923813
    return v0
.end method
