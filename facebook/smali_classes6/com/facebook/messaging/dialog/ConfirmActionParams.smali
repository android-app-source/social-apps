.class public Lcom/facebook/messaging/dialog/ConfirmActionParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/dialog/ConfirmActionParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/6dz;

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/6dz;

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1117343
    new-instance v0, LX/6dx;

    invoke-direct {v0}, LX/6dx;-><init>()V

    sput-object v0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6dy;)V
    .locals 1

    .prologue
    .line 1117313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1117314
    iget-object v0, p1, LX/6dy;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->a:Ljava/lang/String;

    .line 1117315
    iget-object v0, p1, LX/6dy;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->c:Ljava/lang/String;

    .line 1117316
    iget-object v0, p1, LX/6dy;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->b:Ljava/lang/String;

    .line 1117317
    iget-object v0, p1, LX/6dy;->g:LX/6dz;

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->d:LX/6dz;

    .line 1117318
    iget-object v0, p1, LX/6dy;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->e:Ljava/lang/String;

    .line 1117319
    iget-object v0, p1, LX/6dy;->h:LX/6dz;

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->f:LX/6dz;

    .line 1117320
    iget-object v0, p1, LX/6dy;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->g:Ljava/lang/String;

    .line 1117321
    iget-boolean v0, p1, LX/6dy;->f:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->h:Z

    .line 1117322
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1117333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1117334
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->a:Ljava/lang/String;

    .line 1117335
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->b:Ljava/lang/String;

    .line 1117336
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->c:Ljava/lang/String;

    .line 1117337
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/6dz;

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->d:LX/6dz;

    .line 1117338
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->e:Ljava/lang/String;

    .line 1117339
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/6dz;

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->f:LX/6dz;

    .line 1117340
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->g:Ljava/lang/String;

    .line 1117341
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->h:Z

    .line 1117342
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1117332
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1117323
    iget-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1117324
    iget-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1117325
    iget-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1117326
    iget-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->d:LX/6dz;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1117327
    iget-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1117328
    iget-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->f:LX/6dz;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1117329
    iget-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1117330
    iget-boolean v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->h:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1117331
    return-void
.end method
