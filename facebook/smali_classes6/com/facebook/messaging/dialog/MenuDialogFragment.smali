.class public Lcom/facebook/messaging/dialog/MenuDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Lcom/facebook/messaging/dialog/MenuDialogParams;

.field public n:LX/6e1;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1117353
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1117354
    return-void
.end method

.method public static a(Lcom/facebook/messaging/dialog/MenuDialogParams;)Lcom/facebook/messaging/dialog/MenuDialogFragment;
    .locals 2

    .prologue
    .line 1117355
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1117356
    const-string v1, "menu_dialog_params"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1117357
    new-instance v1, Lcom/facebook/messaging/dialog/MenuDialogFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/dialog/MenuDialogFragment;-><init>()V

    .line 1117358
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1117359
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1117360
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1117361
    if-eqz v0, :cond_0

    .line 1117362
    const-string v2, "menu_dialog_params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/dialog/MenuDialogParams;

    .line 1117363
    iput-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogFragment;->m:Lcom/facebook/messaging/dialog/MenuDialogParams;

    .line 1117364
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogFragment;->m:Lcom/facebook/messaging/dialog/MenuDialogParams;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1117365
    new-instance v3, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 1117366
    iget-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogFragment;->m:Lcom/facebook/messaging/dialog/MenuDialogParams;

    .line 1117367
    iget v2, v0, Lcom/facebook/messaging/dialog/MenuDialogParams;->a:I

    move v0, v2

    .line 1117368
    iget-object v2, p0, Lcom/facebook/messaging/dialog/MenuDialogFragment;->m:Lcom/facebook/messaging/dialog/MenuDialogParams;

    .line 1117369
    iget-object v4, v2, Lcom/facebook/messaging/dialog/MenuDialogParams;->b:Ljava/lang/String;

    move-object v2, v4

    .line 1117370
    if-nez v0, :cond_1

    .line 1117371
    invoke-virtual {v3, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1117372
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogFragment;->m:Lcom/facebook/messaging/dialog/MenuDialogParams;

    .line 1117373
    iget-object v2, v0, Lcom/facebook/messaging/dialog/MenuDialogParams;->c:LX/0Px;

    move-object v0, v2

    .line 1117374
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/CharSequence;

    .line 1117375
    iget-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogFragment;->m:Lcom/facebook/messaging/dialog/MenuDialogParams;

    .line 1117376
    iget-object v2, v0, Lcom/facebook/messaging/dialog/MenuDialogParams;->c:LX/0Px;

    move-object v5, v2

    .line 1117377
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    :goto_1
    if-ge v1, v6, :cond_3

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/dialog/MenuDialogItem;

    .line 1117378
    iget v7, v0, Lcom/facebook/messaging/dialog/MenuDialogItem;->b:I

    move v7, v7

    .line 1117379
    iget-object p1, v0, Lcom/facebook/messaging/dialog/MenuDialogItem;->d:Ljava/lang/CharSequence;

    move-object v0, p1

    .line 1117380
    if-nez v7, :cond_2

    .line 1117381
    aput-object v0, v4, v2

    .line 1117382
    :goto_2
    add-int/lit8 v2, v2, 0x1

    .line 1117383
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1117384
    :cond_1
    invoke-virtual {v3, v0}, LX/0ju;->a(I)LX/0ju;

    goto :goto_0

    .line 1117385
    :cond_2
    invoke-virtual {p0, v7}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    goto :goto_2

    .line 1117386
    :cond_3
    new-instance v0, LX/6e0;

    invoke-direct {v0, p0}, LX/6e0;-><init>(Lcom/facebook/messaging/dialog/MenuDialogFragment;)V

    invoke-virtual {v3, v4, v0}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1117387
    invoke-virtual {v3}, LX/0ju;->b()LX/2EJ;

    move-result-object v0

    return-object v0
.end method
