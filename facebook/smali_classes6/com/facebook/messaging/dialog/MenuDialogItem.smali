.class public Lcom/facebook/messaging/dialog/MenuDialogItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/dialog/MenuDialogItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field private final c:I

.field public final d:Ljava/lang/CharSequence;

.field private final e:Ljava/lang/String;

.field public final f:Landroid/os/Parcelable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1117391
    new-instance v0, LX/6e2;

    invoke-direct {v0}, LX/6e2;-><init>()V

    sput-object v0, Lcom/facebook/messaging/dialog/MenuDialogItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6e3;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1117392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1117393
    iget v0, p1, LX/6e3;->b:I

    move v0, v0

    .line 1117394
    if-nez v0, :cond_0

    move v0, v1

    .line 1117395
    :goto_0
    iget-object v3, p1, LX/6e3;->d:Ljava/lang/CharSequence;

    move-object v3, v3

    .line 1117396
    if-nez v3, :cond_1

    :goto_1
    xor-int/2addr v0, v1

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1117397
    iget v0, p1, LX/6e3;->a:I

    move v0, v0

    .line 1117398
    iput v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->a:I

    .line 1117399
    iget v0, p1, LX/6e3;->b:I

    move v0, v0

    .line 1117400
    iput v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->b:I

    .line 1117401
    iget v0, p1, LX/6e3;->c:I

    move v0, v0

    .line 1117402
    iput v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->c:I

    .line 1117403
    iget-object v0, p1, LX/6e3;->d:Ljava/lang/CharSequence;

    move-object v0, v0

    .line 1117404
    iput-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->d:Ljava/lang/CharSequence;

    .line 1117405
    iget-object v0, p1, LX/6e3;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1117406
    iput-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->e:Ljava/lang/String;

    .line 1117407
    iget-object v0, p1, LX/6e3;->f:Landroid/os/Parcelable;

    move-object v0, v0

    .line 1117408
    iput-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->f:Landroid/os/Parcelable;

    .line 1117409
    return-void

    :cond_0
    move v0, v2

    .line 1117410
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1117411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1117412
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->a:I

    .line 1117413
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->b:I

    .line 1117414
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->c:I

    .line 1117415
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->d:Ljava/lang/CharSequence;

    .line 1117416
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->e:Ljava/lang/String;

    .line 1117417
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->f:Landroid/os/Parcelable;

    .line 1117418
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1117419
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1117420
    iget v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1117421
    iget v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1117422
    iget v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1117423
    iget-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->d:Ljava/lang/CharSequence;

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 1117424
    iget-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1117425
    iget-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogItem;->f:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1117426
    return-void
.end method
