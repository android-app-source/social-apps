.class public abstract Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Lcom/facebook/messaging/dialog/ConfirmActionParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1117271
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    .line 1117275
    iget-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1117276
    iget-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    iget-object v0, v0, Lcom/facebook/messaging/dialog/ConfirmActionParams;->a:Ljava/lang/String;

    .line 1117277
    iget-object v1, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    iget-object v1, v1, Lcom/facebook/messaging/dialog/ConfirmActionParams;->c:Ljava/lang/String;

    .line 1117278
    iget-object v2, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    iget-object v2, v2, Lcom/facebook/messaging/dialog/ConfirmActionParams;->b:Ljava/lang/String;

    .line 1117279
    iget-object v3, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    iget-object v3, v3, Lcom/facebook/messaging/dialog/ConfirmActionParams;->e:Ljava/lang/String;

    .line 1117280
    iget-object v4, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    iget-object v4, v4, Lcom/facebook/messaging/dialog/ConfirmActionParams;->g:Ljava/lang/String;

    .line 1117281
    iget-object v5, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    iget-boolean v5, v5, Lcom/facebook/messaging/dialog/ConfirmActionParams;->h:Z

    .line 1117282
    new-instance v6, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 1117283
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1117284
    invoke-virtual {v6, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1117285
    :goto_0
    new-instance v0, LX/6du;

    invoke-direct {v0, p0}, LX/6du;-><init>(Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;)V

    invoke-virtual {v6, v2, v0}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1117286
    if-eqz v3, :cond_0

    .line 1117287
    new-instance v0, LX/6dv;

    invoke-direct {v0, p0}, LX/6dv;-><init>(Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;)V

    invoke-virtual {v6, v3, v0}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1117288
    :cond_0
    new-instance v0, LX/6dw;

    invoke-direct {v0, p0}, LX/6dw;-><init>(Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;)V

    .line 1117289
    if-eqz v4, :cond_3

    .line 1117290
    invoke-virtual {v6, v4, v0}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1117291
    :cond_1
    :goto_1
    invoke-virtual {v6}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0

    .line 1117292
    :cond_2
    invoke-virtual {v6, v0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1117293
    invoke-virtual {v6, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    goto :goto_0

    .line 1117294
    :cond_3
    if-nez v5, :cond_1

    .line 1117295
    const v1, 0x7f080017

    invoke-virtual {v6, v1, v0}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_1
.end method

.method public a()V
    .locals 0

    .prologue
    .line 1117296
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 1117274
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 1117272
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1117273
    return-void
.end method
