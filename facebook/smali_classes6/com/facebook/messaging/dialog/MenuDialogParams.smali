.class public Lcom/facebook/messaging/dialog/MenuDialogParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/dialog/MenuDialogParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/dialog/MenuDialogItem;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1117432
    new-instance v0, LX/6e4;

    invoke-direct {v0}, LX/6e4;-><init>()V

    sput-object v0, Lcom/facebook/messaging/dialog/MenuDialogParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6e5;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1117450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1117451
    iget v0, p1, LX/6e5;->a:I

    move v0, v0

    .line 1117452
    iput v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->a:I

    .line 1117453
    iget-object v0, p1, LX/6e5;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1117454
    iput-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->b:Ljava/lang/String;

    .line 1117455
    iget-object v0, p1, LX/6e5;->c:Ljava/util/List;

    move-object v0, v0

    .line 1117456
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->c:LX/0Px;

    .line 1117457
    iget-object v0, p1, LX/6e5;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1117458
    iput-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->d:Ljava/lang/Object;

    .line 1117459
    iget v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->a:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->b:Ljava/lang/String;

    if-nez v3, :cond_1

    move v3, v1

    :goto_1
    xor-int/2addr v0, v3

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1117460
    iget-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1117461
    return-void

    :cond_0
    move v0, v2

    .line 1117462
    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v1, v2

    .line 1117463
    goto :goto_2
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 1117441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1117442
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->a:I

    .line 1117443
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->b:Ljava/lang/String;

    .line 1117444
    const-class v0, Lcom/facebook/messaging/dialog/MenuDialogItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 1117445
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->c:LX/0Px;

    .line 1117446
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "extra_data"

    invoke-static {v0, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->d:Ljava/lang/Object;

    .line 1117447
    iget-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->d:Ljava/lang/Object;

    instance-of v0, v0, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 1117448
    iget-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->d:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1117449
    :cond_0
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1117440
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1117433
    iget v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1117434
    iget-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1117435
    iget-object v0, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1117436
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1117437
    const-string v1, "extra_data"

    iget-object v2, p0, Lcom/facebook/messaging/dialog/MenuDialogParams;->d:Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1117438
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 1117439
    return-void
.end method
