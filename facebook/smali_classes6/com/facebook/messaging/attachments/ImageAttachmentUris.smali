.class public Lcom/facebook/messaging/attachments/ImageAttachmentUris;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/attachments/ImageAttachmentUris;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1113981
    new-instance v0, LX/6bq;

    invoke-direct {v0}, LX/6bq;-><init>()V

    sput-object v0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6br;)V
    .locals 1

    .prologue
    .line 1113969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113970
    iget-object v0, p1, LX/6br;->a:Landroid/net/Uri;

    move-object v0, v0

    .line 1113971
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->a:Landroid/net/Uri;

    .line 1113972
    iget-object v0, p1, LX/6br;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 1113973
    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->b:Landroid/net/Uri;

    .line 1113974
    iget-object v0, p1, LX/6br;->c:Landroid/net/Uri;

    move-object v0, v0

    .line 1113975
    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->c:Landroid/net/Uri;

    .line 1113976
    iget-object v0, p1, LX/6br;->d:Landroid/net/Uri;

    move-object v0, v0

    .line 1113977
    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->d:Landroid/net/Uri;

    .line 1113978
    iget-object v0, p1, LX/6br;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 1113979
    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->e:Landroid/net/Uri;

    .line 1113980
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1113943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113944
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->a:Landroid/net/Uri;

    .line 1113945
    iput-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->b:Landroid/net/Uri;

    .line 1113946
    iput-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->c:Landroid/net/Uri;

    .line 1113947
    iput-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->d:Landroid/net/Uri;

    .line 1113948
    iput-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->e:Landroid/net/Uri;

    .line 1113949
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1113962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113963
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->a:Landroid/net/Uri;

    .line 1113964
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->b:Landroid/net/Uri;

    .line 1113965
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->c:Landroid/net/Uri;

    .line 1113966
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->d:Landroid/net/Uri;

    .line 1113967
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->e:Landroid/net/Uri;

    .line 1113968
    return-void
.end method

.method public static newBuilder()LX/6br;
    .locals 1

    .prologue
    .line 1113982
    new-instance v0, LX/6br;

    invoke-direct {v0}, LX/6br;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1113961
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1113957
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    if-nez v1, :cond_1

    .line 1113958
    :cond_0
    :goto_0
    return v0

    .line 1113959
    :cond_1
    check-cast p1, Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    .line 1113960
    iget-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->a:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->a:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->b:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->b:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->c:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->c:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->d:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->d:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->e:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->e:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1113956
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->a:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->b:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->c:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->d:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->e:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1113950
    iget-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1113951
    iget-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1113952
    iget-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1113953
    iget-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1113954
    iget-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->e:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1113955
    return-void
.end method
