.class public Lcom/facebook/messaging/attachments/VideoAttachmentData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/attachments/VideoAttachmentData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/engine/VideoDataSource;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/5dX;

.field public final i:Ljava/lang/String;

.field public final j:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final k:I

.field public final l:I

.field public final m:LX/6bx;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1114057
    new-instance v0, LX/6bw;

    invoke-direct {v0}, LX/6bw;-><init>()V

    sput-object v0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6by;)V
    .locals 1

    .prologue
    .line 1114029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1114030
    iget v0, p1, LX/6by;->b:I

    move v0, v0

    .line 1114031
    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->a:I

    .line 1114032
    iget v0, p1, LX/6by;->c:I

    move v0, v0

    .line 1114033
    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->b:I

    .line 1114034
    iget v0, p1, LX/6by;->d:I

    move v0, v0

    .line 1114035
    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->c:I

    .line 1114036
    iget v0, p1, LX/6by;->e:I

    move v0, v0

    .line 1114037
    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->d:I

    .line 1114038
    iget v0, p1, LX/6by;->f:I

    move v0, v0

    .line 1114039
    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->e:I

    .line 1114040
    iget-object v0, p1, LX/6by;->g:Ljava/util/List;

    move-object v0, v0

    .line 1114041
    iput-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->f:Ljava/util/List;

    .line 1114042
    iget-object v0, p1, LX/6by;->h:Landroid/net/Uri;

    move-object v0, v0

    .line 1114043
    iput-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->g:Landroid/net/Uri;

    .line 1114044
    iget-object v0, p1, LX/6by;->i:LX/5dX;

    move-object v0, v0

    .line 1114045
    iput-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->h:LX/5dX;

    .line 1114046
    iget-object v0, p1, LX/6by;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1114047
    iput-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->i:Ljava/lang/String;

    .line 1114048
    iget-object v0, p1, LX/6by;->k:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v0, v0

    .line 1114049
    iput-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->j:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1114050
    iget v0, p1, LX/6by;->l:I

    move v0, v0

    .line 1114051
    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->k:I

    .line 1114052
    iget v0, p1, LX/6by;->m:I

    move v0, v0

    .line 1114053
    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->l:I

    .line 1114054
    iget-object v0, p1, LX/6by;->a:LX/6bx;

    move-object v0, v0

    .line 1114055
    iput-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->m:LX/6bx;

    .line 1114056
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1114074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1114075
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->a:I

    .line 1114076
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->b:I

    .line 1114077
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->c:I

    .line 1114078
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->d:I

    .line 1114079
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->e:I

    .line 1114080
    const-class v0, Lcom/facebook/video/engine/VideoDataSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->f:Ljava/util/List;

    .line 1114081
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->g:Landroid/net/Uri;

    .line 1114082
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5dX;->valueOf(Ljava/lang/String;)LX/5dX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->h:LX/5dX;

    .line 1114083
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->i:Ljava/lang/String;

    .line 1114084
    const-class v0, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->j:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1114085
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->k:I

    .line 1114086
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->l:I

    .line 1114087
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/6bx;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->m:LX/6bx;

    .line 1114088
    return-void
.end method

.method public static newBuilder()LX/6by;
    .locals 1

    .prologue
    .line 1114073
    new-instance v0, LX/6by;

    invoke-direct {v0}, LX/6by;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/video/engine/VideoDataSource;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1114089
    iget-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1114090
    :goto_0
    return-object v0

    .line 1114091
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 1114092
    iget-object v3, v0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    sget-object v4, LX/097;->FROM_LOCAL_STORAGE:LX/097;

    if-ne v3, v4, :cond_3

    iget-object v3, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    invoke-static {v3}, LX/1be;->b(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1114093
    new-instance v3, Ljava/io/File;

    iget-object v4, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1114094
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .line 1114095
    :cond_3
    if-nez v1, :cond_6

    :goto_2
    move-object v1, v0

    .line 1114096
    goto :goto_1

    .line 1114097
    :cond_4
    if-nez v1, :cond_5

    .line 1114098
    iget-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->f:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_0

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1114072
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1114058
    iget v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1114059
    iget v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1114060
    iget v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1114061
    iget v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1114062
    iget v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1114063
    iget-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1114064
    iget-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->g:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1114065
    iget-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->h:LX/5dX;

    invoke-virtual {v0}, LX/5dX;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1114066
    iget-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1114067
    iget-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->j:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1114068
    iget v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1114069
    iget v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1114070
    iget-object v0, p0, Lcom/facebook/messaging/attachments/VideoAttachmentData;->m:LX/6bx;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1114071
    return-void
.end method
