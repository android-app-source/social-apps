.class public Lcom/facebook/messaging/attachments/ImageAttachmentData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/attachments/ImageAttachmentData;",
            ">;"
        }
    .end annotation
.end field

.field private static final j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

.field public final b:Lcom/facebook/messaging/attachments/ImageAttachmentUris;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:I

.field public final d:I

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Lcom/facebook/ui/media/attachments/MediaResource;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Z

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1113936
    const-class v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;

    sput-object v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->j:Ljava/lang/Class;

    .line 1113937
    new-instance v0, LX/6bo;

    invoke-direct {v0}, LX/6bo;-><init>()V

    sput-object v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6bp;)V
    .locals 1

    .prologue
    .line 1113916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113917
    iget-object v0, p1, LX/6bp;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    move-object v0, v0

    .line 1113918
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    .line 1113919
    iget-object v0, p1, LX/6bp;->b:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    move-object v0, v0

    .line 1113920
    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->b:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    .line 1113921
    iget v0, p1, LX/6bp;->c:I

    move v0, v0

    .line 1113922
    iput v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->c:I

    .line 1113923
    iget v0, p1, LX/6bp;->d:I

    move v0, v0

    .line 1113924
    iput v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->d:I

    .line 1113925
    iget-object v0, p1, LX/6bp;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v0, v0

    .line 1113926
    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1113927
    iget-object v0, p1, LX/6bp;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1113928
    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    .line 1113929
    iget-boolean v0, p1, LX/6bp;->g:Z

    move v0, v0

    .line 1113930
    iput-boolean v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->g:Z

    .line 1113931
    iget-object v0, p1, LX/6bp;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1113932
    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->h:Ljava/lang/String;

    .line 1113933
    iget-object v0, p1, LX/6bp;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1113934
    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->i:Ljava/lang/String;

    .line 1113935
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1113887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113888
    const-class v0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    .line 1113889
    const-class v0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->b:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    .line 1113890
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->c:I

    .line 1113891
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->d:I

    .line 1113892
    const-class v0, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1113893
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    .line 1113894
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->g:Z

    .line 1113895
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->h:Ljava/lang/String;

    .line 1113896
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->i:Ljava/lang/String;

    .line 1113897
    return-void

    .line 1113898
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1113915
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1113911
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/attachments/ImageAttachmentData;

    if-nez v1, :cond_1

    .line 1113912
    :cond_0
    :goto_0
    return v0

    .line 1113913
    :cond_1
    check-cast p1, Lcom/facebook/messaging/attachments/ImageAttachmentData;

    .line 1113914
    iget-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    iget-object v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->b:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    iget-object v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->b:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->c:I

    iget v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->c:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->d:I

    iget v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->d:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->g:Z

    iget-boolean v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->g:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->h:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->h:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->i:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/attachments/ImageAttachmentData;->i:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1113910
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->b:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1113899
    iget-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1113900
    iget-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->b:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1113901
    iget v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1113902
    iget v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1113903
    iget-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1113904
    iget-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113905
    iget-boolean v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1113906
    iget-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113907
    iget-object v0, p0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113908
    return-void

    .line 1113909
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
