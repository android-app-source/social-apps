.class public Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1117644
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1117645
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 1117646
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1117647
    if-eqz v1, :cond_1

    .line 1117648
    const-string v2, "arg_dialog_width"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "arg_dialog_height"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1117649
    if-eqz v2, :cond_2

    .line 1117650
    new-instance v2, LX/6eD;

    const-string p0, "arg_dialog_width"

    invoke-virtual {v1, p0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p0

    const-string p1, "arg_dialog_height"

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-direct {v2, p0, p1, v0}, LX/6eD;-><init>(IILandroid/app/Dialog;)V

    .line 1117651
    :goto_1
    move-object v2, v2

    .line 1117652
    :goto_2
    move-object v0, v2

    .line 1117653
    iget-object v1, v0, LX/6eD;->c:Landroid/app/Dialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 1117654
    iget-object v1, v0, LX/6eD;->c:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 1117655
    iget-object v1, v0, LX/6eD;->c:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 1117656
    const v2, 0x7f0e02bc

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 1117657
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const p0, 0x1010100

    or-int/2addr v2, p0

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1117658
    iget-object v2, v0, LX/6eD;->c:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1117659
    const/4 v2, -0x1

    .line 1117660
    iget v1, v0, LX/6eD;->a:I

    if-eq v1, v2, :cond_0

    iget v1, v0, LX/6eD;->b:I

    if-ne v1, v2, :cond_5

    :cond_0
    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 1117661
    if-eqz v1, :cond_4

    .line 1117662
    :goto_4
    iget-object v1, v0, LX/6eD;->c:Landroid/app/Dialog;

    move-object v0, v1

    .line 1117663
    return-object v0

    :cond_1
    invoke-static {v0}, LX/6eD;->a(Landroid/app/Dialog;)LX/6eD;

    move-result-object v2

    goto :goto_2

    :cond_2
    invoke-static {v0}, LX/6eD;->a(Landroid/app/Dialog;)LX/6eD;

    move-result-object v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 1117664
    :cond_4
    iget-object v1, v0, LX/6eD;->c:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1117665
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 1117666
    iget p0, v0, LX/6eD;->a:I

    iput p0, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1117667
    iget p0, v0, LX/6eD;->b:I

    iput p0, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1117668
    const/16 p0, 0x53

    iput p0, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1117669
    invoke-virtual {v1, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto :goto_4

    :cond_5
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/16 v0, 0x2a

    const v1, -0x7cd47be

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1117670
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1117671
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class p1, Landroid/app/Activity;

    invoke-static {v1, p1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1117672
    if-nez v1, :cond_0

    .line 1117673
    const v1, 0x7f0e02bb

    invoke-virtual {p0, v2, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1117674
    :goto_1
    const v1, 0x569426f2

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 1117675
    :cond_0
    const v1, 0x7f0e02ba

    invoke-virtual {p0, v2, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
