.class public Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:LX/5di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5di",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:F

.field public final c:J

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966199
    new-instance v0, LX/5dr;

    invoke-direct {v0}, LX/5dr;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->CREATOR:LX/5di;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 966227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966228
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->a:Ljava/lang/String;

    .line 966229
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->b:F

    .line 966230
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->c:J

    .line 966231
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->d:Ljava/lang/String;

    .line 966232
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->e:Ljava/lang/String;

    .line 966233
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;FJLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 966220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966221
    iput-object p1, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->a:Ljava/lang/String;

    .line 966222
    iput p2, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->b:F

    .line 966223
    iput-wide p3, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->c:J

    .line 966224
    iput-object p5, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->d:Ljava/lang/String;

    .line 966225
    iput-object p6, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->e:Ljava/lang/String;

    .line 966226
    return-void
.end method


# virtual methods
.method public final a()LX/0m9;
    .locals 4

    .prologue
    .line 966212
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 966213
    const-string v1, "name"

    invoke-virtual {p0}, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->b()LX/5dq;

    move-result-object v2

    iget-object v2, v2, LX/5dq;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966214
    const-string v1, "name"

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966215
    const-string v1, "confidence"

    iget v2, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->b:F

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;F)LX/0m9;

    .line 966216
    const-string v1, "amount"

    iget-wide v2, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 966217
    const-string v1, "currency"

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966218
    const-string v1, "type"

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966219
    return-object v0
.end method

.method public final b()LX/5dq;
    .locals 1

    .prologue
    .line 966234
    sget-object v0, LX/5dq;->P2P_PAYMENT:LX/5dq;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 966211
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 966207
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;

    if-nez v1, :cond_1

    .line 966208
    :cond_0
    :goto_0
    return v0

    .line 966209
    :cond_1
    check-cast p1, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;

    .line 966210
    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->b:F

    iget v2, p1, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->b:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->c:J

    iget-wide v4, p1, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->d:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->e:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 966206
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 966200
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966201
    iget v0, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 966202
    iget-wide v0, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966203
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966204
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966205
    return-void
.end method
