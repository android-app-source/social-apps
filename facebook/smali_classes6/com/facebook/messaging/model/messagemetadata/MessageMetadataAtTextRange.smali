.class public Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;",
            ">;"
        }
    .end annotation
.end field

.field public static e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/5dq;",
            "LX/5di;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/5do;

.field public final b:I

.field public final c:I

.field public final d:Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966144
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->e:LX/0P1;

    .line 966145
    new-instance v0, LX/5dn;

    invoke-direct {v0}, LX/5dn;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5do;IILcom/facebook/messaging/model/messagemetadata/MessageMetadata;)V
    .locals 0

    .prologue
    .line 966146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966147
    iput-object p1, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->a:LX/5do;

    .line 966148
    iput p2, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->b:I

    .line 966149
    iput p3, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->c:I

    .line 966150
    iput-object p4, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->d:Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    .line 966151
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 966138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966139
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/5do;->fromRawValue(I)LX/5do;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->a:LX/5do;

    .line 966140
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->b:I

    .line 966141
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->c:I

    .line 966142
    const-class v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    iput-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->d:Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    .line 966143
    return-void
.end method

.method public static a(LX/0lC;LX/03V;Ljava/lang/String;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lC;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 966152
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 966153
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 966154
    :goto_0
    return-object v0

    .line 966155
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 966156
    const/4 v0, 0x0

    .line 966157
    :try_start_0
    invoke-virtual {p0, p2}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 966158
    :goto_1
    if-eqz v0, :cond_3

    .line 966159
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 966160
    const/4 v3, 0x0

    .line 966161
    const-string v4, "type"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->d(LX/0lF;)I

    move-result v4

    invoke-static {v4}, LX/5do;->fromRawValue(I)LX/5do;

    move-result-object v5

    .line 966162
    const-string v4, "data"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    .line 966163
    const-string v6, "name"

    invoke-virtual {v4, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/5dq;->fromRawValue(Ljava/lang/String;)LX/5dq;

    move-result-object v6

    move-object v6, v6

    .line 966164
    sget-object v7, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->e:LX/0P1;

    if-nez v7, :cond_2

    .line 966165
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v7

    sget-object p0, LX/5dq;->TIMESTAMP:LX/5dq;

    sget-object p2, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;->CREATOR:LX/5di;

    invoke-virtual {v7, p0, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v7

    sget-object p0, LX/5dq;->WATCH_MOVIE:LX/5dq;

    sget-object p2, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;->CREATOR:LX/5di;

    invoke-virtual {v7, p0, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v7

    sget-object p0, LX/5dq;->CREATE_EVENT:LX/5dq;

    sget-object p2, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->CREATOR:LX/5di;

    invoke-virtual {v7, p0, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v7

    sget-object p0, LX/5dq;->GET_RIDE:LX/5dq;

    sget-object p2, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->CREATOR:LX/5di;

    invoke-virtual {v7, p0, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v7

    sget-object p0, LX/5dq;->P2P_PAYMENT:LX/5dq;

    sget-object p2, Lcom/facebook/messaging/model/messagemetadata/P2PPaymentMetadata;->CREATOR:LX/5di;

    invoke-virtual {v7, p0, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v7

    invoke-virtual {v7}, LX/0P2;->b()LX/0P1;

    move-result-object v7

    sput-object v7, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->e:LX/0P1;

    .line 966166
    :cond_2
    sget-object v7, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->e:LX/0P1;

    invoke-virtual {v7, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/5di;

    move-object v7, v7

    .line 966167
    if-eqz v7, :cond_5

    .line 966168
    invoke-interface {v7, v4}, LX/5di;->a(LX/0lF;)Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    move-result-object v4

    .line 966169
    :goto_3
    if-nez v4, :cond_4

    .line 966170
    const-string v4, "MessageMetadataAtTextRange"

    const-string v5, "Could not create metadata for type %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 p0, 0x0

    iget-object v6, v6, LX/5dq;->value:Ljava/lang/String;

    aput-object v6, v7, p0

    invoke-static {v5, v7}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 966171
    :goto_4
    move-object v0, v3

    .line 966172
    if-eqz v0, :cond_1

    .line 966173
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_2

    .line 966174
    :catch_0
    move-exception v2

    .line 966175
    const-string v3, "MessageMetadataAtTextRange"

    const-string v4, "Error while parsing MessageMetadataAtTextRange"

    invoke-virtual {p1, v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 966176
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    new-instance v3, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;

    const-string v6, "offset"

    invoke-virtual {v0, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->d(LX/0lF;)I

    move-result v6

    const-string v7, "length"

    invoke-virtual {v0, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->d(LX/0lF;)I

    move-result v7

    invoke-direct {v3, v5, v6, v7, v4}, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;-><init>(LX/5do;IILcom/facebook/messaging/model/messagemetadata/MessageMetadata;)V

    goto :goto_4

    :cond_5
    move-object v4, v3

    goto :goto_3
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 966137
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 966133
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;

    if-nez v1, :cond_1

    .line 966134
    :cond_0
    :goto_0
    return v0

    .line 966135
    :cond_1
    check-cast p1, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;

    .line 966136
    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->a:LX/5do;

    iget v1, v1, LX/5do;->value:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->a:LX/5do;

    iget v2, v2, LX/5do;->value:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->d:Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    iget-object v2, p1, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->d:Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 966127
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->a:LX/5do;

    iget v2, v2, LX/5do;->value:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->d:Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 966128
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->a:LX/5do;

    iget v0, v0, LX/5do;->value:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 966129
    iget v0, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 966130
    iget v0, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 966131
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->d:Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 966132
    return-void
.end method
