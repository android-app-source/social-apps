.class public Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;
.super Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:LX/5dg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5dg",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 965998
    new-instance v0, LX/5dh;

    invoke-direct {v0}, LX/5dh;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;->CREATOR:LX/5dg;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 965999
    invoke-direct {p0}, Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;-><init>()V

    .line 966000
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;->a:Ljava/lang/String;

    .line 966001
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 966002
    invoke-direct {p0}, Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;-><init>()V

    .line 966003
    iput-object p1, p0, Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;->a:Ljava/lang/String;

    .line 966004
    return-void
.end method


# virtual methods
.method public final a()LX/5ds;
    .locals 1

    .prologue
    .line 966005
    sget-object v0, LX/5ds;->BROADCAST_UNIT_ID:LX/5ds;

    return-object v0
.end method

.method public final b()LX/0lF;
    .locals 2

    .prologue
    .line 966006
    new-instance v0, LX/0mD;

    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, LX/0mD;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final c()LX/0lF;
    .locals 2

    .prologue
    .line 966007
    new-instance v0, LX/0mD;

    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, LX/0mD;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 966008
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/BroadcastUnitIDPlatformMetadata;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966009
    return-void
.end method
