.class public Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:LX/5di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5di",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966457
    new-instance v0, LX/5dx;

    invoke-direct {v0}, LX/5dx;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;->CREATOR:LX/5di;

    return-void
.end method

.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 966454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966455
    iput-wide p1, p0, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;->a:J

    .line 966456
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 966451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966452
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;->a:J

    .line 966453
    return-void
.end method


# virtual methods
.method public final a()LX/0m9;
    .locals 4

    .prologue
    .line 966439
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 966440
    const-string v1, "name"

    invoke-virtual {p0}, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;->b()LX/5dq;

    move-result-object v2

    iget-object v2, v2, LX/5dq;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966441
    const-string v1, "value"

    iget-wide v2, p0, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;->a:J

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 966442
    return-object v0
.end method

.method public final b()LX/5dq;
    .locals 1

    .prologue
    .line 966458
    sget-object v0, LX/5dq;->TIMESTAMP:LX/5dq;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 966450
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 966446
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;

    if-nez v1, :cond_1

    .line 966447
    :cond_0
    :goto_0
    return v0

    .line 966448
    :cond_1
    check-cast p1, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;

    .line 966449
    iget-wide v2, p0, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;->a:J

    iget-wide v4, p1, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 966445
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 966443
    iget-wide v0, p0, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966444
    return-void
.end method
