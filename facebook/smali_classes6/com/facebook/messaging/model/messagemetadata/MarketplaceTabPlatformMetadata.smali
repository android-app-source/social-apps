.class public Lcom/facebook/messaging/model/messagemetadata/MarketplaceTabPlatformMetadata;
.super Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:LX/5dg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5dg",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/MarketplaceTabPlatformMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966108
    new-instance v0, LX/5dm;

    invoke-direct {v0}, LX/5dm;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messagemetadata/MarketplaceTabPlatformMetadata;->CREATOR:LX/5dg;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 966101
    invoke-direct {p0}, Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;-><init>()V

    .line 966102
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/model/messagemetadata/MarketplaceTabPlatformMetadata;->a:Z

    .line 966103
    return-void

    .line 966104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 966105
    invoke-direct {p0}, Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;-><init>()V

    .line 966106
    iput-boolean p1, p0, Lcom/facebook/messaging/model/messagemetadata/MarketplaceTabPlatformMetadata;->a:Z

    .line 966107
    return-void
.end method


# virtual methods
.method public final a()LX/5ds;
    .locals 1

    .prologue
    .line 966109
    sget-object v0, LX/5ds;->MARKETPLACE_TAB_MESSAGE:LX/5ds;

    return-object v0
.end method

.method public final b()LX/0lF;
    .locals 1

    .prologue
    .line 966096
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messagemetadata/MarketplaceTabPlatformMetadata;->a:Z

    invoke-static {v0}, LX/1Xb;->b(Z)LX/1Xb;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0lF;
    .locals 1

    .prologue
    .line 966097
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messagemetadata/MarketplaceTabPlatformMetadata;->a:Z

    invoke-static {v0}, LX/1Xb;->b(Z)LX/1Xb;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 966098
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messagemetadata/MarketplaceTabPlatformMetadata;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 966099
    return-void

    .line 966100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
