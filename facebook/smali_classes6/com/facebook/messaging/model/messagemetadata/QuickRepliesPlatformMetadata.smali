.class public Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;
.super Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:LX/5dg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5dg",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966327
    new-instance v0, LX/5du;

    invoke-direct {v0}, LX/5du;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->CREATOR:LX/5dg;

    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 966341
    invoke-direct {p0}, Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;-><init>()V

    .line 966342
    iput-object p1, p0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->a:LX/0Px;

    .line 966343
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 966338
    invoke-direct {p0}, Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;-><init>()V

    .line 966339
    const-class v0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->a:LX/0Px;

    .line 966340
    return-void
.end method


# virtual methods
.method public final a()LX/5ds;
    .locals 1

    .prologue
    .line 966344
    sget-object v0, LX/5ds;->QUICK_REPLIES:LX/5ds;

    return-object v0
.end method

.method public final b()LX/0lF;
    .locals 4

    .prologue
    .line 966333
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 966334
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;

    .line 966335
    invoke-virtual {v0}, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->a()LX/0m9;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/162;->a(LX/0lF;)LX/162;

    .line 966336
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 966337
    :cond_0
    return-object v2
.end method

.method public final c()LX/0lF;
    .locals 2

    .prologue
    .line 966330
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 966331
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 966332
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->a:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->a()LX/0m9;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 966328
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 966329
    return-void
.end method
