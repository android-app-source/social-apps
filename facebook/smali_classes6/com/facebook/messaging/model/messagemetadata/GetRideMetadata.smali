.class public Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:LX/5di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5di",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:F

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966042
    new-instance v0, LX/5dk;

    invoke-direct {v0}, LX/5dk;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->CREATOR:LX/5di;

    return-void
.end method

.method public constructor <init>(FLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 966043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966044
    iput p1, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->a:F

    .line 966045
    iput-object p2, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->b:Ljava/lang/String;

    .line 966046
    iput-object p3, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->c:Ljava/lang/String;

    .line 966047
    iput-object p4, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->d:Ljava/lang/String;

    .line 966048
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 966049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966050
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->a:F

    .line 966051
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->b:Ljava/lang/String;

    .line 966052
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->c:Ljava/lang/String;

    .line 966053
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->d:Ljava/lang/String;

    .line 966054
    return-void
.end method


# virtual methods
.method public final a()LX/0m9;
    .locals 3

    .prologue
    .line 966055
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 966056
    const-string v1, "name"

    invoke-virtual {p0}, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->b()LX/5dq;

    move-result-object v2

    iget-object v2, v2, LX/5dq;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966057
    const-string v1, "confidence"

    iget v2, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->a:F

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;F)LX/0m9;

    .line 966058
    const-string v1, "method"

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966059
    const-string v1, "provider"

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966060
    const-string v1, "to_location"

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966061
    return-object v0
.end method

.method public final b()LX/5dq;
    .locals 1

    .prologue
    .line 966062
    sget-object v0, LX/5dq;->GET_RIDE:LX/5dq;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 966063
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 966064
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;

    if-nez v1, :cond_1

    .line 966065
    :cond_0
    :goto_0
    return v0

    .line 966066
    :cond_1
    check-cast p1, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;

    .line 966067
    iget v1, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->a:F

    iget v2, p1, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->a:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->d:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 966068
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->a:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 966069
    iget v0, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 966070
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966071
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966072
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/GetRideMetadata;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966073
    return-void
.end method
