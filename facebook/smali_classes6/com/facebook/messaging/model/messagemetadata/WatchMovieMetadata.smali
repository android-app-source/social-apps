.class public Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:LX/5di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5di",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966482
    new-instance v0, LX/5dy;

    invoke-direct {v0}, LX/5dy;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;->CREATOR:LX/5di;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 966479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966480
    iput p1, p0, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;->a:I

    .line 966481
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 966476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966477
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;->a:I

    .line 966478
    return-void
.end method


# virtual methods
.method public final a()LX/0m9;
    .locals 3

    .prologue
    .line 966472
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 966473
    const-string v1, "name"

    invoke-virtual {p0}, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;->b()LX/5dq;

    move-result-object v2

    iget-object v2, v2, LX/5dq;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966474
    const-string v1, "confidence"

    iget v2, p0, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;->a:I

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 966475
    return-object v0
.end method

.method public final b()LX/5dq;
    .locals 1

    .prologue
    .line 966463
    sget-object v0, LX/5dq;->WATCH_MOVIE:LX/5dq;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 966471
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 966467
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;

    if-nez v1, :cond_1

    .line 966468
    :cond_0
    :goto_0
    return v0

    .line 966469
    :cond_1
    check-cast p1, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;

    .line 966470
    iget v1, p0, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;->a:I

    iget v2, p1, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;->a:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 966466
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 966464
    iget v0, p0, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 966465
    return-void
.end method
