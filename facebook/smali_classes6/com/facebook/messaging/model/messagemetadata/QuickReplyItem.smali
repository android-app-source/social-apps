.class public Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/5dw;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/0lF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966434
    new-instance v0, LX/5dv;

    invoke-direct {v0}, LX/5dv;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/5dw;Ljava/lang/String;Ljava/lang/String;LX/0lF;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/5dw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 966427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966428
    iput-object p1, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->a:Ljava/lang/String;

    .line 966429
    iput-object p2, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->b:LX/5dw;

    .line 966430
    iput-object p3, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->c:Ljava/lang/String;

    .line 966431
    iput-object p4, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->d:Ljava/lang/String;

    .line 966432
    iput-object p5, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->e:LX/0lF;

    .line 966433
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0lF;)Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;
    .locals 6
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 966422
    if-nez p0, :cond_0

    .line 966423
    const/4 v0, 0x0

    .line 966424
    :goto_0
    return-object v0

    .line 966425
    :cond_0
    invoke-static {p1}, LX/5dw;->fromDbValue(Ljava/lang/String;)LX/5dw;

    move-result-object v2

    .line 966426
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;-><init>(Ljava/lang/String;LX/5dw;Ljava/lang/String;Ljava/lang/String;LX/0lF;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0m9;
    .locals 3

    .prologue
    .line 966386
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 966387
    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 966388
    const-string v1, "title"

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966389
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->b:LX/5dw;

    if-eqz v1, :cond_1

    .line 966390
    const-string v1, "content_type"

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->b:LX/5dw;

    iget-object v2, v2, LX/5dw;->dbValue:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966391
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 966392
    const-string v1, "payload"

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966393
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 966394
    const-string v1, "image_url"

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966395
    :cond_3
    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->e:LX/0lF;

    if-eqz v1, :cond_4

    .line 966396
    const-string v1, "data"

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->e:LX/0lF;

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 966397
    :cond_4
    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 966421
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 966417
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;

    if-nez v1, :cond_1

    .line 966418
    :cond_0
    :goto_0
    return v0

    .line 966419
    :cond_1
    check-cast p1, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;

    .line 966420
    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->b:LX/5dw;

    iget-object v2, p1, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->b:LX/5dw;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->d:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->e:LX/0lF;

    iget-object v2, p1, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->e:LX/0lF;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 966416
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->b:LX/5dw;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->e:LX/0lF;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    .line 966398
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966399
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->b:LX/5dw;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966400
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966401
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966402
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->e:LX/0lF;

    .line 966403
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 966404
    instance-of v1, v0, LX/0m9;

    if-nez v1, :cond_1

    move-object v1, v2

    .line 966405
    :goto_1
    move-object v0, v1

    .line 966406
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 966407
    return-void

    .line 966408
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->b:LX/5dw;

    iget-object v0, v0, LX/5dw;->dbValue:Ljava/lang/String;

    goto :goto_0

    .line 966409
    :cond_1
    invoke-virtual {v0}, LX/0lF;->j()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 966410
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 966411
    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    .line 966412
    if-eqz v4, :cond_2

    invoke-virtual {v4}, LX/0lF;->q()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v4}, LX/0lF;->m()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v4}, LX/0lF;->o()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 966413
    :cond_2
    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 966414
    :cond_3
    const-string v1, "QuickReplyItem::convertToBundle"

    const-string v5, "Unexpected value type: %s"

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    invoke-virtual {v4}, LX/0lF;->k()LX/0nH;

    move-result-object v4

    aput-object v4, p0, p2

    invoke-static {v1, v5, p0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_4
    move-object v1, v2

    .line 966415
    goto :goto_1
.end method
