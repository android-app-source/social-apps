.class public Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:LX/5di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5di",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:F

.field public final b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966014
    new-instance v0, LX/5dj;

    invoke-direct {v0}, LX/5dj;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->CREATOR:LX/5di;

    return-void
.end method

.method public constructor <init>(FJ)V
    .locals 0

    .prologue
    .line 966015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966016
    iput p1, p0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->a:F

    .line 966017
    iput-wide p2, p0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->b:J

    .line 966018
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 966019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966020
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->a:F

    .line 966021
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->b:J

    .line 966022
    return-void
.end method


# virtual methods
.method public final a()LX/0m9;
    .locals 4

    .prologue
    .line 966023
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 966024
    const-string v1, "name"

    invoke-virtual {p0}, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->b()LX/5dq;

    move-result-object v2

    iget-object v2, v2, LX/5dq;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 966025
    const-string v1, "confidence"

    iget v2, p0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->a:F

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;F)LX/0m9;

    .line 966026
    const-string v1, "timestamp"

    iget-wide v2, p0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 966027
    return-object v0
.end method

.method public final b()LX/5dq;
    .locals 1

    .prologue
    .line 966028
    sget-object v0, LX/5dq;->CREATE_EVENT:LX/5dq;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 966029
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 966030
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;

    if-nez v1, :cond_1

    .line 966031
    :cond_0
    :goto_0
    return v0

    .line 966032
    :cond_1
    check-cast p1, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;

    .line 966033
    iget v1, p0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->a:F

    iget v2, p1, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->a:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->b:J

    iget-wide v4, p1, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 966034
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->a:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 966035
    iget v0, p0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 966036
    iget-wide v0, p0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966037
    return-void
.end method
