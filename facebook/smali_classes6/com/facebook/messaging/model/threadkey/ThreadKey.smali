.class public Lcom/facebook/messaging/model/threadkey/ThreadKey;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/5e9;

.field public final b:J

.field public final c:J

.field public final d:J

.field public final e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966760
    new-instance v0, LX/5e7;

    invoke-direct {v0}, LX/5e7;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(LX/5e9;JJJJ)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 966761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966762
    iput-object p1, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    .line 966763
    iput-wide p2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    .line 966764
    iput-wide p4, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    .line 966765
    iput-wide p6, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    .line 966766
    iput-wide p8, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c:J

    .line 966767
    sget-object v0, LX/5e8;->a:[I

    invoke-virtual {p1}, LX/5e9;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 966768
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported ThreadKey type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 966769
    :pswitch_0
    cmp-long v0, p2, v4

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966770
    cmp-long v0, p4, v4

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966771
    cmp-long v0, p6, v4

    if-lez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966772
    cmp-long v0, p8, v4

    if-nez v0, :cond_3

    :goto_3
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 966773
    :goto_4
    return-void

    :cond_0
    move v0, v2

    .line 966774
    goto :goto_0

    :cond_1
    move v0, v2

    .line 966775
    goto :goto_1

    :cond_2
    move v0, v2

    .line 966776
    goto :goto_2

    :cond_3
    move v1, v2

    .line 966777
    goto :goto_3

    .line 966778
    :pswitch_1
    cmp-long v0, p2, v4

    if-lez v0, :cond_4

    move v0, v1

    :goto_5
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966779
    cmp-long v0, p4, v4

    if-nez v0, :cond_5

    move v0, v1

    :goto_6
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966780
    cmp-long v0, p6, v4

    if-nez v0, :cond_6

    move v0, v1

    :goto_7
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966781
    cmp-long v0, p8, v4

    if-nez v0, :cond_7

    :goto_8
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    goto :goto_4

    :cond_4
    move v0, v2

    .line 966782
    goto :goto_5

    :cond_5
    move v0, v2

    .line 966783
    goto :goto_6

    :cond_6
    move v0, v2

    .line 966784
    goto :goto_7

    :cond_7
    move v1, v2

    .line 966785
    goto :goto_8

    .line 966786
    :pswitch_2
    cmp-long v0, p2, v4

    if-nez v0, :cond_8

    move v0, v1

    :goto_9
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966787
    cmp-long v0, p4, v4

    if-lez v0, :cond_9

    move v0, v1

    :goto_a
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966788
    cmp-long v0, p6, v4

    if-lez v0, :cond_a

    move v0, v1

    :goto_b
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966789
    cmp-long v0, p8, v4

    if-nez v0, :cond_b

    :goto_c
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    goto :goto_4

    :cond_8
    move v0, v2

    .line 966790
    goto :goto_9

    :cond_9
    move v0, v2

    .line 966791
    goto :goto_a

    :cond_a
    move v0, v2

    .line 966792
    goto :goto_b

    :cond_b
    move v1, v2

    .line 966793
    goto :goto_c

    .line 966794
    :pswitch_3
    cmp-long v0, p2, v4

    if-lez v0, :cond_c

    move v0, v1

    :goto_d
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966795
    cmp-long v0, p4, p2

    if-nez v0, :cond_d

    move v0, v1

    :goto_e
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966796
    cmp-long v0, p6, v4

    if-lez v0, :cond_e

    move v0, v1

    :goto_f
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966797
    cmp-long v0, p8, v4

    if-nez v0, :cond_f

    :goto_10
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    goto :goto_4

    :cond_c
    move v0, v2

    .line 966798
    goto :goto_d

    :cond_d
    move v0, v2

    .line 966799
    goto :goto_e

    :cond_e
    move v0, v2

    .line 966800
    goto :goto_f

    :cond_f
    move v1, v2

    .line 966801
    goto :goto_10

    .line 966802
    :pswitch_4
    cmp-long v0, p2, v4

    if-nez v0, :cond_10

    move v0, v1

    :goto_11
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966803
    cmp-long v0, p4, v4

    if-nez v0, :cond_11

    move v0, v1

    :goto_12
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966804
    cmp-long v0, p6, v4

    if-nez v0, :cond_12

    move v0, v1

    :goto_13
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966805
    cmp-long v0, p8, v4

    if-lez v0, :cond_13

    :goto_14
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    goto/16 :goto_4

    :cond_10
    move v0, v2

    .line 966806
    goto :goto_11

    :cond_11
    move v0, v2

    .line 966807
    goto :goto_12

    :cond_12
    move v0, v2

    .line 966808
    goto :goto_13

    :cond_13
    move v1, v2

    .line 966809
    goto :goto_14

    .line 966810
    :pswitch_5
    cmp-long v0, p4, v4

    if-nez v0, :cond_14

    move v0, v1

    :goto_15
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966811
    cmp-long v0, p6, v4

    if-nez v0, :cond_15

    move v0, v1

    :goto_16
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966812
    cmp-long v0, p8, v4

    if-nez v0, :cond_16

    :goto_17
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    goto/16 :goto_4

    :cond_14
    move v0, v2

    .line 966813
    goto :goto_15

    :cond_15
    move v0, v2

    .line 966814
    goto :goto_16

    :cond_16
    move v1, v2

    .line 966815
    goto :goto_17

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public synthetic constructor <init>(LX/5e9;JJJJB)V
    .locals 0

    .prologue
    .line 966816
    invoke-direct/range {p0 .. p9}, Lcom/facebook/messaging/model/threadkey/ThreadKey;-><init>(LX/5e9;JJJJ)V

    return-void
.end method

.method public static a()Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 10

    .prologue
    const-wide/16 v2, -0x1

    .line 966817
    new-instance v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v1, LX/5e9;->PENDING_MY_MONTAGE:LX/5e9;

    const-wide/16 v8, 0x0

    move-wide v4, v2

    move-wide v6, v2

    invoke-direct/range {v0 .. v9}, Lcom/facebook/messaging/model/threadkey/ThreadKey;-><init>(LX/5e9;JJJJ)V

    return-object v0
.end method

.method public static a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 10

    .prologue
    const-wide/16 v4, -0x1

    .line 966721
    new-instance v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v1, LX/5e9;->GROUP:LX/5e9;

    move-wide v2, p0

    move-wide v6, v4

    move-wide v8, v4

    invoke-direct/range {v0 .. v9}, Lcom/facebook/messaging/model/threadkey/ThreadKey;-><init>(LX/5e9;JJJJ)V

    return-object v0
.end method

.method public static a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 10

    .prologue
    const-wide/16 v2, -0x1

    .line 966818
    new-instance v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    move-wide v4, p0

    move-wide v6, p2

    move-wide v8, v2

    invoke-direct/range {v0 .. v9}, Lcom/facebook/messaging/model/threadkey/ThreadKey;-><init>(LX/5e9;JJJJ)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    .line 966738
    if-nez p0, :cond_1

    .line 966739
    :cond_0
    :goto_0
    return-object v0

    .line 966740
    :cond_1
    const-string v1, ":"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 966741
    array-length v2, v1

    if-lez v2, :cond_0

    .line 966742
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, LX/5e9;->fromDbValue(Ljava/lang/String;)LX/5e9;

    move-result-object v2

    .line 966743
    :try_start_0
    sget-object v3, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v2, v3, :cond_2

    array-length v3, v1

    if-ne v3, v5, :cond_2

    .line 966744
    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v4, 0x2

    aget-object v1, v1, v4

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0

    .line 966745
    :cond_2
    sget-object v3, LX/5e9;->GROUP:LX/5e9;

    if-ne v2, v3, :cond_3

    array-length v3, v1

    if-ne v3, v4, :cond_3

    .line 966746
    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0

    .line 966747
    :cond_3
    sget-object v3, LX/5e9;->TINCAN:LX/5e9;

    if-ne v2, v3, :cond_4

    array-length v3, v1

    if-ne v3, v5, :cond_4

    .line 966748
    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v4, 0x2

    aget-object v1, v1, v4

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0

    .line 966749
    :cond_4
    sget-object v3, LX/5e9;->TINCAN_MULTI_ENDPOINT:LX/5e9;

    if-ne v2, v3, :cond_7

    array-length v3, v1

    if-eq v3, v4, :cond_5

    array-length v3, v1

    if-ne v3, v5, :cond_7

    .line 966750
    :cond_5
    array-length v2, v1

    if-ne v2, v5, :cond_6

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 966751
    :goto_1
    const/4 v4, 0x1

    aget-object v1, v1, v4

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5, v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0

    .line 966752
    :cond_6
    const-wide/16 v2, 0x0

    goto :goto_1

    .line 966753
    :cond_7
    sget-object v3, LX/5e9;->PENDING_THREAD:LX/5e9;

    if-ne v2, v3, :cond_8

    array-length v3, v1

    if-ne v3, v4, :cond_8

    .line 966754
    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto/16 :goto_0

    .line 966755
    :cond_8
    sget-object v3, LX/5e9;->PENDING_MY_MONTAGE:LX/5e9;

    if-ne v2, v3, :cond_9

    .line 966756
    invoke-static {}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto/16 :goto_0

    .line 966757
    :cond_9
    sget-object v3, LX/5e9;->SMS:LX/5e9;

    if-ne v2, v3, :cond_0

    array-length v2, v1

    if-ne v2, v4, :cond_0

    .line 966758
    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    :catch_0
    goto/16 :goto_0
.end method

.method public static a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/user/model/UserKey;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 966819
    if-eqz p0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-eq v0, v1, :cond_0

    invoke-static {p0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 966820
    :cond_0
    iget-wide v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    .line 966821
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 10

    .prologue
    const-wide/16 v4, -0x1

    .line 966822
    new-instance v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v1, LX/5e9;->SMS:LX/5e9;

    move-wide v2, p0

    move-wide v6, v4

    move-wide v8, v4

    invoke-direct/range {v0 .. v9}, Lcom/facebook/messaging/model/threadkey/ThreadKey;-><init>(LX/5e9;JJJJ)V

    return-object v0
.end method

.method public static b(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 10

    .prologue
    const-wide/16 v2, -0x1

    .line 966823
    new-instance v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v1, LX/5e9;->TINCAN:LX/5e9;

    move-wide v4, p0

    move-wide v6, p2

    move-wide v8, v2

    invoke-direct/range {v0 .. v9}, Lcom/facebook/messaging/model/threadkey/ThreadKey;-><init>(LX/5e9;JJJJ)V

    return-object v0
.end method

.method public static b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 2
    .param p0    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 966824
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 10

    .prologue
    const-wide/16 v2, -0x1

    .line 966825
    new-instance v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v1, LX/5e9;->PENDING_THREAD:LX/5e9;

    move-wide v4, v2

    move-wide v6, v2

    move-wide v8, p0

    invoke-direct/range {v0 .. v9}, Lcom/facebook/messaging/model/threadkey/ThreadKey;-><init>(LX/5e9;JJJJ)V

    return-object v0
.end method

.method public static c(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 10

    .prologue
    .line 966826
    new-instance v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v1, LX/5e9;->TINCAN_MULTI_ENDPOINT:LX/5e9;

    const-wide/16 v8, -0x1

    move-wide v2, p0

    move-wide v4, p0

    move-wide v6, p2

    invoke-direct/range {v0 .. v9}, Lcom/facebook/messaging/model/threadkey/ThreadKey;-><init>(LX/5e9;JJJJ)V

    return-object v0
.end method

.method public static c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 2
    .param p0    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 966827
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->GROUP:LX/5e9;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 2
    .param p0    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 966828
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->SMS:LX/5e9;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 4
    .param p0    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 966829
    invoke-static {p0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v0

    const-wide/16 v2, -0x64

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 2
    .param p0    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 966759
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->PENDING_THREAD:LX/5e9;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 2
    .param p0    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 966686
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->PENDING_MY_MONTAGE:LX/5e9;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 1
    .param p0    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 966687
    invoke-static {p0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->f(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->g(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 2
    .param p0    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 966688
    if-eqz p0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->TINCAN:LX/5e9;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->TINCAN_MULTI_ENDPOINT:LX/5e9;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 966689
    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 966690
    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->GROUP:LX/5e9;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 4

    .prologue
    .line 966691
    invoke-virtual {p0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 966692
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 966693
    if-ne p0, p1, :cond_1

    .line 966694
    :cond_0
    :goto_0
    return v0

    .line 966695
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 966696
    :cond_3
    check-cast p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 966697
    iget-object v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    iget-object v3, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 966698
    :cond_4
    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    iget-wide v4, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    .line 966699
    :cond_5
    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    iget-wide v4, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    .line 966700
    :cond_6
    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    iget-wide v4, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    .line 966701
    :cond_7
    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c:J

    iget-wide v4, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 966702
    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->TINCAN:LX/5e9;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->TINCAN_MULTI_ENDPOINT:LX/5e9;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 4

    .prologue
    .line 966703
    sget-object v0, LX/5e8;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v1}, LX/5e9;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 966704
    const-string v0, "UNKNOWN_TYPE"

    :goto_0
    return-object v0

    .line 966705
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    iget-object v1, v1, LX/5e9;->dbValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 966706
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    iget-object v1, v1, LX/5e9;->dbValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 966707
    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    iget-object v1, v1, LX/5e9;->dbValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 966708
    :pswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    iget-object v1, v1, LX/5e9;->dbValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 966709
    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v0}, LX/5e9;->hashCode()I

    move-result v0

    .line 966710
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    iget-wide v4, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 966711
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    iget-wide v4, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 966712
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    iget-wide v4, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 966713
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c:J

    iget-wide v4, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 966714
    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 6

    .prologue
    .line 966715
    sget-object v0, LX/5e8;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v1}, LX/5e9;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 966716
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 966717
    :pswitch_0
    iget-wide v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 966718
    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    iget-wide v4, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 966719
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "u"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 966720
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "g"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 966722
    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v0, v1, :cond_0

    .line 966723
    iget-wide v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    .line 966724
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    goto :goto_0
.end method

.method public final k()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x3f

    .line 966725
    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v0}, LX/5e9;->hashCode()I

    move-result v0

    int-to-long v0, v0

    .line 966726
    mul-long/2addr v0, v4

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    add-long/2addr v0, v2

    .line 966727
    mul-long/2addr v0, v4

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    add-long/2addr v0, v2

    .line 966728
    mul-long/2addr v0, v4

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    add-long/2addr v0, v2

    .line 966729
    mul-long/2addr v0, v4

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c:J

    add-long/2addr v0, v2

    .line 966730
    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 966731
    invoke-virtual {p0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 966732
    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v0}, LX/5e9;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966733
    iget-wide v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966734
    iget-wide v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966735
    iget-wide v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966736
    iget-wide v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966737
    return-void
.end method
