.class public Lcom/facebook/messaging/model/attachment/ImageData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/attachment/ImageData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

.field public final d:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

.field public final e:LX/5dT;

.field public final f:Z

.field public final g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 965690
    new-instance v0, LX/5dS;

    invoke-direct {v0}, LX/5dS;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/attachment/ImageData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IILcom/facebook/messaging/model/attachment/AttachmentImageMap;Lcom/facebook/messaging/model/attachment/AttachmentImageMap;LX/5dT;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 965691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965692
    iput p1, p0, Lcom/facebook/messaging/model/attachment/ImageData;->a:I

    .line 965693
    iput p2, p0, Lcom/facebook/messaging/model/attachment/ImageData;->b:I

    .line 965694
    iput-object p3, p0, Lcom/facebook/messaging/model/attachment/ImageData;->c:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    .line 965695
    iput-object p4, p0, Lcom/facebook/messaging/model/attachment/ImageData;->d:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    .line 965696
    iput-object p5, p0, Lcom/facebook/messaging/model/attachment/ImageData;->e:LX/5dT;

    .line 965697
    iput-boolean p6, p0, Lcom/facebook/messaging/model/attachment/ImageData;->f:Z

    .line 965698
    iput-object p7, p0, Lcom/facebook/messaging/model/attachment/ImageData;->g:Ljava/lang/String;

    .line 965699
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 965700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965701
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->a:I

    .line 965702
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->b:I

    .line 965703
    const-class v0, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->c:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    .line 965704
    const-class v0, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->d:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    .line 965705
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5dT;->valueOf(Ljava/lang/String;)LX/5dT;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->e:LX/5dT;

    .line 965706
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->f:Z

    .line 965707
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->g:Ljava/lang/String;

    .line 965708
    return-void

    .line 965709
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 965710
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 965711
    iget v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 965712
    iget v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 965713
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->c:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 965714
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->d:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 965715
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->e:LX/5dT;

    invoke-virtual {v0}, LX/5dT;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965716
    iget-boolean v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 965717
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965718
    return-void

    .line 965719
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
