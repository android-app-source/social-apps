.class public Lcom/facebook/messaging/model/attachment/AudioData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/attachment/AudioData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 965659
    new-instance v0, LX/5dR;

    invoke-direct {v0}, LX/5dR;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/attachment/AudioData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 965660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965661
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/model/attachment/AudioData;->a:Z

    .line 965662
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/AudioData;->b:Ljava/lang/String;

    .line 965663
    return-void

    .line 965664
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 965665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965666
    iput-boolean p1, p0, Lcom/facebook/messaging/model/attachment/AudioData;->a:Z

    .line 965667
    iput-object p2, p0, Lcom/facebook/messaging/model/attachment/AudioData;->b:Ljava/lang/String;

    .line 965668
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 965669
    iget-boolean v0, p0, Lcom/facebook/messaging/model/attachment/AudioData;->a:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 965670
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 965671
    iget-boolean v0, p0, Lcom/facebook/messaging/model/attachment/AudioData;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 965672
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/AudioData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965673
    return-void

    .line 965674
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
