.class public Lcom/facebook/messaging/model/attachment/AttachmentImageMap;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/attachment/AttachmentImageMap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/5dQ;",
            "Lcom/facebook/messaging/model/attachment/ImageUrl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 965625
    new-instance v0, LX/5dO;

    invoke-direct {v0}, LX/5dO;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5dP;)V
    .locals 1

    .prologue
    .line 965621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965622
    iget-object v0, p1, LX/5dP;->a:Ljava/util/Map;

    move-object v0, v0

    .line 965623
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a:LX/0P1;

    .line 965624
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 965618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965619
    const-class v0, LX/5dQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a:LX/0P1;

    .line 965620
    return-void
.end method

.method public static newBuilder()LX/5dP;
    .locals 1

    .prologue
    .line 965617
    new-instance v0, LX/5dP;

    invoke-direct {v0}, LX/5dP;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(LX/5dQ;)Lcom/facebook/messaging/model/attachment/ImageUrl;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 965616
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/ImageUrl;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 965615
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 965613
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 965614
    return-void
.end method
