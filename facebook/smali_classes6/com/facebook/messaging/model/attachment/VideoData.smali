.class public Lcom/facebook/messaging/model/attachment/VideoData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/attachment/VideoData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:LX/5dX;

.field public f:Landroid/net/Uri;

.field public g:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 965817
    new-instance v0, LX/5dW;

    invoke-direct {v0}, LX/5dW;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/attachment/VideoData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIILX/5dX;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 0
    .param p7    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 965808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965809
    iput p1, p0, Lcom/facebook/messaging/model/attachment/VideoData;->a:I

    .line 965810
    iput p2, p0, Lcom/facebook/messaging/model/attachment/VideoData;->b:I

    .line 965811
    iput p3, p0, Lcom/facebook/messaging/model/attachment/VideoData;->c:I

    .line 965812
    iput p4, p0, Lcom/facebook/messaging/model/attachment/VideoData;->d:I

    .line 965813
    iput-object p5, p0, Lcom/facebook/messaging/model/attachment/VideoData;->e:LX/5dX;

    .line 965814
    iput-object p6, p0, Lcom/facebook/messaging/model/attachment/VideoData;->f:Landroid/net/Uri;

    .line 965815
    iput-object p7, p0, Lcom/facebook/messaging/model/attachment/VideoData;->g:Landroid/net/Uri;

    .line 965816
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 965799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965800
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->a:I

    .line 965801
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->b:I

    .line 965802
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->c:I

    .line 965803
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->d:I

    .line 965804
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5dX;->valueOf(Ljava/lang/String;)LX/5dX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->e:LX/5dX;

    .line 965805
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->f:Landroid/net/Uri;

    .line 965806
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->g:Landroid/net/Uri;

    .line 965807
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 965798
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 965790
    iget v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 965791
    iget v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 965792
    iget v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 965793
    iget v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 965794
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->e:LX/5dX;

    invoke-virtual {v0}, LX/5dX;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965795
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->f:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 965796
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/VideoData;->g:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 965797
    return-void
.end method
