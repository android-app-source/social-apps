.class public Lcom/facebook/messaging/model/attachment/Attachment;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/attachment/Attachment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:Lcom/facebook/messaging/model/attachment/ImageData;

.field public final h:Lcom/facebook/messaging/model/attachment/VideoData;

.field public final i:Lcom/facebook/messaging/model/attachment/AudioData;

.field public final j:Ljava/lang/String;

.field public final k:[B

.field public final l:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 965591
    new-instance v0, LX/5dL;

    invoke-direct {v0}, LX/5dL;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/attachment/Attachment;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5dN;)V
    .locals 1

    .prologue
    .line 965565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965566
    iget-object v0, p1, LX/5dN;->a:Ljava/lang/String;

    move-object v0, v0

    .line 965567
    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->a:Ljava/lang/String;

    .line 965568
    iget-object v0, p1, LX/5dN;->b:Ljava/lang/String;

    move-object v0, v0

    .line 965569
    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->b:Ljava/lang/String;

    .line 965570
    iget-object v0, p1, LX/5dN;->c:Ljava/lang/String;

    move-object v0, v0

    .line 965571
    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->c:Ljava/lang/String;

    .line 965572
    iget-object v0, p1, LX/5dN;->d:Ljava/lang/String;

    move-object v0, v0

    .line 965573
    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->d:Ljava/lang/String;

    .line 965574
    iget-object v0, p1, LX/5dN;->e:Ljava/lang/String;

    move-object v0, v0

    .line 965575
    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->e:Ljava/lang/String;

    .line 965576
    iget v0, p1, LX/5dN;->f:I

    move v0, v0

    .line 965577
    iput v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->f:I

    .line 965578
    iget-object v0, p1, LX/5dN;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    move-object v0, v0

    .line 965579
    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    .line 965580
    iget-object v0, p1, LX/5dN;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    move-object v0, v0

    .line 965581
    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 965582
    iget-object v0, p1, LX/5dN;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    move-object v0, v0

    .line 965583
    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    .line 965584
    iget-object v0, p1, LX/5dN;->j:Ljava/lang/String;

    move-object v0, v0

    .line 965585
    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->j:Ljava/lang/String;

    .line 965586
    iget-object v0, p1, LX/5dN;->k:[B

    move-object v0, v0

    .line 965587
    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->k:[B

    .line 965588
    iget-object v0, p1, LX/5dN;->l:Ljava/lang/String;

    move-object v0, v0

    .line 965589
    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->l:Ljava/lang/String;

    .line 965590
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 965537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965538
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->a:Ljava/lang/String;

    .line 965539
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->b:Ljava/lang/String;

    .line 965540
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->c:Ljava/lang/String;

    .line 965541
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->d:Ljava/lang/String;

    .line 965542
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->e:Ljava/lang/String;

    .line 965543
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->f:I

    .line 965544
    const-class v0, Lcom/facebook/messaging/model/attachment/ImageData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/ImageData;

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    .line 965545
    const-class v0, Lcom/facebook/messaging/model/attachment/VideoData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/VideoData;

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 965546
    const-class v0, Lcom/facebook/messaging/model/attachment/AudioData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/AudioData;

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    .line 965547
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->j:Ljava/lang/String;

    .line 965548
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->k:[B

    .line 965549
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->l:Ljava/lang/String;

    .line 965550
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 965564
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 965551
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965552
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965553
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965554
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965555
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965556
    iget v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 965557
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 965558
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 965559
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 965560
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965561
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->k:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 965562
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965563
    return-void
.end method
