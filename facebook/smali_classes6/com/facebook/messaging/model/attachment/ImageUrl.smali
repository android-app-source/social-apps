.class public Lcom/facebook/messaging/model/attachment/ImageUrl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/attachment/ImageUrl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 965744
    new-instance v0, LX/5dV;

    invoke-direct {v0}, LX/5dV;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/attachment/ImageUrl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5dM;)V
    .locals 1

    .prologue
    .line 965760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965761
    iget v0, p1, LX/5dM;->a:I

    move v0, v0

    .line 965762
    iput v0, p0, Lcom/facebook/messaging/model/attachment/ImageUrl;->a:I

    .line 965763
    iget v0, p1, LX/5dM;->b:I

    move v0, v0

    .line 965764
    iput v0, p0, Lcom/facebook/messaging/model/attachment/ImageUrl;->b:I

    .line 965765
    iget-object v0, p1, LX/5dM;->c:Ljava/lang/String;

    move-object v0, v0

    .line 965766
    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/ImageUrl;->c:Ljava/lang/String;

    .line 965767
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 965754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965755
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/attachment/ImageUrl;->a:I

    .line 965756
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/attachment/ImageUrl;->b:I

    .line 965757
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attachment/ImageUrl;->c:Ljava/lang/String;

    .line 965758
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 965759
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 965749
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 965750
    iget v1, p0, Lcom/facebook/messaging/model/attachment/ImageUrl;->a:I

    move v1, v1

    .line 965751
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 965752
    iget v1, p0, Lcom/facebook/messaging/model/attachment/ImageUrl;->b:I

    move v1, v1

    .line 965753
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 965745
    iget v0, p0, Lcom/facebook/messaging/model/attachment/ImageUrl;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 965746
    iget v0, p0, Lcom/facebook/messaging/model/attachment/ImageUrl;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 965747
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/ImageUrl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965748
    return-void
.end method
