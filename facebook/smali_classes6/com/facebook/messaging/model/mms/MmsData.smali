.class public Lcom/facebook/messaging/model/mms/MmsData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/mms/MmsData;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/model/mms/MmsData;

.field private static final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:J

.field public final c:J

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const-wide/16 v2, -0x1

    .line 966512
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 966513
    sput-object v0, Lcom/facebook/messaging/model/mms/MmsData;->e:LX/0Px;

    .line 966514
    new-instance v1, Lcom/facebook/messaging/model/mms/MmsData;

    sget-object v6, Lcom/facebook/messaging/model/mms/MmsData;->e:LX/0Px;

    move-wide v4, v2

    invoke-direct/range {v1 .. v6}, Lcom/facebook/messaging/model/mms/MmsData;-><init>(JJLX/0Px;)V

    sput-object v1, Lcom/facebook/messaging/model/mms/MmsData;->a:Lcom/facebook/messaging/model/mms/MmsData;

    .line 966515
    new-instance v0, LX/5dz;

    invoke-direct {v0}, LX/5dz;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/mms/MmsData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(JJLX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 966507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966508
    iput-wide p1, p0, Lcom/facebook/messaging/model/mms/MmsData;->b:J

    .line 966509
    iput-wide p3, p0, Lcom/facebook/messaging/model/mms/MmsData;->c:J

    .line 966510
    iput-object p5, p0, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    .line 966511
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 966501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966502
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/mms/MmsData;->b:J

    .line 966503
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/mms/MmsData;->c:J

    .line 966504
    const-class v0, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    .line 966505
    return-void
.end method

.method public static a(JJ)Lcom/facebook/messaging/model/mms/MmsData;
    .locals 8

    .prologue
    .line 966506
    new-instance v1, Lcom/facebook/messaging/model/mms/MmsData;

    sget-object v6, Lcom/facebook/messaging/model/mms/MmsData;->e:LX/0Px;

    move-wide v2, p0

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/facebook/messaging/model/mms/MmsData;-><init>(JJLX/0Px;)V

    return-object v1
.end method

.method public static a(LX/0Px;)Lcom/facebook/messaging/model/mms/MmsData;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)",
            "Lcom/facebook/messaging/model/mms/MmsData;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 966493
    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966494
    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 966495
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    move v4, v2

    :goto_2
    if-ge v2, v1, :cond_2

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 966496
    int-to-long v4, v4

    iget-wide v6, v0, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    add-long/2addr v4, v6

    long-to-int v0, v4

    .line 966497
    add-int/lit8 v2, v2, 0x1

    move v4, v0

    goto :goto_2

    :cond_0
    move v0, v2

    .line 966498
    goto :goto_0

    :cond_1
    move v1, v2

    .line 966499
    goto :goto_1

    .line 966500
    :cond_2
    new-instance v1, Lcom/facebook/messaging/model/mms/MmsData;

    const-wide/16 v2, 0x0

    int-to-long v4, v4

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/facebook/messaging/model/mms/MmsData;-><init>(JJLX/0Px;)V

    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    .line 966491
    iget-wide v1, p0, Lcom/facebook/messaging/model/mms/MmsData;->b:J

    sget-object v3, Lcom/facebook/messaging/model/mms/MmsData;->a:Lcom/facebook/messaging/model/mms/MmsData;

    iget-wide v3, v3, Lcom/facebook/messaging/model/mms/MmsData;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    iget-wide v1, p0, Lcom/facebook/messaging/model/mms/MmsData;->c:J

    sget-object v3, Lcom/facebook/messaging/model/mms/MmsData;->a:Lcom/facebook/messaging/model/mms/MmsData;

    iget-wide v3, v3, Lcom/facebook/messaging/model/mms/MmsData;->c:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 966492
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 966486
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 966487
    iget-wide v0, p0, Lcom/facebook/messaging/model/mms/MmsData;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966488
    iget-wide v0, p0, Lcom/facebook/messaging/model/mms/MmsData;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966489
    iget-object v0, p0, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 966490
    return-void
.end method
