.class public Lcom/facebook/messaging/model/payment/PaymentTransactionData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/payment/PaymentTransactionData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:J

.field public final d:I

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966560
    new-instance v0, LX/5e3;

    invoke-direct {v0}, LX/5e3;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 966561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966562
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->a:Ljava/lang/String;

    .line 966563
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->b:J

    .line 966564
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->c:J

    .line 966565
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->d:I

    .line 966566
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->e:Ljava/lang/String;

    .line 966567
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JJILjava/lang/String;)V
    .locals 0

    .prologue
    .line 966568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966569
    iput-object p1, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->a:Ljava/lang/String;

    .line 966570
    iput-wide p2, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->b:J

    .line 966571
    iput-wide p4, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->c:J

    .line 966572
    iput p6, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->d:I

    .line 966573
    iput-object p7, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->e:Ljava/lang/String;

    .line 966574
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 966575
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 966576
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966577
    iget-wide v0, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966578
    iget-wide v0, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966579
    iget v0, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 966580
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966581
    return-void
.end method
