.class public final Lcom/facebook/messaging/model/payment/ComposePaymentParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/payment/ComposePaymentParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/payment/SentPayment;

.field public final b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final c:Lcom/facebook/payments/auth/model/NuxFollowUpAction;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966519
    new-instance v0, LX/5e0;

    invoke-direct {v0}, LX/5e0;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/payment/ComposePaymentParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 966520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966521
    const-class v0, Lcom/facebook/messaging/model/payment/SentPayment;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/payment/SentPayment;

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/ComposePaymentParams;->a:Lcom/facebook/messaging/model/payment/SentPayment;

    .line 966522
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/ComposePaymentParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 966523
    const-class v0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/ComposePaymentParams;->c:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    .line 966524
    return-void
.end method

.method public static newBuilder()LX/5e1;
    .locals 1

    .prologue
    .line 966525
    new-instance v0, LX/5e1;

    invoke-direct {v0}, LX/5e1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 966526
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 966527
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/ComposePaymentParams;->a:Lcom/facebook/messaging/model/payment/SentPayment;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 966528
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/ComposePaymentParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 966529
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/ComposePaymentParams;->c:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 966530
    return-void
.end method
