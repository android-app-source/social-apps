.class public Lcom/facebook/messaging/model/payment/SentPayment;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/payment/SentPayment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/currency/CurrencyAmount;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Z

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:LX/5g0;

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966631
    new-instance v0, LX/5e4;

    invoke-direct {v0}, LX/5e4;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/payment/SentPayment;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5e5;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 966599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966600
    iget-object v0, p1, LX/5e5;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, v0

    .line 966601
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 966602
    iget-wide v8, p1, LX/5e5;->b:J

    move-wide v4, v8

    .line 966603
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 966604
    iget-object v0, p1, LX/5e5;->c:Ljava/lang/String;

    move-object v0, v0

    .line 966605
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 966606
    iget-object v0, p1, LX/5e5;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, v0

    .line 966607
    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 966608
    iget-wide v8, p1, LX/5e5;->b:J

    move-wide v0, v8

    .line 966609
    iput-wide v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->b:J

    .line 966610
    iget-object v0, p1, LX/5e5;->c:Ljava/lang/String;

    move-object v0, v0

    .line 966611
    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->c:Ljava/lang/String;

    .line 966612
    iget-object v0, p1, LX/5e5;->d:Ljava/lang/String;

    move-object v0, v0

    .line 966613
    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->d:Ljava/lang/String;

    .line 966614
    iget-object v0, p1, LX/5e5;->e:Ljava/lang/String;

    move-object v0, v0

    .line 966615
    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->e:Ljava/lang/String;

    .line 966616
    iget-object v0, p1, LX/5e5;->f:Ljava/lang/String;

    move-object v0, v0

    .line 966617
    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->f:Ljava/lang/String;

    .line 966618
    iget-boolean v0, p1, LX/5e5;->g:Z

    move v0, v0

    .line 966619
    iput-boolean v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->g:Z

    .line 966620
    iget-object v0, p1, LX/5e5;->h:Ljava/lang/String;

    move-object v0, v0

    .line 966621
    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->h:Ljava/lang/String;

    .line 966622
    iget-object v0, p1, LX/5e5;->i:LX/5g0;

    move-object v0, v0

    .line 966623
    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->i:LX/5g0;

    .line 966624
    iget-object v0, p1, LX/5e5;->j:Ljava/lang/String;

    move-object v0, v0

    .line 966625
    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->j:Ljava/lang/String;

    .line 966626
    iget-object v0, p1, LX/5e5;->k:Ljava/lang/String;

    move-object v0, v0

    .line 966627
    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->k:Ljava/lang/String;

    .line 966628
    return-void

    :cond_0
    move v0, v2

    .line 966629
    goto :goto_0

    :cond_1
    move v1, v2

    .line 966630
    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 966632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966633
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 966634
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->b:J

    .line 966635
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->c:Ljava/lang/String;

    .line 966636
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->d:Ljava/lang/String;

    .line 966637
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->e:Ljava/lang/String;

    .line 966638
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->f:Ljava/lang/String;

    .line 966639
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->g:Z

    .line 966640
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->h:Ljava/lang/String;

    .line 966641
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5g0;

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->i:LX/5g0;

    .line 966642
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->j:Ljava/lang/String;

    .line 966643
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->k:Ljava/lang/String;

    .line 966644
    return-void
.end method

.method public static newBuilder()LX/5e5;
    .locals 1

    .prologue
    .line 966585
    new-instance v0, LX/5e5;

    invoke-direct {v0}, LX/5e5;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 966586
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 966587
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 966588
    iget-wide v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966589
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966590
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966591
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966592
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966593
    iget-boolean v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 966594
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966595
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->i:LX/5g0;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 966596
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966597
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/SentPayment;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966598
    return-void
.end method
