.class public Lcom/facebook/messaging/model/payment/PaymentRequestData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/payment/PaymentRequestData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:J

.field public final d:I

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966556
    new-instance v0, LX/5e2;

    invoke-direct {v0}, LX/5e2;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 966549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966550
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->a:Ljava/lang/String;

    .line 966551
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->b:J

    .line 966552
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->c:J

    .line 966553
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->d:I

    .line 966554
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->e:Ljava/lang/String;

    .line 966555
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JJILjava/lang/String;)V
    .locals 0

    .prologue
    .line 966542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966543
    iput-object p1, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->a:Ljava/lang/String;

    .line 966544
    iput-wide p2, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->b:J

    .line 966545
    iput-wide p4, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->c:J

    .line 966546
    iput p6, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->d:I

    .line 966547
    iput-object p7, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->e:Ljava/lang/String;

    .line 966548
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 966535
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 966536
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966537
    iget-wide v0, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966538
    iget-wide v0, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 966539
    iget v0, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 966540
    iget-object v0, p0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 966541
    return-void
.end method
