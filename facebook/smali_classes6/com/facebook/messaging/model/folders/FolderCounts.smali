.class public Lcom/facebook/messaging/model/folders/FolderCounts;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/folders/FolderCounts;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/model/folders/FolderCounts;


# instance fields
.field public final b:I

.field public final c:I

.field public final d:J

.field public final e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 1118108
    new-instance v1, Lcom/facebook/messaging/model/folders/FolderCounts;

    move v3, v2

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/model/folders/FolderCounts;-><init>(IIJJ)V

    sput-object v1, Lcom/facebook/messaging/model/folders/FolderCounts;->a:Lcom/facebook/messaging/model/folders/FolderCounts;

    .line 1118109
    new-instance v0, LX/6ej;

    invoke-direct {v0}, LX/6ej;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/folders/FolderCounts;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIJJ)V
    .locals 1

    .prologue
    .line 1118110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118111
    iput p1, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->b:I

    .line 1118112
    iput p2, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->c:I

    .line 1118113
    iput-wide p3, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->d:J

    .line 1118114
    iput-wide p5, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->e:J

    .line 1118115
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1118116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118117
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->b:I

    .line 1118118
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->c:I

    .line 1118119
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->d:J

    .line 1118120
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->e:J

    .line 1118121
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1118122
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1118123
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "unseen"

    iget v2, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->c:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "unread"

    iget v2, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->b:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "lastActionId"

    iget-wide v2, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    const-string v1, "lastSeenTime"

    iget-wide v2, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->d:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1118124
    iget v0, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118125
    iget v0, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118126
    iget-wide v0, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1118127
    iget-wide v0, p0, Lcom/facebook/messaging/model/folders/FolderCounts;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1118128
    return-void
.end method
