.class public Lcom/facebook/messaging/model/attribution/ContentAppAttribution;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/attribution/ContentAppAttribution;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

.field public final i:LX/5dc;

.field public final j:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 965946
    new-instance v0, LX/5da;

    invoke-direct {v0}, LX/5da;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5dd;)V
    .locals 1

    .prologue
    .line 965919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965920
    iget-object v0, p1, LX/5dd;->a:Ljava/lang/String;

    move-object v0, v0

    .line 965921
    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->a:Ljava/lang/String;

    .line 965922
    iget-object v0, p1, LX/5dd;->b:Ljava/lang/String;

    move-object v0, v0

    .line 965923
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    .line 965924
    iget-object v0, p1, LX/5dd;->c:Ljava/lang/String;

    move-object v0, v0

    .line 965925
    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->c:Ljava/lang/String;

    .line 965926
    iget-object v0, p1, LX/5dd;->d:Ljava/lang/String;

    move-object v0, v0

    .line 965927
    if-eqz v0, :cond_0

    .line 965928
    iget-object v0, p1, LX/5dd;->d:Ljava/lang/String;

    move-object v0, v0

    .line 965929
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->d:Ljava/lang/String;

    .line 965930
    iget-object v0, p1, LX/5dd;->e:Ljava/lang/String;

    move-object v0, v0

    .line 965931
    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->e:Ljava/lang/String;

    .line 965932
    iget-object v0, p1, LX/5dd;->f:Ljava/lang/String;

    move-object v0, v0

    .line 965933
    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->f:Ljava/lang/String;

    .line 965934
    iget-object v0, p1, LX/5dd;->i:LX/0P1;

    move-object v0, v0

    .line 965935
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->g:LX/0P1;

    .line 965936
    iget-object v0, p1, LX/5dd;->j:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    move-object v0, v0

    .line 965937
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->h:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    .line 965938
    iget-object v0, p1, LX/5dd;->g:LX/5dc;

    if-eqz v0, :cond_1

    .line 965939
    iget-object v0, p1, LX/5dd;->g:LX/5dc;

    .line 965940
    :goto_1
    move-object v0, v0

    .line 965941
    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->i:LX/5dc;

    .line 965942
    iget-object v0, p1, LX/5dd;->h:Ljava/lang/String;

    move-object v0, v0

    .line 965943
    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->j:Ljava/lang/String;

    .line 965944
    return-void

    .line 965945
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v0, LX/5dc;->UNRECOGNIZED:LX/5dc;

    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 965905
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965906
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->a:Ljava/lang/String;

    .line 965907
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    .line 965908
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->c:Ljava/lang/String;

    .line 965909
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->d:Ljava/lang/String;

    .line 965910
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->e:Ljava/lang/String;

    .line 965911
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->f:Ljava/lang/String;

    .line 965912
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 965913
    invoke-static {p1, v0}, LX/46R;->b(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 965914
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->g:LX/0P1;

    .line 965915
    const-class v0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->h:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    .line 965916
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/5dc;->fromValue(I)LX/5dc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->i:LX/5dc;

    .line 965917
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->j:Ljava/lang/String;

    .line 965918
    return-void
.end method

.method public static newBuilder()LX/5dd;
    .locals 1

    .prologue
    .line 965904
    new-instance v0, LX/5dd;

    invoke-direct {v0}, LX/5dd;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 965903
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 965892
    iget-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965893
    iget-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965894
    iget-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965895
    iget-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965896
    iget-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965897
    iget-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965898
    iget-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->g:LX/0P1;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 965899
    iget-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->h:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 965900
    iget-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->i:LX/5dc;

    invoke-virtual {v0}, LX/5dc;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 965901
    iget-object v0, p0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 965902
    return-void
.end method
