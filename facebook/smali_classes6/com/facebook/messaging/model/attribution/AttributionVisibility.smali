.class public Lcom/facebook/messaging/model/attribution/AttributionVisibility;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/attribution/AttributionVisibility;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

.field public static final b:Lcom/facebook/messaging/model/attribution/AttributionVisibility;


# instance fields
.field public final c:Z

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 965821
    invoke-static {}, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->newBuilder()LX/5dZ;

    move-result-object v0

    const/4 v1, 0x0

    .line 965822
    iput-boolean v1, v0, LX/5dZ;->a:Z

    .line 965823
    iput-boolean v1, v0, LX/5dZ;->b:Z

    .line 965824
    iput-boolean v1, v0, LX/5dZ;->c:Z

    .line 965825
    iput-boolean v1, v0, LX/5dZ;->d:Z

    .line 965826
    iput-boolean v1, v0, LX/5dZ;->e:Z

    .line 965827
    move-object v0, v0

    .line 965828
    invoke-virtual {v0}, LX/5dZ;->h()Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->a:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    .line 965829
    invoke-static {}, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->newBuilder()LX/5dZ;

    move-result-object v0

    const/4 v1, 0x1

    .line 965830
    iput-boolean v1, v0, LX/5dZ;->a:Z

    .line 965831
    iput-boolean v1, v0, LX/5dZ;->b:Z

    .line 965832
    iput-boolean v1, v0, LX/5dZ;->c:Z

    .line 965833
    iput-boolean v1, v0, LX/5dZ;->d:Z

    .line 965834
    iput-boolean v1, v0, LX/5dZ;->e:Z

    .line 965835
    move-object v0, v0

    .line 965836
    invoke-virtual {v0}, LX/5dZ;->h()Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->b:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    .line 965837
    new-instance v0, LX/5dY;

    invoke-direct {v0}, LX/5dY;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5dZ;)V
    .locals 1

    .prologue
    .line 965838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965839
    iget-boolean v0, p1, LX/5dZ;->a:Z

    move v0, v0

    .line 965840
    iput-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->c:Z

    .line 965841
    iget-boolean v0, p1, LX/5dZ;->b:Z

    move v0, v0

    .line 965842
    iput-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->d:Z

    .line 965843
    iget-boolean v0, p1, LX/5dZ;->c:Z

    move v0, v0

    .line 965844
    iput-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->e:Z

    .line 965845
    iget-boolean v0, p1, LX/5dZ;->d:Z

    move v0, v0

    .line 965846
    iput-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->f:Z

    .line 965847
    iget-boolean v0, p1, LX/5dZ;->e:Z

    move v0, v0

    .line 965848
    iput-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->g:Z

    .line 965849
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 965850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965851
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->c:Z

    .line 965852
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->d:Z

    .line 965853
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->e:Z

    .line 965854
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->f:Z

    .line 965855
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->g:Z

    .line 965856
    return-void
.end method

.method public static newBuilder()LX/5dZ;
    .locals 1

    .prologue
    .line 965857
    new-instance v0, LX/5dZ;

    invoke-direct {v0}, LX/5dZ;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 965858
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 965859
    iget-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 965860
    iget-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 965861
    iget-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 965862
    iget-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 965863
    iget-boolean v0, p0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 965864
    return-void
.end method
