.class public Lcom/facebook/messaging/business/commerce/model/retail/Subscription;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/commerce/model/retail/Subscription;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/messaging/business/attachments/model/LogoImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 922037
    new-instance v0, LX/5TY;

    invoke-direct {v0}, LX/5TY;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5TZ;)V
    .locals 1

    .prologue
    .line 922027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 922028
    iget-object v0, p1, LX/5TZ;->a:Ljava/lang/String;

    move-object v0, v0

    .line 922029
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;->a:Ljava/lang/String;

    .line 922030
    iget-object v0, p1, LX/5TZ;->b:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-object v0, v0

    .line 922031
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;->b:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 922032
    iget-object v0, p1, LX/5TZ;->c:Landroid/net/Uri;

    move-object v0, v0

    .line 922033
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;->c:Landroid/net/Uri;

    .line 922034
    iget-object v0, p1, LX/5TZ;->d:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    move-object v0, v0

    .line 922035
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;->d:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    .line 922036
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 922021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 922022
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;->a:Ljava/lang/String;

    .line 922023
    const-class v0, Lcom/facebook/messaging/business/attachments/model/LogoImage;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/model/LogoImage;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;->b:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 922024
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;->c:Landroid/net/Uri;

    .line 922025
    const-class v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;->d:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    .line 922026
    return-void
.end method


# virtual methods
.method public final a()LX/5TJ;
    .locals 1

    .prologue
    .line 922014
    sget-object v0, LX/5TJ;->PRODUCT_SUBSCRIPTION:LX/5TJ;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 922020
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 922015
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 922016
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;->b:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 922017
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 922018
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;->d:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 922019
    return-void
.end method
