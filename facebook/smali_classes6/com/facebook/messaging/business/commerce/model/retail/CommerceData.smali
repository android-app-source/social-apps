.class public Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 921639
    new-instance v0, LX/5TK;

    invoke-direct {v0}, LX/5TK;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 921620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921621
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 921622
    invoke-static {v0}, LX/5TJ;->getModelType(I)LX/5TJ;

    move-result-object v0

    .line 921623
    sget-object v1, LX/5TJ;->RECEIPT:LX/5TJ;

    if-ne v0, v1, :cond_0

    .line 921624
    const-class v1, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 921625
    :goto_0
    move-object v0, v1

    .line 921626
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;->a:Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;

    .line 921627
    return-void

    .line 921628
    :cond_0
    sget-object v1, LX/5TJ;->CANCELLATION:LX/5TJ;

    if-ne v0, v1, :cond_1

    .line 921629
    const-class v1, Lcom/facebook/messaging/business/commerce/model/retail/ReceiptCancellation;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    goto :goto_0

    .line 921630
    :cond_1
    sget-object v1, LX/5TJ;->SHIPMENT:LX/5TJ;

    if-eq v0, v1, :cond_2

    sget-object v1, LX/5TJ;->SHIPMENT_FOR_UNSUPPORTED_CARRIER:LX/5TJ;

    if-ne v0, v1, :cond_3

    .line 921631
    :cond_2
    const-class v1, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    goto :goto_0

    .line 921632
    :cond_3
    sget-object v1, LX/5TJ;->SHIPMENT_TRACKING_ETA:LX/5TJ;

    if-eq v0, v1, :cond_4

    sget-object v1, LX/5TJ;->SHIPMENT_ETA:LX/5TJ;

    if-eq v0, v1, :cond_4

    sget-object v1, LX/5TJ;->SHIPMENT_TRACKING_IN_TRANSIT:LX/5TJ;

    if-eq v0, v1, :cond_4

    sget-object v1, LX/5TJ;->SHIPMENT_TRACKING_OUT_FOR_DELIVERY:LX/5TJ;

    if-eq v0, v1, :cond_4

    sget-object v1, LX/5TJ;->SHIPMENT_TRACKING_DELAYED:LX/5TJ;

    if-eq v0, v1, :cond_4

    sget-object v1, LX/5TJ;->SHIPMENT_TRACKING_DELIVERED:LX/5TJ;

    if-ne v0, v1, :cond_5

    .line 921633
    :cond_4
    const-class v1, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    goto :goto_0

    .line 921634
    :cond_5
    sget-object v1, LX/5TJ;->PRODUCT_SUBSCRIPTION:LX/5TJ;

    if-ne v0, v1, :cond_6

    .line 921635
    const-class v1, Lcom/facebook/messaging/business/commerce/model/retail/Subscription;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    goto :goto_0

    .line 921636
    :cond_6
    sget-object v1, LX/5TJ;->AGENT_ITEM_SUGGESTION:LX/5TJ;

    if-ne v0, v1, :cond_7

    .line 921637
    const-class v1, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    goto :goto_0

    .line 921638
    :cond_7
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;)V
    .locals 0

    .prologue
    .line 921640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921641
    iput-object p1, p0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;->a:Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;

    .line 921642
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 921619
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 921614
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;->a:Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;->a:Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;

    invoke-interface {v0}, Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;->a()LX/5TJ;

    move-result-object v0

    .line 921615
    :goto_0
    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 921616
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;->a:Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921617
    return-void

    .line 921618
    :cond_0
    sget-object v0, LX/5TJ;->UNKNOWN:LX/5TJ;

    goto :goto_0
.end method
