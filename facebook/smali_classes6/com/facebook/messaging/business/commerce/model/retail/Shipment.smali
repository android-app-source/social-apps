.class public Lcom/facebook/messaging/business/commerce/model/retail/Shipment;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/commerce/model/retail/Shipment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

.field public final e:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:J

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:J

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:J

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:Ljava/lang/String;

.field public final o:Lcom/facebook/messaging/business/attachments/model/LogoImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 921881
    new-instance v0, LX/5TU;

    invoke-direct {v0}, LX/5TU;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5TV;)V
    .locals 4

    .prologue
    .line 921882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921883
    iget-object v0, p1, LX/5TV;->a:Ljava/lang/String;

    move-object v0, v0

    .line 921884
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->a:Ljava/lang/String;

    .line 921885
    iget-object v0, p1, LX/5TV;->b:Ljava/lang/String;

    move-object v0, v0

    .line 921886
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->b:Ljava/lang/String;

    .line 921887
    iget-object v0, p1, LX/5TV;->c:Ljava/lang/String;

    move-object v0, v0

    .line 921888
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->c:Ljava/lang/String;

    .line 921889
    iget-object v0, p1, LX/5TV;->d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    move-object v0, v0

    .line 921890
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    .line 921891
    iget-object v0, p1, LX/5TV;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 921892
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->e:Landroid/net/Uri;

    .line 921893
    iget-wide v2, p1, LX/5TV;->f:J

    move-wide v0, v2

    .line 921894
    iput-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->f:J

    .line 921895
    iget-object v0, p1, LX/5TV;->g:Ljava/lang/String;

    move-object v0, v0

    .line 921896
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->g:Ljava/lang/String;

    .line 921897
    iget-object v0, p1, LX/5TV;->h:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-object v0, v0

    .line 921898
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->h:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 921899
    iget-object v0, p1, LX/5TV;->i:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-object v0, v0

    .line 921900
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->i:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 921901
    iget-wide v2, p1, LX/5TV;->j:J

    move-wide v0, v2

    .line 921902
    iput-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->j:J

    .line 921903
    iget-object v0, p1, LX/5TV;->k:Ljava/lang/String;

    move-object v0, v0

    .line 921904
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->k:Ljava/lang/String;

    .line 921905
    iget-wide v2, p1, LX/5TV;->l:J

    move-wide v0, v2

    .line 921906
    iput-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->l:J

    .line 921907
    iget-object v0, p1, LX/5TV;->m:Ljava/lang/String;

    move-object v0, v0

    .line 921908
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->m:Ljava/lang/String;

    .line 921909
    iget-object v0, p1, LX/5TV;->n:Ljava/lang/String;

    move-object v0, v0

    .line 921910
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->n:Ljava/lang/String;

    .line 921911
    iget-object v0, p1, LX/5TV;->o:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-object v0, v0

    .line 921912
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->o:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 921913
    iget-object v0, p1, LX/5TV;->p:Ljava/util/List;

    move-object v0, v0

    .line 921914
    if-eqz v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->p:LX/0Px;

    .line 921915
    iget-object v0, p1, LX/5TV;->q:Ljava/util/List;

    move-object v0, v0

    .line 921916
    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->q:LX/0Px;

    .line 921917
    return-void

    .line 921918
    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_0

    .line 921919
    :cond_1
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 921920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921921
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->a:Ljava/lang/String;

    .line 921922
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->b:Ljava/lang/String;

    .line 921923
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->c:Ljava/lang/String;

    .line 921924
    const-class v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    .line 921925
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->e:Landroid/net/Uri;

    .line 921926
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->f:J

    .line 921927
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->g:Ljava/lang/String;

    .line 921928
    const-class v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->h:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 921929
    const-class v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->i:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 921930
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->j:J

    .line 921931
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->k:Ljava/lang/String;

    .line 921932
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->l:J

    .line 921933
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->m:Ljava/lang/String;

    .line 921934
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->n:Ljava/lang/String;

    .line 921935
    const-class v0, Lcom/facebook/messaging/business/attachments/model/LogoImage;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/model/LogoImage;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->o:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 921936
    const-class v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 921937
    if-eqz v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->p:LX/0Px;

    .line 921938
    const-class v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 921939
    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->q:LX/0Px;

    .line 921940
    return-void

    .line 921941
    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_0

    .line 921942
    :cond_1
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/5TJ;
    .locals 1

    .prologue
    .line 921943
    sget-object v0, LX/5TJ;->SHIPMENT:LX/5TJ;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 921944
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 921945
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921946
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921947
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921948
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921949
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->e:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921950
    iget-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 921951
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921952
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->h:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921953
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->i:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921954
    iget-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 921955
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921956
    iget-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->l:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 921957
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921958
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921959
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->o:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921960
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->p:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 921961
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;->q:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 921962
    return-void
.end method
