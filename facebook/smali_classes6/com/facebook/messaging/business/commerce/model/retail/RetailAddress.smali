.class public Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:D

.field public final i:D


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 921808
    new-instance v0, LX/5TP;

    invoke-direct {v0}, LX/5TP;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5TQ;)V
    .locals 4

    .prologue
    .line 921788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921789
    iget-object v0, p1, LX/5TQ;->a:Ljava/lang/String;

    move-object v0, v0

    .line 921790
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->a:Ljava/lang/String;

    .line 921791
    iget-object v0, p1, LX/5TQ;->b:Ljava/lang/String;

    move-object v0, v0

    .line 921792
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->b:Ljava/lang/String;

    .line 921793
    iget-object v0, p1, LX/5TQ;->c:Ljava/lang/String;

    move-object v0, v0

    .line 921794
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->c:Ljava/lang/String;

    .line 921795
    iget-object v0, p1, LX/5TQ;->d:Ljava/lang/String;

    move-object v0, v0

    .line 921796
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->d:Ljava/lang/String;

    .line 921797
    iget-object v0, p1, LX/5TQ;->e:Ljava/lang/String;

    move-object v0, v0

    .line 921798
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->e:Ljava/lang/String;

    .line 921799
    iget-object v0, p1, LX/5TQ;->f:Ljava/lang/String;

    move-object v0, v0

    .line 921800
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->f:Ljava/lang/String;

    .line 921801
    iget-object v0, p1, LX/5TQ;->g:Ljava/lang/String;

    move-object v0, v0

    .line 921802
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->g:Ljava/lang/String;

    .line 921803
    iget-wide v2, p1, LX/5TQ;->h:D

    move-wide v0, v2

    .line 921804
    iput-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->h:D

    .line 921805
    iget-wide v2, p1, LX/5TQ;->i:D

    move-wide v0, v2

    .line 921806
    iput-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->i:D

    .line 921807
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 921819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921820
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->a:Ljava/lang/String;

    .line 921821
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->b:Ljava/lang/String;

    .line 921822
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->c:Ljava/lang/String;

    .line 921823
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->d:Ljava/lang/String;

    .line 921824
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->e:Ljava/lang/String;

    .line 921825
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->f:Ljava/lang/String;

    .line 921826
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->g:Ljava/lang/String;

    .line 921827
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->h:D

    .line 921828
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->i:D

    .line 921829
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 921830
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 921809
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921810
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921811
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921812
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921813
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921814
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921815
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921816
    iget-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->h:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 921817
    iget-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;->i:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 921818
    return-void
.end method
