.class public Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

.field public final b:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 921503
    new-instance v0, LX/5TH;

    invoke-direct {v0}, LX/5TH;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5TI;)V
    .locals 1

    .prologue
    .line 921504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921505
    iget-object v0, p1, LX/5TI;->a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    move-object v0, v0

    .line 921506
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    .line 921507
    iget-object v0, p1, LX/5TI;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 921508
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->b:Landroid/net/Uri;

    .line 921509
    iget-object v0, p1, LX/5TI;->c:Ljava/lang/String;

    move-object v0, v0

    .line 921510
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->c:Ljava/lang/String;

    .line 921511
    iget-object v0, p1, LX/5TI;->d:Ljava/lang/String;

    move-object v0, v0

    .line 921512
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->d:Ljava/lang/String;

    .line 921513
    iget-object v0, p1, LX/5TI;->e:Ljava/lang/String;

    move-object v0, v0

    .line 921514
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->e:Ljava/lang/String;

    .line 921515
    iget-object v0, p1, LX/5TI;->f:Ljava/lang/String;

    move-object v0, v0

    .line 921516
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->f:Ljava/lang/String;

    .line 921517
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 921518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921519
    const-class v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    .line 921520
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->b:Landroid/net/Uri;

    .line 921521
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->c:Ljava/lang/String;

    .line 921522
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->d:Ljava/lang/String;

    .line 921523
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->e:Ljava/lang/String;

    .line 921524
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->f:Ljava/lang/String;

    .line 921525
    return-void
.end method


# virtual methods
.method public final a()LX/5TJ;
    .locals 1

    .prologue
    .line 921526
    sget-object v0, LX/5TJ;->AGENT_ITEM_SUGGESTION:LX/5TJ;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 921527
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 921528
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->a:Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921529
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921530
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921531
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921532
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921533
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/AgentItemSuggestion;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921534
    return-void
.end method
