.class public Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/messaging/business/attachments/model/LogoImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 921849
    new-instance v0, LX/5TS;

    invoke-direct {v0}, LX/5TS;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5TT;)V
    .locals 1

    .prologue
    .line 921850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921851
    iget-object v0, p1, LX/5TT;->a:Ljava/lang/String;

    move-object v0, v0

    .line 921852
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->a:Ljava/lang/String;

    .line 921853
    iget-object v0, p1, LX/5TT;->b:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-object v0, v0

    .line 921854
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->b:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 921855
    iget-object v0, p1, LX/5TT;->c:Landroid/net/Uri;

    move-object v0, v0

    .line 921856
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->c:Landroid/net/Uri;

    .line 921857
    iget-object v0, p1, LX/5TT;->d:Ljava/lang/String;

    move-object v0, v0

    .line 921858
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->d:Ljava/lang/String;

    .line 921859
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 921860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921861
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->a:Ljava/lang/String;

    .line 921862
    const-class v0, Lcom/facebook/messaging/business/attachments/model/LogoImage;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/model/LogoImage;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->b:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 921863
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->c:Landroid/net/Uri;

    .line 921864
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->d:Ljava/lang/String;

    .line 921865
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 921866
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 921867
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921868
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->b:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921869
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921870
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921871
    return-void
.end method
