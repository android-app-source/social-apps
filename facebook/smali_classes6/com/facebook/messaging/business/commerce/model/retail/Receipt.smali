.class public Lcom/facebook/messaging/business/commerce/model/retail/Receipt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/commerce/model/retail/Receipt;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/commerce/model/retail/RetailAdjustment;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:Lcom/facebook/messaging/business/attachments/model/LogoImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final s:Z

.field public final t:I

.field public final u:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 921646
    new-instance v0, LX/5TL;

    invoke-direct {v0}, LX/5TL;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5TM;)V
    .locals 1

    .prologue
    .line 921647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921648
    iget-object v0, p1, LX/5TM;->a:Ljava/lang/String;

    move-object v0, v0

    .line 921649
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->a:Ljava/lang/String;

    .line 921650
    iget-object v0, p1, LX/5TM;->b:Ljava/lang/String;

    move-object v0, v0

    .line 921651
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->b:Ljava/lang/String;

    .line 921652
    iget-object v0, p1, LX/5TM;->c:Ljava/lang/String;

    move-object v0, v0

    .line 921653
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->c:Ljava/lang/String;

    .line 921654
    iget-object v0, p1, LX/5TM;->d:Ljava/lang/String;

    move-object v0, v0

    .line 921655
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->d:Ljava/lang/String;

    .line 921656
    iget-object v0, p1, LX/5TM;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 921657
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->e:Landroid/net/Uri;

    .line 921658
    iget-object v0, p1, LX/5TM;->f:Landroid/net/Uri;

    move-object v0, v0

    .line 921659
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->f:Landroid/net/Uri;

    .line 921660
    iget-object v0, p1, LX/5TM;->g:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-object v0, v0

    .line 921661
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->g:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 921662
    iget-object v0, p1, LX/5TM;->h:Ljava/lang/String;

    move-object v0, v0

    .line 921663
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->h:Ljava/lang/String;

    .line 921664
    iget-object v0, p1, LX/5TM;->i:Ljava/lang/String;

    move-object v0, v0

    .line 921665
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->i:Ljava/lang/String;

    .line 921666
    iget-object v0, p1, LX/5TM;->j:Ljava/lang/String;

    move-object v0, v0

    .line 921667
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->j:Ljava/lang/String;

    .line 921668
    iget-object v0, p1, LX/5TM;->l:Ljava/util/List;

    move-object v0, v0

    .line 921669
    if-eqz v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->k:LX/0Px;

    .line 921670
    iget-object v0, p1, LX/5TM;->k:Ljava/lang/String;

    move-object v0, v0

    .line 921671
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->l:Ljava/lang/String;

    .line 921672
    iget-object v0, p1, LX/5TM;->m:Ljava/lang/String;

    move-object v0, v0

    .line 921673
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->m:Ljava/lang/String;

    .line 921674
    iget-object v0, p1, LX/5TM;->p:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-object v0, v0

    .line 921675
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->n:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 921676
    iget-object v0, p1, LX/5TM;->n:Ljava/lang/String;

    move-object v0, v0

    .line 921677
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->o:Ljava/lang/String;

    .line 921678
    iget v0, p1, LX/5TM;->o:I

    move v0, v0

    .line 921679
    iput v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->t:I

    .line 921680
    iget-object v0, p1, LX/5TM;->q:Ljava/util/List;

    move-object v0, v0

    .line 921681
    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->u:LX/0Px;

    .line 921682
    iget-object v0, p1, LX/5TM;->r:Ljava/lang/String;

    move-object v0, v0

    .line 921683
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->p:Ljava/lang/String;

    .line 921684
    iget-object v0, p1, LX/5TM;->s:Ljava/lang/String;

    move-object v0, v0

    .line 921685
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->q:Ljava/lang/String;

    .line 921686
    iget-boolean v0, p1, LX/5TM;->t:Z

    move v0, v0

    .line 921687
    iput-boolean v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->s:Z

    .line 921688
    iget-object v0, p1, LX/5TM;->u:Ljava/lang/String;

    move-object v0, v0

    .line 921689
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->r:Ljava/lang/String;

    .line 921690
    return-void

    .line 921691
    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_0

    .line 921692
    :cond_1
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 921693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921694
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->a:Ljava/lang/String;

    .line 921695
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->b:Ljava/lang/String;

    .line 921696
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->c:Ljava/lang/String;

    .line 921697
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->d:Ljava/lang/String;

    .line 921698
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->e:Landroid/net/Uri;

    .line 921699
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->f:Landroid/net/Uri;

    .line 921700
    const-class v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->g:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 921701
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->h:Ljava/lang/String;

    .line 921702
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->i:Ljava/lang/String;

    .line 921703
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->j:Ljava/lang/String;

    .line 921704
    const-class v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAdjustment;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 921705
    if-eqz v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->k:LX/0Px;

    .line 921706
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->l:Ljava/lang/String;

    .line 921707
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->m:Ljava/lang/String;

    .line 921708
    const-class v0, Lcom/facebook/messaging/business/attachments/model/LogoImage;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/model/LogoImage;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->n:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 921709
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->o:Ljava/lang/String;

    .line 921710
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->t:I

    .line 921711
    const-class v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 921712
    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->u:LX/0Px;

    .line 921713
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->p:Ljava/lang/String;

    .line 921714
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->q:Ljava/lang/String;

    .line 921715
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->s:Z

    .line 921716
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->r:Ljava/lang/String;

    .line 921717
    return-void

    .line 921718
    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_0

    .line 921719
    :cond_1
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_1

    .line 921720
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a()LX/5TJ;
    .locals 1

    .prologue
    .line 921721
    sget-object v0, LX/5TJ;->RECEIPT:LX/5TJ;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 921722
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 921723
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921724
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921725
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921726
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921727
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->e:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921728
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->f:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921729
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->g:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921730
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921731
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921732
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921733
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->k:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 921734
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921735
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921736
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->n:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 921737
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921738
    iget v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->t:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 921739
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->u:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 921740
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921741
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921742
    iget-boolean v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->s:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 921743
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/Receipt;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 921744
    return-void

    .line 921745
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
