.class public Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:J

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:LX/5TJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 921972
    new-instance v0, LX/5TW;

    invoke-direct {v0}, LX/5TW;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5TX;)V
    .locals 4

    .prologue
    .line 921973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921974
    iget-object v0, p1, LX/5TX;->a:LX/5TJ;

    move-object v0, v0

    .line 921975
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->g:LX/5TJ;

    .line 921976
    iget-object v0, p1, LX/5TX;->b:Ljava/lang/String;

    move-object v0, v0

    .line 921977
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->a:Ljava/lang/String;

    .line 921978
    iget-object v0, p1, LX/5TX;->c:Ljava/lang/String;

    move-object v0, v0

    .line 921979
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->b:Ljava/lang/String;

    .line 921980
    iget-wide v2, p1, LX/5TX;->d:J

    move-wide v0, v2

    .line 921981
    iput-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->c:J

    .line 921982
    iget-object v0, p1, LX/5TX;->e:Ljava/lang/String;

    move-object v0, v0

    .line 921983
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->d:Ljava/lang/String;

    .line 921984
    iget-object v0, p1, LX/5TX;->f:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-object v0, v0

    .line 921985
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->e:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 921986
    iget-object v0, p1, LX/5TX;->g:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    move-object v0, v0

    .line 921987
    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->f:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    .line 921988
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 921989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921990
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/5TJ;->getModelType(I)LX/5TJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->g:LX/5TJ;

    .line 921991
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->a:Ljava/lang/String;

    .line 921992
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->b:Ljava/lang/String;

    .line 921993
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->c:J

    .line 921994
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->d:Ljava/lang/String;

    .line 921995
    const-class v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->e:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 921996
    const-class v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    iput-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->f:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    .line 921997
    return-void
.end method


# virtual methods
.method public final a()LX/5TJ;
    .locals 1

    .prologue
    .line 921998
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->g:LX/5TJ;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 921999
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 922000
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->g:LX/5TJ;

    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 922001
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 922002
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 922003
    iget-wide v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 922004
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 922005
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->e:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 922006
    iget-object v0, p0, Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;->f:Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 922007
    return-void
.end method
