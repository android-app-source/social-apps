.class public final Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 920517
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 920518
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 920519
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 920520
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 920521
    if-nez p1, :cond_0

    move v0, v1

    .line 920522
    :goto_0
    return v0

    .line 920523
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 920524
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 920525
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 920526
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 920527
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 920528
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 920529
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 920530
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 920531
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 920532
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 920533
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 920534
    const v2, -0x2ac8bfd7

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 920535
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 920536
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 920537
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 920538
    :sswitch_2
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v2

    .line 920539
    const-class v0, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceShortOrderReceiptModel;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 920540
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 920541
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 920542
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 920543
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 920544
    invoke-virtual {p3, v1, v2, v1}, LX/186;->a(III)V

    .line 920545
    invoke-virtual {p3, v4, v3}, LX/186;->b(II)V

    .line 920546
    invoke-virtual {p3, v5, v0}, LX/186;->b(II)V

    .line 920547
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 920548
    :sswitch_3
    const-class v0, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$MessengerCommerceFetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 920549
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 920550
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 920551
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 920552
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 920553
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 920554
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 920555
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 920556
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 920557
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 920558
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 920559
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 920560
    invoke-virtual {p0, p1, v4, v1}, LX/15i;->a(III)I

    move-result v2

    .line 920561
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 920562
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 920563
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 920564
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 920565
    invoke-virtual {p3, v4, v2, v1}, LX/186;->a(III)V

    .line 920566
    invoke-virtual {p3, v5, v3}, LX/186;->b(II)V

    .line 920567
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7623dd48 -> :sswitch_0
        -0x3f6ff14c -> :sswitch_5
        -0x2ac8bfd7 -> :sswitch_2
        0x2de91d2b -> :sswitch_3
        0x6717975c -> :sswitch_4
        0x7b5faed8 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 920568
    if-nez p0, :cond_0

    move v0, v1

    .line 920569
    :goto_0
    return v0

    .line 920570
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 920571
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 920572
    :goto_1
    if-ge v1, v2, :cond_2

    .line 920573
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 920574
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 920575
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 920576
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 920578
    const/4 v7, 0x0

    .line 920579
    const/4 v1, 0x0

    .line 920580
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 920581
    invoke-static {v2, v3, v0}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 920582
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 920583
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 920584
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 920577
    new-instance v0, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 920596
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 920597
    if-eqz v0, :cond_0

    .line 920598
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 920599
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 920600
    if-eqz p0, :cond_0

    .line 920601
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 920602
    if-eq v0, p0, :cond_0

    .line 920603
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 920604
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 920585
    sparse-switch p2, :sswitch_data_0

    .line 920586
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 920587
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 920588
    const v1, -0x2ac8bfd7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 920589
    :goto_0
    :sswitch_1
    return-void

    .line 920590
    :sswitch_2
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceShortOrderReceiptModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 920591
    invoke-static {v0, p3}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 920592
    const/4 v0, 0x2

    const-class v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 920593
    invoke-static {v0, p3}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 920594
    :sswitch_3
    const-class v0, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$MessengerCommerceFetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 920595
    invoke-static {v0, p3}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7623dd48 -> :sswitch_1
        -0x3f6ff14c -> :sswitch_1
        -0x2ac8bfd7 -> :sswitch_2
        0x2de91d2b -> :sswitch_3
        0x6717975c -> :sswitch_1
        0x7b5faed8 -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 920510
    if-eqz p1, :cond_0

    .line 920511
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;

    move-result-object v1

    .line 920512
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;

    .line 920513
    if-eq v0, v1, :cond_0

    .line 920514
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 920515
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 920516
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 920477
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 920478
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 920479
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 920480
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 920481
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->a:LX/15i;

    .line 920482
    iput p2, p0, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->b:I

    .line 920483
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 920484
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 920485
    new-instance v0, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 920486
    iget v0, p0, LX/1vt;->c:I

    .line 920487
    move v0, v0

    .line 920488
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 920489
    iget v0, p0, LX/1vt;->c:I

    .line 920490
    move v0, v0

    .line 920491
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 920492
    iget v0, p0, LX/1vt;->b:I

    .line 920493
    move v0, v0

    .line 920494
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 920495
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 920496
    move-object v0, v0

    .line 920497
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 920498
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 920499
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 920500
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 920501
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 920502
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 920503
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 920504
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 920505
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 920506
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 920507
    iget v0, p0, LX/1vt;->c:I

    .line 920508
    move v0, v0

    .line 920509
    return v0
.end method
