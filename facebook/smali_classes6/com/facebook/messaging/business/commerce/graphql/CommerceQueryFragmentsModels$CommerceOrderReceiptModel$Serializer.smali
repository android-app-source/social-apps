.class public final Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceOrderReceiptModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceOrderReceiptModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 919620
    const-class v0, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceOrderReceiptModel;

    new-instance v1, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceOrderReceiptModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceOrderReceiptModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 919621
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 919622
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceOrderReceiptModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 919623
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 919624
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    .line 919625
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 919626
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919627
    if-eqz v2, :cond_0

    .line 919628
    const-string v3, "account_holder_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919629
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919630
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 919631
    if-eqz v2, :cond_1

    .line 919632
    const-string v2, "bubble_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919633
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919634
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919635
    if-eqz v2, :cond_2

    .line 919636
    const-string v3, "cancellation_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919637
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919638
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919639
    if-eqz v2, :cond_3

    .line 919640
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919641
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919642
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919643
    if-eqz v2, :cond_4

    .line 919644
    const-string v3, "merchant_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919645
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919646
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919647
    if-eqz v2, :cond_5

    .line 919648
    const-string v3, "order_payment_method"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919649
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919650
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919651
    if-eqz v2, :cond_6

    .line 919652
    const-string v3, "order_time_for_display"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919653
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919654
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 919655
    if-eqz v2, :cond_7

    .line 919656
    const-string v3, "partner_logo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919657
    invoke-static {v1, v2, p1}, LX/5VF;->a(LX/15i;ILX/0nX;)V

    .line 919658
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 919659
    if-eqz v2, :cond_8

    .line 919660
    const-string v3, "receipient"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919661
    invoke-static {v1, v2, p1}, LX/5T4;->a(LX/15i;ILX/0nX;)V

    .line 919662
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919663
    if-eqz v2, :cond_9

    .line 919664
    const-string v3, "receipt_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919665
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919666
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919667
    if-eqz v2, :cond_a

    .line 919668
    const-string v3, "receipt_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919669
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919670
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919671
    if-eqz v2, :cond_b

    .line 919672
    const-string v3, "recipient_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919673
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919674
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 919675
    if-eqz v2, :cond_c

    .line 919676
    const-string v3, "retail_adjustments"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919677
    invoke-static {v1, v2, p1, p2}, LX/5T5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 919678
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 919679
    if-eqz v2, :cond_d

    .line 919680
    const-string v3, "retail_items"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919681
    invoke-static {v1, v2, p1, p2}, LX/5T7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 919682
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919683
    if-eqz v2, :cond_e

    .line 919684
    const-string v3, "shipping_cost"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919685
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919686
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919687
    if-eqz v2, :cond_f

    .line 919688
    const-string v3, "shipping_method"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919689
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919690
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919691
    if-eqz v2, :cond_10

    .line 919692
    const-string v3, "status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919693
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919694
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 919695
    if-eqz v2, :cond_11

    .line 919696
    const-string v3, "structured_address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919697
    invoke-static {v1, v2, p1}, LX/5V0;->a(LX/15i;ILX/0nX;)V

    .line 919698
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919699
    if-eqz v2, :cond_12

    .line 919700
    const-string v3, "subtotal"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919701
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919702
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919703
    if-eqz v2, :cond_13

    .line 919704
    const-string v3, "tax"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919705
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919706
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 919707
    if-eqz v2, :cond_14

    .line 919708
    const-string v3, "total"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 919709
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 919710
    :cond_14
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 919711
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 919712
    check-cast p1, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceOrderReceiptModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceOrderReceiptModel$Serializer;->a(Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceOrderReceiptModel;LX/0nX;LX/0my;)V

    return-void
.end method
