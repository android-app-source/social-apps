.class public final Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 920073
    const-class v0, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 920074
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 920035
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 920037
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 920038
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 920039
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 920040
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 920041
    if-eqz v2, :cond_0

    .line 920042
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920043
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 920044
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 920045
    if-eqz v2, :cond_6

    .line 920046
    const-string v3, "messenger_commerce"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920047
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 920048
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 920049
    if-eqz v3, :cond_5

    .line 920050
    const-string p0, "retail_receipts"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920051
    const/4 p0, 0x0

    .line 920052
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 920053
    invoke-virtual {v1, v3, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 920054
    if-eqz p0, :cond_1

    .line 920055
    const-string v0, "count"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920056
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 920057
    :cond_1
    const/4 p0, 0x1

    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result p0

    .line 920058
    if-eqz p0, :cond_3

    .line 920059
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920060
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 920061
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 920062
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v2

    invoke-static {v1, v2, p1, p2}, LX/5TB;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 920063
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 920064
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 920065
    :cond_3
    const/4 p0, 0x2

    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result p0

    .line 920066
    if-eqz p0, :cond_4

    .line 920067
    const-string v0, "page_info"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920068
    invoke-static {v1, p0, p1}, LX/3Bn;->a(LX/15i;ILX/0nX;)V

    .line 920069
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 920070
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 920071
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 920072
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 920036
    check-cast p1, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel$Serializer;->a(Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
