.class public final Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceShipmentDetailsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceShipmentDetailsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 920252
    const-class v0, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceShipmentDetailsModel;

    new-instance v1, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceShipmentDetailsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceShipmentDetailsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 920253
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 920251
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceShipmentDetailsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 920185
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 920186
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 920187
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 920188
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 920189
    if-eqz v2, :cond_0

    .line 920190
    const-string v2, "bubble_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920191
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 920192
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 920193
    if-eqz v2, :cond_1

    .line 920194
    const-string p0, "carrier_tracking_url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920195
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 920196
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 920197
    if-eqz v2, :cond_2

    .line 920198
    const-string p0, "commerce_destination"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920199
    invoke-static {v1, v2, p1}, LX/5V0;->a(LX/15i;ILX/0nX;)V

    .line 920200
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 920201
    if-eqz v2, :cond_3

    .line 920202
    const-string p0, "commerce_origin"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920203
    invoke-static {v1, v2, p1}, LX/5V0;->a(LX/15i;ILX/0nX;)V

    .line 920204
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 920205
    if-eqz v2, :cond_4

    .line 920206
    const-string p0, "delayed_delivery_time_for_display"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920207
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 920208
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 920209
    if-eqz v2, :cond_5

    .line 920210
    const-string p0, "estimated_delivery_time_for_display"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920211
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 920212
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 920213
    if-eqz v2, :cond_6

    .line 920214
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920215
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 920216
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 920217
    if-eqz v2, :cond_7

    .line 920218
    const-string p0, "order_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920219
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 920220
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 920221
    if-eqz v2, :cond_8

    .line 920222
    const-string p0, "receipt"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920223
    invoke-static {v1, v2, p1, p2}, LX/5T8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 920224
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 920225
    if-eqz v2, :cond_9

    .line 920226
    const-string p0, "retail_carrier"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920227
    invoke-static {v1, v2, p1, p2}, LX/5VB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 920228
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 920229
    if-eqz v2, :cond_a

    .line 920230
    const-string p0, "retail_shipment_items"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920231
    invoke-static {v1, v2, p1, p2}, LX/5VC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 920232
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 920233
    if-eqz v2, :cond_b

    .line 920234
    const-string p0, "service_type_description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920235
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 920236
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 920237
    if-eqz v2, :cond_c

    .line 920238
    const-string p0, "shipdate_for_display"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920239
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 920240
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 920241
    if-eqz v2, :cond_d

    .line 920242
    const-string p0, "shipment_tracking_events"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920243
    invoke-static {v1, v2, p1, p2}, LX/5VD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 920244
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 920245
    if-eqz v2, :cond_e

    .line 920246
    const-string p0, "tracking_number"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 920247
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 920248
    :cond_e
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 920249
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 920250
    check-cast p1, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceShipmentDetailsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceShipmentDetailsModel$Serializer;->a(Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceShipmentDetailsModel;LX/0nX;LX/0my;)V

    return-void
.end method
