.class public final Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 919963
    const-class v0, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 919964
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 919965
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 919966
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 919967
    const/4 v2, 0x0

    .line 919968
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 919969
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 919970
    :goto_0
    move v1, v2

    .line 919971
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 919972
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 919973
    new-instance v1, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel;

    invoke-direct {v1}, Lcom/facebook/messaging/business/commerce/graphql/CommerceQueryFragmentsModels$CommerceReceiptListQueryModel;-><init>()V

    .line 919974
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 919975
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 919976
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 919977
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 919978
    :cond_0
    return-object v1

    .line 919979
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 919980
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 919981
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 919982
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 919983
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 919984
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 919985
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 919986
    :cond_4
    const-string v5, "messenger_commerce"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 919987
    const/4 v4, 0x0

    .line 919988
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_a

    .line 919989
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 919990
    :goto_2
    move v1, v4

    .line 919991
    goto :goto_1

    .line 919992
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 919993
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 919994
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 919995
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 919996
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 919997
    :cond_8
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_9

    .line 919998
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 919999
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 920000
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_8

    if-eqz v5, :cond_8

    .line 920001
    const-string v6, "retail_receipts"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 920002
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 920003
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v7, :cond_12

    .line 920004
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 920005
    :goto_4
    move v1, v5

    .line 920006
    goto :goto_3

    .line 920007
    :cond_9
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 920008
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 920009
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_a
    move v1, v4

    goto :goto_3

    .line 920010
    :cond_b
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_10

    .line 920011
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 920012
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 920013
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v10, :cond_b

    .line 920014
    const-string p0, "count"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 920015
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v9, v1

    move v1, v6

    goto :goto_5

    .line 920016
    :cond_c
    const-string p0, "nodes"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_e

    .line 920017
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 920018
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v10, p0, :cond_d

    .line 920019
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v10, p0, :cond_d

    .line 920020
    invoke-static {p1, v0}, LX/5TB;->b(LX/15w;LX/186;)I

    move-result v10

    .line 920021
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 920022
    :cond_d
    invoke-static {v8, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v8

    move v8, v8

    .line 920023
    goto :goto_5

    .line 920024
    :cond_e
    const-string p0, "page_info"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 920025
    invoke-static {p1, v0}, LX/3Bn;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_5

    .line 920026
    :cond_f
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 920027
    :cond_10
    const/4 v10, 0x3

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 920028
    if-eqz v1, :cond_11

    .line 920029
    invoke-virtual {v0, v5, v9, v5}, LX/186;->a(III)V

    .line 920030
    :cond_11
    invoke-virtual {v0, v6, v8}, LX/186;->b(II)V

    .line 920031
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 920032
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto/16 :goto_4

    :cond_12
    move v1, v5

    move v7, v5

    move v8, v5

    move v9, v5

    goto/16 :goto_5
.end method
