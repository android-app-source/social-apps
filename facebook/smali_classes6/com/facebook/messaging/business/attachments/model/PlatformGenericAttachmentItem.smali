.class public Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/5St;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:I

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final o:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 919412
    new-instance v0, LX/5Sv;

    invoke-direct {v0}, LX/5Sv;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5Sw;)V
    .locals 1

    .prologue
    .line 919359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 919360
    iget-object v0, p1, LX/5Sw;->a:Ljava/lang/String;

    move-object v0, v0

    .line 919361
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->a:Ljava/lang/String;

    .line 919362
    iget-object v0, p1, LX/5Sw;->b:Ljava/lang/String;

    move-object v0, v0

    .line 919363
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->b:Ljava/lang/String;

    .line 919364
    iget-object v0, p1, LX/5Sw;->c:Ljava/lang/String;

    move-object v0, v0

    .line 919365
    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->c:Ljava/lang/String;

    .line 919366
    iget-object v0, p1, LX/5Sw;->d:Landroid/net/Uri;

    move-object v0, v0

    .line 919367
    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->d:Landroid/net/Uri;

    .line 919368
    iget-object v0, p1, LX/5Sw;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 919369
    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->e:Landroid/net/Uri;

    .line 919370
    iget-object v0, p1, LX/5Sw;->f:Ljava/lang/String;

    move-object v0, v0

    .line 919371
    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->f:Ljava/lang/String;

    .line 919372
    iget v0, p1, LX/5Sw;->g:I

    move v0, v0

    .line 919373
    iput v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->g:I

    .line 919374
    iget-object v0, p1, LX/5Sw;->h:Ljava/lang/String;

    move-object v0, v0

    .line 919375
    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->h:Ljava/lang/String;

    .line 919376
    iget-object v0, p1, LX/5Sw;->i:Ljava/lang/String;

    move-object v0, v0

    .line 919377
    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->i:Ljava/lang/String;

    .line 919378
    iget-object v0, p1, LX/5Sw;->j:Ljava/lang/String;

    move-object v0, v0

    .line 919379
    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->j:Ljava/lang/String;

    .line 919380
    iget-object v0, p1, LX/5Sw;->k:Ljava/lang/String;

    move-object v0, v0

    .line 919381
    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->k:Ljava/lang/String;

    .line 919382
    iget-object v0, p1, LX/5Sw;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-object v0, v0

    .line 919383
    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 919384
    iget-object v0, p1, LX/5Sw;->m:Ljava/util/List;

    move-object v0, v0

    .line 919385
    if-eqz v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->m:LX/0Px;

    .line 919386
    iget-object v0, p1, LX/5Sw;->n:Landroid/net/Uri;

    move-object v0, v0

    .line 919387
    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->n:Landroid/net/Uri;

    .line 919388
    iget-object v0, p1, LX/5Sw;->o:Landroid/net/Uri;

    move-object v0, v0

    .line 919389
    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->o:Landroid/net/Uri;

    .line 919390
    return-void

    .line 919391
    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 919393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 919394
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->a:Ljava/lang/String;

    .line 919395
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->b:Ljava/lang/String;

    .line 919396
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->c:Ljava/lang/String;

    .line 919397
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->d:Landroid/net/Uri;

    .line 919398
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->f:Ljava/lang/String;

    .line 919399
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->g:I

    .line 919400
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->h:Ljava/lang/String;

    .line 919401
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->i:Ljava/lang/String;

    .line 919402
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->j:Ljava/lang/String;

    .line 919403
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->k:Ljava/lang/String;

    .line 919404
    const-class v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 919405
    const-class v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 919406
    if-eqz v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->m:LX/0Px;

    .line 919407
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->n:Landroid/net/Uri;

    .line 919408
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->o:Landroid/net/Uri;

    .line 919409
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->e:Landroid/net/Uri;

    .line 919410
    return-void

    .line 919411
    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 919392
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 919413
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 919341
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->d:Landroid/net/Uri;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 919342
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 919343
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 919344
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 919345
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 919346
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 919347
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 919348
    iget v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 919349
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 919350
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 919351
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 919352
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 919353
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 919354
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->m:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 919355
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->n:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 919356
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->o:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 919357
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;->e:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 919358
    return-void
.end method
