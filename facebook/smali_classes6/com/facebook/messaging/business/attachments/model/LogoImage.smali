.class public Lcom/facebook/messaging/business/attachments/model/LogoImage;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/attachments/model/LogoImage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:I

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 919254
    new-instance v0, LX/5Sp;

    invoke-direct {v0}, LX/5Sp;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5Sq;)V
    .locals 1

    .prologue
    .line 919255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 919256
    iget-object v0, p1, LX/5Sq;->a:Landroid/net/Uri;

    move-object v0, v0

    .line 919257
    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->a:Landroid/net/Uri;

    .line 919258
    iget v0, p1, LX/5Sq;->b:I

    move v0, v0

    .line 919259
    iput v0, p0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->b:I

    .line 919260
    iget v0, p1, LX/5Sq;->c:I

    move v0, v0

    .line 919261
    iput v0, p0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->c:I

    .line 919262
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 919263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 919264
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->a:Landroid/net/Uri;

    .line 919265
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->b:I

    .line 919266
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->c:I

    .line 919267
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 919268
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 919269
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 919270
    iget v0, p0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 919271
    iget v0, p0, Lcom/facebook/messaging/business/attachments/model/LogoImage;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 919272
    return-void
.end method
