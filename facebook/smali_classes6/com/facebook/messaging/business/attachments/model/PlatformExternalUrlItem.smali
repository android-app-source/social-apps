.class public Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/5St;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 919317
    new-instance v0, LX/5Sr;

    invoke-direct {v0}, LX/5Sr;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5Ss;)V
    .locals 1

    .prologue
    .line 919307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 919308
    iget-object v0, p1, LX/5Ss;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->a:Ljava/lang/String;

    .line 919309
    iget-object v0, p1, LX/5Ss;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->b:Ljava/lang/String;

    .line 919310
    iget-object v0, p1, LX/5Ss;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->c:Ljava/lang/String;

    .line 919311
    iget-object v0, p1, LX/5Ss;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->d:Ljava/lang/String;

    .line 919312
    iget-object v0, p1, LX/5Ss;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->e:Ljava/lang/String;

    .line 919313
    iget-object v0, p1, LX/5Ss;->f:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->f:Landroid/net/Uri;

    .line 919314
    iget-object v0, p1, LX/5Ss;->g:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->g:Landroid/net/Uri;

    .line 919315
    iget-object v0, p1, LX/5Ss;->h:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->h:Landroid/net/Uri;

    .line 919316
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 919297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 919298
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->a:Ljava/lang/String;

    .line 919299
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->b:Ljava/lang/String;

    .line 919300
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->c:Ljava/lang/String;

    .line 919301
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->d:Ljava/lang/String;

    .line 919302
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->e:Ljava/lang/String;

    .line 919303
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->f:Landroid/net/Uri;

    .line 919304
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->g:Landroid/net/Uri;

    .line 919305
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->h:Landroid/net/Uri;

    .line 919306
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 919296
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 919295
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 919284
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->g:Landroid/net/Uri;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 919294
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 919285
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 919286
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 919287
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 919288
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 919289
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 919290
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->f:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 919291
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->g:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 919292
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/model/PlatformExternalUrlItem;->h:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 919293
    return-void
.end method
