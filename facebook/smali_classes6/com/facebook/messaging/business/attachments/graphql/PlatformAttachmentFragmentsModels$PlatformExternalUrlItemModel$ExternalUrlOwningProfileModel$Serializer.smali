.class public final Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 918579
    const-class v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    new-instance v1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 918580
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 918581
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 918582
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 918583
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/5Sg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 918584
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 918585
    check-cast p1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel$Serializer;->a(Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;LX/0nX;LX/0my;)V

    return-void
.end method
