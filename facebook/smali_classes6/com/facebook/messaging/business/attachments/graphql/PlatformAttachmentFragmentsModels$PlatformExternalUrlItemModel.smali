.class public final Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x44330415
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 918909
    const-class v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 918910
    const-class v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 918911
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 918912
    return-void
.end method

.method private n()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918913
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->f:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->f:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    .line 918914
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->f:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    return-object v0
.end method

.method private o()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918915
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->g:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->g:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    .line 918916
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->g:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    return-object v0
.end method

.method private p()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918917
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->i:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->i:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    .line 918918
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->i:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    return-object v0
.end method

.method private q()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918921
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->j:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->j:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    .line 918922
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->j:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    return-object v0
.end method

.method private r()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918919
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->l:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->l:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    .line 918920
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->l:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    return-object v0
.end method

.method private s()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918957
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->m:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->m:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    .line 918958
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->m:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 918959
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 918960
    invoke-virtual {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 918961
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->n()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 918962
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->o()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 918963
    invoke-virtual {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 918964
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->p()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 918965
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->q()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 918966
    invoke-virtual {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 918967
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->r()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 918968
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->s()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 918969
    invoke-virtual {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->m()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 918970
    const/16 v10, 0xa

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 918971
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 918972
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 918973
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 918974
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 918975
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 918976
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 918977
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 918978
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 918979
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 918980
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 918981
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 918982
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 918924
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 918925
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->n()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 918926
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->n()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    .line 918927
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->n()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 918928
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;

    .line 918929
    iput-object v0, v1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->f:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    .line 918930
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->o()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 918931
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->o()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    .line 918932
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->o()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 918933
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;

    .line 918934
    iput-object v0, v1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->g:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    .line 918935
    :cond_1
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->p()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 918936
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->p()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    .line 918937
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->p()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 918938
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;

    .line 918939
    iput-object v0, v1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->i:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    .line 918940
    :cond_2
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->q()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 918941
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->q()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    .line 918942
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->q()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 918943
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;

    .line 918944
    iput-object v0, v1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->j:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    .line 918945
    :cond_3
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->r()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 918946
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->r()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    .line 918947
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->r()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 918948
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;

    .line 918949
    iput-object v0, v1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->l:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    .line 918950
    :cond_4
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->s()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 918951
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->s()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    .line 918952
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->s()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 918953
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;

    .line 918954
    iput-object v0, v1, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->m:Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    .line 918955
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 918956
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918923
    invoke-virtual {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 918906
    new-instance v0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;

    invoke-direct {v0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;-><init>()V

    .line 918907
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 918908
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918890
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->e:Ljava/lang/String;

    .line 918891
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918905
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->n()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$ExternalUrlOwningProfileModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic cu_()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918904
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->q()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$LinkMediaModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic cv_()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918903
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->p()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$InstantArticleModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918902
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->o()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$IconModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 918901
    const v0, 0x45904ffd

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918899
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->h:Ljava/lang/String;

    .line 918900
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 918898
    const v0, 0x1eaef984

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918896
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->k:Ljava/lang/String;

    .line 918897
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918895
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->r()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$SourceModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic l()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918894
    invoke-direct {p0}, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->s()Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel$TitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 918892
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->n:Ljava/lang/String;

    .line 918893
    iget-object v0, p0, Lcom/facebook/messaging/business/attachments/graphql/PlatformAttachmentFragmentsModels$PlatformExternalUrlItemModel;->n:Ljava/lang/String;

    return-object v0
.end method
