.class public Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final i:Lorg/apache/http/client/ResponseHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/http/client/ResponseHandler",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile j:Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;


# instance fields
.field public b:Lcom/facebook/http/common/FbHttpRequestProcessor;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/6eH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0kb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1117797
    const-class v0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    sput-object v0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->a:Ljava/lang/Class;

    .line 1117798
    new-instance v0, LX/6eK;

    invoke-direct {v0}, LX/6eK;-><init>()V

    sput-object v0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->i:Lorg/apache/http/client/ResponseHandler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1117859
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;
    .locals 10

    .prologue
    .line 1117839
    sget-object v0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->j:Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    if-nez v0, :cond_1

    .line 1117840
    const-class v1, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    monitor-enter v1

    .line 1117841
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->j:Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1117842
    if-eqz v2, :cond_0

    .line 1117843
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1117844
    new-instance v3, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    invoke-direct {v3}, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;-><init>()V

    .line 1117845
    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v4

    check-cast v4, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    .line 1117846
    new-instance v8, LX/6eH;

    invoke-direct {v8}, LX/6eH;-><init>()V

    .line 1117847
    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    .line 1117848
    iput-object v7, v8, LX/6eH;->a:LX/0Zb;

    .line 1117849
    move-object v7, v8

    .line 1117850
    check-cast v7, LX/6eH;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v9

    check-cast v9, LX/0kb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p0

    check-cast p0, LX/03V;

    .line 1117851
    iput-object v4, v3, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    iput-object v5, v3, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v6, v3, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->d:Ljava/util/concurrent/ExecutorService;

    iput-object v7, v3, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->e:LX/6eH;

    iput-object v8, v3, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->f:LX/0SG;

    iput-object v9, v3, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->g:LX/0kb;

    iput-object p0, v3, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->h:LX/03V;

    .line 1117852
    move-object v0, v3

    .line 1117853
    sput-object v0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->j:Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1117854
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1117855
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1117856
    :cond_1
    sget-object v0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->j:Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    return-object v0

    .line 1117857
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1117858
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 1117835
    cmp-long v0, p1, v4

    if-nez v0, :cond_0

    .line 1117836
    :goto_0
    return-void

    .line 1117837
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6eL;->d:LX/0Tn;

    invoke-interface {v0, v1, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 1117838
    iget-object v2, p0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/6eL;->d:LX/0Tn;

    add-long/2addr v0, p1

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public static declared-synchronized a$redex0(Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;)V
    .locals 4

    .prologue
    .line 1117832
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/6eL;->e:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/6eL;->c:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, LX/6eL;->d:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1117833
    monitor-exit p0

    return-void

    .line 1117834
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;Ljava/lang/String;J)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1117825
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/6eL;->e:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1117826
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 1117827
    :goto_0
    monitor-exit p0

    return-void

    .line 1117828
    :cond_0
    :try_start_1
    invoke-static {p0, p2, p3}, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->a(Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;J)V

    .line 1117829
    iget-object v0, p0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v0, LX/6eL;->e:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0, p2, p3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1117830
    iget-object v0, p0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils$2;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils$2;-><init>(Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;)V

    const v2, -0x4a3044d3

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1117831
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 1117799
    if-nez p1, :cond_0

    .line 1117800
    iget-object v0, p0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->h:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_deferredImage"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Null id passed for image resource "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117801
    :goto_0
    return-void

    .line 1117802
    :cond_0
    new-instance v0, Lorg/apache/http/client/methods/HttpHead;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpHead;-><init>(Ljava/net/URI;)V

    .line 1117803
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v1

    .line 1117804
    iput-object v0, v1, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 1117805
    move-object v0, v1

    .line 1117806
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "data_sensitivity"

    invoke-static {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 1117807
    iput-object v1, v0, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1117808
    move-object v0, v0

    .line 1117809
    const-class v1, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 1117810
    iput-object v1, v0, LX/15E;->c:Ljava/lang/String;

    .line 1117811
    move-object v0, v0

    .line 1117812
    sget-object v1, LX/14P;->RETRY_SAFE:LX/14P;

    .line 1117813
    iput-object v1, v0, LX/15E;->j:LX/14P;

    .line 1117814
    move-object v0, v0

    .line 1117815
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1117816
    iput-object v1, v0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1117817
    move-object v0, v0

    .line 1117818
    sget-object v1, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->i:Lorg/apache/http/client/ResponseHandler;

    .line 1117819
    iput-object v1, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 1117820
    move-object v0, v0

    .line 1117821
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    .line 1117822
    iget-object v1, p0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v0

    .line 1117823
    iget-object v1, v0, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v1

    .line 1117824
    new-instance v1, LX/6eJ;

    invoke-direct {v1, p0, p1}, LX/6eJ;-><init>(Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
