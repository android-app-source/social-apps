.class public final Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7a92db49
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 967996
    const-class v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 967995
    const-class v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 967993
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 967994
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 967990
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 967991
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 967992
    return-void
.end method

.method public static a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;)Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;
    .locals 8

    .prologue
    .line 967969
    if-nez p0, :cond_0

    .line 967970
    const/4 p0, 0x0

    .line 967971
    :goto_0
    return-object p0

    .line 967972
    :cond_0
    instance-of v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;

    if-eqz v0, :cond_1

    .line 967973
    check-cast p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;

    goto :goto_0

    .line 967974
    :cond_1
    new-instance v0, LX/5eW;

    invoke-direct {v0}, LX/5eW;-><init>()V

    .line 967975
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;->a()I

    move-result v1

    iput v1, v0, LX/5eW;->a:I

    .line 967976
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;->b()I

    move-result v1

    iput v1, v0, LX/5eW;->b:I

    .line 967977
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 967978
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 967979
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 967980
    iget v3, v0, LX/5eW;->a:I

    invoke-virtual {v2, v5, v3, v5}, LX/186;->a(III)V

    .line 967981
    iget v3, v0, LX/5eW;->b:I

    invoke-virtual {v2, v6, v3, v5}, LX/186;->a(III)V

    .line 967982
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 967983
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 967984
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 967985
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 967986
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 967987
    new-instance v3, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;

    invoke-direct {v3, v2}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;-><init>(LX/15i;)V

    .line 967988
    move-object p0, v3

    .line 967989
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 967967
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 967968
    iget v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 967997
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 967998
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 967999
    iget v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;->e:I

    invoke-virtual {p1, v2, v0, v2}, LX/186;->a(III)V

    .line 968000
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 968001
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 968002
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 967964
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 967965
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 967966
    return-object p0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 967958
    iput p1, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;->e:I

    .line 967959
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 967960
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 967961
    if-eqz v0, :cond_0

    .line 967962
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 967963
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 967954
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 967955
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;->e:I

    .line 967956
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;->f:I

    .line 967957
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 967952
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 967953
    iget v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 967949
    new-instance v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;

    invoke-direct {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;-><init>()V

    .line 967950
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 967951
    return-object v0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 967943
    iput p1, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$TopLevelCommentsModel;->f:I

    .line 967944
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 967945
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 967946
    if-eqz v0, :cond_0

    .line 967947
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 967948
    :cond_0
    return-void
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 967942
    const v0, 0x31c16fa2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 967941
    const v0, 0x35a359a1

    return v0
.end method
