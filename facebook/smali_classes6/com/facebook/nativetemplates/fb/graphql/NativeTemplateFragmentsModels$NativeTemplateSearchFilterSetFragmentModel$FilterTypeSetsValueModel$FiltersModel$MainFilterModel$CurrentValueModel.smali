.class public final Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x39f39d98
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 968505
    const-class v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 968506
    const-class v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 968507
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 968508
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 968509
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 968510
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 968511
    return-void
.end method

.method public static a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;)Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;
    .locals 8

    .prologue
    .line 968512
    if-nez p0, :cond_0

    .line 968513
    const/4 p0, 0x0

    .line 968514
    :goto_0
    return-object p0

    .line 968515
    :cond_0
    instance-of v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    if-eqz v0, :cond_1

    .line 968516
    check-cast p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    goto :goto_0

    .line 968517
    :cond_1
    new-instance v0, LX/5ec;

    invoke-direct {v0}, LX/5ec;-><init>()V

    .line 968518
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5ec;->a:Ljava/lang/String;

    .line 968519
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 968520
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 968521
    iget-object v3, v0, LX/5ec;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 968522
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 968523
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 968524
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 968525
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 968526
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 968527
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 968528
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 968529
    new-instance v3, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    invoke-direct {v3, v2}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;-><init>(LX/15i;)V

    .line 968530
    move-object p0, v3

    .line 968531
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 968532
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 968533
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 968534
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 968535
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 968536
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 968537
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 968538
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 968539
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 968540
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 968541
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;->e:Ljava/lang/String;

    .line 968542
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 968543
    new-instance v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    invoke-direct {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;-><init>()V

    .line 968544
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 968545
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 968546
    const v0, 0xc29e9e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 968547
    const v0, -0x7f325c79

    return v0
.end method
