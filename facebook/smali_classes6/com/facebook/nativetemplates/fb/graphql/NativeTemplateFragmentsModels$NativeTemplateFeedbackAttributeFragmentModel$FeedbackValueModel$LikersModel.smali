.class public final Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 967861
    const-class v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 967886
    const-class v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 967887
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 967888
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 967908
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 967909
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 967910
    return-void
.end method

.method public static a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;)Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;
    .locals 8

    .prologue
    .line 967889
    if-nez p0, :cond_0

    .line 967890
    const/4 p0, 0x0

    .line 967891
    :goto_0
    return-object p0

    .line 967892
    :cond_0
    instance-of v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;

    if-eqz v0, :cond_1

    .line 967893
    check-cast p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;

    goto :goto_0

    .line 967894
    :cond_1
    new-instance v0, LX/5eU;

    invoke-direct {v0}, LX/5eU;-><init>()V

    .line 967895
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;->a()I

    move-result v1

    iput v1, v0, LX/5eU;->a:I

    .line 967896
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 967897
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 967898
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 967899
    iget v3, v0, LX/5eU;->a:I

    invoke-virtual {v2, v5, v3, v5}, LX/186;->a(III)V

    .line 967900
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 967901
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 967902
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 967903
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 967904
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 967905
    new-instance v3, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;

    invoke-direct {v3, v2}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;-><init>(LX/15i;)V

    .line 967906
    move-object p0, v3

    .line 967907
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 967879
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 967880
    iget v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 967881
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 967882
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 967883
    iget v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 967884
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 967885
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 967876
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 967877
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 967878
    return-object p0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 967870
    iput p1, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;->e:I

    .line 967871
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 967872
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 967873
    if-eqz v0, :cond_0

    .line 967874
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 967875
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 967867
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 967868
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;->e:I

    .line 967869
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 967864
    new-instance v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;

    invoke-direct {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$FeedbackValueModel$LikersModel;-><init>()V

    .line 967865
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 967866
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 967863
    const v0, 0x64e7f9b4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 967862
    const v0, 0x2bb653c8

    return v0
.end method
