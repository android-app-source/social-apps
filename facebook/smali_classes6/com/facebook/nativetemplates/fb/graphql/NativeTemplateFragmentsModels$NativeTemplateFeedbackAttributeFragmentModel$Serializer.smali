.class public final Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 968226
    const-class v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel;

    new-instance v1, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 968227
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 968242
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 968229
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 968230
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 968231
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 968232
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 968233
    if-eqz v2, :cond_0

    .line 968234
    const-string p0, "feedback_value"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 968235
    invoke-static {v1, v2, p1, p2}, LX/5eo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 968236
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 968237
    if-eqz v2, :cond_1

    .line 968238
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 968239
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 968240
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 968241
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 968228
    check-cast p1, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel$Serializer;->a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateFeedbackAttributeFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
