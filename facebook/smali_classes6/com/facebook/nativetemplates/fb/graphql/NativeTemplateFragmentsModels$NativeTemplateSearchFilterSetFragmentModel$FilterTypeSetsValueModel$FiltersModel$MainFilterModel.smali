.class public final Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x9213bbe
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 968620
    const-class v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 968617
    const-class v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 968618
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 968619
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 968621
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 968622
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 968623
    return-void
.end method

.method public static a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;)Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;
    .locals 9

    .prologue
    .line 968568
    if-nez p0, :cond_0

    .line 968569
    const/4 p0, 0x0

    .line 968570
    :goto_0
    return-object p0

    .line 968571
    :cond_0
    instance-of v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    if-eqz v0, :cond_1

    .line 968572
    check-cast p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    goto :goto_0

    .line 968573
    :cond_1
    new-instance v0, LX/5eb;

    invoke-direct {v0}, LX/5eb;-><init>()V

    .line 968574
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;->a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;)Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    move-result-object v1

    iput-object v1, v0, LX/5eb;->a:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    .line 968575
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5eb;->b:Ljava/lang/String;

    .line 968576
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 968577
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 968578
    iget-object v3, v0, LX/5eb;->a:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 968579
    iget-object v5, v0, LX/5eb;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 968580
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 968581
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 968582
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 968583
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 968584
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 968585
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 968586
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 968587
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 968588
    new-instance v3, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    invoke-direct {v3, v2}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;-><init>(LX/15i;)V

    .line 968589
    move-object p0, v3

    .line 968590
    goto :goto_0
.end method

.method private j()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 968607
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->e:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    iput-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->e:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    .line 968608
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->e:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 968609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 968610
    invoke-direct {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->j()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 968611
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 968612
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 968613
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 968614
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 968615
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 968616
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 968599
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 968600
    invoke-direct {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->j()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 968601
    invoke-direct {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->j()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    .line 968602
    invoke-direct {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->j()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 968603
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    .line 968604
    iput-object v0, v1, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->e:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    .line 968605
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 968606
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 968598
    invoke-direct {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->j()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel$CurrentValueModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 968595
    new-instance v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    invoke-direct {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;-><init>()V

    .line 968596
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 968597
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 968593
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->f:Ljava/lang/String;

    .line 968594
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 968592
    const v0, -0x3e12f98d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 968591
    const v0, -0x55db0bf6

    return v0
.end method
