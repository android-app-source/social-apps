.class public final Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x16f7c0d9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 968679
    const-class v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 968678
    const-class v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 968676
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 968677
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 968673
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 968674
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 968675
    return-void
.end method

.method public static a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;)Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;
    .locals 8

    .prologue
    .line 968653
    if-nez p0, :cond_0

    .line 968654
    const/4 p0, 0x0

    .line 968655
    :goto_0
    return-object p0

    .line 968656
    :cond_0
    instance-of v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;

    if-eqz v0, :cond_1

    .line 968657
    check-cast p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;

    goto :goto_0

    .line 968658
    :cond_1
    new-instance v0, LX/5ea;

    invoke-direct {v0}, LX/5ea;-><init>()V

    .line 968659
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;->a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;->a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;)Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    move-result-object v1

    iput-object v1, v0, LX/5ea;->a:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    .line 968660
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 968661
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 968662
    iget-object v3, v0, LX/5ea;->a:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 968663
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 968664
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 968665
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 968666
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 968667
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 968668
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 968669
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 968670
    new-instance v3, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;

    invoke-direct {v3, v2}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;-><init>(LX/15i;)V

    .line 968671
    move-object p0, v3

    .line 968672
    goto :goto_0
.end method

.method private j()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 968651
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;->e:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    iput-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;->e:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    .line 968652
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;->e:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 968645
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 968646
    invoke-direct {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;->j()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 968647
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 968648
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 968649
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 968650
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 968637
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 968638
    invoke-direct {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;->j()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 968639
    invoke-direct {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;->j()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    .line 968640
    invoke-direct {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;->j()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 968641
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;

    .line 968642
    iput-object v0, v1, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;->e:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    .line 968643
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 968644
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 968631
    invoke-direct {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;->j()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel$MainFilterModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 968634
    new-instance v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;

    invoke-direct {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateSearchFilterSetFragmentModel$FilterTypeSetsValueModel$FiltersModel;-><init>()V

    .line 968635
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 968636
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 968633
    const v0, -0x7774f30

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 968632
    const v0, -0x7ffdf76b

    return v0
.end method
