.class public Lcom/facebook/api/story/FetchSingleStoryParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/story/FetchSingleStoryParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0rS;

.field public final d:LX/5Go;

.field public final e:I

.field public final f:LX/21y;

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Z

.field public final j:Z

.field public final k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 889215
    new-instance v0, LX/5Gn;

    invoke-direct {v0}, LX/5Gn;-><init>()V

    sput-object v0, Lcom/facebook/api/story/FetchSingleStoryParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 889216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889217
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->a:Ljava/lang/String;

    .line 889218
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->c:LX/0rS;

    .line 889219
    invoke-static {}, LX/5Go;->values()[LX/5Go;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->d:LX/5Go;

    .line 889220
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->e:I

    .line 889221
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->b:Ljava/lang/String;

    .line 889222
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/21y;->getOrder(Ljava/lang/String;)LX/21y;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->f:LX/21y;

    .line 889223
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->g:Ljava/lang/String;

    .line 889224
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->h:Ljava/lang/String;

    .line 889225
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->i:Z

    .line 889226
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->j:Z

    .line 889227
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->k:Z

    .line 889228
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0rS;)V
    .locals 2

    .prologue
    .line 889229
    sget-object v0, LX/5Go;->GRAPHQL_DEFAULT:LX/5Go;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;I)V

    .line 889230
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0rS;LX/5Go;I)V
    .locals 12

    .prologue
    .line 889231
    const/4 v5, 0x0

    sget-object v6, LX/21y;->DEFAULT_ORDER:LX/21y;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v4, p4

    invoke-direct/range {v0 .. v11}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;ILjava/lang/String;LX/21y;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    .line 889232
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0rS;LX/5Go;ILjava/lang/String;LX/21y;Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 889233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889234
    iput-object p1, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->a:Ljava/lang/String;

    .line 889235
    iput-object p2, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->c:LX/0rS;

    .line 889236
    iput-object p3, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->d:LX/5Go;

    .line 889237
    iput p4, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->e:I

    .line 889238
    iput-object p5, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->b:Ljava/lang/String;

    .line 889239
    iput-object p6, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->f:LX/21y;

    .line 889240
    iput-object p7, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->g:Ljava/lang/String;

    .line 889241
    iput-object p8, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->h:Ljava/lang/String;

    .line 889242
    iput-boolean p9, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->i:Z

    .line 889243
    iput-boolean p10, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->j:Z

    .line 889244
    iput-boolean p11, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->k:Z

    .line 889245
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0rS;LX/5Go;IZ)V
    .locals 12

    .prologue
    .line 889246
    const/4 v5, 0x0

    sget-object v6, LX/21y;->DEFAULT_ORDER:LX/21y;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v4, p4

    move/from16 v11, p5

    invoke-direct/range {v0 .. v11}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;ILjava/lang/String;LX/21y;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    .line 889247
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 889248
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 889249
    iget-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889250
    iget-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->c:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889251
    iget-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->d:LX/5Go;

    invoke-virtual {v0}, LX/5Go;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 889252
    iget v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 889253
    iget-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889254
    iget-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->f:LX/21y;

    invoke-virtual {v0}, LX/21y;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889255
    iget-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889256
    iget-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889257
    iget-boolean v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->i:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 889258
    iget-boolean v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->j:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 889259
    iget-boolean v0, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->k:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 889260
    return-void
.end method
