.class public Lcom/facebook/api/story/FetchSingleStoryResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/story/FetchSingleStoryResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLStory;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 889264
    new-instance v0, LX/5Gp;

    invoke-direct {v0}, LX/5Gp;-><init>()V

    sput-object v0, Lcom/facebook/api/story/FetchSingleStoryResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 889265
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 889266
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 889267
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLStory;LX/0ta;J)V
    .locals 1

    .prologue
    .line 889268
    invoke-direct {p0, p2, p3, p4}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 889269
    iput-object p1, p0, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 889270
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 889271
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 889272
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 889273
    iget-object v0, p0, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 889274
    return-void
.end method
