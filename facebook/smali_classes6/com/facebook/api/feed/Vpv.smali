.class public Lcom/facebook/api/feed/Vpv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/Vpv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829567
    new-instance v0, LX/55g;

    invoke-direct {v0}, LX/55g;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/Vpv;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 829594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829595
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/Vpv;->a:Ljava/lang/String;

    .line 829596
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/Vpv;->b:Ljava/lang/String;

    .line 829597
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/feed/Vpv;->c:I

    .line 829598
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 829588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829589
    iput-object p1, p0, Lcom/facebook/api/feed/Vpv;->a:Ljava/lang/String;

    .line 829590
    iput-object p2, p0, Lcom/facebook/api/feed/Vpv;->b:Ljava/lang/String;

    .line 829591
    iput p3, p0, Lcom/facebook/api/feed/Vpv;->c:I

    .line 829592
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829593
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 829577
    if-ne p0, p1, :cond_1

    .line 829578
    :cond_0
    :goto_0
    return v0

    .line 829579
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 829580
    goto :goto_0

    .line 829581
    :cond_3
    check-cast p1, Lcom/facebook/api/feed/Vpv;

    .line 829582
    iget v2, p0, Lcom/facebook/api/feed/Vpv;->c:I

    iget v3, p1, Lcom/facebook/api/feed/Vpv;->c:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 829583
    goto :goto_0

    .line 829584
    :cond_4
    iget-object v2, p0, Lcom/facebook/api/feed/Vpv;->a:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/api/feed/Vpv;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/api/feed/Vpv;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 829585
    goto :goto_0

    .line 829586
    :cond_6
    iget-object v2, p1, Lcom/facebook/api/feed/Vpv;->a:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 829587
    :cond_7
    iget-object v2, p0, Lcom/facebook/api/feed/Vpv;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v0, p0, Lcom/facebook/api/feed/Vpv;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/api/feed/Vpv;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_8
    iget-object v2, p1, Lcom/facebook/api/feed/Vpv;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 829572
    iget-object v0, p0, Lcom/facebook/api/feed/Vpv;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/api/feed/Vpv;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 829573
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/api/feed/Vpv;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/api/feed/Vpv;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 829574
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/facebook/api/feed/Vpv;->c:I

    add-int/2addr v0, v1

    .line 829575
    return v0

    :cond_1
    move v0, v1

    .line 829576
    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 829568
    iget-object v0, p0, Lcom/facebook/api/feed/Vpv;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829569
    iget-object v0, p0, Lcom/facebook/api/feed/Vpv;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829570
    iget v0, p0, Lcom/facebook/api/feed/Vpv;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 829571
    return-void
.end method
