.class public Lcom/facebook/api/feed/ClearCacheAfterCursorParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/ClearCacheAfterCursorParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/api/feedtype/FeedType;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829084
    new-instance v0, LX/55E;

    invoke-direct {v0}, LX/55E;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/ClearCacheAfterCursorParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 829085
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829086
    const-class v0, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feedtype/FeedType;

    iput-object v0, p0, Lcom/facebook/api/feed/ClearCacheAfterCursorParams;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 829087
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/ClearCacheAfterCursorParams;->b:Ljava/util/List;

    .line 829088
    iget-object v0, p0, Lcom/facebook/api/feed/ClearCacheAfterCursorParams;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 829089
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829083
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 829080
    iget-object v0, p0, Lcom/facebook/api/feed/ClearCacheAfterCursorParams;->a:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 829081
    iget-object v0, p0, Lcom/facebook/api/feed/ClearCacheAfterCursorParams;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 829082
    return-void
.end method
