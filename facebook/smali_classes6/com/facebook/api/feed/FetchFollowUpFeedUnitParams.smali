.class public Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/55K;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

.field public final e:I

.field private final f:Z

.field private final g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829174
    new-instance v0, LX/55J;

    invoke-direct {v0}, LX/55J;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 829175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829176
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->b:Ljava/lang/String;

    .line 829177
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->c:Ljava/lang/String;

    .line 829178
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->d:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 829179
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->e:I

    .line 829180
    const-class v0, LX/55K;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->a:LX/0Px;

    .line 829181
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->f:Z

    .line 829182
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->g:Z

    .line 829183
    return-void

    :cond_0
    move v0, v2

    .line 829184
    goto :goto_0

    :cond_1
    move v1, v2

    .line 829185
    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;)V
    .locals 1

    .prologue
    .line 829186
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;I)V

    .line 829187
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;I)V
    .locals 7

    .prologue
    .line 829188
    invoke-static {}, LX/55K;->getAllSupportedUnitTypes()LX/0Px;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;ILX/0Px;Z)V

    .line 829189
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;ILX/0Px;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;",
            "I",
            "LX/0Px",
            "<",
            "LX/55K;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 829190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829191
    iput-object p1, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->b:Ljava/lang/String;

    .line 829192
    iput-object p2, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->c:Ljava/lang/String;

    .line 829193
    iput-object p3, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->d:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 829194
    iput p4, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->e:I

    .line 829195
    iput-object p5, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->a:LX/0Px;

    .line 829196
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->f:Z

    .line 829197
    iput-boolean p6, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->g:Z

    .line 829198
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829199
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 829200
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829201
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829202
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->d:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829203
    iget v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 829204
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 829205
    iget-boolean v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->f:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 829206
    iget-boolean v0, p0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->g:Z

    if-eqz v0, :cond_1

    :goto_1
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 829207
    return-void

    :cond_0
    move v0, v2

    .line 829208
    goto :goto_0

    :cond_1
    move v1, v2

    .line 829209
    goto :goto_1
.end method
