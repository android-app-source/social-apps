.class public final Lcom/facebook/api/feed/DeleteStoryMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/DeleteStoryMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:LX/55G;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829118
    new-instance v0, LX/55F;

    invoke-direct {v0}, LX/55F;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 829111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829112
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->a:Ljava/lang/String;

    .line 829113
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->b:Ljava/util/List;

    .line 829114
    iget-object v0, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 829115
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->c:Ljava/lang/String;

    .line 829116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/55G;->valueOf(Ljava/lang/String;)LX/55G;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->d:LX/55G;

    .line 829117
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/55G;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "LX/55G;",
            ")V"
        }
    .end annotation

    .prologue
    .line 829105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829106
    iput-object p1, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->a:Ljava/lang/String;

    .line 829107
    iput-object p2, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->b:Ljava/util/List;

    .line 829108
    iput-object p3, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->c:Ljava/lang/String;

    .line 829109
    iput-object p4, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->d:LX/55G;

    .line 829110
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829104
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 829098
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "method"

    const-string v2, "DeleteStoryMethod"

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "legacyApiPostId"

    iget-object v2, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "cacheIds"

    iget-object v2, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "storyId"

    iget-object v2, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "deleteFromServer"

    iget-object v2, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->d:LX/55G;

    invoke-virtual {v2}, LX/55G;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 829099
    iget-object v0, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829100
    iget-object v0, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 829101
    iget-object v0, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829102
    iget-object v0, p0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->d:LX/55G;

    invoke-virtual {v0}, LX/55G;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829103
    return-void
.end method
