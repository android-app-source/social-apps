.class public Lcom/facebook/api/feed/MarkResearchPollCompletedParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/MarkResearchPollCompletedParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829276
    new-instance v0, LX/55P;

    invoke-direct {v0}, LX/55P;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 829277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829278
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;->a:Ljava/lang/String;

    .line 829279
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;->b:Ljava/lang/String;

    .line 829280
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 829281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829282
    iput-object p1, p0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;->a:Ljava/lang/String;

    .line 829283
    iput-object p2, p0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;->b:Ljava/lang/String;

    .line 829284
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829285
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 829286
    iget-object v0, p0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829287
    iget-object v0, p0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829288
    return-void
.end method
