.class public Lcom/facebook/api/feed/SubmitSurveyResponseResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/SubmitSurveyResponseResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829527
    new-instance v0, LX/55d;

    invoke-direct {v0}, LX/55d;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/SubmitSurveyResponseResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 829528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829529
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/SubmitSurveyResponseResult;->a:Ljava/lang/String;

    .line 829530
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/SubmitSurveyResponseResult;->b:Ljava/lang/String;

    .line 829531
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 829532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829533
    iput-object p1, p0, Lcom/facebook/api/feed/SubmitSurveyResponseResult;->a:Ljava/lang/String;

    .line 829534
    iput-object p2, p0, Lcom/facebook/api/feed/SubmitSurveyResponseResult;->b:Ljava/lang/String;

    .line 829535
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829536
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 829537
    iget-object v0, p0, Lcom/facebook/api/feed/SubmitSurveyResponseResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829538
    iget-object v0, p0, Lcom/facebook/api/feed/SubmitSurveyResponseResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829539
    return-void
.end method
