.class public Lcom/facebook/api/feed/SetVideoMuteParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/SetVideoMuteParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829426
    new-instance v0, LX/55Y;

    invoke-direct {v0}, LX/55Y;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/SetVideoMuteParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 829427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829428
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/SetVideoMuteParams;->a:Ljava/lang/String;

    .line 829429
    const-class v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/feed/SetVideoMuteParams;->b:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 829430
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/api/feed/SetVideoMuteParams;->c:Z

    .line 829431
    return-void

    .line 829432
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829433
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 829434
    iget-object v0, p0, Lcom/facebook/api/feed/SetVideoMuteParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829435
    iget-object v0, p0, Lcom/facebook/api/feed/SetVideoMuteParams;->b:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 829436
    iget-boolean v0, p0, Lcom/facebook/api/feed/SetVideoMuteParams;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 829437
    return-void

    .line 829438
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
