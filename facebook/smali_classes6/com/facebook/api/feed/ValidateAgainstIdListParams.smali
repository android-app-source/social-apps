.class public Lcom/facebook/api/feed/ValidateAgainstIdListParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/ValidateAgainstIdListParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/api/feedtype/FeedType;

.field private final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829555
    new-instance v0, LX/55f;

    invoke-direct {v0}, LX/55f;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/ValidateAgainstIdListParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 829556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829557
    const-class v0, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feedtype/FeedType;

    iput-object v0, p0, Lcom/facebook/api/feed/ValidateAgainstIdListParams;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 829558
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/feed/ValidateAgainstIdListParams;->b:I

    .line 829559
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829560
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 829561
    iget-object v0, p0, Lcom/facebook/api/feed/ValidateAgainstIdListParams;->a:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 829562
    iget v0, p0, Lcom/facebook/api/feed/ValidateAgainstIdListParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 829563
    return-void
.end method
