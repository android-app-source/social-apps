.class public final Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:I

.field public g:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829346
    new-instance v0, LX/55R;

    invoke-direct {v0}, LX/55R;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/55S;)V
    .locals 1

    .prologue
    .line 829334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829335
    iget-object v0, p1, LX/55S;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 829336
    iget-object v0, p1, LX/55S;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->b:Ljava/lang/String;

    .line 829337
    iget-object v0, p1, LX/55S;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->c:Ljava/lang/String;

    .line 829338
    iget-object v0, p1, LX/55S;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->d:Ljava/lang/String;

    .line 829339
    iget-object v0, p1, LX/55S;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->e:Ljava/lang/String;

    .line 829340
    iget v0, p1, LX/55S;->f:I

    iput v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->f:I

    .line 829341
    iget-object v0, p1, LX/55S;->g:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->g:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    .line 829342
    iget-object v0, p1, LX/55S;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->h:Ljava/lang/String;

    .line 829343
    iget-boolean v0, p1, LX/55S;->i:Z

    iput-boolean v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->i:Z

    .line 829344
    iget-object v0, p1, LX/55S;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->j:Ljava/lang/String;

    .line 829345
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 829308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829309
    const-class v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 829310
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->b:Ljava/lang/String;

    .line 829311
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->c:Ljava/lang/String;

    .line 829312
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->d:Ljava/lang/String;

    .line 829313
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->e:Ljava/lang/String;

    .line 829314
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->f:I

    .line 829315
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->g:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    .line 829316
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->h:Ljava/lang/String;

    .line 829317
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->i:Z

    .line 829318
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->j:Ljava/lang/String;

    .line 829319
    return-void

    .line 829320
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829333
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 829321
    iget-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 829322
    iget-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829323
    iget-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829324
    iget-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829325
    iget-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829326
    iget v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 829327
    iget-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->g:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 829328
    iget-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829329
    iget-boolean v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 829330
    iget-object v0, p0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829331
    return-void

    .line 829332
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
