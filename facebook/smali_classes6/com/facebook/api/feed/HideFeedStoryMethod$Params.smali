.class public final Lcom/facebook/api/feed/HideFeedStoryMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/HideFeedStoryMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/graphql/enums/StoryVisibility;

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:I

.field public h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829227
    new-instance v0, LX/55M;

    invoke-direct {v0}, LX/55M;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 829228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829229
    const-class v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 829230
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->b:Ljava/lang/String;

    .line 829231
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->c:Ljava/lang/String;

    .line 829232
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/StoryVisibility;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->d:Lcom/facebook/graphql/enums/StoryVisibility;

    .line 829233
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->e:Z

    .line 829234
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->f:Ljava/lang/String;

    .line 829235
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->g:I

    .line 829236
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->h:Ljava/lang/String;

    .line 829237
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;ZLjava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 829238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829239
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 829240
    invoke-virtual {p4}, Lcom/facebook/graphql/enums/StoryVisibility;->isHiddenOrVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 829241
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 829242
    :cond_0
    iput-object p2, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->b:Ljava/lang/String;

    .line 829243
    iput-object p3, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->c:Ljava/lang/String;

    .line 829244
    iput-object p4, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->d:Lcom/facebook/graphql/enums/StoryVisibility;

    .line 829245
    iput-boolean p5, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->e:Z

    .line 829246
    iput-object p6, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->f:Ljava/lang/String;

    .line 829247
    iput p7, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->g:I

    .line 829248
    iput-object p8, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->h:Ljava/lang/String;

    .line 829249
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829250
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 829251
    iget-object v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 829252
    iget-object v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829253
    iget-object v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829254
    iget-object v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->d:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/StoryVisibility;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829255
    iget-boolean v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 829256
    iget-object v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829257
    iget v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 829258
    iget-object v0, p0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829259
    return-void
.end method
