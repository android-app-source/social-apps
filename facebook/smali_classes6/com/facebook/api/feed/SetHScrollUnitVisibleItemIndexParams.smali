.class public Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829366
    new-instance v0, LX/55U;

    invoke-direct {v0}, LX/55U;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 829367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829368
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->a:Ljava/lang/String;

    .line 829369
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->b:Ljava/lang/String;

    .line 829370
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->c:Ljava/lang/Integer;

    .line 829371
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 829372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829373
    iput-object p1, p0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->a:Ljava/lang/String;

    .line 829374
    iput-object p2, p0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->b:Ljava/lang/String;

    .line 829375
    iput-object p3, p0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->c:Ljava/lang/Integer;

    .line 829376
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829377
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 829378
    iget-object v0, p0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829379
    iget-object v0, p0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829380
    iget-object v0, p0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 829381
    return-void
.end method
