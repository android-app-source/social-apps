.class public final Lcom/facebook/api/feed/SetImpressionCountParams$ImpressionCount;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/SetImpressionCountParams$ImpressionCount;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829396
    new-instance v0, LX/55W;

    invoke-direct {v0}, LX/55W;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/SetImpressionCountParams$ImpressionCount;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 829388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829389
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/SetImpressionCountParams$ImpressionCount;->a:Ljava/lang/String;

    .line 829390
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/feed/SetImpressionCountParams$ImpressionCount;->b:I

    .line 829391
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829395
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 829392
    iget-object v0, p0, Lcom/facebook/api/feed/SetImpressionCountParams$ImpressionCount;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829393
    iget v0, p0, Lcom/facebook/api/feed/SetImpressionCountParams$ImpressionCount;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 829394
    return-void
.end method
