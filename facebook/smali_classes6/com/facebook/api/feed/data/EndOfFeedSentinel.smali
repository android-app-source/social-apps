.class public Lcom/facebook/api/feed/data/EndOfFeedSentinel;
.super Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
.source ""

# interfaces
.implements LX/16s;


# static fields
.field private static final m:Lcom/facebook/api/feed/data/EndOfFeedSentinel$EndOfFeedSentinelFeedUnit;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829722
    new-instance v0, Lcom/facebook/api/feed/data/EndOfFeedSentinel$EndOfFeedSentinelFeedUnit;

    invoke-direct {v0}, Lcom/facebook/api/feed/data/EndOfFeedSentinel$EndOfFeedSentinelFeedUnit;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/data/EndOfFeedSentinel;->m:Lcom/facebook/api/feed/data/EndOfFeedSentinel$EndOfFeedSentinelFeedUnit;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 829717
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;-><init>()V

    .line 829718
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 829719
    const-string v0, "END_OF_FEED_DEDUP_KEY"

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 829720
    sget-object v0, Lcom/facebook/api/feed/data/EndOfFeedSentinel;->m:Lcom/facebook/api/feed/data/EndOfFeedSentinel$EndOfFeedSentinelFeedUnit;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 829721
    const-string v0, "FeedUnitCollection.END_OF_FEED_SENTINEL"

    return-object v0
.end method
