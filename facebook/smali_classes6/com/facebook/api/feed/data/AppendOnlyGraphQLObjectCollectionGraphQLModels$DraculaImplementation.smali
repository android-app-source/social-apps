.class public final Lcom/facebook/api/feed/data/AppendOnlyGraphQLObjectCollectionGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/api/feed/data/AppendOnlyGraphQLObjectCollectionGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 829713
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 829714
    return-void
.end method

.method public constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 829711
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 829712
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 829695
    if-nez p1, :cond_0

    .line 829696
    :goto_0
    return v0

    .line 829697
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 829698
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 829699
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 829700
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 829701
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 829702
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 829703
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 829704
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 829705
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 829706
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 829707
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 829708
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 829709
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 829710
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x4f3779d1
        :pswitch_0
    .end packed-switch
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 829692
    packed-switch p0, :pswitch_data_0

    .line 829693
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 829694
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x4f3779d1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 829657
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/feed/data/AppendOnlyGraphQLObjectCollectionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 829690
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/api/feed/data/AppendOnlyGraphQLObjectCollectionGraphQLModels$DraculaImplementation;->b(I)V

    .line 829691
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 829685
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 829686
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 829687
    :cond_0
    iput-object p1, p0, Lcom/facebook/api/feed/data/AppendOnlyGraphQLObjectCollectionGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 829688
    iput p2, p0, Lcom/facebook/api/feed/data/AppendOnlyGraphQLObjectCollectionGraphQLModels$DraculaImplementation;->b:I

    .line 829689
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 829684
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 829683
    new-instance v0, Lcom/facebook/api/feed/data/AppendOnlyGraphQLObjectCollectionGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/api/feed/data/AppendOnlyGraphQLObjectCollectionGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 829680
    iget v0, p0, LX/1vt;->c:I

    .line 829681
    move v0, v0

    .line 829682
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 829677
    iget v0, p0, LX/1vt;->c:I

    .line 829678
    move v0, v0

    .line 829679
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 829674
    iget v0, p0, LX/1vt;->b:I

    .line 829675
    move v0, v0

    .line 829676
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 829671
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 829672
    move-object v0, v0

    .line 829673
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 829661
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 829662
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 829663
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/api/feed/data/AppendOnlyGraphQLObjectCollectionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 829664
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 829665
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 829666
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 829667
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 829668
    new-instance v4, Lcom/facebook/api/feed/data/AppendOnlyGraphQLObjectCollectionGraphQLModels$DraculaImplementation;

    invoke-direct {v4, v3, v9, v2}, Lcom/facebook/api/feed/data/AppendOnlyGraphQLObjectCollectionGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    move-object v3, v4

    .line 829669
    move-object v0, v3

    .line 829670
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 829658
    iget v0, p0, LX/1vt;->c:I

    .line 829659
    move v0, v0

    .line 829660
    return v0
.end method
