.class public Lcom/facebook/api/feed/data/LoadingMoreSentinel;
.super Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
.source ""

# interfaces
.implements LX/16s;


# static fields
.field private static final m:Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829743
    new-instance v0, Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;

    invoke-direct {v0}, Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/data/LoadingMoreSentinel;->m:Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 829741
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;-><init>()V

    .line 829742
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 829740
    const-string v0, "LOADING_MORE_DEDUP_KEY"

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 829739
    sget-object v0, Lcom/facebook/api/feed/data/LoadingMoreSentinel;->m:Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 829738
    const-string v0, "FeedUnitCollection.LOADING_MORE_SENTINEL"

    return-object v0
.end method
