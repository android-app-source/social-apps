.class public Lcom/facebook/api/feed/SubmitResearchPollResponseParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/SubmitResearchPollResponseParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/Boolean;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829484
    new-instance v0, LX/55a;

    invoke-direct {v0}, LX/55a;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 829460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829461
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->a:Ljava/lang/String;

    .line 829462
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->b:Ljava/lang/String;

    .line 829463
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->c:Ljava/lang/Boolean;

    .line 829464
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->d:Ljava/lang/String;

    .line 829465
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->e:Ljava/util/List;

    .line 829466
    iget-object v0, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->e:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 829467
    return-void

    .line 829468
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 829477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829478
    iput-object p1, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->a:Ljava/lang/String;

    .line 829479
    iput-object p2, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->b:Ljava/lang/String;

    .line 829480
    iput-object p3, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->c:Ljava/lang/Boolean;

    .line 829481
    iput-object p4, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->d:Ljava/lang/String;

    .line 829482
    iput-object p5, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->e:Ljava/util/List;

    .line 829483
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829476
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 829469
    iget-object v0, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829470
    iget-object v0, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829471
    iget-object v0, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 829472
    iget-object v0, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829473
    iget-object v0, p0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;->e:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 829474
    return-void

    .line 829475
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
