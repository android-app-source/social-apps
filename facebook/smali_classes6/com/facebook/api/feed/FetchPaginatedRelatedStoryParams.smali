.class public Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 829223
    new-instance v0, LX/55L;

    invoke-direct {v0}, LX/55L;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 829213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829214
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;->a:Ljava/lang/String;

    .line 829215
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;->b:Ljava/lang/String;

    .line 829216
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;->c:I

    .line 829217
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 829222
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 829218
    iget-object v0, p0, Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829219
    iget-object v0, p0, Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 829220
    iget v0, p0, Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 829221
    return-void
.end method
