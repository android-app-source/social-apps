.class public Lcom/facebook/api/ufiservices/FetchSingleCommentParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/FetchSingleCommentParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:Ljava/lang/String;

.field public h:Lcom/facebook/graphql/model/GraphQLComment;

.field public i:Lcom/facebook/graphql/model/GraphQLComment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;

.field public k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1061857
    new-instance v0, LX/6BL;

    invoke-direct {v0}, LX/6BL;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6BM;)V
    .locals 1

    .prologue
    .line 1061858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1061859
    iget-object v0, p1, LX/6BM;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->a:Ljava/lang/String;

    .line 1061860
    iget-object v0, p1, LX/6BM;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->b:Ljava/lang/String;

    .line 1061861
    iget v0, p1, LX/6BM;->c:I

    iput v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->c:I

    .line 1061862
    iget-object v0, p1, LX/6BM;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->d:Ljava/lang/String;

    .line 1061863
    iget-object v0, p1, LX/6BM;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->e:Ljava/lang/String;

    .line 1061864
    iget-boolean v0, p1, LX/6BM;->f:Z

    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->f:Z

    .line 1061865
    iget-object v0, p1, LX/6BM;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->g:Ljava/lang/String;

    .line 1061866
    iget-object v0, p1, LX/6BM;->h:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->h:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1061867
    iget-object v0, p1, LX/6BM;->i:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->i:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1061868
    iget-object v0, p1, LX/6BM;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->j:Ljava/lang/String;

    .line 1061869
    iget-boolean v0, p1, LX/6BM;->k:Z

    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->k:Z

    .line 1061870
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1061871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1061872
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->a:Ljava/lang/String;

    .line 1061873
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->b:Ljava/lang/String;

    .line 1061874
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->c:I

    .line 1061875
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->d:Ljava/lang/String;

    .line 1061876
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->e:Ljava/lang/String;

    .line 1061877
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->f:Z

    .line 1061878
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->g:Ljava/lang/String;

    .line 1061879
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->h:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1061880
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->i:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1061881
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->j:Ljava/lang/String;

    .line 1061882
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->k:Z

    .line 1061883
    return-void

    .line 1061884
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1061885
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1061886
    iget-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1061887
    iget-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1061888
    iget v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1061889
    iget-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1061890
    iget-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1061891
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1061892
    iget-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1061893
    iget-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->h:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1061894
    iget-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->i:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1061895
    iget-object v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1061896
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->k:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1061897
    return-void

    .line 1061898
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
