.class public Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/ipc/media/MediaItem;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1061790
    new-instance v0, LX/6BF;

    invoke-direct {v0}, LX/6BF;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6BG;)V
    .locals 1

    .prologue
    .line 1061791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1061792
    iget-object v0, p1, LX/6BG;->a:Lcom/facebook/ipc/media/MediaItem;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 1061793
    iget-object v0, p1, LX/6BG;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->b:Ljava/lang/String;

    .line 1061794
    iget-object v0, p1, LX/6BG;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1061795
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1061796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1061797
    const-class v0, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 1061798
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->b:Ljava/lang/String;

    .line 1061799
    const-class v0, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1061800
    return-void
.end method

.method public static a()LX/6BG;
    .locals 1

    .prologue
    .line 1061801
    new-instance v0, LX/6BG;

    invoke-direct {v0}, LX/6BG;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1061802
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1061803
    iget-object v0, p0, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->a:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1061804
    iget-object v0, p0, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1061805
    iget-object v0, p0, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1061806
    return-void
.end method
