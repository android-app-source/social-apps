.class public Lcom/facebook/api/ufiservices/common/EditCommentParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/common/EditCommentParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/facebook/graphql/model/GraphQLComment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 889394
    new-instance v0, LX/5Gv;

    invoke-direct {v0}, LX/5Gv;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 889395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889396
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->a:Ljava/lang/String;

    .line 889397
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->b:Ljava/lang/String;

    .line 889398
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->c:Ljava/lang/String;

    .line 889399
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->d:Ljava/lang/String;

    .line 889400
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->e:Lcom/facebook/graphql/model/GraphQLComment;

    .line 889401
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 0

    .prologue
    .line 889402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889403
    iput-object p1, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->a:Ljava/lang/String;

    .line 889404
    iput-object p2, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->b:Ljava/lang/String;

    .line 889405
    iput-object p3, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->c:Ljava/lang/String;

    .line 889406
    iput-object p4, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->d:Ljava/lang/String;

    .line 889407
    iput-object p5, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->e:Lcom/facebook/graphql/model/GraphQLComment;

    .line 889408
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 889409
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 889410
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889411
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889412
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889413
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889414
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->e:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 889415
    return-void
.end method
