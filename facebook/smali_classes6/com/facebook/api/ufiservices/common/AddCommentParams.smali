.class public Lcom/facebook/api/ufiservices/common/AddCommentParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/common/AddCommentParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/graphql/model/GraphQLComment;

.field public final f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Z

.field public final k:I

.field public final l:Lcom/facebook/auth/viewercontext/ViewerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 889310
    new-instance v0, LX/5Gq;

    invoke-direct {v0}, LX/5Gq;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5Gr;)V
    .locals 1

    .prologue
    .line 889311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889312
    iget-object v0, p1, LX/5Gr;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->a:Ljava/lang/String;

    .line 889313
    iget-object v0, p1, LX/5Gr;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->b:Ljava/lang/String;

    .line 889314
    iget-object v0, p1, LX/5Gr;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->c:Ljava/lang/String;

    .line 889315
    iget-object v0, p1, LX/5Gr;->d:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->e:Lcom/facebook/graphql/model/GraphQLComment;

    .line 889316
    iget-object v0, p1, LX/5Gr;->e:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 889317
    iget-object v0, p1, LX/5Gr;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->g:Ljava/lang/String;

    .line 889318
    iget-object v0, p1, LX/5Gr;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->d:Ljava/lang/String;

    .line 889319
    iget-object v0, p1, LX/5Gr;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->i:Ljava/lang/String;

    .line 889320
    iget-object v0, p1, LX/5Gr;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->h:Ljava/lang/String;

    .line 889321
    iget-object v0, p1, LX/5Gr;->j:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->l:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 889322
    iget-boolean v0, p1, LX/5Gr;->k:Z

    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->j:Z

    .line 889323
    iget v0, p1, LX/5Gr;->l:I

    iput v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->k:I

    .line 889324
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 889325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889326
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->a:Ljava/lang/String;

    .line 889327
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->b:Ljava/lang/String;

    .line 889328
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->c:Ljava/lang/String;

    .line 889329
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->e:Lcom/facebook/graphql/model/GraphQLComment;

    .line 889330
    const-class v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 889331
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->g:Ljava/lang/String;

    .line 889332
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->d:Ljava/lang/String;

    .line 889333
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->h:Ljava/lang/String;

    .line 889334
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->i:Ljava/lang/String;

    .line 889335
    const-class v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->l:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 889336
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->j:Z

    .line 889337
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->k:I

    .line 889338
    return-void

    .line 889339
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a()LX/5Gr;
    .locals 1

    .prologue
    .line 889340
    new-instance v0, LX/5Gr;

    invoke-direct {v0}, LX/5Gr;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 889341
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 889342
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889343
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889344
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889345
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->e:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 889346
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 889347
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889348
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889349
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889350
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889351
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->l:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 889352
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 889353
    iget v0, p0, Lcom/facebook/api/ufiservices/common/AddCommentParams;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 889354
    return-void

    .line 889355
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
