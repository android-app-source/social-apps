.class public Lcom/facebook/api/ufiservices/common/ToggleLikeParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/common/ToggleLikeParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:Lcom/facebook/graphql/model/GraphQLActor;

.field public final d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public final e:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 889590
    new-instance v0, LX/5H6;

    invoke-direct {v0}, LX/5H6;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 889591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889592
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->a:Ljava/lang/String;

    .line 889593
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 889594
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->b:Z

    .line 889595
    const-class v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 889596
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 889597
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->f:Ljava/lang/String;

    .line 889598
    return-void

    .line 889599
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V
    .locals 0
    .param p5    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 889600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889601
    iput-object p1, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->a:Ljava/lang/String;

    .line 889602
    iput-boolean p2, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->b:Z

    .line 889603
    iput-object p3, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 889604
    iput-object p4, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 889605
    iput-object p5, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 889606
    iput-object p6, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->f:Ljava/lang/String;

    .line 889607
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 889608
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 889609
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889610
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 889611
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 889612
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 889613
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 889614
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889615
    return-void

    .line 889616
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
