.class public Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:Lcom/facebook/graphql/model/GraphQLActor;

.field public final d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 889634
    new-instance v0, LX/5H7;

    invoke-direct {v0}, LX/5H7;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 889635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889636
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a:Ljava/lang/String;

    .line 889637
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 889638
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->b:Z

    .line 889639
    const-class v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 889640
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->e:Ljava/lang/String;

    .line 889641
    return-void

    .line 889642
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 889643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889644
    iput-object p1, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a:Ljava/lang/String;

    .line 889645
    iput-boolean p2, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->b:Z

    .line 889646
    iput-object p3, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 889647
    iput-object p4, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 889648
    iput-object p5, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->e:Ljava/lang/String;

    .line 889649
    return-void
.end method

.method public static a()LX/5H8;
    .locals 1

    .prologue
    .line 889650
    new-instance v0, LX/5H8;

    invoke-direct {v0}, LX/5H8;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 889651
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 889652
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889653
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 889654
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 889655
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 889656
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889657
    return-void

    .line 889658
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
