.class public Lcom/facebook/api/ufiservices/common/FetchNodeListParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/common/FetchNodeListParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:LX/0rS;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 889440
    new-instance v0, LX/5Gx;

    invoke-direct {v0}, LX/5Gx;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 889433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889434
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->a:Ljava/lang/String;

    .line 889435
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->b:I

    .line 889436
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->c:Ljava/lang/String;

    .line 889437
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->d:Ljava/lang/String;

    .line 889438
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->e:LX/0rS;

    .line 889439
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0rS;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 889426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889427
    iput-object p1, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->a:Ljava/lang/String;

    .line 889428
    iput p2, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->b:I

    .line 889429
    iput-object p3, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->c:Ljava/lang/String;

    .line 889430
    iput-object p4, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->d:Ljava/lang/String;

    .line 889431
    iput-object p5, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->e:LX/0rS;

    .line 889432
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 889425
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 889419
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889420
    iget v0, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 889421
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889422
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889423
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->e:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889424
    return-void
.end method
