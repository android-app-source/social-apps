.class public Lcom/facebook/api/ufiservices/common/FetchReactionsParams;
.super Lcom/facebook/api/ufiservices/common/FetchNodeListParams;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/common/FetchReactionsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:Z

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 889474
    new-instance v0, LX/5H0;

    invoke-direct {v0}, LX/5H0;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 889475
    invoke-direct {p0, p1}, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;-><init>(Landroid/os/Parcel;)V

    .line 889476
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->a:I

    .line 889477
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->b:Z

    .line 889478
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->c:Z

    .line 889479
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/1zt;ZILjava/lang/String;Ljava/lang/String;LX/0rS;)V
    .locals 9
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 889480
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;-><init>(Ljava/lang/String;LX/1zt;ZILjava/lang/String;Ljava/lang/String;LX/0rS;Z)V

    .line 889481
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/1zt;ZILjava/lang/String;Ljava/lang/String;LX/0rS;Z)V
    .locals 6
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 889482
    move-object v0, p0

    move-object v1, p1

    move v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0rS;)V

    .line 889483
    iget v0, p2, LX/1zt;->e:I

    move v0, v0

    .line 889484
    iput v0, p0, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->a:I

    .line 889485
    iput-boolean p3, p0, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->b:Z

    .line 889486
    iput-boolean p8, p0, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->c:Z

    .line 889487
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 889488
    invoke-super {p0, p1, p2}, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 889489
    iget v0, p0, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 889490
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 889491
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 889492
    return-void
.end method
