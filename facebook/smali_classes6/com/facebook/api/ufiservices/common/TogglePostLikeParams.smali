.class public Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Z

.field public final c:Lcom/facebook/graphql/model/GraphQLActor;

.field public final d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public final e:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 889673
    new-instance v0, LX/5H9;

    invoke-direct {v0}, LX/5H9;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 889674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889675
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->a:Ljava/lang/String;

    .line 889676
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 889677
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->b:Z

    .line 889678
    const-class v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 889679
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 889680
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->f:Ljava/lang/String;

    .line 889681
    return-void

    .line 889682
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 889683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889684
    iput-object p1, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->a:Ljava/lang/String;

    .line 889685
    iput-boolean p2, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->b:Z

    .line 889686
    iput-object p3, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 889687
    iput-object p4, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 889688
    iput-object p5, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 889689
    iput-object p6, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->f:Ljava/lang/String;

    .line 889690
    return-void
.end method

.method public static a()LX/5HA;
    .locals 2

    .prologue
    .line 889691
    new-instance v0, LX/5HA;

    invoke-direct {v0}, LX/5HA;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final b()Lcom/facebook/api/ufiservices/common/ToggleLikeParams;
    .locals 7

    .prologue
    .line 889692
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 889693
    iget-object v1, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->a:Ljava/lang/String;

    .line 889694
    :goto_0
    new-instance v0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;

    iget-boolean v2, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->b:Z

    iget-object v3, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    iget-object v4, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iget-object v5, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;-><init>(Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V

    return-object v0

    .line 889695
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_1

    .line 889696
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 889697
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "missing both likeableId and legacyApiPostid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 889698
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 889699
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889700
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 889701
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 889702
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 889703
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 889704
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889705
    return-void

    .line 889706
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
