.class public Lcom/facebook/api/ufiservices/common/DeleteCommentParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/common/DeleteCommentParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 889369
    new-instance v0, LX/5Gt;

    invoke-direct {v0}, LX/5Gt;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5Gu;)V
    .locals 1

    .prologue
    .line 889370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889371
    iget-object v0, p1, LX/5Gu;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->a:Ljava/lang/String;

    .line 889372
    iget-object v0, p1, LX/5Gu;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->b:Ljava/lang/String;

    .line 889373
    iget-object v0, p1, LX/5Gu;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->c:Ljava/lang/String;

    .line 889374
    iget-object v0, p1, LX/5Gu;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->d:Ljava/lang/String;

    .line 889375
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 889376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889377
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->a:Ljava/lang/String;

    .line 889378
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->b:Ljava/lang/String;

    .line 889379
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->c:Ljava/lang/String;

    .line 889380
    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    move-result v0

    if-lez v0, :cond_0

    .line 889381
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->d:Ljava/lang/String;

    .line 889382
    :goto_0
    return-void

    .line 889383
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->d:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 889384
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 889385
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889386
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889387
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889388
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 889389
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889390
    :cond_0
    return-void
.end method
