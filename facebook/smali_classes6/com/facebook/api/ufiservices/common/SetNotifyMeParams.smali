.class public Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 889586
    new-instance v0, LX/5H4;

    invoke-direct {v0}, LX/5H4;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5H5;)V
    .locals 1

    .prologue
    .line 889572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889573
    iget-object v0, p1, LX/5H5;->b:Ljava/lang/String;

    move-object v0, v0

    .line 889574
    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->a:Ljava/lang/String;

    .line 889575
    iget-object v0, p1, LX/5H5;->a:Ljava/lang/String;

    move-object v0, v0

    .line 889576
    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->b:Ljava/lang/String;

    .line 889577
    iget-object v0, p1, LX/5H5;->c:Ljava/lang/String;

    move-object v0, v0

    .line 889578
    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->c:Ljava/lang/String;

    .line 889579
    iget-boolean v0, p1, LX/5H5;->d:Z

    move v0, v0

    .line 889580
    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->e:Z

    .line 889581
    iget-object v0, p1, LX/5H5;->e:Ljava/lang/String;

    move-object v0, v0

    .line 889582
    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->d:Ljava/lang/String;

    .line 889583
    iget-object v0, p1, LX/5H5;->f:Ljava/lang/String;

    move-object v0, v0

    .line 889584
    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->f:Ljava/lang/String;

    .line 889585
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 889564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889565
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->a:Ljava/lang/String;

    .line 889566
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->b:Ljava/lang/String;

    .line 889567
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->c:Ljava/lang/String;

    .line 889568
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->d:Ljava/lang/String;

    .line 889569
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->e:Z

    .line 889570
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->f:Ljava/lang/String;

    .line 889571
    return-void
.end method

.method public static f()LX/5H5;
    .locals 2

    .prologue
    .line 889563
    new-instance v0, LX/5H5;

    invoke-direct {v0}, LX/5H5;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 889562
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 889555
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889556
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889557
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889558
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889559
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 889560
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889561
    return-void
.end method
