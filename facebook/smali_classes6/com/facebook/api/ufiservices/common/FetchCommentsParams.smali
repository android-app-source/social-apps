.class public Lcom/facebook/api/ufiservices/common/FetchCommentsParams;
.super Lcom/facebook/api/ufiservices/common/FetchNodeListParams;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/common/FetchCommentsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/21y;

.field public final b:LX/5Gs;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 889441
    new-instance v0, LX/5Gw;

    invoke-direct {v0}, LX/5Gw;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/common/FetchCommentsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 889442
    invoke-direct {p0, p1}, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;-><init>(Landroid/os/Parcel;)V

    .line 889443
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/21y;->getOrder(Ljava/lang/String;)LX/21y;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchCommentsParams;->a:LX/21y;

    .line 889444
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5Gs;->valueOf(Ljava/lang/String;)LX/5Gs;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchCommentsParams;->b:LX/5Gs;

    .line 889445
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0rS;LX/21y;LX/5Gs;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 889446
    invoke-direct/range {p0 .. p5}, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0rS;)V

    .line 889447
    iput-object p6, p0, Lcom/facebook/api/ufiservices/common/FetchCommentsParams;->a:LX/21y;

    .line 889448
    iput-object p7, p0, Lcom/facebook/api/ufiservices/common/FetchCommentsParams;->b:LX/5Gs;

    .line 889449
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 889450
    invoke-super {p0, p1, p2}, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 889451
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchCommentsParams;->a:LX/21y;

    invoke-virtual {v0}, LX/21y;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889452
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchCommentsParams;->b:LX/5Gs;

    invoke-virtual {v0}, LX/5Gs;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889453
    return-void
.end method
