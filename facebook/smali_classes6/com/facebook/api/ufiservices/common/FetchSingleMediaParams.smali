.class public final Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:LX/5H2;

.field public final d:LX/0rS;

.field public final e:LX/21y;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 889503
    new-instance v0, LX/5H1;

    invoke-direct {v0}, LX/5H1;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 889504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889505
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->a:Ljava/lang/String;

    .line 889506
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->b:I

    .line 889507
    invoke-static {}, LX/5H2;->values()[LX/5H2;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->c:LX/5H2;

    .line 889508
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->d:LX/0rS;

    .line 889509
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/21y;->getOrder(Ljava/lang/String;)LX/21y;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->e:LX/21y;

    .line 889510
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILX/5H2;LX/0rS;LX/21y;)V
    .locals 0

    .prologue
    .line 889511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889512
    iput-object p1, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->a:Ljava/lang/String;

    .line 889513
    iput p2, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->b:I

    .line 889514
    iput-object p3, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->c:LX/5H2;

    .line 889515
    iput-object p4, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->d:LX/0rS;

    .line 889516
    iput-object p5, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->e:LX/21y;

    .line 889517
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 889518
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 889519
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889520
    iget v0, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 889521
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->c:LX/5H2;

    invoke-virtual {v0}, LX/5H2;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 889522
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->d:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889523
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->e:LX/21y;

    invoke-virtual {v0}, LX/21y;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 889524
    return-void
.end method
