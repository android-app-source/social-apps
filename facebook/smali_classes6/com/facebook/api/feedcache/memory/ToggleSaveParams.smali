.class public Lcom/facebook/api/feedcache/memory/ToggleSaveParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feedcache/memory/ToggleSaveParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation
.end field

.field public final h:Lcom/facebook/graphql/enums/GraphQLObjectType;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1058418
    new-instance v0, LX/69u;

    invoke-direct {v0}, LX/69u;-><init>()V

    sput-object v0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/69v;)V
    .locals 1

    .prologue
    .line 1058398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058399
    iget-object v0, p1, LX/69v;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->a:Ljava/lang/String;

    .line 1058400
    iget-object v0, p1, LX/69v;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->b:Ljava/lang/String;

    .line 1058401
    iget-object v0, p1, LX/69v;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->c:Ljava/lang/String;

    .line 1058402
    iget-object v0, p1, LX/69v;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->d:Ljava/lang/String;

    .line 1058403
    iget-boolean v0, p1, LX/69v;->e:Z

    iput-boolean v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->e:Z

    .line 1058404
    iget-object v0, p1, LX/69v;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->f:Ljava/lang/String;

    .line 1058405
    iget-object v0, p1, LX/69v;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->g:Ljava/lang/String;

    .line 1058406
    iget-object v0, p1, LX/69v;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->h:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1058407
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1058408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058409
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->a:Ljava/lang/String;

    .line 1058410
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->b:Ljava/lang/String;

    .line 1058411
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->c:Ljava/lang/String;

    .line 1058412
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->d:Ljava/lang/String;

    .line 1058413
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->e:Z

    .line 1058414
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->f:Ljava/lang/String;

    .line 1058415
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->g:Ljava/lang/String;

    .line 1058416
    const-class v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->h:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1058417
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/graphql/enums/GraphQLObjectType;)LX/69v;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 1058397
    new-instance v0, LX/69v;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, LX/69v;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/graphql/enums/GraphQLObjectType;B)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1058396
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1058387
    iget-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1058388
    iget-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1058389
    iget-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1058390
    iget-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1058391
    iget-boolean v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1058392
    iget-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1058393
    iget-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1058394
    iget-object v0, p0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->h:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1058395
    return-void
.end method
