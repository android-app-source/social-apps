.class public Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1058422
    new-instance v0, LX/69w;

    invoke-direct {v0}, LX/69w;-><init>()V

    sput-object v0, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1058423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058424
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;->a:Ljava/lang/String;

    .line 1058425
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;->b:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 1058426
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;->c:LX/0Px;

    .line 1058427
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)V
    .locals 5
    .param p2    # Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1058428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058429
    iput-object p1, p0, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;->a:Ljava/lang/String;

    .line 1058430
    iput-object p2, p0, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;->b:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 1058431
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1058432
    if-eqz p2, :cond_0

    .line 1058433
    invoke-static {p2}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    .line 1058434
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1058435
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1058436
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;->c:LX/0Px;

    .line 1058437
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1058438
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1058439
    iget-object v0, p0, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1058440
    iget-object v0, p0, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;->b:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1058441
    iget-object v0, p0, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1058442
    return-void
.end method
