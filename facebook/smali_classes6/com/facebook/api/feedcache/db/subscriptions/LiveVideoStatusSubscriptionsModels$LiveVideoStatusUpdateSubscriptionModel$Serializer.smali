.class public final Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1058165
    const-class v0, Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel;

    new-instance v1, Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1058166
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1058167
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1058168
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1058169
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1058170
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1058171
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1058172
    if-eqz v2, :cond_0

    .line 1058173
    const-string p0, "video"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1058174
    invoke-static {v1, v2, p1}, LX/7YH;->a(LX/15i;ILX/0nX;)V

    .line 1058175
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1058176
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1058177
    check-cast p1, Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel$Serializer;->a(Lcom/facebook/api/feedcache/db/subscriptions/LiveVideoStatusSubscriptionsModels$LiveVideoStatusUpdateSubscriptionModel;LX/0nX;LX/0my;)V

    return-void
.end method
