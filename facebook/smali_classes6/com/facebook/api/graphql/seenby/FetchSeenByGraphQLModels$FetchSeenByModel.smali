.class public final Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/3cm;
.implements LX/6Av;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5eaa57d9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:I

.field private F:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Z

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Z

.field private u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1061078
    const-class v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1060945
    const-class v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1060946
    const/16 v0, 0x1c

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1060947
    return-void
.end method

.method private A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060948
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 1060949
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    return-object v0
.end method

.method private B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060950
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 1060951
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    return-object v0
.end method

.method private C()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060952
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->D:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->D:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 1060953
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->D:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private D()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060954
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->F:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->F:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 1060955
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->F:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060956
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1060957
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1060958
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private u()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getComments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060959
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1060960
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->o:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private v()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060961
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->s:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->s:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 1060962
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->s:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    return-object v0
.end method

.method private w()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1061080
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->v:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->v:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 1061081
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->v:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private x()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060963
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1060964
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    return-object v0
.end method

.method private y()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1060965
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->z:Ljava/util/List;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->z:Ljava/util/List;

    .line 1060966
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->z:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060967
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 1060968
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 19

    .prologue
    .line 1060969
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1060970
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->t()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1060971
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->u()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x61c682db

    invoke-static {v4, v3, v5}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1060972
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->l()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1060973
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->n()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1060974
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->v()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1060975
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->p()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1060976
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->w()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1060977
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->x()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1060978
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->q()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1060979
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->s()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1060980
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->y()LX/0Px;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v12

    .line 1060981
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1060982
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1060983
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1060984
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->C()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1060985
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->D()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1060986
    const/16 v18, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1060987
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1060988
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->f:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1060989
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->g:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1060990
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->h:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1060991
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->i:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1060992
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->j:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1060993
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->k:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1060994
    const/4 v2, 0x7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->l:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1060995
    const/16 v2, 0x8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->m:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1060996
    const/16 v2, 0x9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->n:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1060997
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1060998
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1060999
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1061000
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1061001
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1061002
    const/16 v2, 0xf

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->t:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1061003
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1061004
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1061005
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1061006
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1061007
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1061008
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1061009
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1061010
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1061011
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1061012
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1061013
    const/16 v2, 0x1a

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->E:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1061014
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1061015
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1061016
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1061017
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1061018
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->u()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1061019
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->u()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x61c682db

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1061020
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->u()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1061021
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    .line 1061022
    iput v3, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->o:I

    move-object v1, v0

    .line 1061023
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->v()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1061024
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->v()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 1061025
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->v()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1061026
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    .line 1061027
    iput-object v0, v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->s:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 1061028
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->w()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1061029
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->w()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 1061030
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->w()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1061031
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    .line 1061032
    iput-object v0, v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->v:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 1061033
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->x()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1061034
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->x()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1061035
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->x()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1061036
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    .line 1061037
    iput-object v0, v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1061038
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->s()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1061039
    invoke-virtual {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->s()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    .line 1061040
    invoke-virtual {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->s()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1061041
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    .line 1061042
    iput-object v0, v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->y:Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    .line 1061043
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1061044
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->y()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1061045
    if-eqz v2, :cond_5

    .line 1061046
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    .line 1061047
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->z:Ljava/util/List;

    move-object v1, v0

    .line 1061048
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1061049
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 1061050
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1061051
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    .line 1061052
    iput-object v0, v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 1061053
    :cond_6
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1061054
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 1061055
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1061056
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    .line 1061057
    iput-object v0, v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 1061058
    :cond_7
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1061059
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 1061060
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1061061
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    .line 1061062
    iput-object v0, v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 1061063
    :cond_8
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->C()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1061064
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->C()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 1061065
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->C()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1061066
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    .line 1061067
    iput-object v0, v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->D:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 1061068
    :cond_9
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->D()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1061069
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->D()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 1061070
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->D()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1061071
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    .line 1061072
    iput-object v0, v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->F:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 1061073
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1061074
    if-nez v1, :cond_b

    :goto_0
    return-object p0

    .line 1061075
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_b
    move-object p0, v1

    .line 1061076
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1061077
    new-instance v0, LX/6Ay;

    invoke-direct {v0, p1}, LX/6Ay;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1061079
    invoke-virtual {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1060928
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1060929
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->f:Z

    .line 1060930
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->g:Z

    .line 1060931
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->h:Z

    .line 1060932
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->i:Z

    .line 1060933
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->j:Z

    .line 1060934
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->k:Z

    .line 1060935
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->l:Z

    .line 1060936
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->m:Z

    .line 1060937
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->n:Z

    .line 1060938
    const/16 v0, 0xa

    const v1, 0x61c682db

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->o:I

    .line 1060939
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->q:Z

    .line 1060940
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->t:Z

    .line 1060941
    const/16 v0, 0x1a

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->E:I

    .line 1060942
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1060943
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1060944
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1060891
    return-void
.end method

.method public final ac_()Z
    .locals 2

    .prologue
    .line 1060892
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1060893
    iget-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->j:Z

    return v0
.end method

.method public final ad_()Z
    .locals 2

    .prologue
    .line 1060894
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1060895
    iget-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->k:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1060925
    new-instance v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;-><init>()V

    .line 1060926
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1060927
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1060896
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1060897
    iget-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->f:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1060898
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1060899
    iget-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->g:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1060900
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1060901
    iget-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->h:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1060902
    const v0, 0x21d36df1

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1060903
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1060904
    iget-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->i:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1060905
    const v0, 0x252222

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 1060906
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1060907
    iget-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->l:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1060908
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1060909
    iget-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->n:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060910
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->p:Ljava/lang/String;

    .line 1060911
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 1060912
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1060913
    iget-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->q:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060914
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->r:Ljava/lang/String;

    .line 1060915
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 1060916
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1060917
    iget-boolean v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->t:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060918
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->u:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->u:Ljava/lang/String;

    .line 1060919
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060920
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->x:Ljava/lang/String;

    .line 1060921
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic r()LX/59N;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060922
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060923
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->y:Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->y:Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    .line 1060924
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->y:Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    return-object v0
.end method
