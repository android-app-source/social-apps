.class public final Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/6Av;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4621a2ad
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1061224
    const-class v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1061223
    const-class v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1061221
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1061222
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1061219
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;->e:Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;->e:Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    .line 1061220
    iget-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;->e:Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1061213
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1061214
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;->a()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1061215
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1061216
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1061217
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1061218
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1061225
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1061226
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;->a()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1061227
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;->a()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    .line 1061228
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;->a()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1061229
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;

    .line 1061230
    iput-object v0, v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;->e:Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    .line 1061231
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1061232
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1061212
    new-instance v0, LX/6Az;

    invoke-direct {v0, p1}, LX/6Az;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1061204
    const-string v0, "seen_by.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1061205
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;->a()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    move-result-object v0

    .line 1061206
    if-eqz v0, :cond_0

    .line 1061207
    invoke-virtual {v0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1061208
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1061209
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1061210
    :goto_0
    return-void

    .line 1061211
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1061195
    const-string v0, "seen_by.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1061196
    invoke-direct {p0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;->a()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    move-result-object v0

    .line 1061197
    if-eqz v0, :cond_0

    .line 1061198
    if-eqz p3, :cond_1

    .line 1061199
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    .line 1061200
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;->a(I)V

    .line 1061201
    iput-object v0, p0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;->e:Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    .line 1061202
    :cond_0
    :goto_0
    return-void

    .line 1061203
    :cond_1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;->a(I)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1061192
    new-instance v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel;-><init>()V

    .line 1061193
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1061194
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1061191
    const v0, 0x52a8534d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1061190
    const v0, -0x78fb05b

    return v0
.end method
