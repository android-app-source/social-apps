.class public final Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1060764
    const-class v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    new-instance v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1060765
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1060767
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1060768
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1060769
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v4, 0x0

    .line 1060770
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1060771
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1060772
    if-eqz v2, :cond_0

    .line 1060773
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060774
    invoke-static {v1, v0, v4, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1060775
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1060776
    if-eqz v2, :cond_1

    .line 1060777
    const-string v3, "can_page_viewer_invite_post_likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060778
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1060779
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1060780
    if-eqz v2, :cond_2

    .line 1060781
    const-string v3, "can_see_voice_switcher"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060782
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1060783
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1060784
    if-eqz v2, :cond_3

    .line 1060785
    const-string v3, "can_viewer_comment"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060786
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1060787
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1060788
    if-eqz v2, :cond_4

    .line 1060789
    const-string v3, "can_viewer_comment_with_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060790
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1060791
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1060792
    if-eqz v2, :cond_5

    .line 1060793
    const-string v3, "can_viewer_comment_with_sticker"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060794
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1060795
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1060796
    if-eqz v2, :cond_6

    .line 1060797
    const-string v3, "can_viewer_comment_with_video"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060798
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1060799
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1060800
    if-eqz v2, :cond_7

    .line 1060801
    const-string v3, "can_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060802
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1060803
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1060804
    if-eqz v2, :cond_8

    .line 1060805
    const-string v3, "can_viewer_react"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060806
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1060807
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1060808
    if-eqz v2, :cond_9

    .line 1060809
    const-string v3, "can_viewer_subscribe"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060810
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1060811
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060812
    if-eqz v2, :cond_b

    .line 1060813
    const-string v3, "comments"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060814
    const/4 v3, 0x0

    .line 1060815
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1060816
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1060817
    if-eqz v3, :cond_a

    .line 1060818
    const-string p0, "count"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060819
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1060820
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1060821
    :cond_b
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1060822
    if-eqz v2, :cond_c

    .line 1060823
    const-string v3, "comments_mirroring_domain"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060824
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1060825
    :cond_c
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1060826
    if-eqz v2, :cond_d

    .line 1060827
    const-string v3, "does_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060828
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1060829
    :cond_d
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1060830
    if-eqz v2, :cond_e

    .line 1060831
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060832
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1060833
    :cond_e
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060834
    if-eqz v2, :cond_f

    .line 1060835
    const-string v3, "important_reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060836
    invoke-static {v1, v2, p1, p2}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1060837
    :cond_f
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1060838
    if-eqz v2, :cond_10

    .line 1060839
    const-string v3, "is_viewer_subscribed"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060840
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1060841
    :cond_10
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1060842
    if-eqz v2, :cond_11

    .line 1060843
    const-string v3, "legacy_api_post_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060844
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1060845
    :cond_11
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060846
    if-eqz v2, :cond_12

    .line 1060847
    const-string v3, "like_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060848
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1060849
    :cond_12
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060850
    if-eqz v2, :cond_13

    .line 1060851
    const-string v3, "reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060852
    invoke-static {v1, v2, p1}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 1060853
    :cond_13
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1060854
    if-eqz v2, :cond_14

    .line 1060855
    const-string v3, "remixable_photo_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060856
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1060857
    :cond_14
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060858
    if-eqz v2, :cond_15

    .line 1060859
    const-string v3, "seen_by"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060860
    invoke-static {v1, v2, p1, p2}, LX/6B4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1060861
    :cond_15
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060862
    if-eqz v2, :cond_16

    .line 1060863
    const-string v3, "supported_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060864
    invoke-static {v1, v2, p1, p2}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1060865
    :cond_16
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060866
    if-eqz v2, :cond_17

    .line 1060867
    const-string v3, "top_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060868
    invoke-static {v1, v2, p1, p2}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1060869
    :cond_17
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060870
    if-eqz v2, :cond_18

    .line 1060871
    const-string v3, "viewer_acts_as_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060872
    invoke-static {v1, v2, p1, p2}, LX/5AX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1060873
    :cond_18
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060874
    if-eqz v2, :cond_19

    .line 1060875
    const-string v3, "viewer_acts_as_person"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060876
    invoke-static {v1, v2, p1}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 1060877
    :cond_19
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060878
    if-eqz v2, :cond_1a

    .line 1060879
    const-string v3, "viewer_does_not_like_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060880
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1060881
    :cond_1a
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 1060882
    if-eqz v2, :cond_1b

    .line 1060883
    const-string v3, "viewer_feedback_reaction_key"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060884
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1060885
    :cond_1b
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060886
    if-eqz v2, :cond_1c

    .line 1060887
    const-string v3, "viewer_likes_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060888
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1060889
    :cond_1c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1060890
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1060766
    check-cast p1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel$Serializer;->a(Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;LX/0nX;LX/0my;)V

    return-void
.end method
