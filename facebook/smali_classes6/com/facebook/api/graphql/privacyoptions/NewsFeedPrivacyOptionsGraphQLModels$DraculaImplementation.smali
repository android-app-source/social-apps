.class public final Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 868069
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 868070
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 868067
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 868068
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 868056
    if-nez p1, :cond_0

    move v0, v1

    .line 868057
    :goto_0
    return v0

    .line 868058
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 868059
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 868060
    :pswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v2

    .line 868061
    const-class v0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;

    .line 868062
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 868063
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 868064
    invoke-virtual {p3, v1, v2}, LX/186;->a(IZ)V

    .line 868065
    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 868066
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2b6ce9bd
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 868047
    if-nez p0, :cond_0

    move v0, v1

    .line 868048
    :goto_0
    return v0

    .line 868049
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 868050
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 868051
    :goto_1
    if-ge v1, v2, :cond_2

    .line 868052
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 868053
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 868054
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 868055
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 868040
    const/4 v7, 0x0

    .line 868041
    const/4 v1, 0x0

    .line 868042
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 868043
    invoke-static {v2, v3, v0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 868044
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 868045
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 868046
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 868039
    new-instance v0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 868034
    if-eqz p0, :cond_0

    .line 868035
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 868036
    if-eq v0, p0, :cond_0

    .line 868037
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 868038
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 868029
    packed-switch p2, :pswitch_data_0

    .line 868030
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 868031
    :pswitch_0
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;

    .line 868032
    invoke-static {v0, p3}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 868033
    return-void

    :pswitch_data_0
    .packed-switch 0x2b6ce9bd
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 868028
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 867995
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 867996
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 868023
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 868024
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 868025
    :cond_0
    iput-object p1, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 868026
    iput p2, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;->b:I

    .line 868027
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 868022
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 868021
    new-instance v0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 868018
    iget v0, p0, LX/1vt;->c:I

    .line 868019
    move v0, v0

    .line 868020
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 868015
    iget v0, p0, LX/1vt;->c:I

    .line 868016
    move v0, v0

    .line 868017
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 868012
    iget v0, p0, LX/1vt;->b:I

    .line 868013
    move v0, v0

    .line 868014
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868009
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 868010
    move-object v0, v0

    .line 868011
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 868000
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 868001
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 868002
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 868003
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 868004
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 868005
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 868006
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 868007
    invoke-static {v3, v9, v2}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 868008
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 867997
    iget v0, p0, LX/1vt;->c:I

    .line 867998
    move v0, v0

    .line 867999
    return v0
.end method
