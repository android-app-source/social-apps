.class public final Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/1oV;
.implements LX/1oR;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xebfa1b8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyAudienceMemberModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyAudienceMemberModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 868163
    const-class v0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 868162
    const-class v0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 868160
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 868161
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868158
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    iput-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    .line 868159
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    return-object v0
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyAudienceMemberModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 868156
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyAudienceMemberModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->f:Ljava/util/List;

    .line 868157
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868104
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->g:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->g:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    .line 868105
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->g:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyAudienceMemberModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 868154
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyAudienceMemberModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->h:Ljava/util/List;

    .line 868155
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868152
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    .line 868153
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private n()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 868150
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->k:Ljava/util/List;

    .line 868151
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 868132
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 868133
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->a()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 868134
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 868135
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->k()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 868136
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->l()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 868137
    invoke-virtual {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 868138
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 868139
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->n()LX/0Px;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->c(Ljava/util/List;)I

    move-result v6

    .line 868140
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 868141
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 868142
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 868143
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 868144
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 868145
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 868146
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 868147
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 868148
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 868149
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 868114
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 868115
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 868116
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 868117
    if-eqz v1, :cond_3

    .line 868118
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;

    .line 868119
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 868120
    :goto_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->k()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 868121
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->k()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    .line 868122
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->k()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 868123
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;

    .line 868124
    iput-object v0, v1, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->g:Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    .line 868125
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 868126
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 868127
    if-eqz v2, :cond_1

    .line 868128
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;

    .line 868129
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->h:Ljava/util/List;

    move-object v1, v0

    .line 868130
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 868131
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final synthetic b()LX/1Fd;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868113
    invoke-direct {p0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->k()Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyIconFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 868110
    new-instance v0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;-><init>()V

    .line 868111
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 868112
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868108
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    .line 868109
    iget-object v0, p0, Lcom/facebook/api/graphql/privacyoptions/NewsFeedPrivacyOptionsGraphQLModels$PrivacyOptionsFieldsModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 868107
    const v0, -0x79844d7e    # -4.7339E-35f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 868106
    const v0, -0x7646fe03    # -4.4539E-33f

    return v0
.end method
