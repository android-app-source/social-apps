.class public final Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x38cc9b2b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1059584
    const-class v0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1059554
    const-class v0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1059582
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1059583
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1059579
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1059580
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1059581
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1059577
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->f:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->f:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    .line 1059578
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->f:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1059575
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->g:Ljava/lang/String;

    .line 1059576
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1059565
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1059566
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1059567
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->k()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1059568
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1059569
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1059570
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1059571
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1059572
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1059573
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1059574
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1059585
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1059586
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->k()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1059587
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->k()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    .line 1059588
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->k()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1059589
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;

    .line 1059590
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->f:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    .line 1059591
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1059592
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1059564
    new-instance v0, LX/6AY;

    invoke-direct {v0, p1}, LX/6AY;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1059563
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1059561
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1059562
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1059560
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1059557
    new-instance v0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryModel;-><init>()V

    .line 1059558
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1059559
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1059556
    const v0, 0x590b2815

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1059555
    const v0, 0x252222

    return v0
.end method
