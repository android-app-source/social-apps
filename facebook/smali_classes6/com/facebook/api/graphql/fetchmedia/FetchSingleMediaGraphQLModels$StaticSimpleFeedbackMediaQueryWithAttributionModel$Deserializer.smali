.class public final Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1059657
    const-class v0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel;

    new-instance v1, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1059658
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1059593
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1059594
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1059595
    const/4 v2, 0x0

    .line 1059596
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_9

    .line 1059597
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1059598
    :goto_0
    move v1, v2

    .line 1059599
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1059600
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1059601
    new-instance v1, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel;

    invoke-direct {v1}, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel;-><init>()V

    .line 1059602
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1059603
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1059604
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1059605
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1059606
    :cond_0
    return-object v1

    .line 1059607
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1059608
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 1059609
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1059610
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1059611
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_2

    if-eqz v7, :cond_2

    .line 1059612
    const-string v8, "__type__"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "__typename"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1059613
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    goto :goto_1

    .line 1059614
    :cond_4
    const-string v8, "feedback"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1059615
    invoke-static {p1, v0}, LX/5Af;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1059616
    :cond_5
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1059617
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1059618
    :cond_6
    const-string v8, "owner"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1059619
    invoke-static {p1, v0}, LX/6Ac;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1059620
    :cond_7
    const-string v8, "privacy_scope"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1059621
    const/4 v7, 0x0

    .line 1059622
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v8, :cond_d

    .line 1059623
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1059624
    :goto_2
    move v1, v7

    .line 1059625
    goto :goto_1

    .line 1059626
    :cond_8
    const/4 v7, 0x5

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1059627
    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1059628
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1059629
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1059630
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1059631
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1059632
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto/16 :goto_1

    .line 1059633
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1059634
    :cond_b
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_c

    .line 1059635
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1059636
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1059637
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, p0, :cond_b

    if-eqz v8, :cond_b

    .line 1059638
    const-string v9, "icon_image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1059639
    const/4 v8, 0x0

    .line 1059640
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v9, :cond_11

    .line 1059641
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1059642
    :goto_4
    move v1, v8

    .line 1059643
    goto :goto_3

    .line 1059644
    :cond_c
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1059645
    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1059646
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto :goto_2

    :cond_d
    move v1, v7

    goto :goto_3

    .line 1059647
    :cond_e
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1059648
    :cond_f
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_10

    .line 1059649
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1059650
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1059651
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_f

    if-eqz v9, :cond_f

    .line 1059652
    const-string p0, "uri"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1059653
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_5

    .line 1059654
    :cond_10
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1059655
    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1059656
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto :goto_4

    :cond_11
    move v1, v8

    goto :goto_5
.end method
