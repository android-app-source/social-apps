.class public final Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1059749
    const-class v0, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel;

    new-instance v1, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1059750
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1059748
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1059713
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1059714
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1059715
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1059716
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1059717
    if-eqz v2, :cond_0

    .line 1059718
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1059719
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1059720
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1059721
    if-eqz v2, :cond_1

    .line 1059722
    const-string p0, "feedback"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1059723
    invoke-static {v1, v2, p1, p2}, LX/5Af;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1059724
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1059725
    if-eqz v2, :cond_2

    .line 1059726
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1059727
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1059728
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1059729
    if-eqz v2, :cond_3

    .line 1059730
    const-string p0, "owner"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1059731
    invoke-static {v1, v2, p1}, LX/6Ac;->a(LX/15i;ILX/0nX;)V

    .line 1059732
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1059733
    if-eqz v2, :cond_6

    .line 1059734
    const-string p0, "privacy_scope"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1059735
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1059736
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1059737
    if-eqz p0, :cond_5

    .line 1059738
    const-string v0, "icon_image"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1059739
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1059740
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1059741
    if-eqz v0, :cond_4

    .line 1059742
    const-string v2, "uri"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1059743
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1059744
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1059745
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1059746
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1059747
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1059712
    check-cast p1, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel$Serializer;->a(Lcom/facebook/api/graphql/fetchmedia/FetchSingleMediaGraphQLModels$StaticSimpleFeedbackMediaQueryWithAttributionModel;LX/0nX;LX/0my;)V

    return-void
.end method
