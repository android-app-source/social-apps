.class public final Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 849376
    const-class v0, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel;

    new-instance v1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 849377
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 849378
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 849379
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 849380
    const/4 v2, 0x0

    .line 849381
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 849382
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 849383
    :goto_0
    move v1, v2

    .line 849384
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 849385
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 849386
    new-instance v1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel;

    invoke-direct {v1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel;-><init>()V

    .line 849387
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 849388
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 849389
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 849390
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 849391
    :cond_0
    return-object v1

    .line 849392
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 849393
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 849394
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 849395
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 849396
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 849397
    const-string v4, "civic_engagement_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 849398
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 849399
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_9

    .line 849400
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 849401
    :goto_2
    move v1, v3

    .line 849402
    goto :goto_1

    .line 849403
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 849404
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 849405
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 849406
    :cond_5
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_7

    .line 849407
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 849408
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 849409
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_5

    if-eqz v6, :cond_5

    .line 849410
    const-string p0, "is_civic_enabled"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 849411
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v5, v1

    move v1, v4

    goto :goto_3

    .line 849412
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 849413
    :cond_7
    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 849414
    if-eqz v1, :cond_8

    .line 849415
    invoke-virtual {v0, v3, v5}, LX/186;->a(IZ)V

    .line 849416
    :cond_8
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    move v5, v3

    goto :goto_3
.end method
