.class public final Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 849841
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 849842
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 849864
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 849865
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 849848
    if-nez p1, :cond_0

    .line 849849
    :goto_0
    return v0

    .line 849850
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 849851
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 849852
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 849853
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 849854
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 849855
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 849856
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 849857
    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(III)I

    move-result v2

    .line 849858
    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(III)I

    move-result v3

    .line 849859
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 849860
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 849861
    invoke-virtual {p3, v5, v2, v0}, LX/186;->a(III)V

    .line 849862
    invoke-virtual {p3, v6, v3, v0}, LX/186;->a(III)V

    .line 849863
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2b9d528a -> :sswitch_0
        0x7dc36410 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 849847
    new-instance v0, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 849844
    sparse-switch p0, :sswitch_data_0

    .line 849845
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 849846
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x2b9d528a -> :sswitch_0
        0x7dc36410 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 849843
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 849809
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;->b(I)V

    .line 849810
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 849836
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 849837
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 849838
    :cond_0
    iput-object p1, p0, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;->a:LX/15i;

    .line 849839
    iput p2, p0, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;->b:I

    .line 849840
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 849866
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 849835
    new-instance v0, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 849832
    iget v0, p0, LX/1vt;->c:I

    .line 849833
    move v0, v0

    .line 849834
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 849829
    iget v0, p0, LX/1vt;->c:I

    .line 849830
    move v0, v0

    .line 849831
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 849826
    iget v0, p0, LX/1vt;->b:I

    .line 849827
    move v0, v0

    .line 849828
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 849823
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 849824
    move-object v0, v0

    .line 849825
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 849814
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 849815
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 849816
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 849817
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 849818
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 849819
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 849820
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 849821
    invoke-static {v3, v9, v2}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 849822
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 849811
    iget v0, p0, LX/1vt;->c:I

    .line 849812
    move v0, v0

    .line 849813
    return v0
.end method
