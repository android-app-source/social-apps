.class public final Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 849417
    const-class v0, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel;

    new-instance v1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 849418
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 849419
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 849420
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 849421
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 849422
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 849423
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 849424
    if-eqz v2, :cond_1

    .line 849425
    const-string p0, "civic_engagement_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 849426
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 849427
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 849428
    if-eqz p0, :cond_0

    .line 849429
    const-string v0, "is_civic_enabled"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 849430
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 849431
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 849432
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 849433
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 849434
    check-cast p1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel$Serializer;->a(Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentEditDistrictMutationFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
