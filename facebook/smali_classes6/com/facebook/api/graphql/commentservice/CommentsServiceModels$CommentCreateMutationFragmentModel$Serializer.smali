.class public final Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 849313
    const-class v0, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    new-instance v1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 849314
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 849315
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 849316
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 849317
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 849318
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 849319
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 849320
    if-eqz v2, :cond_0

    .line 849321
    const-string p0, "comment"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 849322
    invoke-static {v1, v2, p1, p2}, LX/2sx;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 849323
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 849324
    if-eqz v2, :cond_1

    .line 849325
    const-string p0, "feedback"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 849326
    invoke-static {v1, v2, p1, p2}, LX/594;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 849327
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 849328
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 849329
    check-cast p1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel$Serializer;->a(Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
