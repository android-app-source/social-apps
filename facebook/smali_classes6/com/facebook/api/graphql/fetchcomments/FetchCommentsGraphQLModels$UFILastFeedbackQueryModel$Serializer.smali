.class public final Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 861415
    const-class v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    new-instance v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 861416
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 861414
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 861417
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 861418
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 861419
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 861420
    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result v2

    .line 861421
    if-eqz v2, :cond_0

    .line 861422
    const-string v3, "can_page_viewer_invite_post_likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861423
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861424
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861425
    if-eqz v2, :cond_1

    .line 861426
    const-string v3, "can_see_voice_switcher"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861427
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861428
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861429
    if-eqz v2, :cond_2

    .line 861430
    const-string v3, "can_viewer_comment"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861431
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861432
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861433
    if-eqz v2, :cond_3

    .line 861434
    const-string v3, "can_viewer_comment_with_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861435
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861436
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861437
    if-eqz v2, :cond_4

    .line 861438
    const-string v3, "can_viewer_comment_with_sticker"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861439
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861440
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861441
    if-eqz v2, :cond_5

    .line 861442
    const-string v3, "can_viewer_comment_with_video"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861443
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861444
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861445
    if-eqz v2, :cond_6

    .line 861446
    const-string v3, "can_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861447
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861448
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861449
    if-eqz v2, :cond_7

    .line 861450
    const-string v3, "can_viewer_react"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861451
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861452
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861453
    if-eqz v2, :cond_8

    .line 861454
    const-string v3, "can_viewer_subscribe"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861455
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861456
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 861457
    if-eqz v2, :cond_9

    .line 861458
    const-string v3, "comments_mirroring_domain"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861459
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 861460
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 861461
    if-eqz v2, :cond_a

    .line 861462
    const-string v3, "default_comment_ordering"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861463
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 861464
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861465
    if-eqz v2, :cond_b

    .line 861466
    const-string v3, "does_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861467
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861468
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 861469
    if-eqz v2, :cond_c

    .line 861470
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861471
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 861472
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861473
    if-eqz v2, :cond_d

    .line 861474
    const-string v3, "important_reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861475
    invoke-static {v1, v2, p1, p2}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861476
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861477
    if-eqz v2, :cond_e

    .line 861478
    const-string v3, "is_viewer_subscribed"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861479
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861480
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 861481
    if-eqz v2, :cond_f

    .line 861482
    const-string v3, "legacy_api_post_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861483
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 861484
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861485
    if-eqz v2, :cond_10

    .line 861486
    const-string v3, "like_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861487
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861488
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861489
    if-eqz v2, :cond_11

    .line 861490
    const-string v3, "likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861491
    invoke-static {v1, v2, p1}, LX/5B6;->a(LX/15i;ILX/0nX;)V

    .line 861492
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861493
    if-eqz v2, :cond_12

    .line 861494
    const-string v3, "reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861495
    invoke-static {v1, v2, p1}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 861496
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 861497
    if-eqz v2, :cond_13

    .line 861498
    const-string v3, "remixable_photo_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861499
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 861500
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861501
    if-eqz v2, :cond_14

    .line 861502
    const-string v3, "seen_by"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861503
    invoke-static {v1, v2, p1}, LX/5B7;->a(LX/15i;ILX/0nX;)V

    .line 861504
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861505
    if-eqz v2, :cond_15

    .line 861506
    const-string v3, "supported_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861507
    invoke-static {v1, v2, p1, p2}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861508
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861509
    if-eqz v2, :cond_16

    .line 861510
    const-string v3, "top_level_comments"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861511
    invoke-static {v1, v2, p1, p2}, LX/59L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861512
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861513
    if-eqz v2, :cond_17

    .line 861514
    const-string v3, "top_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861515
    invoke-static {v1, v2, p1, p2}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861516
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861517
    if-eqz v2, :cond_18

    .line 861518
    const-string v3, "viewer_acts_as_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861519
    invoke-static {v1, v2, p1, p2}, LX/5B8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861520
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861521
    if-eqz v2, :cond_19

    .line 861522
    const-string v3, "viewer_acts_as_person"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861523
    invoke-static {v1, v2, p1}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 861524
    :cond_19
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861525
    if-eqz v2, :cond_1a

    .line 861526
    const-string v3, "viewer_does_not_like_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861527
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861528
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 861529
    if-eqz v2, :cond_1b

    .line 861530
    const-string v3, "viewer_feedback_reaction_key"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861531
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 861532
    :cond_1b
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861533
    if-eqz v2, :cond_1c

    .line 861534
    const-string v3, "viewer_likes_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861535
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861536
    :cond_1c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 861537
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 861413
    check-cast p1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel$Serializer;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
