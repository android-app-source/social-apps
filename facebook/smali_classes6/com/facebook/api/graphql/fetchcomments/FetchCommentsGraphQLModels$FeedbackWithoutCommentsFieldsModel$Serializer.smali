.class public final Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 860760
    const-class v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 860761
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 860762
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 860763
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 860764
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 860765
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 860766
    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result v2

    .line 860767
    if-eqz v2, :cond_0

    .line 860768
    const-string v3, "can_page_viewer_invite_post_likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860769
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 860770
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 860771
    if-eqz v2, :cond_1

    .line 860772
    const-string v3, "can_see_voice_switcher"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860773
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 860774
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 860775
    if-eqz v2, :cond_2

    .line 860776
    const-string v3, "can_viewer_comment"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860777
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 860778
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 860779
    if-eqz v2, :cond_3

    .line 860780
    const-string v3, "can_viewer_comment_with_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860781
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 860782
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 860783
    if-eqz v2, :cond_4

    .line 860784
    const-string v3, "can_viewer_comment_with_sticker"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860785
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 860786
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 860787
    if-eqz v2, :cond_5

    .line 860788
    const-string v3, "can_viewer_comment_with_video"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860789
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 860790
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 860791
    if-eqz v2, :cond_6

    .line 860792
    const-string v3, "can_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860793
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 860794
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 860795
    if-eqz v2, :cond_7

    .line 860796
    const-string v3, "can_viewer_react"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860797
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 860798
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 860799
    if-eqz v2, :cond_8

    .line 860800
    const-string v3, "can_viewer_subscribe"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860801
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 860802
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 860803
    if-eqz v2, :cond_9

    .line 860804
    const-string v3, "comments_mirroring_domain"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860805
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 860806
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 860807
    if-eqz v2, :cond_a

    .line 860808
    const-string v3, "default_comment_ordering"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860809
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 860810
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 860811
    if-eqz v2, :cond_b

    .line 860812
    const-string v3, "does_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860813
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 860814
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 860815
    if-eqz v2, :cond_c

    .line 860816
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860817
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 860818
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 860819
    if-eqz v2, :cond_d

    .line 860820
    const-string v3, "important_reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860821
    invoke-static {v1, v2, p1, p2}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 860822
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 860823
    if-eqz v2, :cond_e

    .line 860824
    const-string v3, "is_viewer_subscribed"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860825
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 860826
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 860827
    if-eqz v2, :cond_f

    .line 860828
    const-string v3, "legacy_api_post_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860829
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 860830
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 860831
    if-eqz v2, :cond_10

    .line 860832
    const-string v3, "like_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860833
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 860834
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 860835
    if-eqz v2, :cond_11

    .line 860836
    const-string v3, "likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860837
    invoke-static {v1, v2, p1}, LX/5B6;->a(LX/15i;ILX/0nX;)V

    .line 860838
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 860839
    if-eqz v2, :cond_12

    .line 860840
    const-string v3, "reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860841
    invoke-static {v1, v2, p1}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 860842
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 860843
    if-eqz v2, :cond_13

    .line 860844
    const-string v3, "remixable_photo_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860845
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 860846
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 860847
    if-eqz v2, :cond_14

    .line 860848
    const-string v3, "seen_by"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860849
    invoke-static {v1, v2, p1}, LX/5B7;->a(LX/15i;ILX/0nX;)V

    .line 860850
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 860851
    if-eqz v2, :cond_15

    .line 860852
    const-string v3, "supported_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860853
    invoke-static {v1, v2, p1, p2}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 860854
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 860855
    if-eqz v2, :cond_16

    .line 860856
    const-string v3, "top_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860857
    invoke-static {v1, v2, p1, p2}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 860858
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 860859
    if-eqz v2, :cond_17

    .line 860860
    const-string v3, "viewer_acts_as_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860861
    invoke-static {v1, v2, p1, p2}, LX/5B8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 860862
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 860863
    if-eqz v2, :cond_18

    .line 860864
    const-string v3, "viewer_acts_as_person"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860865
    invoke-static {v1, v2, p1}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 860866
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 860867
    if-eqz v2, :cond_19

    .line 860868
    const-string v3, "viewer_does_not_like_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860869
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 860870
    :cond_19
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 860871
    if-eqz v2, :cond_1a

    .line 860872
    const-string v3, "viewer_feedback_reaction_key"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860873
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 860874
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 860875
    if-eqz v2, :cond_1b

    .line 860876
    const-string v3, "viewer_likes_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 860877
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 860878
    :cond_1b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 860879
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 860880
    check-cast p1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
