.class public final Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 860705
    const-class v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 860683
    const-class v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 860708
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 860709
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 860706
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 860707
    iget v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 860697
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 860698
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 860699
    iget v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 860700
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 860701
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 860702
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 860703
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 860704
    return-object p0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 860691
    iput p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;->e:I

    .line 860692
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 860693
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 860694
    if-eqz v0, :cond_0

    .line 860695
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 860696
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 860688
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 860689
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;->e:I

    .line 860690
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 860685
    new-instance v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;-><init>()V

    .line 860686
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 860687
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 860684
    const v0, -0x30e48d96

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 860682
    const v0, 0x2bb653c8

    return v0
.end method
