.class public final Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 862945
    const-class v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;

    new-instance v1, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 862946
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 862947
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 862948
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 862949
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 862950
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 862951
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 862952
    if-eqz v2, :cond_0

    .line 862953
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 862954
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 862955
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 862956
    if-eqz v2, :cond_1

    .line 862957
    const-string v3, "attachments"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 862958
    invoke-static {v1, v2, p1, p2}, LX/59I;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 862959
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 862960
    if-eqz v2, :cond_2

    .line 862961
    const-string v3, "author"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 862962
    invoke-static {v1, v2, p1, p2}, LX/59B;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 862963
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 862964
    if-eqz v2, :cond_3

    .line 862965
    const-string v3, "body"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 862966
    invoke-static {v1, v2, p1, p2}, LX/59F;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 862967
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 862968
    if-eqz v2, :cond_4

    .line 862969
    const-string v3, "body_markdown_html"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 862970
    invoke-static {v1, v2, p1}, LX/59C;->a(LX/15i;ILX/0nX;)V

    .line 862971
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 862972
    if-eqz v2, :cond_5

    .line 862973
    const-string v3, "can_edit_constituent_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 862974
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 862975
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 862976
    if-eqz v2, :cond_6

    .line 862977
    const-string v3, "can_viewer_delete"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 862978
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 862979
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 862980
    if-eqz v2, :cond_7

    .line 862981
    const-string v3, "can_viewer_edit"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 862982
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 862983
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 862984
    if-eqz v2, :cond_8

    .line 862985
    const-string v3, "can_viewer_share"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 862986
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 862987
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 862988
    if-eqz v2, :cond_9

    .line 862989
    const-string v3, "constituent_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 862990
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 862991
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 862992
    cmp-long v4, v2, v4

    if-eqz v4, :cond_a

    .line 862993
    const-string v4, "created_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 862994
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 862995
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 862996
    if-eqz v2, :cond_b

    .line 862997
    const-string v3, "edit_history"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 862998
    invoke-static {v1, v2, p1}, LX/59G;->a(LX/15i;ILX/0nX;)V

    .line 862999
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 863000
    if-eqz v2, :cond_c

    .line 863001
    const-string v3, "feedback"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863002
    invoke-static {v1, v2, p1, p2}, LX/5BM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863003
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 863004
    if-eqz v2, :cond_d

    .line 863005
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863006
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 863007
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 863008
    if-eqz v2, :cond_e

    .line 863009
    const-string v3, "is_featured"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863010
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863011
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 863012
    if-eqz v2, :cond_f

    .line 863013
    const-string v3, "is_marked_as_spam"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863014
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863015
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 863016
    if-eqz v2, :cond_10

    .line 863017
    const-string v3, "is_pinned"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863018
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 863019
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 863020
    if-eqz v2, :cond_11

    .line 863021
    const-string v3, "permalink_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863022
    invoke-static {v1, v2, p1, p2}, LX/5BP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 863023
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 863024
    if-eqz v2, :cond_12

    .line 863025
    const-string v3, "spam_display_mode"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863026
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 863027
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 863028
    if-eqz v2, :cond_13

    .line 863029
    const-string v3, "translatability_for_viewer"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 863030
    invoke-static {v1, v2, p1}, LX/59K;->a(LX/15i;ILX/0nX;)V

    .line 863031
    :cond_13
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 863032
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 863033
    check-cast p1, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$Serializer;->a(Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
