.class public final Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 861273
    const-class v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    new-instance v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 861274
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 861275
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 861276
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 861277
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 861278
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 861279
    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result v2

    .line 861280
    if-eqz v2, :cond_0

    .line 861281
    const-string v3, "can_page_viewer_invite_post_likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861282
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861283
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861284
    if-eqz v2, :cond_1

    .line 861285
    const-string v3, "can_see_voice_switcher"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861286
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861287
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861288
    if-eqz v2, :cond_2

    .line 861289
    const-string v3, "can_viewer_comment"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861290
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861291
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861292
    if-eqz v2, :cond_3

    .line 861293
    const-string v3, "can_viewer_comment_with_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861294
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861295
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861296
    if-eqz v2, :cond_4

    .line 861297
    const-string v3, "can_viewer_comment_with_sticker"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861298
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861299
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861300
    if-eqz v2, :cond_5

    .line 861301
    const-string v3, "can_viewer_comment_with_video"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861302
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861303
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861304
    if-eqz v2, :cond_6

    .line 861305
    const-string v3, "can_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861306
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861307
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861308
    if-eqz v2, :cond_7

    .line 861309
    const-string v3, "can_viewer_react"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861310
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861311
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861312
    if-eqz v2, :cond_8

    .line 861313
    const-string v3, "can_viewer_subscribe"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861314
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861315
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 861316
    if-eqz v2, :cond_9

    .line 861317
    const-string v3, "comments_mirroring_domain"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861318
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 861319
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 861320
    if-eqz v2, :cond_a

    .line 861321
    const-string v3, "default_comment_ordering"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861322
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 861323
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861324
    if-eqz v2, :cond_b

    .line 861325
    const-string v3, "does_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861326
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861327
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 861328
    if-eqz v2, :cond_c

    .line 861329
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861330
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 861331
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861332
    if-eqz v2, :cond_d

    .line 861333
    const-string v3, "important_reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861334
    invoke-static {v1, v2, p1, p2}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861335
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 861336
    if-eqz v2, :cond_e

    .line 861337
    const-string v3, "is_viewer_subscribed"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861338
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 861339
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 861340
    if-eqz v2, :cond_f

    .line 861341
    const-string v3, "legacy_api_post_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861342
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 861343
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861344
    if-eqz v2, :cond_10

    .line 861345
    const-string v3, "like_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861346
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861347
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861348
    if-eqz v2, :cond_11

    .line 861349
    const-string v3, "likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861350
    invoke-static {v1, v2, p1}, LX/5B6;->a(LX/15i;ILX/0nX;)V

    .line 861351
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861352
    if-eqz v2, :cond_12

    .line 861353
    const-string v3, "reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861354
    invoke-static {v1, v2, p1}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 861355
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 861356
    if-eqz v2, :cond_13

    .line 861357
    const-string v3, "remixable_photo_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861358
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 861359
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861360
    if-eqz v2, :cond_14

    .line 861361
    const-string v3, "seen_by"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861362
    invoke-static {v1, v2, p1}, LX/5B7;->a(LX/15i;ILX/0nX;)V

    .line 861363
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861364
    if-eqz v2, :cond_15

    .line 861365
    const-string v3, "supported_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861366
    invoke-static {v1, v2, p1, p2}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861367
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861368
    if-eqz v2, :cond_16

    .line 861369
    const-string v3, "top_level_comments"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861370
    invoke-static {v1, v2, p1, p2}, LX/59L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861371
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861372
    if-eqz v2, :cond_17

    .line 861373
    const-string v3, "top_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861374
    invoke-static {v1, v2, p1, p2}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861375
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861376
    if-eqz v2, :cond_18

    .line 861377
    const-string v3, "viewer_acts_as_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861378
    invoke-static {v1, v2, p1, p2}, LX/5B8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861379
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861380
    if-eqz v2, :cond_19

    .line 861381
    const-string v3, "viewer_acts_as_person"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861382
    invoke-static {v1, v2, p1}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 861383
    :cond_19
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861384
    if-eqz v2, :cond_1a

    .line 861385
    const-string v3, "viewer_does_not_like_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861386
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861387
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 861388
    if-eqz v2, :cond_1b

    .line 861389
    const-string v3, "viewer_feedback_reaction_key"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861390
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 861391
    :cond_1b
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 861392
    if-eqz v2, :cond_1c

    .line 861393
    const-string v3, "viewer_likes_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 861394
    invoke-static {v1, v2, p1, p2}, LX/412;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 861395
    :cond_1c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 861396
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 861397
    check-cast p1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel$Serializer;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
