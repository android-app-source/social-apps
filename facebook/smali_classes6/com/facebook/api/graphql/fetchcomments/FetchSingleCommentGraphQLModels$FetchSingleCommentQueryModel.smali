.class public final Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7a4f1ed0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:J

.field private p:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$FeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$PermalinkTitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 863151
    const-class v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 863148
    const-class v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 863149
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 863150
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 863152
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 863153
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 863154
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttachments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 863144
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->f:Ljava/util/List;

    .line 863145
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 863155
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    .line 863156
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    return-object v0
.end method

.method private m()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 863157
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->h:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->h:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    .line 863158
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->h:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    return-object v0
.end method

.method private n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBodyMarkdownHtml"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 863159
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 863160
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 863161
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->n:Ljava/lang/String;

    .line 863162
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method private p()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEditHistory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 863163
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 863164
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->p:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private q()Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$FeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 863165
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->q:Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$FeedbackModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$FeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$FeedbackModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->q:Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$FeedbackModel;

    .line 863166
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->q:Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$FeedbackModel;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 863167
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->r:Ljava/lang/String;

    .line 863168
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->r:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$PermalinkTitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 863146
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$PermalinkTitleModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$PermalinkTitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$PermalinkTitleModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$PermalinkTitleModel;

    .line 863147
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$PermalinkTitleModel;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 863034
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->w:Ljava/lang/String;

    .line 863035
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->w:Ljava/lang/String;

    return-object v0
.end method

.method private u()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTranslatabilityForViewer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 863036
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 863037
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->x:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 863038
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 863039
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 863040
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 863041
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 863042
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->m()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 863043
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->n()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, -0x24e06805

    invoke-static {v5, v4, v6}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 863044
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 863045
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->p()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, 0x22ac0806

    invoke-static {v7, v6, v8}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 863046
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->q()Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$FeedbackModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 863047
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->r()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 863048
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->s()Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$PermalinkTitleModel;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 863049
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->t()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 863050
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->u()LX/1vs;

    move-result-object v11

    iget-object v12, v11, LX/1vs;->a:LX/15i;

    iget v11, v11, LX/1vs;->b:I

    const v13, 0x4f659d37

    invoke-static {v12, v11, v13}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    move-result-object v11

    invoke-static {p1, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 863051
    const/16 v12, 0x14

    invoke-virtual {p1, v12}, LX/186;->c(I)V

    .line 863052
    const/4 v12, 0x0

    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 863053
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 863054
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 863055
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 863056
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 863057
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 863058
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 863059
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 863060
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 863061
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 863062
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->o:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 863063
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 863064
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 863065
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 863066
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->s:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 863067
    const/16 v0, 0xf

    iget-boolean v1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->t:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 863068
    const/16 v0, 0x10

    iget-boolean v1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->u:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 863069
    const/16 v0, 0x11

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 863070
    const/16 v0, 0x12

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 863071
    const/16 v0, 0x13

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 863072
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 863073
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 863074
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 863075
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 863076
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 863077
    if-eqz v1, :cond_8

    .line 863078
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;

    .line 863079
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 863080
    :goto_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 863081
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    .line 863082
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 863083
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;

    .line 863084
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    .line 863085
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->m()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 863086
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->m()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    .line 863087
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->m()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 863088
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;

    .line 863089
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->h:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    .line 863090
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 863091
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x24e06805

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 863092
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 863093
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;

    .line 863094
    iput v3, v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->i:I

    move-object v1, v0

    .line 863095
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->p()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 863096
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->p()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x22ac0806

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 863097
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->p()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 863098
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;

    .line 863099
    iput v3, v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->p:I

    move-object v1, v0

    .line 863100
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->q()Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 863101
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->q()Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$FeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$FeedbackModel;

    .line 863102
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->q()Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$FeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 863103
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;

    .line 863104
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->q:Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$FeedbackModel;

    .line 863105
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->s()Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$PermalinkTitleModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 863106
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->s()Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$PermalinkTitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$PermalinkTitleModel;

    .line 863107
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->s()Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$PermalinkTitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 863108
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;

    .line 863109
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel$PermalinkTitleModel;

    .line 863110
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->u()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 863111
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->u()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4f659d37

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 863112
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->u()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 863113
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;

    .line 863114
    iput v3, v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->x:I

    move-object v1, v0

    .line 863115
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 863116
    if-nez v1, :cond_7

    :goto_1
    return-object p0

    .line 863117
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 863118
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 863119
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_7
    move-object p0, v1

    .line 863120
    goto :goto_1

    :cond_8
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 863121
    new-instance v0, LX/5BH;

    invoke-direct {v0, p1}, LX/5BH;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 863122
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 863123
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 863124
    const/4 v0, 0x4

    const v1, -0x24e06805

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->i:I

    .line 863125
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->j:Z

    .line 863126
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->k:Z

    .line 863127
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->l:Z

    .line 863128
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->m:Z

    .line 863129
    const/16 v0, 0xa

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->o:J

    .line 863130
    const/16 v0, 0xb

    const v1, 0x22ac0806

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->p:I

    .line 863131
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->s:Z

    .line 863132
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->t:Z

    .line 863133
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->u:Z

    .line 863134
    const/16 v0, 0x13

    const v1, 0x4f659d37

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;->x:I

    .line 863135
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 863136
    invoke-virtual {p2}, LX/18L;->a()V

    .line 863137
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 863138
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 863141
    new-instance v0, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/fetchcomments/FetchSingleCommentGraphQLModels$FetchSingleCommentQueryModel;-><init>()V

    .line 863142
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 863143
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 863139
    const v0, 0x1d159c02

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 863140
    const v0, 0x252222

    return v0
.end method
