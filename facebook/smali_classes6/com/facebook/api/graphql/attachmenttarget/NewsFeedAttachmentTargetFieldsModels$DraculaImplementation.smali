.class public final Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 843092
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 843093
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 843237
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 843238
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 843165
    if-nez p1, :cond_0

    .line 843166
    :goto_0
    return v1

    .line 843167
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 843168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 843169
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 843170
    const v2, 0x74daa2b0

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 843171
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 843172
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 843173
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 843174
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 843175
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 843176
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 843177
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 843178
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 843179
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 843180
    const v2, 0x52e8515b

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 843181
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 843182
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 843183
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 843184
    :sswitch_3
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 843185
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 843186
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 843187
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 843188
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 843189
    :sswitch_4
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 843190
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 843191
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 843192
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 843193
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLRedirectionReason;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

    move-result-object v0

    .line 843194
    invoke-virtual {p3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 843195
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 843196
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 843197
    invoke-virtual {p3, v10}, LX/186;->c(I)V

    .line 843198
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 843199
    invoke-virtual {p3, v9, v2}, LX/186;->b(II)V

    .line 843200
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 843201
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 843202
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 843203
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 843204
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 843205
    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 843206
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 843207
    const-class v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-virtual {p0, p1, v11, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 843208
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 843209
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 843210
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 843211
    invoke-virtual {p3, v9, v3}, LX/186;->b(II)V

    .line 843212
    invoke-virtual {p3, v10, v4}, LX/186;->b(II)V

    .line 843213
    invoke-virtual {p3, v11, v0}, LX/186;->b(II)V

    .line 843214
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 843215
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 843216
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 843217
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 843218
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 843219
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 843220
    :sswitch_8
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 843221
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 843222
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 843223
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 843224
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 843225
    :sswitch_9
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 843226
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 843227
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 843228
    invoke-virtual {p0, p1, v10, v1}, LX/15i;->a(III)I

    move-result v7

    .line 843229
    invoke-virtual {p0, p1, v11}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 843230
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 843231
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    move-object v0, p3

    .line 843232
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 843233
    invoke-virtual {p3, v9, v6}, LX/186;->b(II)V

    .line 843234
    invoke-virtual {p3, v10, v7, v1}, LX/186;->a(III)V

    .line 843235
    invoke-virtual {p3, v11, v8}, LX/186;->b(II)V

    .line 843236
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3ccc2f7a -> :sswitch_6
        -0x20eebf8d -> :sswitch_8
        -0x1e86a0d0 -> :sswitch_2
        -0xeed38c6 -> :sswitch_5
        -0x3cf0da9 -> :sswitch_0
        0x3d9090a6 -> :sswitch_9
        0x48cea5bf -> :sswitch_4
        0x52e8515b -> :sswitch_3
        0x5caa5ebb -> :sswitch_7
        0x74daa2b0 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 843156
    if-nez p0, :cond_0

    move v0, v1

    .line 843157
    :goto_0
    return v0

    .line 843158
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 843159
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 843160
    :goto_1
    if-ge v1, v2, :cond_2

    .line 843161
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 843162
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 843163
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 843164
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 843149
    const/4 v7, 0x0

    .line 843150
    const/4 v1, 0x0

    .line 843151
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 843152
    invoke-static {v2, v3, v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 843153
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 843154
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 843155
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 843148
    new-instance v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 843143
    if-eqz p0, :cond_0

    .line 843144
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 843145
    if-eq v0, p0, :cond_0

    .line 843146
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 843147
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 843132
    sparse-switch p2, :sswitch_data_0

    .line 843133
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 843134
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 843135
    const v1, 0x74daa2b0

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 843136
    :goto_0
    :sswitch_1
    return-void

    .line 843137
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 843138
    const v1, 0x52e8515b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 843139
    :sswitch_3
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 843140
    invoke-static {v0, p3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 843141
    :sswitch_4
    const/4 v0, 0x3

    const-class v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 843142
    invoke-static {v0, p3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3ccc2f7a -> :sswitch_4
        -0x20eebf8d -> :sswitch_1
        -0x1e86a0d0 -> :sswitch_2
        -0xeed38c6 -> :sswitch_1
        -0x3cf0da9 -> :sswitch_0
        0x3d9090a6 -> :sswitch_1
        0x48cea5bf -> :sswitch_1
        0x52e8515b -> :sswitch_3
        0x5caa5ebb -> :sswitch_1
        0x74daa2b0 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 843126
    if-eqz p1, :cond_0

    .line 843127
    invoke-static {p0, p1, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v1

    .line 843128
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    .line 843129
    if-eq v0, v1, :cond_0

    .line 843130
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 843131
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 843125
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 843239
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 843240
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 843120
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 843121
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 843122
    :cond_0
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a:LX/15i;

    .line 843123
    iput p2, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->b:I

    .line 843124
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 843119
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 843118
    new-instance v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 843115
    iget v0, p0, LX/1vt;->c:I

    .line 843116
    move v0, v0

    .line 843117
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 843112
    iget v0, p0, LX/1vt;->c:I

    .line 843113
    move v0, v0

    .line 843114
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 843109
    iget v0, p0, LX/1vt;->b:I

    .line 843110
    move v0, v0

    .line 843111
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843106
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 843107
    move-object v0, v0

    .line 843108
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 843097
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 843098
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 843099
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 843100
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 843101
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 843102
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 843103
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 843104
    invoke-static {v3, v9, v2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 843105
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 843094
    iget v0, p0, LX/1vt;->c:I

    .line 843095
    move v0, v0

    .line 843096
    return v0
.end method
