.class public final Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 844003
    const-class v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel;

    new-instance v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 844004
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 844005
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 844006
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 844007
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x8

    .line 844008
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 844009
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 844010
    if-eqz v2, :cond_0

    .line 844011
    const-string v3, "action_links"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844012
    invoke-static {v1, v2, p1, p2}, LX/57A;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 844013
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 844014
    if-eqz v2, :cond_1

    .line 844015
    const-string v3, "associated_application"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844016
    invoke-static {v1, v2, p1, p2}, LX/40v;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 844017
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 844018
    if-eqz v2, :cond_2

    .line 844019
    const-string v3, "attachment_properties"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844020
    invoke-static {v1, v2, p1, p2}, LX/58U;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 844021
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 844022
    if-eqz v2, :cond_3

    .line 844023
    const-string v3, "deduplication_key"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844024
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 844025
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 844026
    if-eqz v2, :cond_4

    .line 844027
    const-string v3, "description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844028
    invoke-static {v1, v2, p1, p2}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 844029
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 844030
    if-eqz v2, :cond_5

    .line 844031
    const-string v3, "media_reference_token"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844032
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 844033
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 844034
    if-eqz v2, :cond_6

    .line 844035
    const-string v3, "source"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844036
    invoke-static {v1, v2, p1}, LX/58V;->a(LX/15i;ILX/0nX;)V

    .line 844037
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 844038
    if-eqz v2, :cond_7

    .line 844039
    const-string v3, "style_infos"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844040
    invoke-static {v1, v2, p1, p2}, LX/58k;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 844041
    :cond_7
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 844042
    if-eqz v2, :cond_8

    .line 844043
    const-string v2, "style_list"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844044
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 844045
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 844046
    if-eqz v2, :cond_9

    .line 844047
    const-string v3, "subtitle"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844048
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 844049
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 844050
    if-eqz v2, :cond_a

    .line 844051
    const-string v3, "target"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844052
    invoke-static {v1, v2, p1, p2}, LX/58i;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 844053
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 844054
    if-eqz v2, :cond_b

    .line 844055
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844056
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 844057
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 844058
    if-eqz v2, :cond_c

    .line 844059
    const-string v3, "title_with_entities"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844060
    invoke-static {v1, v2, p1, p2}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 844061
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 844062
    if-eqz v2, :cond_d

    .line 844063
    const-string v3, "tracking"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844064
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 844065
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 844066
    if-eqz v2, :cond_e

    .line 844067
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 844068
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 844069
    :cond_e
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 844070
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 844071
    check-cast p1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$Serializer;->a(Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel;LX/0nX;LX/0my;)V

    return-void
.end method
