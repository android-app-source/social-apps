.class public final Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 842937
    const-class v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 842938
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 842936
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 842887
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 842888
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    const/4 v3, 0x0

    .line 842889
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 842890
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 842891
    if-eqz v2, :cond_0

    .line 842892
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842893
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 842894
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 842895
    if-eqz v2, :cond_1

    .line 842896
    const-string v2, "android_urls"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842897
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 842898
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 842899
    if-eqz v2, :cond_2

    .line 842900
    const-string v3, "application"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842901
    invoke-static {v1, v2, p1, p2}, LX/40y;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 842902
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 842903
    if-eqz v2, :cond_3

    .line 842904
    const-string v3, "bylines"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842905
    invoke-static {v1, v2, p1, p2}, LX/58G;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 842906
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 842907
    if-eqz v2, :cond_4

    .line 842908
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842909
    invoke-static {v1, v2, p1, p2}, LX/58H;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 842910
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 842911
    if-eqz v2, :cond_5

    .line 842912
    const-string v3, "feedback"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842913
    invoke-static {v1, v2, p1, p2}, LX/58T;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 842914
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 842915
    if-eqz v2, :cond_6

    .line 842916
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842917
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 842918
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 842919
    if-eqz v2, :cond_7

    .line 842920
    const-string v3, "media"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842921
    invoke-static {v1, v2, p1}, LX/58I;->a(LX/15i;ILX/0nX;)V

    .line 842922
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 842923
    if-eqz v2, :cond_8

    .line 842924
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842925
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 842926
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 842927
    if-eqz v2, :cond_9

    .line 842928
    const-string v3, "redirection_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842929
    invoke-static {v1, v2, p1, p2}, LX/58J;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 842930
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 842931
    if-eqz v2, :cond_a

    .line 842932
    const-string v3, "social_context"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842933
    invoke-static {v1, v2, p1}, LX/58K;->a(LX/15i;ILX/0nX;)V

    .line 842934
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 842935
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 842886
    check-cast p1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
