.class public final Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5e750617
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$SocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 843071
    const-class v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 843072
    const-class v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 843073
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 843074
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843075
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 843076
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 843077
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 843078
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->f:Ljava/util/List;

    .line 843079
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843080
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->g:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->g:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    .line 843081
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->g:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    return-object v0
.end method

.method private m()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBylines"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 843082
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->h:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x3

    const v4, -0x3cf0da9

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->h:LX/3Sb;

    .line 843083
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->h:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843084
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 843085
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private o()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843086
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->j:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->j:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 843087
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->j:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843088
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->k:Ljava/lang/String;

    .line 843089
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method private q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMedia"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843069
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 843070
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->l:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843090
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 843091
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private s()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRedirectionInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 842979
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->n:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0x9

    const v4, -0xeed38c6

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->n:LX/3Sb;

    .line 842980
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->n:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$SocialContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843007
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->o:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$SocialContextModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$SocialContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$SocialContextModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->o:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$SocialContextModel;

    .line 843008
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->o:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$SocialContextModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 842981
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 842982
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 842983
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 842984
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->l()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 842985
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->m()LX/2uF;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v3

    .line 842986
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->n()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, -0x1e86a0d0

    invoke-static {v5, v4, v6}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 842987
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->o()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 842988
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 842989
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->q()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, 0x48cea5bf

    invoke-static {v8, v7, v9}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 842990
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 842991
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->s()LX/2uF;

    move-result-object v9

    invoke-static {v9, p1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v9

    .line 842992
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$SocialContextModel;

    move-result-object v10

    invoke-static {p1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 842993
    const/16 v11, 0xb

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 842994
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 842995
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 842996
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 842997
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 842998
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 842999
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 843000
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 843001
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 843002
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 843003
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 843004
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 843005
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 843006
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 843009
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 843010
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->l()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 843011
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->l()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    .line 843012
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->l()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 843013
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;

    .line 843014
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->g:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    .line 843015
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->m()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 843016
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->m()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 843017
    if-eqz v2, :cond_1

    .line 843018
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;

    .line 843019
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->h:LX/3Sb;

    move-object v1, v0

    .line 843020
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 843021
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x1e86a0d0

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 843022
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 843023
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;

    .line 843024
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->i:I

    move-object v1, v0

    .line 843025
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->o()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 843026
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->o()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 843027
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->o()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 843028
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;

    .line 843029
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->j:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 843030
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 843031
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x48cea5bf

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 843032
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 843033
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;

    .line 843034
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->l:I

    move-object v1, v0

    .line 843035
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 843036
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 843037
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 843038
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;

    .line 843039
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 843040
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->s()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 843041
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->s()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 843042
    if-eqz v2, :cond_6

    .line 843043
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;

    .line 843044
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->n:LX/3Sb;

    move-object v1, v0

    .line 843045
    :cond_6
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$SocialContextModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 843046
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$SocialContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$SocialContextModel;

    .line 843047
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$SocialContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 843048
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;

    .line 843049
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->o:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$SocialContextModel;

    .line 843050
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 843051
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    .line 843052
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 843053
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_8
    move-object p0, v1

    .line 843054
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 843055
    new-instance v0, LX/587;

    invoke-direct {v0, p1}, LX/587;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843056
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 843057
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 843058
    const/4 v0, 0x4

    const v1, -0x1e86a0d0

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->i:I

    .line 843059
    const/4 v0, 0x7

    const v1, 0x48cea5bf

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;->l:I

    .line 843060
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 843061
    invoke-virtual {p2}, LX/18L;->a()V

    .line 843062
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 843063
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 843064
    new-instance v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;-><init>()V

    .line 843065
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 843066
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 843067
    const v0, -0x34b3294a    # -1.342431E7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 843068
    const v0, 0x252222

    return v0
.end method
