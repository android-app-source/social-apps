.class public final Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/220;
.implements LX/57v;
.implements LX/3co;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2023c08f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:I

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Z

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 843910
    const-class v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 843911
    const-class v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 843912
    const/16 v0, 0x1b

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 843913
    return-void
.end method

.method private A()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843914
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 843915
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    return-object v0
.end method

.method private B()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843916
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->C:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->C:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 843917
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->C:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    return-object v0
.end method

.method private C()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ViewerActsAsPersonModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843918
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->D:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ViewerActsAsPersonModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ViewerActsAsPersonModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ViewerActsAsPersonModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->D:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ViewerActsAsPersonModel;

    .line 843919
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->D:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ViewerActsAsPersonModel;

    return-object v0
.end method

.method private D()I
    .locals 2

    .prologue
    .line 843920
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 843921
    iget v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->E:I

    return v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 843922
    iput p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->E:I

    .line 843923
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 843924
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 843925
    if-eqz v0, :cond_0

    .line 843926
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 843927
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 843928
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->u:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    .line 843929
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 843930
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 843931
    if-eqz v0, :cond_0

    .line 843932
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 843933
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 843934
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->v:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    .line 843935
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 843936
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 843937
    if-eqz v0, :cond_0

    .line 843938
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 843939
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 843940
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->A:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    .line 843941
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 843942
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 843943
    if-eqz v0, :cond_0

    .line 843944
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 843945
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 843946
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 843947
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 843948
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 843949
    if-eqz v0, :cond_0

    .line 843950
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x17

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 843951
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 843952
    iput-boolean p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->g:Z

    .line 843953
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 843954
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 843955
    if-eqz v0, :cond_0

    .line 843956
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 843957
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 843958
    iput-boolean p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->k:Z

    .line 843959
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 843960
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 843961
    if-eqz v0, :cond_0

    .line 843962
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 843963
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 843964
    iput-boolean p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->p:Z

    .line 843965
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 843966
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 843967
    if-eqz v0, :cond_0

    .line 843968
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 843969
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 843970
    iput-boolean p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->s:Z

    .line 843971
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 843972
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 843973
    if-eqz v0, :cond_0

    .line 843974
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 843975
    :cond_0
    return-void
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843976
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->o:Ljava/lang/String;

    .line 843977
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method private t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ImportantReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843978
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->r:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ImportantReactorsModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ImportantReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ImportantReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->r:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ImportantReactorsModel;

    .line 843979
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->r:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ImportantReactorsModel;

    return-object v0
.end method

.method private u()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843980
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->u:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->u:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    .line 843981
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->u:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    return-object v0
.end method

.method private v()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843982
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->v:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->v:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    .line 843983
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->v:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    return-object v0
.end method

.method private w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843984
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    .line 843985
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    return-object v0
.end method

.method private x()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843986
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->y:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->y:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    .line 843987
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->y:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    return-object v0
.end method

.method private y()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 843988
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->z:Ljava/util/List;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$SupportedReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->z:Ljava/util/List;

    .line 843989
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->z:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private z()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843862
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->A:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->A:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    .line 843863
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->A:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 20

    .prologue
    .line 843864
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 843865
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->l()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 843866
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->s()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 843867
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->n()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 843868
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ImportantReactorsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 843869
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->p()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 843870
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->u()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 843871
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->v()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 843872
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 843873
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->q()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 843874
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->x()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 843875
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->y()LX/0Px;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 843876
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->z()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 843877
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->A()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 843878
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->B()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 843879
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->C()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ViewerActsAsPersonModel;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 843880
    const/16 v18, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 843881
    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->e:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 843882
    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->f:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 843883
    const/16 v18, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->g:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 843884
    const/16 v18, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->h:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 843885
    const/16 v18, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->i:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 843886
    const/16 v18, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->j:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 843887
    const/16 v18, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->k:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 843888
    const/16 v18, 0x7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->l:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 843889
    const/16 v18, 0x8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->m:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 843890
    const/16 v18, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 843891
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 843892
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 843893
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 843894
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 843895
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->s:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 843896
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 843897
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 843898
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 843899
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 843900
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 843901
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 843902
    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 843903
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 843904
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 843905
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 843906
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 843907
    const/16 v3, 0x1a

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->E:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 843908
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 843909
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 843787
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 843788
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ImportantReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 843789
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ImportantReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ImportantReactorsModel;

    .line 843790
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ImportantReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 843791
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 843792
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->r:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ImportantReactorsModel;

    .line 843793
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->u()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 843794
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->u()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    .line 843795
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->u()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 843796
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 843797
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->u:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    .line 843798
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->v()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 843799
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->v()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    .line 843800
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->v()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 843801
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 843802
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->v:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    .line 843803
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 843804
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    .line 843805
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 843806
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 843807
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    .line 843808
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->x()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 843809
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->x()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    .line 843810
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->x()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 843811
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 843812
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->y:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    .line 843813
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 843814
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->y()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 843815
    if-eqz v2, :cond_5

    .line 843816
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 843817
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->z:Ljava/util/List;

    move-object v1, v0

    .line 843818
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->z()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 843819
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->z()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    .line 843820
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->z()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 843821
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 843822
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->A:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    .line 843823
    :cond_6
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->A()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 843824
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->A()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 843825
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->A()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 843826
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 843827
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 843828
    :cond_7
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->B()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 843829
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->B()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 843830
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->B()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 843831
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 843832
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->C:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 843833
    :cond_8
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->C()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ViewerActsAsPersonModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 843834
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->C()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ViewerActsAsPersonModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ViewerActsAsPersonModel;

    .line 843835
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->C()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ViewerActsAsPersonModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 843836
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 843837
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->D:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ViewerActsAsPersonModel;

    .line 843838
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 843839
    if-nez v1, :cond_a

    :goto_0
    return-object p0

    :cond_a
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 843785
    new-instance v0, LX/589;

    invoke-direct {v0, p1}, LX/589;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843784
    invoke-virtual {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 843770
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 843771
    invoke-virtual {p1, p2, v1}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->e:Z

    .line 843772
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->f:Z

    .line 843773
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->g:Z

    .line 843774
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->h:Z

    .line 843775
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->i:Z

    .line 843776
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->j:Z

    .line 843777
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->k:Z

    .line 843778
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->l:Z

    .line 843779
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->m:Z

    .line 843780
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->p:Z

    .line 843781
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->s:Z

    .line 843782
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->E:I

    .line 843783
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 843718
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 843719
    invoke-virtual {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 843720
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 843721
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 843722
    :goto_0
    return-void

    .line 843723
    :cond_0
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 843724
    invoke-virtual {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 843725
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 843726
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 843727
    :cond_1
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 843728
    invoke-virtual {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 843729
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 843730
    const/16 v0, 0xb

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 843731
    :cond_2
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 843732
    invoke-virtual {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 843733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 843734
    const/16 v0, 0xe

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 843735
    :cond_3
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 843736
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->u()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    move-result-object v0

    .line 843737
    if-eqz v0, :cond_9

    .line 843738
    invoke-virtual {v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 843739
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 843740
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 843741
    :cond_4
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 843742
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->v()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    move-result-object v0

    .line 843743
    if-eqz v0, :cond_9

    .line 843744
    invoke-virtual {v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 843745
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 843746
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 843747
    :cond_5
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 843748
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->x()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    move-result-object v0

    .line 843749
    if-eqz v0, :cond_9

    .line 843750
    invoke-virtual {v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 843751
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 843752
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 843753
    :cond_6
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 843754
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->z()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 843755
    if-eqz v0, :cond_9

    .line 843756
    invoke-virtual {v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 843757
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 843758
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 843759
    :cond_7
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 843760
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->z()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 843761
    if-eqz v0, :cond_9

    .line 843762
    invoke-virtual {v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 843763
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 843764
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 843765
    :cond_8
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 843766
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->D()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 843767
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 843768
    const/16 v0, 0x1a

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 843769
    :cond_9
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 843709
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 843710
    check-cast p2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->a(Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;)V

    .line 843711
    :cond_0
    :goto_0
    return-void

    .line 843712
    :cond_1
    const-string v0, "reactors"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 843713
    check-cast p2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->a(Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;)V

    goto :goto_0

    .line 843714
    :cond_2
    const-string v0, "top_level_comments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 843715
    check-cast p2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->a(Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;)V

    goto :goto_0

    .line 843716
    :cond_3
    const-string v0, "top_reactions"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 843717
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 843658
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 843659
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->a(Z)V

    .line 843660
    :cond_0
    :goto_0
    return-void

    .line 843661
    :cond_1
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 843662
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->b(Z)V

    goto :goto_0

    .line 843663
    :cond_2
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 843664
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->c(Z)V

    goto :goto_0

    .line 843665
    :cond_3
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 843666
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->d(Z)V

    goto :goto_0

    .line 843667
    :cond_4
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 843668
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->u()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    move-result-object v0

    .line 843669
    if-eqz v0, :cond_0

    .line 843670
    if-eqz p3, :cond_5

    .line 843671
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    .line 843672
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;->a(I)V

    .line 843673
    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->u:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;

    goto :goto_0

    .line 843674
    :cond_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$LikersModel;->a(I)V

    goto :goto_0

    .line 843675
    :cond_6
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 843676
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->v()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    move-result-object v0

    .line 843677
    if-eqz v0, :cond_0

    .line 843678
    if-eqz p3, :cond_7

    .line 843679
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    .line 843680
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;->a(I)V

    .line 843681
    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->v:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;

    goto/16 :goto_0

    .line 843682
    :cond_7
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ReactorsModel;->a(I)V

    goto/16 :goto_0

    .line 843683
    :cond_8
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 843684
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->x()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    move-result-object v0

    .line 843685
    if-eqz v0, :cond_0

    .line 843686
    if-eqz p3, :cond_9

    .line 843687
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    .line 843688
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;->a(I)V

    .line 843689
    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->y:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;

    goto/16 :goto_0

    .line 843690
    :cond_9
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$ResharesModel;->a(I)V

    goto/16 :goto_0

    .line 843691
    :cond_a
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 843692
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->z()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 843693
    if-eqz v0, :cond_0

    .line 843694
    if-eqz p3, :cond_b

    .line 843695
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    .line 843696
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;->a(I)V

    .line 843697
    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->A:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    goto/16 :goto_0

    .line 843698
    :cond_b
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;->a(I)V

    goto/16 :goto_0

    .line 843699
    :cond_c
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 843700
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->z()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 843701
    if-eqz v0, :cond_0

    .line 843702
    if-eqz p3, :cond_d

    .line 843703
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    .line 843704
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;->b(I)V

    .line 843705
    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->A:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;

    goto/16 :goto_0

    .line 843706
    :cond_d
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel$TopLevelCommentsModel;->b(I)V

    goto/16 :goto_0

    .line 843707
    :cond_e
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 843708
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->a(I)V

    goto/16 :goto_0
.end method

.method public final ac_()Z
    .locals 2

    .prologue
    .line 843656
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 843657
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->i:Z

    return v0
.end method

.method public final ad_()Z
    .locals 2

    .prologue
    .line 843654
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 843655
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->j:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 843651
    new-instance v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;-><init>()V

    .line 843652
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 843653
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 843649
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 843650
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 843647
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 843648
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->f:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 843840
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 843841
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 843842
    const v0, 0x72933a22

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 843843
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 843844
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 843845
    const v0, -0x78fb05b

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 843846
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 843847
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->k:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 843848
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 843849
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->m:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843850
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->n:Ljava/lang/String;

    .line 843851
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 843852
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 843853
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->p:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843854
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->q:Ljava/lang/String;

    .line 843855
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 843856
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 843857
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->s:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843858
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->t:Ljava/lang/String;

    .line 843859
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843860
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->x:Ljava/lang/String;

    .line 843861
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic r()LX/59N;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843786
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;->B()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    return-object v0
.end method
