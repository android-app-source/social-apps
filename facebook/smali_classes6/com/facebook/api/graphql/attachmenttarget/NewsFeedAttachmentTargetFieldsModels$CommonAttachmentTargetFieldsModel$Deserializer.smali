.class public final Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 842817
    const-class v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 842818
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 842819
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 842820
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 842821
    const/4 v11, 0x0

    .line 842822
    const/4 v10, 0x0

    .line 842823
    const/4 v9, 0x0

    .line 842824
    const/4 v8, 0x0

    .line 842825
    const/4 v7, 0x0

    .line 842826
    const/4 v6, 0x0

    .line 842827
    const/4 v5, 0x0

    .line 842828
    const/4 v4, 0x0

    .line 842829
    const/4 v3, 0x0

    .line 842830
    const/4 v2, 0x0

    .line 842831
    const/4 v1, 0x0

    .line 842832
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, p0, :cond_2

    .line 842833
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 842834
    const/4 v1, 0x0

    .line 842835
    :goto_0
    move v1, v1

    .line 842836
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 842837
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 842838
    new-instance v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;

    invoke-direct {v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$CommonAttachmentTargetFieldsModel;-><init>()V

    .line 842839
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 842840
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 842841
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 842842
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 842843
    :cond_0
    return-object v1

    .line 842844
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 842845
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_e

    .line 842846
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 842847
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 842848
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 842849
    const-string p0, "__type__"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 842850
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v11

    goto :goto_1

    .line 842851
    :cond_4
    const-string p0, "android_urls"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 842852
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 842853
    :cond_5
    const-string p0, "application"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 842854
    invoke-static {p1, v0}, LX/40y;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 842855
    :cond_6
    const-string p0, "bylines"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 842856
    invoke-static {p1, v0}, LX/58G;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 842857
    :cond_7
    const-string p0, "cover_photo"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 842858
    invoke-static {p1, v0}, LX/58H;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 842859
    :cond_8
    const-string p0, "feedback"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 842860
    invoke-static {p1, v0}, LX/58T;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 842861
    :cond_9
    const-string p0, "id"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 842862
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 842863
    :cond_a
    const-string p0, "media"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    .line 842864
    invoke-static {p1, v0}, LX/58I;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 842865
    :cond_b
    const-string p0, "profile_picture"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 842866
    invoke-static {p1, v0}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 842867
    :cond_c
    const-string p0, "redirection_info"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_d

    .line 842868
    invoke-static {p1, v0}, LX/58J;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 842869
    :cond_d
    const-string p0, "social_context"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 842870
    invoke-static {p1, v0}, LX/58K;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 842871
    :cond_e
    const/16 v12, 0xb

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 842872
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 842873
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 842874
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 842875
    const/4 v9, 0x3

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 842876
    const/4 v8, 0x4

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 842877
    const/4 v7, 0x5

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 842878
    const/4 v6, 0x6

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 842879
    const/4 v5, 0x7

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 842880
    const/16 v4, 0x8

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 842881
    const/16 v3, 0x9

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 842882
    const/16 v2, 0xa

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 842883
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
