.class public final Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/55t;
.implements LX/57w;
.implements LX/580;
.implements LX/57x;
.implements LX/582;
.implements LX/57z;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xa58379a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$Serializer;
.end annotation


# instance fields
.field private A:D

.field private B:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private O:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private P:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Q:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$CommentParentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private S:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private T:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private V:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private X:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Y:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aA:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$GlobalShareModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aB:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aC:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$GreetingCardTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aD:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aE:Z

.field private aF:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aG:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aH:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aI:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aJ:Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aK:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aL:Z

.field private aM:Z

.field private aN:Z

.field private aO:Z

.field private aP:Z

.field private aQ:Z

.field private aR:Z

.field private aS:Z

.field private aT:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aU:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aV:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aX:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aY:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aZ:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aa:J

.field private ab:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ac:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ad:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentVideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ae:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private af:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ag:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ah:I

.field private ai:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aj:I

.field private ak:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$EmployerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private al:J

.field private am:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private an:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ao:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ap:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aq:J

.field private ar:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private as:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private at:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private au:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private av:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aw:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ax:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ay:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private az:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$FundraiserProgressTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bA:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bB:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bC:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bD:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphActionAttachmentTargetModel$PrimaryObjectNodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bE:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bF:Z

.field private bG:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bH:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bI:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bJ:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bK:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bL:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bM:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bN:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolClassModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bO:Z

.field private bP:Z

.field private bQ:Z

.field private bR:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bS:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bT:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bU:Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bV:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SportsAttachmentFieldsModel$SportsMatchDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bX:J

.field private bY:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bZ:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ba:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bb:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bc:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bd:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private be:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bf:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bg:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bh:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$AudioAttachmentFieldsModel$MusicObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bi:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bj:Lcom/facebook/graphql/enums/GraphQLMusicType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bk:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphObjectAttachmentTargetModel$MusiciansModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bl:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bm:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bn:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bo:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bp:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bq:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ProductItemAttachmentModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private br:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bs:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bt:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bu:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bv:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bw:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bx:D

.field private by:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bz:Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ca:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cb:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cc:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cd:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ce:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cf:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cg:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ch:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$UserDonorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ci:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cj:Z

.field private ck:Z

.field private cl:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cm:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cn:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private co:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$WorkProjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cp:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:J

.field private m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 845784
    const-class v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 845826
    const-class v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 845846
    const/16 v0, 0xa8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 845847
    return-void
.end method

.method private K()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845827
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 845828
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 845829
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private L()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845830
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845831
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private M()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActionButtonTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845832
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 845833
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private N()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845834
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->h:Ljava/lang/String;

    .line 845835
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private O()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActionLinks"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845836
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->i:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x4

    const v4, -0x5cda772c

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->i:LX/3Sb;

    .line 845837
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->i:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private P()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdditionalAccentImages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845838
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->j:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x5

    const v4, -0x13519e79

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->j:LX/3Sb;

    .line 845839
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->j:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private Q()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$AddressModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845840
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->k:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$AddressModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$AddressModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$AddressModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->k:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$AddressModel;

    .line 845841
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->k:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$AddressModel;

    return-object v0
.end method

.method private R()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845842
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 845843
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    return-object v0
.end method

.method private S()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845844
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->n:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->n:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    .line 845845
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->n:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    return-object v0
.end method

.method private T()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845822
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->o:Ljava/lang/String;

    .line 845823
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method private U()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845848
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->p:Ljava/util/List;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->p:Ljava/util/List;

    .line 845849
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->p:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private V()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845850
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845851
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private W()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845852
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->r:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->r:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    .line 845853
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->r:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    return-object v0
.end method

.method private X()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845854
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->s:Ljava/lang/String;

    .line 845855
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method private Y()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845856
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->t:Ljava/lang/String;

    .line 845857
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->t:Ljava/lang/String;

    return-object v0
.end method

.method private Z()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845858
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->u:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->u:Ljava/lang/String;

    .line 845859
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->u:Ljava/lang/String;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 845860
    iput p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ah:I

    .line 845861
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 845862
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 845863
    if-eqz v0, :cond_0

    .line 845864
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 845865
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 845866
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aV:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    .line 845867
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 845868
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 845869
    if-eqz v0, :cond_0

    .line 845870
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x5f

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 845871
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 845872
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->S:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    .line 845873
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 845874
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 845875
    if-eqz v0, :cond_0

    .line 845876
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x28

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 845877
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 845878
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bn:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;

    .line 845879
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 845880
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 845881
    if-eqz v0, :cond_0

    .line 845882
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x71

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 845883
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 4

    .prologue
    .line 845884
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ci:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 845885
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 845886
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 845887
    if-eqz v0, :cond_0

    .line 845888
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0xa0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 845889
    :cond_0
    return-void

    .line 845890
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 4

    .prologue
    .line 845793
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cn:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 845794
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 845795
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 845796
    if-eqz v0, :cond_0

    .line 845797
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0xa5

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 845798
    :cond_0
    return-void

    .line 845799
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 845752
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ax:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 845753
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 845754
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 845755
    if-eqz v0, :cond_0

    .line 845756
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x47

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 845757
    :cond_0
    return-void

    .line 845758
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 4

    .prologue
    .line 845759
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cl:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 845760
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 845761
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 845762
    if-eqz v0, :cond_0

    .line 845763
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0xa3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 845764
    :cond_0
    return-void

    .line 845765
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 845766
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->T:Ljava/util/List;

    .line 845767
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 845768
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 845769
    if-eqz v0, :cond_0

    .line 845770
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x29

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 845771
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 845772
    iput-boolean p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aS:Z

    .line 845773
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 845774
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 845775
    if-eqz v0, :cond_0

    .line 845776
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x5c

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 845777
    :cond_0
    return-void
.end method

.method private aA()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCurrentPrice"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845778
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 845779
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ae:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private aB()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDataPoints"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845780
    const/4 v0, 0x6

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 845781
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->af:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private aC()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845782
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ai:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;

    const/16 v1, 0x38

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ai:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;

    .line 845783
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ai:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;

    return-object v0
.end method

.method private aD()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$EmployerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844770
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ak:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$EmployerModel;

    const/16 v1, 0x3a

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$EmployerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$EmployerModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ak:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$EmployerModel;

    .line 844771
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ak:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$EmployerModel;

    return-object v0
.end method

.method private aE()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845785
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->am:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;

    const/16 v1, 0x3c

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->am:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;

    .line 845786
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->am:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;

    return-object v0
.end method

.method private aF()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845787
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->an:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    const/16 v1, 0x3d

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->an:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    .line 845788
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->an:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    return-object v0
.end method

.method private aG()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845789
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ao:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ao:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;

    .line 845790
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ao:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;

    return-object v0
.end method

.method private aH()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845791
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ap:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;

    const/16 v1, 0x3f

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ap:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;

    .line 845792
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ap:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;

    return-object v0
.end method

.method private aI()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFavicon"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845750
    const/16 v0, 0x8

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 845751
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ar:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private aJ()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845800
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->as:Ljava/lang/String;

    const/16 v1, 0x42

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->as:Ljava/lang/String;

    .line 845801
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->as:Ljava/lang/String;

    return-object v0
.end method

.method private aK()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845802
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->at:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    const/16 v1, 0x43

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->at:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 845803
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->at:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    return-object v0
.end method

.method private aL()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845804
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->au:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;

    const/16 v1, 0x44

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->au:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;

    .line 845805
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->au:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;

    return-object v0
.end method

.method private aM()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845806
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->av:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;

    const/16 v1, 0x45

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->av:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;

    .line 845807
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->av:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;

    return-object v0
.end method

.method private aN()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845808
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aw:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    const/16 v1, 0x46

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aw:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    .line 845809
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aw:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    return-object v0
.end method

.method private aO()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845810
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ax:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/16 v1, 0x47

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ax:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 845811
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ax:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method private aP()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845812
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ay:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    const/16 v1, 0x48

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ay:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 845813
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ay:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private aQ()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$FundraiserProgressTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845814
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->az:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$FundraiserProgressTextModel;

    const/16 v1, 0x49

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$FundraiserProgressTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$FundraiserProgressTextModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->az:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$FundraiserProgressTextModel;

    .line 845815
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->az:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$FundraiserProgressTextModel;

    return-object v0
.end method

.method private aR()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$GlobalShareModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGlobalShare"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845816
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aA:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$GlobalShareModel;

    const/16 v1, 0x4a

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$GlobalShareModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$GlobalShareModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aA:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$GlobalShareModel;

    .line 845817
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aA:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$GlobalShareModel;

    return-object v0
.end method

.method private aS()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845818
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aB:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0x4b

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aB:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 845819
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aB:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private aT()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$GreetingCardTemplateModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGreetingCardTemplate"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845820
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aC:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$GreetingCardTemplateModel;

    const/16 v1, 0x4c

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$GreetingCardTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$GreetingCardTemplateModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aC:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$GreetingCardTemplateModel;

    .line 845821
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aC:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$GreetingCardTemplateModel;

    return-object v0
.end method

.method private aU()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845953
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aD:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    const/16 v1, 0x4d

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aD:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 845954
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aD:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private aV()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845927
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aG:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x50

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aG:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845928
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aG:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private aW()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845955
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aH:Ljava/lang/String;

    const/16 v1, 0x51

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aH:Ljava/lang/String;

    .line 845956
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aH:Ljava/lang/String;

    return-object v0
.end method

.method private aX()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getInstantArticle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845957
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aI:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    const/16 v1, 0x52

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aI:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    .line 845958
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aI:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    return-object v0
.end method

.method private aY()Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845959
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aJ:Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    const/16 v1, 0x53

    const-class v2, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aJ:Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    .line 845960
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aJ:Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    return-object v0
.end method

.method private aZ()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845961
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aK:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;

    const/16 v1, 0x54

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aK:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;

    .line 845962
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aK:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;

    return-object v0
.end method

.method private aa()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845963
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->v:Ljava/util/List;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->v:Ljava/util/List;

    .line 845964
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->v:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private ab()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttachments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845965
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->w:Ljava/util/List;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->w:Ljava/util/List;

    .line 845966
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->w:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private ac()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845967
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->x:Ljava/lang/String;

    .line 845968
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->x:Ljava/lang/String;

    return-object v0
.end method

.method private ad()Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845969
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->y:Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->y:Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;

    .line 845970
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->y:Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;

    return-object v0
.end method

.method private ae()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845971
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->z:Ljava/lang/String;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->z:Ljava/lang/String;

    .line 845972
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->z:Ljava/lang/String;

    return-object v0
.end method

.method private af()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845973
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->B:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->B:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 845974
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->B:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method private ag()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBylines"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845989
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->C:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0x18

    const v4, -0x3cf0da9

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->C:LX/3Sb;

    .line 845990
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->C:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private ah()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCampaign"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845975
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->D:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->D:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel;

    .line 845976
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->D:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel;

    return-object v0
.end method

.method private ai()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845991
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->E:Ljava/lang/String;

    const/16 v1, 0x1a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->E:Ljava/lang/String;

    .line 845992
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->E:Ljava/lang/String;

    return-object v0
.end method

.method private aj()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845997
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->L:Ljava/lang/String;

    const/16 v1, 0x21

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->L:Ljava/lang/String;

    .line 845998
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->L:Ljava/lang/String;

    return-object v0
.end method

.method private ak()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845995
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->M:Ljava/util/List;

    const/16 v1, 0x22

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->M:Ljava/util/List;

    .line 845996
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->M:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private al()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845987
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->N:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->N:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;

    .line 845988
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->N:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;

    return-object v0
.end method

.method private am()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845993
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->O:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->O:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    .line 845994
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->O:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    return-object v0
.end method

.method private an()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845985
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->P:Ljava/util/List;

    const/16 v1, 0x25

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->P:Ljava/util/List;

    .line 845986
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->P:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private ao()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$CommentParentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845983
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Q:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$CommentParentModel;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$CommentParentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$CommentParentModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Q:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$CommentParentModel;

    .line 845984
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Q:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$CommentParentModel;

    return-object v0
.end method

.method private ap()Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845981
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->R:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->R:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    .line 845982
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->R:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    return-object v0
.end method

.method private aq()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getConfirmedLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845979
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->S:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->S:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    .line 845980
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->S:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    return-object v0
.end method

.method private ar()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getConfirmedPlacesForAttachment"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845951
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->T:Ljava/util/List;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->T:Ljava/util/List;

    .line 845952
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->T:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private as()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845977
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->U:Ljava/util/List;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->U:Ljava/util/List;

    .line 845978
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->U:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private at()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845893
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->W:Ljava/lang/String;

    const/16 v1, 0x2c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->W:Ljava/lang/String;

    .line 845894
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->W:Ljava/lang/String;

    return-object v0
.end method

.method private au()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845895
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->X:Ljava/lang/String;

    const/16 v1, 0x2d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->X:Ljava/lang/String;

    .line 845896
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->X:Ljava/lang/String;

    return-object v0
.end method

.method private av()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845897
    const/4 v0, 0x5

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 845898
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Y:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private aw()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845899
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Z:Ljava/lang/String;

    const/16 v1, 0x2f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Z:Ljava/lang/String;

    .line 845900
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Z:Ljava/lang/String;

    return-object v0
.end method

.method private ax()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845901
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ab:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ab:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    .line 845902
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ab:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    return-object v0
.end method

.method private ay()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCulturalMomentImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845903
    const/4 v0, 0x6

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 845904
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ac:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private az()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentVideoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845905
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ad:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentVideoModel;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentVideoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentVideoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ad:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentVideoModel;

    .line 845906
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ad:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentVideoModel;

    return-object v0
.end method

.method private b(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 845907
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bt:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    .line 845908
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 845909
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 845910
    if-eqz v0, :cond_0

    .line 845911
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x77

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 845912
    :cond_0
    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 845913
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->U:Ljava/util/List;

    .line 845914
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 845915
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 845916
    if-eqz v0, :cond_0

    .line 845917
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 845918
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 845919
    iput-boolean p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cj:Z

    .line 845920
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 845921
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 845922
    if-eqz v0, :cond_0

    .line 845923
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 845924
    :cond_0
    return-void
.end method

.method private bA()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845925
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bu:Ljava/util/List;

    const/16 v1, 0x78

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bu:Ljava/util/List;

    .line 845926
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bu:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bB()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPendingPlacesForAttachment"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845891
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bv:Ljava/util/List;

    const/16 v1, 0x79

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bv:Ljava/util/List;

    .line 845892
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bv:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bC()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845929
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bw:Ljava/util/List;

    const/16 v1, 0x7a

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bw:Ljava/util/List;

    .line 845930
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bw:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bD()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845931
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->by:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    const/16 v1, 0x7c

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->by:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 845932
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->by:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private bE()Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845933
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bz:Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;

    const/16 v1, 0x7d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bz:Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;

    .line 845934
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bz:Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;

    return-object v0
.end method

.method private bF()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPreviewUrls"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845935
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bA:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0x7e

    const v4, -0x6e52f25

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bA:LX/3Sb;

    .line 845936
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bA:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private bG()Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845937
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bB:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    const/16 v1, 0x7f

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bB:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    .line 845938
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bB:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    return-object v0
.end method

.method private bH()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845939
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bC:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x80

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bC:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845940
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bC:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private bI()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphActionAttachmentTargetModel$PrimaryObjectNodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPrimaryObjectNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845941
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bD:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphActionAttachmentTargetModel$PrimaryObjectNodeModel;

    const/16 v1, 0x81

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphActionAttachmentTargetModel$PrimaryObjectNodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphActionAttachmentTargetModel$PrimaryObjectNodeModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bD:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphActionAttachmentTargetModel$PrimaryObjectNodeModel;

    .line 845942
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bD:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphActionAttachmentTargetModel$PrimaryObjectNodeModel;

    return-object v0
.end method

.method private bJ()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845943
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bE:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x82

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bE:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845944
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bE:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private bK()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getQuote"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845945
    const/16 v0, 0x10

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 845946
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bG:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private bL()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRating"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845947
    const/16 v0, 0x10

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 845948
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bH:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private bM()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845949
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bI:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x86

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bI:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845950
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bI:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private bN()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRedirectionInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 845824
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bJ:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0x87

    const v4, -0xeed38c6

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bJ:LX/3Sb;

    .line 845825
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bJ:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private bO()Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844678
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bK:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    const/16 v1, 0x88

    const-class v2, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bK:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    .line 844679
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bK:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    return-object v0
.end method

.method private bP()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844684
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bL:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    const/16 v1, 0x89

    const-class v2, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bL:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    .line 844685
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bL:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    return-object v0
.end method

.method private bQ()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844686
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bM:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolModel;

    const/16 v1, 0x8a

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bM:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolModel;

    .line 844687
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bM:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolModel;

    return-object v0
.end method

.method private bR()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolClassModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844688
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bN:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolClassModel;

    const/16 v1, 0x8b

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolClassModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolClassModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bN:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolClassModel;

    .line 844689
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bN:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolClassModel;

    return-object v0
.end method

.method private bS()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSlides"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844690
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bR:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel;

    const/16 v1, 0x8f

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bR:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel;

    .line 844691
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bR:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel;

    return-object v0
.end method

.method private bT()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844692
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bS:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;

    const/16 v1, 0x90

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bS:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;

    .line 844693
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bS:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;

    return-object v0
.end method

.method private bU()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844694
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bT:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0x91

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bT:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 844695
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bT:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private bV()Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844696
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bU:Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    const/16 v1, 0x92

    const-class v2, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bU:Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    .line 844697
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bU:Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    return-object v0
.end method

.method private bW()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SportsAttachmentFieldsModel$SportsMatchDataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844698
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bV:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SportsAttachmentFieldsModel$SportsMatchDataModel;

    const/16 v1, 0x93

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SportsAttachmentFieldsModel$SportsMatchDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SportsAttachmentFieldsModel$SportsMatchDataModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bV:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SportsAttachmentFieldsModel$SportsMatchDataModel;

    .line 844699
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bV:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SportsAttachmentFieldsModel$SportsMatchDataModel;

    return-object v0
.end method

.method private bX()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844700
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    const/16 v1, 0x94

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    .line 844701
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    return-object v0
.end method

.method private bY()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844702
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bY:Ljava/lang/String;

    const/16 v1, 0x96

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bY:Ljava/lang/String;

    .line 844703
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bY:Ljava/lang/String;

    return-object v0
.end method

.method private bZ()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844704
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bZ:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const/16 v1, 0x97

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bZ:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 844705
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bZ:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    return-object v0
.end method

.method private ba()Z
    .locals 2

    .prologue
    .line 844682
    const/16 v0, 0xb

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844683
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aS:Z

    return v0
.end method

.method private bb()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getItemPrice"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844708
    const/16 v0, 0xb

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844709
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aT:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private bc()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 844710
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aU:Ljava/util/List;

    const/16 v1, 0x5e

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aU:Ljava/util/List;

    .line 844711
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aU:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bd()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getListItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844712
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aV:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    const/16 v1, 0x5f

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aV:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    .line 844713
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aV:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    return-object v0
.end method

.method private be()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844714
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    const/16 v1, 0x60

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 844715
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    return-object v0
.end method

.method private bf()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844716
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aX:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x61

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aX:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 844717
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aX:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private bg()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMapBoundingBox"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844718
    const/16 v0, 0xc

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844719
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aY:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private bh()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMedia"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844720
    const/16 v0, 0xc

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844721
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aZ:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private bi()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844722
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ba:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel;

    const/16 v1, 0x64

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ba:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel;

    .line 844723
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ba:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel;

    return-object v0
.end method

.method private bj()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844724
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bb:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel;

    const/16 v1, 0x65

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bb:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel;

    .line 844725
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bb:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel;

    return-object v0
.end method

.method private bk()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMediaQuestionPhotos"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 844726
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bc:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0x66

    const v4, 0x7deccd33

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bc:LX/3Sb;

    .line 844727
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bc:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private bl()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844728
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bd:Ljava/lang/String;

    const/16 v1, 0x67

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bd:Ljava/lang/String;

    .line 844729
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bd:Ljava/lang/String;

    return-object v0
.end method

.method private bm()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844706
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->be:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0x68

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->be:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 844707
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->be:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private bn()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844632
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bf:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    const/16 v1, 0x69

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bf:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    .line 844633
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bf:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    return-object v0
.end method

.method private bo()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$AudioAttachmentFieldsModel$MusicObjectModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMusicObject"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844652
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bh:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$AudioAttachmentFieldsModel$MusicObjectModel;

    const/16 v1, 0x6b

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$AudioAttachmentFieldsModel$MusicObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$AudioAttachmentFieldsModel$MusicObjectModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bh:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$AudioAttachmentFieldsModel$MusicObjectModel;

    .line 844653
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bh:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$AudioAttachmentFieldsModel$MusicObjectModel;

    return-object v0
.end method

.method private bp()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844650
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bi:Ljava/lang/String;

    const/16 v1, 0x6c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bi:Ljava/lang/String;

    .line 844651
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bi:Ljava/lang/String;

    return-object v0
.end method

.method private bq()Lcom/facebook/graphql/enums/GraphQLMusicType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844648
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bj:Lcom/facebook/graphql/enums/GraphQLMusicType;

    const/16 v1, 0x6d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMusicType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMusicType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMusicType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMusicType;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bj:Lcom/facebook/graphql/enums/GraphQLMusicType;

    .line 844649
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bj:Lcom/facebook/graphql/enums/GraphQLMusicType;

    return-object v0
.end method

.method private br()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphObjectAttachmentTargetModel$MusiciansModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 844646
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bk:Ljava/util/List;

    const/16 v1, 0x6e

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphObjectAttachmentTargetModel$MusiciansModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bk:Ljava/util/List;

    .line 844647
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bk:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bs()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOpenGraphNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844644
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bm:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    const/16 v1, 0x70

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bm:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    .line 844645
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bm:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    return-object v0
.end method

.method private bt()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOptions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844642
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bn:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;

    const/16 v1, 0x71

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bn:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;

    .line 844643
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bn:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;

    return-object v0
.end method

.method private bu()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOverallStarRating"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844640
    const/16 v0, 0xe

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844641
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bo:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private bv()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844638
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bp:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$OwnerModel;

    const/16 v1, 0x73

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bp:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$OwnerModel;

    .line 844639
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bp:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$OwnerModel;

    return-object v0
.end method

.method private bw()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ProductItemAttachmentModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844636
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bq:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ProductItemAttachmentModel$PageModel;

    const/16 v1, 0x74

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ProductItemAttachmentModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ProductItemAttachmentModel$PageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bq:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ProductItemAttachmentModel$PageModel;

    .line 844637
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bq:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ProductItemAttachmentModel$PageModel;

    return-object v0
.end method

.method private bx()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageLikers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844634
    const/16 v0, 0xe

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844635
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->br:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private by()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageVisits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844630
    const/16 v0, 0xe

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844631
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bs:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private bz()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPendingLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844654
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bt:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    const/16 v1, 0x77

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bt:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    .line 844655
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bt:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    return-object v0
.end method

.method private c(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 844656
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aU:Ljava/util/List;

    .line 844657
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 844658
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 844659
    if-eqz v0, :cond_0

    .line 844660
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x5e

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 844661
    :cond_0
    return-void
.end method

.method private ca()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844662
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ca:Ljava/lang/String;

    const/16 v1, 0x98

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ca:Ljava/lang/String;

    .line 844663
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ca:Ljava/lang/String;

    return-object v0
.end method

.method private cb()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844664
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cb:Ljava/lang/String;

    const/16 v1, 0x99

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cb:Ljava/lang/String;

    .line 844665
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cb:Ljava/lang/String;

    return-object v0
.end method

.method private cc()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 844666
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cc:Ljava/util/List;

    const/16 v1, 0x9a

    const-class v2, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cc:Ljava/util/List;

    .line 844667
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cc:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private cd()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThrowbackMediaAttachments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 844668
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cd:Ljava/util/List;

    const/16 v1, 0x9b

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ThrowbackMediaAttachmentFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cd:Ljava/util/List;

    .line 844669
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cd:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private ce()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$TitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844670
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cf:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$TitleModel;

    const/16 v1, 0x9d

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$TitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$TitleModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cf:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$TitleModel;

    .line 844671
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cf:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$TitleModel;

    return-object v0
.end method

.method private cf()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$UserDonorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844672
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ch:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$UserDonorsModel;

    const/16 v1, 0x9f

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$UserDonorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$UserDonorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ch:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$UserDonorsModel;

    .line 844673
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ch:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$UserDonorsModel;

    return-object v0
.end method

.method private cg()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844674
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cl:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0xa3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cl:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 844675
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cl:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method private ch()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerVisits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844676
    const/16 v0, 0x14

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844677
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cm:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private ci()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$WorkProjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844680
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->co:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$WorkProjectModel;

    const/16 v1, 0xa6

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$WorkProjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$WorkProjectModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->co:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$WorkProjectModel;

    .line 844681
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->co:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$WorkProjectModel;

    return-object v0
.end method

.method private cj()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844802
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cp:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    const/16 v1, 0xa7

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cp:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    .line 844803
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cp:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    return-object v0
.end method

.method private d(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 844773
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bu:Ljava/util/List;

    .line 844774
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 844775
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 844776
    if-eqz v0, :cond_0

    .line 844777
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x78

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 844778
    :cond_0
    return-void
.end method

.method private e(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 844779
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bv:Ljava/util/List;

    .line 844780
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 844781
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 844782
    if-eqz v0, :cond_0

    .line 844783
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x79

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 844784
    :cond_0
    return-void
.end method

.method private f(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 844785
    iput-object p1, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bw:Ljava/util/List;

    .line 844786
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 844787
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 844788
    if-eqz v0, :cond_0

    .line 844789
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x7a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 844790
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic A()LX/585;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844791
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aC()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic B()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844792
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aN()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final C()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844793
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bg:Ljava/lang/String;

    const/16 v1, 0x6a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bg:Ljava/lang/String;

    .line 844794
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bg:Ljava/lang/String;

    return-object v0
.end method

.method public final D()Z
    .locals 2

    .prologue
    .line 844795
    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844796
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->I:Z

    return v0
.end method

.method public final E()I
    .locals 2

    .prologue
    .line 844797
    const/4 v0, 0x6

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844798
    iget v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ah:I

    return v0
.end method

.method public final synthetic F()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844799
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aZ()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic G()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844800
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cj()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic H()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getConfirmedLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844801
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aq()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic I()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getListItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844772
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bd()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic J()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPendingLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844804
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bz()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 143

    .prologue
    .line 844805
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 844806
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 844807
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->L()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 844808
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->M()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, -0x3b404242

    invoke-static {v7, v6, v8}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 844809
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->N()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 844810
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->O()LX/2uF;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v8, v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v8

    .line 844811
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->P()LX/2uF;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v9, v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v9

    .line 844812
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Q()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$AddressModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 844813
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->R()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 844814
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->S()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 844815
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->T()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 844816
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->U()LX/0Px;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/util/List;)I

    move-result v14

    .line 844817
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->V()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 844818
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->W()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 844819
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->X()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 844820
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Y()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 844821
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Z()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 844822
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aa()LX/0Px;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v20

    .line 844823
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ab()LX/0Px;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v21

    .line 844824
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ac()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 844825
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ad()Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 844826
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ae()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 844827
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->af()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 844828
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ag()LX/2uF;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v26

    .line 844829
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ah()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 844830
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ai()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 844831
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aj()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 844832
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ak()LX/0Px;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v30

    .line 844833
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->al()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 844834
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->am()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 844835
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->an()LX/0Px;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v33

    .line 844836
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ao()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$CommentParentModel;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 844837
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ap()Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v35

    .line 844838
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aq()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 844839
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ar()LX/0Px;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v37

    .line 844840
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->as()LX/0Px;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v38

    .line 844841
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->c()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v39

    .line 844842
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->at()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    .line 844843
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->au()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v41

    .line 844844
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->av()LX/1vs;

    move-result-object v42

    move-object/from16 v0, v42

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v43, v0

    move-object/from16 v0, v42

    iget v0, v0, LX/1vs;->b:I

    move/from16 v42, v0

    const v44, -0x1e86a0d0

    move-object/from16 v0, v43

    move/from16 v1, v42

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 844845
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aw()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v43

    .line 844846
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ax()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 844847
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ay()LX/1vs;

    move-result-object v45

    move-object/from16 v0, v45

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v46, v0

    move-object/from16 v0, v45

    iget v0, v0, LX/1vs;->b:I

    move/from16 v45, v0

    const v47, 0x749916b7

    move-object/from16 v0, v46

    move/from16 v1, v45

    move/from16 v2, v47

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 844848
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->az()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentVideoModel;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 844849
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aA()LX/1vs;

    move-result-object v47

    move-object/from16 v0, v47

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v48, v0

    move-object/from16 v0, v47

    iget v0, v0, LX/1vs;->b:I

    move/from16 v47, v0

    const v49, 0x41dc040c

    move-object/from16 v0, v48

    move/from16 v1, v47

    move/from16 v2, v49

    invoke-static {v0, v1, v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 844850
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aB()LX/1vs;

    move-result-object v48

    move-object/from16 v0, v48

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v49, v0

    move-object/from16 v0, v48

    iget v0, v0, LX/1vs;->b:I

    move/from16 v48, v0

    const v50, -0x481f8bd

    move-object/from16 v0, v49

    move/from16 v1, v48

    move/from16 v2, v50

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v48

    .line 844851
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->z()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v49

    .line 844852
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aC()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 844853
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aD()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$EmployerModel;

    move-result-object v51

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 844854
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aE()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 844855
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aF()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 844856
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aG()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v54

    .line 844857
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aH()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;

    move-result-object v55

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v55

    .line 844858
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aI()LX/1vs;

    move-result-object v56

    move-object/from16 v0, v56

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v57, v0

    move-object/from16 v0, v56

    iget v0, v0, LX/1vs;->b:I

    move/from16 v56, v0

    const v58, 0xe7b5c41

    move-object/from16 v0, v57

    move/from16 v1, v56

    move/from16 v2, v58

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v56

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v56

    .line 844859
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aJ()Ljava/lang/String;

    move-result-object v57

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v57

    .line 844860
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aK()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    move-result-object v58

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v58

    .line 844861
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aL()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;

    move-result-object v59

    move-object/from16 v0, p1

    move-object/from16 v1, v59

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v59

    .line 844862
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aM()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;

    move-result-object v60

    move-object/from16 v0, p1

    move-object/from16 v1, v60

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v60

    .line 844863
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aN()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    move-result-object v61

    move-object/from16 v0, p1

    move-object/from16 v1, v61

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v61

    .line 844864
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aO()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v62

    move-object/from16 v0, p1

    move-object/from16 v1, v62

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v62

    .line 844865
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aP()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v63

    move-object/from16 v0, p1

    move-object/from16 v1, v63

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v63

    .line 844866
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aQ()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$FundraiserProgressTextModel;

    move-result-object v64

    move-object/from16 v0, p1

    move-object/from16 v1, v64

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v64

    .line 844867
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aR()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$GlobalShareModel;

    move-result-object v65

    move-object/from16 v0, p1

    move-object/from16 v1, v65

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v65

    .line 844868
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aS()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v66

    move-object/from16 v0, p1

    move-object/from16 v1, v66

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v66

    .line 844869
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aT()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$GreetingCardTemplateModel;

    move-result-object v67

    move-object/from16 v0, p1

    move-object/from16 v1, v67

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v67

    .line 844870
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aU()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v68

    move-object/from16 v0, p1

    move-object/from16 v1, v68

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v68

    .line 844871
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->m()Ljava/lang/String;

    move-result-object v69

    move-object/from16 v0, p1

    move-object/from16 v1, v69

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v69

    .line 844872
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aV()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v70

    move-object/from16 v0, p1

    move-object/from16 v1, v70

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v70

    .line 844873
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aW()Ljava/lang/String;

    move-result-object v71

    move-object/from16 v0, p1

    move-object/from16 v1, v71

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v71

    .line 844874
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aX()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    move-result-object v72

    move-object/from16 v0, p1

    move-object/from16 v1, v72

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v72

    .line 844875
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aY()Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    move-result-object v73

    move-object/from16 v0, p1

    move-object/from16 v1, v73

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v73

    .line 844876
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aZ()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;

    move-result-object v74

    move-object/from16 v0, p1

    move-object/from16 v1, v74

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v74

    .line 844877
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bb()LX/1vs;

    move-result-object v75

    move-object/from16 v0, v75

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v76, v0

    move-object/from16 v0, v75

    iget v0, v0, LX/1vs;->b:I

    move/from16 v75, v0

    const v77, 0x3d9090a6

    move-object/from16 v0, v76

    move/from16 v1, v75

    move/from16 v2, v77

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v75

    move-object/from16 v0, p1

    move-object/from16 v1, v75

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v75

    .line 844878
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bc()LX/0Px;

    move-result-object v76

    move-object/from16 v0, p1

    move-object/from16 v1, v76

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v76

    .line 844879
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bd()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    move-result-object v77

    move-object/from16 v0, p1

    move-object/from16 v1, v77

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v77

    .line 844880
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->be()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v78

    move-object/from16 v0, p1

    move-object/from16 v1, v78

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v78

    .line 844881
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bf()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v79

    move-object/from16 v0, p1

    move-object/from16 v1, v79

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v79

    .line 844882
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bg()LX/1vs;

    move-result-object v80

    move-object/from16 v0, v80

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v81, v0

    move-object/from16 v0, v80

    iget v0, v0, LX/1vs;->b:I

    move/from16 v80, v0

    const v82, 0x180f02e8

    move-object/from16 v0, v81

    move/from16 v1, v80

    move/from16 v2, v82

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v80

    move-object/from16 v0, p1

    move-object/from16 v1, v80

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v80

    .line 844883
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bh()LX/1vs;

    move-result-object v81

    move-object/from16 v0, v81

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v82, v0

    move-object/from16 v0, v81

    iget v0, v0, LX/1vs;->b:I

    move/from16 v81, v0

    const v83, 0x48cea5bf

    move-object/from16 v0, v82

    move/from16 v1, v81

    move/from16 v2, v83

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v81

    move-object/from16 v0, p1

    move-object/from16 v1, v81

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v81

    .line 844884
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bi()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel;

    move-result-object v82

    move-object/from16 v0, p1

    move-object/from16 v1, v82

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v82

    .line 844885
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bj()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel;

    move-result-object v83

    move-object/from16 v0, p1

    move-object/from16 v1, v83

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v83

    .line 844886
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bk()LX/2uF;

    move-result-object v84

    move-object/from16 v0, v84

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v84

    .line 844887
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bl()Ljava/lang/String;

    move-result-object v85

    move-object/from16 v0, p1

    move-object/from16 v1, v85

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v85

    .line 844888
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bm()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v86

    move-object/from16 v0, p1

    move-object/from16 v1, v86

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v86

    .line 844889
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bn()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    move-result-object v87

    move-object/from16 v0, p1

    move-object/from16 v1, v87

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v87

    .line 844890
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->C()Ljava/lang/String;

    move-result-object v88

    move-object/from16 v0, p1

    move-object/from16 v1, v88

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v88

    .line 844891
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bo()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$AudioAttachmentFieldsModel$MusicObjectModel;

    move-result-object v89

    move-object/from16 v0, p1

    move-object/from16 v1, v89

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v89

    .line 844892
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bp()Ljava/lang/String;

    move-result-object v90

    move-object/from16 v0, p1

    move-object/from16 v1, v90

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v90

    .line 844893
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bq()Lcom/facebook/graphql/enums/GraphQLMusicType;

    move-result-object v91

    move-object/from16 v0, p1

    move-object/from16 v1, v91

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v91

    .line 844894
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->br()LX/0Px;

    move-result-object v92

    move-object/from16 v0, p1

    move-object/from16 v1, v92

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v92

    .line 844895
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->p()Ljava/lang/String;

    move-result-object v93

    move-object/from16 v0, p1

    move-object/from16 v1, v93

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v93

    .line 844896
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bs()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    move-result-object v94

    move-object/from16 v0, p1

    move-object/from16 v1, v94

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v94

    .line 844897
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bt()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;

    move-result-object v95

    move-object/from16 v0, p1

    move-object/from16 v1, v95

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v95

    .line 844898
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bu()LX/1vs;

    move-result-object v96

    move-object/from16 v0, v96

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v97, v0

    move-object/from16 v0, v96

    iget v0, v0, LX/1vs;->b:I

    move/from16 v96, v0

    const v98, 0x34c84503

    move-object/from16 v0, v97

    move/from16 v1, v96

    move/from16 v2, v98

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v96

    move-object/from16 v0, p1

    move-object/from16 v1, v96

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v96

    .line 844899
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bv()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$OwnerModel;

    move-result-object v97

    move-object/from16 v0, p1

    move-object/from16 v1, v97

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v97

    .line 844900
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bw()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ProductItemAttachmentModel$PageModel;

    move-result-object v98

    move-object/from16 v0, p1

    move-object/from16 v1, v98

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v98

    .line 844901
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bx()LX/1vs;

    move-result-object v99

    move-object/from16 v0, v99

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v100, v0

    move-object/from16 v0, v99

    iget v0, v0, LX/1vs;->b:I

    move/from16 v99, v0

    const v101, 0x152cb227

    move-object/from16 v0, v100

    move/from16 v1, v99

    move/from16 v2, v101

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v99

    move-object/from16 v0, p1

    move-object/from16 v1, v99

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v99

    .line 844902
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->by()LX/1vs;

    move-result-object v100

    move-object/from16 v0, v100

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v101, v0

    move-object/from16 v0, v100

    iget v0, v0, LX/1vs;->b:I

    move/from16 v100, v0

    const v102, -0x33df0b5b    # -4.2193556E7f

    move-object/from16 v0, v101

    move/from16 v1, v100

    move/from16 v2, v102

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v100

    move-object/from16 v0, p1

    move-object/from16 v1, v100

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v100

    .line 844903
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bz()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v101

    move-object/from16 v0, p1

    move-object/from16 v1, v101

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v101

    .line 844904
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bA()LX/0Px;

    move-result-object v102

    move-object/from16 v0, p1

    move-object/from16 v1, v102

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v102

    .line 844905
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bB()LX/0Px;

    move-result-object v103

    move-object/from16 v0, p1

    move-object/from16 v1, v103

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v103

    .line 844906
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bC()LX/0Px;

    move-result-object v104

    move-object/from16 v0, p1

    move-object/from16 v1, v104

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v104

    .line 844907
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bD()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v105

    move-object/from16 v0, p1

    move-object/from16 v1, v105

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v105

    .line 844908
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bE()Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;

    move-result-object v106

    move-object/from16 v0, p1

    move-object/from16 v1, v106

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v106

    .line 844909
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bF()LX/2uF;

    move-result-object v107

    move-object/from16 v0, v107

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v107

    .line 844910
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bG()Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    move-result-object v108

    move-object/from16 v0, p1

    move-object/from16 v1, v108

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v108

    .line 844911
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bH()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v109

    move-object/from16 v0, p1

    move-object/from16 v1, v109

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v109

    .line 844912
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bI()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphActionAttachmentTargetModel$PrimaryObjectNodeModel;

    move-result-object v110

    move-object/from16 v0, p1

    move-object/from16 v1, v110

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v110

    .line 844913
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bJ()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v111

    move-object/from16 v0, p1

    move-object/from16 v1, v111

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v111

    .line 844914
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bK()LX/1vs;

    move-result-object v112

    move-object/from16 v0, v112

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v113, v0

    move-object/from16 v0, v112

    iget v0, v0, LX/1vs;->b:I

    move/from16 v112, v0

    const v114, 0x1384d012

    move-object/from16 v0, v113

    move/from16 v1, v112

    move/from16 v2, v114

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v112

    move-object/from16 v0, p1

    move-object/from16 v1, v112

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v112

    .line 844915
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bL()LX/1vs;

    move-result-object v113

    move-object/from16 v0, v113

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v114, v0

    move-object/from16 v0, v113

    iget v0, v0, LX/1vs;->b:I

    move/from16 v113, v0

    const v115, -0x5cb8b36c

    move-object/from16 v0, v114

    move/from16 v1, v113

    move/from16 v2, v115

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v113

    move-object/from16 v0, p1

    move-object/from16 v1, v113

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v113

    .line 844916
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bM()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v114

    move-object/from16 v0, p1

    move-object/from16 v1, v114

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v114

    .line 844917
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bN()LX/2uF;

    move-result-object v115

    move-object/from16 v0, v115

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v115

    .line 844918
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bO()Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    move-result-object v116

    move-object/from16 v0, p1

    move-object/from16 v1, v116

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v116

    .line 844919
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bP()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v117

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v117

    .line 844920
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bQ()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolModel;

    move-result-object v118

    move-object/from16 v0, p1

    move-object/from16 v1, v118

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v118

    .line 844921
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bR()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolClassModel;

    move-result-object v119

    move-object/from16 v0, p1

    move-object/from16 v1, v119

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v119

    .line 844922
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bS()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel;

    move-result-object v120

    move-object/from16 v0, p1

    move-object/from16 v1, v120

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v120

    .line 844923
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bT()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;

    move-result-object v121

    move-object/from16 v0, p1

    move-object/from16 v1, v121

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v121

    .line 844924
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bU()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v122

    move-object/from16 v0, p1

    move-object/from16 v1, v122

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v122

    .line 844925
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bV()Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    move-result-object v123

    move-object/from16 v0, p1

    move-object/from16 v1, v123

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v123

    .line 844926
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bW()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SportsAttachmentFieldsModel$SportsMatchDataModel;

    move-result-object v124

    move-object/from16 v0, p1

    move-object/from16 v1, v124

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v124

    .line 844927
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bX()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    move-result-object v125

    move-object/from16 v0, p1

    move-object/from16 v1, v125

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v125

    .line 844928
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bY()Ljava/lang/String;

    move-result-object v126

    move-object/from16 v0, p1

    move-object/from16 v1, v126

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v126

    .line 844929
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bZ()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v127

    move-object/from16 v0, p1

    move-object/from16 v1, v127

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v127

    .line 844930
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ca()Ljava/lang/String;

    move-result-object v128

    move-object/from16 v0, p1

    move-object/from16 v1, v128

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v128

    .line 844931
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cb()Ljava/lang/String;

    move-result-object v129

    move-object/from16 v0, p1

    move-object/from16 v1, v129

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v129

    .line 844932
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cc()LX/0Px;

    move-result-object v130

    move-object/from16 v0, p1

    move-object/from16 v1, v130

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v130

    .line 844933
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cd()LX/0Px;

    move-result-object v131

    move-object/from16 v0, p1

    move-object/from16 v1, v131

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v131

    .line 844934
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->s()Ljava/lang/String;

    move-result-object v132

    move-object/from16 v0, p1

    move-object/from16 v1, v132

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v132

    .line 844935
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ce()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$TitleModel;

    move-result-object v133

    move-object/from16 v0, p1

    move-object/from16 v1, v133

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v133

    .line 844936
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->t()Ljava/lang/String;

    move-result-object v134

    move-object/from16 v0, p1

    move-object/from16 v1, v134

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v134

    .line 844937
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cf()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$UserDonorsModel;

    move-result-object v135

    move-object/from16 v0, p1

    move-object/from16 v1, v135

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v135

    .line 844938
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->u()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v136

    move-object/from16 v0, p1

    move-object/from16 v1, v136

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v136

    .line 844939
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cg()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v137

    move-object/from16 v0, p1

    move-object/from16 v1, v137

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v137

    .line 844940
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ch()LX/1vs;

    move-result-object v138

    move-object/from16 v0, v138

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v139, v0

    move-object/from16 v0, v138

    iget v0, v0, LX/1vs;->b:I

    move/from16 v138, v0

    const v140, 0x471b9538

    move-object/from16 v0, v139

    move/from16 v1, v138

    move/from16 v2, v140

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v138

    move-object/from16 v0, p1

    move-object/from16 v1, v138

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v138

    .line 844941
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->w()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v139

    move-object/from16 v0, p1

    move-object/from16 v1, v139

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v139

    .line 844942
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ci()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$WorkProjectModel;

    move-result-object v140

    move-object/from16 v0, p1

    move-object/from16 v1, v140

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v140

    .line 844943
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cj()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    move-result-object v141

    move-object/from16 v0, p1

    move-object/from16 v1, v141

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v141

    .line 844944
    const/16 v142, 0xa8

    move-object/from16 v0, p1

    move/from16 v1, v142

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 844945
    const/16 v142, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v142

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 844946
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 844947
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 844948
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 844949
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 844950
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 844951
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 844952
    const/4 v5, 0x7

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->l:J

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 844953
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 844954
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 844955
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 844956
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 844957
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 844958
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844959
    const/16 v4, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844960
    const/16 v4, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844961
    const/16 v4, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844962
    const/16 v4, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844963
    const/16 v4, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844964
    const/16 v4, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844965
    const/16 v4, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844966
    const/16 v4, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844967
    const/16 v5, 0x16

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->A:D

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 844968
    const/16 v4, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844969
    const/16 v4, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844970
    const/16 v4, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844971
    const/16 v4, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844972
    const/16 v4, 0x1b

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->F:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 844973
    const/16 v4, 0x1c

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->G:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 844974
    const/16 v4, 0x1d

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->H:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 844975
    const/16 v4, 0x1e

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->I:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 844976
    const/16 v4, 0x1f

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->J:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 844977
    const/16 v4, 0x20

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->K:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 844978
    const/16 v4, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844979
    const/16 v4, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844980
    const/16 v4, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844981
    const/16 v4, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844982
    const/16 v4, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844983
    const/16 v4, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844984
    const/16 v4, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844985
    const/16 v4, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844986
    const/16 v4, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844987
    const/16 v4, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844988
    const/16 v4, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844989
    const/16 v4, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844990
    const/16 v4, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844991
    const/16 v4, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844992
    const/16 v4, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844993
    const/16 v5, 0x30

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aa:J

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 844994
    const/16 v4, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844995
    const/16 v4, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844996
    const/16 v4, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844997
    const/16 v4, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844998
    const/16 v4, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 844999
    const/16 v4, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845000
    const/16 v4, 0x37

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ah:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 845001
    const/16 v4, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845002
    const/16 v4, 0x39

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aj:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 845003
    const/16 v4, 0x3a

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845004
    const/16 v5, 0x3b

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->al:J

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 845005
    const/16 v4, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845006
    const/16 v4, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845007
    const/16 v4, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845008
    const/16 v4, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845009
    const/16 v5, 0x40

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aq:J

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 845010
    const/16 v4, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845011
    const/16 v4, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845012
    const/16 v4, 0x43

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845013
    const/16 v4, 0x44

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845014
    const/16 v4, 0x45

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845015
    const/16 v4, 0x46

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845016
    const/16 v4, 0x47

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845017
    const/16 v4, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845018
    const/16 v4, 0x49

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845019
    const/16 v4, 0x4a

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845020
    const/16 v4, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845021
    const/16 v4, 0x4c

    move-object/from16 v0, p1

    move/from16 v1, v67

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845022
    const/16 v4, 0x4d

    move-object/from16 v0, p1

    move/from16 v1, v68

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845023
    const/16 v4, 0x4e

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aE:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845024
    const/16 v4, 0x4f

    move-object/from16 v0, p1

    move/from16 v1, v69

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845025
    const/16 v4, 0x50

    move-object/from16 v0, p1

    move/from16 v1, v70

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845026
    const/16 v4, 0x51

    move-object/from16 v0, p1

    move/from16 v1, v71

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845027
    const/16 v4, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v72

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845028
    const/16 v4, 0x53

    move-object/from16 v0, p1

    move/from16 v1, v73

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845029
    const/16 v4, 0x54

    move-object/from16 v0, p1

    move/from16 v1, v74

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845030
    const/16 v4, 0x55

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aL:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845031
    const/16 v4, 0x56

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aM:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845032
    const/16 v4, 0x57

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aN:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845033
    const/16 v4, 0x58

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aO:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845034
    const/16 v4, 0x59

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aP:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845035
    const/16 v4, 0x5a

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aQ:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845036
    const/16 v4, 0x5b

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aR:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845037
    const/16 v4, 0x5c

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aS:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845038
    const/16 v4, 0x5d

    move-object/from16 v0, p1

    move/from16 v1, v75

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845039
    const/16 v4, 0x5e

    move-object/from16 v0, p1

    move/from16 v1, v76

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845040
    const/16 v4, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v77

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845041
    const/16 v4, 0x60

    move-object/from16 v0, p1

    move/from16 v1, v78

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845042
    const/16 v4, 0x61

    move-object/from16 v0, p1

    move/from16 v1, v79

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845043
    const/16 v4, 0x62

    move-object/from16 v0, p1

    move/from16 v1, v80

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845044
    const/16 v4, 0x63

    move-object/from16 v0, p1

    move/from16 v1, v81

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845045
    const/16 v4, 0x64

    move-object/from16 v0, p1

    move/from16 v1, v82

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845046
    const/16 v4, 0x65

    move-object/from16 v0, p1

    move/from16 v1, v83

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845047
    const/16 v4, 0x66

    move-object/from16 v0, p1

    move/from16 v1, v84

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845048
    const/16 v4, 0x67

    move-object/from16 v0, p1

    move/from16 v1, v85

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845049
    const/16 v4, 0x68

    move-object/from16 v0, p1

    move/from16 v1, v86

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845050
    const/16 v4, 0x69

    move-object/from16 v0, p1

    move/from16 v1, v87

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845051
    const/16 v4, 0x6a

    move-object/from16 v0, p1

    move/from16 v1, v88

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845052
    const/16 v4, 0x6b

    move-object/from16 v0, p1

    move/from16 v1, v89

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845053
    const/16 v4, 0x6c

    move-object/from16 v0, p1

    move/from16 v1, v90

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845054
    const/16 v4, 0x6d

    move-object/from16 v0, p1

    move/from16 v1, v91

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845055
    const/16 v4, 0x6e

    move-object/from16 v0, p1

    move/from16 v1, v92

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845056
    const/16 v4, 0x6f

    move-object/from16 v0, p1

    move/from16 v1, v93

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845057
    const/16 v4, 0x70

    move-object/from16 v0, p1

    move/from16 v1, v94

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845058
    const/16 v4, 0x71

    move-object/from16 v0, p1

    move/from16 v1, v95

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845059
    const/16 v4, 0x72

    move-object/from16 v0, p1

    move/from16 v1, v96

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845060
    const/16 v4, 0x73

    move-object/from16 v0, p1

    move/from16 v1, v97

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845061
    const/16 v4, 0x74

    move-object/from16 v0, p1

    move/from16 v1, v98

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845062
    const/16 v4, 0x75

    move-object/from16 v0, p1

    move/from16 v1, v99

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845063
    const/16 v4, 0x76

    move-object/from16 v0, p1

    move/from16 v1, v100

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845064
    const/16 v4, 0x77

    move-object/from16 v0, p1

    move/from16 v1, v101

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845065
    const/16 v4, 0x78

    move-object/from16 v0, p1

    move/from16 v1, v102

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845066
    const/16 v4, 0x79

    move-object/from16 v0, p1

    move/from16 v1, v103

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845067
    const/16 v4, 0x7a

    move-object/from16 v0, p1

    move/from16 v1, v104

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845068
    const/16 v5, 0x7b

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bx:D

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 845069
    const/16 v4, 0x7c

    move-object/from16 v0, p1

    move/from16 v1, v105

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845070
    const/16 v4, 0x7d

    move-object/from16 v0, p1

    move/from16 v1, v106

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845071
    const/16 v4, 0x7e

    move-object/from16 v0, p1

    move/from16 v1, v107

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845072
    const/16 v4, 0x7f

    move-object/from16 v0, p1

    move/from16 v1, v108

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845073
    const/16 v4, 0x80

    move-object/from16 v0, p1

    move/from16 v1, v109

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845074
    const/16 v4, 0x81

    move-object/from16 v0, p1

    move/from16 v1, v110

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845075
    const/16 v4, 0x82

    move-object/from16 v0, p1

    move/from16 v1, v111

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845076
    const/16 v4, 0x83

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bF:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845077
    const/16 v4, 0x84

    move-object/from16 v0, p1

    move/from16 v1, v112

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845078
    const/16 v4, 0x85

    move-object/from16 v0, p1

    move/from16 v1, v113

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845079
    const/16 v4, 0x86

    move-object/from16 v0, p1

    move/from16 v1, v114

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845080
    const/16 v4, 0x87

    move-object/from16 v0, p1

    move/from16 v1, v115

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845081
    const/16 v4, 0x88

    move-object/from16 v0, p1

    move/from16 v1, v116

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845082
    const/16 v4, 0x89

    move-object/from16 v0, p1

    move/from16 v1, v117

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845083
    const/16 v4, 0x8a

    move-object/from16 v0, p1

    move/from16 v1, v118

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845084
    const/16 v4, 0x8b

    move-object/from16 v0, p1

    move/from16 v1, v119

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845085
    const/16 v4, 0x8c

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bO:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845086
    const/16 v4, 0x8d

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bP:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845087
    const/16 v4, 0x8e

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bQ:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845088
    const/16 v4, 0x8f

    move-object/from16 v0, p1

    move/from16 v1, v120

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845089
    const/16 v4, 0x90

    move-object/from16 v0, p1

    move/from16 v1, v121

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845090
    const/16 v4, 0x91

    move-object/from16 v0, p1

    move/from16 v1, v122

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845091
    const/16 v4, 0x92

    move-object/from16 v0, p1

    move/from16 v1, v123

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845092
    const/16 v4, 0x93

    move-object/from16 v0, p1

    move/from16 v1, v124

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845093
    const/16 v4, 0x94

    move-object/from16 v0, p1

    move/from16 v1, v125

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845094
    const/16 v5, 0x95

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bX:J

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 845095
    const/16 v4, 0x96

    move-object/from16 v0, p1

    move/from16 v1, v126

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845096
    const/16 v4, 0x97

    move-object/from16 v0, p1

    move/from16 v1, v127

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845097
    const/16 v4, 0x98

    move-object/from16 v0, p1

    move/from16 v1, v128

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845098
    const/16 v4, 0x99

    move-object/from16 v0, p1

    move/from16 v1, v129

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845099
    const/16 v4, 0x9a

    move-object/from16 v0, p1

    move/from16 v1, v130

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845100
    const/16 v4, 0x9b

    move-object/from16 v0, p1

    move/from16 v1, v131

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845101
    const/16 v4, 0x9c

    move-object/from16 v0, p1

    move/from16 v1, v132

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845102
    const/16 v4, 0x9d

    move-object/from16 v0, p1

    move/from16 v1, v133

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845103
    const/16 v4, 0x9e

    move-object/from16 v0, p1

    move/from16 v1, v134

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845104
    const/16 v4, 0x9f

    move-object/from16 v0, p1

    move/from16 v1, v135

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845105
    const/16 v4, 0xa0

    move-object/from16 v0, p1

    move/from16 v1, v136

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845106
    const/16 v4, 0xa1

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cj:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845107
    const/16 v4, 0xa2

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ck:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 845108
    const/16 v4, 0xa3

    move-object/from16 v0, p1

    move/from16 v1, v137

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845109
    const/16 v4, 0xa4

    move-object/from16 v0, p1

    move/from16 v1, v138

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845110
    const/16 v4, 0xa5

    move-object/from16 v0, p1

    move/from16 v1, v139

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845111
    const/16 v4, 0xa6

    move-object/from16 v0, p1

    move/from16 v1, v140

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845112
    const/16 v4, 0xa7

    move-object/from16 v0, p1

    move/from16 v1, v141

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 845113
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 845114
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    return v4
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 845115
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 845116
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->L()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 845117
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->L()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845118
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->L()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 845119
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845120
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845121
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->M()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 845122
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->M()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x3b404242

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 845123
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->M()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 845124
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845125
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->g:I

    move-object v1, v0

    .line 845126
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->O()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 845127
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->O()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 845128
    if-eqz v2, :cond_2

    .line 845129
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845130
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->i:LX/3Sb;

    move-object v1, v0

    .line 845131
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->P()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 845132
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->P()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 845133
    if-eqz v2, :cond_3

    .line 845134
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845135
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->j:LX/3Sb;

    move-object v1, v0

    .line 845136
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Q()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 845137
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Q()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$AddressModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$AddressModel;

    .line 845138
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Q()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$AddressModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 845139
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845140
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->k:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$TravelAttachmentFieldsModel$AddressModel;

    .line 845141
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->R()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 845142
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->R()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 845143
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->R()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 845144
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845145
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->m:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    .line 845146
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->S()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 845147
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->S()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    .line 845148
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->S()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 845149
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845150
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->n:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    .line 845151
    :cond_6
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->V()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 845152
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->V()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845153
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->V()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 845154
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845155
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845156
    :cond_7
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->W()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 845157
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->W()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    .line 845158
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->W()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 845159
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845160
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->r:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel;

    .line 845161
    :cond_8
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ab()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 845162
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ab()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 845163
    if-eqz v2, :cond_9

    .line 845164
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845165
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->w:Ljava/util/List;

    move-object v1, v0

    .line 845166
    :cond_9
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ad()Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 845167
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ad()Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;

    .line 845168
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ad()Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 845169
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845170
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->y:Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsNoProfileVideoModel;

    .line 845171
    :cond_a
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->af()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 845172
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->af()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 845173
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->af()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 845174
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845175
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->B:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 845176
    :cond_b
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ag()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 845177
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ag()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 845178
    if-eqz v2, :cond_c

    .line 845179
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845180
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->C:LX/3Sb;

    move-object v1, v0

    .line 845181
    :cond_c
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ah()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 845182
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ah()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel;

    .line 845183
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ah()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 845184
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845185
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->D:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserPageAttachmentFragmentModel$CampaignModel;

    .line 845186
    :cond_d
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->al()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 845187
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->al()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;

    .line 845188
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->al()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 845189
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845190
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->N:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;

    .line 845191
    :cond_e
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->am()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 845192
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->am()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    .line 845193
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->am()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 845194
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845195
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->O:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    .line 845196
    :cond_f
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ao()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$CommentParentModel;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 845197
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ao()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$CommentParentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$CommentParentModel;

    .line 845198
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ao()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$CommentParentModel;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 845199
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845200
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Q:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentStoryAttachmentTargetModel$CommentParentModel;

    .line 845201
    :cond_10
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aq()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 845202
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aq()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    .line 845203
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aq()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 845204
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845205
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->S:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    .line 845206
    :cond_11
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ar()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 845207
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ar()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 845208
    if-eqz v2, :cond_12

    .line 845209
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845210
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->T:Ljava/util/List;

    move-object v1, v0

    .line 845211
    :cond_12
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->as()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 845212
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->as()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 845213
    if-eqz v2, :cond_13

    .line 845214
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845215
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->U:Ljava/util/List;

    move-object v1, v0

    .line 845216
    :cond_13
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->av()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_14

    .line 845217
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->av()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x1e86a0d0

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 845218
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->av()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_14

    .line 845219
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845220
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Y:I

    move-object v1, v0

    .line 845221
    :cond_14
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ax()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 845222
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ax()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    .line 845223
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ax()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 845224
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845225
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ab:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MemeShareAttachmentTargetModel$CreationStoryModel;

    .line 845226
    :cond_15
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ay()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_16

    .line 845227
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ay()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x749916b7

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 845228
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ay()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_16

    .line 845229
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845230
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ac:I

    move-object v1, v0

    .line 845231
    :cond_16
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->az()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentVideoModel;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 845232
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->az()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentVideoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentVideoModel;

    .line 845233
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->az()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentVideoModel;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 845234
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845235
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ad:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CulturalMomentVideoModel;

    .line 845236
    :cond_17
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aA()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_18

    .line 845237
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aA()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x41dc040c

    invoke-static {v2, v0, v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 845238
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aA()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_18

    .line 845239
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845240
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ae:I

    move-object v1, v0

    .line 845241
    :cond_18
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aB()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_19

    .line 845242
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aB()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x481f8bd

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 845243
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aB()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_19

    .line 845244
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845245
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->af:I

    move-object v1, v0

    .line 845246
    :cond_19
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aC()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 845247
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aC()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;

    .line 845248
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aC()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 845249
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845250
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ai:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$DonorsSocialContextTextModel;

    .line 845251
    :cond_1a
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aD()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$EmployerModel;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 845252
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aD()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$EmployerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$EmployerModel;

    .line 845253
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aD()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$EmployerModel;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 845254
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845255
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ak:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$EmployerModel;

    .line 845256
    :cond_1b
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aE()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 845257
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aE()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;

    .line 845258
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aE()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 845259
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845260
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->am:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;

    .line 845261
    :cond_1c
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aF()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 845262
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aF()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    .line 845263
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aF()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 845264
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845265
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->an:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    .line 845266
    :cond_1d
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aG()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 845267
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aG()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;

    .line 845268
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aG()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 845269
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845270
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ao:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;

    .line 845271
    :cond_1e
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aH()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 845272
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aH()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;

    .line 845273
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aH()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 845274
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845275
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ap:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;

    .line 845276
    :cond_1f
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aI()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_20

    .line 845277
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aI()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0xe7b5c41

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 845278
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aI()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_20

    .line 845279
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845280
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ar:I

    move-object v1, v0

    .line 845281
    :cond_20
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aK()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 845282
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aK()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 845283
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aK()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 845284
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845285
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->at:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 845286
    :cond_21
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aL()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 845287
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aL()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;

    .line 845288
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aL()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 845289
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845290
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->au:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;

    .line 845291
    :cond_22
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aM()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 845292
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aM()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;

    .line 845293
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aM()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 845294
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845295
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->av:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;

    .line 845296
    :cond_23
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aN()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 845297
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aN()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    .line 845298
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aN()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 845299
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845300
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aw:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    .line 845301
    :cond_24
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aP()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 845302
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aP()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 845303
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aP()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 845304
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845305
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ay:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 845306
    :cond_25
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aQ()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$FundraiserProgressTextModel;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 845307
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aQ()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$FundraiserProgressTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$FundraiserProgressTextModel;

    .line 845308
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aQ()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$FundraiserProgressTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_26

    .line 845309
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845310
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->az:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$FundraiserProgressTextModel;

    .line 845311
    :cond_26
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aR()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$GlobalShareModel;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 845312
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aR()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$GlobalShareModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$GlobalShareModel;

    .line 845313
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aR()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$GlobalShareModel;

    move-result-object v2

    if-eq v2, v0, :cond_27

    .line 845314
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845315
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aA:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$GlobalShareModel;

    .line 845316
    :cond_27
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aS()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 845317
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aS()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 845318
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aS()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_28

    .line 845319
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845320
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aB:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 845321
    :cond_28
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aT()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$GreetingCardTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 845322
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aT()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$GreetingCardTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$GreetingCardTemplateModel;

    .line 845323
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aT()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$GreetingCardTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_29

    .line 845324
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845325
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aC:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$GreetingCardTemplateModel;

    .line 845326
    :cond_29
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aU()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 845327
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aU()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 845328
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aU()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2a

    .line 845329
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845330
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aD:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 845331
    :cond_2a
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aV()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 845332
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aV()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845333
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aV()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2b

    .line 845334
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845335
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aG:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845336
    :cond_2b
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aX()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 845337
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aX()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    .line 845338
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aX()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    move-result-object v2

    if-eq v2, v0, :cond_2c

    .line 845339
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845340
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aI:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$InstantArticleFieldsModel$InstantArticleModel;

    .line 845341
    :cond_2c
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aY()Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 845342
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aY()Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    .line 845343
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aY()Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2d

    .line 845344
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845345
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aJ:Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    .line 845346
    :cond_2d
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aZ()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 845347
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aZ()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;

    .line 845348
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aZ()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_2e

    .line 845349
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845350
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aK:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$InvitedFriendsInfoModel;

    .line 845351
    :cond_2e
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bb()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2f

    .line 845352
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bb()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x3d9090a6

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 845353
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bb()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2f

    .line 845354
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845355
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aT:I

    move-object v1, v0

    .line 845356
    :cond_2f
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bc()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 845357
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bc()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 845358
    if-eqz v2, :cond_30

    .line 845359
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845360
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aU:Ljava/util/List;

    move-object v1, v0

    .line 845361
    :cond_30
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bd()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 845362
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bd()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    .line 845363
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bd()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    move-result-object v2

    if-eq v2, v0, :cond_31

    .line 845364
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845365
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aV:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    .line 845366
    :cond_31
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->be()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_32

    .line 845367
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->be()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 845368
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->be()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_32

    .line 845369
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845370
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 845371
    :cond_32
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bf()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 845372
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bf()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845373
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bf()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_33

    .line 845374
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845375
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aX:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845376
    :cond_33
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bg()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_34

    .line 845377
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bg()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x180f02e8

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    .line 845378
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bg()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_34

    .line 845379
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845380
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aY:I

    move-object v1, v0

    .line 845381
    :cond_34
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bh()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_35

    .line 845382
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bh()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x48cea5bf

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    .line 845383
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bh()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_35

    .line 845384
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845385
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aZ:I

    move-object v1, v0

    .line 845386
    :cond_35
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bi()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel;

    move-result-object v0

    if-eqz v0, :cond_36

    .line 845387
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bi()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel;

    .line 845388
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bi()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel;

    move-result-object v2

    if-eq v2, v0, :cond_36

    .line 845389
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845390
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ba:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SouvenirsFieldsModel$MediaElementsModel;

    .line 845391
    :cond_36
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bj()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel;

    move-result-object v0

    if-eqz v0, :cond_37

    .line 845392
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bj()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel;

    .line 845393
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bj()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel;

    move-result-object v2

    if-eq v2, v0, :cond_37

    .line 845394
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845395
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bb:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FBMediaQuestionFragmentModel$MediaQuestionOptionOrderModel;

    .line 845396
    :cond_37
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bk()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 845397
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bk()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 845398
    if-eqz v2, :cond_38

    .line 845399
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845400
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bc:LX/3Sb;

    move-object v1, v0

    .line 845401
    :cond_38
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bm()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_39

    .line 845402
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bm()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 845403
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bm()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_39

    .line 845404
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845405
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->be:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 845406
    :cond_39
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bn()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    move-result-object v0

    if-eqz v0, :cond_3a

    .line 845407
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bn()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    .line 845408
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bn()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_3a

    .line 845409
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845410
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bf:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$MessengerContentSubscriptionTargetModel$MessengerContentSubscriptionOptionModel;

    .line 845411
    :cond_3a
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bo()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$AudioAttachmentFieldsModel$MusicObjectModel;

    move-result-object v0

    if-eqz v0, :cond_3b

    .line 845412
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bo()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$AudioAttachmentFieldsModel$MusicObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$AudioAttachmentFieldsModel$MusicObjectModel;

    .line 845413
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bo()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$AudioAttachmentFieldsModel$MusicObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_3b

    .line 845414
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845415
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bh:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$AudioAttachmentFieldsModel$MusicObjectModel;

    .line 845416
    :cond_3b
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->br()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3c

    .line 845417
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->br()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 845418
    if-eqz v2, :cond_3c

    .line 845419
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845420
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bk:Ljava/util/List;

    move-object v1, v0

    .line 845421
    :cond_3c
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bs()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    move-result-object v0

    if-eqz v0, :cond_3d

    .line 845422
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bs()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    .line 845423
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bs()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_3d

    .line 845424
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845425
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bm:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ExternalUrlAttachmentModel$OpenGraphNodeModel;

    .line 845426
    :cond_3d
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bt()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;

    move-result-object v0

    if-eqz v0, :cond_3e

    .line 845427
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bt()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;

    .line 845428
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bt()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3e

    .line 845429
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845430
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bn:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;

    .line 845431
    :cond_3e
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bu()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3f

    .line 845432
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bu()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x34c84503

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_9

    .line 845433
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bu()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3f

    .line 845434
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845435
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bo:I

    move-object v1, v0

    .line 845436
    :cond_3f
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bv()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_40

    .line 845437
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bv()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$OwnerModel;

    .line 845438
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bv()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_40

    .line 845439
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845440
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bp:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$OwnerModel;

    .line 845441
    :cond_40
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bw()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ProductItemAttachmentModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_41

    .line 845442
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bw()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ProductItemAttachmentModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ProductItemAttachmentModel$PageModel;

    .line 845443
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bw()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ProductItemAttachmentModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_41

    .line 845444
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845445
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bq:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$ProductItemAttachmentModel$PageModel;

    .line 845446
    :cond_41
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bx()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_42

    .line 845447
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bx()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x152cb227

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_a
    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_a

    .line 845448
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bx()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_42

    .line 845449
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845450
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->br:I

    move-object v1, v0

    .line 845451
    :cond_42
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->by()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_43

    .line 845452
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->by()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x33df0b5b    # -4.2193556E7f

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_b
    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_b

    .line 845453
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->by()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_43

    .line 845454
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845455
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bs:I

    move-object v1, v0

    .line 845456
    :cond_43
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bz()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_44

    .line 845457
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bz()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    .line 845458
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bz()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_44

    .line 845459
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845460
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bt:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    .line 845461
    :cond_44
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bA()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_45

    .line 845462
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bA()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 845463
    if-eqz v2, :cond_45

    .line 845464
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845465
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bu:Ljava/util/List;

    move-object v1, v0

    .line 845466
    :cond_45
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bB()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_46

    .line 845467
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bB()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 845468
    if-eqz v2, :cond_46

    .line 845469
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845470
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bv:Ljava/util/List;

    move-object v1, v0

    .line 845471
    :cond_46
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bC()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_47

    .line 845472
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bC()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 845473
    if-eqz v2, :cond_47

    .line 845474
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845475
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bw:Ljava/util/List;

    move-object v1, v0

    .line 845476
    :cond_47
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bD()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_48

    .line 845477
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bD()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 845478
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bD()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_48

    .line 845479
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845480
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->by:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 845481
    :cond_48
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bF()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_49

    .line 845482
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bF()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 845483
    if-eqz v2, :cond_49

    .line 845484
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845485
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bA:LX/3Sb;

    move-object v1, v0

    .line 845486
    :cond_49
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bH()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4a

    .line 845487
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bH()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845488
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bH()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4a

    .line 845489
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845490
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bC:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845491
    :cond_4a
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bI()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphActionAttachmentTargetModel$PrimaryObjectNodeModel;

    move-result-object v0

    if-eqz v0, :cond_4b

    .line 845492
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bI()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphActionAttachmentTargetModel$PrimaryObjectNodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphActionAttachmentTargetModel$PrimaryObjectNodeModel;

    .line 845493
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bI()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphActionAttachmentTargetModel$PrimaryObjectNodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_4b

    .line 845494
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845495
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bD:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$OpenGraphActionAttachmentTargetModel$PrimaryObjectNodeModel;

    .line 845496
    :cond_4b
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bJ()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4c

    .line 845497
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bJ()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845498
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bJ()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4c

    .line 845499
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845500
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bE:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845501
    :cond_4c
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bK()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4d

    .line 845502
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bK()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x1384d012

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_c
    monitor-exit v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_c

    .line 845503
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bK()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4d

    .line 845504
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845505
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bG:I

    move-object v1, v0

    .line 845506
    :cond_4d
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bL()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4e

    .line 845507
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bL()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x5cb8b36c

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_d
    monitor-exit v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_d

    .line 845508
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bL()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4e

    .line 845509
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845510
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bH:I

    move-object v1, v0

    .line 845511
    :cond_4e
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bM()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4f

    .line 845512
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bM()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845513
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bM()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4f

    .line 845514
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845515
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bI:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 845516
    :cond_4f
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bN()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_50

    .line 845517
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bN()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 845518
    if-eqz v2, :cond_50

    .line 845519
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845520
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bJ:LX/3Sb;

    move-object v1, v0

    .line 845521
    :cond_50
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bP()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_51

    .line 845522
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bP()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    .line 845523
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bP()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_51

    .line 845524
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845525
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bL:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    .line 845526
    :cond_51
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bQ()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolModel;

    move-result-object v0

    if-eqz v0, :cond_52

    .line 845527
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bQ()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolModel;

    .line 845528
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bQ()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolModel;

    move-result-object v2

    if-eq v2, v0, :cond_52

    .line 845529
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845530
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bM:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolModel;

    .line 845531
    :cond_52
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bR()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolClassModel;

    move-result-object v0

    if-eqz v0, :cond_53

    .line 845532
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bR()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolClassModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolClassModel;

    .line 845533
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bR()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolClassModel;

    move-result-object v2

    if-eq v2, v0, :cond_53

    .line 845534
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845535
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bN:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$SchoolClassModel;

    .line 845536
    :cond_53
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bS()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel;

    move-result-object v0

    if-eqz v0, :cond_54

    .line 845537
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bS()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel;

    .line 845538
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bS()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel;

    move-result-object v2

    if-eq v2, v0, :cond_54

    .line 845539
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845540
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bR:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$GreetingCardFieldsModel$SlidesModel;

    .line 845541
    :cond_54
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bT()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;

    move-result-object v0

    if-eqz v0, :cond_55

    .line 845542
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bT()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;

    .line 845543
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bT()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_55

    .line 845544
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845545
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bS:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;

    .line 845546
    :cond_55
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bU()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_56

    .line 845547
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bU()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 845548
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bU()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_56

    .line 845549
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845550
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bT:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 845551
    :cond_56
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bV()Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    move-result-object v0

    if-eqz v0, :cond_57

    .line 845552
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bV()Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    .line 845553
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bV()Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_57

    .line 845554
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845555
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bU:Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    .line 845556
    :cond_57
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bW()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SportsAttachmentFieldsModel$SportsMatchDataModel;

    move-result-object v0

    if-eqz v0, :cond_58

    .line 845557
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bW()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SportsAttachmentFieldsModel$SportsMatchDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SportsAttachmentFieldsModel$SportsMatchDataModel;

    .line 845558
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bW()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SportsAttachmentFieldsModel$SportsMatchDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_58

    .line 845559
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845560
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bV:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$SportsAttachmentFieldsModel$SportsMatchDataModel;

    .line 845561
    :cond_58
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bX()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_59

    .line 845562
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bX()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    .line 845563
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bX()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_59

    .line 845564
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845565
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    .line 845566
    :cond_59
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cc()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5a

    .line 845567
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cc()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 845568
    if-eqz v2, :cond_5a

    .line 845569
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845570
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cc:Ljava/util/List;

    move-object v1, v0

    .line 845571
    :cond_5a
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cd()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5b

    .line 845572
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cd()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 845573
    if-eqz v2, :cond_5b

    .line 845574
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845575
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cd:Ljava/util/List;

    move-object v1, v0

    .line 845576
    :cond_5b
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ce()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_5c

    .line 845577
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ce()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$TitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$TitleModel;

    .line 845578
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ce()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$TitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_5c

    .line 845579
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845580
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cf:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$TitleModel;

    .line 845581
    :cond_5c
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cf()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$UserDonorsModel;

    move-result-object v0

    if-eqz v0, :cond_5d

    .line 845582
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cf()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$UserDonorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$UserDonorsModel;

    .line 845583
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cf()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$UserDonorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5d

    .line 845584
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845585
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ch:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$UserDonorsModel;

    .line 845586
    :cond_5d
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ch()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5e

    .line 845587
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ch()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x471b9538

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_e
    monitor-exit v4
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_e

    .line 845588
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ch()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5e

    .line 845589
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845590
    iput v3, v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cm:I

    move-object v1, v0

    .line 845591
    :cond_5e
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ci()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$WorkProjectModel;

    move-result-object v0

    if-eqz v0, :cond_5f

    .line 845592
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ci()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$WorkProjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$WorkProjectModel;

    .line 845593
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ci()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$WorkProjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_5f

    .line 845594
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845595
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->co:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedStoryAttachmentTargetExperienceFieldsModel$WorkProjectModel;

    .line 845596
    :cond_5f
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cj()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    move-result-object v0

    if-eqz v0, :cond_60

    .line 845597
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cj()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    .line 845598
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cj()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    move-result-object v2

    if-eq v2, v0, :cond_60

    .line 845599
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    .line 845600
    iput-object v0, v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cp:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$WorldViewBoundingBoxModel;

    .line 845601
    :cond_60
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 845602
    if-nez v1, :cond_61

    :goto_0
    return-object p0

    .line 845603
    :catchall_0
    move-exception v0

    :try_start_f
    monitor-exit v4
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    throw v0

    .line 845604
    :catchall_1
    move-exception v0

    :try_start_10
    monitor-exit v4
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    throw v0

    .line 845605
    :catchall_2
    move-exception v0

    :try_start_11
    monitor-exit v4
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    throw v0

    .line 845606
    :catchall_3
    move-exception v0

    :try_start_12
    monitor-exit v4
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    throw v0

    .line 845607
    :catchall_4
    move-exception v0

    :try_start_13
    monitor-exit v4
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_4

    throw v0

    .line 845608
    :catchall_5
    move-exception v0

    :try_start_14
    monitor-exit v4
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_5

    throw v0

    .line 845609
    :catchall_6
    move-exception v0

    :try_start_15
    monitor-exit v4
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_6

    throw v0

    .line 845610
    :catchall_7
    move-exception v0

    :try_start_16
    monitor-exit v4
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_7

    throw v0

    .line 845611
    :catchall_8
    move-exception v0

    :try_start_17
    monitor-exit v4
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_8

    throw v0

    .line 845612
    :catchall_9
    move-exception v0

    :try_start_18
    monitor-exit v4
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_9

    throw v0

    .line 845613
    :catchall_a
    move-exception v0

    :try_start_19
    monitor-exit v4
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_a

    throw v0

    .line 845614
    :catchall_b
    move-exception v0

    :try_start_1a
    monitor-exit v4
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_b

    throw v0

    .line 845615
    :catchall_c
    move-exception v0

    :try_start_1b
    monitor-exit v4
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_c

    throw v0

    .line 845616
    :catchall_d
    move-exception v0

    :try_start_1c
    monitor-exit v4
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_d

    throw v0

    .line 845617
    :catchall_e
    move-exception v0

    :try_start_1d
    monitor-exit v4
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_e

    throw v0

    :cond_61
    move-object p0, v1

    .line 845618
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 845619
    new-instance v0, LX/58C;

    invoke-direct {v0, p1}, LX/58C;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845620
    invoke-virtual {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v2, 0x0

    .line 845621
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 845622
    const/4 v0, 0x2

    const v1, -0x3b404242

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->g:I

    .line 845623
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->l:J

    .line 845624
    const/16 v0, 0x16

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->A:D

    .line 845625
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->F:Z

    .line 845626
    const/16 v0, 0x1c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->G:Z

    .line 845627
    const/16 v0, 0x1d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->H:Z

    .line 845628
    const/16 v0, 0x1e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->I:Z

    .line 845629
    const/16 v0, 0x1f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->J:Z

    .line 845630
    const/16 v0, 0x20

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->K:Z

    .line 845631
    const/16 v0, 0x2e

    const v1, -0x1e86a0d0

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->Y:I

    .line 845632
    const/16 v0, 0x30

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aa:J

    .line 845633
    const/16 v0, 0x32

    const v1, 0x749916b7

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ac:I

    .line 845634
    const/16 v0, 0x34

    const v1, 0x41dc040c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ae:I

    .line 845635
    const/16 v0, 0x35

    const v1, -0x481f8bd

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->af:I

    .line 845636
    const/16 v0, 0x37

    invoke-virtual {p1, p2, v0, v6}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ah:I

    .line 845637
    const/16 v0, 0x39

    invoke-virtual {p1, p2, v0, v6}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aj:I

    .line 845638
    const/16 v0, 0x3b

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->al:J

    .line 845639
    const/16 v0, 0x40

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aq:J

    .line 845640
    const/16 v0, 0x41

    const v1, 0xe7b5c41

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ar:I

    .line 845641
    const/16 v0, 0x4e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aE:Z

    .line 845642
    const/16 v0, 0x55

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aL:Z

    .line 845643
    const/16 v0, 0x56

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aM:Z

    .line 845644
    const/16 v0, 0x57

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aN:Z

    .line 845645
    const/16 v0, 0x58

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aO:Z

    .line 845646
    const/16 v0, 0x59

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aP:Z

    .line 845647
    const/16 v0, 0x5a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aQ:Z

    .line 845648
    const/16 v0, 0x5b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aR:Z

    .line 845649
    const/16 v0, 0x5c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aS:Z

    .line 845650
    const/16 v0, 0x5d

    const v1, 0x3d9090a6

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aT:I

    .line 845651
    const/16 v0, 0x62

    const v1, 0x180f02e8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aY:I

    .line 845652
    const/16 v0, 0x63

    const v1, 0x48cea5bf

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aZ:I

    .line 845653
    const/16 v0, 0x72

    const v1, 0x34c84503

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bo:I

    .line 845654
    const/16 v0, 0x75

    const v1, 0x152cb227

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->br:I

    .line 845655
    const/16 v0, 0x76

    const v1, -0x33df0b5b    # -4.2193556E7f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bs:I

    .line 845656
    const/16 v0, 0x7b

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bx:D

    .line 845657
    const/16 v0, 0x83

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bF:Z

    .line 845658
    const/16 v0, 0x84

    const v1, 0x1384d012

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bG:I

    .line 845659
    const/16 v0, 0x85

    const v1, -0x5cb8b36c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bH:I

    .line 845660
    const/16 v0, 0x8c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bO:Z

    .line 845661
    const/16 v0, 0x8d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bP:Z

    .line 845662
    const/16 v0, 0x8e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bQ:Z

    .line 845663
    const/16 v0, 0x95

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bX:J

    .line 845664
    const/16 v0, 0xa1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cj:Z

    .line 845665
    const/16 v0, 0xa2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ck:Z

    .line 845666
    const/16 v0, 0xa4

    const v1, 0x471b9538

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cm:I

    .line 845667
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 845668
    const-string v0, "distinct_recommenders_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 845669
    invoke-virtual {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->E()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 845670
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 845671
    const/16 v0, 0x37

    iput v0, p2, LX/18L;->c:I

    .line 845672
    :goto_0
    return-void

    .line 845673
    :cond_0
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 845674
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aF()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    move-result-object v0

    .line 845675
    if-eqz v0, :cond_7

    .line 845676
    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 845677
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 845678
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 845679
    :cond_1
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 845680
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aO()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 845681
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 845682
    const/16 v0, 0x47

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 845683
    :cond_2
    const-string v0, "is_sold"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 845684
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ba()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 845685
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 845686
    const/16 v0, 0x5c

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 845687
    :cond_3
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 845688
    invoke-virtual {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->u()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 845689
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 845690
    const/16 v0, 0xa0

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 845691
    :cond_4
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 845692
    invoke-virtual {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->v()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 845693
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 845694
    const/16 v0, 0xa1

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 845695
    :cond_5
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 845696
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cg()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 845697
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 845698
    const/16 v0, 0xa3

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 845699
    :cond_6
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 845700
    invoke-virtual {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->w()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 845701
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 845702
    const/16 v0, 0xa5

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 845703
    :cond_7
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 845704
    const-string v0, "confirmed_location"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 845705
    check-cast p2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;)V

    .line 845706
    :cond_0
    :goto_0
    return-void

    .line 845707
    :cond_1
    const-string v0, "confirmed_places_for_attachment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 845708
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->a(Ljava/util/List;)V

    goto :goto_0

    .line 845709
    :cond_2
    const-string v0, "confirmed_profiles"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 845710
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->b(Ljava/util/List;)V

    goto :goto_0

    .line 845711
    :cond_3
    const-string v0, "lightweight_recs"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 845712
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->c(Ljava/util/List;)V

    goto :goto_0

    .line 845713
    :cond_4
    const-string v0, "list_items"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 845714
    check-cast p2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel$ListItemsModel;)V

    goto :goto_0

    .line 845715
    :cond_5
    const-string v0, "options"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 845716
    check-cast p2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$QuestionTargetModel$OptionsModel;)V

    goto :goto_0

    .line 845717
    :cond_6
    const-string v0, "pending_location"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 845718
    check-cast p2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->b(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;)V

    goto :goto_0

    .line 845719
    :cond_7
    const-string v0, "pending_place_slots"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 845720
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->d(Ljava/util/List;)V

    goto :goto_0

    .line 845721
    :cond_8
    const-string v0, "pending_places_for_attachment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 845722
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->e(Ljava/util/List;)V

    goto :goto_0

    .line 845723
    :cond_9
    const-string v0, "pending_profiles"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 845724
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->f(Ljava/util/List;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 845725
    const-string v0, "distinct_recommenders_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 845726
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->a(I)V

    .line 845727
    :cond_0
    :goto_0
    return-void

    .line 845728
    :cond_1
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 845729
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aF()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    move-result-object v0

    .line 845730
    if-eqz v0, :cond_0

    .line 845731
    if-eqz p3, :cond_2

    .line 845732
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    .line 845733
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;->a(I)V

    .line 845734
    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->an:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    goto :goto_0

    .line 845735
    :cond_2
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;->a(I)V

    goto :goto_0

    .line 845736
    :cond_3
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 845737
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_0

    .line 845738
    :cond_4
    const-string v0, "is_sold"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 845739
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->a(Z)V

    goto :goto_0

    .line 845740
    :cond_5
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 845741
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto :goto_0

    .line 845742
    :cond_6
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 845743
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->b(Z)V

    goto :goto_0

    .line 845744
    :cond_7
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 845745
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    goto :goto_0

    .line 845746
    :cond_8
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 845747
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto/16 :goto_0
.end method

.method public final synthetic aG_()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845748
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aG()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$NewsFeedDefaultsEventPlaceFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aH_()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 845749
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aF()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventMembersModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 844750
    new-instance v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;-><init>()V

    .line 844751
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 844752
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 844732
    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844733
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->H:Z

    return v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844734
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->V:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->V:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 844735
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->V:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 844736
    const/4 v0, 0x7

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844737
    iget-wide v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->al:J

    return-wide v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 844738
    const v0, 0x2854648d

    return v0
.end method

.method public final synthetic e()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844739
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aE()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventCoverPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 844740
    const v0, 0x252222

    return v0
.end method

.method public final synthetic j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844741
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aH()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentModel$EventWatchersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844742
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aL()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventMembersFirst3Model;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic l()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844743
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aM()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$EventAttachmentSocialContextModel$FriendEventWatchersFirst3Model;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844744
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aF:Ljava/lang/String;

    const/16 v1, 0x4f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aF:Ljava/lang/String;

    .line 844745
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aF:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 844746
    const/16 v0, 0xa

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844747
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aL:Z

    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 844748
    const/16 v0, 0xa

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844749
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->aM:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844730
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bl:Ljava/lang/String;

    const/16 v1, 0x6f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bl:Ljava/lang/String;

    .line 844731
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bl:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic q()LX/586;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844753
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bT()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$SocialContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final r()J
    .locals 2

    .prologue
    .line 844754
    const/16 v0, 0x12

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844755
    iget-wide v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->bX:J

    return-wide v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844756
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ce:Ljava/lang/String;

    const/16 v1, 0x9c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ce:Ljava/lang/String;

    .line 844757
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ce:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844758
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cg:Ljava/lang/String;

    const/16 v1, 0x9e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cg:Ljava/lang/String;

    .line 844759
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cg:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844760
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ci:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const/16 v1, 0xa0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ci:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 844761
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ci:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method

.method public final v()Z
    .locals 2

    .prologue
    .line 844762
    const/16 v0, 0x14

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 844763
    iget-boolean v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cj:Z

    return v0
.end method

.method public final w()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844764
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cn:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0xa5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cn:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 844765
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->cn:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method

.method public final synthetic x()LX/176;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844766
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->R()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic y()LX/584;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844767
    invoke-direct {p0}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->al()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel$CharityInterfaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 844768
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ag:Ljava/lang/String;

    const/16 v1, 0x36

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ag:Ljava/lang/String;

    .line 844769
    iget-object v0, p0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentFieldsWithoutMediaModel$TargetModel;->ag:Ljava/lang/String;

    return-object v0
.end method
