.class public final Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentStyleInfosModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentStyleInfosModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 846157
    const-class v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentStyleInfosModel;

    new-instance v1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentStyleInfosModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentStyleInfosModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 846158
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 846156
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentStyleInfosModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 846147
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 846148
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 846149
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 846150
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 846151
    if-eqz v2, :cond_0

    .line 846152
    const-string p0, "style_infos"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 846153
    invoke-static {v1, v2, p1, p2}, LX/58k;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 846154
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 846155
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 846146
    check-cast p1, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentStyleInfosModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentStyleInfosModel$Serializer;->a(Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$StoryAttachmentStyleInfosModel;LX/0nX;LX/0my;)V

    return-void
.end method
