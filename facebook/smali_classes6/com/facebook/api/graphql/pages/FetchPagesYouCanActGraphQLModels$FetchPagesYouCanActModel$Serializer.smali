.class public final Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1060388
    const-class v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;

    new-instance v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1060389
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1060354
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1060355
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1060356
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1060357
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1060358
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1060359
    if-eqz v2, :cond_0

    .line 1060360
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060361
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1060362
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1060363
    if-eqz v2, :cond_1

    .line 1060364
    const-string p0, "can_see_voice_switcher"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060365
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1060366
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1060367
    if-eqz v2, :cond_2

    .line 1060368
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060369
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1060370
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060371
    if-eqz v2, :cond_3

    .line 1060372
    const-string p0, "viewer_acts_as_page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060373
    invoke-static {v1, v2, p1}, LX/6An;->a(LX/15i;ILX/0nX;)V

    .line 1060374
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060375
    if-eqz v2, :cond_4

    .line 1060376
    const-string p0, "viewer_acts_as_person_for_inline_voice"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060377
    invoke-static {v1, v2, p1, p2}, LX/7YH;->a$redex0(LX/15i;ILX/0nX;LX/0my;)V

    .line 1060378
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060379
    if-eqz v2, :cond_5

    .line 1060380
    const-string p0, "voice_switcher_actors"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060381
    invoke-static {v1, v2, p1, p2}, LX/6Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1060382
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1060383
    if-eqz v2, :cond_6

    .line 1060384
    const-string p0, "voice_switcher_pages"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1060385
    invoke-static {v1, v2, p1, p2}, LX/6Ar;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1060386
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1060387
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1060353
    check-cast p1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel$Serializer;->a(Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;LX/0nX;LX/0my;)V

    return-void
.end method
