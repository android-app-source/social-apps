.class public final Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x174981b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1060246
    const-class v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1060305
    const-class v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1060303
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1060304
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060301
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->f:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->f:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    .line 1060302
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->f:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    return-object v0
.end method

.method private j()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060299
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->g:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->g:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    .line 1060300
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->g:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    return-object v0
.end method

.method private k()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060297
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->h:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->h:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    .line 1060298
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->h:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060295
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->i:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->i:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    .line 1060296
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->i:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1060282
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1060283
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->a()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1060284
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->j()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1060285
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->k()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1060286
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->l()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1060287
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1060288
    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->e:Z

    invoke-virtual {p1, v4, v5}, LX/186;->a(IZ)V

    .line 1060289
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1060290
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1060291
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1060292
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1060293
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1060294
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1060259
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1060260
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->a()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1060261
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->a()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    .line 1060262
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->a()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1060263
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;

    .line 1060264
    iput-object v0, v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->f:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    .line 1060265
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->j()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1060266
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->j()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    .line 1060267
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->j()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1060268
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;

    .line 1060269
    iput-object v0, v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->g:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    .line 1060270
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->k()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1060271
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->k()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    .line 1060272
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->k()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1060273
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;

    .line 1060274
    iput-object v0, v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->h:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    .line 1060275
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->l()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1060276
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->l()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    .line 1060277
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->l()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1060278
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;

    .line 1060279
    iput-object v0, v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->i:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    .line 1060280
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1060281
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1060258
    new-instance v0, LX/6Ag;

    invoke-direct {v0, p1}, LX/6Ag;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1060255
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1060256
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;->e:Z

    .line 1060257
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1060253
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1060254
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1060252
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1060249
    new-instance v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;-><init>()V

    .line 1060250
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1060251
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1060248
    const v0, -0x76593832

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1060247
    const v0, -0x78fb05b

    return v0
.end method
