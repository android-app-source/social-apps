.class public final Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1060306
    const-class v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;

    new-instance v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1060307
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1060308
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1060309
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1060310
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1060311
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_c

    .line 1060312
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1060313
    :goto_0
    move v1, v2

    .line 1060314
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1060315
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1060316
    new-instance v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;

    invoke-direct {v1}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;-><init>()V

    .line 1060317
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1060318
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1060319
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1060320
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1060321
    :cond_0
    return-object v1

    .line 1060322
    :cond_1
    const-string p0, "can_see_voice_switcher"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1060323
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v9, v1

    move v1, v3

    .line 1060324
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_a

    .line 1060325
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1060326
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1060327
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 1060328
    const-string p0, "__type__"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1060329
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 1060330
    :cond_4
    const-string p0, "id"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1060331
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1060332
    :cond_5
    const-string p0, "viewer_acts_as_page"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1060333
    invoke-static {p1, v0}, LX/6An;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1060334
    :cond_6
    const-string p0, "viewer_acts_as_person_for_inline_voice"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1060335
    invoke-static {p1, v0}, LX/6Ao;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1060336
    :cond_7
    const-string p0, "voice_switcher_actors"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1060337
    invoke-static {p1, v0}, LX/6Ap;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1060338
    :cond_8
    const-string p0, "voice_switcher_pages"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1060339
    invoke-static {p1, v0}, LX/6Ar;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1060340
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1060341
    :cond_a
    const/4 v11, 0x7

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1060342
    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1060343
    if-eqz v1, :cond_b

    .line 1060344
    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 1060345
    :cond_b
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1060346
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1060347
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1060348
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1060349
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1060350
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    goto/16 :goto_1
.end method
