.class public final Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x104a4aa6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1059947
    const-class v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1059948
    const-class v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1059949
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1059950
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1059951
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;->e:Ljava/lang/String;

    .line 1059952
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1059953
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1059954
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1059955
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1059956
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1059957
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1059958
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1059959
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1059960
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1059961
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1059962
    new-instance v0, LX/6Ah;

    invoke-direct {v0, p1}, LX/6Ah;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1059963
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1059964
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1059965
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1059966
    new-instance v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;-><init>()V

    .line 1059967
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1059968
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1059969
    const v0, 0x40f6c929

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1059970
    const v0, 0x25d6af

    return v0
.end method
