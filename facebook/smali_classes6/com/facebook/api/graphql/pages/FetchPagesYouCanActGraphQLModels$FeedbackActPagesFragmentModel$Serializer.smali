.class public final Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1059896
    const-class v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;

    new-instance v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1059897
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1059898
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1059899
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1059900
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1059901
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1059902
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1059903
    if-eqz v2, :cond_0

    .line 1059904
    const-string p0, "can_see_voice_switcher"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1059905
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1059906
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1059907
    if-eqz v2, :cond_1

    .line 1059908
    const-string p0, "viewer_acts_as_page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1059909
    invoke-static {v1, v2, p1}, LX/6An;->a(LX/15i;ILX/0nX;)V

    .line 1059910
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1059911
    if-eqz v2, :cond_2

    .line 1059912
    const-string p0, "viewer_acts_as_person_for_inline_voice"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1059913
    invoke-static {v1, v2, p1, p2}, LX/7YH;->a$redex0(LX/15i;ILX/0nX;LX/0my;)V

    .line 1059914
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1059915
    if-eqz v2, :cond_3

    .line 1059916
    const-string p0, "voice_switcher_actors"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1059917
    invoke-static {v1, v2, p1, p2}, LX/6Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1059918
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1059919
    if-eqz v2, :cond_4

    .line 1059920
    const-string p0, "voice_switcher_pages"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1059921
    invoke-static {v1, v2, p1, p2}, LX/6Ar;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1059922
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1059923
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1059924
    check-cast p1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$Serializer;->a(Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
