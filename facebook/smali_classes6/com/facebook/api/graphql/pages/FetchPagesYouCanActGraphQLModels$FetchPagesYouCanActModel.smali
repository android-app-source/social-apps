.class public final Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x140aef87
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1060459
    const-class v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1060390
    const-class v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1060457
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1060458
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060454
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1060455
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1060456
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060452
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->g:Ljava/lang/String;

    .line 1060453
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060450
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->h:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->h:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    .line 1060451
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->h:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    return-object v0
.end method

.method private m()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060448
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->i:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->i:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    .line 1060449
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->i:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    return-object v0
.end method

.method private n()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060446
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->j:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->j:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    .line 1060447
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->j:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    return-object v0
.end method

.method private o()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060444
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->k:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->k:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    .line 1060445
    iget-object v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->k:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1060427
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1060428
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1060429
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1060430
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->l()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1060431
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->m()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1060432
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->n()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1060433
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->o()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1060434
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1060435
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1060436
    const/4 v0, 0x1

    iget-boolean v6, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->f:Z

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 1060437
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1060438
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1060439
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1060440
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1060441
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1060442
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1060443
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1060404
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1060405
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->l()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1060406
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->l()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    .line 1060407
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->l()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1060408
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;

    .line 1060409
    iput-object v0, v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->h:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPageModel;

    .line 1060410
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->m()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1060411
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->m()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    .line 1060412
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->m()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1060413
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;

    .line 1060414
    iput-object v0, v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->i:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$ViewerActsAsPersonForInlineVoiceModel;

    .line 1060415
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->n()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1060416
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->n()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    .line 1060417
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->n()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1060418
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;

    .line 1060419
    iput-object v0, v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->j:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherActorsModel;

    .line 1060420
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->o()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1060421
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->o()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    .line 1060422
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->o()Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1060423
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;

    .line 1060424
    iput-object v0, v1, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->k:Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FeedbackActPagesFragmentModel$VoiceSwitcherPagesModel;

    .line 1060425
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1060426
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1060403
    new-instance v0, LX/6Al;

    invoke-direct {v0, p1}, LX/6Al;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1060402
    invoke-direct {p0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1060399
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1060400
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;->f:Z

    .line 1060401
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1060397
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1060398
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1060396
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1060393
    new-instance v0, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/pages/FetchPagesYouCanActGraphQLModels$FetchPagesYouCanActModel;-><init>()V

    .line 1060394
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1060395
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1060392
    const v0, -0x15a54818

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1060391
    const v0, 0x252222

    return v0
.end method
