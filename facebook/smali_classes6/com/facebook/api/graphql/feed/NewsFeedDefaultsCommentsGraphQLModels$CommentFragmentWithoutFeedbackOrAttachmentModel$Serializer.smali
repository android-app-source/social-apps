.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 850802
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel;

    new-instance v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 850803
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 850804
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 850805
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 850806
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 850807
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 850808
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 850809
    if-eqz v2, :cond_0

    .line 850810
    const-string v3, "author"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850811
    invoke-static {v1, v2, p1, p2}, LX/59B;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 850812
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 850813
    if-eqz v2, :cond_1

    .line 850814
    const-string v3, "body"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850815
    invoke-static {v1, v2, p1, p2}, LX/59F;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 850816
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 850817
    if-eqz v2, :cond_2

    .line 850818
    const-string v3, "body_markdown_html"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850819
    invoke-static {v1, v2, p1}, LX/59C;->a(LX/15i;ILX/0nX;)V

    .line 850820
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850821
    if-eqz v2, :cond_3

    .line 850822
    const-string v3, "can_edit_constituent_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850823
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850824
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850825
    if-eqz v2, :cond_4

    .line 850826
    const-string v3, "can_viewer_delete"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850827
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850828
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850829
    if-eqz v2, :cond_5

    .line 850830
    const-string v3, "can_viewer_edit"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850831
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850832
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850833
    if-eqz v2, :cond_6

    .line 850834
    const-string v3, "can_viewer_share"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850835
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850836
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 850837
    if-eqz v2, :cond_7

    .line 850838
    const-string v3, "constituent_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850839
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 850840
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 850841
    cmp-long v4, v2, v4

    if-eqz v4, :cond_8

    .line 850842
    const-string v4, "created_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850843
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 850844
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 850845
    if-eqz v2, :cond_9

    .line 850846
    const-string v3, "edit_history"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850847
    invoke-static {v1, v2, p1}, LX/59G;->a(LX/15i;ILX/0nX;)V

    .line 850848
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 850849
    if-eqz v2, :cond_a

    .line 850850
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850851
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 850852
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850853
    if-eqz v2, :cond_b

    .line 850854
    const-string v3, "is_featured"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850855
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850856
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850857
    if-eqz v2, :cond_c

    .line 850858
    const-string v3, "is_marked_as_spam"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850859
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850860
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850861
    if-eqz v2, :cond_d

    .line 850862
    const-string v3, "is_pinned"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850863
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850864
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 850865
    if-eqz v2, :cond_e

    .line 850866
    const-string v3, "spam_display_mode"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850867
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 850868
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 850869
    if-eqz v2, :cond_f

    .line 850870
    const-string v3, "translatability_for_viewer"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850871
    invoke-static {v1, v2, p1}, LX/59K;->a(LX/15i;ILX/0nX;)V

    .line 850872
    :cond_f
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 850873
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 850874
    check-cast p1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$Serializer;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
