.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x9be1e81
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:J

.field private o:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 850517
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 850516
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 850514
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 850515
    return-void
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttachments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 850512
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->e:Ljava/util/List;

    .line 850513
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850510
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->f:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->f:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    .line 850511
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->f:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850508
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    .line 850509
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    return-object v0
.end method

.method private m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBodyMarkdownHtml"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850506
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 850507
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850504
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->m:Ljava/lang/String;

    .line 850505
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method private o()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEditHistory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850502
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 850503
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->o:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850518
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->p:Ljava/lang/String;

    .line 850519
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850500
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->t:Ljava/lang/String;

    .line 850501
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->t:Ljava/lang/String;

    return-object v0
.end method

.method private r()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTranslatabilityForViewer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850412
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 850413
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->u:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 850470
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 850471
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 850472
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->k()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 850473
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 850474
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x24e06805

    invoke-static {v4, v3, v5}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 850475
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 850476
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->o()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, 0x22ac0806

    invoke-static {v6, v5, v7}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 850477
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 850478
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 850479
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->r()LX/1vs;

    move-result-object v5

    iget-object v9, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v10, 0x4f659d37

    invoke-static {v9, v5, v10}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 850480
    const/16 v5, 0x11

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 850481
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 850482
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 850483
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 850484
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 850485
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 850486
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 850487
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 850488
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 850489
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 850490
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->n:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 850491
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 850492
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 850493
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->q:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 850494
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->r:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 850495
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->s:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 850496
    const/16 v0, 0xf

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 850497
    const/16 v0, 0x10

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 850498
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 850499
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 850433
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 850434
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 850435
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 850436
    if-eqz v1, :cond_6

    .line 850437
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;

    .line 850438
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 850439
    :goto_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->k()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 850440
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->k()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    .line 850441
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->k()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 850442
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;

    .line 850443
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->f:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    .line 850444
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 850445
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    .line 850446
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->l()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 850447
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;

    .line 850448
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel;

    .line 850449
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 850450
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x24e06805

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 850451
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 850452
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;

    .line 850453
    iput v3, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->h:I

    move-object v1, v0

    .line 850454
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 850455
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x22ac0806

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 850456
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 850457
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;

    .line 850458
    iput v3, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->o:I

    move-object v1, v0

    .line 850459
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->r()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 850460
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->r()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4f659d37

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 850461
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->r()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 850462
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;

    .line 850463
    iput v3, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->u:I

    move-object v1, v0

    .line 850464
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 850465
    if-nez v1, :cond_5

    :goto_1
    return-object p0

    .line 850466
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 850467
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 850468
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_5
    move-object p0, v1

    .line 850469
    goto :goto_1

    :cond_6
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850432
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 850419
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 850420
    const/4 v0, 0x3

    const v1, -0x24e06805

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->h:I

    .line 850421
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->i:Z

    .line 850422
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->j:Z

    .line 850423
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->k:Z

    .line 850424
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->l:Z

    .line 850425
    const/16 v0, 0x9

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->n:J

    .line 850426
    const/16 v0, 0xa

    const v1, 0x22ac0806

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->o:I

    .line 850427
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->q:Z

    .line 850428
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->r:Z

    .line 850429
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->s:Z

    .line 850430
    const/16 v0, 0x10

    const v1, 0x4f659d37

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;->u:I

    .line 850431
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 850416
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;-><init>()V

    .line 850417
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 850418
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 850415
    const v0, 0x38d8109b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 850414
    const v0, -0x642179c1

    return v0
.end method
