.class public final Lcom/facebook/api/graphql/feed/StoryMutationModels$HideableStoryMutationFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/feed/StoryMutationModels$HideableStoryMutationFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 857426
    const-class v0, Lcom/facebook/api/graphql/feed/StoryMutationModels$HideableStoryMutationFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/feed/StoryMutationModels$HideableStoryMutationFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/feed/StoryMutationModels$HideableStoryMutationFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 857427
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 857428
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/feed/StoryMutationModels$HideableStoryMutationFieldsModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 857429
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 857430
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 857431
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 857432
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 857433
    if-eqz p0, :cond_0

    .line 857434
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857435
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 857436
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 857437
    if-eqz p0, :cond_1

    .line 857438
    const-string p2, "local_last_negative_feedback_action_type"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857439
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 857440
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 857441
    if-eqz p0, :cond_2

    .line 857442
    const-string p2, "local_story_visibility"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857443
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 857444
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 857445
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 857446
    check-cast p1, Lcom/facebook/api/graphql/feed/StoryMutationModels$HideableStoryMutationFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/feed/StoryMutationModels$HideableStoryMutationFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/feed/StoryMutationModels$HideableStoryMutationFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
