.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xd5c3d24
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 852843
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 852849
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 852847
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 852848
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 852844
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 852845
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 852846
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private j()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 852824
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->f:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->f:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    .line 852825
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->f:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 852835
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 852836
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 852837
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->j()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 852838
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 852839
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 852840
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 852841
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 852842
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 852850
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 852851
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->j()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 852852
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->j()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    .line 852853
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->j()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 852854
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;

    .line 852855
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->f:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    .line 852856
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 852857
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 852834
    new-instance v0, LX/59g;

    invoke-direct {v0, p1}, LX/59g;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 852832
    invoke-virtual {p2}, LX/18L;->a()V

    .line 852833
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 852831
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 852828
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;-><init>()V

    .line 852829
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 852830
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 852827
    const v0, -0x1316c236

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 852826
    const v0, 0x252222

    return v0
.end method
