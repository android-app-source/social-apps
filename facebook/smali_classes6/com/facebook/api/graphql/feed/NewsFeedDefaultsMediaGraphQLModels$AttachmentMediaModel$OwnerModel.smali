.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x393c452
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 853216
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 853217
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 853218
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 853219
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;)V
    .locals 4

    .prologue
    .line 853220
    iput-object p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->h:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 853221
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 853222
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 853223
    if-eqz v0, :cond_0

    .line 853224
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 853225
    :cond_0
    return-void

    .line 853226
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 853233
    iput-boolean p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->l:Z

    .line 853234
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 853235
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 853236
    if-eqz v0, :cond_0

    .line 853237
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 853238
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 853227
    iput-boolean p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->m:Z

    .line 853228
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 853229
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 853230
    if-eqz v0, :cond_0

    .line 853231
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 853232
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 853241
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 853242
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 853243
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 853244
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->f:Ljava/lang/String;

    .line 853245
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 853239
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->h:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->h:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 853240
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->h:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 853193
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->i:Ljava/lang/String;

    .line 853194
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 853212
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 853213
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->l:Z

    return v0
.end method

.method private o()Z
    .locals 2

    .prologue
    .line 853214
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 853215
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->m:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 853195
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 853196
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 853197
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 853198
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->l()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 853199
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 853200
    const/16 v4, 0x9

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 853201
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 853202
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 853203
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 853204
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 853205
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 853206
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 853207
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 853208
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 853209
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 853210
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 853211
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 853190
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 853191
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 853192
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 853189
    new-instance v0, LX/59j;

    invoke-direct {v0, p1}, LX/59j;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 853188
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 853181
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 853182
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->g:Z

    .line 853183
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->j:Z

    .line 853184
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->k:Z

    .line 853185
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->l:Z

    .line 853186
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->m:Z

    .line 853187
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 853167
    const-string v0, "live_video_subscription_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 853168
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->l()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 853169
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 853170
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 853171
    :goto_0
    return-void

    .line 853172
    :cond_0
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 853173
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 853174
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 853175
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 853176
    :cond_1
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 853177
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 853178
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 853179
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 853180
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 853160
    const-string v0, "live_video_subscription_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 853161
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->a(Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;)V

    .line 853162
    :cond_0
    :goto_0
    return-void

    .line 853163
    :cond_1
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 853164
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->a(Z)V

    goto :goto_0

    .line 853165
    :cond_2
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 853166
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;->b(Z)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 853157
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;-><init>()V

    .line 853158
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 853159
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 853156
    const v0, 0x4246fd50

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 853155
    const v0, 0x3c2b9d5

    return v0
.end method
