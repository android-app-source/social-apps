.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4b6ad3f8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:J

.field private i:Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ShareableModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 853048
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 853049
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 853050
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 853051
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 853052
    iput-object p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->f:Ljava/util/List;

    .line 853053
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 853054
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 853055
    if-eqz v0, :cond_0

    .line 853056
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 853057
    :cond_0
    return-void
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 853066
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ActorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->e:Ljava/util/List;

    .line 853067
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttachments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 853058
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->f:Ljava/util/List;

    .line 853059
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 853060
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->g:Ljava/lang/String;

    .line 853061
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 853011
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->i:Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->i:Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    .line 853012
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->i:Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 853062
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->j:Ljava/lang/String;

    .line 853063
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ShareableModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 853064
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ShareableModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ShareableModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ShareableModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ShareableModel;

    .line 853065
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ShareableModel;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 853027
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->l:Ljava/lang/String;

    .line 853028
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->l:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 853029
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 853030
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 853031
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 853032
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 853033
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->m()Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 853034
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 853035
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->o()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ShareableModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 853036
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 853037
    const/16 v3, 0x8

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 853038
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 853039
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 853040
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 853041
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->h:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 853042
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 853043
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 853044
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 853045
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 853046
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 853047
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 852986
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 852987
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 852988
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 852989
    if-eqz v1, :cond_0

    .line 852990
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    .line 852991
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->e:Ljava/util/List;

    .line 852992
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 852993
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 852994
    if-eqz v1, :cond_1

    .line 852995
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    .line 852996
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->f:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 852997
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->m()Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 852998
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->m()Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    .line 852999
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->m()Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 853000
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    .line 853001
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->i:Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    .line 853002
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->o()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ShareableModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 853003
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->o()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ShareableModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ShareableModel;

    .line 853004
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->o()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ShareableModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 853005
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    .line 853006
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->k:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel$ShareableModel;

    .line 853007
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 853008
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 853009
    new-instance v0, LX/59h;

    invoke-direct {v0, p1}, LX/59h;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 853010
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 853013
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 853014
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->h:J

    .line 853015
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 853016
    invoke-virtual {p2}, LX/18L;->a()V

    .line 853017
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 853018
    const-string v0, "attachments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 853019
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;->a(Ljava/util/List;)V

    .line 853020
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 853021
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 853022
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;-><init>()V

    .line 853023
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 853024
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 853025
    const v0, -0x741ee306

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 853026
    const v0, 0x4c808d5

    return v0
.end method
