.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xe38a53e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 855665
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 855664
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 855662
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 855663
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 855625
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 855626
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 855627
    return-void
.end method

.method public static a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;
    .locals 10

    .prologue
    .line 855638
    if-nez p0, :cond_0

    .line 855639
    const/4 p0, 0x0

    .line 855640
    :goto_0
    return-object p0

    .line 855641
    :cond_0
    instance-of v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    if-eqz v0, :cond_1

    .line 855642
    check-cast p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    goto :goto_0

    .line 855643
    :cond_1
    new-instance v2, LX/5A3;

    invoke-direct {v2}, LX/5A3;-><init>()V

    .line 855644
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 855645
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 855646
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel$NodesModel;

    invoke-static {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel$NodesModel;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel$NodesModel;)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel$NodesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 855647
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 855648
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/5A3;->a:LX/0Px;

    .line 855649
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 855650
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 855651
    iget-object v5, v2, LX/5A3;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 855652
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 855653
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 855654
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 855655
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 855656
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 855657
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 855658
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 855659
    new-instance v5, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    invoke-direct {v5, v4}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;-><init>(LX/15i;)V

    .line 855660
    move-object p0, v5

    .line 855661
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 855666
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 855667
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 855668
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 855669
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 855670
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 855671
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 855636
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel$NodesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->e:Ljava/util/List;

    .line 855637
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 855628
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 855629
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 855630
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 855631
    if-eqz v1, :cond_0

    .line 855632
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    .line 855633
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;->e:Ljava/util/List;

    .line 855634
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 855635
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 855622
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;-><init>()V

    .line 855623
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 855624
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 855621
    const v0, 0x3c0950a5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 855620
    const v0, 0x678a5a7a

    return v0
.end method
