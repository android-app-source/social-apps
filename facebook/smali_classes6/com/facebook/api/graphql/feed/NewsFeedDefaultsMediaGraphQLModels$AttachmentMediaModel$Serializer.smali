.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 853246
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel;

    new-instance v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 853247
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 853248
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel;LX/0nX;LX/0my;)V
    .locals 10

    .prologue
    .line 853249
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 853250
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 v7, 0x4c

    const/4 v5, 0x7

    const/4 v4, 0x5

    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 853251
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 853252
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v2

    .line 853253
    if-eqz v2, :cond_0

    .line 853254
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853255
    invoke-static {v1, v0, v6, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 853256
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853257
    if-eqz v2, :cond_1

    .line 853258
    const-string v3, "animated_image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853259
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 853260
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853261
    if-eqz v2, :cond_2

    .line 853262
    const-string v3, "atom_size"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853263
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853264
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853265
    if-eqz v2, :cond_3

    .line 853266
    const-string v3, "attribution_app"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853267
    invoke-static {v1, v2, p1, p2}, LX/5C9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 853268
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 853269
    if-eqz v2, :cond_4

    .line 853270
    const-string v3, "attribution_app_metadata"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853271
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853272
    :cond_4
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 853273
    if-eqz v2, :cond_5

    .line 853274
    const-string v2, "audio_availability"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853275
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853276
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853277
    if-eqz v2, :cond_6

    .line 853278
    const-string v3, "bitrate"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853279
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853280
    :cond_6
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 853281
    if-eqz v2, :cond_7

    .line 853282
    const-string v2, "broadcast_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853283
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853284
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853285
    if-eqz v2, :cond_8

    .line 853286
    const-string v3, "can_viewer_delete"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853287
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853288
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853289
    if-eqz v2, :cond_9

    .line 853290
    const-string v3, "can_viewer_export"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853291
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853292
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853293
    if-eqz v2, :cond_a

    .line 853294
    const-string v3, "can_viewer_report"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853295
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853296
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853297
    if-eqz v2, :cond_b

    .line 853298
    const-string v3, "can_viewer_share"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853299
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853300
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 853301
    if-eqz v2, :cond_c

    .line 853302
    const-string v3, "captions_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853303
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853304
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853305
    if-eqz v2, :cond_d

    .line 853306
    const-string v3, "copyright_block_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853307
    invoke-static {v1, v2, p1}, LX/59l;->a(LX/15i;ILX/0nX;)V

    .line 853308
    :cond_d
    const/16 v2, 0xe

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 853309
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_e

    .line 853310
    const-string v4, "created_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853311
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 853312
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853313
    if-eqz v2, :cond_f

    .line 853314
    const-string v3, "creation_story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853315
    invoke-static {v1, v2, p1, p2}, LX/59s;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 853316
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853317
    if-eqz v2, :cond_10

    .line 853318
    const-string v3, "enable_focus"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853319
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853320
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853321
    if-eqz v2, :cond_11

    .line 853322
    const-string v3, "feedback"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853323
    invoke-static {v1, v2, p1, p2}, LX/58T;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 853324
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853325
    if-eqz v2, :cond_12

    .line 853326
    const-string v3, "focus"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853327
    invoke-static {v1, v2, p1}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 853328
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 853329
    cmpl-double v4, v2, v8

    if-eqz v4, :cond_13

    .line 853330
    const-string v4, "focus_width_degrees"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853331
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 853332
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853333
    if-eqz v2, :cond_14

    .line 853334
    const-string v3, "guided_tour"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853335
    invoke-static {v1, v2, p1, p2}, LX/5CB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 853336
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853337
    if-eqz v2, :cond_15

    .line 853338
    const-string v3, "has_stickers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853339
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853340
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853341
    if-eqz v2, :cond_16

    .line 853342
    const-string v3, "hdAtomSize"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853343
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853344
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853345
    if-eqz v2, :cond_17

    .line 853346
    const-string v3, "hdBitrate"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853347
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853348
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853349
    if-eqz v2, :cond_18

    .line 853350
    const-string v3, "height"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853351
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853352
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 853353
    if-eqz v2, :cond_19

    .line 853354
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853355
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853356
    :cond_19
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853357
    if-eqz v2, :cond_1a

    .line 853358
    const-string v3, "image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853359
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 853360
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853361
    if-eqz v2, :cond_1b

    .line 853362
    const-string v3, "imageHigh"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853363
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 853364
    :cond_1b
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853365
    if-eqz v2, :cond_1c

    .line 853366
    const-string v3, "imageLargeAspect"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853367
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 853368
    :cond_1c
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853369
    if-eqz v2, :cond_1d

    .line 853370
    const-string v3, "imageLow"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853371
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 853372
    :cond_1d
    const/16 v2, 0x1e

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853373
    if-eqz v2, :cond_1e

    .line 853374
    const-string v3, "imageMedium"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853375
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 853376
    :cond_1e
    const/16 v2, 0x1f

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853377
    if-eqz v2, :cond_1f

    .line 853378
    const-string v3, "image_blurred"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853379
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 853380
    :cond_1f
    const/16 v2, 0x20

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853381
    if-eqz v2, :cond_20

    .line 853382
    const-string v3, "initial_view_heading_degrees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853383
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853384
    :cond_20
    const/16 v2, 0x21

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853385
    if-eqz v2, :cond_21

    .line 853386
    const-string v3, "initial_view_pitch_degrees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853387
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853388
    :cond_21
    const/16 v2, 0x22

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853389
    if-eqz v2, :cond_22

    .line 853390
    const-string v3, "initial_view_roll_degrees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853391
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853392
    :cond_22
    const/16 v2, 0x23

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853393
    if-eqz v2, :cond_23

    .line 853394
    const-string v3, "instream_video_ad_breaks"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853395
    invoke-static {v1, v2, p1, p2}, LX/59t;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 853396
    :cond_23
    const/16 v2, 0x24

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853397
    if-eqz v2, :cond_24

    .line 853398
    const-string v3, "is_age_restricted"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853399
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853400
    :cond_24
    const/16 v2, 0x25

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853401
    if-eqz v2, :cond_25

    .line 853402
    const-string v3, "is_disturbing"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853403
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853404
    :cond_25
    const/16 v2, 0x26

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853405
    if-eqz v2, :cond_26

    .line 853406
    const-string v3, "is_eligible_for_commercial_break"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853407
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853408
    :cond_26
    const/16 v2, 0x27

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853409
    if-eqz v2, :cond_27

    .line 853410
    const-string v3, "is_live_audio_format"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853411
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853412
    :cond_27
    const/16 v2, 0x28

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853413
    if-eqz v2, :cond_28

    .line 853414
    const-string v3, "is_live_streaming"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853415
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853416
    :cond_28
    const/16 v2, 0x29

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853417
    if-eqz v2, :cond_29

    .line 853418
    const-string v3, "is_looping"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853419
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853420
    :cond_29
    const/16 v2, 0x2a

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853421
    if-eqz v2, :cond_2a

    .line 853422
    const-string v3, "is_playable"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853423
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853424
    :cond_2a
    const/16 v2, 0x2b

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853425
    if-eqz v2, :cond_2b

    .line 853426
    const-string v3, "is_rtc_broadcast"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853427
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853428
    :cond_2b
    const/16 v2, 0x2c

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853429
    if-eqz v2, :cond_2c

    .line 853430
    const-string v3, "is_save_offline_allowed"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853431
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853432
    :cond_2c
    const/16 v2, 0x2d

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853433
    if-eqz v2, :cond_2d

    .line 853434
    const-string v3, "is_save_primary_action"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853435
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853436
    :cond_2d
    const/16 v2, 0x2e

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853437
    if-eqz v2, :cond_2e

    .line 853438
    const-string v3, "is_spherical"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853439
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853440
    :cond_2e
    const/16 v2, 0x2f

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853441
    if-eqz v2, :cond_2f

    .line 853442
    const-string v3, "is_video_broadcast"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853443
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853444
    :cond_2f
    const/16 v2, 0x30

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853445
    if-eqz v2, :cond_30

    .line 853446
    const-string v3, "loop_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853447
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853448
    :cond_30
    const/16 v2, 0x31

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853449
    if-eqz v2, :cond_31

    .line 853450
    const-string v3, "mean_linear_volume"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853451
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853452
    :cond_31
    const/16 v2, 0x32

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853453
    if-eqz v2, :cond_32

    .line 853454
    const-string v3, "message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853455
    invoke-static {v1, v2, p1, p2}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 853456
    :cond_32
    const/16 v2, 0x33

    invoke-virtual {v1, v0, v2, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 853457
    cmpl-double v4, v2, v8

    if-eqz v4, :cond_33

    .line 853458
    const-string v4, "off_focus_level"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853459
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 853460
    :cond_33
    const/16 v2, 0x34

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853461
    if-eqz v2, :cond_34

    .line 853462
    const-string v3, "owner"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853463
    invoke-static {v1, v2, p1}, LX/59u;->a(LX/15i;ILX/0nX;)V

    .line 853464
    :cond_34
    const/16 v2, 0x35

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853465
    if-eqz v2, :cond_35

    .line 853466
    const-string v3, "paired_video"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853467
    invoke-static {v1, v2, p1}, LX/59v;->a(LX/15i;ILX/0nX;)V

    .line 853468
    :cond_35
    const/16 v2, 0x36

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853469
    if-eqz v2, :cond_36

    .line 853470
    const-string v3, "photo_encodings"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853471
    invoke-static {v1, v2, p1, p2}, LX/59z;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 853472
    :cond_36
    const/16 v2, 0x37

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853473
    if-eqz v2, :cond_37

    .line 853474
    const-string v3, "play_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853475
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853476
    :cond_37
    const/16 v2, 0x38

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 853477
    if-eqz v2, :cond_38

    .line 853478
    const-string v3, "playableUrlHdString"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853479
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853480
    :cond_38
    const/16 v2, 0x39

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853481
    if-eqz v2, :cond_39

    .line 853482
    const-string v3, "playable_duration_in_ms"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853483
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853484
    :cond_39
    const/16 v2, 0x3a

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 853485
    if-eqz v2, :cond_3a

    .line 853486
    const-string v3, "playable_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853487
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853488
    :cond_3a
    const/16 v2, 0x3b

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 853489
    if-eqz v2, :cond_3b

    .line 853490
    const-string v3, "playlist"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853491
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853492
    :cond_3b
    const/16 v2, 0x3c

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853493
    if-eqz v2, :cond_3c

    .line 853494
    const-string v3, "post_play_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853495
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853496
    :cond_3c
    const/16 v2, 0x3d

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 853497
    if-eqz v2, :cond_3d

    .line 853498
    const-string v3, "preferredPlayableUrlString"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853499
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853500
    :cond_3d
    const/16 v2, 0x3e

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 853501
    if-eqz v2, :cond_3e

    .line 853502
    const-string v3, "profile_video_approximate_play_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853503
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853504
    :cond_3e
    const/16 v2, 0x3f

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 853505
    if-eqz v2, :cond_3f

    .line 853506
    const-string v3, "projection_type"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853507
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853508
    :cond_3f
    const/16 v2, 0x40

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853509
    if-eqz v2, :cond_40

    .line 853510
    const-string v3, "publisher_context"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853511
    invoke-static {v1, v2, p1}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 853512
    :cond_40
    const/16 v2, 0x41

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853513
    if-eqz v2, :cond_41

    .line 853514
    const-string v3, "should_open_single_publisher"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853515
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853516
    :cond_41
    const/16 v2, 0x42

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853517
    if-eqz v2, :cond_42

    .line 853518
    const-string v3, "should_show_live_subscribe"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853519
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853520
    :cond_42
    const/16 v2, 0x43

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853521
    if-eqz v2, :cond_43

    .line 853522
    const-string v3, "show_video_channel_subscribe_button"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853523
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853524
    :cond_43
    const/16 v2, 0x44

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853525
    if-eqz v2, :cond_44

    .line 853526
    const-string v3, "show_video_home_follow_button"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853527
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853528
    :cond_44
    const/16 v2, 0x45

    invoke-virtual {v1, v0, v2, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 853529
    cmpl-double v4, v2, v8

    if-eqz v4, :cond_45

    .line 853530
    const-string v4, "sphericalFullscreenAspectRatio"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853531
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 853532
    :cond_45
    const/16 v2, 0x46

    invoke-virtual {v1, v0, v2, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 853533
    cmpl-double v4, v2, v8

    if-eqz v4, :cond_46

    .line 853534
    const-string v4, "sphericalInlineAspectRatio"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853535
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 853536
    :cond_46
    const/16 v2, 0x47

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 853537
    if-eqz v2, :cond_47

    .line 853538
    const-string v3, "sphericalPlayableUrlHdString"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853539
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853540
    :cond_47
    const/16 v2, 0x48

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 853541
    if-eqz v2, :cond_48

    .line 853542
    const-string v3, "sphericalPlayableUrlSdString"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853543
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853544
    :cond_48
    const/16 v2, 0x49

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853545
    if-eqz v2, :cond_49

    .line 853546
    const-string v3, "sphericalPreferredFov"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853547
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853548
    :cond_49
    const/16 v2, 0x4a

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 853549
    if-eqz v2, :cond_4a

    .line 853550
    const-string v3, "supports_time_slices"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853551
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 853552
    :cond_4a
    const/16 v2, 0x4b

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853553
    if-eqz v2, :cond_4b

    .line 853554
    const-string v3, "total_posts"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853555
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853556
    :cond_4b
    invoke-virtual {v1, v0, v7}, LX/15i;->g(II)I

    move-result v2

    .line 853557
    if-eqz v2, :cond_4c

    .line 853558
    const-string v2, "video_captions_locales"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853559
    invoke-virtual {v1, v0, v7}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 853560
    :cond_4c
    const/16 v2, 0x4d

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853561
    if-eqz v2, :cond_4d

    .line 853562
    const-string v3, "video_channel"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853563
    invoke-static {v1, v2, p1}, LX/5CD;->a(LX/15i;ILX/0nX;)V

    .line 853564
    :cond_4d
    const/16 v2, 0x4e

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853565
    if-eqz v2, :cond_4e

    .line 853566
    const-string v3, "video_full_size"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853567
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853568
    :cond_4e
    const/16 v2, 0x4f

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 853569
    if-eqz v2, :cond_4f

    .line 853570
    const-string v2, "video_status_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853571
    const/16 v2, 0x4f

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 853572
    :cond_4f
    const/16 v2, 0x50

    invoke-virtual {v1, v0, v2, v6}, LX/15i;->a(III)I

    move-result v2

    .line 853573
    if-eqz v2, :cond_50

    .line 853574
    const-string v3, "width"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 853575
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 853576
    :cond_50
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 853577
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 853578
    check-cast p1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$Serializer;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel;LX/0nX;LX/0my;)V

    return-void
.end method
