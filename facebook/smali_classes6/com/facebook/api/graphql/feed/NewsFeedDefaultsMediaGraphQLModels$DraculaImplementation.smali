.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 854039
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 854040
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 854037
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 854038
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 22

    .prologue
    .line 853993
    if-nez p1, :cond_0

    .line 853994
    const/4 v6, 0x0

    .line 853995
    :goto_0
    return v6

    .line 853996
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 853997
    new-instance v6, Ljava/lang/IllegalArgumentException;

    invoke-direct {v6}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v6

    .line 853998
    :sswitch_0
    const/4 v6, 0x0

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v6, v8, v9}, LX/15i;->a(IIJ)J

    move-result-wide v8

    .line 853999
    const/4 v6, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;

    move-result-object v6

    .line 854000
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    .line 854001
    const/4 v6, 0x2

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 854002
    const/4 v7, 0x0

    const-wide/16 v10, 0x0

    move-object/from16 v6, p3

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IJJ)V

    .line 854003
    const/4 v6, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v12}, LX/186;->b(II)V

    .line 854004
    invoke-virtual/range {p3 .. p3}, LX/186;->d()I

    move-result v6

    goto :goto_0

    .line 854005
    :sswitch_1
    const/4 v6, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 854006
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 854007
    const/4 v7, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 854008
    const/4 v7, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 854009
    invoke-virtual/range {p3 .. p3}, LX/186;->d()I

    move-result v6

    goto :goto_0

    .line 854010
    :sswitch_2
    const/4 v6, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 854011
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 854012
    const/4 v7, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 854013
    const/4 v7, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 854014
    invoke-virtual/range {p3 .. p3}, LX/186;->d()I

    move-result v6

    goto :goto_0

    .line 854015
    :sswitch_3
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v6, v7}, LX/15i;->a(III)I

    move-result v6

    .line 854016
    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v7, v8}, LX/15i;->a(III)I

    move-result v7

    .line 854017
    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v8, v9}, LX/15i;->a(III)I

    move-result v10

    .line 854018
    const/4 v8, 0x3

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v8, v9}, LX/15i;->a(III)I

    move-result v11

    .line 854019
    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v8, v9}, LX/15i;->a(III)I

    move-result v12

    .line 854020
    const/4 v8, 0x5

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v8, v9}, LX/15i;->a(III)I

    move-result v13

    .line 854021
    const/4 v8, 0x6

    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v8, v14, v15}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 854022
    const/4 v14, 0x7

    const-wide/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-wide/from16 v2, v16

    invoke-virtual {v0, v1, v14, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v14

    .line 854023
    const/16 v16, 0x8

    const-wide/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v16

    move-wide/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3, v4}, LX/15i;->a(IID)D

    move-result-wide v16

    .line 854024
    const/16 v18, 0x9

    const-wide/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v18

    move-wide/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v4}, LX/15i;->a(IID)D

    move-result-wide v18

    .line 854025
    const/16 v20, 0xa

    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 854026
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v6, v2}, LX/186;->a(III)V

    .line 854027
    const/4 v6, 0x1

    const/16 v20, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v6, v7, v1}, LX/186;->a(III)V

    .line 854028
    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10, v7}, LX/186;->a(III)V

    .line 854029
    const/4 v6, 0x3

    const/4 v7, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v11, v7}, LX/186;->a(III)V

    .line 854030
    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v12, v7}, LX/186;->a(III)V

    .line 854031
    const/4 v6, 0x5

    const/4 v7, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v13, v7}, LX/186;->a(III)V

    .line 854032
    const/4 v7, 0x6

    const-wide/16 v10, 0x0

    move-object/from16 v6, p3

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IDD)V

    .line 854033
    const/4 v7, 0x7

    const-wide/16 v10, 0x0

    move-object/from16 v6, p3

    move-wide v8, v14

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IDD)V

    .line 854034
    const/16 v7, 0x8

    const-wide/16 v10, 0x0

    move-object/from16 v6, p3

    move-wide/from16 v8, v16

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IDD)V

    .line 854035
    const/16 v7, 0x9

    const-wide/16 v10, 0x0

    move-object/from16 v6, p3

    move-wide/from16 v8, v18

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IDD)V

    .line 854036
    invoke-virtual/range {p3 .. p3}, LX/186;->d()I

    move-result v6

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5ab1603d -> :sswitch_3
        -0x4827bf4e -> :sswitch_1
        -0x412840f7 -> :sswitch_2
        -0x2246275c -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 853992
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 853989
    sparse-switch p0, :sswitch_data_0

    .line 853990
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 853991
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x5ab1603d -> :sswitch_0
        -0x4827bf4e -> :sswitch_0
        -0x412840f7 -> :sswitch_0
        -0x2246275c -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 853977
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 853987
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;->b(I)V

    .line 853988
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 853982
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 853983
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 853984
    :cond_0
    iput-object p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 853985
    iput p2, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;->b:I

    .line 853986
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 854041
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 853981
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 853978
    iget v0, p0, LX/1vt;->c:I

    .line 853979
    move v0, v0

    .line 853980
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 853974
    iget v0, p0, LX/1vt;->c:I

    .line 853975
    move v0, v0

    .line 853976
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 853971
    iget v0, p0, LX/1vt;->b:I

    .line 853972
    move v0, v0

    .line 853973
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 853968
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 853969
    move-object v0, v0

    .line 853970
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 853959
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 853960
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 853961
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 853962
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 853963
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 853964
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 853965
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 853966
    invoke-static {v3, v9, v2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 853967
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 853956
    iget v0, p0, LX/1vt;->c:I

    .line 853957
    move v0, v0

    .line 853958
    return v0
.end method
