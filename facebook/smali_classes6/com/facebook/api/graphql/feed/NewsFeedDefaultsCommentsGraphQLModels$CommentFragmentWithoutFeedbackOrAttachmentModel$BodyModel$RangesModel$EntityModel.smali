.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/171;
.implements LX/16i;
.implements LX/16f;
.implements LX/1y6;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x608d4137
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 850706
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 850705
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 850703
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 850704
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 850683
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 850684
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 850685
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 850686
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 850687
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 850688
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->v_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 850689
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->w_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 850690
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 850691
    const/16 v7, 0x9

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 850692
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 850693
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 850694
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 850695
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 850696
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 850697
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 850698
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 850699
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 850700
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 850701
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 850702
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 850680
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 850681
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 850682
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 850668
    new-instance v0, LX/599;

    invoke-direct {v0, p1}, LX/599;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850679
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 850675
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 850676
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->i:Z

    .line 850677
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->j:Z

    .line 850678
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 850673
    invoke-virtual {p2}, LX/18L;->a()V

    .line 850674
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 850672
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850669
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 850670
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 850671
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 850707
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;-><init>()V

    .line 850708
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 850709
    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 850652
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->f:Ljava/util/List;

    .line 850653
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850654
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->g:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->g:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 850655
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->g:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 850656
    const v0, -0x2652c184

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850657
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->h:Ljava/lang/String;

    .line 850658
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 850659
    const v0, 0x7c02d003

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850660
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->m:Ljava/lang/String;

    .line 850661
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 850662
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 850663
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->i:Z

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 850650
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 850651
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->j:Z

    return v0
.end method

.method public final v_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850664
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->k:Ljava/lang/String;

    .line 850665
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final w_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850666
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->l:Ljava/lang/String;

    .line 850667
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$BodyModel$RangesModel$EntityModel;->l:Ljava/lang/String;

    return-object v0
.end method
