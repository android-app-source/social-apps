.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x673472e2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 850584
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 850583
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 850581
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 850582
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850578
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 850579
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 850580
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850576
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->f:Ljava/lang/String;

    .line 850577
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850574
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->j:Ljava/lang/String;

    .line 850575
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850572
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 850573
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850570
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 850571
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 850585
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 850586
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 850587
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 850588
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 850589
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 850590
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 850591
    const/16 v5, 0x8

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 850592
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 850593
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 850594
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 850595
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 850596
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 850597
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 850598
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 850599
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 850600
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 850601
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 850557
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 850558
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 850559
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 850560
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 850561
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    .line 850562
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 850563
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 850564
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 850565
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 850566
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    .line 850567
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 850568
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 850569
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 850556
    new-instance v0, LX/598;

    invoke-direct {v0, p1}, LX/598;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 850555
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 850550
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 850551
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->g:Z

    .line 850552
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->h:Z

    .line 850553
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;->i:Z

    .line 850554
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 850548
    invoke-virtual {p2}, LX/18L;->a()V

    .line 850549
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 850547
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 850544
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackOrAttachmentModel$AuthorModel;-><init>()V

    .line 850545
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 850546
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 850543
    const v0, -0x43a4615

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 850542
    const v0, 0x3c2b9d5

    return v0
.end method
