.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 851101
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 851102
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 851099
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 851100
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 851081
    if-nez p1, :cond_0

    .line 851082
    :goto_0
    return v0

    .line 851083
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 851084
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 851085
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 851086
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 851087
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 851088
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 851089
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 851090
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 851091
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 851092
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 851093
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 851094
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v1

    .line 851095
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 851096
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 851097
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 851098
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x24e06805 -> :sswitch_0
        0x22ac0806 -> :sswitch_1
        0x4f659d37 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 851080
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 851077
    sparse-switch p0, :sswitch_data_0

    .line 851078
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 851079
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x24e06805 -> :sswitch_0
        0x22ac0806 -> :sswitch_0
        0x4f659d37 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 851076
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 851074
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->b(I)V

    .line 851075
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 851069
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 851070
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 851071
    :cond_0
    iput-object p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 851072
    iput p2, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->b:I

    .line 851073
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 851068
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 851043
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 851065
    iget v0, p0, LX/1vt;->c:I

    .line 851066
    move v0, v0

    .line 851067
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 851062
    iget v0, p0, LX/1vt;->c:I

    .line 851063
    move v0, v0

    .line 851064
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 851059
    iget v0, p0, LX/1vt;->b:I

    .line 851060
    move v0, v0

    .line 851061
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 851056
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 851057
    move-object v0, v0

    .line 851058
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 851047
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 851048
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 851049
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 851050
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 851051
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 851052
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 851053
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 851054
    invoke-static {v3, v9, v2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 851055
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 851044
    iget v0, p0, LX/1vt;->c:I

    .line 851045
    move v0, v0

    .line 851046
    return v0
.end method
