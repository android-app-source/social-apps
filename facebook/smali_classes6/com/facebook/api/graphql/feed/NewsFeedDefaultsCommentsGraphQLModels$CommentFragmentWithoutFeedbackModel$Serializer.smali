.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 850410
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;

    new-instance v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 850411
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 850409
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 850335
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 850336
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 850337
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 850338
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 850339
    if-eqz v2, :cond_0

    .line 850340
    const-string v3, "attachments"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850341
    invoke-static {v1, v2, p1, p2}, LX/59I;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 850342
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 850343
    if-eqz v2, :cond_1

    .line 850344
    const-string v3, "author"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850345
    invoke-static {v1, v2, p1, p2}, LX/59B;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 850346
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 850347
    if-eqz v2, :cond_2

    .line 850348
    const-string v3, "body"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850349
    invoke-static {v1, v2, p1, p2}, LX/59F;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 850350
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 850351
    if-eqz v2, :cond_3

    .line 850352
    const-string v3, "body_markdown_html"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850353
    invoke-static {v1, v2, p1}, LX/59C;->a(LX/15i;ILX/0nX;)V

    .line 850354
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850355
    if-eqz v2, :cond_4

    .line 850356
    const-string v3, "can_edit_constituent_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850357
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850358
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850359
    if-eqz v2, :cond_5

    .line 850360
    const-string v3, "can_viewer_delete"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850361
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850362
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850363
    if-eqz v2, :cond_6

    .line 850364
    const-string v3, "can_viewer_edit"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850365
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850366
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850367
    if-eqz v2, :cond_7

    .line 850368
    const-string v3, "can_viewer_share"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850369
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850370
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 850371
    if-eqz v2, :cond_8

    .line 850372
    const-string v3, "constituent_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850373
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 850374
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 850375
    cmp-long v4, v2, v4

    if-eqz v4, :cond_9

    .line 850376
    const-string v4, "created_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850377
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 850378
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 850379
    if-eqz v2, :cond_a

    .line 850380
    const-string v3, "edit_history"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850381
    invoke-static {v1, v2, p1}, LX/59G;->a(LX/15i;ILX/0nX;)V

    .line 850382
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 850383
    if-eqz v2, :cond_b

    .line 850384
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850385
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 850386
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850387
    if-eqz v2, :cond_c

    .line 850388
    const-string v3, "is_featured"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850389
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850390
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850391
    if-eqz v2, :cond_d

    .line 850392
    const-string v3, "is_marked_as_spam"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850393
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850394
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 850395
    if-eqz v2, :cond_e

    .line 850396
    const-string v3, "is_pinned"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850397
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 850398
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 850399
    if-eqz v2, :cond_f

    .line 850400
    const-string v3, "spam_display_mode"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850401
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 850402
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 850403
    if-eqz v2, :cond_10

    .line 850404
    const-string v3, "translatability_for_viewer"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850405
    invoke-static {v1, v2, p1}, LX/59K;->a(LX/15i;ILX/0nX;)V

    .line 850406
    :cond_10
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 850407
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 850408
    check-cast p1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel$Serializer;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$CommentFragmentWithoutFeedbackModel;LX/0nX;LX/0my;)V

    return-void
.end method
