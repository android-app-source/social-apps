.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/59d;
.implements LX/59c;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x57bec52d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel$Serializer;
.end annotation


# instance fields
.field private A:Z

.field private B:I

.field private C:I

.field private D:I

.field private E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private L:I

.field private M:I

.field private N:I

.field private O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$InstreamVideoAdBreaksModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private P:Z

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:Z

.field private Z:Z

.field private aA:I

.field private aB:Z

.field private aC:I

.field private aD:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aE:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aF:I

.field private aG:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aH:I

.field private aa:Z

.field private ab:I

.field private ac:I

.field private ad:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ae:D

.field private af:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ag:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ah:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$Photo360FieldsModel$PhotoEncodingsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ai:I

.field private aj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ak:I

.field private al:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private am:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private an:I

.field private ao:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ap:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aq:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ar:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private as:Z

.field private at:Z

.field private au:Z

.field private av:Z

.field private aw:D

.field private ax:D

.field private ay:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private az:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I

.field private i:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I

.field private m:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:J

.field private u:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Z

.field private w:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:D

.field private z:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 856076
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 856077
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 856078
    const/16 v0, 0x52

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 856079
    return-void
.end method

.method private A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856080
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 856081
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private B()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856082
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->K:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->K:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 856083
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->K:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private C()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$InstreamVideoAdBreaksModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 856084
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->O:Ljava/util/List;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$InstreamVideoAdBreaksModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->O:Ljava/util/List;

    .line 856085
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->O:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856086
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ad:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ad:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 856087
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ad:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method private E()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856088
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->af:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;

    const/16 v1, 0x35

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->af:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;

    .line 856089
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->af:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;

    return-object v0
.end method

.method private F()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPairedVideo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x6

    .line 856090
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 856091
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ag:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private G()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhotoEncodings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$Photo360FieldsModel$PhotoEncodingsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 856092
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ah:Ljava/util/List;

    const/16 v1, 0x37

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$Photo360FieldsModel$PhotoEncodingsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ah:Ljava/util/List;

    .line 856093
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ah:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private H()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856094
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aj:Ljava/lang/String;

    const/16 v1, 0x39

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aj:Ljava/lang/String;

    .line 856095
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aj:Ljava/lang/String;

    return-object v0
.end method

.method private I()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856096
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->al:Ljava/lang/String;

    const/16 v1, 0x3b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->al:Ljava/lang/String;

    .line 856097
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->al:Ljava/lang/String;

    return-object v0
.end method

.method private J()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856098
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->am:Ljava/lang/String;

    const/16 v1, 0x3c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->am:Ljava/lang/String;

    .line 856099
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->am:Ljava/lang/String;

    return-object v0
.end method

.method private K()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856100
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ao:Ljava/lang/String;

    const/16 v1, 0x3e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ao:Ljava/lang/String;

    .line 856101
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ao:Ljava/lang/String;

    return-object v0
.end method

.method private L()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856130
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ap:Ljava/lang/String;

    const/16 v1, 0x3f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ap:Ljava/lang/String;

    .line 856131
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ap:Ljava/lang/String;

    return-object v0
.end method

.method private M()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856102
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aq:Ljava/lang/String;

    const/16 v1, 0x40

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aq:Ljava/lang/String;

    .line 856103
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aq:Ljava/lang/String;

    return-object v0
.end method

.method private N()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856104
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ar:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0x41

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ar:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 856105
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ar:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private O()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856106
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ay:Ljava/lang/String;

    const/16 v1, 0x48

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ay:Ljava/lang/String;

    .line 856107
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ay:Ljava/lang/String;

    return-object v0
.end method

.method private P()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856108
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->az:Ljava/lang/String;

    const/16 v1, 0x49

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->az:Ljava/lang/String;

    .line 856109
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->az:Ljava/lang/String;

    return-object v0
.end method

.method private Q()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 856110
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aD:Ljava/util/List;

    const/16 v1, 0x4d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aD:Ljava/util/List;

    .line 856111
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aD:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private R()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856002
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aE:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    const/16 v1, 0x4e

    const-class v2, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aE:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    .line 856003
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aE:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    return-object v0
.end method

.method private S()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856114
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aG:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const/16 v1, 0x50

    const-class v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aG:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 856115
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aG:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 856116
    iput-object p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ad:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 856117
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 856118
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 856119
    if-eqz v0, :cond_0

    .line 856120
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x33

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 856121
    :cond_0
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856122
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->f:Ljava/lang/String;

    .line 856123
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856124
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 856125
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttributionApp"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856126
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->i:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->i:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    .line 856127
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->i:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856128
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->j:Ljava/lang/String;

    .line 856129
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLAudioAvailability;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856074
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->k:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAudioAvailability;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->k:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    .line 856075
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->k:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856112
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->m:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->m:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 856113
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->m:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 855752
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->r:Ljava/lang/String;

    .line 855753
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->r:Ljava/lang/String;

    return-object v0
.end method

.method private r()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCopyrightBlockInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856000
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 856001
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->s:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private s()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCreationStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 855998
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->u:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->u:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    .line 855999
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->u:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    return-object v0
.end method

.method private t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 855996
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->w:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->w:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 855997
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->w:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 855994
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 855995
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    return-object v0
.end method

.method private v()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 855992
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->z:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->z:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 855993
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->z:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 855990
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855991
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 855988
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->G:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->G:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855989
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->G:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 855986
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855987
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 855984
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855985
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 42

    .prologue
    .line 855861
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 855862
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 855863
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->k()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 855864
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 855865
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->m()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 855866
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->n()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 855867
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->o()Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 855868
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->p()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 855869
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->q()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 855870
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->r()LX/1vs;

    move-result-object v12

    iget-object v13, v12, LX/1vs;->a:LX/15i;

    iget v12, v12, LX/1vs;->b:I

    const v14, -0x2246275c

    invoke-static {v13, v12, v14}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 855871
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->s()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 855872
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 855873
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 855874
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->v()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 855875
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->d()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 855876
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 855877
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 855878
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->y()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 855879
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 855880
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 855881
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->B()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 855882
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->C()LX/0Px;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v24

    .line 855883
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 855884
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->E()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 855885
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->F()LX/1vs;

    move-result-object v27

    move-object/from16 v0, v27

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    iget v0, v0, LX/1vs;->b:I

    move/from16 v27, v0

    const v29, -0x412840f7

    move-object/from16 v0, v28

    move/from16 v1, v27

    move/from16 v2, v29

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 855886
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->G()LX/0Px;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v28

    .line 855887
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->H()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 855888
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->I()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    .line 855889
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->J()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 855890
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->K()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    .line 855891
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->L()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    .line 855892
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->M()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v34

    .line 855893
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->N()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 855894
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->O()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    .line 855895
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->P()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 855896
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->Q()LX/0Px;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v38

    .line 855897
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->R()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 855898
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->S()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v40

    .line 855899
    const/16 v41, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 855900
    const/16 v41, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 855901
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 855902
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 855903
    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->h:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855904
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 855905
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 855906
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 855907
    const/4 v4, 0x7

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->l:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855908
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 855909
    const/16 v4, 0x9

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->n:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855910
    const/16 v4, 0xa

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->o:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855911
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855912
    const/16 v4, 0xc

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855913
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 855914
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 855915
    const/16 v5, 0xf

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->t:J

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 855916
    const/16 v4, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 855917
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->v:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855918
    const/16 v4, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 855919
    const/16 v4, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 855920
    const/16 v5, 0x14

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->y:D

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 855921
    const/16 v4, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855922
    const/16 v4, 0x16

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->A:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855923
    const/16 v4, 0x17

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->B:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855924
    const/16 v4, 0x18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->C:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855925
    const/16 v4, 0x19

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->D:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855926
    const/16 v4, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855927
    const/16 v4, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855928
    const/16 v4, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855929
    const/16 v4, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855930
    const/16 v4, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855931
    const/16 v4, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855932
    const/16 v4, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855933
    const/16 v4, 0x21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->L:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855934
    const/16 v4, 0x22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->M:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855935
    const/16 v4, 0x23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->N:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855936
    const/16 v4, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855937
    const/16 v4, 0x25

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->P:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855938
    const/16 v4, 0x26

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->Q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855939
    const/16 v4, 0x27

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->R:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855940
    const/16 v4, 0x28

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->S:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855941
    const/16 v4, 0x29

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->T:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855942
    const/16 v4, 0x2a

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->U:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855943
    const/16 v4, 0x2b

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->V:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855944
    const/16 v4, 0x2c

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->W:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855945
    const/16 v4, 0x2d

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->X:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855946
    const/16 v4, 0x2e

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->Y:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855947
    const/16 v4, 0x2f

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->Z:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855948
    const/16 v4, 0x30

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aa:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855949
    const/16 v4, 0x31

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ab:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855950
    const/16 v4, 0x32

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ac:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855951
    const/16 v4, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855952
    const/16 v5, 0x34

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ae:D

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 855953
    const/16 v4, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855954
    const/16 v4, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855955
    const/16 v4, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855956
    const/16 v4, 0x38

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ai:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855957
    const/16 v4, 0x39

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855958
    const/16 v4, 0x3a

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ak:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855959
    const/16 v4, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855960
    const/16 v4, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855961
    const/16 v4, 0x3d

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->an:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855962
    const/16 v4, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855963
    const/16 v4, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855964
    const/16 v4, 0x40

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855965
    const/16 v4, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855966
    const/16 v4, 0x42

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->as:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855967
    const/16 v4, 0x43

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->at:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855968
    const/16 v4, 0x44

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->au:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855969
    const/16 v4, 0x45

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->av:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855970
    const/16 v5, 0x46

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aw:D

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 855971
    const/16 v5, 0x47

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ax:D

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 855972
    const/16 v4, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855973
    const/16 v4, 0x49

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855974
    const/16 v4, 0x4a

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aA:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855975
    const/16 v4, 0x4b

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aB:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 855976
    const/16 v4, 0x4c

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aC:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855977
    const/16 v4, 0x4d

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855978
    const/16 v4, 0x4e

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855979
    const/16 v4, 0x4f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aF:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855980
    const/16 v4, 0x50

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 855981
    const/16 v4, 0x51

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aH:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 855982
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 855983
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    return v4
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 855755
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 855756
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 855757
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855758
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 855759
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855760
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855761
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->m()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 855762
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->m()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    .line 855763
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->m()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 855764
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855765
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->i:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    .line 855766
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->r()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 855767
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->r()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2246275c

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 855768
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->r()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 855769
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855770
    iput v3, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->s:I

    move-object v1, v0

    .line 855771
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->s()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 855772
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->s()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    .line 855773
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->s()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 855774
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855775
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->u:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$CreationStoryModel;

    .line 855776
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 855777
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 855778
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->t()Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 855779
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855780
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->w:Lcom/facebook/api/graphql/attachmenttarget/NewsFeedAttachmentTargetFieldsModels$NewsFeedAttachmentTargetMediaFeedbackModel;

    .line 855781
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 855782
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 855783
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 855784
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855785
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 855786
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->v()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 855787
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->v()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 855788
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->v()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 855789
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855790
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->z:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 855791
    :cond_6
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 855792
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855793
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 855794
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855795
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855796
    :cond_7
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 855797
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855798
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 855799
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855800
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->G:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855801
    :cond_8
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->y()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 855802
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->y()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855803
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->y()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 855804
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855805
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855806
    :cond_9
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 855807
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855808
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 855809
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855810
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855811
    :cond_a
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 855812
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855813
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 855814
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855815
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855816
    :cond_b
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->B()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 855817
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->B()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855818
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->B()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 855819
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855820
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->K:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 855821
    :cond_c
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->C()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 855822
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->C()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 855823
    if-eqz v2, :cond_d

    .line 855824
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855825
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->O:Ljava/util/List;

    move-object v1, v0

    .line 855826
    :cond_d
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 855827
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 855828
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 855829
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855830
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ad:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 855831
    :cond_e
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->E()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 855832
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->E()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;

    .line 855833
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->E()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 855834
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855835
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->af:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$AttachmentMediaModel$OwnerModel;

    .line 855836
    :cond_f
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->F()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_10

    .line 855837
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->F()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x412840f7

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/feed/NewsFeedDefaultsMediaGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 855838
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->F()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_10

    .line 855839
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855840
    iput v3, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ag:I

    move-object v1, v0

    .line 855841
    :cond_10
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->G()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 855842
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->G()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 855843
    if-eqz v2, :cond_11

    .line 855844
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855845
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ah:Ljava/util/List;

    move-object v1, v0

    .line 855846
    :cond_11
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->N()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 855847
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->N()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 855848
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->N()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 855849
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855850
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ar:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 855851
    :cond_12
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->R()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 855852
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->R()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    .line 855853
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->R()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 855854
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    .line 855855
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aE:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    .line 855856
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 855857
    if-nez v1, :cond_14

    :goto_0
    return-object p0

    .line 855858
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 855859
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_14
    move-object p0, v1

    .line 855860
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 855754
    new-instance v0, LX/5A6;

    invoke-direct {v0, p1}, LX/5A6;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 855751
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 856004
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 856005
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->h:I

    .line 856006
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->l:I

    .line 856007
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->n:Z

    .line 856008
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->o:Z

    .line 856009
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->p:Z

    .line 856010
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->q:Z

    .line 856011
    const/16 v0, 0xe

    const v1, -0x2246275c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->s:I

    .line 856012
    const/16 v0, 0xf

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->t:J

    .line 856013
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->v:Z

    .line 856014
    const/16 v0, 0x14

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->y:D

    .line 856015
    const/16 v0, 0x16

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->A:Z

    .line 856016
    const/16 v0, 0x17

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->B:I

    .line 856017
    const/16 v0, 0x18

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->C:I

    .line 856018
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->D:I

    .line 856019
    const/16 v0, 0x21

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->L:I

    .line 856020
    const/16 v0, 0x22

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->M:I

    .line 856021
    const/16 v0, 0x23

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->N:I

    .line 856022
    const/16 v0, 0x25

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->P:Z

    .line 856023
    const/16 v0, 0x26

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->Q:Z

    .line 856024
    const/16 v0, 0x27

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->R:Z

    .line 856025
    const/16 v0, 0x28

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->S:Z

    .line 856026
    const/16 v0, 0x29

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->T:Z

    .line 856027
    const/16 v0, 0x2a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->U:Z

    .line 856028
    const/16 v0, 0x2b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->V:Z

    .line 856029
    const/16 v0, 0x2c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->W:Z

    .line 856030
    const/16 v0, 0x2d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->X:Z

    .line 856031
    const/16 v0, 0x2e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->Y:Z

    .line 856032
    const/16 v0, 0x2f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->Z:Z

    .line 856033
    const/16 v0, 0x30

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aa:Z

    .line 856034
    const/16 v0, 0x31

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ab:I

    .line 856035
    const/16 v0, 0x32

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ac:I

    .line 856036
    const/16 v0, 0x34

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ae:D

    .line 856037
    const/16 v0, 0x36

    const v1, -0x412840f7

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ag:I

    .line 856038
    const/16 v0, 0x38

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ai:I

    .line 856039
    const/16 v0, 0x3a

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ak:I

    .line 856040
    const/16 v0, 0x3d

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->an:I

    .line 856041
    const/16 v0, 0x42

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->as:Z

    .line 856042
    const/16 v0, 0x43

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->at:Z

    .line 856043
    const/16 v0, 0x44

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->au:Z

    .line 856044
    const/16 v0, 0x45

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->av:Z

    .line 856045
    const/16 v0, 0x46

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aw:D

    .line 856046
    const/16 v0, 0x47

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->ax:D

    .line 856047
    const/16 v0, 0x4a

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aA:I

    .line 856048
    const/16 v0, 0x4b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aB:Z

    .line 856049
    const/16 v0, 0x4c

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aC:I

    .line 856050
    const/16 v0, 0x4f

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aF:I

    .line 856051
    const/16 v0, 0x51

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->aH:I

    .line 856052
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 856053
    invoke-virtual {p2}, LX/18L;->a()V

    .line 856054
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 856055
    const-string v0, "message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 856056
    check-cast p2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->a(Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;)V

    .line 856057
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 856058
    return-void
.end method

.method public final synthetic ai_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856059
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aj_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856060
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856061
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 856062
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 856063
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 856064
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;-><init>()V

    .line 856065
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 856066
    return-object v0
.end method

.method public final synthetic c()LX/1f8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856067
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856068
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->E:Ljava/lang/String;

    const/16 v1, 0x1a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->E:Ljava/lang/String;

    .line 856069
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->E:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 856070
    const v0, 0x2015b9af

    return v0
.end method

.method public final synthetic e()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856071
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 856072
    const v0, 0x46c7fc4

    return v0
.end method

.method public final synthetic j()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 856073
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$StoryAttachmentFieldsModel$MediaModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method
