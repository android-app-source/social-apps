.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 852068
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 852067
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 852041
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 852042
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 852065
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 852066
    iget v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 852060
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 852061
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 852062
    iget v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 852063
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 852064
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 852057
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 852058
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 852059
    return-object p0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 852051
    iput p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;->e:I

    .line 852052
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 852053
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 852054
    if-eqz v0, :cond_0

    .line 852055
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 852056
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 852048
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 852049
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;->e:I

    .line 852050
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 852045
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;-><init>()V

    .line 852046
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 852047
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 852044
    const v0, 0x6c06b459

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 852043
    const v0, -0x2cdb5330

    return v0
.end method
