.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 852547
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    new-instance v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 852548
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 852553
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 852550
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 852551
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/59Z;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 852552
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 852549
    check-cast p1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel$Serializer;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
