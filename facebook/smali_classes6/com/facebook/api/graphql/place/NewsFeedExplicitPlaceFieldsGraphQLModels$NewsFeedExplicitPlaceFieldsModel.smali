.class public final Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/5CI;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x118d75e3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Z

.field private p:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Z

.field private r:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 867601
    const-class v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 867602
    const-class v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 867603
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 867604
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 4

    .prologue
    .line 867605
    iput-object p1, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->t:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 867606
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 867607
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 867608
    if-eqz v0, :cond_0

    .line 867609
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0xf

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 867610
    :cond_0
    return-void

    .line 867611
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 867612
    iput-object p1, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->k:Ljava/lang/String;

    .line 867613
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 867614
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 867615
    if-eqz v0, :cond_0

    .line 867616
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 867617
    :cond_0
    return-void
.end method

.method private u()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867618
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->g:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->g:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    .line 867619
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->g:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867620
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 867621
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867622
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 867623
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private x()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867730
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->p:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->p:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    .line 867731
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->p:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 867624
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 867625
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 867626
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->c()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 867627
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->u()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 867628
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->e()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 867629
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 867630
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->j()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 867631
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->r()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, 0x34c84503

    invoke-static {v8, v7, v9}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 867632
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->s()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    const v10, -0x33df0b5b    # -4.2193556E7f

    invoke-static {v9, v8, v10}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 867633
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 867634
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->x()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 867635
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->o()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 867636
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->p()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 867637
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->q()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    .line 867638
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->t()LX/1vs;

    move-result-object v14

    iget-object v15, v14, LX/1vs;->a:LX/15i;

    iget v14, v14, LX/1vs;->b:I

    const v16, 0x471b9538

    move/from16 v0, v16

    invoke-static {v15, v14, v0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 867639
    const/16 v15, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->c(I)V

    .line 867640
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v1}, LX/186;->b(II)V

    .line 867641
    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 867642
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 867643
    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 867644
    const/4 v1, 0x4

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 867645
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 867646
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 867647
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 867648
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 867649
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 867650
    const/16 v1, 0xa

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->o:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 867651
    const/16 v1, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 867652
    const/16 v1, 0xc

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 867653
    const/16 v1, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 867654
    const/16 v1, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 867655
    const/16 v1, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 867656
    const/16 v1, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v14}, LX/186;->b(II)V

    .line 867657
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 867658
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    return v1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 867688
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 867689
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->u()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 867690
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->u()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    .line 867691
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->u()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 867692
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;

    .line 867693
    iput-object v0, v1, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->g:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    .line 867694
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 867695
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 867696
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 867697
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;

    .line 867698
    iput-object v0, v1, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 867699
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->r()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 867700
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->r()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x34c84503

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 867701
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->r()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 867702
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;

    .line 867703
    iput v3, v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->l:I

    move-object v1, v0

    .line 867704
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->s()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 867705
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->s()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x33df0b5b    # -4.2193556E7f

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 867706
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->s()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 867707
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;

    .line 867708
    iput v3, v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->m:I

    move-object v1, v0

    .line 867709
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 867710
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 867711
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 867712
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;

    .line 867713
    iput-object v0, v1, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 867714
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->x()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 867715
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->x()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    .line 867716
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->x()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 867717
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;

    .line 867718
    iput-object v0, v1, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->p:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    .line 867719
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->t()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 867720
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->t()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x471b9538

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 867721
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->t()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 867722
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;

    .line 867723
    iput v3, v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->u:I

    move-object v1, v0

    .line 867724
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 867725
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    .line 867726
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 867727
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 867728
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_7
    move-object p0, v1

    .line 867729
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 867572
    new-instance v0, LX/5CN;

    invoke-direct {v0, p1}, LX/5CN;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867687
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 867679
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 867680
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->i:Z

    .line 867681
    const/4 v0, 0x7

    const v1, 0x34c84503

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->l:I

    .line 867682
    const/16 v0, 0x8

    const v1, -0x33df0b5b    # -4.2193556E7f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->m:I

    .line 867683
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->o:Z

    .line 867684
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->q:Z

    .line 867685
    const/16 v0, 0x10

    const v1, 0x471b9538

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->u:I

    .line 867686
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 867669
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 867670
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 867671
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 867672
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    .line 867673
    :goto_0
    return-void

    .line 867674
    :cond_0
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 867675
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->q()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 867676
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 867677
    const/16 v0, 0xf

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 867678
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 867664
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 867665
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->a(Ljava/lang/String;)V

    .line 867666
    :cond_0
    :goto_0
    return-void

    .line 867667
    :cond_1
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 867668
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    goto :goto_0
.end method

.method public final ae_()Z
    .locals 2

    .prologue
    .line 867662
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 867663
    iget-boolean v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->i:Z

    return v0
.end method

.method public final synthetic af_()LX/1k1;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867600
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867659
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 867660
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 867661
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 867569
    new-instance v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;-><init>()V

    .line 867570
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 867571
    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 867573
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->f:Ljava/util/List;

    .line 867574
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867575
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->u()Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 867576
    const v0, -0x41fdc685

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867577
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->h:Ljava/lang/String;

    .line 867578
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 867579
    const v0, 0x499e8e7

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867580
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->k:Ljava/lang/String;

    .line 867581
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic k()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867582
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 867583
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 867584
    iget-boolean v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->o:Z

    return v0
.end method

.method public final synthetic m()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867585
    invoke-direct {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->x()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 867586
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 867587
    iget-boolean v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->q:Z

    return v0
.end method

.method public final o()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867588
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->r:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    iput-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->r:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 867589
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->r:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867590
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->s:Ljava/lang/String;

    .line 867591
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867592
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->t:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->t:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 867593
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->t:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final r()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOverallStarRating"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867594
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 867595
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->l:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final s()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageVisits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867596
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 867597
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->m:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerVisits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867598
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 867599
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;->u:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
