.class public final Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xa048060
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 867252
    const-class v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 867251
    const-class v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 867249
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 867250
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 867246
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 867247
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 867248
    return-void
.end method

.method public static a(Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;
    .locals 8

    .prologue
    .line 867226
    if-nez p0, :cond_0

    .line 867227
    const/4 p0, 0x0

    .line 867228
    :goto_0
    return-object p0

    .line 867229
    :cond_0
    instance-of v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    if-eqz v0, :cond_1

    .line 867230
    check-cast p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    goto :goto_0

    .line 867231
    :cond_1
    new-instance v0, LX/5CK;

    invoke-direct {v0}, LX/5CK;-><init>()V

    .line 867232
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5CK;->a:Ljava/lang/String;

    .line 867233
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 867234
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 867235
    iget-object v3, v0, LX/5CK;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 867236
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 867237
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 867238
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 867239
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 867240
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 867241
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 867242
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 867243
    new-instance v3, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    invoke-direct {v3, v2}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;-><init>(LX/15i;)V

    .line 867244
    move-object p0, v3

    .line 867245
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 867206
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 867207
    invoke-virtual {p0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 867208
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 867209
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 867210
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 867211
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 867223
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 867224
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 867225
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 867222
    new-instance v0, LX/5CL;

    invoke-direct {v0, p1}, LX/5CL;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 867220
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;->e:Ljava/lang/String;

    .line 867221
    iget-object v0, p0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 867218
    invoke-virtual {p2}, LX/18L;->a()V

    .line 867219
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 867217
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 867214
    new-instance v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;-><init>()V

    .line 867215
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 867216
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 867213
    const v0, 0x7e8624a4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 867212
    const v0, 0x25d6af

    return v0
.end method
