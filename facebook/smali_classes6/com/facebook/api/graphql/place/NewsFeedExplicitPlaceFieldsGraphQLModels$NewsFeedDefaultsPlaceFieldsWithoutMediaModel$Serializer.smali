.class public final Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 867268
    const-class v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel;

    new-instance v1, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 867269
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 867270
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 867271
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 867272
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0xd

    const/16 v4, 0xb

    const/4 v3, 0x0

    .line 867273
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 867274
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 867275
    if-eqz v2, :cond_0

    .line 867276
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867277
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 867278
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 867279
    if-eqz v2, :cond_1

    .line 867280
    const-string v3, "city"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867281
    invoke-static {v1, v2, p1}, LX/5CP;->a(LX/15i;ILX/0nX;)V

    .line 867282
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 867283
    if-eqz v2, :cond_2

    .line 867284
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867285
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 867286
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 867287
    if-eqz v2, :cond_3

    .line 867288
    const-string v3, "is_owned"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867289
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 867290
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 867291
    if-eqz v2, :cond_4

    .line 867292
    const-string v3, "location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867293
    invoke-static {v1, v2, p1}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 867294
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 867295
    if-eqz v2, :cond_5

    .line 867296
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867297
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 867298
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 867299
    if-eqz v2, :cond_6

    .line 867300
    const-string v3, "overall_star_rating"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867301
    invoke-static {v1, v2, p1}, LX/5CQ;->a(LX/15i;ILX/0nX;)V

    .line 867302
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 867303
    if-eqz v2, :cond_7

    .line 867304
    const-string v3, "page_visits"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867305
    invoke-static {v1, v2, p1}, LX/5CR;->a(LX/15i;ILX/0nX;)V

    .line 867306
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 867307
    if-eqz v2, :cond_8

    .line 867308
    const-string v3, "profile_picture_is_silhouette"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867309
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 867310
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 867311
    if-eqz v2, :cond_9

    .line 867312
    const-string v3, "saved_collection"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867313
    invoke-static {v1, v2, p1, p2}, LX/40i;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 867314
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 867315
    if-eqz v2, :cond_a

    .line 867316
    const-string v3, "should_show_reviews_on_profile"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867317
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 867318
    :cond_a
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 867319
    if-eqz v2, :cond_b

    .line 867320
    const-string v2, "super_category_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867321
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 867322
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 867323
    if-eqz v2, :cond_c

    .line 867324
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867325
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 867326
    :cond_c
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 867327
    if-eqz v2, :cond_d

    .line 867328
    const-string v2, "viewer_saved_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867329
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 867330
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 867331
    if-eqz v2, :cond_e

    .line 867332
    const-string v3, "viewer_visits"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867333
    invoke-static {v1, v2, p1}, LX/5CS;->a(LX/15i;ILX/0nX;)V

    .line 867334
    :cond_e
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 867335
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 867336
    check-cast p1, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$Serializer;->a(Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel;LX/0nX;LX/0my;)V

    return-void
.end method
