.class public final Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 867567
    const-class v0, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 867568
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 867566
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 867492
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 867493
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0xf

    const/16 v5, 0xd

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 867494
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 867495
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 867496
    if-eqz v2, :cond_0

    .line 867497
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867498
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 867499
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 867500
    if-eqz v2, :cond_1

    .line 867501
    const-string v2, "category_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867502
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 867503
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 867504
    if-eqz v2, :cond_2

    .line 867505
    const-string v3, "city"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867506
    invoke-static {v1, v2, p1}, LX/5CP;->a(LX/15i;ILX/0nX;)V

    .line 867507
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 867508
    if-eqz v2, :cond_3

    .line 867509
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867510
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 867511
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 867512
    if-eqz v2, :cond_4

    .line 867513
    const-string v3, "is_owned"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867514
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 867515
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 867516
    if-eqz v2, :cond_5

    .line 867517
    const-string v3, "location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867518
    invoke-static {v1, v2, p1}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 867519
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 867520
    if-eqz v2, :cond_6

    .line 867521
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867522
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 867523
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 867524
    if-eqz v2, :cond_7

    .line 867525
    const-string v3, "overall_star_rating"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867526
    invoke-static {v1, v2, p1}, LX/5CQ;->a(LX/15i;ILX/0nX;)V

    .line 867527
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 867528
    if-eqz v2, :cond_8

    .line 867529
    const-string v3, "page_visits"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867530
    invoke-static {v1, v2, p1}, LX/5CR;->a(LX/15i;ILX/0nX;)V

    .line 867531
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 867532
    if-eqz v2, :cond_9

    .line 867533
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867534
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 867535
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 867536
    if-eqz v2, :cond_a

    .line 867537
    const-string v3, "profile_picture_is_silhouette"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867538
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 867539
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 867540
    if-eqz v2, :cond_b

    .line 867541
    const-string v3, "saved_collection"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867542
    invoke-static {v1, v2, p1, p2}, LX/40i;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 867543
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 867544
    if-eqz v2, :cond_c

    .line 867545
    const-string v3, "should_show_reviews_on_profile"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867546
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 867547
    :cond_c
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 867548
    if-eqz v2, :cond_d

    .line 867549
    const-string v2, "super_category_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867550
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 867551
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 867552
    if-eqz v2, :cond_e

    .line 867553
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867554
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 867555
    :cond_e
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 867556
    if-eqz v2, :cond_f

    .line 867557
    const-string v2, "viewer_saved_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867558
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 867559
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 867560
    if-eqz v2, :cond_10

    .line 867561
    const-string v3, "viewer_visits"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 867562
    invoke-static {v1, v2, p1}, LX/5CS;->a(LX/15i;ILX/0nX;)V

    .line 867563
    :cond_10
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 867564
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 867565
    check-cast p1, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedExplicitPlaceFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
