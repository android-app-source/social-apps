.class public final Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x11423724
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 857895
    const-class v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 857860
    const-class v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 857861
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 857862
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 857863
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 857864
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 857865
    return-void
.end method

.method public static a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;)Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;
    .locals 11

    .prologue
    .line 857866
    if-nez p0, :cond_0

    .line 857867
    const/4 p0, 0x0

    .line 857868
    :goto_0
    return-object p0

    .line 857869
    :cond_0
    instance-of v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;

    if-eqz v0, :cond_1

    .line 857870
    check-cast p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;

    goto :goto_0

    .line 857871
    :cond_1
    new-instance v0, LX/5AQ;

    invoke-direct {v0}, LX/5AQ;-><init>()V

    .line 857872
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/5AQ;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 857873
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5AQ;->b:Ljava/lang/String;

    .line 857874
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5AQ;->c:Ljava/lang/String;

    .line 857875
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->e()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/5AQ;->d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 857876
    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 857877
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 857878
    iget-object v3, v0, LX/5AQ;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 857879
    iget-object v5, v0, LX/5AQ;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 857880
    iget-object v7, v0, LX/5AQ;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 857881
    iget-object v8, v0, LX/5AQ;->d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 857882
    const/4 v9, 0x4

    invoke-virtual {v2, v9}, LX/186;->c(I)V

    .line 857883
    invoke-virtual {v2, v10, v3}, LX/186;->b(II)V

    .line 857884
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 857885
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 857886
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 857887
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 857888
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 857889
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 857890
    invoke-virtual {v3, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 857891
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 857892
    new-instance v3, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;

    invoke-direct {v3, v2}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;-><init>(LX/15i;)V

    .line 857893
    move-object p0, v3

    .line 857894
    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 857916
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 857917
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 857896
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 857897
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 857898
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 857899
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 857900
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 857901
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 857902
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 857903
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 857904
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 857905
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 857906
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 857907
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 857908
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 857909
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 857910
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 857911
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 857912
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;

    .line 857913
    iput-object v0, v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 857914
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 857915
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 857858
    new-instance v0, LX/5AR;

    invoke-direct {v0, p1}, LX/5AR;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 857859
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 857842
    invoke-virtual {p2}, LX/18L;->a()V

    .line 857843
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 857844
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 857845
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 857846
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 857847
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 857855
    new-instance v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;-><init>()V

    .line 857856
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 857857
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 857848
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->f:Ljava/lang/String;

    .line 857849
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 857850
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->g:Ljava/lang/String;

    .line 857851
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 857852
    const v0, 0x70e71a42

    return v0
.end method

.method public final synthetic e()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 857853
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 857854
    const v0, 0x3c2b9d5

    return v0
.end method
