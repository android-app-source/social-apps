.class public final Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/5AI;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x68880daa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:I

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Z

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Z

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 858634
    const-class v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 858601
    const-class v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 858602
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 858603
    return-void
.end method

.method private A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858604
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 858605
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    return-object v0
.end method

.method private B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858606
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 858607
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    return-object v0
.end method

.method private C()I
    .locals 2

    .prologue
    .line 858608
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 858609
    iget v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->D:I

    return v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 858610
    iput p1, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->D:I

    .line 858611
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 858612
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 858613
    if-eqz v0, :cond_0

    .line 858614
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x19

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 858615
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 858616
    iput-object p1, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 858617
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 858618
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 858619
    if-eqz v0, :cond_0

    .line 858620
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 858621
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 858622
    iput-object p1, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 858623
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 858624
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 858625
    if-eqz v0, :cond_0

    .line 858626
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 858627
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 858628
    iput-object p1, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 858629
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 858630
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 858631
    if-eqz v0, :cond_0

    .line 858632
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 858633
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 858635
    iput-object p1, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 858636
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 858637
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 858638
    if-eqz v0, :cond_0

    .line 858639
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 858640
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 858643
    iput-boolean p1, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->g:Z

    .line 858644
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 858645
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 858646
    if-eqz v0, :cond_0

    .line 858647
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 858648
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 858675
    iput-boolean p1, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->k:Z

    .line 858676
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 858677
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 858678
    if-eqz v0, :cond_0

    .line 858679
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 858680
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 858669
    iput-boolean p1, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->o:Z

    .line 858670
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 858671
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 858672
    if-eqz v0, :cond_0

    .line 858673
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 858674
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 858663
    iput-boolean p1, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->r:Z

    .line 858664
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 858665
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 858666
    if-eqz v0, :cond_0

    .line 858667
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 858668
    :cond_0
    return-void
.end method

.method private s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858661
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->q:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->q:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 858662
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->q:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    return-object v0
.end method

.method private t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858659
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 858660
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    return-object v0
.end method

.method private u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858657
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 858658
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    return-object v0
.end method

.method private v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858655
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->v:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->v:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    .line 858656
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->v:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    return-object v0
.end method

.method private w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858653
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    .line 858654
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    return-object v0
.end method

.method private x()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 858651
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->y:Ljava/util/List;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->y:Ljava/util/List;

    .line 858652
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->y:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858649
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 858650
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    return-object v0
.end method

.method private z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858641
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 858642
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 19

    .prologue
    .line 858504
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 858505
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->l()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 858506
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->n()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 858507
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 858508
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->p()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 858509
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 858510
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 858511
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 858512
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->q()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 858513
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 858514
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->x()LX/0Px;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v12

    .line 858515
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 858516
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 858517
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 858518
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 858519
    const/16 v17, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 858520
    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->e:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 858521
    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->f:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 858522
    const/16 v17, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->g:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 858523
    const/16 v17, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->h:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 858524
    const/16 v17, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->i:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 858525
    const/16 v17, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->j:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 858526
    const/16 v17, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->k:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 858527
    const/16 v17, 0x7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->l:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 858528
    const/16 v17, 0x8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->m:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 858529
    const/16 v17, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 858530
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->o:Z

    move/from16 v17, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 858531
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 858532
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 858533
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 858534
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 858535
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 858536
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 858537
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 858538
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 858539
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 858540
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 858541
    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 858542
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 858543
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 858544
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 858545
    const/16 v3, 0x19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->D:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 858546
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 858547
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 858548
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 858549
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 858550
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 858551
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->s()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 858552
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    .line 858553
    iput-object v0, v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->q:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 858554
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 858555
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 858556
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 858557
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    .line 858558
    iput-object v0, v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 858559
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 858560
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 858561
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 858562
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    .line 858563
    iput-object v0, v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 858564
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 858565
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    .line 858566
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 858567
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    .line 858568
    iput-object v0, v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->v:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    .line 858569
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 858570
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    .line 858571
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 858572
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    .line 858573
    iput-object v0, v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    .line 858574
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->x()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 858575
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->x()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 858576
    if-eqz v2, :cond_5

    .line 858577
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    .line 858578
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->y:Ljava/util/List;

    move-object v1, v0

    .line 858579
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 858580
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 858581
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 858582
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    .line 858583
    iput-object v0, v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 858584
    :cond_6
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 858585
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 858586
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 858587
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    .line 858588
    iput-object v0, v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 858589
    :cond_7
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 858590
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 858591
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 858592
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    .line 858593
    iput-object v0, v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 858594
    :cond_8
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 858595
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 858596
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 858597
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    .line 858598
    iput-object v0, v1, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 858599
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 858600
    if-nez v1, :cond_a

    :goto_0
    return-object p0

    :cond_a
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 858344
    new-instance v0, LX/5AT;

    invoke-direct {v0, p1}, LX/5AT;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858482
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 858468
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 858469
    invoke-virtual {p1, p2, v1}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->e:Z

    .line 858470
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->f:Z

    .line 858471
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->g:Z

    .line 858472
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->h:Z

    .line 858473
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->i:Z

    .line 858474
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->j:Z

    .line 858475
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->k:Z

    .line 858476
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->l:Z

    .line 858477
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->m:Z

    .line 858478
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->o:Z

    .line 858479
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->r:Z

    .line 858480
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->D:I

    .line 858481
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 858416
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 858417
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 858418
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 858419
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 858420
    :goto_0
    return-void

    .line 858421
    :cond_0
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 858422
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 858423
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 858424
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 858425
    :cond_1
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 858426
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 858427
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 858428
    const/16 v0, 0xa

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 858429
    :cond_2
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 858430
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 858431
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 858432
    const/16 v0, 0xd

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 858433
    :cond_3
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 858434
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    .line 858435
    if-eqz v0, :cond_9

    .line 858436
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 858437
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 858438
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 858439
    :cond_4
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 858440
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 858441
    if-eqz v0, :cond_9

    .line 858442
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 858443
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 858444
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 858445
    :cond_5
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 858446
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    .line 858447
    if-eqz v0, :cond_9

    .line 858448
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 858449
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 858450
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 858451
    :cond_6
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 858452
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 858453
    if-eqz v0, :cond_9

    .line 858454
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 858455
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 858456
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 858457
    :cond_7
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 858458
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 858459
    if-eqz v0, :cond_9

    .line 858460
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 858461
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 858462
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 858463
    :cond_8
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 858464
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->C()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 858465
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 858466
    const/16 v0, 0x19

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 858467
    :cond_9
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 858407
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 858408
    check-cast p2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;)V

    .line 858409
    :cond_0
    :goto_0
    return-void

    .line 858410
    :cond_1
    const-string v0, "reactors"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 858411
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)V

    goto :goto_0

    .line 858412
    :cond_2
    const-string v0, "top_level_comments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 858413
    check-cast p2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;)V

    goto :goto_0

    .line 858414
    :cond_3
    const-string v0, "top_reactions"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 858415
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 858356
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 858357
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->a(Z)V

    .line 858358
    :cond_0
    :goto_0
    return-void

    .line 858359
    :cond_1
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 858360
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->b(Z)V

    goto :goto_0

    .line 858361
    :cond_2
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 858362
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->c(Z)V

    goto :goto_0

    .line 858363
    :cond_3
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 858364
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->d(Z)V

    goto :goto_0

    .line 858365
    :cond_4
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 858366
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->t()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    .line 858367
    if-eqz v0, :cond_0

    .line 858368
    if-eqz p3, :cond_5

    .line 858369
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 858370
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;->a(I)V

    .line 858371
    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->t:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    goto :goto_0

    .line 858372
    :cond_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;->a(I)V

    goto :goto_0

    .line 858373
    :cond_6
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 858374
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 858375
    if-eqz v0, :cond_0

    .line 858376
    if-eqz p3, :cond_7

    .line 858377
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 858378
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    .line 858379
    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    goto/16 :goto_0

    .line 858380
    :cond_7
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    goto/16 :goto_0

    .line 858381
    :cond_8
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 858382
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->w()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    .line 858383
    if-eqz v0, :cond_0

    .line 858384
    if-eqz p3, :cond_9

    .line 858385
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    .line 858386
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;->a(I)V

    .line 858387
    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    goto/16 :goto_0

    .line 858388
    :cond_9
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;->a(I)V

    goto/16 :goto_0

    .line 858389
    :cond_a
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 858390
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 858391
    if-eqz v0, :cond_0

    .line 858392
    if-eqz p3, :cond_b

    .line 858393
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 858394
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->a(I)V

    .line 858395
    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    goto/16 :goto_0

    .line 858396
    :cond_b
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->a(I)V

    goto/16 :goto_0

    .line 858397
    :cond_c
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 858398
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 858399
    if-eqz v0, :cond_0

    .line 858400
    if-eqz p3, :cond_d

    .line 858401
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 858402
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->b(I)V

    .line 858403
    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    goto/16 :goto_0

    .line 858404
    :cond_d
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->b(I)V

    goto/16 :goto_0

    .line 858405
    :cond_e
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 858406
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->a(I)V

    goto/16 :goto_0
.end method

.method public final ac_()Z
    .locals 2

    .prologue
    .line 858354
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 858355
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->i:Z

    return v0
.end method

.method public final ad_()Z
    .locals 2

    .prologue
    .line 858352
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 858353
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->j:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 858349
    new-instance v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;-><init>()V

    .line 858350
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 858351
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 858347
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 858348
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 858345
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 858346
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->f:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 858342
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 858343
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 858483
    const v0, 0x7c083f1a

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 858484
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 858485
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 858503
    const v0, -0x78fb05b

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 858486
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 858487
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->k:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 858488
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 858489
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->m:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858490
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->n:Ljava/lang/String;

    .line 858491
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 858492
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 858493
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->o:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858494
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->p:Ljava/lang/String;

    .line 858495
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 858496
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 858497
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->r:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858498
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->s:Ljava/lang/String;

    .line 858499
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858500
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->w:Ljava/lang/String;

    .line 858501
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic r()LX/59N;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 858502
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    return-object v0
.end method
