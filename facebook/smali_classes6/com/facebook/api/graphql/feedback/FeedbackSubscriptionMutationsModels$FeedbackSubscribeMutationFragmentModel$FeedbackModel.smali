.class public final Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1698f3df
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 859433
    const-class v0, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 859432
    const-class v0, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 859430
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 859431
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 859427
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 859428
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 859429
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 859421
    iput-boolean p1, p0, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->f:Z

    .line 859422
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 859423
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 859424
    if-eqz v0, :cond_0

    .line 859425
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 859426
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 859419
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->e:Ljava/lang/String;

    .line 859420
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 859384
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 859385
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->f:Z

    return v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 859417
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->g:Ljava/lang/String;

    .line 859418
    iget-object v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 859408
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 859409
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 859410
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 859411
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 859412
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 859413
    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->f:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 859414
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 859415
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 859416
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 859405
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 859406
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 859407
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 859404
    new-instance v0, LX/5Al;

    invoke-direct {v0, p1}, LX/5Al;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 859403
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 859400
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 859401
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->f:Z

    .line 859402
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 859394
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 859395
    invoke-direct {p0}, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 859396
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 859397
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 859398
    :goto_0
    return-void

    .line 859399
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 859391
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 859392
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;->a(Z)V

    .line 859393
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 859388
    new-instance v0, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;-><init>()V

    .line 859389
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 859390
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 859387
    const v0, 0x2b0f05d0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 859386
    const v0, -0x78fb05b

    return v0
.end method
