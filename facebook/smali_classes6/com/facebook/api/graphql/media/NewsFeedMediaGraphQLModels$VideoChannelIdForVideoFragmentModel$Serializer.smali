.class public final Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 866749
    const-class v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel;

    new-instance v1, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 866750
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 866751
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 866752
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 866753
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 866754
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 866755
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 866756
    if-eqz v2, :cond_0

    .line 866757
    const-string p0, "should_open_single_publisher"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866758
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 866759
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 866760
    if-eqz v2, :cond_1

    .line 866761
    const-string p0, "video_channel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866762
    invoke-static {v1, v2, p1}, LX/5CD;->a(LX/15i;ILX/0nX;)V

    .line 866763
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 866764
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 866765
    check-cast p1, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$Serializer;->a(Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
