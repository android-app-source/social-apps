.class public final Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 866719
    const-class v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel;

    new-instance v1, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 866720
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 866721
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 866722
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 866723
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 866724
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_6

    .line 866725
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 866726
    :goto_0
    move v1, v2

    .line 866727
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 866728
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 866729
    new-instance v1, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel;

    invoke-direct {v1}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel;-><init>()V

    .line 866730
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 866731
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 866732
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 866733
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 866734
    :cond_0
    return-object v1

    .line 866735
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_4

    .line 866736
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 866737
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 866738
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_1

    if-eqz v6, :cond_1

    .line 866739
    const-string p0, "should_open_single_publisher"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 866740
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v5, v1

    move v1, v3

    goto :goto_1

    .line 866741
    :cond_2
    const-string p0, "video_channel"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 866742
    invoke-static {p1, v0}, LX/5CD;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 866743
    :cond_3
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 866744
    :cond_4
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 866745
    if-eqz v1, :cond_5

    .line 866746
    invoke-virtual {v0, v2, v5}, LX/186;->a(IZ)V

    .line 866747
    :cond_5
    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 866748
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_6
    move v1, v2

    move v4, v2

    move v5, v2

    goto :goto_1
.end method
