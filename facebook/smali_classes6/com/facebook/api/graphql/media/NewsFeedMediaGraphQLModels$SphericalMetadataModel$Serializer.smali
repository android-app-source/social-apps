.class public final Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 866596
    const-class v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel;

    new-instance v1, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 866597
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 866595
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 866599
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 866600
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    .line 866601
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 866602
    invoke-virtual {v1, v0, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 866603
    if-eqz v2, :cond_0

    .line 866604
    const-string v3, "enable_focus"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866605
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 866606
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 866607
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_1

    .line 866608
    const-string v4, "focus_width_degrees"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866609
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 866610
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 866611
    if-eqz v2, :cond_2

    .line 866612
    const-string v3, "guided_tour"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866613
    invoke-static {v1, v2, p1, p2}, LX/5CB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 866614
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 866615
    if-eqz v2, :cond_3

    .line 866616
    const-string v3, "initial_view_heading_degrees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866617
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 866618
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 866619
    if-eqz v2, :cond_4

    .line 866620
    const-string v3, "initial_view_pitch_degrees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866621
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 866622
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 866623
    if-eqz v2, :cond_5

    .line 866624
    const-string v3, "initial_view_roll_degrees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866625
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 866626
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 866627
    if-eqz v2, :cond_6

    .line 866628
    const-string v3, "is_spherical"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866629
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 866630
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 866631
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_7

    .line 866632
    const-string v4, "off_focus_level"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866633
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 866634
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 866635
    if-eqz v2, :cond_8

    .line 866636
    const-string v3, "projection_type"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866637
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 866638
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 866639
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_9

    .line 866640
    const-string v4, "sphericalFullscreenAspectRatio"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866641
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 866642
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 866643
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_a

    .line 866644
    const-string v4, "sphericalInlineAspectRatio"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866645
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 866646
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 866647
    if-eqz v2, :cond_b

    .line 866648
    const-string v3, "sphericalPlayableUrlHdString"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866649
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 866650
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 866651
    if-eqz v2, :cond_c

    .line 866652
    const-string v3, "sphericalPlayableUrlSdString"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866653
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 866654
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 866655
    if-eqz v2, :cond_d

    .line 866656
    const-string v3, "sphericalPreferredFov"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866657
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 866658
    :cond_d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 866659
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 866598
    check-cast p1, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$Serializer;->a(Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel;LX/0nX;LX/0my;)V

    return-void
.end method
