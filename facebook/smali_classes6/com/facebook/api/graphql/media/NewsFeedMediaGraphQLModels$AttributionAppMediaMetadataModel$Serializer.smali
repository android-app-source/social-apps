.class public final Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 866319
    const-class v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel;

    new-instance v1, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 866320
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 866321
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 866322
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 866323
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 866324
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 866325
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 866326
    if-eqz v2, :cond_0

    .line 866327
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866328
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 866329
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 866330
    if-eqz v2, :cond_1

    .line 866331
    const-string p0, "attribution_app"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866332
    invoke-static {v1, v2, p1, p2}, LX/5C9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 866333
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 866334
    if-eqz v2, :cond_2

    .line 866335
    const-string p0, "attribution_app_metadata"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866336
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 866337
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 866338
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 866339
    check-cast p1, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$Serializer;->a(Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel;LX/0nX;LX/0my;)V

    return-void
.end method
