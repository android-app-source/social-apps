.class public final Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x23a334ca
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 866284
    const-class v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 866235
    const-class v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 866282
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 866283
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 866280
    iget-object v0, p0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->e:Ljava/lang/String;

    .line 866281
    iget-object v0, p0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 866278
    iget-object v0, p0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->f:Ljava/lang/String;

    .line 866279
    iget-object v0, p0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNativeStoreObject"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 866276
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 866277
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSquareLogo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 866274
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 866275
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 866262
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 866263
    invoke-direct {p0}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 866264
    invoke-direct {p0}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 866265
    invoke-direct {p0}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x69b6b47d

    invoke-static {v3, v2, v4}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 866266
    invoke-direct {p0}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x4ac1df68

    invoke-static {v4, v3, v5}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 866267
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 866268
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 866269
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 866270
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 866271
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 866272
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 866273
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 866246
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 866247
    invoke-direct {p0}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 866248
    invoke-direct {p0}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x69b6b47d

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 866249
    invoke-direct {p0}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 866250
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    .line 866251
    iput v3, v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->g:I

    move-object v1, v0

    .line 866252
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 866253
    invoke-direct {p0}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4ac1df68

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 866254
    invoke-direct {p0}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 866255
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    .line 866256
    iput v3, v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->h:I

    move-object v1, v0

    .line 866257
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 866258
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 866259
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 866260
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 866261
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 866245
    invoke-direct {p0}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 866241
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 866242
    const/4 v0, 0x2

    const v1, -0x69b6b47d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->g:I

    .line 866243
    const/4 v0, 0x3

    const v1, -0x4ac1df68

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;->h:I

    .line 866244
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 866238
    new-instance v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$AttributionAppMediaMetadataModel$AttributionAppModel;-><init>()V

    .line 866239
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 866240
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 866237
    const v0, -0x5b473f0d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 866236
    const v0, -0x3ff252d0

    return v0
.end method
