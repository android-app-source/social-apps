.class public final Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x13d0f656
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I

.field private n:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 868557
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 868558
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 868559
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 868560
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 868561
    iput-object p1, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 868562
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 868563
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 868564
    if-eqz v0, :cond_0

    .line 868565
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 868566
    :cond_0
    return-void

    .line 868567
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868568
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 868569
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 868570
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868571
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 868572
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868573
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->g:Ljava/lang/String;

    .line 868574
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868490
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->h:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->h:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 868491
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->h:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    return-object v0
.end method

.method private n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMutualFriends"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868575
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 868576
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868577
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->k:Ljava/lang/String;

    .line 868578
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868555
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 868556
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private q()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868579
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->n:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->n:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;

    .line 868580
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->n:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 868533
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 868534
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 868535
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 868536
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 868537
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->m()Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 868538
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->n()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, 0x3eee8a09

    invoke-static {v5, v4, v6}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 868539
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 868540
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 868541
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->q()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 868542
    const/16 v8, 0xa

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 868543
    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 868544
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 868545
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 868546
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 868547
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 868548
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 868549
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 868550
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 868551
    const/16 v0, 0x8

    iget v1, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->m:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 868552
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 868553
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 868554
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 868513
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 868514
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 868515
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x3eee8a09

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 868516
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 868517
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;

    .line 868518
    iput v3, v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->j:I

    move-object v1, v0

    .line 868519
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 868520
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 868521
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 868522
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;

    .line 868523
    iput-object v0, v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 868524
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->q()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 868525
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->q()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;

    .line 868526
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->q()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 868527
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;

    .line 868528
    iput-object v0, v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->n:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;

    .line 868529
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 868530
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 868531
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 868532
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 868512
    new-instance v0, LX/5Cd;

    invoke-direct {v0, p1}, LX/5Cd;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868511
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 868506
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 868507
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->i:Z

    .line 868508
    const/4 v0, 0x5

    const v1, 0x3eee8a09

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->j:I

    .line 868509
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->m:I

    .line 868510
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 868500
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 868501
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 868502
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 868503
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 868504
    :goto_0
    return-void

    .line 868505
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 868497
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 868498
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 868499
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 868494
    new-instance v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel$NodeModel;-><init>()V

    .line 868495
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 868496
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 868493
    const v0, 0x79fc811f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 868492
    const v0, 0x3c2b9d5

    return v0
.end method
