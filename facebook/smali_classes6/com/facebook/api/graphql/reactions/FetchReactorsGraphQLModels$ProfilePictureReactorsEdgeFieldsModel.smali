.class public final Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x356fa7e0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 869154
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 869153
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 869151
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 869152
    return-void
.end method

.method private j()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 869149
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->e:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->e:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;

    .line 869150
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->e:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;

    return-object v0
.end method

.method private k()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 869147
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->f:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->f:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;

    .line 869148
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->f:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 869139
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 869140
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->j()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 869141
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->k()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 869142
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 869143
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 869144
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 869145
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 869146
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 869119
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 869120
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->j()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 869121
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->j()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;

    .line 869122
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->j()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 869123
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;

    .line 869124
    iput-object v0, v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->e:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;

    .line 869125
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->k()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 869126
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->k()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;

    .line 869127
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->k()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 869128
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;

    .line 869129
    iput-object v0, v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->f:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;

    .line 869130
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 869131
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 869138
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->j()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 869137
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->k()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 869134
    new-instance v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;-><init>()V

    .line 869135
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 869136
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 869133
    const v0, -0x163bc1b1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 869132
    const v0, 0x2ac821ce

    return v0
.end method
