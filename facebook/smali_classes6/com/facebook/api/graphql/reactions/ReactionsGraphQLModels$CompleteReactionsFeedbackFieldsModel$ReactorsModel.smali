.class public final Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x462c67fa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 869933
    const-class v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 869932
    const-class v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 869936
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 869937
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 869934
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 869935
    iget v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 869917
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 869918
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 869919
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 869920
    iget v1, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 869921
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 869922
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 869923
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 869924
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 869925
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 869926
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 869927
    if-eqz v1, :cond_0

    .line 869928
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    .line 869929
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->f:Ljava/util/List;

    .line 869930
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 869931
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 869911
    iput p1, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->e:I

    .line 869912
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 869913
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 869914
    if-eqz v0, :cond_0

    .line 869915
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 869916
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 869908
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 869909
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->e:I

    .line 869910
    return-void
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 869906
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->f:Ljava/util/List;

    .line 869907
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 869903
    new-instance v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;-><init>()V

    .line 869904
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 869905
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 869901
    const v0, 0xcf8d8d1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 869902
    const v0, 0xf50222f

    return v0
.end method
