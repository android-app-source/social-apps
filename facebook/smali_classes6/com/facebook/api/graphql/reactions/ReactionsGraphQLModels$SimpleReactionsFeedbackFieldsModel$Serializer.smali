.class public final Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 870503
    const-class v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 870504
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 870505
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 870506
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 870507
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 870508
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 870509
    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result v2

    .line 870510
    if-eqz v2, :cond_0

    .line 870511
    const-string v3, "can_viewer_react"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 870512
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 870513
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 870514
    if-eqz v2, :cond_1

    .line 870515
    const-string v3, "important_reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 870516
    invoke-static {v1, v2, p1, p2}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 870517
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 870518
    if-eqz v2, :cond_2

    .line 870519
    const-string v3, "reactors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 870520
    invoke-static {v1, v2, p1}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 870521
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 870522
    if-eqz v2, :cond_3

    .line 870523
    const-string v3, "supported_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 870524
    invoke-static {v1, v2, p1, p2}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 870525
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 870526
    if-eqz v2, :cond_4

    .line 870527
    const-string v3, "top_reactions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 870528
    invoke-static {v1, v2, p1, p2}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 870529
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 870530
    if-eqz v2, :cond_5

    .line 870531
    const-string v3, "viewer_acts_as_person"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 870532
    invoke-static {v1, v2, p1}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 870533
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 870534
    if-eqz v2, :cond_6

    .line 870535
    const-string v3, "viewer_feedback_reaction_key"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 870536
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 870537
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 870538
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 870539
    check-cast p1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
