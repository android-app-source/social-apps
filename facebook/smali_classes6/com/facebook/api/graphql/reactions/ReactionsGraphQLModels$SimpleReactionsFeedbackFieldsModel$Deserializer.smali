.class public final Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 870384
    const-class v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 870385
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 870386
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 870387
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 870388
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 870389
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_c

    .line 870390
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 870391
    :goto_0
    move v1, v2

    .line 870392
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 870393
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 870394
    new-instance v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel;

    invoke-direct {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel;-><init>()V

    .line 870395
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 870396
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 870397
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 870398
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 870399
    :cond_0
    return-object v1

    .line 870400
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_9

    .line 870401
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 870402
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 870403
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_1

    if-eqz v12, :cond_1

    .line 870404
    const-string p0, "can_viewer_react"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 870405
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v4

    move v11, v4

    move v4, v3

    goto :goto_1

    .line 870406
    :cond_2
    const-string p0, "important_reactors"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 870407
    invoke-static {p1, v0}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 870408
    :cond_3
    const-string p0, "reactors"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 870409
    invoke-static {p1, v0}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 870410
    :cond_4
    const-string p0, "supported_reactions"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 870411
    invoke-static {p1, v0}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 870412
    :cond_5
    const-string p0, "top_reactions"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 870413
    invoke-static {p1, v0}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 870414
    :cond_6
    const-string p0, "viewer_acts_as_person"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 870415
    invoke-static {p1, v0}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 870416
    :cond_7
    const-string p0, "viewer_feedback_reaction_key"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 870417
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v5, v1

    move v1, v3

    goto :goto_1

    .line 870418
    :cond_8
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 870419
    :cond_9
    const/4 v12, 0x7

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 870420
    if-eqz v4, :cond_a

    .line 870421
    invoke-virtual {v0, v2, v11}, LX/186;->a(IZ)V

    .line 870422
    :cond_a
    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 870423
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 870424
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 870425
    const/4 v3, 0x4

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 870426
    const/4 v3, 0x5

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 870427
    if-eqz v1, :cond_b

    .line 870428
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5, v2}, LX/186;->a(III)V

    .line 870429
    :cond_b
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    goto/16 :goto_1
.end method
