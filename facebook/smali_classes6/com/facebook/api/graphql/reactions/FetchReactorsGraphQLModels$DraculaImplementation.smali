.class public final Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 868890
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 868891
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 868888
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 868889
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 868876
    if-nez p1, :cond_0

    .line 868877
    :goto_0
    return v0

    .line 868878
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 868879
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 868880
    :sswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 868881
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 868882
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 868883
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 868884
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 868885
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 868886
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 868887
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7ad57919 -> :sswitch_0
        0x3eee8a09 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 868875
    new-instance v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 868872
    sparse-switch p0, :sswitch_data_0

    .line 868873
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 868874
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7ad57919 -> :sswitch_0
        0x3eee8a09 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 868871
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 868869
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;->b(I)V

    .line 868870
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 868892
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 868893
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 868894
    :cond_0
    iput-object p1, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 868895
    iput p2, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;->b:I

    .line 868896
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 868843
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 868844
    new-instance v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 868845
    iget v0, p0, LX/1vt;->c:I

    .line 868846
    move v0, v0

    .line 868847
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 868848
    iget v0, p0, LX/1vt;->c:I

    .line 868849
    move v0, v0

    .line 868850
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 868851
    iget v0, p0, LX/1vt;->b:I

    .line 868852
    move v0, v0

    .line 868853
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868854
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 868855
    move-object v0, v0

    .line 868856
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 868857
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 868858
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 868859
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 868860
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 868861
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 868862
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 868863
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 868864
    invoke-static {v3, v9, v2}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 868865
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 868866
    iget v0, p0, LX/1vt;->c:I

    .line 868867
    move v0, v0

    .line 868868
    return v0
.end method
