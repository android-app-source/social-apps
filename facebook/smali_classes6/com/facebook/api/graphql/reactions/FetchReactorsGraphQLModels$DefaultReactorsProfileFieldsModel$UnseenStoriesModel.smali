.class public final Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 868754
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 868753
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 868751
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 868752
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 868746
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 868747
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 868748
    iget v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 868749
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 868750
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 868755
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 868756
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 868757
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 868743
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 868744
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;->e:I

    .line 868745
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 868738
    new-instance v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$UnseenStoriesModel;-><init>()V

    .line 868739
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 868740
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 868742
    const v0, -0x400175fd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 868741
    const v0, 0x69e993d

    return v0
.end method
