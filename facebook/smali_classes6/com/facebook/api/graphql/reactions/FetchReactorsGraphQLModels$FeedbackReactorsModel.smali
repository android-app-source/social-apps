.class public final Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x26b16ac9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 868983
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 868984
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 868981
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 868982
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868978
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 868979
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 868980
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private j()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868976
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->f:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->f:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    .line 868977
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->f:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    return-object v0
.end method

.method private k()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getReactors"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868974
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->g:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->g:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    .line 868975
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->g:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 868985
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 868986
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 868987
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->j()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 868988
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->k()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 868989
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 868990
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 868991
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 868992
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 868993
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 868994
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 868961
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 868962
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->j()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 868963
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->j()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    .line 868964
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->j()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 868965
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;

    .line 868966
    iput-object v0, v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->f:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    .line 868967
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->k()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 868968
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->k()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    .line 868969
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->k()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 868970
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;

    .line 868971
    iput-object v0, v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;->g:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    .line 868972
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 868973
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 868960
    new-instance v0, LX/5Cf;

    invoke-direct {v0, p1}, LX/5Cf;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 868958
    invoke-virtual {p2}, LX/18L;->a()V

    .line 868959
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 868957
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 868954
    new-instance v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;-><init>()V

    .line 868955
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 868956
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 868952
    const v0, 0x3ea7c1ce

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 868953
    const v0, 0x252222

    return v0
.end method
