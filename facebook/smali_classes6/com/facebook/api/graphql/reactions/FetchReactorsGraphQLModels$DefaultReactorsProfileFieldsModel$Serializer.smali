.class public final Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 868716
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 868717
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 868715
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 868677
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 868678
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x1

    const/4 p0, 0x0

    .line 868679
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 868680
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 868681
    if-eqz v2, :cond_0

    .line 868682
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868683
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 868684
    :cond_0
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 868685
    if-eqz v2, :cond_1

    .line 868686
    const-string v2, "friendship_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868687
    invoke-virtual {v1, v0, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 868688
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 868689
    if-eqz v2, :cond_2

    .line 868690
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868691
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 868692
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 868693
    if-eqz v2, :cond_3

    .line 868694
    const-string v3, "mutual_friends"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868695
    invoke-static {v1, v2, p1}, LX/5Cn;->a(LX/15i;ILX/0nX;)V

    .line 868696
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 868697
    if-eqz v2, :cond_4

    .line 868698
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868699
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 868700
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 868701
    if-eqz v2, :cond_5

    .line 868702
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868703
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 868704
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 868705
    if-eqz v2, :cond_6

    .line 868706
    const-string v3, "unread_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868707
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 868708
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 868709
    if-eqz v2, :cond_7

    .line 868710
    const-string v3, "unseen_stories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868711
    invoke-static {v1, v2, p1}, LX/5Co;->a(LX/15i;ILX/0nX;)V

    .line 868712
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 868713
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 868714
    check-cast p1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsProfileFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
