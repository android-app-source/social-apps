.class public final Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2a4ea2c8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 868404
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 868403
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 868401
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 868402
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEdges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 868399
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultReactorsEdgeFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->f:Ljava/util/List;

    .line 868400
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 868397
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 868398
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 868388
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 868389
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 868390
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 868391
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 868392
    iget v2, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->e:I

    invoke-virtual {p1, v3, v2, v3}, LX/186;->a(III)V

    .line 868393
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 868394
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 868395
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 868396
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 868375
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 868376
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 868377
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 868378
    if-eqz v1, :cond_2

    .line 868379
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    .line 868380
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 868381
    :goto_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 868382
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 868383
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 868384
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    .line 868385
    iput-object v0, v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 868386
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 868387
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 868367
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 868368
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;->e:I

    .line 868369
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 868372
    new-instance v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultFeedbackReactorsFieldsModel;-><init>()V

    .line 868373
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 868374
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 868371
    const v0, -0x6a0e4dac

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 868370
    const v0, 0xf50222f

    return v0
.end method
