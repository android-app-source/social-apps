.class public final Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1d7b9fe5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 869237
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 869236
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 869262
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 869263
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 869260
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel;->e:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel;->e:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    .line 869261
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel;->e:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 869254
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 869255
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel;->a()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 869256
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 869257
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 869258
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 869259
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 869246
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 869247
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel;->a()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 869248
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel;->a()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    .line 869249
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel;->a()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 869250
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel;

    .line 869251
    iput-object v0, v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel;->e:Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel$ProfileDiscoveryBucketModel;

    .line 869252
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 869253
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 869264
    new-instance v0, LX/5Ch;

    invoke-direct {v0, p1}, LX/5Ch;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 869244
    invoke-virtual {p2}, LX/18L;->a()V

    .line 869245
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 869243
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 869240
    new-instance v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ReactorsProfileDiscoveryEntryPointFieldsModel;-><init>()V

    .line 869241
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 869242
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 869239
    const v0, 0x7a8a2341    # 3.58626E35f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 869238
    const v0, -0x78fb05b

    return v0
.end method
