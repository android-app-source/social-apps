.class public final Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/222;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xe251061
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 869831
    const-class v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 869830
    const-class v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 869806
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 869807
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTopReactions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 869828
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;->e:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;->e:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    .line 869829
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;->e:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    return-object v0
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 869822
    iput-object p1, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;->e:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    .line 869823
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 869824
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 869825
    if-eqz v0, :cond_0

    .line 869826
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 869827
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 869816
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 869817
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 869818
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 869819
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 869820
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 869821
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 869808
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 869809
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 869810
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    .line 869811
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 869812
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;

    .line 869813
    iput-object v0, v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;->e:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    .line 869814
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 869815
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 869832
    new-instance v0, LX/5Cx;

    invoke-direct {v0, p1}, LX/5Cx;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 869795
    invoke-virtual {p2}, LX/18L;->a()V

    .line 869796
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 869797
    const-string v0, "top_reactions"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 869798
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;)V

    .line 869799
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 869800
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 869801
    new-instance v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel;-><init>()V

    .line 869802
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 869803
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 869804
    const v0, -0x48367bf1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 869805
    const v0, -0x78fb05b

    return v0
.end method
