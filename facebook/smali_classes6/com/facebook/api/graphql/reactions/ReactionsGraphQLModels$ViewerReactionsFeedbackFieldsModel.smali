.class public final Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/225;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x32d18680
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 870831
    const-class v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 870832
    const-class v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 870833
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 870834
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 870835
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->f:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->f:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 870836
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->f:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 870837
    iput p1, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->i:I

    .line 870838
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 870839
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 870840
    if-eqz v0, :cond_0

    .line 870841
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 870842
    :cond_0
    return-void
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 870843
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->g:Ljava/util/List;

    .line 870844
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 870845
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->h:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->h:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 870846
    iget-object v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->h:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    return-object v0
.end method

.method private l()I
    .locals 2

    .prologue
    .line 870829
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 870830
    iget v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->i:I

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 870847
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 870848
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 870849
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 870850
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->k()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 870851
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 870852
    iget-boolean v3, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->e:Z

    invoke-virtual {p1, v4, v3}, LX/186;->a(IZ)V

    .line 870853
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 870854
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 870855
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 870856
    const/4 v0, 0x4

    iget v1, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->i:I

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 870857
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 870858
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 870792
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 870793
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 870794
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 870795
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 870796
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;

    .line 870797
    iput-object v0, v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->f:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 870798
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 870799
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 870800
    if-eqz v2, :cond_1

    .line 870801
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;

    .line 870802
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->g:Ljava/util/List;

    move-object v1, v0

    .line 870803
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->k()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 870804
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->k()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 870805
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->k()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 870806
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;

    .line 870807
    iput-object v0, v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->h:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 870808
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 870809
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 870828
    new-instance v0, LX/5D5;

    invoke-direct {v0, p1}, LX/5D5;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 870824
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 870825
    invoke-virtual {p1, p2, v1}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->e:Z

    .line 870826
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->i:I

    .line 870827
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 870818
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 870819
    invoke-direct {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 870820
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 870821
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    .line 870822
    :goto_0
    return-void

    .line 870823
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 870815
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 870816
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;->a(I)V

    .line 870817
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 870812
    new-instance v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel;-><init>()V

    .line 870813
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 870814
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 870811
    const v0, 0x4e16d450    # 6.3262413E8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 870810
    const v0, -0x78fb05b

    return v0
.end method
