.class public final Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x197998fc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 869729
    const-class v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 869730
    const-class v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 869731
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 869732
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 869733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 869734
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x63b16869

    invoke-static {v1, v0, v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 869735
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 869736
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 869737
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->f:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 869738
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 869739
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 869740
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 869741
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 869742
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x63b16869

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 869743
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 869744
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;

    .line 869745
    iput v3, v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->e:I

    .line 869746
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 869747
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 869748
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 869749
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 869750
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 869751
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 869752
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 869753
    const v0, -0x63b16869

    invoke-static {p1, p2, v2, v0}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->e:I

    .line 869754
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->f:I

    .line 869755
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 869756
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 869757
    iget v0, p0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 869758
    new-instance v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;-><init>()V

    .line 869759
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 869760
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 869761
    const v0, 0x79ef7cc6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 869762
    const v0, -0x667f32ee

    return v0
.end method
