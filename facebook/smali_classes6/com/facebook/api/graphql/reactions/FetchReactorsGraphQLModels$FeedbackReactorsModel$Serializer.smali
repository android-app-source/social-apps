.class public final Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 868931
    const-class v0, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;

    new-instance v1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 868932
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 868933
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 868934
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 868935
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 868936
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 868937
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 868938
    if-eqz v2, :cond_0

    .line 868939
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868940
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 868941
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 868942
    if-eqz v2, :cond_1

    .line 868943
    const-string p0, "profile_discovery_bucket"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868944
    invoke-static {v1, v2, p1}, LX/5Cs;->a(LX/15i;ILX/0nX;)V

    .line 868945
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 868946
    if-eqz v2, :cond_2

    .line 868947
    const-string p0, "reactors"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868948
    invoke-static {v1, v2, p1, p2}, LX/5Cj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 868949
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 868950
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 868951
    check-cast p1, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel$Serializer;->a(Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$FeedbackReactorsModel;LX/0nX;LX/0my;)V

    return-void
.end method
