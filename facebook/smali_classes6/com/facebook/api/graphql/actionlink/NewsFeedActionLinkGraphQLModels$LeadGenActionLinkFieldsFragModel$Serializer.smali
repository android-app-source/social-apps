.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 831574
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel;

    new-instance v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 831575
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 831577
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 831578
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 831579
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 831580
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 831581
    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831582
    if-eqz v2, :cond_0

    .line 831583
    const-string v3, "ad_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831584
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831585
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831586
    if-eqz v2, :cond_1

    .line 831587
    const-string v3, "agree_to_privacy_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831588
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831589
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 831590
    if-eqz v2, :cond_2

    .line 831591
    const-string v3, "android_minimal_screen_form_height"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831592
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 831593
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 831594
    if-eqz v2, :cond_3

    .line 831595
    const-string v3, "android_small_screen_phone_threshold"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831596
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 831597
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831598
    if-eqz v2, :cond_4

    .line 831599
    const-string v3, "country_code"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831600
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831601
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831602
    if-eqz v2, :cond_5

    .line 831603
    const-string v3, "disclaimer_accept_button_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831604
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831605
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831606
    if-eqz v2, :cond_6

    .line 831607
    const-string v3, "disclaimer_continue_button_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831608
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831609
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 831610
    if-eqz v2, :cond_7

    .line 831611
    const-string v3, "error_codes"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831612
    invoke-static {v1, v2, p1, p2}, LX/56V;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 831613
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831614
    if-eqz v2, :cond_8

    .line 831615
    const-string v3, "error_message_brief"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831616
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831617
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831618
    if-eqz v2, :cond_9

    .line 831619
    const-string v3, "error_message_detail"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831620
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831621
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831622
    if-eqz v2, :cond_a

    .line 831623
    const-string v3, "fb_data_policy_setting_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831624
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831625
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831626
    if-eqz v2, :cond_b

    .line 831627
    const-string v3, "fb_data_policy_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831628
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831629
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831630
    if-eqz v2, :cond_c

    .line 831631
    const-string v3, "follow_up_action_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831632
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831633
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831634
    if-eqz v2, :cond_d

    .line 831635
    const-string v3, "follow_up_action_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831636
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831637
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831638
    if-eqz v2, :cond_e

    .line 831639
    const-string v3, "landing_page_cta"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831640
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831641
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831642
    if-eqz v2, :cond_f

    .line 831643
    const-string v3, "landing_page_redirect_instruction"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831644
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831645
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 831646
    if-eqz v2, :cond_10

    .line 831647
    const-string v3, "lead_gen_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831648
    invoke-static {v1, v2, p1, p2}, LX/56Y;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 831649
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831650
    if-eqz v2, :cond_11

    .line 831651
    const-string v3, "lead_gen_data_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831652
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831653
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 831654
    if-eqz v2, :cond_12

    .line 831655
    const-string v3, "lead_gen_deep_link_user_status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831656
    invoke-static {v1, v2, p1}, LX/56h;->a(LX/15i;ILX/0nX;)V

    .line 831657
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 831658
    if-eqz v2, :cond_13

    .line 831659
    const-string v3, "lead_gen_user_status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831660
    invoke-static {v1, v2, p1}, LX/56Z;->a(LX/15i;ILX/0nX;)V

    .line 831661
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831662
    if-eqz v2, :cond_14

    .line 831663
    const-string v3, "nux_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831664
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831665
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831666
    if-eqz v2, :cond_15

    .line 831667
    const-string v3, "nux_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831668
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831669
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 831670
    if-eqz v2, :cond_16

    .line 831671
    const-string v3, "page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831672
    invoke-static {v1, v2, p1, p2}, LX/56c;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 831673
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831674
    if-eqz v2, :cond_17

    .line 831675
    const-string v3, "primary_button_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831676
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831677
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831678
    if-eqz v2, :cond_18

    .line 831679
    const-string v3, "privacy_checkbox_error"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831680
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831681
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831682
    if-eqz v2, :cond_19

    .line 831683
    const-string v3, "privacy_setting_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831684
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831685
    :cond_19
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831686
    if-eqz v2, :cond_1a

    .line 831687
    const-string v3, "progress_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831688
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831689
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831690
    if-eqz v2, :cond_1b

    .line 831691
    const-string v3, "secure_sharing_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831692
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831693
    :cond_1b
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831694
    if-eqz v2, :cond_1c

    .line 831695
    const-string v3, "select_text_hint"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831696
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831697
    :cond_1c
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831698
    if-eqz v2, :cond_1d

    .line 831699
    const-string v3, "send_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831700
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831701
    :cond_1d
    const/16 v2, 0x1e

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831702
    if-eqz v2, :cond_1e

    .line 831703
    const-string v3, "sent_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831704
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831705
    :cond_1e
    const/16 v2, 0x1f

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831706
    if-eqz v2, :cond_1f

    .line 831707
    const-string v3, "share_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831708
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831709
    :cond_1f
    const/16 v2, 0x20

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831710
    if-eqz v2, :cond_20

    .line 831711
    const-string v3, "short_secure_sharing_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831712
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831713
    :cond_20
    const/16 v2, 0x21

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 831714
    if-eqz v2, :cond_21

    .line 831715
    const-string v3, "skip_experiments"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831716
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 831717
    :cond_21
    const/16 v2, 0x22

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831718
    if-eqz v2, :cond_22

    .line 831719
    const-string v3, "split_flow_landing_page_hint_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831720
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831721
    :cond_22
    const/16 v2, 0x23

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831722
    if-eqz v2, :cond_23

    .line 831723
    const-string v3, "split_flow_landing_page_hint_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831724
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831725
    :cond_23
    const/16 v2, 0x24

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831726
    if-eqz v2, :cond_24

    .line 831727
    const-string v3, "submit_card_instruction_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831728
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831729
    :cond_24
    const/16 v2, 0x25

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 831730
    if-eqz v2, :cond_25

    .line 831731
    const-string v3, "unsubscribe_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 831732
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 831733
    :cond_25
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 831734
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 831576
    check-cast p1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$Serializer;->a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel;LX/0nX;LX/0my;)V

    return-void
.end method
