.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 830280
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 830281
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 830282
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 830283
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 830284
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 830285
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 830286
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 830287
    if-eqz v2, :cond_0

    .line 830288
    const-string p0, "album"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 830289
    invoke-static {v1, v2, p1, p2}, LX/56L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 830290
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 830291
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 830292
    check-cast p1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
