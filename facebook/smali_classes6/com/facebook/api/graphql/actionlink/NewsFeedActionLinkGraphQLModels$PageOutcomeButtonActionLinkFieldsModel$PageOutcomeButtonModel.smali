.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x63c865b9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$ButtonIconImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 835472
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 835471
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 835469
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 835470
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$ButtonIconImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 835467
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$ButtonIconImageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$ButtonIconImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$ButtonIconImageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$ButtonIconImageModel;

    .line 835468
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$ButtonIconImageModel;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 835465
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->f:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->f:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 835466
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->f:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 835463
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->g:Ljava/lang/String;

    .line 835464
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 835461
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->h:Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->h:Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    .line 835462
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->h:Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 835449
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 835450
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$ButtonIconImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 835451
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->j()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 835452
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 835453
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->l()Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 835454
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 835455
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 835456
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 835457
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 835458
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 835459
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 835460
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 835441
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 835442
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$ButtonIconImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 835443
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$ButtonIconImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$ButtonIconImageModel;

    .line 835444
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$ButtonIconImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 835445
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;

    .line 835446
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel$ButtonIconImageModel;

    .line 835447
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 835448
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 835436
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;-><init>()V

    .line 835437
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 835438
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 835440
    const v0, 0x33b296e7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 835439
    const v0, -0x258d3d4b

    return v0
.end method
