.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2a618819
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 836871
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 836870
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 836868
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 836869
    return-void
.end method

.method private j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836866
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    .line 836867
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836799
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->g:Ljava/lang/String;

    .line 836800
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 836864
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->h:Ljava/util/List;

    .line 836865
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestOwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836862
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestOwnerModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestOwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestOwnerModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestOwnerModel;

    .line 836863
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestOwnerModel;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836860
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->j:Ljava/lang/String;

    .line 836861
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836858
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->k:Ljava/lang/String;

    .line 836859
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836856
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->l:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->l:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    .line 836857
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->l:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836854
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->m:Ljava/lang/String;

    .line 836855
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->m:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 836833
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 836834
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 836835
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 836836
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->l()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v8

    .line 836837
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestOwnerModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 836838
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 836839
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 836840
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->p()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 836841
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 836842
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 836843
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 836844
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 836845
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 836846
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 836847
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 836848
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 836849
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 836850
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 836851
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v13}, LX/186;->b(II)V

    .line 836852
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 836853
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 836810
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 836811
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 836812
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    .line 836813
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 836814
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;

    .line 836815
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    .line 836816
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 836817
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 836818
    if-eqz v2, :cond_1

    .line 836819
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;

    .line 836820
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->h:Ljava/util/List;

    move-object v1, v0

    .line 836821
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 836822
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestOwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestOwnerModel;

    .line 836823
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestOwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 836824
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;

    .line 836825
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestOwnerModel;

    .line 836826
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->p()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 836827
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->p()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    .line 836828
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->p()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 836829
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;

    .line 836830
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->l:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotFontResourceModel;

    .line 836831
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 836832
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836809
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 836806
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 836807
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;->e:J

    .line 836808
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 836803
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;-><init>()V

    .line 836804
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 836805
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 836802
    const v0, 0x7f76673e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 836801
    const v0, -0x11b13572    # -1.5999696E28f

    return v0
.end method
