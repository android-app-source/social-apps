.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 832923
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 832924
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 832922
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 832896
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 832897
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x3

    .line 832898
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 832899
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 832900
    if-eqz v2, :cond_0

    .line 832901
    const-string v3, "ad"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 832902
    invoke-static {v1, v2, p1}, LX/56s;->a(LX/15i;ILX/0nX;)V

    .line 832903
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 832904
    if-eqz v2, :cond_1

    .line 832905
    const-string v3, "messenger_extensions_payment_privacy_policy"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 832906
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 832907
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 832908
    if-eqz v2, :cond_2

    .line 832909
    const-string v3, "messenger_extensions_user_profile"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 832910
    invoke-static {v1, v2, p1}, LX/56t;->a(LX/15i;ILX/0nX;)V

    .line 832911
    :cond_2
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 832912
    if-eqz v2, :cond_3

    .line 832913
    const-string v2, "messenger_extensions_whitelisted_domains"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 832914
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 832915
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 832916
    if-eqz v2, :cond_4

    .line 832917
    const-string v3, "page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 832918
    invoke-static {v1, v2, p1}, LX/56u;->a(LX/15i;ILX/0nX;)V

    .line 832919
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 832920
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 832921
    check-cast p1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
