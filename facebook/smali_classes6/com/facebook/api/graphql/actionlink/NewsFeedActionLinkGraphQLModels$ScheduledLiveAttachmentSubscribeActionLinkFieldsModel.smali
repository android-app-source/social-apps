.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3e260b3d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 836419
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 836418
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 836416
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 836417
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836414
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->e:Ljava/lang/String;

    .line 836415
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836412
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->f:Ljava/lang/String;

    .line 836413
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836410
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->g:Ljava/lang/String;

    .line 836411
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836408
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    .line 836409
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 836383
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 836384
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 836385
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 836386
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 836387
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 836388
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 836389
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 836390
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 836391
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 836392
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 836393
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 836394
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 836400
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 836401
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 836402
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    .line 836403
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 836404
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;

    .line 836405
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    .line 836406
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 836407
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 836397
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel;-><init>()V

    .line 836398
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 836399
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 836396
    const v0, -0x6dcd8dff

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 836395
    const v0, -0x6818e089

    return v0
.end method
