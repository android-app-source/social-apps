.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2a0b2166
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$HiResThemeImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$LowResThemeImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$MedResThemeImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 830760
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 830759
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 830757
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 830758
    return-void
.end method

.method private j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$HiResThemeImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830755
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$HiResThemeImageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$HiResThemeImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$HiResThemeImageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$HiResThemeImageModel;

    .line 830756
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$HiResThemeImageModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830753
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->f:Ljava/lang/String;

    .line 830754
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$LowResThemeImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830751
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$LowResThemeImageModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$LowResThemeImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$LowResThemeImageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$LowResThemeImageModel;

    .line 830752
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$LowResThemeImageModel;

    return-object v0
.end method

.method private m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$MedResThemeImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830749
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$MedResThemeImageModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$MedResThemeImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$MedResThemeImageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$MedResThemeImageModel;

    .line 830750
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$MedResThemeImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 830737
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 830738
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$HiResThemeImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 830739
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 830740
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$LowResThemeImageModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 830741
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$MedResThemeImageModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 830742
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 830743
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 830744
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 830745
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 830746
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 830747
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 830748
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 830713
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 830714
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$HiResThemeImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 830715
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$HiResThemeImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$HiResThemeImageModel;

    .line 830716
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$HiResThemeImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 830717
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    .line 830718
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$HiResThemeImageModel;

    .line 830719
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$LowResThemeImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 830720
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$LowResThemeImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$LowResThemeImageModel;

    .line 830721
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$LowResThemeImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 830722
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    .line 830723
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$LowResThemeImageModel;

    .line 830724
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$MedResThemeImageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 830725
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$MedResThemeImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$MedResThemeImageModel;

    .line 830726
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$MedResThemeImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 830727
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    .line 830728
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel$MedResThemeImageModel;

    .line 830729
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 830730
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830736
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 830733
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;-><init>()V

    .line 830734
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 830735
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 830732
    const v0, -0x47e81756

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 830731
    const v0, -0x3543e3fd    # -6163969.5f

    return v0
.end method
