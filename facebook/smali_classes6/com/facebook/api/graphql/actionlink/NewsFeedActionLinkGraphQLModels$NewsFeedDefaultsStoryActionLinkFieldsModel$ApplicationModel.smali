.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3299f9b1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$ApplicationModel$SquareLogoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 833269
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 833272
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 833270
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 833271
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 833267
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->e:Ljava/lang/String;

    .line 833268
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 833265
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->f:Ljava/lang/String;

    .line 833266
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$ApplicationModel$SquareLogoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 833273
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$ApplicationModel$SquareLogoModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$ApplicationModel$SquareLogoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$ApplicationModel$SquareLogoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$ApplicationModel$SquareLogoModel;

    .line 833274
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$ApplicationModel$SquareLogoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 833255
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 833256
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 833257
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 833258
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$ApplicationModel$SquareLogoModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 833259
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 833260
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 833261
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 833262
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 833263
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 833264
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 833247
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 833248
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$ApplicationModel$SquareLogoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 833249
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$ApplicationModel$SquareLogoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$ApplicationModel$SquareLogoModel;

    .line 833250
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$ApplicationModel$SquareLogoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 833251
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;

    .line 833252
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$ApplicationModel$SquareLogoModel;

    .line 833253
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 833254
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 833246
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 833241
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;-><init>()V

    .line 833242
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 833243
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 833245
    const v0, -0x29240535

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 833244
    const v0, -0x3ff252d0

    return v0
.end method
