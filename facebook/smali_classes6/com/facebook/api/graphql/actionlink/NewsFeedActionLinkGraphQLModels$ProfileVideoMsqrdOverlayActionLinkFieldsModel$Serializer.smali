.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 835802
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 835803
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 835801
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 835763
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 835764
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 835765
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 835766
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 835767
    if-eqz v2, :cond_0

    .line 835768
    const-string v3, "application"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 835769
    invoke-static {v1, v2, p1, p2}, LX/57K;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 835770
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 835771
    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 835772
    const-string v4, "default_expiration_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 835773
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 835774
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 835775
    if-eqz v2, :cond_2

    .line 835776
    const-string v3, "description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 835777
    invoke-static {v1, v2, p1, p2}, LX/4av;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 835778
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 835779
    if-eqz v2, :cond_3

    .line 835780
    const-string v3, "mask"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 835781
    invoke-static {v1, v2, p1}, LX/57L;->a(LX/15i;ILX/0nX;)V

    .line 835782
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 835783
    if-eqz v2, :cond_4

    .line 835784
    const-string v3, "not_installed_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 835785
    invoke-static {v1, v2, p1, p2}, LX/4av;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 835786
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 835787
    if-eqz v2, :cond_5

    .line 835788
    const-string v3, "not_installed_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 835789
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 835790
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 835791
    if-eqz v2, :cond_6

    .line 835792
    const-string v3, "profile"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 835793
    invoke-static {v1, v2, p1, p2}, LX/57N;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 835794
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 835795
    if-eqz v2, :cond_7

    .line 835796
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 835797
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 835798
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 835799
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 835800
    check-cast p1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
