.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5fa6c988
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$EntityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 832502
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 832501
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 832499
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 832500
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$EntityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 832497
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$EntityModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$EntityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$EntityModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$EntityModel;

    .line 832498
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$EntityModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 832503
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 832504
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$EntityModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 832505
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 832506
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 832507
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 832508
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 832509
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 832510
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 832489
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 832490
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$EntityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 832491
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$EntityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$EntityModel;

    .line 832492
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$EntityModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 832493
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;

    .line 832494
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel$EntityModel;

    .line 832495
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 832496
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 832485
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 832486
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;->f:I

    .line 832487
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;->g:I

    .line 832488
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 832482
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenLegalContentFieldsModel$DisclaimerBodyModel$RangesModel;-><init>()V

    .line 832483
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 832484
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 832481
    const v0, -0x1f7bad05

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 832480
    const v0, -0x3d10ccb9

    return v0
.end method
