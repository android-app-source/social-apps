.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 837148
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel;

    new-instance v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 837149
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 837150
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 837151
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 837152
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 837153
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 837154
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 837155
    if-eqz v2, :cond_0

    .line 837156
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837157
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 837158
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 837159
    if-eqz v2, :cond_1

    .line 837160
    const-string p0, "card_description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837161
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837162
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 837163
    if-eqz v2, :cond_2

    .line 837164
    const-string p0, "card_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837165
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837166
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 837167
    if-eqz v2, :cond_3

    .line 837168
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837169
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837170
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 837171
    if-eqz v2, :cond_4

    .line 837172
    const-string p0, "video"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837173
    invoke-static {v1, v2, p1, p2}, LX/57e;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 837174
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 837175
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 837176
    check-cast p1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$Serializer;->a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel;LX/0nX;LX/0my;)V

    return-void
.end method
