.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6f47577d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$AdModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 832975
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 832974
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 832925
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 832926
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$AdModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 832972
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$AdModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$AdModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$AdModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$AdModel;

    .line 832973
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$AdModel;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 832970
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->f:Ljava/lang/String;

    .line 832971
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 832968
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    .line 832969
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 832966
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->h:Ljava/util/List;

    .line 832967
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 832964
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$PageModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$PageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$PageModel;

    .line 832965
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$PageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 832950
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 832951
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$AdModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 832952
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 832953
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 832954
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->l()LX/0Px;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v3

    .line 832955
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$PageModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 832956
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 832957
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 832958
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 832959
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 832960
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 832961
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 832962
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 832963
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 832932
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 832933
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$AdModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 832934
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$AdModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$AdModel;

    .line 832935
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$AdModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 832936
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;

    .line 832937
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$AdModel;

    .line 832938
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 832939
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    .line 832940
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 832941
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;

    .line 832942
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    .line 832943
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 832944
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$PageModel;

    .line 832945
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 832946
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;

    .line 832947
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$PageModel;

    .line 832948
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 832949
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 832929
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel;-><init>()V

    .line 832930
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 832931
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 832928
    const v0, 0x354f8720

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 832927
    const v0, -0x1e53800c

    return v0
.end method
