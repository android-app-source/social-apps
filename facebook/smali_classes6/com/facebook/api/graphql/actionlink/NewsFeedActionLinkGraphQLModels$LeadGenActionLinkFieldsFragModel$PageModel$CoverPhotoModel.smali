.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xd029a13
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$PhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 831502
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 831507
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 831505
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 831506
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$PhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 831503
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$PhotoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$PhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$PhotoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$PhotoModel;

    .line 831504
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$PhotoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 831508
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 831509
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 831510
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 831511
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 831512
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 831513
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 831489
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 831490
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 831491
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$PhotoModel;

    .line 831492
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$PhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 831493
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel;

    .line 831494
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel$PhotoModel;

    .line 831495
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 831496
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 831497
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$PageModel$CoverPhotoModel;-><init>()V

    .line 831498
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 831499
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 831500
    const v0, 0x743a83ef

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 831501
    const v0, 0x1da3a91b

    return v0
.end method
