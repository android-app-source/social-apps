.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1f6f629
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$IconImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$PrivacyOptionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 830095
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 830140
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 830138
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 830139
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830136
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->e:Ljava/lang/String;

    .line 830137
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$IconImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830134
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$IconImageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$IconImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$IconImageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$IconImageModel;

    .line 830135
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$IconImageModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830132
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->g:Ljava/lang/String;

    .line 830133
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830130
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->h:Ljava/lang/String;

    .line 830131
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$PrivacyOptionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830128
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$PrivacyOptionsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$PrivacyOptionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$PrivacyOptionsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$PrivacyOptionsModel;

    .line 830129
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$PrivacyOptionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 830114
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 830115
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 830116
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$IconImageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 830117
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 830118
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 830119
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 830120
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 830121
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 830122
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 830123
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 830124
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 830125
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 830126
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 830127
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 830101
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 830102
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$IconImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 830103
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$IconImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$IconImageModel;

    .line 830104
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$IconImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 830105
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    .line 830106
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$IconImageModel;

    .line 830107
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 830108
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$PrivacyOptionsModel;

    .line 830109
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 830110
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    .line 830111
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel$PrivacyOptionsModel;

    .line 830112
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 830113
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 830098
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;-><init>()V

    .line 830099
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 830100
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 830097
    const v0, -0x1e542015

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 830096
    const v0, -0x1c648c34

    return v0
.end method
