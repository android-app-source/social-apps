.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x31cf7a33
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 830761
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 830793
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 830791
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 830792
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830789
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    .line 830790
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830787
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->g:Ljava/lang/String;

    .line 830788
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 830778
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 830779
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 830780
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 830781
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 830782
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 830783
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 830784
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 830785
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 830786
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 830770
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 830771
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 830772
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    .line 830773
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 830774
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;

    .line 830775
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel$ThemeModel;

    .line 830776
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 830777
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 830767
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 830768
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;->e:J

    .line 830769
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 830764
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;-><init>()V

    .line 830765
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 830766
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 830763
    const v0, -0x77c01f76

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 830762
    const v0, 0x54994d46

    return v0
.end method
