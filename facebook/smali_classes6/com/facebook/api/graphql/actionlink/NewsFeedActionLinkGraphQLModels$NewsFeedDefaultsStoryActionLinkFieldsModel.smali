.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x21b43abf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$ErrorCodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private L:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$EventModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private O:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$FeaturedInstantArticleElementModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private P:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupToggleCommentingActionLinkFieldsModel$FeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private S:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private T:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private U:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private V:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupJoinActionLinkFieldsModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private X:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$InfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Z:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$InstantArticleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aA:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aB:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aC:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aD:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OfferStoryActionLinkFieldsModel$OfferModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aE:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aF:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aG:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ParentStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aH:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aJ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aK:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PrivacyScopeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aL:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aM:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aP:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aQ:I

.field private aR:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aS:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aU:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aV:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aW:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aX:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aY:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aZ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aa:Z

.field private ab:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ac:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ad:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ae:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private af:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ag:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenDeepLinkUserStatusFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ah:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenUserStatusModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ai:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ak:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private al:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private am:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LinkTargetStoreDataFragModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private an:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ao:Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ap:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aq:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ar:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private as:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private at:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private au:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private av:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aw:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ax:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ay:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntityRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private az:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ba:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bb:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bc:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bd:Z

.field private be:Z

.field private bf:Z

.field private bg:Z

.field private bh:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bi:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bk:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bl:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bm:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bn:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bo:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$SupportInboxActionLinkFieldsModel$SupportItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bp:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TaggedAndMentionedUsersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bq:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private br:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bs:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bt:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TopicModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bu:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bv:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bw:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$VideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bx:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$VideoAnnotationFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private by:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MisinformationWarningActionLinkFieldsModel$ActionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$AdModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:I

.field private p:I

.field private q:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:I

.field private t:Z

.field private u:Z

.field private v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:J

.field private z:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 834816
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 834843
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 834841
    const/16 v0, 0x7d

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 834842
    return-void
.end method

.method private A()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834839
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->C:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->C:Ljava/lang/String;

    .line 834840
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->C:Ljava/lang/String;

    return-object v0
.end method

.method private B()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834837
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->D:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->D:Ljava/lang/String;

    .line 834838
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->D:Ljava/lang/String;

    return-object v0
.end method

.method private C()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834835
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->E:Ljava/lang/String;

    const/16 v1, 0x1a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->E:Ljava/lang/String;

    .line 834836
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->E:Ljava/lang/String;

    return-object v0
.end method

.method private D()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834833
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->F:Ljava/lang/String;

    const/16 v1, 0x1b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->F:Ljava/lang/String;

    .line 834834
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->F:Ljava/lang/String;

    return-object v0
.end method

.method private E()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834831
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->G:Ljava/lang/String;

    const/16 v1, 0x1c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->G:Ljava/lang/String;

    .line 834832
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->G:Ljava/lang/String;

    return-object v0
.end method

.method private F()Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834829
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->H:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->H:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    .line 834830
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->H:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    return-object v0
.end method

.method private G()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$ErrorCodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 834827
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->I:Ljava/util/List;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$ErrorCodesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->I:Ljava/util/List;

    .line 834828
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->I:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private H()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834825
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->J:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->J:Ljava/lang/String;

    .line 834826
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->J:Ljava/lang/String;

    return-object v0
.end method

.method private I()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834823
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->K:Ljava/lang/String;

    const/16 v1, 0x20

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->K:Ljava/lang/String;

    .line 834824
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->K:Ljava/lang/String;

    return-object v0
.end method

.method private J()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$EventModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834821
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->L:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$EventModel;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$EventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$EventModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->L:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$EventModel;

    .line 834822
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->L:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$EventModel;

    return-object v0
.end method

.method private K()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834819
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->M:Ljava/lang/String;

    const/16 v1, 0x22

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->M:Ljava/lang/String;

    .line 834820
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->M:Ljava/lang/String;

    return-object v0
.end method

.method private L()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834785
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->N:Ljava/lang/String;

    const/16 v1, 0x23

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->N:Ljava/lang/String;

    .line 834786
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->N:Ljava/lang/String;

    return-object v0
.end method

.method private M()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$FeaturedInstantArticleElementModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834258
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->O:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$FeaturedInstantArticleElementModel;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$FeaturedInstantArticleElementModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$FeaturedInstantArticleElementModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->O:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$FeaturedInstantArticleElementModel;

    .line 834259
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->O:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$FeaturedInstantArticleElementModel;

    return-object v0
.end method

.method private N()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupToggleCommentingActionLinkFieldsModel$FeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834814
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->P:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupToggleCommentingActionLinkFieldsModel$FeedbackModel;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupToggleCommentingActionLinkFieldsModel$FeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupToggleCommentingActionLinkFieldsModel$FeedbackModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->P:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupToggleCommentingActionLinkFieldsModel$FeedbackModel;

    .line 834815
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->P:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupToggleCommentingActionLinkFieldsModel$FeedbackModel;

    return-object v0
.end method

.method private O()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834812
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Q:Ljava/lang/String;

    const/16 v1, 0x26

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Q:Ljava/lang/String;

    .line 834813
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Q:Ljava/lang/String;

    return-object v0
.end method

.method private P()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834810
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->R:Ljava/lang/String;

    const/16 v1, 0x27

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->R:Ljava/lang/String;

    .line 834811
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->R:Ljava/lang/String;

    return-object v0
.end method

.method private Q()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 834808
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->S:Ljava/util/List;

    const/16 v1, 0x28

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->S:Ljava/util/List;

    .line 834809
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->S:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private R()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834806
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->T:Ljava/lang/String;

    const/16 v1, 0x29

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->T:Ljava/lang/String;

    .line 834807
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->T:Ljava/lang/String;

    return-object v0
.end method

.method private S()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834804
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->U:Ljava/lang/String;

    const/16 v1, 0x2a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->U:Ljava/lang/String;

    .line 834805
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->U:Ljava/lang/String;

    return-object v0
.end method

.method private T()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupJoinActionLinkFieldsModel$GroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834802
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->V:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupJoinActionLinkFieldsModel$GroupModel;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupJoinActionLinkFieldsModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupJoinActionLinkFieldsModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->V:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupJoinActionLinkFieldsModel$GroupModel;

    .line 834803
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->V:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupJoinActionLinkFieldsModel$GroupModel;

    return-object v0
.end method

.method private U()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834800
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->W:Ljava/lang/String;

    const/16 v1, 0x2c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->W:Ljava/lang/String;

    .line 834801
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->W:Ljava/lang/String;

    return-object v0
.end method

.method private V()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$InfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834798
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->X:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$InfoModel;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$InfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$InfoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->X:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$InfoModel;

    .line 834799
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->X:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$InfoModel;

    return-object v0
.end method

.method private W()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 834796
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Y:Ljava/util/List;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Y:Ljava/util/List;

    .line 834797
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Y:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private X()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$InstantArticleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834794
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Z:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$InstantArticleModel;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$InstantArticleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$InstantArticleModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Z:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$InstantArticleModel;

    .line 834795
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Z:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$InstantArticleModel;

    return-object v0
.end method

.method private Y()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834792
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ab:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ab:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    .line 834793
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ab:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    return-object v0
.end method

.method private Z()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834790
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ac:Ljava/lang/String;

    const/16 v1, 0x32

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ac:Ljava/lang/String;

    .line 834791
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ac:Ljava/lang/String;

    return-object v0
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834787
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 834788
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 834789
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private aA()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OfferStoryActionLinkFieldsModel$OfferModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834848
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aD:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OfferStoryActionLinkFieldsModel$OfferModel;

    const/16 v1, 0x4d

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OfferStoryActionLinkFieldsModel$OfferModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OfferStoryActionLinkFieldsModel$OfferModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aD:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OfferStoryActionLinkFieldsModel$OfferModel;

    .line 834849
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aD:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OfferStoryActionLinkFieldsModel$OfferModel;

    return-object v0
.end method

.method private aB()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834846
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aE:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PageModel;

    const/16 v1, 0x4e

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aE:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PageModel;

    .line 834847
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aE:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PageModel;

    return-object v0
.end method

.method private aC()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834882
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aF:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;

    const/16 v1, 0x4f

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aF:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;

    .line 834883
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aF:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;

    return-object v0
.end method

.method private aD()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ParentStoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834884
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aG:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ParentStoryModel;

    const/16 v1, 0x50

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ParentStoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ParentStoryModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aG:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ParentStoryModel;

    .line 834885
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aG:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ParentStoryModel;

    return-object v0
.end method

.method private aE()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834886
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aH:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    const/16 v1, 0x51

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aH:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    .line 834887
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aH:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    return-object v0
.end method

.method private aF()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834888
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aI:Ljava/lang/String;

    const/16 v1, 0x52

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aI:Ljava/lang/String;

    .line 834889
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aI:Ljava/lang/String;

    return-object v0
.end method

.method private aG()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834890
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aJ:Ljava/lang/String;

    const/16 v1, 0x53

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aJ:Ljava/lang/String;

    .line 834891
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aJ:Ljava/lang/String;

    return-object v0
.end method

.method private aH()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PrivacyScopeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834892
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aK:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PrivacyScopeModel;

    const/16 v1, 0x54

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PrivacyScopeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PrivacyScopeModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aK:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PrivacyScopeModel;

    .line 834893
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aK:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PrivacyScopeModel;

    return-object v0
.end method

.method private aI()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834894
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aL:Ljava/lang/String;

    const/16 v1, 0x55

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aL:Ljava/lang/String;

    .line 834895
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aL:Ljava/lang/String;

    return-object v0
.end method

.method private aJ()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834896
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aM:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ProfileModel;

    const/16 v1, 0x56

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ProfileModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aM:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ProfileModel;

    .line 834897
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aM:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ProfileModel;

    return-object v0
.end method

.method private aK()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834898
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aN:Ljava/lang/String;

    const/16 v1, 0x57

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aN:Ljava/lang/String;

    .line 834899
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aN:Ljava/lang/String;

    return-object v0
.end method

.method private aL()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834900
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aO:Ljava/lang/String;

    const/16 v1, 0x58

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aO:Ljava/lang/String;

    .line 834901
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aO:Ljava/lang/String;

    return-object v0
.end method

.method private aM()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834902
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aP:Ljava/lang/String;

    const/16 v1, 0x59

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aP:Ljava/lang/String;

    .line 834903
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aP:Ljava/lang/String;

    return-object v0
.end method

.method private aN()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834904
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aR:Ljava/lang/String;

    const/16 v1, 0x5b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aR:Ljava/lang/String;

    .line 834905
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aR:Ljava/lang/String;

    return-object v0
.end method

.method private aO()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834878
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aS:Ljava/lang/String;

    const/16 v1, 0x5c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aS:Ljava/lang/String;

    .line 834879
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aS:Ljava/lang/String;

    return-object v0
.end method

.method private aP()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834880
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aT:Ljava/lang/String;

    const/16 v1, 0x5d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aT:Ljava/lang/String;

    .line 834881
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aT:Ljava/lang/String;

    return-object v0
.end method

.method private aQ()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834876
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aU:Ljava/lang/String;

    const/16 v1, 0x5e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aU:Ljava/lang/String;

    .line 834877
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aU:Ljava/lang/String;

    return-object v0
.end method

.method private aR()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834874
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aV:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    const/16 v1, 0x5f

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aV:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    .line 834875
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aV:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    return-object v0
.end method

.method private aS()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834872
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aW:Ljava/lang/String;

    const/16 v1, 0x60

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aW:Ljava/lang/String;

    .line 834873
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aW:Ljava/lang/String;

    return-object v0
.end method

.method private aT()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834870
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aX:Ljava/lang/String;

    const/16 v1, 0x61

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aX:Ljava/lang/String;

    .line 834871
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aX:Ljava/lang/String;

    return-object v0
.end method

.method private aU()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834868
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aY:Ljava/lang/String;

    const/16 v1, 0x62

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aY:Ljava/lang/String;

    .line 834869
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aY:Ljava/lang/String;

    return-object v0
.end method

.method private aV()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834866
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aZ:Ljava/lang/String;

    const/16 v1, 0x63

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aZ:Ljava/lang/String;

    .line 834867
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aZ:Ljava/lang/String;

    return-object v0
.end method

.method private aW()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834864
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ba:Ljava/lang/String;

    const/16 v1, 0x64

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ba:Ljava/lang/String;

    .line 834865
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ba:Ljava/lang/String;

    return-object v0
.end method

.method private aX()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834862
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bb:Ljava/lang/String;

    const/16 v1, 0x65

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bb:Ljava/lang/String;

    .line 834863
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bb:Ljava/lang/String;

    return-object v0
.end method

.method private aY()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834860
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bc:Ljava/lang/String;

    const/16 v1, 0x66

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bc:Ljava/lang/String;

    .line 834861
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bc:Ljava/lang/String;

    return-object v0
.end method

.method private aZ()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834858
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bh:Ljava/lang/String;

    const/16 v1, 0x6b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bh:Ljava/lang/String;

    .line 834859
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bh:Ljava/lang/String;

    return-object v0
.end method

.method private aa()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834856
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ad:Ljava/lang/String;

    const/16 v1, 0x33

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ad:Ljava/lang/String;

    .line 834857
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ad:Ljava/lang/String;

    return-object v0
.end method

.method private ab()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834854
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ae:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ae:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel;

    .line 834855
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ae:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel;

    return-object v0
.end method

.method private ac()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834852
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->af:Ljava/lang/String;

    const/16 v1, 0x35

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->af:Ljava/lang/String;

    .line 834853
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->af:Ljava/lang/String;

    return-object v0
.end method

.method private ad()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenDeepLinkUserStatusFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834850
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ag:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenDeepLinkUserStatusFieldsModel;

    const/16 v1, 0x36

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenDeepLinkUserStatusFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenDeepLinkUserStatusFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ag:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenDeepLinkUserStatusFieldsModel;

    .line 834851
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ag:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenDeepLinkUserStatusFieldsModel;

    return-object v0
.end method

.method private ae()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenUserStatusModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834817
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ah:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenUserStatusModel;

    const/16 v1, 0x37

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenUserStatusModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenUserStatusModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ah:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenUserStatusModel;

    .line 834818
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ah:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenUserStatusModel;

    return-object v0
.end method

.method private af()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834844
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ai:Ljava/lang/String;

    const/16 v1, 0x38

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ai:Ljava/lang/String;

    .line 834845
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ai:Ljava/lang/String;

    return-object v0
.end method

.method private ag()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834254
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aj:Ljava/lang/String;

    const/16 v1, 0x39

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aj:Ljava/lang/String;

    .line 834255
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aj:Ljava/lang/String;

    return-object v0
.end method

.method private ah()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834252
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ak:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x3a

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ak:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 834253
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ak:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private ai()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834250
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->al:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    const/16 v1, 0x3b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->al:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 834251
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->al:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    return-object v0
.end method

.method private aj()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LinkTargetStoreDataFragModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834248
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->am:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LinkTargetStoreDataFragModel;

    const/16 v1, 0x3c

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LinkTargetStoreDataFragModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LinkTargetStoreDataFragModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->am:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LinkTargetStoreDataFragModel;

    .line 834249
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->am:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LinkTargetStoreDataFragModel;

    return-object v0
.end method

.method private ak()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834246
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->an:Ljava/lang/String;

    const/16 v1, 0x3d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->an:Ljava/lang/String;

    .line 834247
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->an:Ljava/lang/String;

    return-object v0
.end method

.method private al()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834244
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ao:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ao:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 834245
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ao:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method

.method private am()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834242
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ap:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x3f

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ap:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 834243
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ap:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private an()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834240
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aq:Ljava/lang/String;

    const/16 v1, 0x40

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aq:Ljava/lang/String;

    .line 834241
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aq:Ljava/lang/String;

    return-object v0
.end method

.method private ao()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 834238
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ar:Ljava/util/List;

    const/16 v1, 0x41

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ar:Ljava/util/List;

    .line 834239
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ar:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private ap()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834222
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->as:Ljava/lang/String;

    const/16 v1, 0x42

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->as:Ljava/lang/String;

    .line 834223
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->as:Ljava/lang/String;

    return-object v0
.end method

.method private aq()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834236
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->at:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;

    const/16 v1, 0x43

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->at:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;

    .line 834237
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->at:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;

    return-object v0
.end method

.method private ar()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834234
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->au:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/16 v1, 0x44

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->au:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 834235
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->au:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method private as()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834232
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->av:Ljava/lang/String;

    const/16 v1, 0x45

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->av:Ljava/lang/String;

    .line 834233
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->av:Ljava/lang/String;

    return-object v0
.end method

.method private at()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834230
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aw:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    const/16 v1, 0x46

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aw:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    .line 834231
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aw:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    return-object v0
.end method

.method private au()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 834228
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ax:Ljava/util/List;

    const/16 v1, 0x47

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ax:Ljava/util/List;

    .line 834229
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ax:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private av()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntityRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834226
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ay:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntityRangesFieldsModel;

    const/16 v1, 0x48

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntityRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntityRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ay:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntityRangesFieldsModel;

    .line 834227
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ay:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntityRangesFieldsModel;

    return-object v0
.end method

.method private aw()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834208
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->az:Ljava/lang/String;

    const/16 v1, 0x49

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->az:Ljava/lang/String;

    .line 834209
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->az:Ljava/lang/String;

    return-object v0
.end method

.method private ax()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834206
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aA:Ljava/lang/String;

    const/16 v1, 0x4a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aA:Ljava/lang/String;

    .line 834207
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aA:Ljava/lang/String;

    return-object v0
.end method

.method private ay()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834204
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aB:Ljava/lang/String;

    const/16 v1, 0x4b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aB:Ljava/lang/String;

    .line 834205
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aB:Ljava/lang/String;

    return-object v0
.end method

.method private az()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834202
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aC:Ljava/lang/String;

    const/16 v1, 0x4c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aC:Ljava/lang/String;

    .line 834203
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aC:Ljava/lang/String;

    return-object v0
.end method

.method private ba()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834200
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bi:Ljava/lang/String;

    const/16 v1, 0x6c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bi:Ljava/lang/String;

    .line 834201
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bi:Ljava/lang/String;

    return-object v0
.end method

.method private bb()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834196
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bj:Ljava/lang/String;

    const/16 v1, 0x6d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bj:Ljava/lang/String;

    .line 834197
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bj:Ljava/lang/String;

    return-object v0
.end method

.method private bc()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834210
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bk:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    const/16 v1, 0x6e

    const-class v2, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bk:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 834211
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bk:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    return-object v0
.end method

.method private bd()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834212
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bl:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x6f

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bl:Lcom/facebook/graphql/model/GraphQLStory;

    .line 834213
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bl:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method private be()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834214
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bm:Ljava/lang/String;

    const/16 v1, 0x70

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bm:Ljava/lang/String;

    .line 834215
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bm:Ljava/lang/String;

    return-object v0
.end method

.method private bf()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834216
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bn:Ljava/lang/String;

    const/16 v1, 0x71

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bn:Ljava/lang/String;

    .line 834217
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bn:Ljava/lang/String;

    return-object v0
.end method

.method private bg()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$SupportInboxActionLinkFieldsModel$SupportItemModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834218
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bo:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$SupportInboxActionLinkFieldsModel$SupportItemModel;

    const/16 v1, 0x72

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$SupportInboxActionLinkFieldsModel$SupportItemModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$SupportInboxActionLinkFieldsModel$SupportItemModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bo:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$SupportInboxActionLinkFieldsModel$SupportItemModel;

    .line 834219
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bo:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$SupportInboxActionLinkFieldsModel$SupportItemModel;

    return-object v0
.end method

.method private bh()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TaggedAndMentionedUsersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834220
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bp:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TaggedAndMentionedUsersModel;

    const/16 v1, 0x73

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TaggedAndMentionedUsersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TaggedAndMentionedUsersModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bp:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TaggedAndMentionedUsersModel;

    .line 834221
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bp:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TaggedAndMentionedUsersModel;

    return-object v0
.end method

.method private bi()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834198
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bq:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;

    const/16 v1, 0x74

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bq:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;

    .line 834199
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bq:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;

    return-object v0
.end method

.method private bj()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834224
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->br:Ljava/lang/String;

    const/16 v1, 0x75

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->br:Ljava/lang/String;

    .line 834225
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->br:Ljava/lang/String;

    return-object v0
.end method

.method private bk()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834755
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bs:Ljava/lang/String;

    const/16 v1, 0x76

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bs:Ljava/lang/String;

    .line 834756
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bs:Ljava/lang/String;

    return-object v0
.end method

.method private bl()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TopicModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834783
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bt:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TopicModel;

    const/16 v1, 0x77

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TopicModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TopicModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bt:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TopicModel;

    .line 834784
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bt:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TopicModel;

    return-object v0
.end method

.method private bm()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834781
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bu:Ljava/lang/String;

    const/16 v1, 0x78

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bu:Ljava/lang/String;

    .line 834782
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bu:Ljava/lang/String;

    return-object v0
.end method

.method private bn()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834779
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bv:Ljava/lang/String;

    const/16 v1, 0x79

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bv:Ljava/lang/String;

    .line 834780
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bv:Ljava/lang/String;

    return-object v0
.end method

.method private bo()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$VideoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834777
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bw:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$VideoModel;

    const/16 v1, 0x7a

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$VideoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$VideoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bw:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$VideoModel;

    .line 834778
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bw:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$VideoModel;

    return-object v0
.end method

.method private bp()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$VideoAnnotationFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 834775
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bx:Ljava/util/List;

    const/16 v1, 0x7b

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$VideoAnnotationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bx:Ljava/util/List;

    .line 834776
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bx:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bq()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834773
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->by:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    const/16 v1, 0x7c

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->by:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    .line 834774
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->by:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834771
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 834772
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MisinformationWarningActionLinkFieldsModel$ActionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 834769
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MisinformationWarningActionLinkFieldsModel$ActionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->g:Ljava/util/List;

    .line 834770
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$AdModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834767
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$AdModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$AdModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$AdModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$AdModel;

    .line 834768
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$AdModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834765
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->i:Ljava/lang/String;

    .line 834766
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834763
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->j:Ljava/lang/String;

    .line 834764
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834761
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->k:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->k:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    .line 834762
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->k:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834759
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->l:Ljava/lang/String;

    .line 834760
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834757
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->m:Ljava/lang/String;

    .line 834758
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834256
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->n:Ljava/lang/String;

    .line 834257
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834753
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->q:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->q:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    .line 834754
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->q:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    return-object v0
.end method

.method private t()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834751
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->r:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->r:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;

    .line 834752
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->r:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834749
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->v:Ljava/lang/String;

    .line 834750
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->v:Ljava/lang/String;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834747
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->w:Ljava/lang/String;

    .line 834748
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->w:Ljava/lang/String;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834745
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->x:Ljava/lang/String;

    .line 834746
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->x:Ljava/lang/String;

    return-object v0
.end method

.method private x()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$DescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834743
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->z:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$DescriptionModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$DescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$DescriptionModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->z:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$DescriptionModel;

    .line 834744
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->z:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$DescriptionModel;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 834741
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->A:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->A:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 834742
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->A:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    return-object v0
.end method

.method private z()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 834739
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->B:Ljava/util/List;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->B:Ljava/util/List;

    .line 834740
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->B:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 116

    .prologue
    .line 834497
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 834498
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 834499
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 834500
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->k()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 834501
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$AdModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 834502
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->m()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 834503
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->n()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 834504
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 834505
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->p()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 834506
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->q()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 834507
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->r()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 834508
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->s()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 834509
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->t()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 834510
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->u()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 834511
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->v()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 834512
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->w()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 834513
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->x()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$DescriptionModel;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 834514
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->y()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    .line 834515
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->z()LX/0Px;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v19

    .line 834516
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->A()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 834517
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->B()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 834518
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->C()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 834519
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->D()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 834520
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->E()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 834521
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->F()Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v25

    .line 834522
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->G()LX/0Px;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v26

    .line 834523
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->H()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 834524
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->I()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 834525
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->J()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$EventModel;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 834526
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->K()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    .line 834527
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->L()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 834528
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->M()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$FeaturedInstantArticleElementModel;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 834529
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->N()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupToggleCommentingActionLinkFieldsModel$FeedbackModel;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 834530
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->O()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v34

    .line 834531
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->P()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    .line 834532
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Q()LX/0Px;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v36

    .line 834533
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->R()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 834534
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->S()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v38

    .line 834535
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->T()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupJoinActionLinkFieldsModel$GroupModel;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 834536
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->U()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    .line 834537
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->V()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$InfoModel;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 834538
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->W()LX/0Px;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v42

    .line 834539
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->X()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$InstantArticleModel;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 834540
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Y()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 834541
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Z()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v45

    .line 834542
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aa()Ljava/lang/String;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v46

    .line 834543
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ab()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 834544
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ac()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    .line 834545
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ad()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenDeepLinkUserStatusFieldsModel;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 834546
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ae()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenUserStatusModel;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 834547
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->af()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v51

    .line 834548
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ag()Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v52

    .line 834549
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ah()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 834550
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ai()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v54

    .line 834551
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aj()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LinkTargetStoreDataFragModel;

    move-result-object v55

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v55

    .line 834552
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ak()Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v56

    .line 834553
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->al()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v57

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v57

    .line 834554
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->am()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v58

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v58

    .line 834555
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->an()Ljava/lang/String;

    move-result-object v59

    move-object/from16 v0, p1

    move-object/from16 v1, v59

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v59

    .line 834556
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ao()LX/0Px;

    move-result-object v60

    move-object/from16 v0, p1

    move-object/from16 v1, v60

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v60

    .line 834557
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ap()Ljava/lang/String;

    move-result-object v61

    move-object/from16 v0, p1

    move-object/from16 v1, v61

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v61

    .line 834558
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aq()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;

    move-result-object v62

    move-object/from16 v0, p1

    move-object/from16 v1, v62

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v62

    .line 834559
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ar()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v63

    move-object/from16 v0, p1

    move-object/from16 v1, v63

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v63

    .line 834560
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->as()Ljava/lang/String;

    move-result-object v64

    move-object/from16 v0, p1

    move-object/from16 v1, v64

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v64

    .line 834561
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->at()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    move-result-object v65

    move-object/from16 v0, p1

    move-object/from16 v1, v65

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v65

    .line 834562
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->au()LX/0Px;

    move-result-object v66

    move-object/from16 v0, p1

    move-object/from16 v1, v66

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v66

    .line 834563
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->av()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntityRangesFieldsModel;

    move-result-object v67

    move-object/from16 v0, p1

    move-object/from16 v1, v67

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v67

    .line 834564
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aw()Ljava/lang/String;

    move-result-object v68

    move-object/from16 v0, p1

    move-object/from16 v1, v68

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v68

    .line 834565
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ax()Ljava/lang/String;

    move-result-object v69

    move-object/from16 v0, p1

    move-object/from16 v1, v69

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v69

    .line 834566
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ay()Ljava/lang/String;

    move-result-object v70

    move-object/from16 v0, p1

    move-object/from16 v1, v70

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v70

    .line 834567
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->az()Ljava/lang/String;

    move-result-object v71

    move-object/from16 v0, p1

    move-object/from16 v1, v71

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v71

    .line 834568
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aA()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OfferStoryActionLinkFieldsModel$OfferModel;

    move-result-object v72

    move-object/from16 v0, p1

    move-object/from16 v1, v72

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v72

    .line 834569
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aB()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PageModel;

    move-result-object v73

    move-object/from16 v0, p1

    move-object/from16 v1, v73

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v73

    .line 834570
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aC()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;

    move-result-object v74

    move-object/from16 v0, p1

    move-object/from16 v1, v74

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v74

    .line 834571
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aD()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ParentStoryModel;

    move-result-object v75

    move-object/from16 v0, p1

    move-object/from16 v1, v75

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v75

    .line 834572
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aE()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    move-result-object v76

    move-object/from16 v0, p1

    move-object/from16 v1, v76

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v76

    .line 834573
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aF()Ljava/lang/String;

    move-result-object v77

    move-object/from16 v0, p1

    move-object/from16 v1, v77

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v77

    .line 834574
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aG()Ljava/lang/String;

    move-result-object v78

    move-object/from16 v0, p1

    move-object/from16 v1, v78

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v78

    .line 834575
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aH()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PrivacyScopeModel;

    move-result-object v79

    move-object/from16 v0, p1

    move-object/from16 v1, v79

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v79

    .line 834576
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aI()Ljava/lang/String;

    move-result-object v80

    move-object/from16 v0, p1

    move-object/from16 v1, v80

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v80

    .line 834577
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aJ()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ProfileModel;

    move-result-object v81

    move-object/from16 v0, p1

    move-object/from16 v1, v81

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v81

    .line 834578
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aK()Ljava/lang/String;

    move-result-object v82

    move-object/from16 v0, p1

    move-object/from16 v1, v82

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v82

    .line 834579
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aL()Ljava/lang/String;

    move-result-object v83

    move-object/from16 v0, p1

    move-object/from16 v1, v83

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v83

    .line 834580
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aM()Ljava/lang/String;

    move-result-object v84

    move-object/from16 v0, p1

    move-object/from16 v1, v84

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v84

    .line 834581
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aN()Ljava/lang/String;

    move-result-object v85

    move-object/from16 v0, p1

    move-object/from16 v1, v85

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v85

    .line 834582
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aO()Ljava/lang/String;

    move-result-object v86

    move-object/from16 v0, p1

    move-object/from16 v1, v86

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v86

    .line 834583
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aP()Ljava/lang/String;

    move-result-object v87

    move-object/from16 v0, p1

    move-object/from16 v1, v87

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v87

    .line 834584
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aQ()Ljava/lang/String;

    move-result-object v88

    move-object/from16 v0, p1

    move-object/from16 v1, v88

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v88

    .line 834585
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aR()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    move-result-object v89

    move-object/from16 v0, p1

    move-object/from16 v1, v89

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v89

    .line 834586
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aS()Ljava/lang/String;

    move-result-object v90

    move-object/from16 v0, p1

    move-object/from16 v1, v90

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v90

    .line 834587
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aT()Ljava/lang/String;

    move-result-object v91

    move-object/from16 v0, p1

    move-object/from16 v1, v91

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v91

    .line 834588
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aU()Ljava/lang/String;

    move-result-object v92

    move-object/from16 v0, p1

    move-object/from16 v1, v92

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v92

    .line 834589
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aV()Ljava/lang/String;

    move-result-object v93

    move-object/from16 v0, p1

    move-object/from16 v1, v93

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v93

    .line 834590
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aW()Ljava/lang/String;

    move-result-object v94

    move-object/from16 v0, p1

    move-object/from16 v1, v94

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v94

    .line 834591
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aX()Ljava/lang/String;

    move-result-object v95

    move-object/from16 v0, p1

    move-object/from16 v1, v95

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v95

    .line 834592
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aY()Ljava/lang/String;

    move-result-object v96

    move-object/from16 v0, p1

    move-object/from16 v1, v96

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v96

    .line 834593
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aZ()Ljava/lang/String;

    move-result-object v97

    move-object/from16 v0, p1

    move-object/from16 v1, v97

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v97

    .line 834594
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ba()Ljava/lang/String;

    move-result-object v98

    move-object/from16 v0, p1

    move-object/from16 v1, v98

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v98

    .line 834595
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bb()Ljava/lang/String;

    move-result-object v99

    move-object/from16 v0, p1

    move-object/from16 v1, v99

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v99

    .line 834596
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bc()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v100

    move-object/from16 v0, p1

    move-object/from16 v1, v100

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v100

    .line 834597
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bd()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v101

    move-object/from16 v0, p1

    move-object/from16 v1, v101

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v101

    .line 834598
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->be()Ljava/lang/String;

    move-result-object v102

    move-object/from16 v0, p1

    move-object/from16 v1, v102

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v102

    .line 834599
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bf()Ljava/lang/String;

    move-result-object v103

    move-object/from16 v0, p1

    move-object/from16 v1, v103

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v103

    .line 834600
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bg()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$SupportInboxActionLinkFieldsModel$SupportItemModel;

    move-result-object v104

    move-object/from16 v0, p1

    move-object/from16 v1, v104

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v104

    .line 834601
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bh()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TaggedAndMentionedUsersModel;

    move-result-object v105

    move-object/from16 v0, p1

    move-object/from16 v1, v105

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v105

    .line 834602
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bi()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;

    move-result-object v106

    move-object/from16 v0, p1

    move-object/from16 v1, v106

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v106

    .line 834603
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bj()Ljava/lang/String;

    move-result-object v107

    move-object/from16 v0, p1

    move-object/from16 v1, v107

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v107

    .line 834604
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bk()Ljava/lang/String;

    move-result-object v108

    move-object/from16 v0, p1

    move-object/from16 v1, v108

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v108

    .line 834605
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bl()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TopicModel;

    move-result-object v109

    move-object/from16 v0, p1

    move-object/from16 v1, v109

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v109

    .line 834606
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bm()Ljava/lang/String;

    move-result-object v110

    move-object/from16 v0, p1

    move-object/from16 v1, v110

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v110

    .line 834607
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bn()Ljava/lang/String;

    move-result-object v111

    move-object/from16 v0, p1

    move-object/from16 v1, v111

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v111

    .line 834608
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bo()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$VideoModel;

    move-result-object v112

    move-object/from16 v0, p1

    move-object/from16 v1, v112

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v112

    .line 834609
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bp()LX/0Px;

    move-result-object v113

    move-object/from16 v0, p1

    move-object/from16 v1, v113

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v113

    .line 834610
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bq()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    move-result-object v114

    move-object/from16 v0, p1

    move-object/from16 v1, v114

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v114

    .line 834611
    const/16 v115, 0x7d

    move-object/from16 v0, p1

    move/from16 v1, v115

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 834612
    const/16 v115, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v115

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 834613
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 834614
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 834615
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 834616
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 834617
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 834618
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 834619
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 834620
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 834621
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 834622
    const/16 v2, 0xa

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->o:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 834623
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->p:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 834624
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 834625
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 834626
    const/16 v2, 0xe

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->s:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 834627
    const/16 v2, 0xf

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->t:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 834628
    const/16 v2, 0x10

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->u:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 834629
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 834630
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 834631
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834632
    const/16 v3, 0x14

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->y:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 834633
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834634
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834635
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834636
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834637
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834638
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834639
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834640
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834641
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834642
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834643
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834644
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834645
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834646
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834647
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834648
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834649
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834650
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834651
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834652
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834653
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834654
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834655
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834656
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834657
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834658
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834659
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834660
    const/16 v2, 0x30

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aa:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 834661
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834662
    const/16 v2, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834663
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834664
    const/16 v2, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834665
    const/16 v2, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834666
    const/16 v2, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834667
    const/16 v2, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834668
    const/16 v2, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834669
    const/16 v2, 0x39

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834670
    const/16 v2, 0x3a

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834671
    const/16 v2, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834672
    const/16 v2, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834673
    const/16 v2, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834674
    const/16 v2, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834675
    const/16 v2, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834676
    const/16 v2, 0x40

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834677
    const/16 v2, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834678
    const/16 v2, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834679
    const/16 v2, 0x43

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834680
    const/16 v2, 0x44

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834681
    const/16 v2, 0x45

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834682
    const/16 v2, 0x46

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834683
    const/16 v2, 0x47

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834684
    const/16 v2, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v67

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834685
    const/16 v2, 0x49

    move-object/from16 v0, p1

    move/from16 v1, v68

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834686
    const/16 v2, 0x4a

    move-object/from16 v0, p1

    move/from16 v1, v69

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834687
    const/16 v2, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v70

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834688
    const/16 v2, 0x4c

    move-object/from16 v0, p1

    move/from16 v1, v71

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834689
    const/16 v2, 0x4d

    move-object/from16 v0, p1

    move/from16 v1, v72

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834690
    const/16 v2, 0x4e

    move-object/from16 v0, p1

    move/from16 v1, v73

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834691
    const/16 v2, 0x4f

    move-object/from16 v0, p1

    move/from16 v1, v74

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834692
    const/16 v2, 0x50

    move-object/from16 v0, p1

    move/from16 v1, v75

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834693
    const/16 v2, 0x51

    move-object/from16 v0, p1

    move/from16 v1, v76

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834694
    const/16 v2, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v77

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834695
    const/16 v2, 0x53

    move-object/from16 v0, p1

    move/from16 v1, v78

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834696
    const/16 v2, 0x54

    move-object/from16 v0, p1

    move/from16 v1, v79

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834697
    const/16 v2, 0x55

    move-object/from16 v0, p1

    move/from16 v1, v80

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834698
    const/16 v2, 0x56

    move-object/from16 v0, p1

    move/from16 v1, v81

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834699
    const/16 v2, 0x57

    move-object/from16 v0, p1

    move/from16 v1, v82

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834700
    const/16 v2, 0x58

    move-object/from16 v0, p1

    move/from16 v1, v83

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834701
    const/16 v2, 0x59

    move-object/from16 v0, p1

    move/from16 v1, v84

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834702
    const/16 v2, 0x5a

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aQ:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 834703
    const/16 v2, 0x5b

    move-object/from16 v0, p1

    move/from16 v1, v85

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834704
    const/16 v2, 0x5c

    move-object/from16 v0, p1

    move/from16 v1, v86

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834705
    const/16 v2, 0x5d

    move-object/from16 v0, p1

    move/from16 v1, v87

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834706
    const/16 v2, 0x5e

    move-object/from16 v0, p1

    move/from16 v1, v88

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834707
    const/16 v2, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v89

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834708
    const/16 v2, 0x60

    move-object/from16 v0, p1

    move/from16 v1, v90

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834709
    const/16 v2, 0x61

    move-object/from16 v0, p1

    move/from16 v1, v91

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834710
    const/16 v2, 0x62

    move-object/from16 v0, p1

    move/from16 v1, v92

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834711
    const/16 v2, 0x63

    move-object/from16 v0, p1

    move/from16 v1, v93

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834712
    const/16 v2, 0x64

    move-object/from16 v0, p1

    move/from16 v1, v94

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834713
    const/16 v2, 0x65

    move-object/from16 v0, p1

    move/from16 v1, v95

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834714
    const/16 v2, 0x66

    move-object/from16 v0, p1

    move/from16 v1, v96

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834715
    const/16 v2, 0x67

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bd:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 834716
    const/16 v2, 0x68

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->be:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 834717
    const/16 v2, 0x69

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bf:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 834718
    const/16 v2, 0x6a

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bg:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 834719
    const/16 v2, 0x6b

    move-object/from16 v0, p1

    move/from16 v1, v97

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834720
    const/16 v2, 0x6c

    move-object/from16 v0, p1

    move/from16 v1, v98

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834721
    const/16 v2, 0x6d

    move-object/from16 v0, p1

    move/from16 v1, v99

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834722
    const/16 v2, 0x6e

    move-object/from16 v0, p1

    move/from16 v1, v100

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834723
    const/16 v2, 0x6f

    move-object/from16 v0, p1

    move/from16 v1, v101

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834724
    const/16 v2, 0x70

    move-object/from16 v0, p1

    move/from16 v1, v102

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834725
    const/16 v2, 0x71

    move-object/from16 v0, p1

    move/from16 v1, v103

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834726
    const/16 v2, 0x72

    move-object/from16 v0, p1

    move/from16 v1, v104

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834727
    const/16 v2, 0x73

    move-object/from16 v0, p1

    move/from16 v1, v105

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834728
    const/16 v2, 0x74

    move-object/from16 v0, p1

    move/from16 v1, v106

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834729
    const/16 v2, 0x75

    move-object/from16 v0, p1

    move/from16 v1, v107

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834730
    const/16 v2, 0x76

    move-object/from16 v0, p1

    move/from16 v1, v108

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834731
    const/16 v2, 0x77

    move-object/from16 v0, p1

    move/from16 v1, v109

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834732
    const/16 v2, 0x78

    move-object/from16 v0, p1

    move/from16 v1, v110

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834733
    const/16 v2, 0x79

    move-object/from16 v0, p1

    move/from16 v1, v111

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834734
    const/16 v2, 0x7a

    move-object/from16 v0, p1

    move/from16 v1, v112

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834735
    const/16 v2, 0x7b

    move-object/from16 v0, p1

    move/from16 v1, v113

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834736
    const/16 v2, 0x7c

    move-object/from16 v0, p1

    move/from16 v1, v114

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 834737
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 834738
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 834279
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 834280
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2b

    .line 834281
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 834282
    if-eqz v1, :cond_2b

    .line 834283
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834284
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->g:Ljava/util/List;

    move-object v1, v0

    .line 834285
    :goto_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$AdModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 834286
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$AdModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$AdModel;

    .line 834287
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$AdModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 834288
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834289
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$AdModel;

    .line 834290
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 834291
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    .line 834292
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 834293
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834294
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->k:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    .line 834295
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->s()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 834296
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->s()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    .line 834297
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->s()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 834298
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834299
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->q:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    .line 834300
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->t()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 834301
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->t()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;

    .line 834302
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->t()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 834303
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834304
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->r:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ApplicationModel;

    .line 834305
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->x()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 834306
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->x()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$DescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$DescriptionModel;

    .line 834307
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->x()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$DescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 834308
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834309
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->z:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$DescriptionModel;

    .line 834310
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->z()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 834311
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->z()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 834312
    if-eqz v2, :cond_5

    .line 834313
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834314
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->B:Ljava/util/List;

    move-object v1, v0

    .line 834315
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->G()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 834316
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->G()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 834317
    if-eqz v2, :cond_6

    .line 834318
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834319
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->I:Ljava/util/List;

    move-object v1, v0

    .line 834320
    :cond_6
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->J()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$EventModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 834321
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->J()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$EventModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$EventModel;

    .line 834322
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->J()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$EventModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 834323
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834324
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->L:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$EventModel;

    .line 834325
    :cond_7
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->M()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$FeaturedInstantArticleElementModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 834326
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->M()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$FeaturedInstantArticleElementModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$FeaturedInstantArticleElementModel;

    .line 834327
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->M()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$FeaturedInstantArticleElementModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 834328
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834329
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->O:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$FeaturedInstantArticleElementModel;

    .line 834330
    :cond_8
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->N()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupToggleCommentingActionLinkFieldsModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 834331
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->N()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupToggleCommentingActionLinkFieldsModel$FeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupToggleCommentingActionLinkFieldsModel$FeedbackModel;

    .line 834332
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->N()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupToggleCommentingActionLinkFieldsModel$FeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 834333
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834334
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->P:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupToggleCommentingActionLinkFieldsModel$FeedbackModel;

    .line 834335
    :cond_9
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->T()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupJoinActionLinkFieldsModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 834336
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->T()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupJoinActionLinkFieldsModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupJoinActionLinkFieldsModel$GroupModel;

    .line 834337
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->T()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupJoinActionLinkFieldsModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 834338
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834339
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->V:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$GroupJoinActionLinkFieldsModel$GroupModel;

    .line 834340
    :cond_a
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->V()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$InfoModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 834341
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->V()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$InfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$InfoModel;

    .line 834342
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->V()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$InfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 834343
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834344
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->X:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$InfoModel;

    .line 834345
    :cond_b
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->W()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 834346
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->W()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 834347
    if-eqz v2, :cond_c

    .line 834348
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834349
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Y:Ljava/util/List;

    move-object v1, v0

    .line 834350
    :cond_c
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->X()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$InstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 834351
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->X()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$InstantArticleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$InstantArticleModel;

    .line 834352
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->X()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$InstantArticleModel;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 834353
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834354
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Z:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$InstantArticleModel;

    .line 834355
    :cond_d
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Y()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 834356
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Y()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    .line 834357
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->Y()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 834358
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834359
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ab:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    .line 834360
    :cond_e
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ab()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 834361
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ab()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel;

    .line 834362
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ab()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 834363
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834364
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ae:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel;

    .line 834365
    :cond_f
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ad()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenDeepLinkUserStatusFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 834366
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ad()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenDeepLinkUserStatusFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenDeepLinkUserStatusFieldsModel;

    .line 834367
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ad()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenDeepLinkUserStatusFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 834368
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834369
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ag:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenDeepLinkUserStatusFieldsModel;

    .line 834370
    :cond_10
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ae()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenUserStatusModel;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 834371
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ae()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenUserStatusModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenUserStatusModel;

    .line 834372
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ae()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenUserStatusModel;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 834373
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834374
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ah:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenUserStatusModel;

    .line 834375
    :cond_11
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ah()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 834376
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ah()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 834377
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ah()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 834378
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834379
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ak:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 834380
    :cond_12
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aj()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LinkTargetStoreDataFragModel;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 834381
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aj()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LinkTargetStoreDataFragModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LinkTargetStoreDataFragModel;

    .line 834382
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aj()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LinkTargetStoreDataFragModel;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 834383
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834384
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->am:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LinkTargetStoreDataFragModel;

    .line 834385
    :cond_13
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->am()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 834386
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->am()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 834387
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->am()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 834388
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834389
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ap:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 834390
    :cond_14
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aq()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 834391
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aq()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;

    .line 834392
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aq()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 834393
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834394
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->at:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;

    .line 834395
    :cond_15
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ar()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 834396
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ar()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 834397
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ar()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 834398
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834399
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->au:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 834400
    :cond_16
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->at()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 834401
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->at()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    .line 834402
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->at()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 834403
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834404
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aw:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MessengerExtensionInfoFieldsModel$MessengerExtensionsUserProfileModel;

    .line 834405
    :cond_17
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->av()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntityRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 834406
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->av()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntityRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntityRangesFieldsModel;

    .line 834407
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->av()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntityRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 834408
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834409
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->ay:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntityRangesFieldsModel;

    .line 834410
    :cond_18
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aA()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OfferStoryActionLinkFieldsModel$OfferModel;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 834411
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aA()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OfferStoryActionLinkFieldsModel$OfferModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OfferStoryActionLinkFieldsModel$OfferModel;

    .line 834412
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aA()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OfferStoryActionLinkFieldsModel$OfferModel;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 834413
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834414
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aD:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OfferStoryActionLinkFieldsModel$OfferModel;

    .line 834415
    :cond_19
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aB()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 834416
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aB()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PageModel;

    .line 834417
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aB()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 834418
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834419
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aE:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PageModel;

    .line 834420
    :cond_1a
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aC()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 834421
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aC()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;

    .line 834422
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aC()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 834423
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834424
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aF:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$PageOutcomeButtonActionLinkFieldsModel$PageOutcomeButtonModel;

    .line 834425
    :cond_1b
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aD()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ParentStoryModel;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 834426
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aD()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ParentStoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ParentStoryModel;

    .line 834427
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aD()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ParentStoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 834428
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834429
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aG:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ParentStoryModel;

    .line 834430
    :cond_1c
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aE()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 834431
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aE()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    .line 834432
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aE()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 834433
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834434
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aH:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    .line 834435
    :cond_1d
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aH()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PrivacyScopeModel;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 834436
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aH()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PrivacyScopeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PrivacyScopeModel;

    .line 834437
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aH()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PrivacyScopeModel;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 834438
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834439
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aK:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$PrivacyScopeModel;

    .line 834440
    :cond_1e
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aJ()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 834441
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aJ()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ProfileModel;

    .line 834442
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aJ()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 834443
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834444
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aM:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$ProfileModel;

    .line 834445
    :cond_1f
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aR()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 834446
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aR()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    .line 834447
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aR()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 834448
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834449
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aV:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    .line 834450
    :cond_20
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bc()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 834451
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bc()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 834452
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bc()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 834453
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834454
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bk:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 834455
    :cond_21
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bd()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 834456
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bd()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 834457
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bd()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 834458
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834459
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bl:Lcom/facebook/graphql/model/GraphQLStory;

    .line 834460
    :cond_22
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bg()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$SupportInboxActionLinkFieldsModel$SupportItemModel;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 834461
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bg()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$SupportInboxActionLinkFieldsModel$SupportItemModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$SupportInboxActionLinkFieldsModel$SupportItemModel;

    .line 834462
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bg()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$SupportInboxActionLinkFieldsModel$SupportItemModel;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 834463
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834464
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bo:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$SupportInboxActionLinkFieldsModel$SupportItemModel;

    .line 834465
    :cond_23
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bh()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TaggedAndMentionedUsersModel;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 834466
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bh()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TaggedAndMentionedUsersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TaggedAndMentionedUsersModel;

    .line 834467
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bh()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TaggedAndMentionedUsersModel;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 834468
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834469
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bp:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TaggedAndMentionedUsersModel;

    .line 834470
    :cond_24
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bi()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 834471
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bi()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;

    .line 834472
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bi()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 834473
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834474
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bq:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EventCreateActionLinkFieldsModel$TemporalEventInfoModel;

    .line 834475
    :cond_25
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bl()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TopicModel;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 834476
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bl()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TopicModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TopicModel;

    .line 834477
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bl()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TopicModel;

    move-result-object v2

    if-eq v2, v0, :cond_26

    .line 834478
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834479
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bt:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$TopicModel;

    .line 834480
    :cond_26
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bo()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$VideoModel;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 834481
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bo()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$VideoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$VideoModel;

    .line 834482
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bo()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$VideoModel;

    move-result-object v2

    if-eq v2, v0, :cond_27

    .line 834483
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834484
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bw:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel$VideoModel;

    .line 834485
    :cond_27
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bp()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 834486
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bp()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 834487
    if-eqz v2, :cond_28

    .line 834488
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834489
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bx:Ljava/util/List;

    move-object v1, v0

    .line 834490
    :cond_28
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bq()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 834491
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bq()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    .line 834492
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bq()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    move-result-object v2

    if-eq v2, v0, :cond_29

    .line 834493
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    .line 834494
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->by:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ScheduledLiveAttachmentSubscribeActionLinkFieldsModel$VideoBroadcastScheduleModel;

    .line 834495
    :cond_29
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 834496
    if-nez v1, :cond_2a

    :goto_1
    return-object p0

    :cond_2a
    move-object p0, v1

    goto :goto_1

    :cond_2b
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 834265
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 834266
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->o:I

    .line 834267
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->p:I

    .line 834268
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->s:I

    .line 834269
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->t:Z

    .line 834270
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->u:Z

    .line 834271
    const/16 v0, 0x14

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->y:J

    .line 834272
    const/16 v0, 0x30

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aa:Z

    .line 834273
    const/16 v0, 0x5a

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->aQ:I

    .line 834274
    const/16 v0, 0x67

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bd:Z

    .line 834275
    const/16 v0, 0x68

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->be:Z

    .line 834276
    const/16 v0, 0x69

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bf:Z

    .line 834277
    const/16 v0, 0x6a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;->bg:Z

    .line 834278
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 834262
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$NewsFeedDefaultsStoryActionLinkFieldsModel;-><init>()V

    .line 834263
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 834264
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 834261
    const v0, -0x4cdf0dcf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 834260
    const v0, -0x6829c9fb

    return v0
.end method
