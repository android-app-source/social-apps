.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 836056
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 836057
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 836033
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 836035
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 836036
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 836037
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 836038
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 836039
    if-eqz v2, :cond_0

    .line 836040
    const-string p0, "app_icon"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 836041
    invoke-static {v1, v2, p1}, LX/57P;->a(LX/15i;ILX/0nX;)V

    .line 836042
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 836043
    if-eqz v2, :cond_1

    .line 836044
    const-string p0, "application"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 836045
    invoke-static {v1, v2, p1}, LX/57Q;->a(LX/15i;ILX/0nX;)V

    .line 836046
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 836047
    if-eqz v2, :cond_2

    .line 836048
    const-string p0, "description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 836049
    invoke-static {v1, v2, p1}, LX/57R;->a(LX/15i;ILX/0nX;)V

    .line 836050
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 836051
    if-eqz v2, :cond_3

    .line 836052
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 836053
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 836054
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 836055
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 836034
    check-cast p1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
