.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MisinformationWarningActionLinkFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MisinformationWarningActionLinkFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 833121
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MisinformationWarningActionLinkFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MisinformationWarningActionLinkFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MisinformationWarningActionLinkFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 833122
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 833120
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MisinformationWarningActionLinkFieldsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 833078
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 833079
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x5

    .line 833080
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 833081
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 833082
    if-eqz v2, :cond_0

    .line 833083
    const-string v3, "actions"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 833084
    invoke-static {v1, v2, p1, p2}, LX/56v;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 833085
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 833086
    if-eqz v2, :cond_1

    .line 833087
    const-string v3, "alert_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 833088
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 833089
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 833090
    if-eqz v2, :cond_2

    .line 833091
    const-string v3, "cta_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 833092
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 833093
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 833094
    if-eqz v2, :cond_3

    .line 833095
    const-string v3, "dispute_form_uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 833096
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 833097
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 833098
    if-eqz v2, :cond_4

    .line 833099
    const-string v3, "dispute_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 833100
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 833101
    :cond_4
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 833102
    if-eqz v2, :cond_5

    .line 833103
    const-string v2, "link_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 833104
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 833105
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 833106
    if-eqz v2, :cond_6

    .line 833107
    const-string v3, "reshare_alert_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 833108
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 833109
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 833110
    if-eqz v2, :cond_7

    .line 833111
    const-string v3, "reshare_alert_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 833112
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 833113
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 833114
    if-eqz v2, :cond_8

    .line 833115
    const-string v3, "subtitle"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 833116
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 833117
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 833118
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 833119
    check-cast p1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MisinformationWarningActionLinkFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MisinformationWarningActionLinkFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$MisinformationWarningActionLinkFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
