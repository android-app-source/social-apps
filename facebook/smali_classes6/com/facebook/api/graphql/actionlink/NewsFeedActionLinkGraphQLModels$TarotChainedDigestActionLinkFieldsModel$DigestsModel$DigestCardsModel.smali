.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x32060c6d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$PhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$VideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 836644
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 836643
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 836641
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 836642
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836638
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 836639
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 836640
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836636
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->f:Ljava/lang/String;

    .line 836637
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836634
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->g:Ljava/lang/String;

    .line 836635
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836632
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->h:Ljava/lang/String;

    .line 836633
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$PhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836645
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$PhotoModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$PhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$PhotoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$PhotoModel;

    .line 836646
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$PhotoModel;

    return-object v0
.end method

.method private o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$VideoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836630
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->j:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$VideoModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$VideoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$VideoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->j:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$VideoModel;

    .line 836631
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->j:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$VideoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 836595
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 836596
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 836597
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 836598
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 836599
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 836600
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->n()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$PhotoModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 836601
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$VideoModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 836602
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 836603
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 836604
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 836605
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 836606
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 836607
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 836608
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 836609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 836610
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 836617
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 836618
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->n()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 836619
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->n()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$PhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$PhotoModel;

    .line 836620
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->n()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$PhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 836621
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;

    .line 836622
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$PhotoModel;

    .line 836623
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$VideoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 836624
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$VideoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$VideoModel;

    .line 836625
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$VideoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 836626
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;

    .line 836627
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->j:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentVideoCardModel$VideoModel;

    .line 836628
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 836629
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836616
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 836613
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotChainedDigestActionLinkFieldsModel$DigestsModel$DigestCardsModel;-><init>()V

    .line 836614
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 836615
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 836612
    const v0, 0x622c8568

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 836611
    const v0, 0x5539525a

    return v0
.end method
