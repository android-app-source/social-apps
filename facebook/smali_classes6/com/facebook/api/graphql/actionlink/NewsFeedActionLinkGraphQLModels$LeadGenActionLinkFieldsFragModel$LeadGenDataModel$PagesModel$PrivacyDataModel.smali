.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xe13154d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 831204
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 831205
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 831206
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 831207
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 831208
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel;->e:Ljava/lang/String;

    .line 831209
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 831210
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel;->f:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel;->f:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    .line 831211
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel;->f:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 831212
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 831213
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 831214
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel;->j()Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 831215
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 831216
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 831217
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 831218
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 831219
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 831220
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 831221
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 831222
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 831223
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenActionLinkFieldsFragModel$LeadGenDataModel$PagesModel$PrivacyDataModel;-><init>()V

    .line 831224
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 831225
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 831226
    const v0, 0x65a9572b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 831227
    const v0, -0x7625412a

    return v0
.end method
