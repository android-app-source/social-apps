.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x17640fc8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$ApplicationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 836102
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 836058
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 836100
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 836101
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836098
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    .line 836099
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    return-object v0
.end method

.method private j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$ApplicationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836096
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$ApplicationModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$ApplicationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$ApplicationModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$ApplicationModel;

    .line 836097
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$ApplicationModel;

    return-object v0
.end method

.method private k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$DescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836094
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$DescriptionModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$DescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$DescriptionModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$DescriptionModel;

    .line 836095
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$DescriptionModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 836103
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->h:Ljava/lang/String;

    .line 836104
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 836082
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 836083
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 836084
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$ApplicationModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 836085
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$DescriptionModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 836086
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 836087
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 836088
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 836089
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 836090
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 836091
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 836092
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 836093
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 836064
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 836065
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 836066
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    .line 836067
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 836068
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;

    .line 836069
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$AppIconModel;

    .line 836070
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$ApplicationModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 836071
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$ApplicationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$ApplicationModel;

    .line 836072
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->j()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$ApplicationModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 836073
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;

    .line 836074
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->f:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$ApplicationModel;

    .line 836075
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 836076
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$DescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$DescriptionModel;

    .line 836077
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$DescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 836078
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;

    .line 836079
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;->g:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel$DescriptionModel;

    .line 836080
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 836081
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 836061
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$QuicksilverActionLinkFieldsModel;-><init>()V

    .line 836062
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 836063
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 836060
    const v0, 0x5b26a1e1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 836059
    const v0, 0x1172f9ae

    return v0
.end method
