.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/55r;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x13672903
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$RepresentedProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 830457
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 830456
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 830454
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 830455
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830452
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->e:Ljava/lang/String;

    .line 830453
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830450
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 830451
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$RepresentedProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830448
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$RepresentedProfileModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$RepresentedProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$RepresentedProfileModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$RepresentedProfileModel;

    .line 830449
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$RepresentedProfileModel;

    return-object v0
.end method

.method private m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830446
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->i:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->i:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 830447
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->i:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 830433
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 830434
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 830435
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 830436
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$RepresentedProfileModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 830437
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 830438
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 830439
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 830440
    const/4 v0, 0x1

    iget v4, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->f:I

    invoke-virtual {p1, v0, v4, v5}, LX/186;->a(III)V

    .line 830441
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 830442
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 830443
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 830444
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 830445
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 830458
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 830459
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 830460
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 830461
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 830462
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    .line 830463
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 830464
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$RepresentedProfileModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 830465
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$RepresentedProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$RepresentedProfileModel;

    .line 830466
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->l()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$RepresentedProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 830467
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    .line 830468
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->h:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel$RepresentedProfileModel;

    .line 830469
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 830470
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 830471
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 830472
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    .line 830473
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->i:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 830474
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 830475
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830432
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 830429
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 830430
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->f:I

    .line 830431
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 830427
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 830428
    iget v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 830424
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;-><init>()V

    .line 830425
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 830426
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830423
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830420
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$EditReviewActionLinkFieldsModel$ReviewModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 830422
    const v0, -0x5957eda6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 830421
    const v0, -0x7d2175f

    return v0
.end method
