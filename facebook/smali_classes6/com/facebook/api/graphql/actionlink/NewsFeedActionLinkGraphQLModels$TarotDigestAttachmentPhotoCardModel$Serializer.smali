.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 837038
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel;

    new-instance v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 837039
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 837040
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 837041
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 837042
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 837043
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 837044
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 837045
    if-eqz v2, :cond_0

    .line 837046
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837047
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 837048
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 837049
    if-eqz v2, :cond_1

    .line 837050
    const-string p0, "card_description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837051
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837052
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 837053
    if-eqz v2, :cond_2

    .line 837054
    const-string p0, "card_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837055
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837056
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 837057
    if-eqz v2, :cond_3

    .line 837058
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837059
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837060
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 837061
    if-eqz v2, :cond_4

    .line 837062
    const-string p0, "photo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837063
    invoke-static {v1, v2, p1, p2}, LX/57c;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 837064
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 837065
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 837066
    check-cast p1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel$Serializer;->a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$TarotDigestAttachmentPhotoCardModel;LX/0nX;LX/0my;)V

    return-void
.end method
