.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 836225
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 836226
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 836227
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 836228
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 836229
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 836230
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 836231
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 836232
    if-eqz v2, :cond_0

    .line 836233
    const-string p0, "featured_instant_article_element"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 836234
    invoke-static {v1, v2, p1}, LX/57S;->a(LX/15i;ILX/0nX;)V

    .line 836235
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 836236
    if-eqz v2, :cond_1

    .line 836237
    const-string p0, "instant_article"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 836238
    invoke-static {v1, v2, p1}, LX/57T;->a(LX/15i;ILX/0nX;)V

    .line 836239
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 836240
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 836241
    check-cast p1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ReadInstantArticleActionLinkFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
