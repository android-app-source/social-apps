.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x58300206
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 835661
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 835660
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 835658
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 835659
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 835656
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;->e:Ljava/lang/String;

    .line 835657
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 835654
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;->f:Ljava/lang/String;

    .line 835655
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 835646
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 835647
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 835648
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 835649
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 835650
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 835651
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 835652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 835653
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 835643
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 835644
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 835645
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 835640
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$ProfileVideoMsqrdOverlayActionLinkFieldsModel$MaskModel;-><init>()V

    .line 835641
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 835642
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 835638
    const v0, 0xece61c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 835639
    const v0, -0x7c6a937f

    return v0
.end method
