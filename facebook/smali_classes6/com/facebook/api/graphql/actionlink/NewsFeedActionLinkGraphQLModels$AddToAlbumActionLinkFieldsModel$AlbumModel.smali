.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7146bc04
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:J

.field private k:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$ExplicitPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$MediaOwnerObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:J

.field private p:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PhotoItemsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 830244
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 830251
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 830249
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 830250
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830247
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->e:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->e:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 830248
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->e:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    return-object v0
.end method

.method private k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$ExplicitPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830245
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->k:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$ExplicitPlaceModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$ExplicitPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$ExplicitPlaceModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->k:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$ExplicitPlaceModel;

    .line 830246
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->k:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$ExplicitPlaceModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830242
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->l:Ljava/lang/String;

    .line 830243
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$MediaOwnerObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830240
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->m:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$MediaOwnerObjectModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$MediaOwnerObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$MediaOwnerObjectModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->m:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$MediaOwnerObjectModel;

    .line 830241
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->m:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$MediaOwnerObjectModel;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830238
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 830239
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830236
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->p:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$OwnerModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->p:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$OwnerModel;

    .line 830237
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->p:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$OwnerModel;

    return-object v0
.end method

.method private p()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PhotoItemsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830252
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->q:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PhotoItemsModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PhotoItemsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PhotoItemsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->q:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PhotoItemsModel;

    .line 830253
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->q:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PhotoItemsModel;

    return-object v0
.end method

.method private q()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830234
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->r:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->r:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    .line 830235
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->r:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830232
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->s:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->s:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 830233
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->s:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830230
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->t:Ljava/lang/String;

    .line 830231
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->t:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 830200
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 830201
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->j()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 830202
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$ExplicitPlaceModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 830203
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->l()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 830204
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$MediaOwnerObjectModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 830205
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 830206
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$OwnerModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 830207
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->p()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PhotoItemsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 830208
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->q()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 830209
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 830210
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->s()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 830211
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 830212
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 830213
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->f:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 830214
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->g:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 830215
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->h:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 830216
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 830217
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->j:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 830218
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 830219
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 830220
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 830221
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 830222
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->o:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 830223
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 830224
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 830225
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 830226
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 830227
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 830228
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 830229
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 830162
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 830163
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$ExplicitPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 830164
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$ExplicitPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$ExplicitPlaceModel;

    .line 830165
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->k()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$ExplicitPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 830166
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    .line 830167
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->k:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$ExplicitPlaceModel;

    .line 830168
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$MediaOwnerObjectModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 830169
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$MediaOwnerObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$MediaOwnerObjectModel;

    .line 830170
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$MediaOwnerObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 830171
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    .line 830172
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->m:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$MediaOwnerObjectModel;

    .line 830173
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 830174
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 830175
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 830176
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    .line 830177
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 830178
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 830179
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$OwnerModel;

    .line 830180
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->o()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 830181
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    .line 830182
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->p:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$OwnerModel;

    .line 830183
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->p()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PhotoItemsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 830184
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->p()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PhotoItemsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PhotoItemsModel;

    .line 830185
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->p()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PhotoItemsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 830186
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    .line 830187
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->q:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PhotoItemsModel;

    .line 830188
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->q()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 830189
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->q()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    .line 830190
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->q()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 830191
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    .line 830192
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->r:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel$PrivacyScopeModel;

    .line 830193
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 830194
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 830195
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 830196
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    .line 830197
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->s:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 830198
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 830199
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 830148
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 830154
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 830155
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->f:Z

    .line 830156
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->g:Z

    .line 830157
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->h:Z

    .line 830158
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->i:Z

    .line 830159
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->j:J

    .line 830160
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;->o:J

    .line 830161
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 830151
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$AddToAlbumActionLinkFieldsModel$AlbumModel;-><init>()V

    .line 830152
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 830153
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 830150
    const v0, -0x6d8d46b0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 830149
    const v0, 0x3c68e4f

    return v0
.end method
