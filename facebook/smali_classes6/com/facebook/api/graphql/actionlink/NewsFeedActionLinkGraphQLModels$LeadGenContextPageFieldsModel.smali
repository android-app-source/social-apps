.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2c436d0f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextCardPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 832087
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 832086
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 832084
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 832085
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextCardPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 832082
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextCardPhotoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextCardPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextCardPhotoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextCardPhotoModel;

    .line 832083
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextCardPhotoModel;

    return-object v0
.end method

.method private j()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 832080
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->f:Ljava/util/List;

    .line 832081
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 832078
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    .line 832079
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->g:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 832076
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->h:Ljava/lang/String;

    .line 832077
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 832038
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextImageModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextImageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextImageModel;

    .line 832039
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextImageModel;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 832074
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->j:Ljava/lang/String;

    .line 832075
    iget-object v0, p0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 832058
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 832059
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextCardPhotoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 832060
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 832061
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->k()Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 832062
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 832063
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextImageModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 832064
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 832065
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 832066
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 832067
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 832068
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 832069
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 832070
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 832071
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 832072
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 832073
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 832045
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 832046
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextCardPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 832047
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextCardPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextCardPhotoModel;

    .line 832048
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->a()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextCardPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 832049
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;

    .line 832050
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->e:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextCardPhotoModel;

    .line 832051
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 832052
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextImageModel;

    .line 832053
    invoke-direct {p0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->m()Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 832054
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;

    .line 832055
    iput-object v0, v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;->i:Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel$ContextImageModel;

    .line 832056
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 832057
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 832042
    new-instance v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$LeadGenContextPageFieldsModel;-><init>()V

    .line 832043
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 832044
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 832041
    const v0, -0x7774f04c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 832040
    const v0, 0x2a1e1a0a

    return v0
.end method
