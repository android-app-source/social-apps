.class public final Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 835246
    const-class v0, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 835247
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 835248
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 835249
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 835250
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 835251
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 835252
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 835253
    if-eqz v2, :cond_0

    .line 835254
    const-string p0, "description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 835255
    invoke-static {v1, v2, p1}, LX/57C;->a(LX/15i;ILX/0nX;)V

    .line 835256
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 835257
    if-eqz v2, :cond_1

    .line 835258
    const-string p0, "info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 835259
    invoke-static {v1, v2, p1, p2}, LX/57F;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 835260
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 835261
    if-eqz v2, :cond_2

    .line 835262
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 835263
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 835264
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 835265
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 835266
    check-cast p1, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/actionlink/NewsFeedActionLinkGraphQLModels$OverlayActionLinkFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
