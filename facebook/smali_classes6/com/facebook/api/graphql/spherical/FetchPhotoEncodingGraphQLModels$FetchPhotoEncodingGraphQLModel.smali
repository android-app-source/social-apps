.class public final Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x295c1639
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$TilesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1061680
    const-class v0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1061681
    const-class v0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1061693
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1061694
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1061682
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1061683
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1061684
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1061685
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1061686
    invoke-direct {p0}, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1061687
    invoke-virtual {p0}, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1061688
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1061689
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1061690
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1061691
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1061692
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$TilesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1061670
    iget-object v0, p0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$TilesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;->f:Ljava/util/List;

    .line 1061671
    iget-object v0, p0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1061672
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1061673
    invoke-virtual {p0}, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1061674
    invoke-virtual {p0}, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1061675
    if-eqz v1, :cond_0

    .line 1061676
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;

    .line 1061677
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;->f:Ljava/util/List;

    .line 1061678
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1061679
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1061669
    new-instance v0, LX/6B8;

    invoke-direct {v0, p1}, LX/6B8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1061667
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1061668
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1061666
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1061663
    new-instance v0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;-><init>()V

    .line 1061664
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1061665
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1061661
    const v0, 0x23da41ff

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1061662
    const v0, 0x252222

    return v0
.end method
