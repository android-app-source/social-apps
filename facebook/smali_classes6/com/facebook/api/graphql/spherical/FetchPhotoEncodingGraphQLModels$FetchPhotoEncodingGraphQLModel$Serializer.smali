.class public final Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1061582
    const-class v0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;

    new-instance v1, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1061583
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1061584
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1061585
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1061586
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1061587
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1061588
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1061589
    if-eqz v2, :cond_0

    .line 1061590
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1061591
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1061592
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1061593
    if-eqz v2, :cond_2

    .line 1061594
    const-string p0, "tiles"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1061595
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1061596
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_1

    .line 1061597
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1}, LX/6BA;->a(LX/15i;ILX/0nX;)V

    .line 1061598
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 1061599
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1061600
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1061601
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1061602
    check-cast p1, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$Serializer;->a(Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
