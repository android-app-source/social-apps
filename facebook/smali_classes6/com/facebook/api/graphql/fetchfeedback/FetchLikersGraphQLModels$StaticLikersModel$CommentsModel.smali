.class public final Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$CommentsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$CommentsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$CommentsModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 864765
    const-class v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$CommentsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 864764
    const-class v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$CommentsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 864762
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 864763
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 864760
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 864761
    iget v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$CommentsModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 864766
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 864767
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 864768
    iget v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$CommentsModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 864769
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 864770
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 864757
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 864758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 864759
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 864754
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 864755
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$CommentsModel;->e:I

    .line 864756
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 864751
    new-instance v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$CommentsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel$CommentsModel;-><init>()V

    .line 864752
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 864753
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 864750
    const v0, -0x56c146e7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 864749
    const v0, -0x603ebee

    return v0
.end method
